# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( LArCalibUtils )

<<<<<<< HEAD
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Calorimeter/CaloIdentifier
                          Control/AthenaBaseComps
                          Control/AthenaKernel
                          Control/StoreGate
                          DetectorDescription/Identifier
                          GaudiKernel
                          LArCalorimeter/LArElecCalib
                          LArCalorimeter/LArIdentifier
                          LArCalorimeter/LArRawConditions
                          LArCalorimeter/LArRawEvent
                          LArCalorimeter/LArRawUtils
                          LArCalorimeter/LArRecConditions
                          LArCalorimeter/LArRecUtils
                          LArCalorimeter/LArCabling
                          TestBeam/TBEvent
                          PRIVATE
                          Calorimeter/CaloDetDescr
                          Database/AthenaPOOL/AthenaPoolUtilities
                          Event/xAOD/xAODEventInfo
                          LArCalorimeter/LArBadChannelTool
                          LArCalorimeter/LArCOOLConditions
                          LArCalorimeter/LArSimEvent
                          Trigger/TrigAnalysis/TrigAnalysisInterfaces )

>>>>>>> release/21.0.127
# External dependencies:
find_package( CLHEP )
find_package( CORAL COMPONENTS CoralBase )
find_package( Eigen )
find_package( GSL )
find_package( ROOT COMPONENTS Minuit Core Tree MathCore Hist RIO Graf )
find_package( TBB )

# Component(s) in the package:
atlas_add_component( LArCalibUtils
                     src/*.cxx
                     src/components/*.cxx
                     INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${GSL_INCLUDE_DIRS} ${CORAL_INCLUDE_DIRS} ${EIGEN_INCLUDE_DIRS} ${TBB_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
<<<<<<< HEAD
                     LINK_LIBRARIES ${ROOT_LIBRARIES} ${GSL_LIBRARIES} ${CORAL_LIBRARIES} ${EIGEN_LIBRARIES} ${TBB_LIBRARIES} ${CLHEP_LIBRARIES} CaloIdentifier AthContainers AthenaBaseComps AthenaKernel StoreGateLib Identifier GaudiKernel LArElecCalib LArIdentifier LArRawConditions LArRawEvent LArRawUtilsLib LArRecConditions LArRecUtilsLib LArCablingLib TBEvent CaloDetDescrLib AthenaPoolUtilities xAODEventInfo LArBadChannelToolLib LArCOOLConditions LArSimEvent TrigAnalysisInterfaces )
=======
                     LINK_LIBRARIES ${ROOT_LIBRARIES} ${GSL_LIBRARIES} ${CORAL_LIBRARIES} ${EIGEN_LIBRARIES} ${TBB_LIBRARIES} ${CLHEP_LIBRARIES} CaloIdentifier AthenaBaseComps AthenaKernel StoreGateLib SGtests Identifier GaudiKernel LArIdentifier LArRawConditions LArRawEvent LArRawUtilsLib LArRecConditions LArRecUtilsLib LArCablingLib TBEvent CaloDetDescrLib AthenaPoolUtilities xAODEventInfo LArBadChannelToolLib LArCOOLConditions LArSimEvent )
>>>>>>> release/21.0.127

# Install files from the package:
atlas_install_python_modules( python/*.py POST_BUILD_CMD ${ATLAS_FLAKE8} )
