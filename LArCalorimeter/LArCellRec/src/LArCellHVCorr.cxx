/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

<<<<<<< HEAD
#include "LArCellHVCorr.h" 
=======
#include "LArCellRec/LArCellHVCorr.h" 
>>>>>>> release/21.0.127
#include "CaloEvent/CaloCell.h"


LArCellHVCorr::LArCellHVCorr (const std::string& type, 
				  const std::string& name, 
				  const IInterface* parent) :
  CaloCellCorrection(type, name, parent)
{ 
  declareInterface<CaloCellCorrection>(this); 
  declareInterface<ILArCellHVCorrTool>(this);
}


LArCellHVCorr::~LArCellHVCorr() {}


StatusCode LArCellHVCorr::initialize() {

<<<<<<< HEAD
  ATH_CHECK( m_cablingKey.initialize() );
  ATH_CHECK( m_scaleCorrKey.initialize() );
=======
  ATH_CHECK( m_hvCorrTool.retrieve() );

  // if (m_undoHVonline) {
  //   sc = detStore()->regHandle(m_dd_HVScaleCorr,m_keyHVScaleCorr);
  //   if (sc.isFailure()) {
  //     msg(MSG::ERROR) << "Unable to register handle to HVScaleCorr " << endmsg;
  //     return StatusCode::FAILURE;
  //   }
  // }

  ATH_CHECK(detStore()->regFcn(&ILArHVCorrTool::LoadCalibration,&(*m_hvCorrTool),&ILArCellHVCorrTool::LoadCalibration,(ILArCellHVCorrTool*)this));

  ATH_MSG_INFO( "Registered callback on ILArHVCorrTool"  );

  ATH_CHECK(detStore()->regFcn(&LArCellHVCorr::LoadCalibration,this,m_dd_HVScaleCorr,m_keyHVScaleCorr));

  ATH_MSG_INFO( "Registered callback on DataHandle<ILArHVScaleCorr>"  );
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}


<<<<<<< HEAD
float LArCellHVCorr::getCorrection(const Identifier id)
{
  //Here we ADD const-ness so const_cast is fine
  return const_cast<const LArCellHVCorr*>(this)->getCorrection (id);
=======
StatusCode  LArCellHVCorr::LoadCalibration(IOVSVC_CALLBACK_ARGS) {
  //Dummy callback method to forward callback on HV update to CaloNoiseTool
  ATH_MSG_DEBUG( "LArCellHVCorr::LoadCalibration callback invoked"  );
  m_updateOnLastCallback=m_hvCorrTool->updateOnLastCallback();
  if (!m_updateOnLastCallback) 
    ATH_MSG_DEBUG( "No real HV change, chaches remain valid"  );
  return StatusCode::SUCCESS;
>>>>>>> release/21.0.127
}

float LArCellHVCorr::getCorrection(const Identifier id) const
{
  // this is highly ineffective, but this tool will be soon decommissioned, so could live with this for some time...
  SG::ReadCondHandle<LArOnOffIdMapping> cablingHdl{m_cablingKey};
  const LArOnOffIdMapping* cabling{*cablingHdl};
  if(!cabling){
      ATH_MSG_WARNING("Do not have mapping object " << m_cablingKey.key() << " returning 1.");
      return 1.;
  }
 SG::ReadCondHandle<ILArHVScaleCorr> scaleCorr (m_scaleCorrKey);
 float hvcorr = scaleCorr->HVScaleCorr(cabling->createSignalChannelID(id));

 if (hvcorr<0.9 ) {
   if (hvcorr<0.4) ATH_MSG_WARNING( "HV corr for cell with id " << id.get_identifier32().get_compact() << " = " << hvcorr  );
   else ATH_MSG_DEBUG( "HV corr for cell with id " << id.get_identifier32().get_compact() << " = " << hvcorr  );
 }


 //hvcorr might be zero in case of problems with the DCS database
 if (hvcorr<0.01) hvcorr=1.0;
 
 return hvcorr;
}


void LArCellHVCorr::MakeCorrection (CaloCell* theCell,
                                    const EventContext& /*ctx*/) const
{
  const Identifier id=theCell->ID();
  const float hvcorr=getCorrection(id);
  theCell->setEnergy(theCell->energy()*hvcorr);
}  

