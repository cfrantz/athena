/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef __LARCALCULATORSVCIMP_H__
#define __LARCALCULATORSVCIMP_H__

#include "ILArCalculatorSvc.h"
#include "AthenaBaseComps/AthService.h"

<<<<<<< HEAD
class LArCalculatorSvcImp: public extends<AthService, ILArCalculatorSvc> {
=======
class LArCalculatorSvcImp: public AthService, virtual public ILArCalculatorSvc {
>>>>>>> release/21.0.127

public:

  LArCalculatorSvcImp(const std::string& name, ISvcLocator * pSvcLocator);

<<<<<<< HEAD
=======
  /** Query interface method to make athena happy */
  virtual StatusCode queryInterface(const InterfaceID&, void**) override final;

>>>>>>> release/21.0.127
protected:
  // Birks' law
  bool m_BirksLaw;

  // Birks' law, constant k
  double m_Birksk;

  // OOTcut
  double m_OOTcut;

};

#endif
