/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#ifndef LARG4BARREL_PsMap_h
#define LARG4BARREL_PsMap_h

#include "CurrMap.h"

#include <map>
#include <vector>
#include <string>

typedef std::map<int, CurrMap*> curr_map;

class PsMap {
  public:
   ~PsMap();
   static PsMap* GetPsMap();
   void SetDirectory(const std::string& dir);
   void Reset();
<<<<<<< HEAD:LArCalorimeter/LArG4/LArG4Barrel/src/PsMap.h
   CurrMap* GetMap (int module) const;
=======
   void SetMap(int module);
   CurrMap* Map() const {return m_curr;}
>>>>>>> release/21.0.127:LArCalorimeter/LArG4/LArG4Barrel/LArG4Barrel/PsMap.h
  private:
   PsMap();
   static PsMap* s_thePointer;
   curr_map m_theMap;
   std::string m_directory;
};
#endif // LARG4BARREL_PsMap_h
