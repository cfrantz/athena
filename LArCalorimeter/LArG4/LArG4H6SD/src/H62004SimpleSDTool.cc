/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#include "H62004SimpleSDTool.h"

<<<<<<< HEAD
=======
// Framework utilities
#include "CxxUtils/make_unique.h"

>>>>>>> release/21.0.127
// LArG4 includes
#include "LArG4Code/VolumeUtils.h"

// Local includes
#include "LArG4H62004SD.h"

namespace LArG4
{

  //---------------------------------------------------------------------------
  // Tool constructor
  //---------------------------------------------------------------------------
  H62004SimpleSDTool::H62004SimpleSDTool(const std::string& type,
                                         const std::string& name,
                                         const IInterface* parent)
    : SimpleSDTool(type, name, parent)
  {}

  //---------------------------------------------------------------------------
  // Create one SD
  //---------------------------------------------------------------------------
  std::unique_ptr<LArG4SimpleSD>
  H62004SimpleSDTool::makeOneSD(const std::string& sdName, ILArCalculatorSvc* calc,
                                const std::vector<std::string>& volumes) const
  {
    ATH_MSG_VERBOSE( name() << " makeOneSD" );

    // Parse the wildcard patterns for existing volume names
    auto parsedVolumes = findLogicalVolumes(volumes, msg());

    // Create the simple SD
<<<<<<< HEAD
    auto sd = std::make_unique<LArG4H62004SD>
=======
    auto sd = CxxUtils::make_unique<LArG4H62004SD>
>>>>>>> release/21.0.127
      (sdName, calc, m_timeBinType, m_timeBinWidth);
    sd->setupHelpers(m_larEmID, m_larFcalID, m_larHecID, m_larMiniFcalID);

    // Assign the volumes to the SD
    if( assignSD( sd.get(), parsedVolumes ).isFailure() ) {
      throw GaudiException("Failed to assign sd: " + sdName,
                           name(), StatusCode::FAILURE);
    }
<<<<<<< HEAD
    return sd;
=======
    return std::move(sd);
>>>>>>> release/21.0.127
  }

} // namespace LArG4
