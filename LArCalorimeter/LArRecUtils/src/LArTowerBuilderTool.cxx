/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/


#include "GaudiKernel/Service.h"

#include "CaloIdentifier/CaloCell_ID.h"

#include "CaloUtils/CaloTowerBuilderTool.h"

#include "LArTowerBuilderTool.h"

#include <string>

LArTowerBuilderTool::LArTowerBuilderTool(const std::string& name,
					 const std::string& type,
					 const IInterface* parent)
  : CaloTowerBuilderTool(name,type,parent)
{ }

LArTowerBuilderTool::~LArTowerBuilderTool()
{ }


/**
 * @brief Convert calorimeter strings to enums.
 * @param includedCalos Property with calorimeter strings.
 */
std::vector<CaloCell_ID::SUBCALO>
LArTowerBuilderTool::parseCalos 
  (const std::vector<std::string>& includedCalos) const
{
<<<<<<< HEAD
  // convert to enumerators
  std::vector<CaloCell_ID::SUBCALO> indices;

  for (const std::string& s : includedCalos) {
    if ( s == "LAREM" ) {
      indices.push_back(CaloCell_ID::LAREM);
    }
    else if ( s == "LARHEC" ) {
      indices.push_back(CaloCell_ID::LARHEC);
=======
  // allow only LAREM and LARHEC cells!
  for ( size_t iCalos=0; iCalos<m_includedCalos.size(); iCalos++ )
    {
      if ( m_includedCalos[iCalos] == "LAREM" )
	{
	  m_caloIndices.push_back(CaloCell_ID::LAREM);
	}
      else if ( m_includedCalos[iCalos] == "LARHEC" )
	{
	  m_caloIndices.push_back(CaloCell_ID::LARHEC);
	}
      else if ( m_includedCalos[iCalos] == "LARFCAL" )
	{
	  ATH_MSG_INFO( "use LArFCalTowerBuilderTool for the FCal - request ignored" );
	}
>>>>>>> release/21.0.127
    }
    else if ( s == "LARFCAL" ) {
      ATH_MSG_INFO( "use LArFCalTowerBuilderTool for the FCal - request ignored" );
    }
  }

<<<<<<< HEAD
  return indices;
=======
  // check setup
  return this->checkSetup(msg());
>>>>>>> release/21.0.127
}

