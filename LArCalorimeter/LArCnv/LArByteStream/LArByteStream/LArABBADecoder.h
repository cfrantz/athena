//Dear emacs, this is -*- c++ -*-

/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#ifndef LARBYTESTREAM_LARABBADDECODER_H
#define LARBYTESTREAM_LARABBADDECODER_H

#include <stdint.h>

#include "AthenaBaseComps/AthAlgTool.h"
//#include "GaudiKernel/ToolHandle.h"

#include "LArRawEvent/LArDigitContainer.h"

#include "ByteStreamData/RawEvent.h" 

// #include "eformat/RODHeader.h"
#include "eformat/Version.h"
#include "eformat/Issue.h"
#include <vector>
#include <string>
#include "eformat/index.h"
//#include "eformat/ROBFragment.h"
//class ROBFragment;

<<<<<<< HEAD
=======
using namespace OFFLINE_FRAGMENTS_NAMESPACE ; 
>>>>>>> release/21.0.127

class LArABBADecoder : public AthAlgTool {

public: 

  LArABBADecoder(const std::string& type, const std::string& name,
		 const IInterface* parent ) ;

  static const InterfaceID& interfaceID( ) ;
  /** Destructor
   */ 
  virtual ~LArABBADecoder(); 

<<<<<<< HEAD
  virtual StatusCode initialize() override;
  virtual StatusCode finalize() override;


  StatusCode convert(const RawEvent* re, LArDigitContainer* coll) const;

private:

  void fillCollection(const OFFLINE_FRAGMENTS_NAMESPACE::ROBFragment* pROB,
                      LArDigitContainer* coll) const;
=======
  virtual StatusCode initialize();
  virtual StatusCode finalize();


  StatusCode convert(const RawEvent* re, LArDigitContainer* coll);

private:

  void fillCollection(const ROBFragment* pROB, LArDigitContainer* coll);
>>>>>>> release/21.0.127
}; 

#endif

