/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "LArByteStream/LArAccumulatedCalibDigitContByteStreamCnv.h"
#include "LArByteStream/LArRawDataContByteStreamTool.h"

#include "CaloIdentifier/CaloGain.h"

#include "ByteStreamCnvSvc/ByteStreamCnvSvc.h"
#include "ByteStreamCnvSvcBase/ByteStreamCnvSvcBase.h" 
#include "ByteStreamCnvSvcBase/ByteStreamAddress.h" 
#include "ByteStreamCnvSvcBase/IROBDataProviderSvc.h" 
#include "ByteStreamData/RawEvent.h" 

#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IRegistry.h"

#include "LArRawEvent/LArAccumulatedCalibDigitContainer.h"
#include "LArRecConditions/LArCalibLineMapping.h"

#include "StoreGate/StoreGateSvc.h"
#include "StoreGate/ReadCondHandle.h"
#include "AthenaKernel/CLASS_DEF.h"

// Tool 
#include "GaudiKernel/IToolSvc.h"

//STL-Stuff
#include <map> 
#include <iostream>


LArAccumulatedCalibDigitContByteStreamCnv::LArAccumulatedCalibDigitContByteStreamCnv(ISvcLocator* svcloc) :
  AthConstConverter(storageType(), classID(),svcloc,"LArAccumulatedCalibDigitContByteStreamCnv"),
  m_tool("LArRawDataContByteStreamTool"),
  m_rdpSvc("ROBDataProviderSvc", name()),
  m_calibLineMappingKey ("LArCalibLineMap")
{}

const CLID& LArAccumulatedCalibDigitContByteStreamCnv::classID(){
  return ClassID_traits<LArAccumulatedCalibDigitContainer>::ID() ;
}

LArAccumulatedCalibDigitContByteStreamCnv::~LArAccumulatedCalibDigitContByteStreamCnv() {
}


StatusCode
LArAccumulatedCalibDigitContByteStreamCnv::initialize()
{
<<<<<<< HEAD
  ATH_CHECK( AthConstConverter::initialize() );

  if ( m_rdpSvc.retrieve().isFailure()) {
    ATH_MSG_WARNING(  " Can't get ByteStreamInputSvc interface Reading of ByteStream Data not possible. " );
=======
   StatusCode sc = Converter::initialize(); 
   if(StatusCode::SUCCESS!=sc) 
   { 
    return sc; 
   } 
   m_log = new MsgStream(msgSvc(),"LArAccumulatedCalibDigitBSCnv");
   IService* svc;
  //Get ByteStreamCnvSvc
  if(StatusCode::SUCCESS != serviceLocator()->getService("ByteStreamCnvSvc",svc)){
    (*m_log) << MSG::ERROR << " Can't get ByteStreamEventAccess interface " << endmsg;
    return StatusCode::FAILURE;
  }
  m_ByteStreamEventAccess=dynamic_cast<ByteStreamCnvSvc*>(svc);
  if (m_ByteStreamEventAccess==NULL)
    {
      (*m_log) <<MSG::ERROR<< "  LArAccumulatedCalibDigitContByteStreamCnv: Can't cast to  ByteStreamCnvSvc " <<endmsg; 
      return StatusCode::FAILURE ;
    }

  //Get ByteStreamInputSvc (only necessary for reading of digits, not for writing and for channels)
  
  if(StatusCode::SUCCESS != serviceLocator()->getService("ROBDataProviderSvc",svc)){
    (*m_log) << MSG::WARNING << " Can't get ByteStreamInputSvc interface Reading of ByteStream Data not possible. " << endmsg;
    m_rdpSvc=0;
  }
  else {
    m_rdpSvc=dynamic_cast<IROBDataProviderSvc*>(svc);
    if(m_rdpSvc == 0 ) {
      (*m_log) <<MSG::ERROR<< "  LArAccumulatedCalibDigitContByteStreamCnv: Can't cast to  ByteStreamInputSvc " <<endmsg; 
      return StatusCode::FAILURE;
    }
  }

  // retrieve ToolSvc
  IToolSvc* toolSvc;

  if(StatusCode::SUCCESS != service("ToolSvc",toolSvc)){
    (*m_log) << MSG::ERROR << " Can't get ToolSvc " << endmsg;
    return StatusCode::FAILURE;
  }

  //Get LArByteStreamTool
  std::string toolType = "LArRawDataContByteStreamTool" ; 
  if(StatusCode::SUCCESS !=toolSvc->retrieveTool(toolType,m_tool))
  {
    (*m_log) << MSG::ERROR << " Can't get LArRawDataByteStreamTool " << endmsg;
    return StatusCode::FAILURE;
>>>>>>> release/21.0.127
  }

  ATH_CHECK( m_tool.retrieve() );
  ATH_CHECK( m_calibLineMappingKey.initialize() );

  return StatusCode::SUCCESS;
}

StatusCode
LArAccumulatedCalibDigitContByteStreamCnv::createObjConst(IOpaqueAddress* pAddr, DataObject*& pObj) const
{//Convert Accumulated Calib Digits from ByteStream to StoreGate
<<<<<<< HEAD
  ATH_MSG_VERBOSE(  "Executing CreateObj method for LArAccumulatedCalibDigitContainer " );
  

  if (!m_rdpSvc) {
    ATH_MSG_ERROR( " ROBDataProviderSvc not loaded. Can't read ByteStream." );
    return StatusCode::FAILURE;
  }
  ByteStreamAddress *pRE_Addr;
  pRE_Addr = dynamic_cast<ByteStreamAddress*>(pAddr); //Cast from OpaqueAddress to ByteStreamAddress
  if (!pRE_Addr) {
    ATH_MSG_ERROR(  "dynamic_cast of OpaqueAdress to ByteStreamAddress failed!" );
    return StatusCode::FAILURE;
  }

  const RawEvent* re = m_rdpSvc->getEvent();
  if (!re) {
    ATH_MSG_ERROR(  "Could not get raw event from ByteStreamInputSvc" );
  }
=======
  MsgStream log(msgSvc(), "LArAccumulatedCalibDigitContByteStreamCnv");
  (*m_log) << MSG::VERBOSE << "Executing CreateObj method for LArAccumulatedCalibDigitContainer " << endmsg;
  

  if (!m_rdpSvc)
    {(*m_log) << MSG::ERROR << " ROBDataProviderSvc not loaded. Can't read ByteStream." << endmsg;
     return StatusCode::FAILURE;
    }
  ByteStreamAddress *pRE_Addr;
  pRE_Addr = dynamic_cast<ByteStreamAddress*>(pAddr); //Cast from OpaqueAddress to ByteStreamAddress
  if (!pRE_Addr)
    {(*m_log) << MSG::ERROR << "dynamic_cast of OpaqueAdress to ByteStreamAddress failed!" << endmsg;
     return StatusCode::FAILURE;
    }

  const RawEvent* re = m_rdpSvc->getEvent();
  if (!re)
    {(*m_log) << MSG::ERROR << "Could not get raw event from ByteStreamInputSvc" << endmsg;
     return StatusCode::FAILURE;
    }
>>>>>>> release/21.0.127
  const std::string& key = *(pAddr->par()); // Get key used in the StoreGateSvc::retrieve function
  // get gain and pass to convert function.
  CaloGain::CaloGain gain=CaloGain::LARNGAIN; //At this place, LARNGAINS means Automatic gain.
  if (key=="HIGH")
    gain=CaloGain::LARHIGHGAIN;
  else if (key=="MEDIUM")
    gain=CaloGain::LARMEDIUMGAIN;
  else if (key=="LOW")
    gain=CaloGain::LARLOWGAIN;

  SG::ReadCondHandle<LArCalibLineMapping> calibLineMapping (m_calibLineMappingKey);

  // Convert the RawEvent to  LArAccumulatedCalibDigitContainer
<<<<<<< HEAD
  ATH_MSG_DEBUG(  "Converting LArAccumulatedCalibDigits (from ByteStream). key=" << key << " ,gain=" << gain );
  LArAccumulatedCalibDigitContainer *DigitContainer=new LArAccumulatedCalibDigitContainer;
  StatusCode sc=m_tool->convert(re,DigitContainer,gain,
                                **calibLineMapping);
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_WARNING(  "Conversion tool returned an error. LArAccumulatedCalibDigitContainer might be empty." );
  }
=======
  (*m_log) << MSG::DEBUG << "Converting LArAccumulatedCalibDigits (from ByteStream). key=" << key << " ,gain=" << gain << endmsg; 
  LArAccumulatedCalibDigitContainer *DigitContainer=new LArAccumulatedCalibDigitContainer;
  StatusCode sc=m_tool->convert(re,DigitContainer,gain);
  if (sc!=StatusCode::SUCCESS)
    (*m_log) << MSG::WARNING << "Conversion tool returned an error. LArAccumulatedCalibDigitContainer might be empty." << endmsg;
>>>>>>> release/21.0.127
  DigitContainer->setDelayScale(25./240.);  //FIXME should not be hardcoded! 
  pObj = SG::asStorable(DigitContainer) ;

  return StatusCode::SUCCESS;
}

// IWS 19.07.2005 
 StatusCode 
 LArAccumulatedCalibDigitContByteStreamCnv::createRepConst(DataObject* /*pObj*/, IOpaqueAddress*& /*pAddr*/)  const
{// convert LArAccumulatedCalibDigits from StoreGate into ByteStream
<<<<<<< HEAD
  ATH_MSG_ERROR( "CreateRep method of LArAccumulatedCalibDigitContainer not implemented" );
=======
  (*m_log) << MSG::ERROR << "CreateRep method of LArAccumulatedCalibDigitContainer not implemented" << endmsg;
>>>>>>> release/21.0.127
  return StatusCode::SUCCESS;
}

