/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#include "LArByteStream/LArDigitContByteStreamCnv.h"
#include "LArByteStream/LArRawDataContByteStreamTool.h"
<<<<<<< HEAD
#include "LArByteStream/LArLATOMEDecoder.h"
#include "LArByteStream/LATOMEMapping.h"
#include "LArByteStream/Mon.h"

=======
#include "LArByteStream/LArABBADecoder.h"
#include "LArByteStream/ABBAMapping.h"
>>>>>>> release/21.0.127

#include "CaloIdentifier/CaloGain.h"

#include "ByteStreamCnvSvc/ByteStreamCnvSvc.h"
#include "ByteStreamCnvSvcBase/ByteStreamCnvSvcBase.h" 
#include "ByteStreamCnvSvcBase/ByteStreamAddress.h" 
#include "ByteStreamCnvSvcBase/IROBDataProviderSvc.h" 
#include "ByteStreamData/RawEvent.h" 

#include "LArRawEvent/LArDigitContainer.h"

#include "AthenaKernel/StorableConversions.h"
#include "AthenaKernel/CLASS_DEF.h"

//STL-Stuff
#include <map> 
#include <iostream>


LArDigitContByteStreamCnv::LArDigitContByteStreamCnv(ISvcLocator* svcloc) :
  AthConstConverter(storageType(), classID(),svcloc,"LArDigitContByteStreamCnv"),
  m_tool("LArRawDataContByteStreamTool"),
  m_scTool("LArLATOMEDecoder"),
  m_rdpSvc("ROBDataProviderSvc", name()),
  m_byteStreamEventAccess("ByteStreamCnvSvc", name()),
  m_byteStreamCnvSvc(nullptr)
{}
    
LArDigitContByteStreamCnv::~LArDigitContByteStreamCnv() {
}

const CLID& LArDigitContByteStreamCnv::classID(){
  return ClassID_traits<LArDigitContainer>::ID() ;
}


StatusCode
LArDigitContByteStreamCnv::initialize()
{
<<<<<<< HEAD
  ATH_CHECK( AthConstConverter::initialize() );

  if ( m_rdpSvc.retrieve().isFailure()) {
    ATH_MSG_WARNING(  " Can't get ByteStreamInputSvc interface Reading of ByteStream Data not possible. " );
  }

  ATH_CHECK( m_byteStreamEventAccess.retrieve() );
  m_byteStreamCnvSvc = dynamic_cast<ByteStreamCnvSvc*>(&*m_byteStreamEventAccess);
=======
   StatusCode sc = Converter::initialize(); 
   if(StatusCode::SUCCESS!=sc) 
   { 
    return sc; 
   } 
   m_log = new MsgStream(msgSvc(),"LArDigitBSCnv");
   (*m_log) << MSG::DEBUG<< " initialize " <<endmsg; 
   IService* svc;
  //Get ByteStreamCnvSvc
  if(StatusCode::SUCCESS != serviceLocator()->getService("ByteStreamCnvSvc",svc)){
    (*m_log) << MSG::ERROR << " Can't get ByteStreamEventAccess interface " << endmsg;
    return StatusCode::FAILURE;
  }
  m_ByteStreamEventAccess=dynamic_cast<ByteStreamCnvSvc*>(svc);
  if (m_ByteStreamEventAccess==NULL)
    {
      (*m_log) <<MSG::ERROR<< "  LArDigitContByteStreamCnv: Can't cast to  ByteStreamCnvSvc " <<endmsg; 
      return StatusCode::FAILURE ;
    }

  //Get ByteStreamInputSvc (only necessary for reading of digits, not for writing and for channels)
  
  if(StatusCode::SUCCESS != serviceLocator()->getService("ROBDataProviderSvc",svc)){
    (*m_log) << MSG::WARNING << " Can't get ByteStreamInputSvc interface Reading of ByteStream Data not possible. " << endmsg;
    m_rdpSvc=0;
  }
  else {
    m_rdpSvc=dynamic_cast<IROBDataProviderSvc*>(svc);
    if(m_rdpSvc == 0 ) {
      (*m_log) <<MSG::ERROR<< "  LArDigitContByteStreamCnv: Can't cast to  ByteStreamInputSvc " <<endmsg; 
      return StatusCode::FAILURE;
    }
  }

  // retrieve ToolSvc
  IToolSvc* toolSvc;

  if(StatusCode::SUCCESS != service("ToolSvc",toolSvc)){
    (*m_log) << MSG::ERROR << " Can't get ToolSvc " << endmsg;
    return StatusCode::FAILURE;
  }

  //Get LArByteStreamTool
  std::string toolType = "LArRawDataContByteStreamTool" ; 
  if(StatusCode::SUCCESS !=toolSvc->retrieveTool(toolType,m_tool))
  {
    (*m_log) << MSG::ERROR << " Can't get LArRawDataByteStreamTool " << endmsg;
    return StatusCode::FAILURE;
  }
  
  //Get SuperCellTool
  sc=toolSvc->retrieveTool("LArABBADecoder",m_scTool);
  if (sc.isFailure()) {
    (*m_log) << MSG::ERROR << "Can't get LArABBADecoder Tool" << endmsg;
    return sc;
  }
>>>>>>> release/21.0.127

  ATH_CHECK( m_tool.retrieve() );
  ATH_CHECK( m_scTool.retrieve() );

  return StatusCode::SUCCESS;
}

StatusCode
LArDigitContByteStreamCnv::createObjConst(IOpaqueAddress* pAddr, DataObject*& pObj)  const
{//Convert Digits from ByteStream to StoreGate
<<<<<<< HEAD
  ATH_MSG_VERBOSE( "Executing CreateObj method for LArDigitContainer " );
  
  if (!m_rdpSvc) {
    ATH_MSG_ERROR( " ROBDataProviderSvc not loaded. Can't read ByteStream." );
    return StatusCode::FAILURE;
  }
  ByteStreamAddress *pRE_Addr;
  pRE_Addr = dynamic_cast<ByteStreamAddress*>(pAddr); //Cast from OpaqueAddress to ByteStreamAddress
  if (!pRE_Addr) {
    ATH_MSG_ERROR( "dynamic_cast of OpaqueAdress to ByteStreamAddress failed!" );
    return StatusCode::FAILURE;
  }

  const RawEvent* re = m_rdpSvc->getEvent();
  if (!re) {
    ATH_MSG_ERROR( "Could not get raw event from ByteStreamInputSvc" );
    return StatusCode::FAILURE;
  }
=======
  (*m_log) << MSG::VERBOSE << "Executing CreateObj method for LArDigitContainer " << endmsg;
  

  if (!m_rdpSvc)
    {(*m_log) << MSG::ERROR << " ROBDataProviderSvc not loaded. Can't read ByteStream." << endmsg;
     return StatusCode::FAILURE;
    }
  ByteStreamAddress *pRE_Addr;
  pRE_Addr = dynamic_cast<ByteStreamAddress*>(pAddr); //Cast from OpaqueAddress to ByteStreamAddress
  if (!pRE_Addr)
    {(*m_log) << MSG::ERROR << "dynamic_cast of OpaqueAdress to ByteStreamAddress failed!" << endmsg;
     return StatusCode::FAILURE;
    }

  const RawEvent* re = m_rdpSvc->getEvent();
  if (!re)
    {(*m_log) << MSG::ERROR << "Could not get raw event from ByteStreamInputSvc" << endmsg;
     return StatusCode::FAILURE;
    }
>>>>>>> release/21.0.127
  const std::string& key = *(pAddr->par()); // Get key used in the StoreGateSvc::retrieve function
  // get gain and pass to convert function.
  CaloGain::CaloGain gain=CaloGain::LARNGAIN; //At this place, LARNGAINS means Automatic gain.
  bool isSC=false;
<<<<<<< HEAD
  LArDigitContainer* adc_coll=0;
  LArDigitContainer* adc_bas_coll=0;
  LArRawSCContainer* et_coll=0;
  LArRawSCContainer* et_id_coll=0;
  LArLATOMEHeaderContainer* header_coll=0;
  
=======
>>>>>>> release/21.0.127
  if (key=="HIGH")
    gain=CaloGain::LARHIGHGAIN;
  else if (key=="MEDIUM")
    gain=CaloGain::LARMEDIUMGAIN;
  else if (key=="LOW")
    gain=CaloGain::LARLOWGAIN;
<<<<<<< HEAD
  else if (key=="SC"){
    isSC=true;
    adc_coll=new LArDigitContainer();
  }
  else if(key=="SC_ADC_BAS"){
    isSC=true;
    adc_bas_coll=new LArDigitContainer();
  }
  
  // Convert the RawEvent to  LArDigitContainer
  ATH_MSG_DEBUG( "Converting LArDigits (from ByteStream). key=" << key << " ,gain=" << gain );
  StatusCode sc;
  if (!isSC) {//Regular readout
    LArDigitContainer *DigitContainer=new LArDigitContainer;
    sc=m_tool->convert(re,DigitContainer,gain);
    if (sc!=StatusCode::SUCCESS)
      ATH_MSG_WARNING( "Conversion tool returned an error. LArDigitContainer might be empty." );
    pObj = SG::asStorable(DigitContainer) ;
    return StatusCode::SUCCESS;
  }

  //Supercell readout
  sc=m_scTool->convert(re,adc_coll, adc_bas_coll, et_coll, et_id_coll, header_coll);
  if (sc!=StatusCode::SUCCESS)
    ATH_MSG_WARNING( "Conversion tool returned an error. LAr SC containers might be empty." );

  if (key=="SC"){
    pObj = SG::asStorable(adc_coll);
  }
  else if(key=="SC_ADC_BAS"){
    pObj = SG::asStorable(adc_bas_coll);
  }
=======
  else if (key=="SC")
    isSC=true;

  // Convert the RawEvent to  LArDigitContainer
  (*m_log) << MSG::DEBUG << "Converting LArDigits (from ByteStream). key=" << key << " ,gain=" << gain << endmsg; 
  LArDigitContainer *DigitContainer=new LArDigitContainer;
  StatusCode sc;
  if (!isSC) {//Regular readout
    sc=m_tool->convert(re,DigitContainer,gain);
  }
  else { //Supercell readout
    sc=m_scTool->convert(re,DigitContainer);
  }
  if (sc!=StatusCode::SUCCESS)
    (*m_log) << MSG::WARNING << "Conversion tool returned an error. LArDigitContainer might be empty." << endmsg;
    
  pObj = SG::asStorable(DigitContainer) ;
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;

}

StatusCode 
LArDigitContByteStreamCnv::createRepConst(DataObject* pObj, IOpaqueAddress*& pAddr)  const
{// convert LArDigits from StoreGate into ByteStream
<<<<<<< HEAD
  ATH_MSG_VERBOSE( "Execute CreateRep method of LArDigitContainer " );
=======
  (*m_log) << MSG::VERBOSE << "Execute CreateRep method of LArDigitContainer " << endmsg;
  StatusCode sc;
>>>>>>> release/21.0.127

  // Get Full Event Assembler
<<<<<<< HEAD
  FullEventAssembler<RodRobIdMap> *fea = 0;
  ATH_CHECK( m_byteStreamCnvSvc->getFullEventAssembler(fea,"LAr") );

  LArDigitContainer* DigitContainer=0;
  if (!SG::fromStorable (pObj, DigitContainer) || !DigitContainer) {
    ATH_MSG_ERROR( "Cannot get LArDigitContainer for DataObject. Key=" << pObj->registry()->name() );
    return StatusCode::FAILURE;    
  }

  std::string nm = pObj->registry()->name(); 
  pAddr = new ByteStreamAddress(classID(),nm,""); 

  ATH_CHECK( m_tool->WriteLArDigits(DigitContainer, *fea) );
  return StatusCode::SUCCESS;
=======
  FullEventAssembler<Hid2RESrcID> *fea = 0;
  std::string key("LAr");
  sc=m_ByteStreamEventAccess->getFullEventAssembler(fea,key);
  if (sc.isFailure())
    {(*m_log) << MSG::ERROR << "Cannot get full event assember with key \"LAr\" from ByteStreamEventAccess." << endmsg;
     return sc;
    }
//  fea=new  FullEventAssembler<Hid2RESrcID>;
  if (!fea->idMap().isInitialized()) 
  {
   sc = fea->idMap().initialize(); 
   if (sc.isFailure())
    {(*m_log) << MSG::ERROR << "Cannot initialize Hid2RESrcID " << endmsg;
     return sc;
    }
  }
  
  LArDigitContainer* DigitContainer=0;
  sc=m_storeGate->fromStorable(pObj, DigitContainer ); 
  if (sc==StatusCode::FAILURE)
    {(*m_log) << MSG::ERROR << "StoreGateSvc::fromStorable failed!" << endmsg;
     return sc;
    }
  if(!DigitContainer){
     (*m_log) << MSG::ERROR << "Cannot get LArDigitContainer for DataObject. Key=" << pObj->registry()->name() << endmsg ;
     return StatusCode::FAILURE;    
  }
   std::string nm = pObj->registry()->name(); 
   ByteStreamAddress* addr = new
       ByteStreamAddress(classID(),nm,""); 

   pAddr = addr; 

   //(*m_log) << MSG::VERBOSE << "Start conversion of LArDigitContainer requested. Writing channels and digits. " << endmsg;
   //return m_tool->convert(ChannelContainer, DigitContainer, DigitContainerArray, re, log); 
   sc=m_tool->WriteLArDigits(DigitContainer, fea);
   if (sc.isFailure())
     return sc;
   //fea->fill(re,log);
   //delete fea;
   return StatusCode::SUCCESS;
>>>>>>> release/21.0.127
}
