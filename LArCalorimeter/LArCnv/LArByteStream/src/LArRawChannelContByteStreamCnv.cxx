/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "LArByteStream/LArRawChannelContByteStreamCnv.h"
#include "LArByteStream/LArRawDataContByteStreamTool.h"

#include "ByteStreamCnvSvc/ByteStreamCnvSvc.h"
#include "ByteStreamCnvSvcBase/ByteStreamCnvSvcBase.h" 
#include "ByteStreamCnvSvcBase/ByteStreamAddress.h" 
#include "ByteStreamCnvSvcBase/IROBDataProviderSvc.h" 
#include "ByteStreamData/RawEvent.h" 

#include "LArRawEvent/LArRawChannelContainer.h"
#include "LArRawEvent/LArDigitContainer.h"

#include "AthenaKernel/CLASS_DEF.h"

//STL-Stuff
#include <map> 
#include <iostream>
#include <string>


LArRawChannelContByteStreamCnv::LArRawChannelContByteStreamCnv(ISvcLocator* svcloc) :
  AthConstConverter(storageType(), classID(),svcloc,"LArRawChannelContByteStreamCnv"),
  m_tool("LArRawDataContByteStreamTool"),
  m_rdpSvc("ROBDataProviderSvc", name()),
  m_byteStreamEventAccess("ByteStreamCnvSvc", name()),
  m_byteStreamCnvSvc(nullptr),
  m_contSize(0)
{}

const CLID& LArRawChannelContByteStreamCnv::classID(){
  return ClassID_traits<LArRawChannelContainer>::ID() ;
}

LArRawChannelContByteStreamCnv::~LArRawChannelContByteStreamCnv() {
}


StatusCode
LArRawChannelContByteStreamCnv::initialize()
{
<<<<<<< HEAD
  ATH_CHECK( AthConstConverter::initialize() );
   
  if ( m_rdpSvc.retrieve().isFailure()) {
    ATH_MSG_WARNING(  " Can't get ByteStreamInputSvc interface Reading of ByteStream Data not possible. " );
=======
   StatusCode sc = Converter::initialize(); 
   if(StatusCode::SUCCESS!=sc) 
   { 
    return sc; 
   } 
    m_log = new MsgStream(msgSvc(),"LArRawChannelBSCnv");
    (*m_log) << MSG::DEBUG<< " initialize " <<endmsg; 
   
  //Get ByteStreamCnvSvc
   IService* svc;
  if(StatusCode::SUCCESS != serviceLocator()->getService("ByteStreamCnvSvc",svc)){
    (*m_log) << MSG::ERROR << " Can't get ByteStreamEventAccess interface " << endmsg;
    return StatusCode::FAILURE;
  }
  m_ByteStreamEventAccess=dynamic_cast<ByteStreamCnvSvc*>(svc);
  if (m_ByteStreamEventAccess==NULL)
    {
      (*m_log) << MSG::ERROR<< "  LArDigitContByteStreamCnv: Can't cast to  ByteStreamCnvSvc " <<endmsg; 
      return StatusCode::FAILURE ;
    }

  if(StatusCode::SUCCESS != serviceLocator()->getService("ROBDataProviderSvc",svc)){
    (*m_log) << MSG::WARNING << " Can't get ByteStreamInputSvc interface Reading of ByteStream Data not possible. " << endmsg;
    m_rdpSvc=0;
  }
  else {
    m_rdpSvc=dynamic_cast<IROBDataProviderSvc*>(svc);
    if(m_rdpSvc == 0 ) {
      (*m_log) <<MSG::ERROR<< "  LArDigitContByteStreamCnv: Can't cast to  ByteStreamInputSvc " <<endmsg; 
      return StatusCode::FAILURE;
    }
>>>>>>> release/21.0.127
  }

  ATH_CHECK( m_byteStreamEventAccess.retrieve() );
  m_byteStreamCnvSvc = dynamic_cast<ByteStreamCnvSvc*>(&*m_byteStreamEventAccess);

<<<<<<< HEAD
  ATH_CHECK( m_tool.retrieve() );
=======
  if(StatusCode::SUCCESS != service("ToolSvc",toolSvc)){
    (*m_log) << MSG::ERROR << " Can't get ToolSvc " << endmsg;
    return StatusCode::FAILURE;
  }
  // retrieve ConversionTool
  if(StatusCode::SUCCESS !=toolSvc->retrieveTool("LArRawDataContByteStreamTool",m_tool))
  {
    (*m_log) << MSG::ERROR << " Can't get ByteStreamTool " << endmsg;
    return StatusCode::FAILURE;
  }
  //retrieve StoreGateSvc
  return service("StoreGateSvc", m_storeGate); 
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}

StatusCode
LArRawChannelContByteStreamCnv::createObjConst(IOpaqueAddress* pAddr, DataObject*& pObj)  const
{ //Convert RawChannels from ByteStream to StoreGate
<<<<<<< HEAD
=======
  //(*m_log) << MSG::VERBOSE << "Executing CreateObj method for LArRawChannelContainer " << endmsg;
>>>>>>> release/21.0.127
    
  ByteStreamAddress *pRE_Addr;
  pRE_Addr = dynamic_cast<ByteStreamAddress*>(pAddr); 
  if(!pRE_Addr) {
<<<<<<< HEAD
    ATH_MSG_ERROR( " Can not cast to ByteStreamAddress " );
=======
    (*m_log) << MSG::ERROR << " Can not cast to ByteStreamAddress " << endmsg ; 
>>>>>>> release/21.0.127
    return StatusCode::FAILURE;    
  }
  
  const RawEvent* re = m_rdpSvc->getEvent();
<<<<<<< HEAD
  if (!re) {
    ATH_MSG_ERROR( "Could not get raw event from ByteStreamInputSvc" );
    return StatusCode::FAILURE;
  }
=======
  if (!re)
    {(*m_log) << MSG::ERROR << "Could not get raw event from ByteStreamInputSvc" << endmsg;
     return StatusCode::FAILURE;
    }
>>>>>>> release/21.0.127

  // create LArRawChannelContainer
  LArRawChannelContainer* channelContainer = new LArRawChannelContainer() ;
  if (m_contSize) channelContainer->reserve(m_contSize);

  StatusCode sc=m_tool->convert(re,channelContainer,CaloGain::CaloGain(0)); //Gain is a dummy variable in this case...
<<<<<<< HEAD
  if (sc!=StatusCode::SUCCESS) {
    ATH_MSG_WARNING( "Conversion tool returned an error. LArRawChannelContainer might be empty." );
  }
    
  pObj = StoreGateSvc::asStorable( channelContainer ) ; 

  ATH_MSG_DEBUG( "Created a LArRawChannelContainer of size" << channelContainer->size() );

  if (!m_contSize) {
    m_contSize=channelContainer->size();
    ATH_MSG_DEBUG( "For the following events, we will reserve space for " 
                   << m_contSize << " LArRawchannels" );
=======
  if (sc!=StatusCode::SUCCESS)
    (*m_log) << MSG::WARNING << "Conversion tool returned an error. LArRawChannelContainer might be empty." << endmsg;
    
  pObj = StoreGateSvc::asStorable( channelContainer ) ; 

  if (m_log->level()<=MSG::DEBUG) {
    (*m_log) << MSG::DEBUG << "Created a LArRawChannelContainer of size" << channelContainer->size() << endmsg;
  }
  if (!m_contSize) {
    m_contSize=channelContainer->size();
    (*m_log) << MSG::DEBUG << "For the following events, we will reserve space for " 
	     << m_contSize << " LArRawchannels" << endmsg;
>>>>>>> release/21.0.127
  }
    
  return StatusCode::SUCCESS;  
    
  
}

StatusCode 
LArRawChannelContByteStreamCnv::createRepConst(DataObject* pObj, IOpaqueAddress*& pAddr) const
{ // convert LArRawChannels in the container into ByteStream
<<<<<<< HEAD
   
   // Get Full Event Assembler
  FullEventAssembler<RodRobIdMap> *fea = 0;
  ATH_CHECK( m_byteStreamCnvSvc->getFullEventAssembler(fea,"LAr") );

  LArRawChannelContainer* ChannelContainer = nullptr;
  if (!SG::fromStorable (pObj, ChannelContainer ) || !ChannelContainer) {
    ATH_MSG_ERROR( " Can not cast to LArRawChannelContainer " );
=======
  //(*m_log) << MSG::VERBOSE << "Executing CreateRep method of LArRawChannelContainer" << endmsg;
   StatusCode sc;
   
   // Get Full Event Assembler
   FullEventAssembler<Hid2RESrcID> *fea = 0;
   sc=m_ByteStreamEventAccess->getFullEventAssembler(fea,"LAr");
   if (sc.isFailure())
     {(*m_log) << MSG::ERROR << "Cannot get full event assember with key \"LAr\" from ByteStreamEventAccess." << endmsg;
      return sc;
     }
   if (!fea->idMap().isInitialized())
   {
    sc = fea->idMap().initialize();
    if (sc.isFailure())
     {(*m_log) << MSG::ERROR << "Cannot initialize Hid2RESrcID " << endmsg;
      return sc;
     } 
   }


   LArRawChannelContainer* ChannelContainer=0; 
   StoreGateSvc::fromStorable(pObj, ChannelContainer ); 
   if(!ChannelContainer){
    (*m_log) << MSG::ERROR << " Can not cast to LArRawChannelContainer " << endmsg ;
>>>>>>> release/21.0.127
    return StatusCode::FAILURE;    
  } 

  const std::string& nm = pObj->registry()->name(); 

  pAddr = new  ByteStreamAddress(classID(),nm,""); 

  ATH_CHECK( m_tool->WriteLArRawChannels(ChannelContainer,*fea) );
  return StatusCode::SUCCESS;
}
