///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

// METSoftAssociator.cxx
// Implementation file for class METSoftAssociator
//
//  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//
// Author: P Loch, S Resconi, TJ Khoo, AS Mete
///////////////////////////////////////////////////////////////////

// METReconstruction includes
#include "METReconstruction/METSoftAssociator.h"
<<<<<<< HEAD
#include "xAODCaloEvent/CaloVertexedClusterBase.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
=======
#include "xAODCaloEvent/CaloClusterChangeSignalState.h"
#include "xAODCaloEvent/CaloClusterContainer.h"

#include "PFlowUtils/IWeightPFOTool.h"
>>>>>>> release/21.0.127

namespace met {

  using namespace xAOD;
  static const SG::AuxElement::Decorator<std::vector<ElementLink<IParticleContainer> > > dec_softConst("softConstituents");

  // Constructors
  ////////////////
  METSoftAssociator::METSoftAssociator(const std::string& name) :
    AsgTool(name),
    METAssociator(name),
    m_lcmodclus_key("LCOriginTopoClusters"),
    m_emmodclus_key("EMOriginTopoClusters")
  {
    declareProperty("DecorateSoftConst", m_decorateSoftTermConst=false);
<<<<<<< HEAD
    declareProperty("LCModClusterKey",   m_lcmodclus_key);
    declareProperty("EMModClusterKey",   m_emmodclus_key);
=======
    declareProperty("LCModClusterKey",   m_lcmodclus_key = "LCOriginTopoClusters");
    declareProperty("EMModClusterKey",   m_emmodclus_key = "EMOriginTopoClusters");
    declareProperty("WeightSoftPFO",     m_weight_soft_pfo = false);
>>>>>>> release/21.0.127
  }

  // Destructor
  ///////////////
  METSoftAssociator::~METSoftAssociator()
  {}

  // Athena algtool's Hooks
  ////////////////////////////
  StatusCode METSoftAssociator::initialize()
  {
    ATH_CHECK( METAssociator::initialize() );
    ATH_MSG_VERBOSE ("Initializing " << name() << "...");
    ATH_CHECK( m_lcmodclus_key.initialize());
    ATH_CHECK( m_emmodclus_key.initialize());

    return StatusCode::SUCCESS;
  }

  StatusCode METSoftAssociator::finalize()
  {
    ATH_MSG_VERBOSE ("Finalizing " << name() << "...");
    return StatusCode::SUCCESS;
  }

  ///////////////////////////////////////////////////////////////////
  // Const methods:
  ///////////////////////////////////////////////////////////////////

  ///////////////////////////////////////////////////////////////////
  // Non-const methods:
  ///////////////////////////////////////////////////////////////////

  ///////////////////////////////////////////////////////////////////
  // Protected methods:
  ///////////////////////////////////////////////////////////////////

  // executeTool
  ////////////////
  StatusCode METSoftAssociator::executeTool(xAOD::MissingETContainer* metCont, xAOD::MissingETAssociationMap* metMap) const
  {

    // Add MET terms to the container
    // Always do this in order that the terms exist even if the method fails
    MissingET* metCoreCl = new MissingET(0.,0.,0.,"SoftClusCore",MissingETBase::Source::softEvent() | MissingETBase::Source::clusterLC());
    metCont->push_back(metCoreCl);
    MissingET* metCoreTrk = new MissingET(0.,0.,0.,"PVSoftTrkCore",MissingETBase::Source::softEvent() | MissingETBase::Source::track());
    metCont->push_back(metCoreTrk);

    ATH_MSG_VERBOSE ("In execute: " << name() << "...");
    met::METAssociator::ConstitHolder constits;
    if (retrieveConstituents(constits).isFailure()) {
      ATH_MSG_WARNING("Unable to retrieve constituent containers");
      return StatusCode::FAILURE;
    }

<<<<<<< HEAD
    if(!m_fecollKey.key().empty()){
      // PFOs have been provided as FlowElements
      const IParticleContainer* uniquePFOs = metMap->getUniqueSignals(constits.feCont,MissingETBase::UsageHandler::Policy::ParticleFlow);
=======
    if (m_pflow) {
      const IParticleContainer* uniquePFOs = metMap->getUniqueSignals(constits.pfoCont,MissingETBase::UsageHandler::Policy::ParticleFlow);
>>>>>>> release/21.0.127
      if(m_decorateSoftTermConst) {
        dec_softConst(*metCoreTrk) = std::vector<ElementLink<IParticleContainer> >();
        dec_softConst(*metCoreTrk).reserve(uniquePFOs->size());
        dec_softConst(*metCoreCl) = std::vector<ElementLink<IParticleContainer> >();
        dec_softConst(*metCoreCl).reserve(uniquePFOs->size());
      }
<<<<<<< HEAD
      for(const IParticle* sig : *uniquePFOs) {
        const xAOD::FlowElement *pfo = static_cast<const xAOD::FlowElement*>(sig);
        if (pfo->isCharged()) { // Charged PFOs
          // We set a small -ve pt for cPFOs that were rejected
          // by the ChargedHadronSubtractionTool
          const static SG::AuxElement::ConstAccessor<char> PVMatchedAcc("matchedToPV");        
          if (PVMatchedAcc(*pfo) && ( !m_cleanChargedPFO || isGoodEoverP(static_cast<const xAOD::TrackParticle*>(pfo->chargedObject(0))) ) ) {
            // For the TST, we add the track pt, as this need not be
            // corrected for nearby energy in the calo
            *metCoreTrk += pfo->chargedObject(0);
            // For CST we add the PFO pt, which is weighted down
            // to account for energy in the calo that may not have
            // been subtracted
            *metCoreCl  += sig;
            if(m_decorateSoftTermConst) {
              dec_softConst(*metCoreTrk).push_back(ElementLink<IParticleContainer>(*static_cast<const IParticleContainer*>(sig->container()),sig->index()));
              dec_softConst(*metCoreCl).push_back(ElementLink<IParticleContainer>(*static_cast<const IParticleContainer*>(sig->container()),sig->index()));
            }
          }
        } else { // Neutral PFOs
          if (pfo->e()>FLT_MIN) {
            // This is a non-issue; just add the four-vector
             *metCoreCl += sig;
             if(m_decorateSoftTermConst) dec_softConst(*metCoreCl).push_back(ElementLink<IParticleContainer>(*static_cast<const IParticleContainer*>(sig->container()),sig->index()));
          }
        }
      }
      delete uniquePFOs;
    }
    else if (m_pflow) {
      const IParticleContainer* uniquePFOs = metMap->getUniqueSignals(constits.pfoCont,MissingETBase::UsageHandler::Policy::ParticleFlow);
      if(m_decorateSoftTermConst) {
        dec_softConst(*metCoreTrk) = std::vector<ElementLink<IParticleContainer> >();
        dec_softConst(*metCoreTrk).reserve(uniquePFOs->size());
        dec_softConst(*metCoreCl) = std::vector<ElementLink<IParticleContainer> >();
        dec_softConst(*metCoreCl).reserve(uniquePFOs->size());
      }
      for(const auto sig : *uniquePFOs) {
        const PFO *pfo = static_cast<const PFO*>(sig);
        if (pfo->isCharged()) { // Charged PFOs
          // We set a small -ve pt for cPFOs that were rejected
          // by the ChargedHadronSubtractionTool
          const static SG::AuxElement::ConstAccessor<char> PVMatchedAcc("matchedToPV");        
          if (PVMatchedAcc(*pfo) && ( !m_cleanChargedPFO || isGoodEoverP(pfo->track(0)) ) ) {
            // For the TST, we add the track pt, as this need not be
            // corrected for nearby energy in the calo
            *metCoreTrk += pfo->track(0);
            // For CST we add the PFO pt, which is weighted down
            // to account for energy in the calo that may not have
            // been subtracted
            *metCoreCl  += sig;
            if(m_decorateSoftTermConst) {
              dec_softConst(*metCoreTrk).push_back(ElementLink<IParticleContainer>(*static_cast<const IParticleContainer*>(sig->container()),sig->index()));
              dec_softConst(*metCoreCl).push_back(ElementLink<IParticleContainer>(*static_cast<const IParticleContainer*>(sig->container()),sig->index()));
            }
          }
        } else { // Neutral PFOs
          if (pfo->e()>FLT_MIN) {
            // This is a non-issue; just add the four-vector
             *metCoreCl += sig;
             if(m_decorateSoftTermConst) dec_softConst(*metCoreCl).push_back(ElementLink<IParticleContainer>(*static_cast<const IParticleContainer*>(sig->container()),sig->index()));
          }
        }
=======
      for(const auto& sig : *uniquePFOs) {
	const PFO *pfo = static_cast<const PFO*>(sig);
	if (fabs(pfo->charge())>1e-9) {
	  if (acceptChargedPFO(pfo->track(0),constits.pv) &&
	      ( !m_cleanChargedPFO || isGoodEoverP(pfo->track(0)) ) 
	      ) {
	    *metCoreTrk += sig;
	    float weight = 1.0;
	    if(m_weight_charged_pfo && m_weight_soft_pfo) {
	      ATH_CHECK( m_pfoweighttool->fillWeight( *pfo, weight ) );
	    }
	    metCoreCl->add(sig,weight);
	    if(m_decorateSoftTermConst) {
	      dec_softConst(*metCoreTrk).push_back(ElementLink<IParticleContainer>(*static_cast<const IParticleContainer*>(sig->container()),sig->index()));
	      dec_softConst(*metCoreCl).push_back(ElementLink<IParticleContainer>(*static_cast<const IParticleContainer*>(sig->container()),sig->index()));
	    }
	  }
	} else {
	  TLorentzVector corrected = constits.pv ? pfo->GetVertexCorrectedEMFourVec(*constits.pv) : pfo->p4EM();
	  if (pfo->eEM()>0) {
 	    metCoreCl->add(corrected.Px(),corrected.Py(),corrected.Pt());
 	    if(m_decorateSoftTermConst) dec_softConst(*metCoreCl).push_back(ElementLink<IParticleContainer>(*static_cast<const IParticleContainer*>(sig->container()),sig->index()));
	  }
	}
>>>>>>> release/21.0.127
      }
      delete uniquePFOs;
    } else {
      MissingET* metCoreEMCl = new MissingET(0.,0.,0.,"SoftClusEMCore",MissingETBase::Source::softEvent() | MissingETBase::Source::clusterEM());
      metCont->push_back(metCoreEMCl);
      const IParticleContainer* uniqueClusters = metMap->getUniqueSignals(constits.tcCont,MissingETBase::UsageHandler::AllCalo);
      const IParticleContainer* uniqueTracks = constits.trkCont == NULL ? new const IParticleContainer() : metMap->getUniqueSignals(constits.trkCont);
      if(m_decorateSoftTermConst) {
        dec_softConst(*metCoreTrk) = std::vector<ElementLink<IParticleContainer> >();
        dec_softConst(*metCoreTrk).reserve(uniqueTracks->size());
        dec_softConst(*metCoreCl) = std::vector<ElementLink<IParticleContainer> >();
        dec_softConst(*metCoreCl).reserve(uniqueClusters->size());
      }
<<<<<<< HEAD
      SG::ReadHandle<xAOD::CaloClusterContainer> lctc(m_lcmodclus_key);
      SG::ReadHandle<xAOD::CaloClusterContainer> emtc(m_emmodclus_key);

      for(const auto cl : *uniqueClusters) {
        if (cl->e()>FLT_MIN) {
          if(m_useModifiedClus) {
            if(lctc.isValid() && emtc.isValid()) {
              size_t cl_idx(cl->index());
              // clusters at LC scale
              *metCoreCl += (*lctc)[cl_idx];
              if(m_decorateSoftTermConst) dec_softConst(*metCoreCl).push_back(ElementLink<IParticleContainer>(*static_cast<const IParticleContainer*>(lctc.cptr()),cl->index()));
              // clusters at EM scale
              *metCoreEMCl += (*emtc)[cl_idx];
            } else {
              ATH_MSG_WARNING("Invalid LC/EM modified cluster collections -- cannot add cluster to soft term!");
            }
          } else {
            // clusters at LC scale
            if (cl->type()==xAOD::Type::CaloCluster) {
        CaloVertexedClusterBase stateClLC(*(static_cast<const CaloCluster*>(cl)),xAOD::CaloCluster::CALIBRATED);
              *metCoreCl += (&stateClLC);
            } else *metCoreCl += cl;
            if(m_decorateSoftTermConst) dec_softConst(*metCoreCl).push_back(ElementLink<IParticleContainer>(*static_cast<const IParticleContainer*>(cl->container()),cl->index()));
            // clusters at EM scale
            if (cl->type()==xAOD::Type::CaloCluster) {
        CaloVertexedClusterBase stateClEM( *(static_cast<const CaloCluster*>(cl)),xAOD::CaloCluster::UNCALIBRATED);
        *metCoreEMCl += (&stateClEM);
            } else *metCoreEMCl += cl;
          }
        }
      }

      if(constits.pv) {
        for(const auto trk : *uniqueTracks) {
          ATH_MSG_VERBOSE("Test core track with pt " << trk->pt());
          if(acceptTrack(static_cast<const TrackParticle*>(trk),constits.pv) && isGoodEoverP(static_cast<const TrackParticle*>(trk))) {
            ATH_MSG_VERBOSE("Add core track with pt " << trk->pt());
            *metCoreTrk += trk;
            if(m_decorateSoftTermConst) dec_softConst(*metCoreTrk).push_back(ElementLink<IParticleContainer>(*static_cast<const IParticleContainer*>(trk->container()),trk->index()));

          }
        }
=======
      const CaloClusterContainer *lctc(0), *emtc(0);
      if(m_useModifiedClus) {
	ATH_CHECK( evtStore()->retrieve(lctc,m_lcmodclus_key) );
	ATH_CHECK( evtStore()->retrieve(emtc,m_emmodclus_key) );
      }
      for(const auto& cl : *uniqueClusters) {
	if (cl->e()>0) {
	  if(m_useModifiedClus) {
	    if(lctc && emtc) {
	      size_t cl_idx(cl->index());
	      // clusters at LC scale
	      *metCoreCl += (*lctc)[cl_idx];
	      if(m_decorateSoftTermConst) dec_softConst(*metCoreCl).push_back(ElementLink<IParticleContainer>(*static_cast<const IParticleContainer*>(lctc),cl->index()));
	      // clusters at EM scale
	      *metCoreEMCl += (*emtc)[cl_idx];
	    } else {
	      ATH_MSG_WARNING("Invalid LC/EM modified cluster collections -- cannot add cluster to soft term!");
	    }
	  } else {
	    // clusters at LC scale
	    if (cl->type()==xAOD::Type::CaloCluster) {
	      CaloClusterChangeSignalState statehelperLC(static_cast<const CaloCluster*>(cl),xAOD::CaloCluster::CALIBRATED);
	      *metCoreCl += cl;
	    } else *metCoreCl += cl;
	    if(m_decorateSoftTermConst) dec_softConst(*metCoreCl).push_back(ElementLink<IParticleContainer>(*static_cast<const IParticleContainer*>(cl->container()),cl->index()));
	    // clusters at EM scale
	    if (cl->type()==xAOD::Type::CaloCluster) {
	      CaloClusterChangeSignalState statehelperEM(static_cast<const CaloCluster*>(cl),xAOD::CaloCluster::UNCALIBRATED);
	      *metCoreEMCl += cl;
	    } else *metCoreEMCl += cl;
	  }
	}
      }

      if(constits.pv) {
	for(const auto& trk : *uniqueTracks) {
	  ATH_MSG_VERBOSE("Test core track with pt " << trk->pt());
	  if(acceptTrack(static_cast<const TrackParticle*>(trk),constits.pv) && isGoodEoverP(static_cast<const TrackParticle*>(trk))) {
	    ATH_MSG_VERBOSE("Add core track with pt " << trk->pt());
	    *metCoreTrk += trk;
	    if(m_decorateSoftTermConst) dec_softConst(*metCoreTrk).push_back(ElementLink<IParticleContainer>(*static_cast<const IParticleContainer*>(trk->container()),trk->index()));

	  }
	}
>>>>>>> release/21.0.127
      }
      delete uniqueClusters;
      delete uniqueTracks;
    }
    return StatusCode::SUCCESS;
  }
}
