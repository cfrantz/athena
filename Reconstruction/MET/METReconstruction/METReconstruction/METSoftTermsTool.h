///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

// METSoftTermsTool.h 
// Header file for class METSoftTermsTool
//
// This is a scheduler for the MET Reconstruction, and sets up
// the sequence in which the individual terms are constructed.
//
// Note that this tool is now quite outdated and should be used only
// for basic monitoring purposes.
//
//  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//
// Author: P Loch, S Resconi, TJ Khoo
/////////////////////////////////////////////////////////////////// 
#ifndef METRECONSTRUCTION_METSOFTTERMSTOOL_H
#define METRECONSTRUCTION_METSOFTTERMSTOOL_H 1

// STL includes
#include <string>

// METReconstruction includes
#include "METReconstruction/METBuilderTool.h"

//Framework includes
#include "StoreGate/DataHandle.h"

<<<<<<< HEAD
//#include "xAODCaloEvent/CaloClusterFwd.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
//#include "xAODTracking/TrackParticleFwd.h"
#include "xAODTracking/TrackParticleContainer.h"
=======
// PFlow EDM and helpers
#include "xAODPFlow/PFO.h"

namespace CP {
  class IRetrievePFOTool;
}
>>>>>>> release/21.0.127

namespace met{

  class METSoftTermsTool
    : public METBuilderTool
  { 
    // This macro defines the constructor with the interface declaration
    ASG_TOOL_CLASS(METSoftTermsTool, IMETToolBase)

  public: 

    // Constructor with name (does this have to be a non-const
    // std::string and not a const reference?)
    METSoftTermsTool(const std::string& name);
    ~METSoftTermsTool();

    // AsgTool Hooks
    StatusCode  initialize();
    StatusCode  finalize();

  protected: 
    StatusCode  executeTool(xAOD::MissingET* metTerm, xAOD::MissingETComponentMap* metMap) const;
    // Accept functions
    bool accept            (const xAOD::IParticle* object) const;
    bool accept            (const xAOD::CaloCluster* clus) const;
    bool accept            (const xAOD::TrackParticle* trk) const;
<<<<<<< HEAD
=======
    bool accept            (const xAOD::PFO* pfo, const xAOD::Vertex* pv) const;
>>>>>>> release/21.0.127
    // Overlap resolver function
    bool resolveOverlap    (const xAOD::IParticle* object,
                            xAOD::MissingETComponentMap* metMap,
                            std::vector<const xAOD::IParticle*>& acceptedSignals,
                            MissingETBase::Types::weight_t& objWeight) const;
    bool resolveOverlap    (xAOD::MissingETComponentMap* metMap,
                            std::vector<const xAOD::IParticle*>& acceptedSignals) const;

  private:
    // Default constructor: 
    METSoftTermsTool();
    // Use Case - Clusters OR Tracks OR PFOs
    std::string m_inputType;
    xAOD::Type::ObjectType m_st_objtype;
    // Cluster selection
    bool m_cl_vetoNegE;
    bool m_cl_onlyNegE;
<<<<<<< HEAD
    SG::ReadHandleKey<xAOD::CaloClusterContainer>  m_caloClusterKey{""};
    SG::ReadHandleKey<xAOD::TrackParticleContainer>  m_trackParticleKey{""};
=======
    // temporary, until a track-vertex association tool is available
    std::string m_pv_inputkey;
>>>>>>> release/21.0.127

  }; 

}

#endif //> !METRECONSTRUCTION_METSOFTTERMSTOOL_H
