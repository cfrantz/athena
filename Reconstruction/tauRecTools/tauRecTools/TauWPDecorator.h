/*
<<<<<<< HEAD
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TAURECTOOLS_TAUWPDECORATOR_H
#define TAURECTOOLS_TAUWPDECORATOR_H

#include "tauRecTools/TauRecToolBase.h"

#include "xAODTau/TauDefs.h"
#include "xAODEventInfo/EventInfo.h"
#include "AsgDataHandles/ReadDecorHandleKey.h"

#include <utility>
#include <memory>
#include <vector>
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TAUREC_TAUWPDECORATOR_H
#define TAUREC_TAUWPDECORATOR_H

#include "tauRecTools/TauRecToolBase.h"
#include "xAODTau/TauDefs.h"
#include "xAODTau/TauJet.h"
>>>>>>> release/21.0.127
#include <map>

class TH2;

/**
 * @brief Implementation of tool to decorate flattened BDT score and working points
 * 
<<<<<<< HEAD
 *  Input comes from ROOT files with lists of TH2s containing BDT/RNN score distributions
 *  as a function of the dependent variables. For eVeto, the score distributions depend on
 *  tau pT and |eta| of the leading track. Otherwise, the score distributions depend on
 *  tau pT and pileup.
=======
 *  Input comes from 2 ROOT files with lists of TH2s containing BDTScore distributions
 *  as a function of the dependent variables
>>>>>>> release/21.0.127
 *
 * @author P.O. DeViveiros
 * @author W. Davey
 * @author L. Hauswald
<<<<<<< HEAD
 */

class TauWPDecorator : public TauRecToolBase {
  public:
    
    ASG_TOOL_CLASS2(TauWPDecorator, TauRecToolBase, ITauToolBase)
    
    /** @brief Constructor */
    TauWPDecorator(const std::string& name="TauWPDecorator");
    
    /** @brief Destructor */
    ~TauWPDecorator();

    /** @brief Initialization of this tool */
    virtual StatusCode initialize() override;

    /** @brief Executation of this tool */
    virtual StatusCode execute(xAOD::TauJet& tau) const override;
    
  private:

    /** 
     * @brief Retrieve the histograms containing BDT/RNN score distributions as a function of dependent variables 
     * @param nProng Prong of the tau candidate
     */
    StatusCode retrieveHistos(int nProng);

    /**
     * @brief Obtain the limit of the dependent variables 
     * @param nProng Prong of the tau candidate
     */ 
    StatusCode storeLimits(int nProng);

    /**
     * @brief Obtain the flattened score
     * @param score Original BDT/RNN score
     * @param cutLow Lower score cut
     * @param effLow Efficiency of the lower cut
     * @param cutHigh Higher score cut
     * @param effHigh Efficiency of the higher cut
     */ 
    double transformScore(double score, double cutLow, double effLow, double cutHigh, double effHigh) const;

    bool m_electronMode; //!< Whether we are flatterning electron veto WP
    bool m_defineWPs; //!< Whether to decorate the WPs
    
    std::string m_scoreName; //!< Name of the original score
    std::string m_scoreNameTrans; //!< Name of the transformed score

    std::string m_file0p; //!< Calibration file name of 0-prong taus
    std::string m_file1p; //!< Calibration file name of 1-prong taus
    std::string m_file2p; //!< Calibration file name of 2-prong taus
    std::string m_file3p; //!< Calibration file name of 3-prong taus

    std::vector<int> m_EDMWPs; //!< Vector of WPs in the EDM
    std::vector<float> m_EDMWPEffs0p; //!< Efficiency of each WP in EDM for 0-prong taus
    std::vector<float> m_EDMWPEffs1p; //!< Efficiency of each WP in EDM for 1-prong taus
    std::vector<float> m_EDMWPEffs2p; //!< Efficiency of each WP in EDM for 2-prong taus
    std::vector<float> m_EDMWPEffs3p; //!< Efficiency of each WP in EDM for 3-prong taus

    std::vector<std::string> m_decorWPs; //!< Vector of WPs not in the EDM (to be decorated)
    std::vector<SG::AuxElement::Decorator<char>> m_charDecors; //!
    std::vector<float> m_decorWPEffs0p; //!< Efficiency of each WP to be docorated for 0-prong taus
    std::vector<float> m_decorWPEffs1p; //!< Efficiency of each WP to be docorated for 1-prong taus
    std::vector<float> m_decorWPEffs2p; //!< Efficiency of each WP to be docorated for 2-prong taus
    std::vector<float> m_decorWPEffs3p; //!< Efficiency of each WP to be docorated for 3-prong taus
    
    SG::ReadDecorHandleKey<xAOD::EventInfo> m_aveIntPerXKey {this, 
        "averageInteractionsPerCrossingKey", 
        "EventInfo.averageInteractionsPerCrossing",
        "Decoration for Average Interaction Per Crossing"};
    
    typedef std::pair<double, std::shared_ptr<TH2> > m_pair_t;

    std::shared_ptr<std::vector<m_pair_t>> m_hists0p; //!< Efficiency and corresponding score distributions of 0-prong taus
    std::shared_ptr<std::vector<m_pair_t>> m_hists1p; //!< Efficiency and corresponding score distributions of 1-prong taus
    std::shared_ptr<std::vector<m_pair_t>> m_hists2p; //!< Efficiency and corresponding score distributions of 2-prong taus
    std::shared_ptr<std::vector<m_pair_t>> m_hists3p; //!< Efficiency and corresponding score distributions of 3-prong taus
    
    std::map<int, double> m_xMin; //!< Map of n-prong and the minimum value of x variables
    std::map<int, double> m_yMin; //!< Map of n-prong and the minimum value of y variables
    std::map<int, double> m_xMax; //!< Map of n-prong and the maximum value of x variables
    std::map<int, double> m_yMax; //!< Map of n-prong and the maximum value of y variables
};

#endif // TAURECTOOLS_TAUWPDECORATOR_H
=======
 *                                                                              
 */

class TauWPDecorator : public TauRecToolBase {
public:

    TauWPDecorator(const std::string& name="TauWPDecorator") ;
    ASG_TOOL_CLASS2(TauWPDecorator, TauRecToolBase, ITauToolBase)
    ~TauWPDecorator();

    virtual StatusCode initialize();
    virtual StatusCode finalize();
    virtual StatusCode execute(xAOD::TauJet& pTau);

    virtual StatusCode retrieveHistos(int nProng);
    virtual StatusCode storeLimits(int nProng);
    virtual double transformScore(double score, double cut_lo, double eff_lo, double cut_hi, double eff_hi);

    virtual void print() const { }
    virtual StatusCode eventInitialize() { return StatusCode::SUCCESS; }
    virtual StatusCode eventFinalize() { return StatusCode::SUCCESS; }
    virtual void cleanup(xAOD::TauJet* ) { }


private:
    std::string m_file1P; //!< energy calibration file
    std::string m_file3P; //!< energy calibration file
    
    typedef std::pair<double, std::unique_ptr<TH2> > m_pair_t;
    
    std::vector<m_pair_t> m_hists1P;
    std::vector<m_pair_t> m_hists3P;

    // Limits, probably not needed
    std::map<int, double> m_xmin;
    std::map<int, double> m_ymin;
    std::map<int, double> m_xmax;
    std::map<int, double> m_ymax;
    
    // Ugly, but makes configuration easier to read
    //double m_effVeryLoose1P;
    //double m_effLoose1P;
    //double m_effMedium1P;
    //double m_effTight1P;
    
    //double m_effVeryLoose3P;
    //double m_effLoose3P;
    //double m_effMedium3P;
    //double m_effTight3P;
    
    
    bool m_defineWP;
    bool m_electronMode;

    std::vector<int> m_cut_bits;
    std::vector<float> m_cut_effs_1p;
    std::vector<float> m_cut_effs_3p;

    std::vector<std::string> m_decoration_names;
    std::vector<float> m_cut_effs_decoration_1p;
    std::vector<float> m_cut_effs_decoration_3p;

    std::string m_scoreName;
    std::string m_newScoreName;

    SG::AuxElement::ConstAccessor<float>* acc_score;
    SG::AuxElement::Accessor<float>* acc_newScore;

};

#endif
>>>>>>> release/21.0.127
