/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TAURECTOOLS_TAUPI0CLUSTERSCALER_H
#define TAURECTOOLS_TAUPI0CLUSTERSCALER_H

#include <string>
#include "tauRecTools/TauRecToolBase.h"
#include "AsgTools/ToolHandle.h"
#include "xAODPFlow/PFO.h"
#include "xAODPFlow/PFOAuxContainer.h"

/**
 * @brief scale cluster energy to take care of charged pion energy
 *
 * @author Stephanie Yuen <stephanie.yuen@cern.ch> 
 * @author Benedict Winter <benedict.tobias.winter@cern.ch> 
 * @author Will Davey <will.davey@cern.ch> 
 */

<<<<<<< HEAD:Reconstruction/tauRecTools/src/TauPi0ClusterScaler.h
=======
//namespace Trk {
//    class IParticleCaloExtensionTool;
//}
>>>>>>> release/21.0.127:Reconstruction/tauRecTools/tauRecTools/TauPi0ClusterScaler.h
class TauPi0ClusterScaler : virtual public TauRecToolBase {

public:
<<<<<<< HEAD:Reconstruction/tauRecTools/src/TauPi0ClusterScaler.h
  
  ASG_TOOL_CLASS2(TauPi0ClusterScaler, TauRecToolBase, ITauToolBase)
=======
    TauPi0ClusterScaler(const std::string& name);
    ASG_TOOL_CLASS2(TauPi0ClusterScaler, TauRecToolBase, ITauToolBase)
    virtual ~TauPi0ClusterScaler();
>>>>>>> release/21.0.127:Reconstruction/tauRecTools/tauRecTools/TauPi0ClusterScaler.h

  TauPi0ClusterScaler(const std::string& name);
  virtual ~TauPi0ClusterScaler() = default;

  virtual StatusCode executePi0ClusterScaler(xAOD::TauJet& pTau, xAOD::PFOContainer& pNeutralPFOContainer, xAOD::PFOContainer& pChargedPFOContainer) const override; 

private:
<<<<<<< HEAD:Reconstruction/tauRecTools/src/TauPi0ClusterScaler.h
  
  /** @brief Clear accosicated partcle links for the pfo container */
  void clearAssociatedParticleLinks(xAOD::PFOContainer& pfoContainer, xAOD::PFODetails::PFOParticleType type) const;

  /** @brief Get extrapolated position to the CAL */
  float getExtrapolatedPosition(const xAOD::PFO& chargedPFO, xAOD::TauJetParameters::TrackDetail detail) const; 

  /** @brief Correct neutral PFO kinematics to point at the current tau vertex */
  void correctNeutralPFOs(xAOD::TauJet& pTau, xAOD::PFOContainer& pNeutralPFOContainer) const;

  /** @brief create charged PFOs */
  void createChargedPFOs(xAOD::TauJet& pTau, xAOD::PFOContainer& pChargedPFOContainer) const;

  /** @brief associate hadronic PFOs to charged PFOs */
  void associateHadronicToChargedPFOs(xAOD::TauJet& pTau, xAOD::PFOContainer& pChargedPFOContainer) const;
  
  /** @brief associate charged PFOs to neutral PFOs */
  void associateChargedToNeutralPFOs(xAOD::TauJet& pTau, xAOD::PFOContainer& pNeutralPFOContainer) const;
  
  /** @brief associate charged PFOs to neutral PFOs */
  void subtractChargedEnergyFromNeutralPFOs(xAOD::PFOContainer& pNeutralPFOContainer) const;
=======

    /** @brief tool handles */
    //ToolHandle<Trk::IParticleCaloExtensionTool> m_caloExtensionTool;

    /** @brief reset neutral PFO kinematics (for AOD running) */
    void resetNeutralPFOs(xAOD::TauJet& pTau);
    
    /** @brief create charged PFOs */
    void createChargedPFOs(xAOD::TauJet& pTau);

    /** @brief extrapolate charged PFO tracks to EM and HAD layers */
    //void extrapolateChargedPFOs(xAOD::TauJet& pTau);
    
    /** @brief associate hadronic PFOs to charged PFOs */
    void associateHadronicToChargedPFOs(xAOD::TauJet& pTau);
    
    /** @brief associate charged PFOs to neutral PFOs */
    void associateChargedToNeutralPFOs(xAOD::TauJet& pTau);
    
    /** @brief associate charged PFOs to neutral PFOs */
    void subtractChargedEnergyFromNeutralPFOs(xAOD::TauJet& pTau);

    /** @brief new charged PFO container and name */
    xAOD::PFOContainer* m_chargedPFOContainer;
    std::string m_chargedPFOContainerName;
    xAOD::PFOAuxContainer* m_chargedPFOAuxStore;

    /** @brief run on AOD */
    bool m_AODmode;

    /** @brief sets of EM/Had samplings for track extrapolation */
    //std::set<CaloSampling::CaloSample> m_EMSamplings;
    //std::set<CaloSampling::CaloSample> m_HadSamplings;

    /** dodgy re-purposed PFOAttributes enums */
    //xAOD::PFODetails::PFOAttributes ETAECAL; 
    //xAOD::PFODetails::PFOAttributes PHIECAL;
    //xAOD::PFODetails::PFOAttributes ETAHCAL;
    //xAOD::PFODetails::PFOAttributes PHIHCAL;

>>>>>>> release/21.0.127:Reconstruction/tauRecTools/tauRecTools/TauPi0ClusterScaler.h
};

#endif  // TAURECTOOLS_TAUPI0CLUSTERSCALER_H
