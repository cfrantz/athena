/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TAURECTOOLS_MVATESVARIABLEDECORATOR_H
#define TAURECTOOLS_MVATESVARIABLEDECORATOR_H

#include "tauRecTools/TauRecToolBase.h"

#include "xAODEventInfo/EventInfo.h"
#include "AsgDataHandles/ReadHandleKey.h"
#include "AsgDataHandles/ReadDecorHandleKey.h"

<<<<<<< HEAD

class MvaTESVariableDecorator : public TauRecToolBase {
 
public:
=======
class MvaTESVariableDecorator
: public TauRecToolBase
{
 public:
>>>>>>> release/21.0.127
  
  ASG_TOOL_CLASS2( MvaTESVariableDecorator, TauRecToolBase, ITauToolBase )
    
  MvaTESVariableDecorator(const std::string& name="MvaTESVariableDecorator");
  
  virtual ~MvaTESVariableDecorator() = default;
    
  virtual StatusCode initialize() override;
  
<<<<<<< HEAD
  virtual StatusCode execute(xAOD::TauJet& xTau) const override;

private:
=======
 private:
  const xAOD::EventInfo* m_xEventInfo;  //!
  const xAOD::VertexContainer* m_xVertexContainer; //!
  int m_mu; //!
  int m_nVtxPU; //!
  bool m_emitVertexWarning=true; //!
  
};
>>>>>>> release/21.0.127

  bool m_doVertexCorrection;

  SG::ReadDecorHandleKey<xAOD::EventInfo> m_aveIntPerXKey {this, 
      "averageInteractionsPerCrossingKey", 
      "EventInfo.averageInteractionsPerCrossing",
      "Decoration for Average Interaction Per Crossing"};
  
  SG::ReadHandleKey<xAOD::VertexContainer> m_vertexContainerKey {this,
      "Key_vertexInputContainer",
      "PrimaryVertices",
      "input vertex container key"};
};

#endif // TAURECTOOLS_MVATESVARIABLEDECORATOR_H
