/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#ifndef TAURECTOOLS_TAUJETBDTEVALUATOR_H
#define TAURECTOOLS_TAUJETBDTEVALUATOR_H

// tauRecTools include(s)
#include "tauRecTools/TauRecToolBase.h"
<<<<<<< HEAD
#include "tauRecTools/BDTHelper.h"
=======
#include "tauRecTools/HelperFunctions.h"
>>>>>>> release/21.0.127

/**
 * @brief Implementation of a generic BDT for tau ID
 * @brief Can support dynamic loading of floats attached
 * @brief to the tau object, wih a few modifications to
 * @brief HelperFunctions TRTBDT, should handle ints too
 * @author Justin Griffiths
 *                                                                              
 */

class TauJetBDTEvaluator
: public TauRecToolBase
{
 public:
  ASG_TOOL_CLASS2(TauJetBDTEvaluator, TauRecToolBase, ITauToolBase)

  TauJetBDTEvaluator(const std::string& name="TauJetBDTEvaluator");
  virtual ~TauJetBDTEvaluator() { }
    
<<<<<<< HEAD
  virtual StatusCode initialize() override;
  virtual StatusCode execute(xAOD::TauJet& xTau) const override;
  
 private:
  std::unique_ptr<tauRecTools::BDTHelper> m_mvaBDT;
  std::string m_weightsFile;
  std::string m_outputVarName;
=======
  StatusCode initialize();
  StatusCode execute(xAOD::TauJet& xTau);
  StatusCode finalize();// { delete myBdt; delete m_outputVar; return StatusCode::SUCCESS;}
  
 private:

  std::string m_weightsFile;
  std::string m_outputVarName;
  SG::AuxElement::Accessor<float>* m_outputVar=0;

  tauRecTools::TRTBDT* m_myBdt=0;
>>>>>>> release/21.0.127
  int m_minNTracks;
  int m_maxNTracks;
  float m_minAbsTrackEta;
  float m_maxAbsTrackEta;
<<<<<<< HEAD
  float m_dummyValue;
};

#endif // TAURECTOOLS_TAUJETBDTEVALUATOR_H
=======
  float m_dummyValue;// in case no configs are set, set a dummy value.

  bool m_isGrad;
};
#endif
>>>>>>> release/21.0.127
