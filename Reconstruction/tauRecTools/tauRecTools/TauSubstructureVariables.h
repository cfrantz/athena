/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TAURECTOOLS_TAUSUBSTRUCTUREVARIABLES_H
#define TAURECTOOLS_TAUSUBSTRUCTUREVARIABLES_H

#include "tauRecTools/TauRecToolBase.h"

#include <string>

/**
 * @brief Calculate variables from the tau substructure.
 * 
 * @author M. Trottier-McDonald
 * @author Felix Friedrich
 * 
 */

<<<<<<< HEAD
class TauSubstructureVariables : public TauRecToolBase {

public: 

  ASG_TOOL_CLASS2(TauSubstructureVariables, TauRecToolBase, ITauToolBase)
  
  TauSubstructureVariables(const std::string& name="TauSubstructureVariables");

  virtual ~TauSubstructureVariables() = default;

  virtual StatusCode execute(xAOD::TauJet& tau) const override;

  static const float DEFAULT;

private:

  bool m_doVertexCorrection;
=======
class TauSubstructureVariables : public TauRecToolBase
{
    public: 
        
        static const double DEFAULT;

        TauSubstructureVariables(const std::string& name="TauSubstructureVariables");
	ASG_TOOL_CLASS2(TauSubstructureVariables, TauRecToolBase, ITauToolBase)
	
        ~TauSubstructureVariables();

        virtual StatusCode execute(xAOD::TauJet& pTau);

        virtual StatusCode initialize();
        virtual StatusCode finalize();
        virtual StatusCode eventInitialize();

	virtual StatusCode eventFinalize() { return StatusCode::SUCCESS; }

	virtual void print() const { }

    private:
        /** Maximal pile up correction in GeV for a tau candidate.
         *  Used for the caloIso corrected variable.
         */
	std::string m_configPath;
        double m_maxPileUpCorrection; 
        double m_pileUpAlpha;         //!< slope of the pileup correction
        
        /** 
         * enable cell origin correction 
         * eta and phi of the cells are corrected wrt to the origin of the tau vertex
         */
        bool m_doVertexCorrection;
        bool m_inAODmode; //!< don't update everything if running on AODs
>>>>>>> release/21.0.127
};

#endif // TAURECTOOLS_TAUSUBSTRUCTUREVARIABLES_H
