/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

// local include(s)
#include "tauRecTools/MvaTESEvaluator.h"
#include "tauRecTools/HelperFunctions.h"
<<<<<<< HEAD
=======

#include "TFile.h"
#include "TTree.h"

#include <vector>
>>>>>>> release/21.0.127

#include <TTree.h>

<<<<<<< HEAD
#include <vector>

//_____________________________________________________________________________
MvaTESEvaluator::MvaTESEvaluator(const std::string& name)
  : TauRecToolBase(name) {
  declareProperty("WeightFileName", m_sWeightFileName = "");
=======
//#include "TauAnalysisTools/HelperFunctions.h"

//_____________________________________________________________________________
MvaTESEvaluator::MvaTESEvaluator(const std::string& name)
  : TauRecToolBase(name)
  , reader(0)
  , mu(0)
  , nVtxPU(0)    
  , center_lambda(0)
  , first_eng_dens(0)
  , second_lambda(0)
  , presampler_frac(0)
  , em_probability(0)    
  , ptCombined(0)
  , ptLC_D_ptCombined(0)
  , ptConstituent_D_ptCombined(0)
  , etaConstituent(0)    
  , PanTauBDT_1p0n_vs_1p1n(0)
  , PanTauBDT_1p1n_vs_1pXn(0)
  , PanTauBDT_3p0n_vs_3pXn(0)
  , nTracks(0)
  , PFOEngRelDiff(0)    
  , truthPtVis(0)
  , pt(0)
  , ptPanTauCellBased(0)
  , ptDetectorAxis(0)
  , truthDecayMode(0)
  , PanTau_DecayMode(0)
{
  declareProperty( "WeightFileName", m_sWeightFileName = "MvaTES_20170207_v2_BDTG.weights.root" );
  //R20.7 pi0-fix version: "MvaTES_20161015_pi0fix_BDTG.weights.root"
>>>>>>> release/21.0.127
}

//_____________________________________________________________________________
MvaTESEvaluator::~MvaTESEvaluator() {
}

//_____________________________________________________________________________
StatusCode MvaTESEvaluator::initialize(){
<<<<<<< HEAD
  
  const std::string weightFile = find_file(m_sWeightFileName);
  m_bdtHelper = std::make_unique<tauRecTools::BDTHelper>();
  ATH_CHECK(m_bdtHelper->initialize(weightFile));
=======

  // Declare input variables to the reader
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.mu", &mu) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.nVtxPU", &nVtxPU) );
  
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.ClustersMeanCenterLambda", &center_lambda) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.ClustersMeanFirstEngDens", &first_eng_dens) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.ClustersMeanSecondLambda", &second_lambda) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.ClustersMeanPresamplerFrac", &presampler_frac) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.ClustersMeanEMProbability", &em_probability) );
  
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.pt_combined", &ptCombined) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.ptDetectorAxis/TauJetsAuxDyn.pt_combined", &ptLC_D_ptCombined) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.ptPanTauCellBased/TauJetsAuxDyn.pt_combined", &ptConstituent_D_ptCombined) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.etaPanTauCellBased", &etaConstituent) );
  
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.PanTau_BDTValue_1p0n_vs_1p1n", &PanTauBDT_1p0n_vs_1p1n) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.PanTau_BDTValue_1p1n_vs_1pXn", &PanTauBDT_1p1n_vs_1pXn) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.PanTau_BDTValue_3p0n_vs_3pXn", &PanTauBDT_3p0n_vs_3pXn) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.nTracks", &nTracks) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.PFOEngRelDiff", &PFOEngRelDiff) );

  // Spectator variables declared in the training have to be added to the reader, too
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.truthPtVis", &truthPtVis) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.pt", &pt) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.ptPanTauCellBased", &ptPanTauCellBased) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.ptDetectorAxis", &ptDetectorAxis) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.truthDecayMode", &truthDecayMode) );
  m_availableVars.insert( std::make_pair("TauJetsAuxDyn.PanTau_DecayMode", &PanTau_DecayMode) );
  
  std::string weightFile = find_file(m_sWeightFileName);

  reader = tauRecTools::configureMVABDT( m_availableVars, weightFile.c_str() );
  if(reader==0) {
    ATH_MSG_FATAL("Couldn't configure MVA");
    return StatusCode::FAILURE;
  }

>>>>>>> release/21.0.127
  return StatusCode::SUCCESS;
}

//_____________________________________________________________________________
<<<<<<< HEAD
StatusCode MvaTESEvaluator::execute(xAOD::TauJet& xTau) const {

  std::map<TString, float*> availableVars;
  MvaInputVariables vars;

  // Declare input variables to the reader
  if(!inTrigger()) {
    availableVars.insert( std::make_pair("TauJetsAuxDyn.mu", &vars.mu) );
    availableVars.insert( std::make_pair("TauJetsAuxDyn.nVtxPU", &vars.nVtxPU) );
    
    availableVars.insert( std::make_pair("TauJetsAuxDyn.ClustersMeanCenterLambda", &vars.center_lambda) );
    availableVars.insert( std::make_pair("TauJetsAuxDyn.ClustersMeanFirstEngDens", &vars.first_eng_dens) );
    availableVars.insert( std::make_pair("TauJetsAuxDyn.ClustersMeanSecondLambda", &vars.second_lambda) );
    availableVars.insert( std::make_pair("TauJetsAuxDyn.ClustersMeanPresamplerFrac", &vars.presampler_frac) );
    availableVars.insert( std::make_pair("TauJetsAuxDyn.ClustersMeanEMProbability", &vars.eprobability) );
    
    availableVars.insert( std::make_pair("TauJetsAuxDyn.pt_combined", &vars.ptCombined) );
    availableVars.insert( std::make_pair("TauJetsAuxDyn.ptDetectorAxis/TauJetsAuxDyn.pt_combined", &vars.ptLC_D_ptCombined) );
    availableVars.insert( std::make_pair("TauJetsAuxDyn.ptPanTauCellBased/TauJetsAuxDyn.pt_combined", &vars.ptConstituent_D_ptCombined) );
    availableVars.insert( std::make_pair("TauJetsAuxDyn.etaPanTauCellBased", &vars.etaConstituent) );
    
    availableVars.insert( std::make_pair("TauJetsAuxDyn.PanTau_BDTValue_1p0n_vs_1p1n", &vars.PanTauBDT_1p0n_vs_1p1n) );
    availableVars.insert( std::make_pair("TauJetsAuxDyn.PanTau_BDTValue_1p1n_vs_1pXn", &vars.PanTauBDT_1p1n_vs_1pXn) );
    availableVars.insert( std::make_pair("TauJetsAuxDyn.PanTau_BDTValue_3p0n_vs_3pXn", &vars.PanTauBDT_3p0n_vs_3pXn) );
    availableVars.insert( std::make_pair("TauJetsAuxDyn.nTracks", &vars.nTracks) );
    availableVars.insert( std::make_pair("TauJetsAuxDyn.PFOEngRelDiff", &vars.PFOEngRelDiff) );
  }
  else {
    availableVars.insert( std::make_pair("TrigTauJetsAuxDyn.mu", &vars.mu) );
    availableVars.insert( std::make_pair("TrigTauJetsAuxDyn.ClustersMeanCenterLambda", &vars.center_lambda) );
    availableVars.insert( std::make_pair("TrigTauJetsAuxDyn.ClustersMeanFirstEngDens", &vars.first_eng_dens) );
    availableVars.insert( std::make_pair("TrigTauJetsAuxDyn.ClustersMeanSecondLambda", &vars.second_lambda) );
    availableVars.insert( std::make_pair("TrigTauJetsAuxDyn.ClustersMeanPresamplerFrac", &vars.presampler_frac) );
    availableVars.insert( std::make_pair("TrigTauJetsAuxDyn.ClustersMeanEMProbability", &vars.eprobability) );
    availableVars.insert( std::make_pair("TrigTauJetsAuxDyn.LeadClusterFrac", &vars.lead_cluster_frac) );
    availableVars.insert( std::make_pair("TrigTauJetsAuxDyn.UpsilonCluster", &vars.upsilon_cluster) );
    availableVars.insert( std::make_pair("TrigTauJetsAuxDyn.ptDetectorAxis", &vars.ptDetectorAxis) );
    availableVars.insert( std::make_pair("TrigTauJetsAuxDyn.etaDetectorAxis", &vars.etaDetectorAxis) );
  }
=======
StatusCode MvaTESEvaluator::eventInitialize()
{
  // HACK HACK HACK: Get nVtxPU, AuxElement::ConstAccessor doesn't work
  nVtxPU = 0;
  if(evtStore()->contains<xAOD::VertexContainer>("PrimaryVertices")){
    ATH_CHECK(evtStore()->retrieve(m_xVertexContainer, "PrimaryVertices"));  
    for (auto xVertex : *m_xVertexContainer)
    if (xVertex->vertexType() == xAOD::VxType::PileUp)
      nVtxPU++;
  }
  else {
    ATH_MSG_WARNING("No xAOD::VertexContainer, setting nVtxPU to 0");
    nVtxPU=0;
  }

  return StatusCode::SUCCESS;
}

//_____________________________________________________________________________
StatusCode MvaTESEvaluator::execute(xAOD::TauJet& xTau){
>>>>>>> release/21.0.127

  // Retrieve event info
<<<<<<< HEAD
  static const SG::AuxElement::ConstAccessor<float> acc_mu("mu");
  static const SG::AuxElement::ConstAccessor<int> acc_nVtxPU("nVtxPU");
  vars.mu = acc_mu(xTau);
  vars.nVtxPU = acc_nVtxPU(xTau);

  // Retrieve cluster moments
  xTau.detail(xAOD::TauJetParameters::ClustersMeanCenterLambda, vars.center_lambda);
  xTau.detail(xAOD::TauJetParameters::ClustersMeanFirstEngDens, vars.first_eng_dens);
  xTau.detail(xAOD::TauJetParameters::ClustersMeanEMProbability,vars.eprobability);
  xTau.detail(xAOD::TauJetParameters::ClustersMeanSecondLambda, vars.second_lambda);
  xTau.detail(xAOD::TauJetParameters::ClustersMeanPresamplerFrac, vars.presampler_frac);

  if(!inTrigger()) {

    // Retrieve pantau and LC-precalib TES
    vars.etaConstituent = xTau.etaPanTauCellBased();
    float ptLC = xTau.ptDetectorAxis();
    float ptConstituent = xTau.ptPanTauCellBased();
    static const SG::AuxElement::ConstAccessor<float> acc_ptCombined("ptCombined");
    vars.ptCombined = acc_ptCombined(xTau);

    if(vars.ptCombined>0.) {
      vars.ptLC_D_ptCombined = ptLC / vars.ptCombined;
      vars.ptConstituent_D_ptCombined = ptConstituent / vars.ptCombined;
    }
    else {
      xTau.setP4(xAOD::TauJetParameters::FinalCalib, 1., vars.etaConstituent, xTau.phiPanTauCellBased(), 0.);
      // apply MVA calibration as default
      xTau.setP4(1., vars.etaConstituent, xTau.phiPanTauCellBased(), 0.);
      return StatusCode::SUCCESS;
    }

    // Retrieve substructure info
    static const SG::AuxElement::ConstAccessor<float> acc_PanTauBDT_1p0n_vs_1p1n("PanTau_BDTValue_1p0n_vs_1p1n");
    static const SG::AuxElement::ConstAccessor<float> acc_PanTauBDT_1p1n_vs_1pXn("PanTau_BDTValue_1p1n_vs_1pXn");
    static const SG::AuxElement::ConstAccessor<float> acc_PanTauBDT_3p0n_vs_3pXn("PanTau_BDTValue_3p0n_vs_3pXn");
    vars.PanTauBDT_1p0n_vs_1p1n = acc_PanTauBDT_1p0n_vs_1p1n(xTau);
    vars.PanTauBDT_1p1n_vs_1pXn = acc_PanTauBDT_1p1n_vs_1pXn(xTau);
    vars.PanTauBDT_3p0n_vs_3pXn = acc_PanTauBDT_3p0n_vs_3pXn(xTau);
    vars.nTracks = (float)xTau.nTracks();
    xTau.detail(xAOD::TauJetParameters::PFOEngRelDiff, vars.PFOEngRelDiff);
    
    float ptMVA = float( vars.ptCombined * m_bdtHelper->getResponse(availableVars) );
    if(ptMVA<1.) ptMVA=1.;
    xTau.setP4(xAOD::TauJetParameters::FinalCalib, ptMVA, vars.etaConstituent, xTau.phiPanTauCellBased(), 0.);

    // apply MVA calibration as default
    xTau.setP4(ptMVA, vars.etaConstituent, xTau.phiPanTauCellBased(), 0.);
  }
  else {

    vars.ptDetectorAxis = xTau.ptDetectorAxis();
    vars.etaDetectorAxis = xTau.etaDetectorAxis();

    static const SG::AuxElement::ConstAccessor<float> acc_UpsilonCluster("UpsilonCluster");
    static const SG::AuxElement::ConstAccessor<float> acc_LeadClusterFrac("LeadClusterFrac");
    vars.upsilon_cluster = acc_UpsilonCluster(xTau);
    vars.lead_cluster_frac = acc_LeadClusterFrac(xTau);

    float ptMVA = float( vars.ptDetectorAxis * m_bdtHelper->getResponse(availableVars) );
    if(ptMVA<1.) ptMVA=1.;

    // this may have to be changed if we apply a calo-only MVA calibration first, followed by a calo+track MVA calibration
    // in which case, the calo-only would be TauJetParameters::TrigCaloOnly, and the final one TauJetParameters::FinalCalib
    xTau.setP4(xAOD::TauJetParameters::FinalCalib, ptMVA, vars.etaDetectorAxis, xTau.phiDetectorAxis(), 0.);
    
    // apply MVA calibration
    xTau.setP4(ptMVA, vars.etaDetectorAxis, xTau.phiDetectorAxis(), 0.);
  }
  
  ATH_MSG_DEBUG("final calib:" << xTau.pt() << " " << xTau.eta() << " " << xTau.phi() << " " << xTau.e());
=======
  static SG::AuxElement::ConstAccessor<double> acc_mu("mu");
  //static SG::AuxElement::ConstAccessor<int> acc_nVtxPU("nVtxPU");
  mu = acc_mu(xTau);
  //nVtxPU = acc_nVtxPU(xTau);

  // Retrieve seed jet info
  xTau.detail(xAOD::TauJetParameters::ClustersMeanCenterLambda, center_lambda);
  xTau.detail(xAOD::TauJetParameters::ClustersMeanFirstEngDens, first_eng_dens);
  xTau.detail(xAOD::TauJetParameters::ClustersMeanEMProbability,em_probability);
  xTau.detail(xAOD::TauJetParameters::ClustersMeanSecondLambda, second_lambda);
  xTau.detail(xAOD::TauJetParameters::ClustersMeanPresamplerFrac, presampler_frac);

  // Retrieve pantau and LC-precalib TES
  etaConstituent = xTau.etaPanTauCellBased();
  float ptLC = xTau.ptDetectorAxis();
  float ptConstituent = xTau.ptPanTauCellBased();
  static SG::AuxElement::ConstAccessor<float> acc_pt_combined("pt_combined");
  ptCombined = acc_pt_combined(xTau);
  ptLC_D_ptCombined = ptLC / ptCombined;
  ptConstituent_D_ptCombined = ptConstituent / ptCombined;
  
  // Retrieve substructures info
  static SG::AuxElement::ConstAccessor<float> acc_PanTauBDT_1p0n_vs_1p1n("PanTau_BDTValue_1p0n_vs_1p1n");
  static SG::AuxElement::ConstAccessor<float> acc_PanTauBDT_1p1n_vs_1pXn("PanTau_BDTValue_1p1n_vs_1pXn");
  static SG::AuxElement::ConstAccessor<float> acc_PanTauBDT_3p0n_vs_3pXn("PanTau_BDTValue_3p0n_vs_3pXn");
  PanTauBDT_1p0n_vs_1p1n = acc_PanTauBDT_1p0n_vs_1p1n(xTau);
  PanTauBDT_1p1n_vs_1pXn = acc_PanTauBDT_1p1n_vs_1pXn(xTau);
  PanTauBDT_3p0n_vs_3pXn = acc_PanTauBDT_3p0n_vs_3pXn(xTau);
  nTracks = (float)xTau.nTracks();
  xTau.detail(xAOD::TauJetParameters::PFOEngRelDiff, PFOEngRelDiff);

  float ptMVA = float( ptCombined * reader->GetResponse() );
  if(ptMVA<1) ptMVA=1;
  xTau.setP4(xAOD::TauJetParameters::FinalCalib, ptMVA, etaConstituent, xTau.phiPanTauCellBased(), 0);
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}
