/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// ASG include(s)
#include "PathResolver/PathResolver.h"

>>>>>>> release/21.0.127
// xAOD include(s)
#include "xAODTracking/TrackParticle.h"
#include "xAODTau/TauTrackContainer.h"
#include "xAODTau/TauxAODHelpers.h"

// local include(s)
#include "tauRecTools/TauTrackClassifier.h"
#include "tauRecTools/HelperFunctions.h"

<<<<<<< HEAD
#include <fstream>

=======
// #include "TMVA/MethodBDT.h"
// #include "TMVA/Reader.h"

#include <fstream>

//root includes
#include <TFile.h>
#include <TTree.h>

>>>>>>> release/21.0.127
using namespace tauRecTools;

//==============================================================================
// class TauTrackClassifier
//==============================================================================

//______________________________________________________________________________
TauTrackClassifier::TauTrackClassifier(const std::string& sName)
  : TauRecToolBase(sName)
{
  declareProperty("Classifiers", m_vClassifier );
  declareProperty("ClassifierNames", m_vClassifierNames );
<<<<<<< HEAD
=======
  declareProperty("TauTrackContainerName", m_tauTrackConName="TauTracks");
>>>>>>> release/21.0.127
}

//______________________________________________________________________________
TauTrackClassifier::~TauTrackClassifier()
{
}

//______________________________________________________________________________
StatusCode TauTrackClassifier::initialize()
{
  ATH_MSG_DEBUG("intialize classifiers");

<<<<<<< HEAD
=======
  #ifdef ROOTCORE
  for (auto cClassifierName : m_vClassifierNames){
    tauRecTools::TrackMVABDT* mva_tool = dynamic_cast<tauRecTools::TrackMVABDT*> (asg::ToolStore::get(cClassifierName));
    ToolHandle< tauRecTools::TrackMVABDT > handle(mva_tool);    
    m_vClassifier.push_back(handle);    
    ATH_CHECK(m_vClassifier.back()->initialize());//retrieve() does not seem to work
  }
  #else
>>>>>>> release/21.0.127
  for (auto cClassifier : m_vClassifier){
    ATH_MSG_INFO("TauTrackClassifier tool : " << cClassifier );
    ATH_CHECK(cClassifier.retrieve());
  }
<<<<<<< HEAD
=======
  #endif

>>>>>>> release/21.0.127
 
  return StatusCode::SUCCESS;
}

//______________________________________________________________________________
<<<<<<< HEAD
StatusCode TauTrackClassifier::executeTrackClassifier(xAOD::TauJet& xTau, xAOD::TauTrackContainer& tauTrackCon) const
{
  std::vector<xAOD::TauTrack*> vTracks = xAOD::TauHelpers::allTauTracksNonConst(&xTau, &tauTrackCon);
=======
StatusCode TauTrackClassifier::execute(xAOD::TauJet& xTau)
{
  xAOD::TauTrackContainer* tauTrackCon = 0;
  ATH_CHECK(evtStore()->retrieve(tauTrackCon, m_tauTrackConName));
  std::vector<xAOD::TauTrack*> vTracks = xAOD::TauHelpers::allTauTracksNonConst(&xTau, tauTrackCon);
>>>>>>> release/21.0.127
  for (xAOD::TauTrack* xTrack : vTracks)
  {
    // reset all track flags and set status to unclassified
    xTrack->setFlag(xAOD::TauJetParameters::classifiedCharged, false);
    xTrack->setFlag(xAOD::TauJetParameters::classifiedConversion, false);
    xTrack->setFlag(xAOD::TauJetParameters::classifiedIsolation, false);
    xTrack->setFlag(xAOD::TauJetParameters::classifiedFake, false);
    xTrack->setFlag(xAOD::TauJetParameters::unclassified, true);

    // execute the bdt track classifier 
    for (auto cClassifier : m_vClassifier)
      ATH_CHECK(cClassifier->classifyTrack(*xTrack, xTau));
  }
  std::vector< ElementLink< xAOD::TauTrackContainer > > &tauTrackLinks(xTau.allTauTrackLinksNonConst());
  std::sort(tauTrackLinks.begin(), tauTrackLinks.end(), sortTracks);
  float charge=0.0;
  for( const xAOD::TauTrack* trk : xTau.tracks(xAOD::TauJetParameters::classifiedCharged) ){
    charge += trk->track()->charge();
  }
<<<<<<< HEAD

=======
>>>>>>> release/21.0.127
  xTau.setCharge(charge);
  xTau.setDetail(xAOD::TauJetParameters::nChargedTracks, (int) xTau.nTracks());
  xTau.setDetail(xAOD::TauJetParameters::nIsolatedTracks, (int) xTau.nTracks(xAOD::TauJetParameters::classifiedIsolation));

  //set modifiedIsolationTrack
  for (xAOD::TauTrack* xTrack : vTracks) {
    if( not xTrack->flag(xAOD::TauJetParameters::classifiedCharged) and 
	xTrack->flag(xAOD::TauJetParameters::passTrkSelector) ) xTrack->setFlag(xAOD::TauJetParameters::modifiedIsolationTrack, true);
    else xTrack->setFlag(xAOD::TauJetParameters::modifiedIsolationTrack, false);
  }
  xTau.setDetail(xAOD::TauJetParameters::nModifiedIsolationTracks, (int) xTau.nTracks(xAOD::TauJetParameters::modifiedIsolationTrack));

<<<<<<< HEAD
=======

>>>>>>> release/21.0.127
  return StatusCode::SUCCESS;
}

//==============================================================================
// class TrackMVABDT
//==============================================================================

//______________________________________________________________________________
TrackMVABDT::TrackMVABDT(const std::string& sName)
  : TauRecToolBase(sName)
  , m_sInputWeightsPath("")
  , m_fThreshold(0.)
  , m_iSignalType(xAOD::TauJetParameters::classifiedCharged)
  , m_iBackgroundType(xAOD::TauJetParameters::classifiedFake)
  , m_iExpectedFlag(xAOD::TauJetParameters::unclassified)
<<<<<<< HEAD
  , m_rReader(nullptr)
  , m_inputVariableNames()
=======
  , m_rReader(0)
    //  , m_mParsedVarsBDT({})
  , m_mAvailableVars({})
>>>>>>> release/21.0.127
{
  declareProperty( "InputWeightsPath", m_sInputWeightsPath );
  declareProperty( "Threshold", m_fThreshold );
  declareProperty( "BackgroundType" , m_iBackgroundType );
  declareProperty( "SignalType", m_iSignalType );
  declareProperty( "ExpectedFlag", m_iExpectedFlag );
<<<<<<< HEAD
=======
  //  m_mParsedVarsBDT.clear();
>>>>>>> release/21.0.127
}

//______________________________________________________________________________
TrackMVABDT::~TrackMVABDT()
{
}

//______________________________________________________________________________
<<<<<<< HEAD
StatusCode TrackMVABDT::initialize()
{
=======
StatusCode TrackMVABDT::finalize()
{
  delete m_rReader;
  for( std::pair<TString, float*> p : m_mAvailableVars ) delete p.second;
  m_mAvailableVars.clear();
  return StatusCode::SUCCESS;
}

//______________________________________________________________________________
StatusCode TrackMVABDT::initialize()
{
  m_mAvailableVars={{"TracksAuxDyn.tauPt", new float(0)}
                    , {"TracksAuxDyn.jetSeedPt", new float(0)}
                    , {"TracksAuxDyn.tauEta", new float(0)}
                    , {"TracksAuxDyn.trackEta", new float(0)}
                    , {"TracksAuxDyn.z0sinThetaTJVA", new float(0)}
                    , {"TracksAuxDyn.rConv", new float(0)}
                    , {"TracksAuxDyn.rConvII", new float(0)}
                    , {"TauTracksAuxDyn.rConv/TauTracksAuxDyn.rConvII", new float(0)}
                    , {"TracksAuxDyn.DRJetSeedAxis", new float(0)}
                    , {"TracksAuxDyn.dRJetSeedAxis", new float(0)}
                    , {"TracksAux.d0", new float(0)}
                    , {"TracksAux.qOverP", new float(0)}
                    , {"TracksAux.theta", new float(0)}
                    , {"TracksAux.eProbabilityHT", new float(0)}
                    , {"TracksAux.numberOfInnermostPixelLayerHits", new float(0)}
                    , {"TracksAux.numberOfPixelHits", new float(0)}
                    , {"TracksAux.numberOfPixelDeadSensors", new float(0)}
                    , {"TracksAux.numberOfPixelSharedHits", new float(0)}
                    , {"TracksAux.numberOfSCTHits", new float(0)}
                    , {"TracksAux.numberOfSCTDeadSensors", new float(0)}
                    , {"TracksAux.numberOfSCTSharedHits", new float(0)}
                    , {"TracksAux.numberOfTRTHighThresholdHits", new float(0)}
                    , {"TracksAux.numberOfTRTHits", new float(0)}
                    , {"TracksAux.numberOfPixelHits+TracksAux.numberOfPixelDeadSensors", new float(0)}
                    , {"TracksAux.numberOfPixelHits+TracksAux.numberOfPixelDeadSensors+TracksAux.numberOfSCTHits+TracksAux.numberOfSCTDeadSensors", new float(0)}

                    , {"TauTracksAuxDyn.tauPt", new float(0)}
                    , {"TauTracksAuxDyn.jetSeedPt", new float(0)}
                    , {"TauTracksAuxDyn.tauEta", new float(0)}
                    , {"TauTracksAuxDyn.trackEta", new float(0)}
                    , {"TauTracksAuxDyn.z0sinThetaTJVA", new float(0)}
                    , {"TauTracksAuxDyn.rConv", new float(0)}
                    , {"TauTracksAuxDyn.rConvII", new float(0)}
                    , {"TauTracksAuxDyn.dRJetSeedAxis", new float(0)}
                    , {"TauTracksAuxDyn.d0", new float(0)}
                    , {"TauTracksAuxDyn.qOverP", new float(0)}
                    , {"TauTracksAuxDyn.theta", new float(0)}
                    , {"TauTracksAuxDyn.eProbabilityHT", new float(0)}
                    , {"TauTracksAuxDyn.numberOfInnermostPixelLayerHits", new float(0)}
                    , {"TauTracksAuxDyn.numberOfPixelHits", new float(0)}
                    , {"TauTracksAuxDyn.numberOfPixelDeadSensors", new float(0)}
                    , {"TauTracksAuxDyn.numberOfPixelSharedHits", new float(0)}
                    , {"TauTracksAuxDyn.numberOfSCTHits", new float(0)}
                    , {"TauTracksAuxDyn.numberOfSCTDeadSensors", new float(0)}
                    , {"TauTracksAuxDyn.numberOfSCTSharedHits", new float(0)}
                    , {"TauTracksAuxDyn.numberOfTRTHighThresholdHits", new float(0)}
                    , {"TauTracksAuxDyn.numberOfTRTHits", new float(0)}
                    , {"TauTracksAuxDyn.numberOfPixelHits+TauTracksAuxDyn.numberOfPixelDeadSensors", new float(0)}
                    , {"TauTracksAuxDyn.numberOfPixelHits+TauTracksAuxDyn.numberOfPixelDeadSensors+TauTracksAuxDyn.numberOfSCTHits+TauTracksAuxDyn.numberOfSCTDeadSensors", new float(0)}


                    , {"1/(TauTracksAuxDyn.trackPt)", new float(0)}
                    , {"fabs(TauTracksAuxDyn.qOverP)", new float(0)}
                    , {"TauTracksAuxDyn.numberOfContribPixelLayers", new float(0)}
                    , {"TauTracksAuxDyn.numberOfPixelHits+TauTracksAuxDyn.numberOfPixelDeadSensors+TauTracksAuxDyn.numberOfPixelHoles", new float(0)}
                    , {"TauTracksAuxDyn.numberOfPixelHits+TauTracksAuxDyn.numberOfPixelDeadSensors+TauTracksAuxDyn.numberOfPixelHoles+TauTracksAuxDyn.numberOfSCTHits+TauTracksAuxDyn.numberOfSCTDeadSensors+TauTracksAuxDyn.numberOfSCTHoles", new float(0)}
                    , {"TauTracksAuxDyn.numberOfPixelHoles", new float(0)}
                    , {"TauTracksAuxDyn.numberOfPixelHoles+TauTracksAuxDyn.numberOfSCTHoles", new float(0)}
                    , {"TauTracksAuxDyn.numberOfSCTHoles", new float(0)}
                    , {"TauTracksAux.pt", new float(0)}
  };
    
>>>>>>> release/21.0.127
  ATH_CHECK(addWeightsFile());
  
  return StatusCode::SUCCESS;
}

//______________________________________________________________________________
<<<<<<< HEAD
StatusCode TrackMVABDT::classifyTrack(xAOD::TauTrack& xTrack, const xAOD::TauJet& xTau) const
{
  /// If TT/IT gives TT, only run TT/CR; otherwise, run IT/FT 
  if (xTrack.flag((xAOD::TauJetParameters::TauTrackFlag) m_iExpectedFlag)==false)
    return StatusCode::SUCCESS;
  
  std::vector<float> values;
  ATH_CHECK(calculateVariables(xTrack, xTau, values));
  double dValue = m_rReader->GetClassification(values);
=======
StatusCode TrackMVABDT::classifyTrack(xAOD::TauTrack& xTrack, const xAOD::TauJet& xTau)
{
  ATH_CHECK( setVars(xTrack, xTau) );

  //why?
  if (xTrack.flag((xAOD::TauJetParameters::TauTrackFlag) m_iExpectedFlag)==false)
    return StatusCode::SUCCESS;
  
  double dValue = m_rReader->GetClassification();
>>>>>>> release/21.0.127
  
  xTrack.setFlag((xAOD::TauJetParameters::TauTrackFlag) m_iExpectedFlag, false);
  if (m_fThreshold < dValue)
    xTrack.setFlag((xAOD::TauJetParameters::TauTrackFlag) m_iSignalType, true);
  else 
    xTrack.setFlag((xAOD::TauJetParameters::TauTrackFlag) m_iBackgroundType, true);

  xTrack.addBdtScore(dValue);

  return StatusCode::SUCCESS;
}

//______________________________________________________________________________
StatusCode TrackMVABDT::addWeightsFile()
{
  m_sInputWeightsPath = find_file(m_sInputWeightsPath);
  ATH_MSG_DEBUG("InputWeightsPath: " << m_sInputWeightsPath);
  
<<<<<<< HEAD
  m_rReader = tauRecTools::configureMVABDT( m_inputVariableNames, m_sInputWeightsPath.c_str() );
  if(m_rReader==nullptr) {
=======
  // if (m_mParsedVarsBDT.empty())
  // {
  //   ATH_CHECK(parseVariableContent());
      
  //   for (auto i : m_mParsedVarsBDT)
  //     ATH_MSG_DEBUG(i.first<<" "<<i.second);
    
  //   for (size_t i = 0; i < m_mParsedVarsBDT.size(); i++){
  //     std::string sVarName = m_mParsedVarsBDT[i];
  //     if (m_mAvailableVars.find(sVarName) == m_mAvailableVars.end())
  //     {
  //       ATH_MSG_ERROR("Variable "<<sVarName<<" not in map of available variables");
  //       return StatusCode::FAILURE;
  //     }
  //     reader->AddVariable( sVarName, &(m_mAvailableVars[sVarName]));
  //   }
  // }

  m_rReader = tauRecTools::configureMVABDT( m_mAvailableVars, m_sInputWeightsPath.c_str() );
  if(m_rReader==0) {
>>>>>>> release/21.0.127
    ATH_MSG_FATAL("Couldn't configure MVA");
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

//______________________________________________________________________________
<<<<<<< HEAD
StatusCode TrackMVABDT::calculateVariables(const xAOD::TauTrack& xTrack, const xAOD::TauJet& xTau, std::vector<float>& values) const
{
  const xAOD::TrackParticle* xTrackParticle = xTrack.track();
  uint8_t iTracksNumberOfInnermostPixelLayerHits = 0; ATH_CHECK( xTrackParticle->summaryValue(iTracksNumberOfInnermostPixelLayerHits, xAOD::numberOfInnermostPixelLayerHits) );
  uint8_t iTracksNPixelHits = 0; ATH_CHECK( xTrackParticle->summaryValue(iTracksNPixelHits, xAOD::numberOfPixelHits) );
  uint8_t iTracksNPixelSharedHits = 0; ATH_CHECK( xTrackParticle->summaryValue(iTracksNPixelSharedHits, xAOD::numberOfPixelSharedHits) );
  uint8_t iTracksNPixelDeadSensors = 0; ATH_CHECK( xTrackParticle->summaryValue(iTracksNPixelDeadSensors, xAOD::numberOfPixelDeadSensors) );
  uint8_t iTracksNSCTHits = 0; ATH_CHECK( xTrackParticle->summaryValue(iTracksNSCTHits, xAOD::numberOfSCTHits) );
  uint8_t iTracksNSCTSharedHits = 0; ATH_CHECK( xTrackParticle->summaryValue(iTracksNSCTSharedHits, xAOD::numberOfSCTSharedHits) );
  uint8_t iTracksNSCTDeadSensors = 0; ATH_CHECK( xTrackParticle->summaryValue(iTracksNSCTDeadSensors, xAOD::numberOfSCTDeadSensors) );
  uint8_t iTracksNTRTHighThresholdHits = 0; ATH_CHECK( xTrackParticle->summaryValue( iTracksNTRTHighThresholdHits, xAOD::numberOfTRTHighThresholdHits) );
  uint8_t iTracksNTRTHits = 0; ATH_CHECK( xTrackParticle->summaryValue( iTracksNTRTHits, xAOD::numberOfTRTHits) );
  uint8_t iNumberOfContribPixelLayers = 0; ATH_CHECK( xTrackParticle->summaryValue(iNumberOfContribPixelLayers, xAOD::numberOfContribPixelLayers) );
  uint8_t iNumberOfPixelHoles = 0; ATH_CHECK( xTrackParticle->summaryValue(iNumberOfPixelHoles, xAOD::numberOfPixelHoles) );
  uint8_t iNumberOfSCTHoles = 0; ATH_CHECK( xTrackParticle->summaryValue(iNumberOfSCTHoles, xAOD::numberOfSCTHoles) );
=======
StatusCode TrackMVABDT::parseVariableContent()
{
  // // example     <Variable VarIndex="0" Expression="TracksAuxDyn.tauPt" Label="TracksAuxDyn.tauPt" Title="tauPt" Unit="" Internal="TracksAuxDyn.tauPt" Type="F" Min="1.50000762e+04" Max="5.32858625e+05"/>
  // std::string sLine;
  // std::ifstream sFileStream (m_sInputWeightsPath);
  // if (sFileStream.is_open())
  // {
  //   while ( getline (sFileStream,sLine) )
  //   {
  //     if (sLine.find("/Variables") != std::string::npos)
  //       break;
  //     size_t iPosVarindex = sLine.find("VarIndex=");
	
  //     if ( iPosVarindex == std::string::npos )
  //       continue;
	
  //     iPosVarindex += 10;
	
  //     size_t iPosVarindexEnd = sLine.find("\"",iPosVarindex);
  //     size_t iPosExpression = sLine.find("Expression=")+12;
  //     size_t iPosExpressionEnd = sLine.find("\"",iPosExpression);
	
  //     int iVarIndex = std::stoi(sLine.substr(iPosVarindex, iPosVarindexEnd-iPosVarindex));
  //     std::string sExpression = sLine.substr(iPosExpression, iPosExpressionEnd-iPosExpression);
	
  //     m_mParsedVarsBDT.insert(std::pair<int, std::string >(iVarIndex,sExpression));
  //   }
  //   sFileStream.close();
  //   return StatusCode::SUCCESS;
  // }

  // ATH_MSG_ERROR("Unable to open file "<<m_sInputWeightsPath);
  // return StatusCode::FAILURE;
  return StatusCode::SUCCESS;
}

//______________________________________________________________________________
StatusCode TrackMVABDT::setVars(const xAOD::TauTrack& xTrack, const xAOD::TauJet& xTau)
{
  const xAOD::TrackParticle* xTrackParticle = xTrack.track();
  uint8_t iTracksNumberOfInnermostPixelLayerHits = 0; TRT_CHECK_BOOL( xTrackParticle->summaryValue(iTracksNumberOfInnermostPixelLayerHits, xAOD::numberOfInnermostPixelLayerHits), StatusCode::FAILURE );
  uint8_t iTracksNPixelHits = 0; TRT_CHECK_BOOL( xTrackParticle->summaryValue(iTracksNPixelHits, xAOD::numberOfPixelHits), StatusCode::FAILURE );
  uint8_t iTracksNPixelSharedHits = 0; TRT_CHECK_BOOL( xTrackParticle->summaryValue(iTracksNPixelSharedHits, xAOD::numberOfPixelSharedHits), StatusCode::FAILURE );
  uint8_t iTracksNPixelDeadSensors = 0; TRT_CHECK_BOOL( xTrackParticle->summaryValue(iTracksNPixelDeadSensors, xAOD::numberOfPixelDeadSensors), StatusCode::FAILURE );
  uint8_t iTracksNSCTHits = 0; TRT_CHECK_BOOL( xTrackParticle->summaryValue(iTracksNSCTHits, xAOD::numberOfSCTHits), StatusCode::FAILURE );
  uint8_t iTracksNSCTSharedHits = 0; TRT_CHECK_BOOL( xTrackParticle->summaryValue(iTracksNSCTSharedHits, xAOD::numberOfSCTSharedHits), StatusCode::FAILURE );
  uint8_t iTracksNSCTDeadSensors = 0; TRT_CHECK_BOOL( xTrackParticle->summaryValue(iTracksNSCTDeadSensors, xAOD::numberOfSCTDeadSensors), StatusCode::FAILURE );
  uint8_t iTracksNTRTHighThresholdHits = 0; TRT_CHECK_BOOL( xTrackParticle->summaryValue( iTracksNTRTHighThresholdHits, xAOD::numberOfTRTHighThresholdHits), StatusCode::FAILURE );
  uint8_t iTracksNTRTHits = 0; TRT_CHECK_BOOL( xTrackParticle->summaryValue( iTracksNTRTHits, xAOD::numberOfTRTHits), StatusCode::FAILURE );
  uint8_t iNumberOfContribPixelLayers = 0; TRT_CHECK_BOOL( xTrackParticle->summaryValue(iNumberOfContribPixelLayers, xAOD::numberOfContribPixelLayers), StatusCode::FAILURE );
  uint8_t iNumberOfPixelHoles = 0; TRT_CHECK_BOOL( xTrackParticle->summaryValue(iNumberOfPixelHoles, xAOD::numberOfPixelHoles), StatusCode::FAILURE );
  uint8_t iNumberOfSCTHoles = 0; TRT_CHECK_BOOL( xTrackParticle->summaryValue(iNumberOfSCTHoles, xAOD::numberOfSCTHoles), StatusCode::FAILURE );
>>>>>>> release/21.0.127
	
  float fTracksNumberOfInnermostPixelLayerHits = (float)iTracksNumberOfInnermostPixelLayerHits;
  float fTracksNPixelHits = (float)iTracksNPixelHits;
  float fTracksNPixelDeadSensors = (float)iTracksNPixelDeadSensors;
  float fTracksNPixelSharedHits = (float)iTracksNPixelSharedHits;
  float fTracksNSCTHits = (float)iTracksNSCTHits;
  float fTracksNSCTDeadSensors = (float)iTracksNSCTDeadSensors;
  float fTracksNSCTSharedHits = (float)iTracksNSCTSharedHits;
  float fTracksNTRTHighThresholdHits = (float)iTracksNTRTHighThresholdHits;
  float fTracksNTRTHits = (float)iTracksNTRTHits;
	
  float fTracksNPixHits = fTracksNPixelHits + fTracksNPixelDeadSensors;
  float fTracksNSiHits = fTracksNPixelHits + fTracksNPixelDeadSensors + fTracksNSCTHits + fTracksNSCTDeadSensors;

<<<<<<< HEAD
  float fTracksEProbabilityHT; ATH_CHECK( xTrackParticle->summaryValue( fTracksEProbabilityHT, xAOD::eProbabilityHT) );
=======
  float fTracksEProbabilityHT; TRT_CHECK_BOOL( xTrackParticle->summaryValue( fTracksEProbabilityHT, xAOD::eProbabilityHT), StatusCode::FAILURE );
>>>>>>> release/21.0.127

  float fNumberOfContribPixelLayers = float(iNumberOfContribPixelLayers);
  float fNumberOfPixelHoles = float(iNumberOfPixelHoles);
  float fNumberOfSCTHoles = float(iNumberOfSCTHoles);

<<<<<<< HEAD
  std::map<TString, float> valueMap;
  // Could use the same naming convention in the BDT to simplify 
  valueMap["TracksAuxDyn.jetSeedPt"] = xTau.ptJetSeed();
  valueMap["TracksAuxDyn.tauPt"] = xTau.ptIntermediateAxis();
  valueMap["TracksAuxDyn.tauEta"] = xTau.etaIntermediateAxis();
  valueMap["TracksAuxDyn.z0sinThetaTJVA"] = xTrack.z0sinthetaTJVA();
  valueMap["TracksAuxDyn.rConv"] = xTrack.rConv();
  valueMap["TracksAuxDyn.rConvII"] = xTrack.rConvII();
  valueMap["TauTracksAuxDyn.rConv/TauTracksAuxDyn.rConvII"] = xTrack.rConv()/xTrack.rConvII();
  valueMap["TracksAuxDyn.DRJetSeedAxis"] = xTrack.dRJetSeedAxis(xTau);
  valueMap["TracksAuxDyn.dRJetSeedAxis"] = xTrack.dRJetSeedAxis(xTau);
  valueMap["TracksAuxDyn.trackEta"] = xTrackParticle->eta();
  valueMap["TracksAux.d0"] = xTrack.d0TJVA();
  valueMap["TracksAux.qOverP"] = xTrackParticle->qOverP();
  valueMap["TracksAux.theta"] = xTrackParticle->theta();
  valueMap["TracksAux.eProbabilityHT"] = fTracksEProbabilityHT;
  valueMap["TracksAux.numberOfInnermostPixelLayerHits"] = fTracksNumberOfInnermostPixelLayerHits;
  valueMap["TracksAux.numberOfPixelHits"] = fTracksNPixelHits;
  valueMap["TracksAux.numberOfPixelDeadSensors"] = fTracksNPixelDeadSensors;
  valueMap["TracksAux.numberOfPixelSharedHits"] = fTracksNPixelSharedHits;
  valueMap["TracksAux.numberOfSCTHits"] = fTracksNSCTHits;
  valueMap["TracksAux.numberOfSCTDeadSensors"] = fTracksNSCTDeadSensors;
  valueMap["TracksAux.numberOfSCTSharedHits"] = fTracksNSCTSharedHits;
  valueMap["TracksAux.numberOfTRTHighThresholdHits"] = fTracksNTRTHighThresholdHits;
  valueMap["TracksAux.numberOfTRTHits"] = fTracksNTRTHits;
  valueMap["TracksAux.numberOfPixelHits+TracksAux.numberOfPixelDeadSensors"] = fTracksNPixHits;
  valueMap["TracksAux.numberOfPixelHits+TracksAux.numberOfPixelDeadSensors+TracksAux.numberOfSCTHits+TracksAux.numberOfSCTDeadSensors"] = fTracksNSiHits;

  valueMap["TauTracksAuxDyn.jetSeedPt"] = xTau.ptJetSeed();
  valueMap["TauTracksAuxDyn.tauPt"] = xTau.ptIntermediateAxis();
  valueMap["TauTracksAuxDyn.tauEta"] = xTau.etaIntermediateAxis();
  valueMap["TauTracksAuxDyn.z0sinThetaTJVA"] = xTrack.z0sinthetaTJVA();
  valueMap["TauTracksAuxDyn.rConv"] = xTrack.rConv();
  valueMap["TauTracksAuxDyn.rConvII"] = xTrack.rConvII();
  valueMap["TauTracksAuxDyn.rConv/TauTracksAuxDyn.rConvII"] = xTrack.rConv()/xTrack.rConvII();
  valueMap["TauTracksAuxDyn.dRJetSeedAxis"] = xTrack.dRJetSeedAxis(xTau);
  valueMap["TauTracksAuxDyn.trackEta"] = xTrackParticle->eta();
  valueMap["TauTracksAuxDyn.d0"] = xTrack.d0TJVA();
  valueMap["TauTracksAuxDyn.qOverP"] = xTrackParticle->qOverP();
  valueMap["TauTracksAuxDyn.theta"] = xTrackParticle->theta();
  valueMap["TauTracksAuxDyn.eProbabilityHT"] = fTracksEProbabilityHT;
  valueMap["TauTracksAuxDyn.numberOfInnermostPixelLayerHits"] = fTracksNumberOfInnermostPixelLayerHits;
  valueMap["TauTracksAuxDyn.numberOfPixelHits"] = fTracksNPixelHits;
  valueMap["TauTracksAuxDyn.numberOfPixelDeadSensors"] = fTracksNPixelDeadSensors;
  valueMap["TauTracksAuxDyn.numberOfPixelSharedHits"] = fTracksNPixelSharedHits;
  valueMap["TauTracksAuxDyn.numberOfSCTHits"] = fTracksNSCTHits;
  valueMap["TauTracksAuxDyn.numberOfSCTDeadSensors"] = fTracksNSCTDeadSensors;
  valueMap["TauTracksAuxDyn.numberOfSCTSharedHits"] = fTracksNSCTSharedHits;
  valueMap["TauTracksAuxDyn.numberOfTRTHighThresholdHits"] = fTracksNTRTHighThresholdHits;
  valueMap["TauTracksAuxDyn.numberOfTRTHits"] = fTracksNTRTHits;
  valueMap["TauTracksAuxDyn.numberOfPixelHits+TauTracksAuxDyn.numberOfPixelDeadSensors"] = fTracksNPixHits;
  valueMap["TauTracksAuxDyn.numberOfPixelHits+TauTracksAuxDyn.numberOfPixelDeadSensors+TauTracksAuxDyn.numberOfSCTHits+TauTracksAuxDyn.numberOfSCTDeadSensors"] = fTracksNSiHits;

  valueMap["1/(TauTracksAuxDyn.trackPt)"] = 1./xTrackParticle->pt();
  valueMap["fabs(TauTracksAuxDyn.qOverP)"] = std::abs(xTrackParticle->qOverP());
  valueMap["TauTracksAuxDyn.numberOfContribPixelLayers"] = fNumberOfContribPixelLayers;
  valueMap["TauTracksAuxDyn.numberOfPixelHits+TauTracksAuxDyn.numberOfPixelDeadSensors+TauTracksAuxDyn.numberOfPixelHoles"] = fTracksNPixHits+fNumberOfPixelHoles;
  valueMap["TauTracksAuxDyn.numberOfPixelHits+TauTracksAuxDyn.numberOfPixelDeadSensors+TauTracksAuxDyn.numberOfPixelHoles+TauTracksAuxDyn.numberOfSCTHits+TauTracksAuxDyn.numberOfSCTDeadSensors+TauTracksAuxDyn.numberOfSCTHoles"] = fTracksNSiHits+fNumberOfPixelHoles+fNumberOfSCTHoles;
  valueMap["TauTracksAuxDyn.numberOfPixelHoles"] = fNumberOfPixelHoles;
  valueMap["TauTracksAuxDyn.numberOfPixelHoles+TauTracksAuxDyn.numberOfSCTHoles"] = fNumberOfPixelHoles+fNumberOfSCTHoles;
  valueMap["TauTracksAuxDyn.numberOfSCTHoles"] = fNumberOfSCTHoles;
  valueMap["TauTracksAux.pt"] = xTrackParticle->pt();

  values.clear();
  values.reserve(m_inputVariableNames.size());
  for (auto varName : m_inputVariableNames) {
    values.push_back(valueMap[varName]);
  }

  return StatusCode::SUCCESS;  
=======
  setVar("TracksAuxDyn.jetSeedPt") = xTau.ptJetSeed();
  setVar("TracksAuxDyn.tauPt") = xTau.ptIntermediateAxis();
  setVar("TracksAuxDyn.tauEta") = xTau.etaIntermediateAxis();
  setVar("TracksAuxDyn.z0sinThetaTJVA") = xTrack.z0sinThetaTJVA(xTau);
  setVar("TracksAuxDyn.rConv") = xTrack.rConv(xTau);
  setVar("TracksAuxDyn.rConvII") = xTrack.rConvII(xTau);
  setVar("TauTracksAuxDyn.rConv/TauTracksAuxDyn.rConvII") = xTrack.rConv(xTau)/xTrack.rConvII(xTau);
  setVar("TracksAuxDyn.DRJetSeedAxis") = xTrack.dRJetSeedAxis(xTau);
  setVar("TracksAuxDyn.dRJetSeedAxis") = xTrack.dRJetSeedAxis(xTau);
  setVar("TracksAuxDyn.trackEta") = xTrackParticle->eta();
  setVar("TracksAux.d0") = xTrackParticle->d0();
  setVar("TracksAux.qOverP") = xTrackParticle->qOverP();
  setVar("TracksAux.theta") = xTrackParticle->theta();
  setVar("TracksAux.eProbabilityHT") = fTracksEProbabilityHT;
  setVar("TracksAux.numberOfInnermostPixelLayerHits") = fTracksNumberOfInnermostPixelLayerHits;
  setVar("TracksAux.numberOfPixelHits") = fTracksNPixelHits;
  setVar("TracksAux.numberOfPixelDeadSensors") = fTracksNPixelDeadSensors;
  setVar("TracksAux.numberOfPixelSharedHits") = fTracksNPixelSharedHits;
  setVar("TracksAux.numberOfSCTHits") = fTracksNSCTHits;
  setVar("TracksAux.numberOfSCTDeadSensors") = fTracksNSCTDeadSensors;
  setVar("TracksAux.numberOfSCTSharedHits") = fTracksNSCTSharedHits;
  setVar("TracksAux.numberOfTRTHighThresholdHits") = fTracksNTRTHighThresholdHits;
  setVar("TracksAux.numberOfTRTHits") = fTracksNTRTHits;
  setVar("TracksAux.numberOfPixelHits+TracksAux.numberOfPixelDeadSensors") = fTracksNPixHits;
  setVar("TracksAux.numberOfPixelHits+TracksAux.numberOfPixelDeadSensors+TracksAux.numberOfSCTHits+TracksAux.numberOfSCTDeadSensors") = fTracksNSiHits;

  setVar("TauTracksAuxDyn.jetSeedPt") = xTau.ptJetSeed();
  setVar("TauTracksAuxDyn.tauPt") = xTau.ptIntermediateAxis();
  setVar("TauTracksAuxDyn.tauEta") = xTau.etaIntermediateAxis();
  setVar("TauTracksAuxDyn.z0sinThetaTJVA") = xTrack.z0sinThetaTJVA(xTau);
  setVar("TauTracksAuxDyn.rConv") = xTrack.rConv(xTau);
  setVar("TauTracksAuxDyn.rConvII") = xTrack.rConvII(xTau);
  setVar("TauTracksAuxDyn.rConv/TauTracksAuxDyn.rConvII") = xTrack.rConv(xTau)/xTrack.rConvII(xTau);
  setVar("TauTracksAuxDyn.dRJetSeedAxis") = xTrack.dRJetSeedAxis(xTau);
  setVar("TauTracksAuxDyn.trackEta") = xTrackParticle->eta();
  setVar("TauTracksAuxDyn.d0") = xTrackParticle->d0();
  setVar("TauTracksAuxDyn.qOverP") = xTrackParticle->qOverP();
  setVar("TauTracksAuxDyn.theta") = xTrackParticle->theta();
  setVar("TauTracksAuxDyn.eProbabilityHT") = fTracksEProbabilityHT;
  setVar("TauTracksAuxDyn.numberOfInnermostPixelLayerHits") = fTracksNumberOfInnermostPixelLayerHits;
  setVar("TauTracksAuxDyn.numberOfPixelHits") = fTracksNPixelHits;
  setVar("TauTracksAuxDyn.numberOfPixelDeadSensors") = fTracksNPixelDeadSensors;
  setVar("TauTracksAuxDyn.numberOfPixelSharedHits") = fTracksNPixelSharedHits;
  setVar("TauTracksAuxDyn.numberOfSCTHits") = fTracksNSCTHits;
  setVar("TauTracksAuxDyn.numberOfSCTDeadSensors") = fTracksNSCTDeadSensors;
  setVar("TauTracksAuxDyn.numberOfSCTSharedHits") = fTracksNSCTSharedHits;
  setVar("TauTracksAuxDyn.numberOfTRTHighThresholdHits") = fTracksNTRTHighThresholdHits;
  setVar("TauTracksAuxDyn.numberOfTRTHits") = fTracksNTRTHits;
  setVar("TauTracksAuxDyn.numberOfPixelHits+TauTracksAuxDyn.numberOfPixelDeadSensors") = fTracksNPixHits;
  setVar("TauTracksAuxDyn.numberOfPixelHits+TauTracksAuxDyn.numberOfPixelDeadSensors+TauTracksAuxDyn.numberOfSCTHits+TauTracksAuxDyn.numberOfSCTDeadSensors") = fTracksNSiHits;

  setVar("1/(TauTracksAuxDyn.trackPt)") = 1./xTrackParticle->pt();
  setVar("fabs(TauTracksAuxDyn.qOverP)") = std::abs(xTrackParticle->qOverP());
  setVar("TauTracksAuxDyn.numberOfContribPixelLayers") = fNumberOfContribPixelLayers;
  setVar("TauTracksAuxDyn.numberOfPixelHits+TauTracksAuxDyn.numberOfPixelDeadSensors+TauTracksAuxDyn.numberOfPixelHoles") = fTracksNPixHits+fNumberOfPixelHoles;
  setVar("TauTracksAuxDyn.numberOfPixelHits+TauTracksAuxDyn.numberOfPixelDeadSensors+TauTracksAuxDyn.numberOfPixelHoles+TauTracksAuxDyn.numberOfSCTHits+TauTracksAuxDyn.numberOfSCTDeadSensors+TauTracksAuxDyn.numberOfSCTHoles") = fTracksNSiHits+fNumberOfPixelHoles+fNumberOfSCTHoles;
  setVar("TauTracksAuxDyn.numberOfPixelHoles") = fNumberOfPixelHoles;
  setVar("TauTracksAuxDyn.numberOfPixelHoles+TauTracksAuxDyn.numberOfSCTHoles") = fNumberOfPixelHoles+fNumberOfSCTHoles;
  setVar("TauTracksAuxDyn.numberOfSCTHoles") = fNumberOfSCTHoles;
  setVar("TauTracksAux.pt") = xTrackParticle->pt();

  return StatusCode::SUCCESS;
  
  // for (auto eEntry : m_mAvailableVars)
  //   std::cout << eEntry.first<<": "<<eEntry.second<<"\n";
>>>>>>> release/21.0.127
}
