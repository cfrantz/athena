#ifndef XAOD_ANALYSIS
#include "../JetSeedBuilder.h"
#include "../TauAxisSetter.h"
<<<<<<< HEAD
=======
#include "../TauCalibrateEM.h"
>>>>>>> release/21.0.127
#include "../TauCellVariables.h"
#include "../TauTrackFinder.h"
#include "../TauClusterFinder.h"
#include "../TauVertexFinder.h"
#include "../TauElectronVetoVariables.h"
#include "../TauShotFinder.h"
#include "../TauPi0ClusterCreator.h"
#include "../TauPi0CreateROI.h"
<<<<<<< HEAD
#include "../TauPi0ClusterScaler.h"
#include "../TauVertexVariables.h"
#endif

#include "tauRecTools/TauCalibrateLC.h"
#include "tauRecTools/TauCommonCalcVars.h"
#include "tauRecTools/TauSubstructureVariables.h"
#include "tauRecTools/MvaTESEvaluator.h"
#include "tauRecTools/MvaTESVariableDecorator.h"
#include "tauRecTools/TauTrackClassifier.h"
#include "tauRecTools/TauTrackRNNClassifier.h"
#include "tauRecTools/TauCombinedTES.h"
#include "tauRecTools/TauPi0ScoreCalculator.h"
#include "tauRecTools/TauPi0Selector.h"
#include "tauRecTools/TauWPDecorator.h"
#include "tauRecTools/TauJetBDTEvaluator.h"
#include "tauRecTools/TauIDVarCalculator.h"
#include "tauRecTools/TauJetRNNEvaluator.h"
#include "tauRecTools/TauDecayModeNNClassifier.h"
#include "tauRecTools/TauVertexedClusterDecorator.h"
=======
#include "../TauConversionFinder.h"
#include "../PhotonConversionPID.h"
#include "../PhotonConversionVertex.h"
#include "../TauConversionTagger.h"
#include "../TauVertexVariables.h"
#include "../TauTestDump.h"
//#include "../tauCalibrateWeightTool.h"  //for trigger
#endif
#include "tauRecTools/TauTrackFilter.h"
#include "tauRecTools/TauGenericPi0Cone.h"
#include "tauRecTools/TauCalibrateLC.h"
#include "tauRecTools/TauIDPileupCorrection.h"
#include "tauRecTools/TauCommonCalcVars.h"
#include "tauRecTools/TauSubstructureVariables.h"
#include "tauRecTools/TauProcessorTool.h"
#include "tauRecTools/TauBuilderTool.h"
#include "tauRecTools/MvaTESEvaluator.h"
#include "tauRecTools/MvaTESVariableDecorator.h"
#include "tauRecTools/TauTrackClassifier.h"
#include "tauRecTools/CombinedP4FromRecoTaus.h"
#include "tauRecTools/TauPi0ClusterScaler.h"
#include "tauRecTools/TauPi0ScoreCalculator.h"
#include "tauRecTools/TauPi0Selector.h"
#include "tauRecTools/TauWPDecorator.h"
#include "tauRecTools/DiTauDiscriminantTool.h"
#include "tauRecTools/DiTauIDVarCalculator.h"
#include "tauRecTools/TauJetBDTEvaluator.h"
#include "tauRecTools/TauEleOLRDecorator.h"
#include "tauRecTools/TauIDVarCalculator.h"
>>>>>>> release/21.0.127

#ifndef XAOD_ANALYSIS
DECLARE_COMPONENT( JetSeedBuilder )
DECLARE_COMPONENT( TauAxisSetter )
DECLARE_COMPONENT( TauCellVariables )
DECLARE_COMPONENT( TauTrackFinder )
DECLARE_COMPONENT( TauClusterFinder )
DECLARE_COMPONENT( TauVertexFinder )
DECLARE_COMPONENT( TauElectronVetoVariables )
DECLARE_COMPONENT( TauShotFinder )
DECLARE_COMPONENT( TauPi0ClusterCreator )
DECLARE_COMPONENT( TauPi0CreateROI )
DECLARE_COMPONENT( TauPi0ClusterScaler )
DECLARE_COMPONENT( TauVertexVariables )
#endif

<<<<<<< HEAD
DECLARE_COMPONENT( TauCalibrateLC )
DECLARE_COMPONENT( MvaTESVariableDecorator )
DECLARE_COMPONENT( MvaTESEvaluator )
DECLARE_COMPONENT( tauRecTools::TauTrackClassifier )
DECLARE_COMPONENT( tauRecTools::TauTrackRNNClassifier )
DECLARE_COMPONENT( tauRecTools::TrackMVABDT )
DECLARE_COMPONENT( tauRecTools::TrackRNN )
DECLARE_COMPONENT( TauCombinedTES )
DECLARE_COMPONENT( TauSubstructureVariables )
DECLARE_COMPONENT( TauCommonCalcVars )
DECLARE_COMPONENT( TauPi0ScoreCalculator )
DECLARE_COMPONENT( TauPi0Selector )
DECLARE_COMPONENT( TauWPDecorator )
DECLARE_COMPONENT( TauJetBDTEvaluator )
DECLARE_COMPONENT( TauIDVarCalculator )
DECLARE_COMPONENT( TauJetRNNEvaluator )
DECLARE_COMPONENT( TauDecayModeNNClassifier )
DECLARE_COMPONENT( TauVertexedClusterDecorator )
=======
#ifndef XAOD_ANALYSIS
DECLARE_TOOL_FACTORY( JetSeedBuilder             )
DECLARE_TOOL_FACTORY( LockTauContainers          )
DECLARE_TOOL_FACTORY( TauAxisSetter         )
DECLARE_TOOL_FACTORY( TauCalibrateEM             )
DECLARE_TOOL_FACTORY( TauCellVariables           )
DECLARE_TOOL_FACTORY( TauTrackFinder             )
DECLARE_TOOL_FACTORY( TauVertexFinder            )
DECLARE_TOOL_FACTORY( TauElectronVetoVariables             )
DECLARE_TOOL_FACTORY( TauShotFinder              )
DECLARE_TOOL_FACTORY( TauPi0ClusterCreator   )
DECLARE_TOOL_FACTORY( TauPi0CreateROI        )
DECLARE_TOOL_FACTORY( PhotonConversionPID )
DECLARE_TOOL_FACTORY( PhotonConversionVertex )
DECLARE_TOOL_FACTORY( TauConversionFinder )
DECLARE_TOOL_FACTORY( TauConversionTagger )
DECLARE_TOOL_FACTORY( TauVertexVariables )
//DECLARE_TOOL_FACTORY( tauCalibrateWeightTool )
DECLARE_TOOL_FACTORY( TauTestDump )
#endif

DECLARE_TOOL_FACTORY( TauCalibrateLC             )
DECLARE_TOOL_FACTORY( TauIDPileupCorrection             )
DECLARE_TOOL_FACTORY( TauProcessorTool             )
DECLARE_TOOL_FACTORY( TauBuilderTool             )
DECLARE_TOOL_FACTORY( MvaTESVariableDecorator )
DECLARE_TOOL_FACTORY( MvaTESEvaluator )
DECLARE_NAMESPACE_TOOL_FACTORY( tauRecTools, TauTrackClassifier  )
DECLARE_NAMESPACE_TOOL_FACTORY( tauRecTools, TrackMVABDT         )
DECLARE_TOOL_FACTORY( CombinedP4FromRecoTaus )
DECLARE_TOOL_FACTORY( TauTrackFilter )
DECLARE_TOOL_FACTORY( TauGenericPi0Cone )
DECLARE_TOOL_FACTORY( TauSubstructureVariables     )
DECLARE_TOOL_FACTORY( TauCommonCalcVars          )
DECLARE_TOOL_FACTORY( TauPi0ClusterScaler  )
DECLARE_TOOL_FACTORY( TauPi0ScoreCalculator  )
DECLARE_TOOL_FACTORY( TauPi0Selector        )
DECLARE_TOOL_FACTORY( TauWPDecorator )
DECLARE_NAMESPACE_TOOL_FACTORY( tauRecTools, DiTauDiscriminantTool )
DECLARE_NAMESPACE_TOOL_FACTORY( tauRecTools, DiTauIDVarCalculator )
DECLARE_TOOL_FACTORY( TauJetBDTEvaluator )
DECLARE_TOOL_FACTORY( TauEleOLRDecorator )
DECLARE_TOOL_FACTORY( TauIDVarCalculator )
>>>>>>> release/21.0.127
