/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#ifndef XAOD_ANALYSIS
<<<<<<< HEAD
#ifndef TAURECTOOLS_TAUTRACKFINDER_H
#define TAURECTOOLS_TAUTRACKFINDER_H
=======
#ifndef TAUREC_TAUTRACKFINDER_H
#define TAUREC_TAUTRACKFINDER_H
>>>>>>> release/21.0.127

#include "tauRecTools/TauRecToolBase.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/ReadHandleKey.h"
#include "ITrackToVertex/ITrackToVertex.h"
#include "TrkCaloExtension/CaloExtensionCollection.h"

#include "xAODTracking/Vertex.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"

<<<<<<< HEAD
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"
#include "TrkToolInterfaces/ITrackSelectorTool.h"
#include "RecoToolInterfaces/IParticleCaloExtensionTool.h"
#include "TrkVertexFitterInterfaces/ITrackToVertexIPEstimator.h"
#include "BeamSpotConditionsData/BeamSpotData.h"
=======
#include "VxVertex/RecVertex.h"

// xAOD Tracking Tool
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"

namespace Trk {
  class ITrackSelectorTool;
  class IParticleCaloExtensionTool;
}
>>>>>>> release/21.0.127

/////////////////////////////////////////////////////////////////////////////

/** 
 * @brief Associate tracks to the tau candidate.
 * 
 *  The tracks have to pass dedicated quality criteria and have to 
 *  match to a primary vertex consistent with the tau origin.
 * 
 * @author KG Tan <Kong.Guan.Tan@cern.ch>
 * @author Felix Friedrich
 */

class TauTrackFinder : public TauRecToolBase {
public:
    //-------------------------------------------------------------
    //! Constructor and Destructor
    //-------------------------------------------------------------
    TauTrackFinder(const std::string& name);
    ASG_TOOL_CLASS2(TauTrackFinder, TauRecToolBase, ITauToolBase);
    ~TauTrackFinder();

    //-------------------------------------------------------------
    //! Enumerator defining type of tau track
    //-------------------------------------------------------------
    enum TauTrackType
    {
        TauTrackCore  = 0,
        TauTrackWide  = 1,
        TauTrackOther = 2,
        NotTauTrack   = 3
    };

    //-------------------------------------------------------------
    //! Algorithm functions
    //-------------------------------------------------------------
    virtual StatusCode initialize() override;
    virtual StatusCode executeTrackFinder(xAOD::TauJet& pTau, xAOD::TauTrackContainer& tauTrackCon, const xAOD::TrackParticleContainer* trackContainer = nullptr) const override;
    
private:
    //-------------------------------------------------------------
    //! Extrapolate track eta and phi to the calorimeter middle surface
    //-------------------------------------------------------------
    StatusCode extrapolateToCaloSurface(xAOD::TauJet& pTau) const;

    TauTrackType tauTrackType( const xAOD::TauJet& tauJet,
    		const xAOD::TrackParticle& trackParticle,
    		const xAOD::Vertex* primaryVertex) const;

    void getTauTracksFromPV( const xAOD::TauJet& tauJet,
    		const xAOD::TrackParticleContainer& trackParticleCont,
    		const xAOD::Vertex* primaryVertex,
    		std::vector<const xAOD::TrackParticle*> &tauTracks,
    		std::vector<const xAOD::TrackParticle*> &wideTracks,
    		std::vector<const xAOD::TrackParticle*> &otherTracks) const;

    // new xAOD version
    void removeOffsideTracksWrtLeadTrk(std::vector<const xAOD::TrackParticle*> &tauTracks,
                                           std::vector<const xAOD::TrackParticle*> &wideTracks,
                                           std::vector<const xAOD::TrackParticle*> &otherTracks,
                                           const xAOD::Vertex* tauOrigin,
                                           double maxDeltaZ0) const;

    //-------------------------------------------------------------
    //! Some internally used functions
    //-------------------------------------------------------------
<<<<<<< HEAD
    float getZ0(const xAOD::TrackParticle* track, const xAOD::Vertex* vertex) const;   //xAOD version
    bool  isLargeD0Track(const xAOD::TrackParticle* track) const;   
=======
    float getZ0(const Rec::TrackParticle* track, const Trk::RecVertex* vertex);  //AOD version
    float getZ0(const xAOD::TrackParticle* track, const xAOD::Vertex* vertex);   //xAOD version

private:
    //-------------------------------------------------------------
    //! Storegate names of input containers and output containers
    //-------------------------------------------------------------
    std::string m_inputTauJetContainerName;
    std::string m_inputTrackParticleContainerName;
    std::string m_inputPrimaryVertexContainerName;
    std::string m_inputTauTrackContainerName;
>>>>>>> release/21.0.127

    //-------------------------------------------------------------
    //! tools
    //-------------------------------------------------------------
<<<<<<< HEAD
    ToolHandle<Trk::IParticleCaloExtensionTool> m_caloExtensionTool {this, "ParticleCaloExtensionTool", "Trk::ParticleCaloExtensionTool/ParticleCaloExtensionTool", "Tool for the extrapolation of charged tracks"};
    ToolHandle<Trk::ITrackSelectorTool> m_trackSelectorTool_tau {this, "TrackSelectorToolTau", "", "Tool for track selection"};
    ToolHandle<Reco::ITrackToVertex> m_trackToVertexTool {this, "TrackToVertexTool", "Reco::TrackToVertex"};
    ToolHandle<Trk::ITrackToVertexIPEstimator> m_trackToVertexIPEstimator {this, "TrackToVertexIPEstimator", ""};
=======
    ToolHandle< Trk::IParticleCaloExtensionTool >  m_caloExtensionTool;
    ToolHandle<Trk::ITrackSelectorTool> m_trackSelectorTool_tau;
    ToolHandle<Reco::ITrackToVertex> m_trackToVertexTool;
    ToolHandle<InDet::IInDetTrackSelectionTool> m_trackSelectorTool_tau_xAOD;

>>>>>>> release/21.0.127
    
    Gaudi::Property<double>  m_maxJetDr_tau {this, "MaxJetDrTau", 0.2};
    Gaudi::Property<double> m_maxJetDr_wide {this, "MaxJetDrWide", 0.4};   
    Gaudi::Property<bool> m_applyZ0cut {this, "removeTracksOutsideZ0wrtLeadTrk", false};
    Gaudi::Property<float> m_z0maxDelta {this, "maxDeltaZ0wrtLeadTrk", 1000};    
    Gaudi::Property<bool> m_storeInOtherTrks {this, "StoreRemovedCoreWideTracksInOtherTracks", true};
    Gaudi::Property<bool> m_removeDuplicateCoreTracks {this, "removeDuplicateCoreTracks", true};
    Gaudi::Property<bool> m_bypassSelector {this, "BypassSelector", false};
    Gaudi::Property<bool> m_bypassExtrapolator {this, "BypassExtrapolator", false};

    SG::ReadHandleKey<xAOD::TrackParticleContainer> m_trackPartInputContainer{this,"Key_trackPartInputContainer", "InDetTrackParticles", "input track particle container key"};
 	SG::ReadHandleKey<xAOD::TrackParticleContainer> m_largeD0TracksInputContainer{this,"Key_LargeD0TrackInputContainer", "", "input LRT particle container key"}; //Expecting InDetLargeD0TrackParticles (offline tracks) if using LRT used
    SG::ReadHandleKey<CaloExtensionCollection>  m_ParticleCacheKey{this,"tauParticleCache", "ParticleCaloExtension", "Name of the particle measurement extrapolation cache for TauTrackFinder"};
    
<<<<<<< HEAD
    SG::ReadCondHandleKey<InDet::BeamSpotData> m_beamSpotKey { this, "BeamSpotKey", "BeamSpotData", "SG key for beam spot" };
=======
    //-------------------------------------------------------------
    // z0 cuts
    //-------------------------------------------------------------
    float m_z0maxDelta;
    bool m_applyZ0cut;
    bool m_storeInOtherTrks;
    bool m_removeDuplicateCoreTracks;
    std::vector<float> m_vDeltaZ0coreTrks;
    std::vector<float> m_vDeltaZ0wideTrks;

    //-------------------------------------------------------------
    // Bypass TrackSelectorTool / Extrapolation
    //-------------------------------------------------------------

    bool m_bypassSelector;
    bool m_bypassExtrapolator;

    //-------------------------------------------------------------
    // Sets of EM/Had samplings for track extrapolation 
    //-------------------------------------------------------------
    std::set<CaloSampling::CaloSample> m_EMSamplings;
    std::set<CaloSampling::CaloSample> m_HadSamplings;

    //-------------------------------------------------------------
    //! Convenience functions to handle storegate objects
    //-------------------------------------------------------------
    template <class T>
    bool openContainer(T* &container, std::string containerName, bool printFATAL=false);

    template <class T>
    bool retrieveTool(T &tool);
>>>>>>> release/21.0.127

    std::set<CaloSampling::CaloSample> m_EMSamplings;
    std::set<CaloSampling::CaloSample> m_HadSamplings;
};

<<<<<<< HEAD
#endif // TAURECTOOLS_TAUTRACKFINDER_H
#endif // XAOD_ANALYSIS
=======
#endif //TAUREC_TAUTRACKFINDER_H
#endif //XAOD_ANALYSIS
>>>>>>> release/21.0.127
