/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef PFOCHARGEDVALIDATIONPLOTS_H
#define PFOCHARGEDVALIDATIONPLOTS_H

#include "TrkValHistUtils/PlotBase.h"
#include "PFOHistUtils/PFOPlots.h"
#include "PFOHistUtils/PFOPVMatchedPlots.h"
#include "PFOHistUtils/PFOAlgPropertyPlots.h"
<<<<<<< HEAD
#include "PFOHistUtils/FlowElement_LinkerPlots.h"
#include "xAODPFlow/PFO.h"
#include "xAODPFlow/FlowElement.h"
=======
#include "xAODPFlow/PFO.h"
>>>>>>> release/21.0.127
#include "xAODTracking/Vertex.h" 

class PFOChargedValidationPlots : public PlotBase {

 public:

  /** Standard Constructor */
<<<<<<< HEAD
  PFOChargedValidationPlots(PlotBase* pParent, std::string sDir, std::string sPFOContainerName,std::string sFEContainerName);

  /** fill the histograms up */
  void fill(const xAOD::PFO& thePFO, const xAOD::Vertex* theVertex);
  void fill(const xAOD::FlowElement& theFE, const xAOD::Vertex* theVertex);
 private:
  // PFO plots
=======
  PFOChargedValidationPlots(PlotBase* pParent, std::string sDir, std::string sPFOContainerName);

  /** fill the histograms up */
  void fill(const xAOD::PFO& thePFO, const xAOD::Vertex* theVertex);

 private:
>>>>>>> release/21.0.127
  /** 4-vector and charge histograms */
  PFO::PFOPlots m_PFOPlots;
  /** 4-vector and charge histograms with PV match cut applied */
  PFO::PFOPVMatchedPlots m_PFOPVMatchedPlots;
  /** Algorithm property plots */
  PFO::PFOAlgPropertyPlots m_PFOAlgPropertyPlots;
<<<<<<< HEAD

  //Flow Element Plots
  /** 4-vector and charge histograms */
  PFO::PFOPlots m_FEPlots;
  /** 4-vector and charge histograms with PV match cut applied */
  PFO::PFOPVMatchedPlots m_FEPVMatchedPlots;
  /** Algorithm property plots */
  PFO::PFOAlgPropertyPlots m_FEAlgPropertyPlots;

  //Flow Element specific plots (linker algs)
  PFO::FlowElement_LinkerPlots m_FELinkerPlots;
=======
>>>>>>> release/21.0.127
};
#endif
