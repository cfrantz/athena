/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef PHYSVALPFO_H
#define PHYSVALPFO_H

#include "PFOChargedValidationPlots.h"
#include "PFONeutralValidationPlots.h"
#include "AthenaMonitoring/ManagedMonitorToolBase.h"
#include <string>
<<<<<<< HEAD
#include "xAODTracking/VertexContainer.h"
#include "xAODPFlow/PFOContainer.h"
#include "StoreGate/ReadHandleKey.h"
=======
#include "PFlowUtils/IRetrievePFOTool.h"
#include "xAODTracking/VertexContainer.h"
>>>>>>> release/21.0.127

class PhysValPFO : public ManagedMonitorToolBase {

public:

  /** Standard Constructor */
  PhysValPFO (const std::string& type, const std::string& name, const IInterface* parent );

  /** Standard Destructor */
  virtual ~PhysValPFO();
  
  /** Standard AlgTool Functions */
  virtual StatusCode initialize();
  virtual StatusCode bookHistograms();
  virtual StatusCode fillHistograms();
  virtual StatusCode procHistograms();

 private:

  /** ReadHandle to retrieve xAOD::VertexContainer */
<<<<<<< HEAD
  SG::ReadHandleKey<xAOD::VertexContainer> m_vertexContainerReadHandleKey{this,"primaryVerticesName","PrimaryVertices","ReadHandleKey for the PrimaryVertices container"};

  /** ReadHandle to retrieve xAOD::PFOContainer */
  SG::ReadHandleKey<xAOD::PFOContainer> m_PFOContainerHandleKey{this,"PFOContainerName","JetETMissChargedParticleFlowObjects","ReadHandleKey for the PFO container"};
    
=======
  SG::ReadHandle<xAOD::VertexContainer> m_vertexContainerReadHandle;
  
>>>>>>> release/21.0.127
  /** Pointer to class that deals with histograms for charged PFO */
  std::unique_ptr<PFOChargedValidationPlots> m_PFOChargedValidationPlots;

  /** Pointer to class that deals with histograms for neutral PFO */
  std::unique_ptr<PFONeutralValidationPlots> m_PFONeutralValidationPlots;
<<<<<<< HEAD
=======
  
  /** Tool to retrieve PFO */
  ToolHandle<CP::IRetrievePFOTool> m_retrievePFOTool;

  /** Select which PFO setup to use - LC or EM */
  bool m_useLCScale;
>>>>>>> release/21.0.127
  
  /** Select whether to use neutral or charged PFO */
  bool m_useNeutralPFO;

};
#endif
