/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef IWEIGHTPFOTOOL_H
#define IWEIGHTPFOTOOL_H

/** Simple class to retrieve PFO for jets and met in the two possible configurations we provide */

#include "AsgTools/IAsgTool.h"

#include "xAODPFlow/PFOContainer.h"
<<<<<<< HEAD
#include "xAODPFlow/FlowElementContainer.h"
=======
>>>>>>> release/21.0.127
#include "PFlowUtils/PFODefs.h"

namespace CP {

  class IWeightPFOTool : public virtual asg::IAsgTool {

    /** Declare the interface that the class provides */
    ASG_TOOL_INTERFACE( CP::IWeightPFOTool )
      
    public:

    /** given a PFO, extract weight */
<<<<<<< HEAD
    virtual StatusCode fillWeight( const xAOD::PFO& cpfo, float& weight ) const = 0;
    virtual StatusCode fillWeight( const xAOD::FlowElement& cpfo, float& weight ) const = 0;
=======
    virtual StatusCode fillWeight( const xAOD::PFO& cpfo, float& weight ) = 0;
>>>>>>> release/21.0.127

  };

}
#endif
