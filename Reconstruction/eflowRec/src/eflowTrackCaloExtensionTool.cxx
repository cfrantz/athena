/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/********************************************************************

 NAME:     eflowTrackCaloExtensionTool.cxx
 PACKAGE:  offline/Reconstruction/eflowRec

 AUTHORS:  T.Guttenberger
 CREATED:  19th September, 2014

 ********************************************************************/

#include <eflowRec/eflowTrackCaloExtensionTool.h>

#include "eflowRec/eflowTrackCaloPoints.h"
#include "eflowRec/eflowDepthCalculator.h"

#include "TrkParameters/TrackParameters.h"  // typedef
#include "TrkCaloExtension/CaloExtension.h"
#include "TrkParametersIdentificationHelpers/TrackParametersIdHelper.h"

#include "CaloDetDescr/CaloDepthTool.h"

#include "GaudiKernel/ListItem.h"

#include <map>
#include <vector>
#include <utility>

eflowTrackCaloExtensionTool::eflowTrackCaloExtensionTool(const std::string& type, const std::string& name, const IInterface* parent)  :
    AthAlgTool(type, name, parent),
<<<<<<< HEAD
    m_theTrackExtrapolatorTool("Trk::ParticleCaloExtensionTool",this),
    m_trackParametersIdHelper(std::make_unique<Trk::TrackParametersIdHelper>())
=======
    m_theTrackExtrapolatorTool("Trk::ParticleCaloExtensionTool"),
    m_trackParametersIdHelper(new Trk::TrackParametersIdHelper)//,
    //m_tracksProcessed(0)
>>>>>>> release/21.0.127
{
  declareInterface<eflowTrackExtrapolatorBaseAlgTool>(this);
  declareProperty("TrackCaloExtensionTool", m_theTrackExtrapolatorTool, "TrackCaloExtension Tool Handle");
}

eflowTrackCaloExtensionTool::~eflowTrackCaloExtensionTool() {
}

StatusCode eflowTrackCaloExtensionTool::initialize() {
  /* Tool service */
  IToolSvc* myToolSvc;
  if (service("ToolSvc", myToolSvc).isFailure()) {
<<<<<<< HEAD
    ATH_MSG_WARNING(" Tool Service Not Found");
=======
    msg(MSG::WARNING) << " Tool Service Not Found" << endmsg;
>>>>>>> release/21.0.127
    return StatusCode::SUCCESS;
  }

  if (m_theTrackExtrapolatorTool.retrieve().isFailure()) {
<<<<<<< HEAD
    ATH_MSG_WARNING("Cannot find Extrapolation tool "
		    << m_theTrackExtrapolatorTool.typeAndName());
    return StatusCode::SUCCESS;
  } else {
    ATH_MSG_VERBOSE("Successfully retrieved Extrapolation tool "
		    << m_theTrackExtrapolatorTool.typeAndName());
=======
    msg(MSG::WARNING) << "Cannot find Extrapolation tool "
    << m_theTrackExtrapolatorTool.typeAndName() << endmsg;
    return StatusCode::SUCCESS;
  } else {
    msg(MSG::INFO) << "Successfully retrieved Extrapolation tool "
    << m_theTrackExtrapolatorTool.typeAndName() << endmsg;
>>>>>>> release/21.0.127
  }

  if (!m_ParticleCacheKey.key().empty()) ATH_CHECK(m_ParticleCacheKey.initialize());
  else m_useOldCalo = true;
  
  return StatusCode::SUCCESS;
}

<<<<<<< HEAD
std::unique_ptr<eflowTrackCaloPoints> eflowTrackCaloExtensionTool::execute(const xAOD::TrackParticle* track) const {

  ATH_MSG_VERBOSE(" Now running eflowTrackCaloExtensionTool");
=======
eflowTrackCaloPoints* eflowTrackCaloExtensionTool::execute(const xAOD::TrackParticle* track) const {
  //++m_tracksProcessed;
  msg(MSG::VERBOSE) << " Now running eflowTrackCaloExtensionTool" << endmsg;
>>>>>>> release/21.0.127

  /*Create a map to index the TrackParameters at calo (owned by the extension) wrt to layers*/
  std::map<eflowCalo::LAYER, const Trk::TrackParameters*> parametersMap;

  /*get the CaloExtension object*/
  const Trk::CaloExtension * extension = nullptr;
  std::unique_ptr<Trk::CaloExtension> uniqueExtension;
  const int index = track->index();
  ATH_MSG_VERBOSE("Getting element " << index << " from the particleCache");

  if (m_useOldCalo) {
    /* If CaloExtensionBuilder is unavailable, use the calo extension tool */
    ATH_MSG_VERBOSE("Using the CaloExtensionTool");
    uniqueExtension = m_theTrackExtrapolatorTool->caloExtension(*track);
    extension = uniqueExtension.get();
  } else {
    /*get the CaloExtension object*/
    SG::ReadHandle<CaloExtensionCollection>  particleCache {m_ParticleCacheKey};
    ATH_MSG_VERBOSE("Using the CaloExtensionBuilder Cache");
    extension = (*particleCache)[index];
    ATH_MSG_VERBOSE("Getting element " << index << " from the particleCache");
    if( not extension ){
      ATH_MSG_VERBOSE("Cache does not contain a calo extension -> Calculating with the a CaloExtensionTool" );
      uniqueExtension = m_theTrackExtrapolatorTool->caloExtension(*track);
      extension = uniqueExtension.get();
    }
  }
  
  if (extension != nullptr) {

    /*extract the CurvilinearParameters*/
    const std::vector<Trk::CurvilinearParameters>& clParametersVector = extension->caloLayerIntersections();

     /*The parameters are owned by the CaloExtension so are handled by it the eflowTrackCaloPoints does
     * not take ownership */
    for ( const Trk::CurvilinearParameters& clParameter : clParametersVector) {
      if (parametersMap[getLayer(&clParameter)] == nullptr) {
        parametersMap[getLayer(&clParameter)] = &clParameter;
      } else if (m_trackParametersIdHelper->isEntryToVolume(clParameter.cIdentifier())) {
        parametersMap[getLayer(&clParameter)] = &clParameter;
      }
    }
    /*
      parametersMap may have several entries for Tile1,2,3.
      The impact is negligible as the eta/phi of these entries are very similar
      https://its.cern.ch/jira/browse/ATLJETMET-242
    */
    
    return std::make_unique<eflowTrackCaloPoints>(parametersMap);

  }
  else{
<<<<<<< HEAD
    if (track->pt() > 3*Gaudi::Units::GeV) ATH_MSG_WARNING("TrackExtension failed for track with pt and eta " << track->pt() << " and " << track->eta());
    parametersMap[eflowCalo::LAYER::Unknown] = nullptr;
    return std::make_unique<eflowTrackCaloPoints>(parametersMap);
=======
    msg(MSG::WARNING) << "TrackExtension failed for track with pt and eta " << track->pt() << " and " << track->eta() << endmsg;
    parametersMap[eflowCalo::LAYER::Unknown] = 0;
    return new eflowTrackCaloPoints(parametersMap);
>>>>>>> release/21.0.127
  }
}

StatusCode eflowTrackCaloExtensionTool::finalize() {
  return StatusCode::SUCCESS;
}

/*This function translates the information embedded within the CurvilinearParameters of the CaloExtension object into an eflowCaloLayer*/
eflowCalo::LAYER eflowTrackCaloExtensionTool::getLayer(const Trk::CurvilinearParameters* clParameter) const {
  unsigned int parametersIdentifier = clParameter->cIdentifier();

  /*Return unknown when the identifier is invalid */
  if (!m_trackParametersIdHelper->isValid(parametersIdentifier)) {
<<<<<<< HEAD
    ATH_MSG_ERROR("invalid Track Identifier");
=======
    msg(MSG::ERROR) << "invalid Track Identifier"<<endmsg;
>>>>>>> release/21.0.127
    return eflowCalo::LAYER::Unknown;
  };

  if(m_trackParametersIdHelper->isEntryToVolume(parametersIdentifier)) {
<<<<<<< HEAD
    ATH_MSG_VERBOSE("is Volume Entry");
 } else {
    ATH_MSG_VERBOSE("is Volume Exit");
=======
    msg(MSG::VERBOSE) << "is Volume Entry" << endmsg;
 } else {
    msg(MSG::VERBOSE) << "is Volume Exit" << endmsg;
>>>>>>> release/21.0.127
    }

  return eflowCalo::translateSampl(m_trackParametersIdHelper->caloSample(parametersIdentifier));
}
