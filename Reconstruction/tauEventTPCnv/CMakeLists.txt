# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( tauEventTPCnv )

<<<<<<< HEAD
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/DataModelAthenaPool
                          Database/AthenaPOOL/AthenaPoolCnvSvc
                          Database/AthenaPOOL/AthenaPoolUtilities
                          Database/AthenaPOOL/RootConversions
                          Event/EventCommonTPCnv
                          PhysicsAnalysis/AnalysisCommon/ParticleEventTPCnv
                          Reconstruction/tauEvent
                          Tracking/TrkEventCnv/TrkEventTPCnv
                          PRIVATE
                          Control/AthenaKernel )

>>>>>>> release/21.0.127
# External dependencies:
find_package( CLHEP )

# Component(s) in the package:
atlas_add_tpcnv_library( tauEventTPCnv
                         src/*.cxx
                         PUBLIC_HEADERS tauEventTPCnv
                         INCLUDE_DIRS ${CLHEP_INCLUDE_DIRS}
                         DEFINITIONS ${CLHEP_DEFINITIONS}
<<<<<<< HEAD
                         LINK_LIBRARIES ${CLHEP_LIBRARIES} DataModelAthenaPoolLib AthenaPoolCnvSvcLib AthenaPoolUtilities RootConversions EventCommonTPCnv ParticleEventTPCnv tauEvent TrkEventTPCnv
                         PRIVATE_LINK_LIBRARIES AthenaKernel )
=======
                         LINK_LIBRARIES ${ROOT_LIBRARIES} ${CLHEP_LIBRARIES} DataModelAthenaPoolLib AthenaPoolCnvSvcLib AthenaPoolUtilities RootConversions EventCommonTPCnv ParticleEventTPCnv tauEvent TrkEventTPCnv AthenaKernel )
>>>>>>> release/21.0.127

atlas_add_dictionary( tauEventTPCnvDict
                      tauEventTPCnv/tauEventTPCnvDict.h
                      tauEventTPCnv/selection.xml
<<<<<<< HEAD
                      LINK_LIBRARIES tauEventTPCnv )
=======
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} ${CLHEP_LIBRARIES} DataModelAthenaPoolLib AthenaPoolCnvSvcLib AthenaPoolUtilities RootConversions EventCommonTPCnv ParticleEventTPCnv tauEvent TrkEventTPCnv AthenaKernel tauEventTPCnv )

>>>>>>> release/21.0.127
