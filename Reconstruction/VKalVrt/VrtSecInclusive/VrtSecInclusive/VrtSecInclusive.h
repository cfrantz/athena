/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

// VKalVrt.h
//
#ifndef _VrtSecInclusive_VrtSecInclusive_H
#define _VrtSecInclusive_VrtSecInclusive_H


<<<<<<< HEAD
=======
#include "VrtSecInclusive/Constants.h"

>>>>>>> release/21.0.127
#include "AthenaBaseComps/AthAlgorithm.h"

// Gaudi includes
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ITHistSvc.h"
<<<<<<< HEAD
//
#include "TrkVKalVrtFitter/TrkVKalVrtFitter.h"
//#include "TrkTrack/TrackInfo.h"
//#include "TrkParameters/TrackParameters.h"

=======
#include "StoreGate/StoreGateSvc.h"
//
#include "TrkVKalVrtFitter/TrkVKalVrtFitter.h"
>>>>>>> release/21.0.127

// for truth
#include "GeneratorObjects/McEventCollection.h"

<<<<<<< HEAD
//#include "ParticleTruth/TrackParticleTruthCollection.h"
//-----
//#include "TrkEventUtils/InverseTruthMap.h"
//#include "TrkEventUtils/TruthCollectionInverter.h"
=======
>>>>>>> release/21.0.127
#include "TrkToolInterfaces/ITruthToTrack.h"
#include "ITrackToVertex/ITrackToVertex.h"
#include "TrkVertexFitterInterfaces/ITrackToVertexIPEstimator.h"
#include "TrkExInterfaces/IExtrapolator.h"
#include "TrkExInterfaces/IPropagator.h"
#include "TrkSurfaces/CylinderSurface.h"
#include "TrkDetDescrInterfaces/IVertexMapper.h"
<<<<<<< HEAD
=======
#include "GaudiKernel/ServiceHandle.h"
#include "InDetConditionsSummaryService/IInDetConditionsSvc.h"
#include "InDetIdentifier/PixelID.h"
#include "InDetIdentifier/SCT_ID.h"
>>>>>>> release/21.0.127

// xAOD Classes
#include "xAODEventInfo/EventInfo.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
<<<<<<< HEAD
=======
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/Muon.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/Electron.h"
>>>>>>> release/21.0.127

// Normal STL and physical vectors
#include <vector>
#include <deque>
<<<<<<< HEAD


class TH1D;

//using namespace std;
=======
#include <functional>


/** Forward declarations **/

class TH1;
>>>>>>> release/21.0.127

namespace Trk {
  class ITruthToTrack;
  class ITrackToVertex;
  class ITrackToVertexIPEstimator;
  class IExtrapolator;
  class IVertexMapper;
  struct MappedVertex;
}

namespace VKalVrtAthena {

  namespace GeoModel { enum GeoModel { Run1=1, Run2=2 }; }
  
  class IntersectionPos;
  class IntersectionPos_barrel;
  class IntersectionPos_endcap;
<<<<<<< HEAD
=======
  
  class NtupleVars;
}


namespace VKalVrtAthena {
>>>>>>> release/21.0.127
  
  class NtupleVars;
    
  class VrtSecInclusive : public AthAlgorithm {
  public:
    /** Standard Athena-Algorithm Constructor */
    VrtSecInclusive(const std::string& name, ISvcLocator* pSvcLocator);
    
    /** Default Destructor */
    ~VrtSecInclusive();

<<<<<<< HEAD
    virtual StatusCode initialize();
    virtual StatusCode finalize();
    virtual StatusCode execute();
    virtual StatusCode initEvent();
=======
    virtual StatusCode beginRun()   override;
    virtual StatusCode initialize() override;
    virtual StatusCode finalize()   override;
    virtual StatusCode execute()    override;
>>>>>>> release/21.0.127
   
    virtual StatusCode initEvent();
    
  private:
    
    /////////////////////////////////////////////////////////
    //
    //  Member Variables
    //
    
<<<<<<< HEAD
    // Physics Constants
    const double m_pi; // mass of charged pion
    const double m_e; // mass of electron
    const double m_proton; // mass of proton
    const double m_PI; // value of pi
    
    // Unit Conversion Constants
    const double m_cnv_xyz;  /* Conversion constants*/
    const double m_cnv_mom;  /* Conversion constants*/
    
    // Detector Geometry Constants
    const double m_avRad_bp;
    const double m_avRad_pix0;
    const double m_avRad_pix1;
    const double m_avRad_pix2;
    const double m_avRad_pix3;
    const double m_avRad_sct0;
    const double m_avRad_sct1;
    
    const double m_ec_pix_rmin;
    const double m_ec_pix_rmax;
    const double m_ec_pix0;
    const double m_ec_pix1;
    const double m_ec_pix2;
    
    const double m_ec_sct_rmin;
    const double m_ec_sct_rmax;
    const double m_ec_sct0_rmin;
    const double m_ec_sct0;
    const double m_ec_sct1;
    const double m_ec_sct2;
    const double m_ec_sct3;
    const double m_ec_sct4;
    
    // xAOD Accessors
    const xAOD::VertexContainer*        m_vertexTES; // primary vertex container
    
    /////////////////////////////////////////////////////////
    //
    //  Athena JobOption Properties
    //
    std::string          m_TrackLocation;
    std::string          m_PrimVrtLocation;
    std::string          m_SecVrtLocation;
    std::string          m_truthParticleContainerName;
    std::string          m_mcEventContainerName;
    std::vector<double>  m_BeamPosition;
    
    // JO: GeoModel
    int    m_geoModel;
    
    // JO: Analysis Cut Variables
    bool   m_ImpactWrtBL;
    double m_a0TrkPVDstMinCut;
    double m_a0TrkPVDstMaxCut;
    double m_zTrkPVDstMinCut;
    double m_zTrkPVDstMaxCut;
    double m_a0TrkPVSignifCut;
    double m_zTrkPVSignifCut;
    
    double               m_TrkChi2Cut;
    double               m_SelVrtChi2Cut;
    double               m_TrkPtCut;
    int 		 m_CutSctHits;
    int 		 m_CutPixelHits;
    int 		 m_CutSiHits;
    int			 m_CutBLayHits;
    int 		 m_CutSharedHits;
    int 		 m_CutTRTHits; // Kazuki
    double 		 m_VertexMergeFinalDistCut; // Kazuki
    double		 m_A0TrkErrorCut;
    double		 m_ZTrkErrorCut;
    double               m_VertexMergeCut;
    double               m_TrackDetachCut;
    bool                 m_FillHist;
    bool                 m_FillNtuple;
    double               m_LoPtCut; // also check for truth vertices with truth tracks passing LoPtCut
    unsigned int         m_SelTrkMaxCutoff;
    bool                 m_doIntersectionPos;
    bool                 m_doMapToLocal;
    
    // Internal statuses of the algorithm
    bool                 m_SAloneTRT;
    bool                 m_doTRTPixCut; // Kazuki
    bool                 m_mergeFinalVerticesDistance; // Kazuki
    bool                 m_doTruth;
    bool                 m_removeFakeVrt;
    double               m_mcTrkResolution;
    double               m_TruthTrkLen;
    bool                 m_extrapPV; //extrapolate reco and prim truth trks to PV (for testing only)
    const xAOD::Vertex*  m_thePV;
    
    
    ToolHandle < Trk::ITrkVKalVrtFitter >  m_fitSvc;       // VKalVrtFitter tool
    ToolHandle <Trk::ITruthToTrack>        m_truthToTrack; // tool to create trkParam from genPart
    
    /** get a handle on the Track to Vertex tool */
    ToolHandle< Reco::ITrackToVertex > m_trackToVertexTool;
    ToolHandle<Trk::ITrackToVertexIPEstimator> m_trackToVertexIPEstimatorTool;
    ToolHandle<Trk::IExtrapolator> m_extrapolator;
    ToolHandle<Trk::IVertexMapper> m_vertexMapper;
    
    // Histograms for stats
    TH1D* m_hb_massPiPi;
    TH1D* m_hb_2Ddist;
    TH1D* m_hb_massEE;
    TH1D* m_hb_nvrt2;
    TH1D* m_hb_ratio;
    TH1D* m_trkSelCuts;
=======
    struct JobProperties {
      // JO: GeoModel
      int    geoModel;
   
      std::string          TrackLocation;
      std::string          PrimVrtLocation;
      std::string          truthParticleContainerName;
      std::string          mcEventContainerName;
      
      std::string all2trksVerticesContainerName;
      std::string secondaryVerticesContainerName;
      
      // common feature flags
      bool   doTruth;
      bool   FillHist;
      bool   FillNtuple;
      bool   FillIntermediateVertices;
      bool   doIntersectionPos;
      bool   doMapToLocal;
      bool   extrapPV; //extrapolate reco and prim truth trks to PV (for testing only)
      
      // track selection conditions
      unsigned int SelTrkMaxCutoff;
      bool  SAloneTRT;
      
      /* impact parameters */
      bool   do_PVvetoCut;
      bool   do_d0Cut;
      bool   do_z0Cut;
      bool   do_d0errCut;
      bool   do_z0errCut;
      bool   do_d0signifCut;
      bool   do_z0signifCut;
      
      bool   ImpactWrtBL;
      double d0TrkPVDstMinCut;
      double d0TrkPVDstMaxCut;
      double d0TrkPVSignifCut;
      double z0TrkPVDstMinCut;
      double z0TrkPVDstMaxCut;
      double z0TrkPVSignifCut;
      double d0TrkErrorCut;
      double z0TrkErrorCut;
      
      /* pT anc chi2 */
      double TrkChi2Cut;
      double TrkPtCut;
      
      /* hit requirements */
      bool doTRTPixCut; // Kazuki
      int  CutSctHits;
      int  CutPixelHits;
      int  CutSiHits;
      int  CutBLayHits;
      int  CutSharedHits;
      int  CutTRTHits; // Kazuki
      
      // Vertex reconstruction
      bool   doPVcompatibilityCut;
      bool   removeFakeVrt;
      bool   removeFakeVrtLate;
      bool   doReassembleVertices;
      bool   doMergeByShuffling;
      bool   doSuggestedRefitOnMerging;
      bool   doMagnetMerging;
      bool   doWildMerging;
      bool   doMergeFinalVerticesDistance; // Kazuki
      bool   doAssociateNonSelectedTracks;
      bool   doFinalImproveChi2;
      double pvCompatibilityCut;
      double SelVrtChi2Cut;
      double VertexMergeFinalDistCut; // Kazuki
      double VertexMergeFinalDistScaling;
      double VertexMergeCut;
      double TrackDetachCut;
      
      
      double associateMinDistanceToPV;
      double associateMaxD0;
      double associateMaxZ0;
      double associatePtCut;
      double associateChi2Cut;
      
      double reassembleMaxImpactParameterD0;
      double reassembleMaxImpactParameterZ0;
      double mergeByShufflingMaxSignificance;
      double mergeByShufflingAllowance;
      
      double improveChi2ProbThreshold;
      
      // vertexing using muons (test implementation)
      bool doSelectTracksFromMuons;
      bool doSelectTracksFromElectrons;
      
      // Additional dressing option
      bool doAugmentDVimpactParametersToMuons;     // potentially useful for DV + muon search
      bool doAugmentDVimpactParametersToElectrons; // potentially useful for analyses involving electrons
      
      // MC truth
      double               mcTrkResolution;
      double               TruthTrkLen;
      
    };
    
    struct JobProperties m_jp;
    
    // xAOD Accessors
    const xAOD::VertexContainer*  m_primaryVertices;
    xAOD::Vertex*                 m_thePV;
    std::unique_ptr<std::vector<const xAOD::TrackParticle*> > m_selectedTracks;
    std::unique_ptr<std::vector<const xAOD::TrackParticle*> > m_associatedTracks;
    
    std::vector<double>  m_BeamPosition;
    
    /////////////////////////////////////////////////////////
    //
    //  Athena JobOption Properties
    //
    
    ToolHandle <Trk::ITrkVKalVrtFitter>        m_fitSvc;       // VKalVrtFitter tool
    ToolHandle <Trk::ITruthToTrack>            m_truthToTrack; // tool to create trkParam from genPart
    
    /** get a handle on the Track to Vertex tool */
    ToolHandle< Reco::ITrackToVertex >         m_trackToVertexTool;
    ToolHandle<Trk::ITrackToVertexIPEstimator> m_trackToVertexIPEstimatorTool;
    ToolHandle<Trk::IExtrapolator>             m_extrapolator;
    ToolHandle<Trk::IVertexMapper>             m_vertexMapper;
    
    /** Condition service **/
    ServiceHandle <IInDetConditionsSvc> m_pixelCondSummarySvc;
    ServiceHandle <IInDetConditionsSvc> m_sctCondSummarySvc;
    
    const AtlasDetectorID* m_atlasId;
    const PixelID* m_pixelId;
    const SCT_ID*  m_sctId;
    
    std::string m_checkPatternStrategy;
    using PatternStrategyFunc = bool (VrtSecInclusive::*) ( const xAOD::TrackParticle *trk, const Amg::Vector3D& vertex );
    std::map<std::string, PatternStrategyFunc> m_patternStrategyFuncs;
    
>>>>>>> release/21.0.127
    
    //////////////////////////////////////////////////////////////////////////////////////
    //
    // define ntuple variables here
    //
    
    // The standard AANT, CollectionTree, is bare bones
    TTree      *m_tree_Vert; 
<<<<<<< HEAD
    NtupleVars *m_ntupleVars;
    
    
    // to get truthParticle from Reco. particle
    const xAOD::TrackParticleContainer* m_importedTrkColl;        
    const xAOD::TruthParticleContainer* m_importedTrkTruthColl;        
    const xAOD::TruthEventContainer* m_importedFullTruthColl;
    const xAOD::TrackParticle* m_TrkColl;
=======
    std::unique_ptr<NtupleVars> m_ntupleVars;
    
    // Histograms for stats
    std::map<std::string, TH1*> m_hists;
>>>>>>> release/21.0.127
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    // 
    // Private member functions
    //
    
    // for event info to new ntuple (used to go by default in CollectionTree)
    void declareProperties();
    
    StatusCode addEventInfo(); 
    StatusCode setupNtupleVariables();
    StatusCode setupNtuple();
    StatusCode clearNtupleVariables();
    StatusCode deleteNtupleVariables();
    StatusCode processPrimaryVertices();
    StatusCode fillAANT_SelectedBaseTracks();
    StatusCode fillAANT_SecondaryVertices( xAOD::VertexContainer* );
    
<<<<<<< HEAD
    //    StatusCode m_scRes;
    //
    struct WrkVrt { 
      bool Good;
      std::deque<long int> SelTrk;
      Amg::Vector3D        vertex;
      TLorentzVector       vertexMom;
      long int             Charge;
      std::vector<double>  vertexCov;
      std::vector<double>  Chi2PerTrk;
      std::vector< std::vector<double> > TrkAtVrt; 
      double               Chi2;
      int                  nCloseVrt;
      double               dCloseVrt;
    };
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    // 
    // Vertexing Algorithm Tool Member Functions
    //
    
    StatusCode  SelGoodTrkParticle( const xAOD::TrackParticleContainer* );

    StatusCode extractIncompatibleTracks( std::vector<int>& );
    StatusCode reconstruct2TrackVertices( std::vector<int>&, std::vector<WrkVrt>* );
    StatusCode reconstructNTrackVertices( std::vector<WrkVrt>* );
    StatusCode mergeFinalVertices( std::vector<WrkVrt>* ); // Kazuki
    StatusCode refitAndSelectGoodQualityVertices( std::vector<WrkVrt>* );
    
    void printWrkSet(const std::vector<WrkVrt> *WrkVrtSet, const std::string name);

    StatusCode DisassembleVertex(std::vector<WrkVrt> *WrkVrtSet, int , 
				 const xAOD::TrackParticleContainer* );
    
    void TrackClassification(std::vector< WrkVrt >* , 
                             std::vector< std::deque<long int> >* );
    
    double MaxOfShared(std::vector<WrkVrt> *, 
                       std::vector< std::deque<long int> >*,
	               long int & ,long int & );

    void RemoveTrackFromVertex(std::vector<WrkVrt> *, 
=======
    //
    struct WrkVrt { 
      bool isGood;                                    //! flaged true for good vertex candidates
      std::deque<long int> selectedTrackIndices;      //! list if indices in TrackParticleContainer for selectedBaseTracks
      std::deque<long int> associatedTrackIndices;    //! list if indices in TrackParticleContainer for associatedTracks
      Amg::Vector3D        vertex;                    //! VKalVrt fit vertex position
      TLorentzVector       vertexMom;                 //! VKalVrt fit vertex 4-momentum
      std::vector<double>  vertexCov;                 //! VKalVrt fit covariance
      double               Chi2;                      //! VKalVrt fit chi2 result
      std::vector<double>  Chi2PerTrk;                //! list of VKalVrt fit chi2 for each track
      long int             Charge;                    //! total charge of the vertex
      std::vector< std::vector<double> > TrkAtVrt;    //! list of track parameters wrt the reconstructed vertex
      unsigned long        closestWrkVrtIndex;        //! stores the index of the closest WrkVrt in std::vector<WrkVrt>
      double               closestWrkVrtValue;        //! stores the value of some observable to the closest WrkVrt ( observable = e.g. significance )
      
      inline double ndof() const { return 2.0*( selectedTrackIndices.size() + associatedTrackIndices.size() ) - 3.0; }
      inline unsigned nTracksTotal() const { return selectedTrackIndices.size() + associatedTrackIndices.size(); }
    };
    
    
    using Detector = int;
    using Bec      = int;
    using Layer    = int;
    using Flag     = int;
    using ExtrapolatedPoint   = std::tuple<const TVector3, Detector, Bec, Layer, Flag>;
    using ExtrapolatedPattern = std::vector< ExtrapolatedPoint >;
    
    std::vector< std::pair<int, int> > m_incomp;
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    ///
    /// Vertexing Algorithm Member Functions
    ///
    
    /** select tracks which become seeds for vertex finding */
    StatusCode selectTracks();
    StatusCode selectTracksFromMuons();
    StatusCode selectTracksFromElectrons();
    
    using TrackSelectionAlg = StatusCode (VrtSecInclusive::*)();
    std::vector<TrackSelectionAlg> m_trackSelectionAlgs;
    
    /** track-by-track selection strategies */
    bool selectTrack_notPVassociated ( const xAOD::TrackParticle* ) const;
    bool selectTrack_pTCut           ( const xAOD::TrackParticle* ) const;
    bool selectTrack_chi2Cut         ( const xAOD::TrackParticle* ) const;
    bool selectTrack_hitPattern      ( const xAOD::TrackParticle* ) const;
    bool selectTrack_d0Cut           ( const xAOD::TrackParticle* ) const;
    bool selectTrack_z0Cut           ( const xAOD::TrackParticle* ) const;
    bool selectTrack_d0errCut        ( const xAOD::TrackParticle* ) const;
    bool selectTrack_z0errCut        ( const xAOD::TrackParticle* ) const;
    bool selectTrack_d0signifCut     ( const xAOD::TrackParticle* ) const;
    bool selectTrack_z0signifCut     ( const xAOD::TrackParticle* ) const;
    
    /** related to the graph method and verte finding */
    StatusCode extractIncompatibleTrackPairs( std::vector<WrkVrt>* );
    StatusCode findNtrackVertices(std::vector<WrkVrt>* );
    StatusCode rearrangeTracks( std::vector<WrkVrt>* );
    
    /** attempt to merge vertices when all tracks of a vertex A is close to vertex B in terms of impact parameter */
    StatusCode reassembleVertices( std::vector<WrkVrt>* );
    
    /** attempt to merge splitted vertices when they are significantly distant
        due to the long-tail behavior of the vertex reconstruction resolution */
    StatusCode mergeByShuffling( std::vector<WrkVrt>* );
    
    /** attempt to merge vertices by lookng at the distance between two vertices */
    StatusCode mergeFinalVertices( std::vector<WrkVrt>* ); // Kazuki
    
    /** in addition to selected tracks, associate as much tracks as possible */
    StatusCode associateNonSelectedTracks( std::vector<WrkVrt>* );
    
    /** finalization of the vertex and store to xAOD::VertexContainer */
    StatusCode refitAndSelectGoodQualityVertices( std::vector<WrkVrt>* );
    
    using vertexingAlg = StatusCode (VrtSecInclusive::*)( std::vector<WrkVrt>* );
    std::vector< std::pair<std::string, vertexingAlg> > m_vertexingAlgorithms;
    unsigned m_vertexingAlgorithmStep;
    
    
    ////////////////////////////////////////////////////////////////////////////////////////
    // 
    // Supporting utility functions
    
    /** print the contents of reconstructed vertices */
    void printWrkSet(const std::vector<WrkVrt> *WrkVrtSet, const std::string name);

    /** refit the vertex. */
    StatusCode refitVertex( WrkVrt& );
    
    /** refit the vertex with suggestion */
    StatusCode refitVertexWithSuggestion( WrkVrt&, const Amg::Vector3D& );
    
    /** attempt to improve the vertex chi2 by removing the most-outlier track one by one until 
        the vertex chi2 satisfies a certain condition. */
    double improveVertexChi2( WrkVrt& );
    
    void removeTrackFromVertex(std::vector<WrkVrt>*, 
>>>>>>> release/21.0.127
                               std::vector< std::deque<long int> > *,
			       const long int & ,const long int & );
 
<<<<<<< HEAD
    int   nTrkCommon( std::vector<WrkVrt> *WrkVrtSet, int V1, int V2) const;
    
    double minVrtVrtDist( std::vector<WrkVrt> *WrkVrtSet, int & V1, int & V2);
    
    double minVrtVrtDistNext( std::vector<WrkVrt> *WrkVrtSet, int & V1, int & V2);
    
    void MergeVertices( std::vector<WrkVrt> *WrkVrtSet, int & V1, int & V2);
    
    double VrtVrtDist(const Amg::Vector3D & Vrt1, const std::vector<double>  & VrtErr1,
		      const Amg::Vector3D & Vrt2, const std::vector<double>  & VrtErr2);

    double improveVertexChi2( std::vector<WrkVrt> *WrkVrtSet, int V, 
			      const xAOD::TrackParticleContainer* AllTrackList);
    
    template<class Track> void getIntersection(Track *trk, std::vector<IntersectionPos*>& layers, const Trk::Perigee* per);
    template<class Track> void setIntersection(Track *trk, IntersectionPos *bec, const Trk::Perigee* per);
    
    StatusCode CutTrk(double PInvVert,double ThetaVert,double A0Vert, double Zvert, double Chi2, 
		      long int PixelHits,long int SctHits,long int SharedHits, long int BLayHits, long int TRTHits);

    const Trk::Perigee* GetPerigee( const xAOD::TrackParticle* i_ntrk);
    
    StatusCode RefitVertex( WrkVrt& WrkVrt, const xAOD::TrackParticleContainer* );
    StatusCode RefitVertex( WrkVrt& WrkVrt, const xAOD::TrackParticleContainer*,
                            Trk::IVKalState& istate);
    
    void  FillCovMatrix(int iTrk, std::vector<double> & Matrix, AmgSymMatrix(5)& );
    
=======
    StatusCode disassembleVertex(std::vector<WrkVrt> *, const unsigned& vertexIndex );
    
    void trackClassification(std::vector< WrkVrt >* , std::map< long int, std::vector<long int> >& );
    
    double findWorstChi2ofMaximallySharedTrack(std::vector<WrkVrt>*, std::map< long int, std::vector<long int> >&, long int & ,long int & );
    
    /** returns the number of tracks commonly present in both vertices */
    size_t nTrkCommon( std::vector<WrkVrt> *WrkVrtSet, const std::pair<unsigned, unsigned>& pairIndex ) const;
    
    /** calculate the significance (Mahalanobis distance) between two reconstructed vertices */
    double significanceBetweenVertices( const WrkVrt&, const WrkVrt& ) const;
    
    /** calculate the physical distance */
    double distanceBetweenVertices( const WrkVrt&, const WrkVrt& ) const;
    
    using AlgForVerticesPair = double (VrtSecInclusive::*)( const WrkVrt&, const WrkVrt& ) const;
    
    /** returns the pair of vertices that give minimum in terms of some observable (e.g. distance, significance) */
    double findMinVerticesPair( std::vector<WrkVrt>*, std::pair<unsigned, unsigned>&, AlgForVerticesPair );
    
    /** returns the next pair of vertices that give next-to-minimum distance significance */
    double findMinVerticesNextPair( std::vector<WrkVrt>*, std::pair<unsigned, unsigned>& );
    
    /** the 2nd vertex is merged into the 1st vertex. A destructive operation. */
    StatusCode mergeVertices( WrkVrt& destination, WrkVrt& source );
    
    enum mergeStep { RECONSTRUCT_NTRK, REASSEMBLE, SHUFFLE1, SHUFFLE2, SHUFFLE3, FINAL };
>>>>>>> release/21.0.127
    
    typedef struct track_summary_properties {
      uint8_t numIBLHits;
      uint8_t numBLayerHits;
      uint8_t numPixelLayer1_Hits;
      uint8_t numPixelLayer2_Hits;
      uint8_t numPixelDisk0_Hits;
      uint8_t numPixelDisk1_Hits;
      uint8_t numPixelDisk2_Hits;
      uint8_t numPixelHits;
      uint8_t numSctBarrelLayer0_Hits;
      uint8_t numSctBarrelLayer1_Hits;
      uint8_t numSctBarrelLayer2_Hits;
      uint8_t numSctBarrelLayer3_Hits;
      uint8_t numSctEC0_Hits;
      uint8_t numSctEC1_Hits;
      uint8_t numSctEC2_Hits;
      uint8_t numSctEC3_Hits;
      uint8_t numSctEC4_Hits;
      uint8_t numSctHits;
      uint8_t numTrtHits;
    } track_summary;
   
<<<<<<< HEAD
    void fillTrackSummary( track_summary& summary, const xAOD::TrackParticle *trk );
    bool passedFakeReject( const Amg::Vector3D& FitVertex, const xAOD::TrackParticle *itrk, const xAOD::TrackParticle *jtrk );
    
   
=======
    /** cretrieve the track hit information */
    void fillTrackSummary( track_summary& summary, const xAOD::TrackParticle *trk );
    
    ExtrapolatedPattern* extrapolatedPattern( const xAOD::TrackParticle* );
   
    /** A classical method with hard-coded geometry */
    bool checkTrackHitPatternToVertex( const xAOD::TrackParticle *trk, const Amg::Vector3D& vertex );
    
    /** New method with track extrapolation */
    bool checkTrackHitPatternToVertexByExtrapolation( const xAOD::TrackParticle *trk, const Amg::Vector3D& vertex );
    
    /** Flag false if the consistituent tracks are not consistent with the vertex position */
    bool passedFakeReject( const Amg::Vector3D& FitVertex, const xAOD::TrackParticle *itrk, const xAOD::TrackParticle *jtrk );
    
    /** Remove inconsistent tracks from vertices */
    void removeInconsistentTracks( WrkVrt& );
   
    template<class Track> void getIntersection(Track *trk, std::vector<IntersectionPos*>& layers, const Trk::Perigee* per);
    template<class Track> void setIntersection(Track *trk, IntersectionPos *bec, const Trk::Perigee* per);
    
    /** monitor the intermediate status of vertexing */
    StatusCode monitorVertexingAlgorithmStep( std::vector<WrkVrt>*, const std::string name, bool final = false );
    
>>>>>>> release/21.0.127
    ////////////////////////////////////////////////////////////////////////////////////////
    // 
    // Truth Information Algorithms Member Functions
    //
    // 
    
<<<<<<< HEAD
    // new version of truth routine - MCEventCollection (GEN_AOD or TruthEvent (ESD)
    StatusCode getNewTruthInfo();
    
    //
    const xAOD::TruthParticle *getTrkGenParticle(const xAOD::TrackParticle* /*, double& matchProb*/) const;
    
    
    StatusCode categorizeVertexTruthTopology( xAOD::Vertex *vertex );
      
    
    ////////////////////////////////////////////////////////////////////////////////////////
    // 
    // xAOD Accessors Member Functions
    //
    void setMcEventCollection( const xAOD::TruthEventContainer*); 
    void setTrackParticleTruthCollection( const xAOD::TruthParticleContainer*);        
    void setTrackParticleContainer( const xAOD::TrackParticleContainer*);

=======
    const xAOD::TruthParticle *getTrkGenParticle(const xAOD::TrackParticle*) const;
    
    StatusCode categorizeVertexTruthTopology( xAOD::Vertex *vertex );
    
    ////////////////////////////////////////////////////////////////////////////////////////
    // 
    // Additional augmentation
    //
    // 
    
    StatusCode augmentDVimpactParametersToMuons();
    StatusCode augmentDVimpactParametersToElectrons();
    
>>>>>>> release/21.0.127
  };
  
} // end of namespace bracket


// This header file contains the definition of member templates
#include "details/Utilities.h"


#endif /* _VrtSecInclusive_VrtSecInclusive_H */
