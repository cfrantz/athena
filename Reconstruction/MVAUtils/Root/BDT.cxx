/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "MVAUtils/BDT.h"
#include "MVAUtils/ForestTMVA.h"
#include "MVAUtils/ForestLGBM.h"
#include "MVAUtils/ForestXGBoost.h"

#include "TTree.h"
#include <cmath>

#include <memory>
#include <set>
#include <sstream>
#include <stack>
#include <stdexcept>
#include <string>
#include <utility>

<<<<<<< HEAD
using namespace MVAUtils;
=======
/** c-tor from TTree **/
BDT::BDT(TTree *tree)
  : m_sumWeights(0)
{
    std::vector<int> *vars = 0;
    std::vector<float> *values = 0;
    
    tree->SetBranchAddress("offset", &m_offset);
    tree->SetBranchAddress("vars", &vars);
    tree->SetBranchAddress("values", &values);
    
    for (int i = 0; i < tree->GetEntries(); ++i)
    {
	tree->GetEntry(i);
	assert (vars);
	assert (values);
	m_forest.push_back(m_nodes.size());
	newTree(*vars, *values);
	m_weights.push_back(m_offset);
	m_sumWeights+=m_offset;
    }
    
    m_offset = m_weights[0];//original use of m_offset

    delete vars;
    delete values;

    // // For Debug
    // std::cout << "Constructed from a TTree" << std::endl;
    // PrintForest();
>>>>>>> release/21.0.127

namespace{

<<<<<<< HEAD
/*  utility functions : to split option (e.g. "creator=lgbm;node=lgbm_simple")
*  in a std::map {{"creator", "lgbm"}, {"node", "lgbm_simple"}}
*/
std::string get_default_string_map(const std::map <std::string, std::string> & m,
                                   const std::string& key, const std::string & defval="")
{
   std::map<std::string, std::string>::const_iterator it = m.find(key);
   if (it == m.end()) { return defval; }
   return it->second;
=======
/** c-tor from TMVA::MethodBDT **/
BDT::BDT(TMVA::MethodBDT* bdt, bool isRegression, bool useYesNoLeaf)
 : m_sumWeights(0)
{
    assert(bdt);
    m_offset = bdt->GetBoostWeights().size() ? bdt->GetBoostWeights()[0] : 0.;
    std::vector<TMVA::DecisionTree*>::const_iterator it;
    for(it = bdt->GetForest().begin(); it != bdt->GetForest().end(); ++it) {
      m_forest.push_back(m_nodes.size());
      uint index=it - bdt->GetForest().begin();
      if( bdt->GetBoostWeights().size() > index ) {
	m_weights.push_back( bdt->GetBoostWeights()[index]);
	m_sumWeights+=m_weights.back();
      }
      else m_weights.push_back(0);
      newTree((*it)->GetRoot(), isRegression, useYesNoLeaf);      
    }
    // // For Debug
    // std::cout << "Constructed from a MethodBDT" << std::endl;
    // PrintForest();
>>>>>>> release/21.0.127
}

std::map<std::string, std::string> parseOptions(const std::string& raw_options)
{
<<<<<<< HEAD
  std::stringstream ss(raw_options);
  std::map<std::string, std::string> options;
  std::string item;
  while (std::getline(ss, item, ';')) {
    auto pos = item.find_first_of('=');
    const auto right = item.substr(pos+1);
    const auto left = item.substr(0, pos);
    if (!options.insert(std::make_pair(left, right)).second)
    {
      throw std::runtime_error(std::string("option ") + left +
                               " duplicated in title of TTree used as input");
=======
  assert ( vars.size() == values.size());

  // parent index is relative to root of tree (and only used inside this function)
  // right index is relative to the then processed node
  std::vector<Node::index_t> right(vars.size());
  std::stack<Node::index_t> parent; // not strictly parent if doing a right node

  parent.push(-1);
  for (size_t i = 0; i < vars.size(); ++i) {
    if (vars.at(i) >= 0) { // not a leaf
      parent.push(i);
    } else {
      // a leaf
      auto currParent = parent.top();
      // if right has not been visited, next will be right
      if (currParent >= 0) {
	right[currParent] = i+1-currParent;
      }
      parent.pop();
    }
  }

  for (size_t i = 0; i < vars.size(); ++i) {
    //std::cout << "    i = " << i << ", vars = " << vars[i] << ", values = " << values[i] << ", right = " <<  right[i] << std::endl;
    m_nodes.emplace_back(vars[i], values[i], right[i]);
  }
}

/**
 * Creates the full tree structure from TMVA::DecisionTree node.
 **/
void BDT::newTree(const TMVA::DecisionTreeNode *node, bool isRegression, bool useYesNoLeaf)
{

  // index is relative to the current node
  std::vector<Node::index_t> right;
  {

    // not strictly parent if doing a right node
    std::stack<const TMVA::DecisionTreeNode *> parent; 
    std::stack<Node::index_t> parentIndex;
    
    parentIndex.push(-1);
    parent.push(nullptr);
    
    auto currNode = node;
    int i = -1;
    while (currNode) {
      ++i;
      right.push_back(-1);
      if (!currNode->GetLeft()){
	// a leaf
	auto currParent = parent.top();
	auto currParentIndex = parentIndex.top();
	// if right has not been visited, next will be right
	if (currParentIndex >= 0) {
	  right[currParentIndex] = i + 1 - currParentIndex;
	  currNode = currParent->GetCutType() ? currParent->GetLeft() : currParent->GetRight();
	} else {
	  currNode = nullptr;
	}
	parent.pop();
	parentIndex.pop();
      } else {
	// not a leaf
	parent.push(currNode);
	parentIndex.push(i);
	currNode = currNode->GetCutType() ? currNode->GetRight() : currNode->GetLeft();
      }
    }
  }
  {
    std::stack<const TMVA::DecisionTreeNode *> parent; // not strictly parent if doing a right node
    
    parent.push(nullptr);

    auto currNode = node;
    int i = -1;
    while (currNode) {
      ++i;
      if (!currNode->GetLeft()){
	// a leaf
	m_nodes.emplace_back(-1, 
			     isRegression ? 
			     currNode->GetResponse() : useYesNoLeaf ? currNode->GetNodeType() : currNode->GetPurity(), 
			     right[i]);
	auto currParent = parent.top();
	// if right has not been visited, next will be right
	if (currParent) {
	  currNode = currParent->GetCutType() ? currParent->GetLeft() : currParent->GetRight();
	} else {
	  currNode = nullptr;
	}
	parent.pop();
      } else {
	// not a leaf
	parent.push(currNode);
	m_nodes.emplace_back(currNode->GetSelector(), currNode->GetCutValue(), right[i]);
	
	currNode = currNode->GetCutType() ? currNode->GetRight() : currNode->GetLeft();
      }
>>>>>>> release/21.0.127
    }
  }

  return options;
}
}

<<<<<<< HEAD
/** c-tor from TTree **/
BDT::BDT(::TTree *tree)
=======
float BDT::GetClassification(const std::vector<float*>& pointers) const
{
    float result = 0;
    for (auto it = m_forest.begin(); it != m_forest.end(); ++it){
      uint index = it-m_forest.begin();
      result += GetTreeResponse(pointers, *it) * m_weights[index];
    }
    return result/m_sumWeights;
}

/** Return 2.0/(1.0+exp(-2.0*sum))-1, with no offset  **/
float BDT::GetGradBoostMVA(const std::vector<float>& values) const
{
  float sum = 0; // ignores the offset
  for (auto it = m_forest.begin(); it != m_forest.end(); ++it)
    sum += GetTreeResponse(values, *it);
  return 2./(1+exp(-2*sum))-1;//output shaping for gradient boosted decision tree (-1,1)
}

/** Return 2.0/(1.0+exp(-2.0*sum))-1, with no offset  **/
float BDT::GetGradBoostMVA(const std::vector<float*>& pointers) const
{
  float sum = 0; // ignores the offset
  for (auto it = m_forest.begin(); it != m_forest.end(); ++it)
    sum += GetTreeResponse(pointers, *it);
  return 2./(1+exp(-2*sum))-1;//output shaping for gradient boosted decision tree (-1,1)
}

/** special function when there are mutliple classes (for b-tagging) **/
std::vector<float> BDT::GetMultiResponse(const std::vector<float>& values, 
					 unsigned int numClasses) const
{
  std::vector<float> sum;
  std::vector<float> v_out;
  if (numClasses > 0) {
    sum.resize(numClasses); // ignores the offset
    for (unsigned int i = 0; i < m_forest.size(); ++i) {
      sum[i%numClasses] += GetTreeResponse(values, m_forest[i]);
    }

    for (unsigned icl=0; icl < numClasses; icl++) {
      float norm=0;
      for (unsigned jcl=0; jcl < numClasses; jcl++) {
    	if (icl!=jcl) norm += exp(sum[jcl]-sum[icl]);
      }
      v_out.push_back(1/(1+norm));
    }
  }
  return v_out;
}

/** special function when there are mutliple classes (for b-tagging) **/
std::vector<float> BDT::GetMultiResponse(const std::vector<float*>& pointers, 
					      unsigned int numClasses) const
>>>>>>> release/21.0.127
{
  // at runtime decide which flavour of BDT we need to build
  // the information is coming from the title of the TTree
  std::map<std::string, std::string> options = parseOptions(tree->GetTitle());
  std::string creator = get_default_string_map(options, std::string("creator"));
  if (creator == "lgbm")
  {
    std::string node_type = get_default_string_map (options, std::string("node_type"));
    if (node_type == "lgbm") {
      m_forest = std::make_unique<ForestLGBM>(tree);
    } else if (node_type == "lgbm_simple") {
      m_forest = std::make_unique<ForestLGBMSimple>(
        tree); // this do not support nan as inputs
    } else {
      throw std::runtime_error(
        "the title of the input tree is misformatted: cannot understand which "
        "BDT implementation to use");
    }
  } else if (creator == "xgboost") {
    // this do support nan as inputs
    m_forest = std::make_unique<ForestXGBoost>(tree);
  } else {
    // default for compatibility: old TTree (based on TMVA) don't have a special title
    m_forest = std::make_unique<ForestTMVA>(tree);
  }
}


<<<<<<< HEAD
TTree* BDT::WriteTree(TString name) const { return m_forest->WriteTree(std::move(name)); }
void BDT::PrintForest() const { m_forest->PrintForest(); }
void BDT::PrintTree(unsigned int itree) const { m_forest->PrintTree(itree); }
=======
    auto forSize = m_forest.size();
    auto nodeSize = m_nodes.size();
    for (size_t i = 0; i < forSize; ++i) {
      vars.clear();
      values.clear();
      auto beg = m_forest[i];
      auto end = static_cast<Node::index_t>(nodeSize);
      if (i+1 < forSize) end = m_forest[i+1];
      for(auto j = beg; j < end; ++j) {
	vars.push_back(m_nodes[j].GetVar());
	values.push_back(m_nodes[j].GetVal());
      }
      m_offset = m_weights[i];
      tree->Fill();
    }	
    return tree;
}

/** For debugging only:
  * print the forest
  **/
void BDT::PrintForest() const
{
  std::cout << "***BDT: Printing entire forest***" << std::endl; 
  std::cout << "offset: " << m_offset << std::endl; 
  for (size_t i = 0; i < m_forest.size(); i++) {
    std::cout << "Tree number: " << i << std::endl; 
    PrintTree(m_forest[i]);
  }
    
}

/** For debugging only:
  * Print the tree in a way that can compare implementations
  * Using pre-order search for now
  **/
void BDT::PrintTree(Node::index_t index) const
{
  std::stack<Node::index_t> s;
  s.push(index);
  while (!s.empty()) {
    auto node = s.top();
    s.pop();
    m_nodes.at(node).Print(node);
    if (!m_nodes[node].IsLeaf()) {
      s.push(m_nodes[node].GetRight(node));
      s.push(m_nodes[node].GetLeft(node));
    }
  }
}
>>>>>>> release/21.0.127
