/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#ifndef EGAMMAINTERFACES_EGAMMAINTERFACESDICT_H
#define EGAMMAINTERFACES_EGAMMAINTERFACESDICT_H
/**
 * @file egammaInterfacesDict.h
 *
 * @brief Header file for Reflex dictionary generation
 *
 * @author RD Schaffer <R.D.Schaffer@cern.ch>
 *
 * @author RD Schaffer <R.D.Schaffer@cern.ch>
 *
 */

#include "egammaInterfaces/ICaloCluster_OnTrackBuilder.h"
#include "egammaInterfaces/IEMClusterTool.h"
#include "egammaInterfaces/IEMConversionBuilder.h"
#include "egammaInterfaces/IEMFourMomBuilder.h"
#include "egammaInterfaces/IEMShowerBuilder.h"
#include "egammaInterfaces/IegammaBackShape.h"
#include "egammaInterfaces/IegammaBaseTool.h"
#include "egammaInterfaces/IegammaCheckEnergyDepositTool.h"
#include "egammaInterfaces/IegammaIso.h"
#include "egammaInterfaces/IegammaMiddleShape.h"
#include "egammaInterfaces/IegammaPreSamplerShape.h"
#include "egammaInterfaces/IegammaShowerShape.h"
#include "egammaInterfaces/IegammaStripsShape.h"
#include "egammaInterfaces/IegammaSwTool.h"
<<<<<<< HEAD
#include "egammaInterfaces/IegammaMVASvc.h"
#include "egammaInterfaces/IegammaOQFlagsBuilder.h"
#ifndef XAOD_ANALYSIS
#   include "egammaInterfaces/IEMExtrapolationTools.h"
#   include "egammaInterfaces/IEMTrackMatchBuilder.h"
#   include "egammaInterfaces/IegammaTrkRefitterTool.h"
#endif // not XAOD_ANALYSIS

=======
#include "egammaInterfaces/IegammaTrkRefitterTool.h"
#include "egammaInterfaces/Iegammaqweta1c.h"
#include "egammaInterfaces/Iegammaqweta2c.h"
#include "egammaInterfaces/IelectronSuperClusterBuilder.h"
#include "egammaInterfaces/IphotonSuperClusterBuilder.h"
#include "egammaInterfaces/IegammaTopoClusterCopier.h"
>>>>>>> release/21.0.127
#endif // EGAMMAINTERFACES_EGAMMAINTERFACESDICT_H
