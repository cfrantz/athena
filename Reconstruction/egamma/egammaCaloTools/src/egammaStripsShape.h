/*
   Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
 */

#ifndef EGAMMACALOTOOLS_EGAMMASTRIPSSHAPE_H
#define EGAMMACALOTOOLS_EGAMMASTRIPSSHAPE_H

/// @class egammaStripsShape
/// @brief   EM cluster shower shape calculations in 1st ECAL sampling
/// Calculate the width in the strip layer around the eta,phi of
/// the hottest cell in the **middle layer**.
///
/// @author Frederic Derue derue@lpnhe.in2p3.fr
/// @author Christos Anastopoulos
///

class CaloCellContainer;
class CaloDetDescrManager;
class LArEM_ID;

#include "AthenaBaseComps/AthAlgTool.h"
#include "CaloGeoHelpers/CaloSampling.h"
#include "CaloIdentifier/CaloCell_ID.h"
#include "GaudiKernel/ToolHandle.h"
#include "egammaInterfaces/IegammaStripsShape.h"
#include "xAODCaloEvent/CaloCluster.h"

class egammaStripsShape
  : public AthAlgTool
  , virtual public IegammaStripsShape
{

public:
  /** @brief Default constructor*/
  egammaStripsShape(const std::string& type,
                    const std::string& name,
                    const IInterface* parent);
  /** @brief Destructor*/
  ~egammaStripsShape() = default;

  /** @brief AlgTool initialize method.*/
  StatusCode initialize() override;
  /** @brief AlgTool finalize method */
  StatusCode finalize() override;

  /** @brief AlgTool main method */
  virtual StatusCode execute(const xAOD::CaloCluster& cluster,
                             const CaloDetDescrManager& cmgr,
                             Info& info) const override final;

private:
  /** @brief boolean to calculate all variables*/
<<<<<<< HEAD
  Gaudi::Property<bool> m_ExecAllVariables{ this,
                                            "ExecAllVariables",
                                            true,
                                            "flag used by trigger" };
=======
  bool m_ExecAllVariables;
  /** @brief boolean to calculate less important variables*/
  bool m_ExecOtherVariables;
  // Calo variables
  const CaloDetDescrManager* m_calo_dd;

  int m_sizearrayeta;
  double m_deta;
  double m_dphi;

  enum { STRIP_ARRAY_SIZE = 40 };
  enum { DOUBLE_STRIP_ARRAY_SIZE = 80 };
  /** @brief array of cell energies*/
  double m_enecell[STRIP_ARRAY_SIZE]; 
  /** @brief array of cell eta*/
  double m_etacell[STRIP_ARRAY_SIZE]; 
  /** @brief array of cell granularity*/
  double m_gracell[STRIP_ARRAY_SIZE]; 
  /** @brief number of cells */
  int m_ncell[STRIP_ARRAY_SIZE]; 

  /** @brief uncorrected cluster energy in all samplings*/
  double m_eallsamples;
  /** @brief uncorrected cluster energy in 1st sampling*/
  double m_e1;

  /** @brief CaloSample */
  CaloSampling::CaloSample m_sam;
  CaloSampling::CaloSample m_samgran;
  CaloSampling::CaloSample m_offset;
  CaloCell_ID::SUBCALO m_subcalo;
  bool m_barrel;
  int m_sampling_or_module;

  /** @brief eta of the seed cell*/
  double m_etaseed;   
  /** @brief phi of the seed cell  */
  double m_phiseed;   
  /** @brief  eta of the hottest cell*/
  double m_etamax;    
  /** @brief phi of the hottest cell */
  double m_phimax;    
  /** @brief index of the array in eta of the seed  */
  int m_ncetaseed; 
  /** @brief index of the array in eta of the hottest cell*/
  int m_ncetamax;  
  /** @brief total width*/
  double m_wstot;      
  /** @brief total energy*/
  double m_etot;      
  /** @brief eta position in 3 strips*/
  double m_etas3; 
  /** @brief difference between track and shower position in +/- 1 cells */
  double m_deltaEtaTrackShower; 
  /** @brief difference between track and shower position in +/- 7 cells*/
  double m_deltaEtaTrackShower7;
  /** @brief energy in three strips*/
  double m_e132;     
  /** @brief energy in 15 strips*/
  double m_e1152;    
  /** @brief width with three strips*/
  double m_ws3;   
  /** @brief width with three strips, after corrections LArqweta1 */
  double m_ws3c;  
  /** @brief rel. position in eta within cell*/
  double m_poscs1;  
  /** @brief position within cell   */
  double m_etaincell;
  /** @brief asymmetry of signal with three strips */
  double m_asymmetrys3;     
  /** @brief  fraction of energy in sample 1*/
  double m_f1;       
  /** @brief energy in E1core/E i.e E132/E*/
  double m_f1core;
  /** @brief raction of energy in two highest energy strips */
  double m_f2;       
  /** @brief shower width on 5 strip around max*/
  double m_widths5;   
  /** @brief energy of the second local maximum */
  double m_esec;     
  /** @brief energy of strip with second max 2 */
  double m_esec1;    
  /** @brief  Michal Seman's valley */
  double m_val;     
  /** @brief Fraction of energy outside shower core 
      (E(+/-3strips)-E(+/-1strips))/ E(+/-1strips) */
  double m_fside;    
  /** @briefenergy of strip with max e   */
  double m_emaxs1;     
  /** @brief  energy of strip with min e */
  double m_emins1;    
  /** @brief flag for success */
  bool  m_success;   

>>>>>>> release/21.0.127
};

#endif
