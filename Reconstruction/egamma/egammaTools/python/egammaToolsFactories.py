# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

__doc__ = """ToolFactories to instantiate
all egammaTools with default configuration"""
__author__ = "Bruno Lenzi"


<<<<<<< HEAD
from ROOT import egammaPID
from ElectronPhotonSelectorTools.ConfiguredAsgForwardElectronIsEMSelectors \
    import ConfiguredAsgForwardElectronIsEMSelector
from .EMPIDBuilderBase import EMPIDBuilderPhotonBase
from .EMPIDBuilderBase import EMPIDBuilderElectronBase
from ElectronPhotonSelectorTools import ElectronPhotonSelectorToolsConf
from egammaTrackTools.egammaTrackToolsFactories import EMExtrapolationTools
from egammaMVACalib.egammaMVACalibFactories import egammaMVASvc


from egammaTools import egammaToolsConf
from egammaRec.Factories import ToolFactory
from egammaRec import egammaKeys
# to set jobproperties.egammaRecFlags
from egammaRec.egammaRecFlags import jobproperties


_clusterTypes = dict(
    Ele35='ele35', Ele55='ele55', Ele37='ele37',
    Gam35='gam35_unconv', Gam55='gam55_unconv', Gam37='gam37_unconv',
    Econv35='gam35_conv', Econv55='gam55_conv', Econv37='gam37_conv'
)
=======
def configureClusterCorrections(swTool):
  "Add attributes ClusterCorrectionToolsXX to egammaSwTool object"
  from CaloClusterCorrection.CaloSwCorrections import  make_CaloSwCorrections, rfac, etaoff_b1, etaoff_e1, \
      etaoff_b2,etaoff_e2,phioff_b2,phioff_e2,update,time,listBadChannel
  from CaloRec.CaloRecMakers import _process_tools

  clusterTypes = dict(
    Ele35='ele35', Ele55='ele55', Ele37='ele37',
    Gam35='gam35_unconv', Gam55='gam55_unconv',Gam37='gam37_unconv',
    Econv35='gam35_conv', Econv55='gam55_conv', Econv37='gam37_conv'
  )
  for attrName, clName in clusterTypes.iteritems():
    x = 'ClusterCorrectionTools' + attrName
    if not hasattr(swTool, x) or getattr(swTool, x):
      continue
    y = make_CaloSwCorrections(clName, suffix='EG',
      version = jobproperties.egammaRecFlags.clusterCorrectionVersion(),
      cells_name=egammaKeys.caloCellKey() )
    setattr(swTool, x, _process_tools (swTool, y) )

  #Super cluster position only corrections
  if jobproperties.egammaRecFlags.doSuperclusters():
    for attrName, clName in clusterTypes.iteritems():
      n = 'ClusterCorrectionToolsSuperCluster' + attrName
      if not hasattr(swTool, n) or getattr(swTool, n):
        continue
  
      setattr(swTool, n ,_process_tools(swTool, 
                                        make_CaloSwCorrections(clName, suffix ='EGSuperCluster',
                                                               corrlist=[[rfac,'v5'],[etaoff_b1,'v5'],[etaoff_e1,'v5'],
                                                                         [etaoff_b2,'v5'],[etaoff_e2,'v5'], [phioff_b2, 'v5data'], 
                                                                         [phioff_e2,  'v5data'], [update], [time], [listBadChannel]],
                                                               cells_name=egammaKeys.caloCellKey() )))
    #End of super cluster position only corrections
#-------------------------

egammaSwTool = ToolFactory(egammaToolsConf.egammaSwTool,
                           postInit=[configureClusterCorrections])

from egammaMVACalib import egammaMVACalibConf 
egammaMVATool =  ToolFactory(egammaMVACalibConf.egammaMVATool,
                              folder=jobproperties.egammaRecFlags.calibMVAVersion())

EMClusterTool = ToolFactory(egammaToolsConf.EMClusterTool,
                            OutputClusterContainerName = egammaKeys.outputClusterKey(),
                            OutputTopoSeededClusterContainerName = egammaKeys.outputTopoSeededClusterKey(),
                            ElectronContainerName = egammaKeys.outputElectronKey(),
                            PhotonContainerName = egammaKeys.outputPhotonKey(),
                            ClusterCorrectionToolName = FullNameWrapper(egammaSwTool),
                            doSuperCluster = jobproperties.egammaRecFlags.doSuperclusters(),
                            MVACalibTool= egammaMVATool
                            )
>>>>>>> release/21.0.127


# Configure fixed-size (non-supercell) corrections
def configureFixedSizeClusterCorrections(swTool):
    "Add attributes ClusterCorrectionToolsXX to egammaSwTool object for fixed-size cluster corrections."
    from CaloClusterCorrection.CaloSwCorrections import make_CaloSwCorrections
    from CaloRec.CaloRecMakers import _process_tools

    for attrName, clName in _clusterTypes.items():
        x = 'ClusterCorrectionTools' + attrName
        if not hasattr(swTool, x) or getattr(swTool, x):
            continue
        y = make_CaloSwCorrections(
            clName,
            suffix='EG',
            version=jobproperties.egammaRecFlags.clusterCorrectionVersion(),
            cells_name=egammaKeys.caloCellKey())
        setattr(swTool, x, _process_tools(swTool, y))


# Configure corrections for superclusters.
def configureSuperClusterCorrections(swTool):
    "Add attributes ClusterCorrectionToolsXX to egammaSwTool object for corrections for superclusters."
    from CaloClusterCorrection.CaloSwCorrections import (
        make_CaloSwCorrections, rfac, etaoff_b1, etaoff_e1,
        etaoff_b2, etaoff_e2, phioff_b2, phioff_e2, update,
        time, listBadChannel)
    from CaloRec.CaloRecMakers import _process_tools

    for attrName, clName in _clusterTypes.items():
        n = 'ClusterCorrectionToolsSuperCluster' + attrName
        if not hasattr(swTool, n) or getattr(swTool, n):
            continue

        setattr(swTool, n, _process_tools(
            swTool,
            make_CaloSwCorrections(
                clName,
                suffix='EGSuperCluster',
                version=jobproperties.egammaRecFlags.clusterCorrectionVersion(),
                corrlist=[
                    [rfac, 'v5'],
                    [etaoff_b1, 'v5'],
                    [etaoff_e1, 'v5'],
                    [etaoff_b2, 'v5'],
                    [etaoff_e2, 'v5'],
                    [phioff_b2, 'v5data'],
                    [phioff_e2, 'v5data'],
                    [update],
                    [time],
                    [listBadChannel]],
                cells_name=egammaKeys.caloCellKey())))


<<<<<<< HEAD
=======
from EMBremCollectionBuilder import egammaBremCollectionBuilder
from egammaTrackTools.egammaTrackToolsFactories import EMExtrapolationTools
EMBremCollectionBuilder = ToolFactory( egammaBremCollectionBuilder,
                                       name = 'EMBremCollectionBuilder',
                                       ExtrapolationTool = EMExtrapolationTools,
                                       OutputTrackContainerName=egammaKeys.outputTrackKey(),
                                       ClusterContainerName=egammaKeys.inputClusterKey(),
                                       DoTruth=rec.doTruth()
                                       )
>>>>>>> release/21.0.127

def configureClusterCorrections(swTool):
    "Add attributes ClusterCorrectionToolsXX to egammaSwTool object"
    configureFixedSizeClusterCorrections(swTool)
    if jobproperties.egammaRecFlags.doSuperclusters():
        configureSuperClusterCorrections(swTool)


egammaSwTool = ToolFactory(egammaToolsConf.egammaSwTool,
                           postInit=[configureClusterCorrections])


egammaSwSuperClusterTool = ToolFactory(egammaToolsConf.egammaSwTool,
                                       postInit=[configureSuperClusterCorrections])

<<<<<<< HEAD

EMClusterTool = ToolFactory(
    egammaToolsConf.EMClusterTool,
    OutputClusterContainerName=egammaKeys.outputClusterKey(),
    OutputTopoSeededClusterContainerName=egammaKeys.outputTopoSeededClusterKey(),
    ClusterCorrectionTool=egammaSwTool,
    doSuperCluster=jobproperties.egammaRecFlags.doSuperclusters(),
    MVACalibSvc=egammaMVASvc
)


EMConversionBuilder = ToolFactory(
    egammaToolsConf.EMConversionBuilder,
    ConversionContainerName=egammaKeys.outputConversionKey(),
    ExtrapolationTool=EMExtrapolationTools)

EGammaAmbiguityTool = ToolFactory(
    ElectronPhotonSelectorToolsConf.EGammaAmbiguityTool)

EMFourMomBuilder = ToolFactory(egammaToolsConf.EMFourMomBuilder)

egammaLargeClusterMakerTool = ToolFactory(
    egammaToolsConf.egammaLargeClusterMaker,
    name="egammaLCMakerTool",
    InputClusterCollection=egammaKeys.ClusterKey(),
    CellsName=egammaKeys.caloCellKey()
)

egammaLargeFWDClusterMakerTool = ToolFactory(
    egammaToolsConf.egammaLargeClusterMaker,
    name="egammaLCFWDMakerTool",
    InputClusterCollection=egammaKeys.FwdClusterKey(),
    CellsName=egammaKeys.caloCellKey(),
    doFWDelesurraundingWindows = True
)
=======
egammaTopoClusterCopier = ToolFactory( egammaToolsConf.egammaTopoClusterCopier,
                                       name = 'egammaTopoClusterCopier' ,
                                       InputTopoCollection=jobproperties.egammaRecFlags.inputTopoClusterCollection(),
                                       OutputTopoCollection=jobproperties.egammaRecFlags.egammaTopoClusterCollection()
                                       )

electronSuperClusterBuilder = ToolFactory( egammaToolsConf.electronSuperClusterBuilder,
                                           name = 'electronSuperClusterBuilder',
                                           ClusterCorrectionTool=egammaSwTool,
                                           MVACalibTool=egammaMVATool,
                                           EtThresholdCut=1000, 
                                           AddCellsWindowEtaCellsBarrel=3,
                                           AddCellsWindowPhiCellsBarrel=999,
                                           AddCellsWindowEtaCellsEndcap=5,
                                           AddCellsWindowPhiCellsEndcap=999
                                         )

photonSuperClusterBuilder = ToolFactory( egammaToolsConf.photonSuperClusterBuilder,
                                         name = 'photonSuperClusterBuilder',
                                         ClusterCorrectionTool=egammaSwTool,
                                         MVACalibTool= egammaMVATool,
                                         AddCellsWindowEtaCellsBarrel=3,
                                         AddCellsWindowPhiCellsBarrel=999,
                                         AddCellsWindowEtaCellsEndcap=5,
                                         AddCellsWindowPhiCellsEndcap=999
                                         )

#End of super clustering
>>>>>>> release/21.0.127

# Electron Selectors
ElectronPIDBuilder = ToolFactory(
    EMPIDBuilderElectronBase,
    name="ElectronPIDBuilder")

# Photon Selectors
PhotonPIDBuilder = ToolFactory(
    EMPIDBuilderPhotonBase,
    name="PhotonPIDBuilder")

# ForwardElectron Selectors

## Eventually we want to get rid of cppyy here
#cppyy.load_library('libElectronPhotonSelectorToolsDict')

LooseForwardElectronSelector = ToolFactory(
    ConfiguredAsgForwardElectronIsEMSelector,
    name="LooseForwardElectronSelector",
    quality=egammaPID.ForwardElectronIDLoose)
MediumForwardElectronSelector = ToolFactory(
    ConfiguredAsgForwardElectronIsEMSelector,
    name="MediumForwardElectronSelector",
    quality=egammaPID.ForwardElectronIDMedium)
TightForwardElectronSelector = ToolFactory(
    ConfiguredAsgForwardElectronIsEMSelector,
    name="TightForwardElectronSelector",
    quality=egammaPID.ForwardElectronIDTight)

# -------------------------

# Import the factories that are not defined here
from .EMTrackMatchBuilder import EMTrackMatchBuilder    # noqa: F401
from .egammaOQFlagsBuilder import egammaOQFlagsBuilder  # noqa: F401
from .EMShowerBuilder import EMShowerBuilder            # noqa: F401
