<<<<<<< HEAD

# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# =======================================================================
# File:   egammaRec/python/egammaRecFlags.py
# =======================================================================
=======
# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#=======================================================================
# File:   egammaRec/python/egammaRecFlags.py
#=======================================================================
__author__  = 'C. Anastopoulos'
__version__="$Revision: 2.0 $"
__doc__="egamma flags . "

#=======================================================================
# imports
#=======================================================================
from AthenaCommon.JobProperties import JobProperty, JobPropertyContainer
>>>>>>> release/21.0.127
from AthenaCommon.JobProperties import jobproperties
from AthenaCommon.JobProperties import JobProperty, JobPropertyContainer
__author__ = 'C. Anastopoulos'
__version__ = "$Revision: 2.0 $"
__doc__ = "egamma flags . "


class Enabled(JobProperty):
<<<<<<< HEAD
    """ jobproperty to disable/enable the egamma algorithms as a group
    (container) in one go.
=======
    """ jobproperty to disable/enable the egamma algorithms as a group (container) in one go.
>>>>>>> release/21.0.127
    Can enable/disable the full egamma
    """
    statusOn = True
    allowedTypes = ['bool']
    StoredValue = True


class egammaRecFlagsJobProperty(JobProperty):
<<<<<<< HEAD
    """ special flag . Subclass  which  has get_value depending on
    job properties.egammaRecFlags.Enabled
    so properties inheriting from this will be disables
    if egammaRec is not Enabled.
=======
    """ special flag . Subclass  which  has get_value depending on job properties.egammaRecFlags.Enabled
    so properties inheriting from this will be disables if egammaRec is not Enabled.
>>>>>>> release/21.0.127
    """

    def get_Value(self):
<<<<<<< HEAD
        return (self.statusOn and
                self.StoredValue
                and jobproperties.egammaRecFlags.Enabled())

# Different egamma seeding styles


=======
        return self.statusOn and self.StoredValue and jobproperties.egammaRecFlags.Enabled()

#Different egamma seeding styles    
>>>>>>> release/21.0.127
class doEgammaCaloSeeded (egammaRecFlagsJobProperty):
    """ switch for standard cluster-based egamma algorithm
    """
    statusOn = True
    allowedTypes = ['bool']
    StoredValue = True


class doEgammaForwardSeeded (egammaRecFlagsJobProperty):
    """ switch for standard forward electrons algorithm
    """
    statusOn = True
    allowedTypes = ['bool']
    StoredValue = True

<<<<<<< HEAD

class doBremFinding (egammaRecFlagsJobProperty):
    """ switch for whether to do the brem finding
=======
class doTopoCaloSeeded (JobProperty):
    """ switch for SW TopoCalo algorithm
    """
    statusOn=True
    allowedTypes=['bool']
    StoredValue=True

#Other options
class doEgammaTruthAssociation (JobProperty):
    """ switch for truth association alg
>>>>>>> release/21.0.127
    """
    statusOn = True
    allowedTypes = ['bool']
    StoredValue = True


<<<<<<< HEAD
class doVertexBuilding (egammaRecFlagsJobProperty):
    """ switch for whether to do the conversion vertex building
    """
    statusOn = True
    allowedTypes = ['bool']
    StoredValue = True


# Other options

class doEgammaTruthAssociation (JobProperty):
    """ switch for truth association alg
    """
    statusOn = True
    allowedTypes = ['bool']
    StoredValue = True


class doConversions (JobProperty):
    """ switch for doing conversion matching
    """
    statusOn = True
    allowedTypes = ['bool']
    StoredValue = True


class cellContainerName (JobProperty):
    """Allows overriding the name of the cell container used
    for building EM object clusters.
    """
    statusOn = False
    allowedTypes = ['str']
    StoredValue = ''


# Super cluster options
class doSuperclusters (JobProperty):
    """ switch to enable/disable the supercluster based algorithm
=======
class cellContainerName (JobProperty):
    """Allows overriding the name of the cell container used for building EM object clusters.
    """
    statusOn=False
    allowedTypes=['str']
    StoredValue=''
 
class doSwitchTRTGas (JobProperty):
    """ switch using Xenon gas in TRT
>>>>>>> release/21.0.127
    """
    statusOn = True
    allowedTypes = ['bool']
    StoredValue = True

<<<<<<< HEAD

class inputTopoClusterCollection (JobProperty):
    """Name of input cluster container from which the topological
    cluster to be used during superclustering
    are selected
=======
# Super cluster options
class doSuperclusters (JobProperty):
    """ switch to enable/disable the supercluster based algorithm
    """
    statusOn=True
    allowedTypes=['bool']
    StoredValue=True

class inputTopoClusterCollection (JobProperty):
    """Name of input cluster container from which the topological cluster to be used during superclustering
    are selected 
>>>>>>> release/21.0.127
    """
    statusOn = True
    allowedTypes = ['str']
    StoredValue = 'CaloTopoCluster'


class egammaTopoClusterCollection (JobProperty):
<<<<<<< HEAD
    """Name of cluster container containing the selected topological clusters
=======
    """Name of cluster container containing the selected topological clusters 
>>>>>>> release/21.0.127
    used during superclustering
    """
    statusOn = True
    allowedTypes = ['str']
    StoredValue = 'egammaTopoCluster'

# Calibration Options

<<<<<<< HEAD
=======
#Calibration Options
class clusterCorrectionVersion (JobProperty):
    """Version of calo cluster corrections used for calibration.
    """
    statusOn=True
    allowedTypes=['str', 'None']
    StoredValue='v12phiflip_noecorrnogap'
>>>>>>> release/21.0.127

class calibMVAVersion (JobProperty):
    """Version of calo cluster corrections used for calibration.
    """
<<<<<<< HEAD
    statusOn = True
    allowedTypes = ['str', 'None']
    StoredValue = 'v12phiflip_noecorrnogap'
=======
    statusOn=True
    allowedTypes=['str', 'None']
    StoredValue="egammaMVACalib/offline/v7_pre"
>>>>>>> release/21.0.127

# Defines a sub-container holding the jobProperties for egamma
class egammaRecFlags(JobPropertyContainer):
    """ egamma information """

class calibMVAVersion (JobProperty):
    """Version of calo cluster corrections used for calibration.
    """
    statusOn = True
    allowedTypes = ['str', 'None']
    StoredValue = "egammaMVACalib/offline/v7"

<<<<<<< HEAD
# Defines a sub-container holding the jobProperties for egamma

=======
# I want always the following flags in the container  
_list_Egamma=[Enabled,doEgammaCaloSeeded,doEgammaForwardSeeded,doConversions,doTopoCaloSeeded,cellContainerName,doSwitchTRTGas,
              doEgammaTruthAssociation,clusterCorrectionVersion,calibMVAVersion, doSuperclusters, inputTopoClusterCollection, 
              egammaTopoClusterCollection]
>>>>>>> release/21.0.127

class egammaRecFlags(JobPropertyContainer):
    """ egamma information """


# add the egammaRec flags container to the top container
jobproperties.add_Container(egammaRecFlags)

# I want always the following flags in the container
_list_Egamma = [Enabled, doEgammaCaloSeeded, doEgammaForwardSeeded,
                doBremFinding, doVertexBuilding, doConversions,
                cellContainerName, doEgammaTruthAssociation,
                clusterCorrectionVersion, calibMVAVersion, doSuperclusters,
                inputTopoClusterCollection, egammaTopoClusterCollection]

for j in _list_Egamma:
    jobproperties.egammaRecFlags.add_JobProperty(j)
del _list_Egamma


# =======================================================================
