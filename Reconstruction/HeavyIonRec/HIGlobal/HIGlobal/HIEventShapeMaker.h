/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#ifndef __HIEVENTSHAPEMAKER_H__
#define __HIEVENTSHAPEMAKER_H__

#include "AthenaBaseComps/AthAlgorithm.h"

#include "xAODHIEvent/HIEventShapeContainer.h"
#include "xAODHIEvent/HIEventShapeAuxContainer.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include <string>

#include "GaudiKernel/ToolHandle.h"
#include "Gaudi/Property.h"
#include "IHIEventShapeFiller.h"
#include "HIEventUtils/HIEventShapeSummaryTool.h"

#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/WriteHandleKey.h"

class CaloCellContainer;

class HIEventShapeMaker : public AthAlgorithm
{
public:

  /** Standard Athena-Algorithm Constructor */
  HIEventShapeMaker(const std::string& name, ISvcLocator* pSvcLocator);
  /** Default Destructor */
  ~HIEventShapeMaker() {};


  virtual StatusCode initialize();
  virtual StatusCode execute();
  virtual StatusCode finalize();

private:
<<<<<<< HEAD
  SG::ReadHandleKey<xAOD::CaloClusterContainer>      m_towerContainerKey { this, "InputTowerKey"     , "CombinedTower"   , "InputTowerKey"}; //TowerContainer key
  SG::ReadHandleKey<INavigable4MomentumCollection>   m_naviContainerKey  { this, "NaviTowerKey"     ,  "CombinedTower"   , "InputTowerKey for Navigable Momentum collection"};
  SG::ReadHandleKey<CaloCellContainer>            m_cellContainerKey  { this, "InputCellKey"      , "AllCalo"         , "InputCellKey" }; //CaloCellContainer key
  SG::ReadHandleKey<xAOD::HIEventShapeContainer>  m_readExistingKey   { this, "ReadExistingKey", "HIEventShapeContainer", "Read key to retrieve the shape if m_summary_only is true"};
  SG::WriteHandleKey<xAOD::HIEventShapeContainer> m_outputKey         { this, "OutputContainerKey"    , "HIEventShapeContainer", "Output Container Key"}; //Name of output container key
  SG::WriteHandleKey<xAOD::HIEventShapeContainer>  m_summaryKey       { this, "SummaryContainerKey"   , "", "SummaryContainerKey" };

  //use cells instead of towers to fill the EventShape
  Gaudi::Property< bool > m_useCaloCell { this, "UseCaloCell", false, "Use cells instead of towers to fill the EventShape" }  ;
  //The number of Orders of harmonic flow to store in the EventShape
  Gaudi::Property< int > m_numOrders { this, "OrderOfFlowHarmonics", 7, "The number of Orders of harmonic flow to store in the EventShape" }  ;
  Gaudi::Property< bool > m_summaryOnly { this, "SummaryOnly", false, "Summary Only boolean switch" }  ;

  ToolHandle<IHIEventShapeFiller> m_HIEventShapeFillerTool { this, "HIEventShapeFillerTool", "HIEventShapeFillerTool", "HIEventShapeFillerTool" };
  ToolHandle<IHIEventShapeSummaryTool> m_summaryTool      { this, "SummaryTool", "HIEventShapeSummaryTool", "Handle to IHIEventShapeSummaryTool" };

=======
  std::string m_tower_container_key;//TowerContainer key
  std::string m_cell_container_key ;//CaloCellContainer key
  std::string m_output_key         ;//Name of output container key
  bool        m_use_calo_cell      ;//use cells instead of towers to fill the EventShape
  int         m_NumOrders          ;//The number of Orders of harmonic flow to store in the EventShape
  std::string m_summary_key;
  bool m_summary_only;

  ToolHandle<IHIEventShapeFiller> m_HIEventShapeFillerTool;
  ToolHandle<IHIEventShapeSummaryTool> m_summary_tool;
>>>>>>> release/21.0.127
  void PrintHIEventShapeContainer(const xAOD::HIEventShapeContainer *Container);
};

#endif
