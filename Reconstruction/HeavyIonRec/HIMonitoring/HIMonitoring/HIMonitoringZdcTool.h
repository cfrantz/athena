/*
   Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
 */

#ifndef HIMONITORINGZDCTOOL_H
#define HIMONITORINGZDCTOOL_H


#include "AthenaMonitoring/ManagedMonitorToolBase.h"

<<<<<<< HEAD

=======
#include <xAODHIEvent/HIEventShape.h>
#include <xAODHIEvent/HIEventShapeContainer.h>
#include <ZdcAnalysis/ZdcAnalysisTool.h>
#include <xAODForward/ZdcModule.h>
#include <xAODForward/ZdcModuleAuxContainer.h>
#include <xAODForward/ZdcModuleContainer.h>
#include <xAODForward/xAODForwardDict.h>

// #include <xAODTrigEgamma/TrigElectronContainer.h>



class TH1;
class TGraph;
class TTree;
>>>>>>> release/21.0.127
class TH1D;
class TH2D_LW;


<<<<<<< HEAD
class HIMonitoringZdcTool: public ManagedMonitorToolBase
{
public:
  HIMonitoringZdcTool(const std::string& type, const std::string& name,
                      const IInterface* parent);

  virtual ~HIMonitoringZdcTool();

  virtual StatusCode bookHistogramsRecurrent();
  virtual StatusCode bookHistograms();
  virtual StatusCode fillHistograms();
  virtual StatusCode procHistograms();


  void book_hist();
private:
  double m_NEW_AEM {}, m_NEW_AHAD1 {}, m_NEW_AHAD2 {}, m_NEW_AHAD3 {};
  double m_NEW_CEM {}, m_NEW_CHAD1 {}, m_NEW_CHAD2 {}, m_NEW_CHAD3 {};
  //double m_AEM {}, m_AHAD1 {}, m_AHAD2 {}, m_AHAD3 {};
  //double m_CEM {}, m_CHAD1 {}, m_CHAD2 {}, m_CHAD3 {};
  double m_G0AEM {}, m_G0AHAD1 {}, m_G0AHAD2 {}, m_G0AHAD3 {};
  double m_G0CEM {}, m_G0CHAD1 {}, m_G0CHAD2 {}, m_G0CHAD3 {};
  double m_G1AEM {}, m_G1AHAD1 {}, m_G1AHAD2 {}, m_G1AHAD3 {};
  double m_G1CEM {}, m_G1CHAD1 {}, m_G1CHAD2 {}, m_G1CHAD3 {};
  //double m_SumSideA {}, m_SumSideC {};
  double m_NEW_SumSideA {}, m_NEW_SumSideC {};

  static constexpr int s_Nsamp {
    7
  };
  static constexpr int s_Nmod {
    4
  };
  static constexpr int s_Nside {
    2
  };


  /// histograms
  TH1D* m_hamp[s_Nmod][s_Nside] {};
  //TH1D* m_hamp_NEW[s_Nmod][s_Nside] {};
  TH1D* m_hampG0[s_Nmod][s_Nside] {};
  TH1D* m_hampG1[s_Nmod][s_Nside] {};
  TH1D* m_hSumSideAmp[s_Nside] {};
  //TH1D* m_hSumSideAmp_NEW[s_Nside] {};
  TH1D* m_hSumSideAmpG0[s_Nside] {};
  TH1D* m_hSumSideAmpG1[s_Nside] {};
  TH2D_LW* m_hEM_HAD1[s_Nside] {};
  TH2D_LW* m_hHAD1_HAD2[s_Nside] {};
  TH2D_LW* m_hHAD2_HAD3[s_Nside] {};
  TH2D_LW* m_hSideAC {};

  //TH2D_LW* m_hEM_HAD1_NEW[s_Nside] {};
  //TH2D_LW* m_hHAD1_HAD2_NEW[s_Nside] {};
  //TH2D_LW* m_hHAD2_HAD3_NEW[s_Nside] {};
  //TH2D_LW* m_hSideAC_NEW {};
=======
      
	  void book_hist();


	 private:
		double NEW_AEM, NEW_AHAD1, NEW_AHAD2, NEW_AHAD3;
		double NEW_CEM, NEW_CHAD1, NEW_CHAD2, NEW_CHAD3;
		double AEM, AHAD1, AHAD2, AHAD3;
		double CEM, CHAD1, CHAD2, CHAD3;
		double G0AEM, G0AHAD1, G0AHAD2, G0AHAD3;
		double G0CEM, G0CHAD1, G0CHAD2, G0CHAD3;
		double G1AEM, G1AHAD1, G1AHAD2, G1AHAD3;
		double G1CEM, G1CHAD1, G1CHAD2, G1CHAD3;
		double SumSideA, SumSideC;
		double NEW_SumSideA, NEW_SumSideC;


		/// histograms 
		TH1D* hamp[Nmod][Nside];
		TH1D* hamp_NEW[Nmod][Nside];
		TH1D* hampG0[Nmod][Nside];
		TH1D* hampG1[Nmod][Nside];
		TH1D* hSumSideAmp[Nside];
		TH1D* hSumSideAmp_NEW[Nside];
		TH1D* hSumSideAmpG0[Nside];
		TH1D* hSumSideAmpG1[Nside];
		TH2D_LW* hEM_HAD1[Nside];
		TH2D_LW* hHAD1_HAD2[Nside];
		TH2D_LW* hHAD2_HAD3[Nside];
		TH2D_LW* hSideAC;

		TH2D_LW* hEM_HAD1_NEW[Nside];
		TH2D_LW* hHAD1_HAD2_NEW[Nside];
		TH2D_LW* hHAD2_HAD3_NEW[Nside];
		TH2D_LW* hSideAC_NEW;

	    
>>>>>>> release/21.0.127
};

#endif
