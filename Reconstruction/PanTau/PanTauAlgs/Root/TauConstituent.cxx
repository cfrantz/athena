/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#include "PanTauAlgs/TauConstituent.h"

<<<<<<< HEAD
PanTau::TauConstituent::TauConstituent() :
  IParticle(),
  m_p4(), m_p4Cached( false ),
  m_TypeFlags(),
  m_BDTValue(PanTau::TauConstituent::DefaultBDTValue()),
  m_Charge(PanTau::TauConstituent::DefaultCharge()),
=======
#include <iostream>

PanTau::TauConstituent2::TauConstituent2()
  :
  IParticle(),
  m_p4(), m_p4Cached( false ),
  m_TypeFlags(),
  m_BDTValue(PanTau::TauConstituent2::DefaultBDTValue()),
  m_Charge(PanTau::TauConstituent2::DefaultCharge()),
>>>>>>> release/21.0.127
  m_PFOLink(0),
  m_Shots(),
  m_nPhotonsInShot(0)
{
}


<<<<<<< HEAD
PanTau::TauConstituent::TauConstituent(TLorentzVector   itsMomentum,
				       int              itsCharge,
				       std::vector<int> itsType,
				       double           itsBDTValue,
				       xAOD::PFO*       itsPFO) :
=======

PanTau::TauConstituent2::TauConstituent2(TLorentzVector   itsMomentum,
					 int              itsCharge,
					 std::vector<int> itsType,
					 double           itsBDTValue,
					 xAOD::PFO*       itsPFO
					 )
  :
>>>>>>> release/21.0.127
  IParticle(),
  m_p4(itsMomentum), m_p4Cached(true),
  m_TypeFlags(itsType),
  m_BDTValue(itsBDTValue),
  m_Charge(itsCharge),
  m_PFOLink(itsPFO),
  m_Shots(),
  m_nPhotonsInShot(0)
{
}


<<<<<<< HEAD
PanTau::TauConstituent::TauConstituent(const PanTau::TauConstituent& rhs) :
=======

PanTau::TauConstituent2::TauConstituent2(
					 const PanTau::TauConstituent2& rhs
					 ):
>>>>>>> release/21.0.127
  IParticle(rhs),
  m_p4(rhs.m_p4), m_p4Cached(rhs.m_p4Cached),
  m_TypeFlags(rhs.m_TypeFlags),
  m_BDTValue(rhs.m_BDTValue),
  m_Charge(rhs.m_Charge),
  m_PFOLink(rhs.m_PFOLink),
  m_Shots(rhs.m_Shots),
  m_nPhotonsInShot(rhs.m_nPhotonsInShot)
{
}


<<<<<<< HEAD
PanTau::TauConstituent::~TauConstituent()
{
  //delete the shot constituents
  for(unsigned int iShot=0; iShot<m_Shots.size(); iShot++) {
    PanTau::TauConstituent* curConst = m_Shots[iShot];
=======

PanTau::TauConstituent2::~TauConstituent2()
{
  //delete the shot constituents
  for(unsigned int iShot=0; iShot<m_Shots.size(); iShot++) {
    PanTau::TauConstituent2* curConst = m_Shots[iShot];
>>>>>>> release/21.0.127
    delete curConst;
  }
}


<<<<<<< HEAD
PanTau::TauConstituent& PanTau::TauConstituent::operator=(const PanTau::TauConstituent& tauConst)
=======

PanTau::TauConstituent2& PanTau::TauConstituent2::operator=(const PanTau::TauConstituent2& tauConst)
>>>>>>> release/21.0.127
{
  if (this!=&tauConst){

    if (!this->container() && !this->hasStore() ) {      
<<<<<<< HEAD
      makePrivateStore();
=======
    	makePrivateStore();
>>>>>>> release/21.0.127
    }
    this->IParticle::operator=( tauConst );
    this->m_p4 = tauConst.m_p4;
    this->m_p4Cached = tauConst.m_p4Cached;
    m_TypeFlags = tauConst.m_TypeFlags;
    m_BDTValue  = tauConst.m_BDTValue;
    m_Charge    = tauConst.m_Charge;
    m_PFOLink   = tauConst.m_PFOLink;
    m_Shots     = tauConst.m_Shots;
    m_nPhotonsInShot = tauConst.m_nPhotonsInShot;
  }
  return *this;
}


<<<<<<< HEAD
double PanTau::TauConstituent::pt() const {
=======
double PanTau::TauConstituent2::pt() const {
>>>>>>> release/21.0.127
  static Accessor< float > acc( "pt" );
  return acc( *this );
}

<<<<<<< HEAD
double PanTau::TauConstituent::eta() const {
=======
double PanTau::TauConstituent2::eta() const {
>>>>>>> release/21.0.127
  static Accessor<float > acc( "eta" );
  return acc( *this );
}

<<<<<<< HEAD
double PanTau::TauConstituent::phi() const {
=======
double PanTau::TauConstituent2::phi() const {
>>>>>>> release/21.0.127
  static Accessor< float > acc( "phi" );
  return acc( *this );
}

<<<<<<< HEAD
double PanTau::TauConstituent::m() const {
=======
double PanTau::TauConstituent2::m() const {
>>>>>>> release/21.0.127
  static Accessor< float> acc( "m" );
  return acc( *this );
}

<<<<<<< HEAD
double PanTau::TauConstituent::e() const{
  return p4().E(); 
}

double PanTau::TauConstituent::rapidity() const {
  return p4().Rapidity(); 
}

PanTau::TauConstituent::FourMom_t PanTau::TauConstituent::p4() const {
=======
double PanTau::TauConstituent2::e() const{
  return p4().E(); 
}

double PanTau::TauConstituent2::rapidity() const {
  return p4().Rapidity(); 
}

const PanTau::TauConstituent2::FourMom_t& PanTau::TauConstituent2::p4() const {
>>>>>>> release/21.0.127
  if( ! m_p4Cached ) {
    m_p4.SetPtEtaPhiM( pt(), eta(), phi(),m()); 
    m_p4Cached=true;
  }
  return m_p4;
}

<<<<<<< HEAD
void PanTau::TauConstituent::setP4(float pt, float eta, float phi, float m){
=======
void PanTau::TauConstituent2::setP4(float pt, float eta, float phi, float m){
>>>>>>> release/21.0.127
  static Accessor< float > acc1( "pt" );
  acc1( *this ) = pt;
  static Accessor< float > acc2( "eta" );
  acc2( *this ) = eta;
  static Accessor< float > acc3( "phi" );
  acc3( *this ) = phi;
  static Accessor< float > acc4( "m" );
  acc4( *this ) = m;
  //Need to recalculate m_p4 if requested after update
  m_p4Cached=false;
}

<<<<<<< HEAD
void PanTau::TauConstituent::setPt(float pt){
=======
void PanTau::TauConstituent2::setPt(float pt){
>>>>>>> release/21.0.127
  static Accessor< float > acc( "pt" );
  acc( *this ) = pt;
  //Need to recalculate m_p4 if requested after update
  m_p4Cached=false;
}

<<<<<<< HEAD
void PanTau::TauConstituent::setEta(float eta){
=======
void PanTau::TauConstituent2::setEta(float eta){
>>>>>>> release/21.0.127
  static Accessor< float > acc( "eta" );
  acc( *this ) = eta;
  //Need to recalculate m_p4 if requested after update
  m_p4Cached=false;
}

<<<<<<< HEAD
void PanTau::TauConstituent::setPhi(float phi){
=======
void PanTau::TauConstituent2::setPhi(float phi){
>>>>>>> release/21.0.127
  static Accessor< float > acc( "phi" );
  acc( *this ) = phi;
  //Need to recalculate m_p4 if requested after update
  m_p4Cached=false;
}

<<<<<<< HEAD
void PanTau::TauConstituent::setM(float m){
=======
void PanTau::TauConstituent2::setM(float m){
>>>>>>> release/21.0.127
  static Accessor< float > acc( "m" );
  acc( *this ) = m;
  //Need to recalculate m_p4 if requested after update
  m_p4Cached=false;
}


<<<<<<< HEAD
xAOD::Type::ObjectType PanTau::TauConstituent::type() const {
=======
xAOD::Type::ObjectType PanTau::TauConstituent2::type() const {
>>>>>>> release/21.0.127
  return xAOD::Type::ParticleFlow;
}


<<<<<<< HEAD
void PanTau::TauConstituent::removeTypeFlag(TauConstituent::Type aType) {
=======


void             PanTau::TauConstituent2::removeTypeFlag(TauConstituent2::Type aType) {
>>>>>>> release/21.0.127
  unsigned int typeIndex = (unsigned int)aType;
  m_TypeFlags.at(typeIndex) = 0;
  return;
}


//the static getTypeName which does a translation
<<<<<<< HEAD
std::string  PanTau::TauConstituent::getTypeName(PanTau::TauConstituent::Type aType) {
  switch(aType) {
  case PanTau::TauConstituent::t_Charged: return "Charged";
  case PanTau::TauConstituent::t_Neutral: return "Neutral";
  case PanTau::TauConstituent::t_Pi0Neut: return "Pi0Neut";
  case PanTau::TauConstituent::t_OutChrg: return "OuterChrg";
  case PanTau::TauConstituent::t_OutNeut: return "OuterNeut";
  case PanTau::TauConstituent::t_NeutLowA: return "NeutLowA";
  case PanTau::TauConstituent::t_NeutLowB: return "NeutLowB";
  case PanTau::TauConstituent::t_NoType: return "All";
=======
std::string      PanTau::TauConstituent2::getTypeName(PanTau::TauConstituent2::Type aType) {
  switch(aType) {
  case PanTau::TauConstituent2::t_Charged: return "Charged";
  case PanTau::TauConstituent2::t_Neutral: return "Neutral";
  case PanTau::TauConstituent2::t_Pi0Neut: return "Pi0Neut";
  case PanTau::TauConstituent2::t_OutChrg: return "OuterChrg";
  case PanTau::TauConstituent2::t_OutNeut: return "OuterNeut";
  case PanTau::TauConstituent2::t_NeutLowA: return "NeutLowA";
  case PanTau::TauConstituent2::t_NeutLowB: return "NeutLowB";
  case PanTau::TauConstituent2::t_NoType: return "All";
>>>>>>> release/21.0.127
  default: return "UnkownType";
  }
}


<<<<<<< HEAD
bool PanTau::TauConstituent::isNeutralType(int tauConstituentType) {
  PanTau::TauConstituent::Type type = (PanTau::TauConstituent::Type)tauConstituentType;
  switch(type) {
  case PanTau::TauConstituent::t_Neutral: return true;
  case PanTau::TauConstituent::t_Pi0Neut: return true;
  case PanTau::TauConstituent::t_OutNeut: return true;
  case PanTau::TauConstituent::t_NeutLowA: return true;
  case PanTau::TauConstituent::t_NeutLowB: return true;
=======

bool         PanTau::TauConstituent2::isNeutralType(int tauConstituentType) {
  PanTau::TauConstituent2::Type type = (PanTau::TauConstituent2::Type)tauConstituentType;
  switch(type) {
  case PanTau::TauConstituent2::t_Neutral: return true;
  case PanTau::TauConstituent2::t_Pi0Neut: return true;
  case PanTau::TauConstituent2::t_OutNeut: return true;
  case PanTau::TauConstituent2::t_NeutLowA: return true;
  case PanTau::TauConstituent2::t_NeutLowB: return true;
>>>>>>> release/21.0.127
  default: return false;
  }
  return false;
}


<<<<<<< HEAD
bool PanTau::TauConstituent::isCoreType(int tauConstituentType) {
  PanTau::TauConstituent::Type type = (PanTau::TauConstituent::Type)tauConstituentType;
  switch(type) {
  case PanTau::TauConstituent::t_Charged: return true;
  case PanTau::TauConstituent::t_Neutral: return true;
  case PanTau::TauConstituent::t_Pi0Neut: return true;
  case PanTau::TauConstituent::t_OutNeut: return false;
  case PanTau::TauConstituent::t_OutChrg: return false;
  case PanTau::TauConstituent::t_NeutLowA: return true;
  case PanTau::TauConstituent::t_NeutLowB: return true;
=======

bool         PanTau::TauConstituent2::isCoreType(int tauConstituentType) {
  PanTau::TauConstituent2::Type type = (PanTau::TauConstituent2::Type)tauConstituentType;
  switch(type) {
  case PanTau::TauConstituent2::t_Charged: return true;
  case PanTau::TauConstituent2::t_Neutral: return true;
  case PanTau::TauConstituent2::t_Pi0Neut: return true;
  case PanTau::TauConstituent2::t_OutNeut: return false;
  case PanTau::TauConstituent2::t_OutChrg: return false;
  case PanTau::TauConstituent2::t_NeutLowA: return true;
  case PanTau::TauConstituent2::t_NeutLowB: return true;
>>>>>>> release/21.0.127
  default: return false;
  }
  return false;
}


<<<<<<< HEAD
//the non static getType name, which returns
std::vector<std::string> PanTau::TauConstituent::getTypeName() const {
  std::vector<std::string> res;
  for(unsigned int iType=0; iType<TauConstituent::t_nTypes; iType++) {
    if(m_TypeFlags[iType] == 1) {
      PanTau::TauConstituent::Type curType = (PanTau::TauConstituent::Type)iType;
      res.push_back( TauConstituent::getTypeName(curType) );
=======

//the non static getType name, which returns
std::vector<std::string>            PanTau::TauConstituent2::getTypeName() const {
  std::vector<std::string> res;
  for(unsigned int iType=0; iType<TauConstituent2::t_nTypes; iType++) {
    if(m_TypeFlags[iType] == 1) {
      PanTau::TauConstituent2::Type curType = (PanTau::TauConstituent2::Type)iType;
      res.push_back( TauConstituent2::getTypeName(curType) );
>>>>>>> release/21.0.127
    }
  }
  return res;
}


<<<<<<< HEAD
std::string PanTau::TauConstituent::getTypeNameString() const {
  std::string res;
  for(unsigned int iType=0; iType<m_TypeFlags.size(); iType++) {
    if(m_TypeFlags[iType] == 1) {
      res += PanTau::TauConstituent::getTypeName((PanTau::TauConstituent::Type)(iType)) + ",";
=======

std::string                         PanTau::TauConstituent2::getTypeNameString() const {
  std::string res;
  for(unsigned int iType=0; iType<m_TypeFlags.size(); iType++) {
    if(m_TypeFlags[iType] == 1) {
      res += PanTau::TauConstituent2::getTypeName((PanTau::TauConstituent2::Type)(iType)) + ",";
>>>>>>> release/21.0.127
    }
  }
  return res;
}


<<<<<<< HEAD
double PanTau::TauConstituent::getBDTValue() const {
=======

double PanTau::TauConstituent2::getBDTValue() const {
>>>>>>> release/21.0.127
  return m_BDTValue;
}


<<<<<<< HEAD
std::vector<int> PanTau::TauConstituent::getTypeFlags() const {
=======

std::vector<int> PanTau::TauConstituent2::getTypeFlags() const {
>>>>>>> release/21.0.127
  return m_TypeFlags;
}


<<<<<<< HEAD
bool PanTau::TauConstituent::isOfType(PanTau::TauConstituent::Type theType) const {
  int typeIndex = (int)theType;
  if(theType >= (int)TauConstituent::t_nTypes) {
=======
bool                                      PanTau::TauConstituent2::isOfType(PanTau::TauConstituent2::Type theType) const {
  int typeIndex = (int)theType;
  if(theType >= (int)TauConstituent2::t_nTypes) {
>>>>>>> release/21.0.127
    return false;
  }
  if(m_TypeFlags.at(typeIndex) == 1) return true;
  return false;
}


<<<<<<< HEAD
int PanTau::TauConstituent::getCharge() const {
=======
int PanTau::TauConstituent2::getCharge() const {
>>>>>>> release/21.0.127
  return m_Charge;
}


<<<<<<< HEAD
xAOD::PFO* PanTau::TauConstituent::getPFO() const {
=======

xAOD::PFO* PanTau::TauConstituent2::getPFO() const {
>>>>>>> release/21.0.127
  return m_PFOLink;
}


<<<<<<< HEAD
void PanTau::TauConstituent::addShot(TauConstituent* shot) {
=======

void                      PanTau::TauConstituent2::addShot(TauConstituent2* shot) {
>>>>>>> release/21.0.127
  if(shot != 0) m_Shots.push_back(shot);
}


<<<<<<< HEAD
std::vector<PanTau::TauConstituent*> PanTau::TauConstituent::getShots() {
  return m_Shots;
}


unsigned int PanTau::TauConstituent::getNShots() {
  return m_Shots.size();
}


void PanTau::TauConstituent::setNPhotonsInShot(int nPhotons) {
  m_nPhotonsInShot = nPhotons;
}


int PanTau::TauConstituent::getNPhotonsInShot() {
  return m_nPhotonsInShot;
}
=======
std::vector<PanTau::TauConstituent2*>            PanTau::TauConstituent2::getShots() {
  return m_Shots;
}

unsigned int                PanTau::TauConstituent2::getNShots() {
  return m_Shots.size();
}

void                        PanTau::TauConstituent2::setNPhotonsInShot(int nPhotons) {
  m_nPhotonsInShot = nPhotons;
}
int                         PanTau::TauConstituent2::getNPhotonsInShot() {
  return m_nPhotonsInShot;
}

>>>>>>> release/21.0.127
