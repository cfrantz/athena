///////////////////////// -*- C++ -*- /////////////////////////////

/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

// IsolationBuilder.cxx
// Implementation file for class IsolationBuilder
///////////////////////////////////////////////////////////////////

// Isolation includes
#include "IsolationBuilder.h"
#include "xAODEgamma/Egamma.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
<<<<<<< HEAD
#include "xAODEgamma/Photon.h"

IsolationBuilder::IsolationBuilder(const std::string& name,
                                   ISvcLocator* pSvcLocator)
  : ::AthReentrantAlgorithm(name, pSvcLocator)
{}
=======
#include "xAODEgamma/EgammaContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"
#include "xAODEgamma/Photon.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"
#include "xAODMuon/Muon.h"
#include "CaloEvent/CaloCellContainer.h"

#include "boost/foreach.hpp"
#include "boost/format.hpp"

#include <set>
#include <utility>

IsolationBuilder::IsolationBuilder( const std::string& name, 
			  ISvcLocator* pSvcLocator ) : 
  ::AthAlgorithm( name, pSvcLocator ),
  m_cellColl (nullptr)
{
  //
  // Property declaration
  // 
  declareProperty("ElectronCollectionContainerName",    m_ElectronContainerName    = "Electrons");
  declareProperty("PhotonCollectionContainerName",      m_PhotonContainerName      = "Photons");
  declareProperty("MuonCollectionContainerName",        m_MuonContainerName        = "Muons");
  declareProperty("FwdElectronCollectionContainerName", m_FwdElectronContainerName = "ForwardElectrons");
  declareProperty("CellCollectionName",              m_cellsName             = "AllCalo", "Name of container which contain calo cells");
  declareProperty("CaloCellIsolationTool",           m_cellIsolationTool,    "Handle of the calo cell IsolationTool");
  declareProperty("CaloTopoIsolationTool",           m_topoIsolationTool,    "Handle of the calo topo IsolationTool");
  declareProperty("PFlowIsolationTool",              m_pflowIsolationTool,   "Handle of the pflow IsolationTool");
  declareProperty("TrackIsolationTool",              m_trackIsolationTool,   "Handle of the track IsolationTool");
  declareProperty("useBremAssoc",                    m_useBremAssoc          = true, "use track to track assoc after brem");
  declareProperty("FeIsoTypes",                      m_feisoInts, "The isolation types to do for forward electron: vector of vector of enum type Iso::IsolationType, stored as float");
  declareProperty("FeCorTypes",                      m_fecorInts, "The correction types to do for forward electron iso: vector of vector of enum type Iso::IsolationCalo/TrackCorrection, stored as float");
  declareProperty("EgIsoTypes",                      m_egisoInts, "The isolation types to do for egamma: vector of vector of enum type Iso::IsolationType, stored as float");
  declareProperty("EgCorTypes",                      m_egcorInts, "The correction types to do for egamma iso: vector of vector of enum type Iso::IsolationCalo/TrackCorrection, stored as float");
  declareProperty("MuIsoTypes",                      m_muisoInts, "The isolation types to do for Muons : vector of vector of enum type Iso::IsolationType, stored as float");
  declareProperty("MuCorTypes",                      m_mucorInts, "The correction types to do for Muon iso: vector of vector of enum type Iso::IsolationCalo/TrackCorrection, stored as float");
  declareProperty("CustomConfigurationName",         m_customConfig          = "", "use a custom configuration");
  declareProperty("CustomConfigurationNameEl",       m_customConfigEl        = "", "use a custom configuration for electron");
  declareProperty("CustomConfigurationNamePh",       m_customConfigPh        = "", "use a custom configuration for photon");
  declareProperty("CustomConfigurationNameFwd",      m_customConfigFwd       = "", "use a custom configuration for forward electron");
  declareProperty("CustomConfigurationNameMu",       m_customConfigMu        = "", "use a custom configuration for muon");
  declareProperty("IsAODFix",                        m_isAODFix              = false);
  declareProperty("LeakageTool",                     m_leakTool,                   "Handle of the leakage Tool");
  declareProperty("IsolateEl",                       m_isolateEl             = true, "since egIsoTypes is common for el and ph, a new flag to control individually : electron");
  declareProperty("IsolatePh",                       m_isolatePh             = true, "since egIsoTypes is common for el and ph, a new flag to control individually : photon");
  declareProperty("AllTrackRemoval",                 m_allTrackRemoval       = true);
  
}
>>>>>>> release/21.0.127

IsolationBuilder::~IsolationBuilder() {}

StatusCode
IsolationBuilder::initialize()
{
  ATH_MSG_INFO("Initializing " << name() << "...");

  std::set<xAOD::Iso::IsolationFlavour> runIsoType;

<<<<<<< HEAD
  ATH_MSG_DEBUG("Initializing central electrons");
  ATH_CHECK(initializeIso(runIsoType,
                          &m_elCaloIso,
                          &m_elTrackIso,
                          m_ElectronContainerName,
                          m_elisoInts,
                          m_elcorInts,
                          m_elcorIntsExtra,
                          m_customConfigEl));

  ATH_MSG_DEBUG("Initializing central photons");
  ATH_CHECK(initializeIso(runIsoType,
                          &m_phCaloIso,
                          &m_phTrackIso,
                          m_PhotonContainerName,
                          m_phisoInts,
                          m_phcorInts,
                          m_phcorIntsExtra,
                          m_customConfigPh));

  ATH_MSG_DEBUG("Initializing forward electrons");
  ATH_CHECK(initializeIso(runIsoType,
                          &m_feCaloIso,
                          nullptr,
                          m_FwdElectronContainerName,
                          m_feisoInts,
                          m_fecorInts,
                          m_fecorIntsExtra,
                          m_customConfigFwd));

  ATH_MSG_DEBUG("Initializing muons");
  ATH_CHECK(initializeIso(runIsoType,
                          &m_muCaloIso,
                          &m_muTrackIso,
                          m_MuonContainerName,
                          m_muisoInts,
                          m_mucorInts,
                          m_mucorIntsExtra,
                          m_customConfigMu));

  // declare the dependencies
  // (need to do this since the WriteDecorHandleKeys are not properties
  declareIso(m_elCaloIso);
  declareIso(m_phCaloIso);
  declareIso(m_feCaloIso);
  declareIso(m_muCaloIso);

  declareIso(m_elTrackIso);
  declareIso(m_phTrackIso);
  declareIso(m_muTrackIso);
=======
  // Build helpers for all required isolations
  // For egamma (central)
  unsigned int nI = m_egisoInts.size();
  if (nI > 0 && m_egisoInts[0].size() > 0) 
    ATH_MSG_INFO("Will built egamma isolation types : ");
  else 
    nI = 0;
  for (unsigned int ii = 0; ii < nI; ii++) { // the various types : should be 4 by default (etcone, topoetcone, ptcone, neflowisol)
    std::vector<xAOD::Iso::IsolationType> isoTypes;
    std::vector<SG::AuxElement::Decorator<float>*> Deco;
    xAOD::Iso::IsolationFlavour isoFlav =
      xAOD::Iso::numIsolationFlavours;
    for (unsigned int jj = 0; jj < m_egisoInts[ii].size(); jj++) { // the size of the cone : should be 3 by default
      int egIso = int(m_egisoInts[ii][jj]+0.5);
      xAOD::Iso::IsolationType isoType = static_cast<xAOD::Iso::IsolationType>(egIso);
      isoTypes.push_back(isoType);
      isoFlav = xAOD::Iso::isolationFlavour(isoType);
      std::string isoName = xAOD::Iso::toString(isoType);
      if (m_customConfig != "") {
	isoName += "_"; isoName += m_customConfig;
	Deco.push_back(new SG::AuxElement::Decorator<float>(isoName));
      }
      ATH_MSG_INFO(isoName);
      if (isoFlav == xAOD::Iso::ptcone) {
	isoName = "ptvarcone";
	int is = 100*xAOD::Iso::coneSize(isoType);
	isoName += std::to_string(is);
	if (m_customConfig != "") {
	  isoName += "_"; isoName += m_customConfig;
	  Deco.push_back(new SG::AuxElement::Decorator<float>(isoName));
	}
	ATH_MSG_INFO(isoName);
      }
    }
    if (isoFlav == xAOD::Iso::etcone || isoFlav == xAOD::Iso::topoetcone || isoFlav == xAOD::Iso::neflowisol) {
      CaloIsoHelp cisoH;
      cisoH.isoTypes = isoTypes;
      if (m_customConfig != "") cisoH.isoDeco  = Deco;
      xAOD::CaloCorrection corrlist;
      for (unsigned int jj = 0; jj < m_egcorInts[ii].size(); jj++) {
        int egCor = int(m_egcorInts[ii][jj]+0.5);
	corrlist.calobitset.set(static_cast<unsigned int>(egCor));
      }
      cisoH.CorrList = corrlist;
      m_egCaloIso.insert(std::make_pair(isoFlav,cisoH));
    } else if (isoFlav == xAOD::Iso::ptcone) {
      TrackIsoHelp tisoH;
      tisoH.isoTypes = isoTypes;
      if (m_customConfig != "") tisoH.isoDeco  = Deco;
      xAOD::TrackCorrection corrlist;
      corrlist.trackbitset.set(static_cast<unsigned int>(xAOD::Iso::coreTrackPtr));
      tisoH.CorrList = corrlist;
      m_egTrackIso.insert(std::make_pair(isoFlav,tisoH));
    } else 
      ATH_MSG_WARNING("Isolation flavour " << xAOD::Iso::toString(isoFlav) << " does not exist ! Check your inputs");
    if (runIsoType.find(isoFlav) == runIsoType.end()) runIsoType.insert(isoFlav);
  }

  // For forward electrons
  nI = m_feisoInts.size();
  if (nI > 0 && m_feisoInts[0].size() > 0) 
    ATH_MSG_INFO("Will built forward electron isolation types : ");
  else 
    nI = 0;
  for (unsigned int ii = 0; ii < nI; ii++) { // here, should be at most three (no ptcone)
    std::vector<xAOD::Iso::IsolationType> isoTypes;
    std::vector<SG::AuxElement::Decorator<float>*> Deco;
    xAOD::Iso::IsolationFlavour isoFlav =
      xAOD::Iso::numIsolationFlavours;
    for (unsigned int jj = 0; jj < m_feisoInts[ii].size(); jj++) {
      int egIso = int(m_feisoInts[ii][jj]+0.5);
      xAOD::Iso::IsolationType isoType = static_cast<xAOD::Iso::IsolationType>(egIso);
      isoTypes.push_back(isoType);
      isoFlav = xAOD::Iso::isolationFlavour(isoType);
      std::string isoName = xAOD::Iso::toString(isoType);
      if (m_customConfig != "") {
	isoName += "_"; isoName += m_customConfig;
	Deco.push_back(new SG::AuxElement::Decorator<float>(isoName));
      }
      ATH_MSG_INFO(isoName);
    }
    if (isoFlav == xAOD::Iso::etcone || isoFlav == xAOD::Iso::topoetcone || isoFlav == xAOD::Iso::neflowisol) {
      CaloIsoHelp cisoH;
      cisoH.isoTypes = isoTypes;
      if (m_customConfig != "") cisoH.isoDeco  = Deco;
      xAOD::CaloCorrection corrlist;
      for (unsigned int jj = 0; jj < m_fecorInts[ii].size(); jj++) {
        int egCor = int(m_fecorInts[ii][jj]+0.5);
	corrlist.calobitset.set(static_cast<unsigned int>(egCor));
      }
      cisoH.CorrList = corrlist;
      m_feCaloIso.insert(std::make_pair(isoFlav,cisoH));
    } else 
      ATH_MSG_WARNING("Isolation flavour " << xAOD::Iso::toString(isoFlav) << " does not exist ! Check your inputs");
    if (runIsoType.find(isoFlav) == runIsoType.end()) runIsoType.insert(isoFlav);
  }

  // For muons
  nI = m_muisoInts.size();
  if (nI > 0 && m_muisoInts[0].size() > 0) 
    ATH_MSG_INFO("Will built muon isolation types : ");
  else 
    nI = 0;
  for (unsigned int ii = 0; ii < nI; ii++) {   // the flavor (cell, topo, eflow, track
    std::vector<xAOD::Iso::IsolationType> isoTypes;
    std::vector<SG::AuxElement::Decorator<float>*> Deco;
    xAOD::Iso::IsolationFlavour isoFlav =
      xAOD::Iso::numIsolationFlavours;
    for (unsigned int jj = 0; jj < m_muisoInts[ii].size(); jj++) { // the cone size
      int muIso = int(m_muisoInts[ii][jj]+0.5);
      xAOD::Iso::IsolationType isoType = static_cast<xAOD::Iso::IsolationType>(muIso);
      isoTypes.push_back(isoType);
      isoFlav = xAOD::Iso::isolationFlavour(isoType);
      std::string isoName = xAOD::Iso::toString(isoType);
      if (m_customConfigMu != "") {
	isoName += "_"; isoName += m_customConfigMu;
	Deco.push_back(new SG::AuxElement::Decorator<float>(isoName));
      }
      ATH_MSG_INFO(isoName);
      if (isoFlav == xAOD::Iso::ptcone) {
	isoName = "ptvarcone";
	int is = 100*xAOD::Iso::coneSize(isoType);
	isoName += std::to_string(is);
	if (m_customConfigMu != "") {
	  isoName += "_"; isoName += m_customConfigMu;
	  Deco.push_back(new SG::AuxElement::Decorator<float>(isoName));
	}
	ATH_MSG_INFO(isoName);
      }
    }
    if (isoFlav == xAOD::Iso::etcone || isoFlav == xAOD::Iso::topoetcone || isoFlav == xAOD::Iso::neflowisol) {
      CaloIsoHelp cisoH;
      cisoH.isoTypes = isoTypes;
      cisoH.coreCorisoDeco = nullptr;
      if (m_customConfigMu != "") cisoH.isoDeco  = Deco;
      xAOD::CaloCorrection corrlist;
      for (unsigned int jj = 0; jj < m_mucorInts[ii].size(); jj++) {
	int muCor = int(m_mucorInts[ii][jj]+0.5);
	corrlist.calobitset.set(static_cast<unsigned int>(muCor));
	xAOD::Iso::IsolationCaloCorrection isoCor = static_cast<xAOD::Iso::IsolationCaloCorrection>(muCor);
	if (m_customConfigMu != "" && (isoCor == xAOD::Iso::coreCone || isoCor == xAOD::Iso::coreMuon) ) {
	  std::string isoCorName = "";
	  if (isoFlav == xAOD::Iso::topoetcone || isoFlav == xAOD::Iso::neflowisol)
	    isoCorName = xAOD::Iso::toString(isoFlav);
	  isoCorName += xAOD::Iso::toString(isoCor);
	  isoCorName += xAOD::Iso::toString(xAOD::Iso::coreEnergy); isoCorName += "Correction"; // hard coded since we never store the core area in fact
	  isoCorName += "_"; isoCorName += m_customConfigMu;
	  cisoH.coreCorisoDeco = new SG::AuxElement::Decorator<float>(isoCorName);
      }
      }
      cisoH.CorrList = corrlist;
      m_muCaloIso.insert(std::make_pair(isoFlav,cisoH));
    } else if (isoFlav == xAOD::Iso::ptcone) {
      TrackIsoHelp tisoH;
      tisoH.isoTypes = isoTypes;
      if (m_customConfigMu != "") tisoH.isoDeco  = Deco;
      xAOD::TrackCorrection corrlist;
      corrlist.trackbitset.set(static_cast<unsigned int>(xAOD::Iso::coreTrackPtr));
      tisoH.CorrList = corrlist;
      m_muTrackIso.insert(std::make_pair(isoFlav,tisoH));
    } else 
      ATH_MSG_WARNING("Isolation flavour " << xAOD::Iso::toString(isoFlav) << " does not exist ! Check your inputs");
    if (runIsoType.find(isoFlav) == runIsoType.end()) runIsoType.insert(isoFlav);
  }
>>>>>>> release/21.0.127

  // Retrieve the tools (there three Calo ones are the same in fact)
  if (!m_cellIsolationTool.empty() &&
      runIsoType.find(xAOD::Iso::etcone) != runIsoType.end()) {
    ATH_CHECK(m_cellIsolationTool.retrieve());
  } else {
    m_cellIsolationTool.disable();
  }

  if (!m_topoIsolationTool.empty() &&
      runIsoType.find(xAOD::Iso::topoetcone) != runIsoType.end()) {
    ATH_CHECK(m_topoIsolationTool.retrieve());
  } else {
    m_topoIsolationTool.disable();
  }

  if (!m_pflowIsolationTool.empty() &&
      runIsoType.find(xAOD::Iso::neflowisol) != runIsoType.end()) {
    ATH_CHECK(m_pflowIsolationTool.retrieve());
  } else {
    m_pflowIsolationTool.disable();
  }

  if (!m_trackIsolationTool.empty() &&
      runIsoType.find(xAOD::Iso::IsolationFlavour::ptcone) !=
        runIsoType.end()) {
    ATH_CHECK(m_trackIsolationTool.retrieve());
  } else {
    m_trackIsolationTool.disable();
  }

  // initialise data handles
  ATH_CHECK(m_cellsKey.initialize(!m_cellIsolationTool.empty()));


  // Also can apply leakage correction if AODFix :
  // be carefull ! There is a leakage in topoetcone, etcone, ... : do not run this !!!!!! (useless)
  if (m_isAODFix && !m_leakTool.empty()) {
    ATH_MSG_INFO("Will run leakage corrections for photons and electrons");
    ATH_CHECK(m_leakTool.retrieve());
  }

  return StatusCode::SUCCESS;
}

StatusCode
IsolationBuilder::finalize()
{
<<<<<<< HEAD
  ATH_MSG_INFO("Finalizing " << name() << "...");
=======
  ATH_MSG_INFO ("Finalizing " << name() << "...");

 // Delete pointers to decorators
  std::map<xAOD::Iso::IsolationFlavour,CaloIsoHelp>::iterator itc = m_egCaloIso.begin(), itcE = m_egCaloIso.end();
  for (; itc != itcE; itc++) {
    std::vector<SG::AuxElement::Decorator<float>*> vec = itc->second.isoDeco;
    for (unsigned int ii = 0; ii < vec.size(); ii++)
      delete vec[ii];
  }
  std::map<xAOD::Iso::IsolationFlavour,TrackIsoHelp>::iterator itt = m_egTrackIso.begin(), ittE = m_egTrackIso.end();
  for (; itt != ittE; itt++) {
    std::vector<SG::AuxElement::Decorator<float>*> vec = itt->second.isoDeco;
    for (unsigned int ii = 0; ii < vec.size(); ii++)
      delete vec[ii];
  }
  itc = m_muCaloIso.begin(), itcE = m_muCaloIso.end();
  for (; itc != itcE; itc++) {
    if (itc->second.coreCorisoDeco != nullptr)      // it is only there for coreCone or coreMuon and if decoration
      delete itc->second.coreCorisoDeco;            // only implemented for muon calo iso for the time being...
    std::vector<SG::AuxElement::Decorator<float>*> vec = itc->second.isoDeco;
    for (unsigned int ii = 0; ii < vec.size(); ii++)
      delete vec[ii];
  }
  itt = m_muTrackIso.begin(), ittE = m_muTrackIso.end();
  for (; itt != ittE; itt++) {
    std::vector<SG::AuxElement::Decorator<float>*> vec = itt->second.isoDeco;
    for (unsigned int ii = 0; ii < vec.size(); ii++)
      delete vec[ii];
  }
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}

StatusCode
IsolationBuilder::execute(const EventContext& ctx) const
{
  ATH_MSG_DEBUG("Executing " << name() << "...");

  // For etcone, needs the cells

  const CaloCellContainer* cellColl = nullptr;
  if (!m_cellIsolationTool.empty()) {
    SG::ReadHandle<CaloCellContainer> cellcoll(m_cellsKey, ctx);
    // check is only used for serial running; remove when MT scheduler used
    if (!cellcoll.isValid()) {
      ATH_MSG_FATAL("Failed to retrieve cell container: " << m_cellsKey.key());
      return StatusCode::FAILURE;
    }
    cellColl = cellcoll.cptr();
  }
<<<<<<< HEAD
  // Compute isolations

  ATH_MSG_DEBUG("About to execute Electron calo iso");
  ATH_CHECK(executeCaloIso(m_elCaloIso, cellColl));
  ATH_MSG_DEBUG("About to execute Photon calo iso");
  ATH_CHECK(executeCaloIso(m_phCaloIso, cellColl));
  ATH_MSG_DEBUG("About to execute Forwerd Electron calo iso");
  ATH_CHECK(executeCaloIso(m_feCaloIso, cellColl));
  ATH_MSG_DEBUG("About to execute muon calo iso");
  ATH_CHECK(executeCaloIso(m_muCaloIso, cellColl));

  ATH_MSG_DEBUG("About to execute Electron track iso");
  ATH_CHECK(executeTrackIso(m_elTrackIso));
  ATH_MSG_DEBUG("About to execute Photon track iso");
  ATH_CHECK(executeTrackIso(m_phTrackIso));
  ATH_MSG_DEBUG("About to execute Muon track iso");
  ATH_CHECK(executeTrackIso(m_muTrackIso));
=======

  // If AODFix, first deep copy
  if (m_isAODFix) {
    if (m_ElectronContainerName.size()) {
      if (!evtStore()->tryRetrieve<xAOD::ElectronContainer>(m_ElectronContainerName)) {
	if( deepCopy<xAOD::ElectronContainer,xAOD::ElectronAuxContainer>(m_ElectronContainerName).isFailure()) {
	  ATH_MSG_FATAL( "Couldn't deep copy electrons" );
	  return StatusCode::FAILURE;
	}
      }
    }
    if (m_FwdElectronContainerName.size()) {
      if (!evtStore()->tryRetrieve<xAOD::ElectronContainer>(m_FwdElectronContainerName)) {
	if( deepCopy<xAOD::ElectronContainer,xAOD::ElectronAuxContainer>(m_FwdElectronContainerName).isFailure()) {
	  ATH_MSG_FATAL( "Couldn't deep copy forward electrons" );
	  return StatusCode::FAILURE;
	}
      }
    }
    if (m_PhotonContainerName.size()) {
      if (!evtStore()->tryRetrieve<xAOD::PhotonContainer>(m_PhotonContainerName)) {
	if( deepCopy<xAOD::PhotonContainer,xAOD::PhotonAuxContainer>(m_PhotonContainerName).isFailure()) {
	  ATH_MSG_FATAL( "Couldn't deep copy photons" );
	  return StatusCode::FAILURE;
	}
      }
    }
    if (m_MuonContainerName.size()) {
      if (!evtStore()->tryRetrieve<xAOD::MuonContainer>(m_MuonContainerName)) {
	if( deepCopy<xAOD::MuonContainer,xAOD::MuonAuxContainer>(m_MuonContainerName).isFailure()) {
	  ATH_MSG_FATAL( "Couldn't deep copy muons" );
	  return StatusCode::FAILURE;
	}
      }
    }
  }

  // Compute isolations
  /*
  if (m_customConfig == "") { 
    // Here, default configurations with standard names can be are computed
    if (m_egCaloIso.size() != 0 || m_egTrackIso.size() != 0) {
      if (m_ElectronContainerName.size()) CHECK(IsolateEgamma("electron"));
      if (m_PhotonContainerName.size())   CHECK(IsolateEgamma("photon"));
    }
    if (m_feCaloIso.size() != 0)
      if (m_FwdElectronContainerName.size()) CHECK(IsolateEgamma("fwdelectron"));
      
    if (m_muCaloIso.size() != 0 || m_muTrackIso.size() != 0)
      if (m_MuonContainerName.size()) CHECK(IsolateMuon());
  } else {
    // Here, custom configurations with custom names can be computed
    if (m_egCaloIso.size() != 0 || m_egTrackIso.size() != 0) {
      if (m_ElectronContainerName.size()) CHECK(DecorateEgamma("electron"));
      if (m_PhotonContainerName.size())   CHECK(DecorateEgamma("photon"));
    }
    if (m_feCaloIso.size() != 0)
      if (m_FwdElectronContainerName.size()) CHECK(DecorateEgamma("fwdelectron"));

    if (m_muCaloIso.size() != 0 || m_muTrackIso.size() != 0)
      if (m_MuonContainerName.size()) CHECK(DecorateMuon());
  }
  */
  if (m_egCaloIso.size() != 0 || m_egTrackIso.size() != 0) {
    if (m_isolateEl && m_ElectronContainerName.size()) {
      if (m_customConfigEl == "")
	CHECK(IsolateEgamma("electron"));
      else
	CHECK(DecorateEgamma("electron"));
    }
    if (m_isolatePh && m_PhotonContainerName.size()) {
      if (m_customConfigPh == "")
	CHECK(IsolateEgamma("photon"));
      else
	CHECK(DecorateEgamma("photon"));
    }
  }
  if (m_feCaloIso.size() != 0 && m_FwdElectronContainerName.size()) {
    if (m_customConfigFwd == "")
      CHECK(IsolateEgamma("fwdelectron"));
    else
      CHECK(DecorateEgamma("fwdelectron"));
  }
  if ( (m_muCaloIso.size() != 0 || m_muTrackIso.size() != 0) && m_MuonContainerName.size()) {
    if (m_customConfigMu == "")
      CHECK(IsolateMuon());
    else
      CHECK(DecorateMuon());
  }
>>>>>>> release/21.0.127

  if (m_isAODFix && !m_leakTool.empty())
    CHECK(runLeakage());

  
  return StatusCode::SUCCESS;
}

// constructor
IsolationBuilder::CaloIsoHelpKey::CaloIsoHelpKey(IDataHandleHolder* owningAlg)
{
  isoDeco.setOwner(owningAlg);
  corrBitsetDeco.setOwner(owningAlg);
}

// declare dependencies
void
IsolationBuilder::CaloIsoHelpKey::declare(IDataHandleHolder* owningAlg)
{
  isoDeco.declare(owningAlg);
  owningAlg->declare(corrBitsetDeco);

  for (auto& coreCor : coreCorDeco) {
    owningAlg->declare(coreCor.second);
  }

  for (auto& noncoreCor : noncoreCorDeco) {
    noncoreCor.second.declare(owningAlg);
  }
}

// constructor
IsolationBuilder::TrackIsoHelpKey::TrackIsoHelpKey(IDataHandleHolder* owningAlg)
{
  isoDeco.setOwner(owningAlg);
  isoDecoV.setOwner(owningAlg);
  corrBitsetDeco.setOwner(owningAlg);
}

// declare dependencies
void
IsolationBuilder::TrackIsoHelpKey::declare(IDataHandleHolder* owningAlg)
{
  isoDeco.declare(owningAlg);
  isoDecoV.declare(owningAlg);
  owningAlg->declare(corrBitsetDeco);

  for (auto& coreCor : coreCorDeco) {
    owningAlg->declare(coreCor.second);
  }
}

// constructor
IsolationBuilder::CaloIsoHelpHandles::CaloIsoHelpHandles(
  const IsolationBuilder::CaloIsoHelpKey& keys)
  : corrBitsetDeco(keys.corrBitsetDeco)
{
  for (const auto& key : keys.isoDeco) {
    isoDeco.emplace_back(key);
  }
  for (const auto& coreCor : keys.coreCorDeco) {
    coreCorDeco.emplace(coreCor);
  }
  for (const auto& noncoreCor : keys.noncoreCorDeco) {
    noncoreCorDeco.emplace(
      noncoreCor.first,
      std::vector<SG::WriteDecorHandle<xAOD::IParticleContainer, float>>{
        std::begin(noncoreCor.second), std::end(noncoreCor.second) });
  }
}

IsolationBuilder::TrackIsoHelpHandles::TrackIsoHelpHandles(
  const IsolationBuilder::TrackIsoHelpKey& keys)
  : corrBitsetDeco(keys.corrBitsetDeco)
{
  for (const auto& key : keys.isoDeco) {
    isoDeco.emplace_back(key);
  }
  for (const auto& key : keys.isoDecoV) {
    isoDecoV.emplace_back(key);
  }
  for (const auto& coreCor : keys.coreCorDeco) {
    coreCorDeco.emplace(coreCor);
  }
}

bool
IsolationBuilder::isCoreCor(xAOD::Iso::IsolationCaloCorrection cor)
{
  return (cor == xAOD::Iso::coreCone || cor == xAOD::Iso::coreConeSC ||
          cor == xAOD::Iso::coreMuon || cor == xAOD::Iso::core57cells);
}

StatusCode
IsolationBuilder::initializeIso(
  std::set<xAOD::Iso::IsolationFlavour>& runIsoType, // out
  std::vector<std::pair<xAOD::Iso::IsolationFlavour, CaloIsoHelpKey>>*
    caloIsoMap, // out
  std::vector<std::pair<xAOD::Iso::IsolationFlavour, TrackIsoHelpKey>>*
    trackIsoMap, // out
  const std::string& containerName,
  const std::vector<std::vector<int>>& isoInts,
  const std::vector<std::vector<int>>& corInts,
  const std::vector<std::vector<int>>& corIntsExtra,
  const std::string& customConfig)
{

  std::string prefix = containerName + ".";

  for (size_t flavor = 0; flavor < isoInts.size(); flavor++) {
    // iterate over the flavor (cell, topo, eflow, track
    //   Note: it is a configuration error if different types
    //         are included in one inner vector

    CaloIsoHelpKey cisoH(this);
    TrackIsoHelpKey tisoH(this);

    // std::vector<SG::AuxElement::Decorator<float>*> Deco;
    xAOD::Iso::IsolationFlavour isoFlav = xAOD::Iso::numIsolationFlavours;
    xAOD::Iso::IsolationFlavour oldIsoFlav = xAOD::Iso::numIsolationFlavours;

    for (size_t type = 0; type < isoInts[flavor].size(); type++) {
      // iterate over the cone sizes for a given flavor.
      // (also check that the cone sizes really are of the same flavor;
      // otherwise an error)

      xAOD::Iso::IsolationType isoType =
        static_cast<xAOD::Iso::IsolationType>(isoInts[flavor][type]);
      isoFlav = xAOD::Iso::isolationFlavour(isoType);
      ATH_MSG_DEBUG("Saw isoType " << isoType << " and isoFlav " << isoFlav);
      if (oldIsoFlav != xAOD::Iso::numIsolationFlavours &&
          oldIsoFlav != isoFlav) {
        ATH_MSG_FATAL("Configuration error:  can only have one type of "
                      "isolation in inner vector");
        return StatusCode::FAILURE;
      }
      oldIsoFlav = isoFlav;
      std::string isoName = prefix + xAOD::Iso::toString(isoType);
      if (!customConfig.empty()) {
        isoName += "_" + customConfig;
      }
      if (isoFlav == xAOD::Iso::etcone || isoFlav == xAOD::Iso::topoetcone ||
          isoFlav == xAOD::Iso::neflowisol) {
        cisoH.isoTypes.push_back(isoType);
        cisoH.isoDeco.emplace_back(isoName);
      } else if (isoFlav == xAOD::Iso::ptcone) {
        tisoH.isoTypes.push_back(isoType);
        tisoH.isoDeco.emplace_back(isoName);
        auto coneSize =
          static_cast<int>(round(100 * xAOD::Iso::coneSize(isoType)));
        std::string isoNameV = prefix + "ptvarcone" + std::to_string(coneSize);
        if (!customConfig.empty()) {
          isoNameV += "_" + customConfig;
        }
        tisoH.isoDecoV.emplace_back(isoNameV);
      } else {
        ATH_MSG_FATAL("Configuration error: Isolation flavor "
                      << isoFlav << " not supported.");
        return StatusCode::FAILURE;
      }
<<<<<<< HEAD
    }

    // check that there were isolations configured
    if (isoFlav == xAOD::Iso::numIsolationFlavours) {
      ATH_MSG_WARNING("The configuration was malformed: an empty inner vector "
                      "was added; ignoring");
      continue;
=======
    } else {
      ATH_MSG_DEBUG("PhotonContainer " << m_PhotonContainerName << " not available");
      return StatusCode::SUCCESS;
    }
  } else if (egType == "fwdelectron") {
    if (evtStore()->contains<xAOD::ElectronContainer>(m_FwdElectronContainerName)) {
      if (evtStore()->retrieve(egC,m_FwdElectronContainerName).isFailure()) {
	ATH_MSG_FATAL("Cannot retrieve forward electron container " << m_FwdElectronContainerName);
	return StatusCode::FAILURE;
      }
    } else {
      ATH_MSG_DEBUG("Forward ElectronContainer " << m_FwdElectronContainerName << " not available");
      return StatusCode::SUCCESS;
    }
  } else {
    ATH_MSG_WARNING("Unknown egamma type " << egType);
    return StatusCode::SUCCESS;
  }
  xAOD::EgammaContainer::iterator it = egC->begin(), itE = egC->end();
  for (; it != itE; it++) {
    xAOD::Egamma *eg = *it; 
    //
    ATH_MSG_DEBUG(egType << " pt,eta,phi = " << eg->pt()/1e3 << " " << eg->eta() << " " << eg->phi());
    // 
    // Calo Isolation types
    //
    std::map<xAOD::Iso::IsolationFlavour,CaloIsoHelp>::iterator itc = m_egCaloIso.begin(), itcE = m_egCaloIso.end();
    if (egType == "fwdelectron") {
      itc  = m_feCaloIso.begin();
      itcE = m_feCaloIso.end();
    }
    for (; itc != itcE; itc++) {
      CaloIsoHelp isoH = itc->second;
      xAOD::Iso::IsolationFlavour flav = itc->first;
      bool bsc = false;
      if (flav == xAOD::Iso::etcone && m_cellColl) 
	bsc = m_cellIsolationTool->decorateParticle_caloCellIso(*eg, isoH.isoTypes, isoH.CorrList, m_cellColl);
      else if (flav == xAOD::Iso::topoetcone)
	bsc = m_topoIsolationTool->decorateParticle_topoClusterIso(*eg, isoH.isoTypes, isoH.CorrList);
      else if (flav == xAOD::Iso::neflowisol)
	bsc = m_pflowIsolationTool->decorateParticle_eflowIso(*eg, isoH.isoTypes, isoH.CorrList);
      if (bsc) {
	for (unsigned int i = 0; i < isoH.isoTypes.size(); i++) {
	  float iso = 0;
	  bool gotIso  = eg->isolationValue(iso,isoH.isoTypes[i]);
	  if (gotIso)
	    ATH_MSG_DEBUG("Iso " << xAOD::Iso::toString(isoH.isoTypes[i]) << " = " << iso/1e3);
	  else
	    ATH_MSG_WARNING("Missing isolation result for " << isoH.isoTypes[i]);
	}
      } else
	ATH_MSG_WARNING("Call to CaloIsolationTool failed for flavour " << xAOD::Iso::toString(flav));
    }
    if (egType == "fwdelectron")
      return StatusCode::SUCCESS;
    // 
    // Track Isolation types
    //
    std::map<xAOD::Iso::IsolationFlavour,TrackIsoHelp>::iterator itt = m_egTrackIso.begin(), ittE = m_egTrackIso.end();
    for (; itt != ittE; itt++) {
      TrackIsoHelp isoH = itt->second;
      //const std::set<const xAOD::TrackParticle*> tracksToExclude = xAOD::EgammaHelpers::getTrackParticles(eg, m_useBremAssoc);
      std::set<const xAOD::TrackParticle*> tracksToExclude;
      if (eg->type() == xAOD::Type::Electron)
	tracksToExclude = xAOD::EgammaHelpers::getTrackParticles(eg, m_useBremAssoc);
      else {
	if (m_allTrackRemoval) //New (from ??/??/16) : now this gives all tracks
	  tracksToExclude = xAOD::EgammaHelpers::getTrackParticles(eg, m_useBremAssoc);
	else { // this is just to be able to have the 2015+2016 default case (only tracks from first vertex)
	  const xAOD::Photon *gam = dynamic_cast<const xAOD::Photon *>(eg);
	  if (gam->nVertices() > 0) {
	    const xAOD::Vertex *phvtx = gam->vertex(0);
	    for (unsigned int itk = 0; itk < phvtx->nTrackParticles(); itk++)
	      tracksToExclude.insert(m_useBremAssoc ? xAOD::EgammaHelpers::getOriginalTrackParticleFromGSF(phvtx->trackParticle(itk)) : phvtx->trackParticle(itk));
	  }
	}
      }
      xAOD::Vertex *vx = 0;
      bool bsc = m_trackIsolationTool->decorateParticle(*eg, isoH.isoTypes, isoH.CorrList, vx, &tracksToExclude);
      if (bsc) {
	unsigned int nI = isoH.isoTypes.size();
	for (unsigned int i = 0; i < nI; i++) {
	  xAOD::Iso::IsolationConeSize coneSize = enumconeSize(isoH.isoTypes[i]);
	  xAOD::Iso::IsolationType varIsoType   = xAOD::Iso::isolationType(xAOD::Iso::ptvarcone, coneSize);
	  float iso = 0, isoV = 0;
	  bool gotIso  = eg->isolationValue(iso,isoH.isoTypes[i]);
	  bool gotIsoV = eg->isolationValue(isoV,varIsoType);
	  if (gotIso && gotIsoV)
	    ATH_MSG_DEBUG("Iso " << xAOD::Iso::toString(isoH.isoTypes[i]) << " = " << iso/1e3 << ", var cone = " << isoV/1e3);
	  else
	    ATH_MSG_WARNING("Missing isolation result " << gotIso << " " << gotIsoV);
	}
      } else
	ATH_MSG_WARNING("Call to TrackIsolationTool failed");
>>>>>>> release/21.0.127
    }

    ///////////////////////////////
    // Now that the isolations to calculate are determined,
    // initialize the isolation decorations
    // and then determine the corrections to apply,
    // and finally add it to the IsoMap.
    ///////////////////////////////

    if (isoFlav == xAOD::Iso::etcone || isoFlav == xAOD::Iso::topoetcone ||
        isoFlav == xAOD::Iso::neflowisol) {

      // let's initialize the decos
      ATH_MSG_DEBUG("Initializing cisoH.isoDeco");
      ATH_CHECK(cisoH.isoDeco.initialize());

      ATH_CHECK(addCaloIsoCorrections(
        flavor, isoFlav, cisoH, corInts, false, prefix, customConfig));
      ATH_CHECK(addCaloIsoCorrections(
        flavor, isoFlav, cisoH, corIntsExtra, true, prefix, customConfig));

      if (caloIsoMap) {
        caloIsoMap->push_back(std::make_pair(isoFlav, cisoH));
      } else {
        ATH_MSG_FATAL(
          "caloIsoMap was nullptr but the configuration attempted to use it");
        return StatusCode::FAILURE;
      }
    } else if (isoFlav == xAOD::Iso::ptcone) {

      // let's initialize the decos
      ATH_MSG_DEBUG("Initializing tisoH.isoDeco");
      ATH_CHECK(tisoH.isoDeco.initialize());
      ATH_MSG_DEBUG("Initializing tisoH.isoDecoV");
      ATH_CHECK(tisoH.isoDecoV.initialize());

      ATH_CHECK(addTrackIsoCorrections(
        flavor, isoFlav, tisoH, corInts, false, prefix, customConfig));
      ATH_CHECK(addTrackIsoCorrections(
        flavor, isoFlav, tisoH, corIntsExtra, true, prefix, customConfig));

      if (trackIsoMap) {
        trackIsoMap->push_back(std::make_pair(isoFlav, tisoH));
      } else {
        ATH_MSG_FATAL(
          "trackIsoMap was nullptr but the configuration attempted to use it");
        return StatusCode::FAILURE;
      }
    } else {
      ATH_MSG_WARNING("Isolation flavour "
                      << xAOD::Iso::toCString(isoFlav)
                      << " does not exist ! Check your inputs");
    }
    runIsoType.insert(isoFlav);
  }
  return StatusCode::SUCCESS;
}

StatusCode
IsolationBuilder::addCaloIsoCorrections(
  size_t flavor,
  xAOD::Iso::IsolationFlavour isoFlav,
  CaloIsoHelpKey& cisoH,
  const std::vector<std::vector<int>>& corInts,
  bool corrsAreExtra,
  const std::string& prefix,
  const std::string& customConfig)
{

  if (!corrsAreExtra) {
    std::string bitsetName =
      prefix + xAOD::Iso::toString(isoFlav) + "CorrBitset";
    if (!customConfig.empty()) {
      bitsetName += "_" + customConfig;
    }

    cisoH.corrBitsetDeco = bitsetName;
    ATH_MSG_DEBUG("Initializing " << cisoH.corrBitsetDeco.key());
    ATH_CHECK(cisoH.corrBitsetDeco.initialize());
  }

  for (size_t corrType = 0; corrType < corInts[flavor].size(); corrType++) {

    // iterate over the calo isolation corrections
    const auto cor = static_cast<unsigned int>(corInts[flavor][corrType]);
    if (!corrsAreExtra)
      cisoH.CorrList.calobitset.set(cor);
    const xAOD::Iso::IsolationCaloCorrection isoCor =
      static_cast<xAOD::Iso::IsolationCaloCorrection>(cor);

    if (isCoreCor(isoCor)) {
      std::string isoCorName = prefix;

      if (isoCor != xAOD::Iso::core57cells) {
        isoCorName += xAOD::Iso::toString(
          isoFlav); // since this doesn't depend on the flavor, just have one
      }

      // a core correction; only store core energy, not the core area
      isoCorName += xAOD::Iso::toString(isoCor) +
                    xAOD::Iso::toString(xAOD::Iso::coreEnergy) + "Correction";
      if (!customConfig.empty()) {
        isoCorName += "_" + customConfig;
      }
      cisoH.coreCorDeco.emplace(isoCor, isoCorName);
      cisoH.coreCorDeco[isoCor].setOwner(this);
      ATH_MSG_DEBUG("initializing " << cisoH.coreCorDeco[isoCor].key());
      ATH_CHECK(cisoH.coreCorDeco[isoCor].initialize());
    } else if (isoCor == xAOD::Iso::pileupCorrection) {
      // do not store pileup corrections as they are rho * pi * (R**2 -
      // areaCore) and rho is stored...
      continue;
    } else {
<<<<<<< HEAD
      // noncore correction
      cisoH.noncoreCorDeco.emplace(
        isoCor, SG::WriteDecorHandleKeyArray<xAOD::IParticleContainer>());
      auto& vec = cisoH.noncoreCorDeco[isoCor];
      vec.setOwner(this);
      for (auto type : cisoH.isoTypes) {
        std::string corName = prefix + xAOD::Iso::toString(type) +
                              xAOD::Iso::toString(isoCor) + "Correction";
        if (!customConfig.empty()) {
          corName += "_" + customConfig;
        }
        vec.emplace_back(corName);
      }
      ATH_MSG_DEBUG("Initializing " << xAOD::Iso::toString(isoCor)
                                    << " Corrections");
      ATH_CHECK(vec.initialize());
=======
      ATH_MSG_DEBUG("PhotonContainer " << m_PhotonContainerName << " not available");
      return StatusCode::SUCCESS;
    } 
  } else if (egType == "fwdelectron") {
    if (evtStore()->contains<xAOD::ElectronContainer>(m_FwdElectronContainerName)) {
      if (evtStore()->retrieve(egC,m_FwdElectronContainerName).isFailure()) {
	ATH_MSG_FATAL("Cannot retrieve forward electron container " << m_FwdElectronContainerName);
	return StatusCode::FAILURE;
      }
    } else {
      ATH_MSG_DEBUG("Forward ElectronContainer " << m_FwdElectronContainerName << " not available");
      return StatusCode::SUCCESS;
>>>>>>> release/21.0.127
    }
  }
<<<<<<< HEAD
  return StatusCode::SUCCESS;
}

StatusCode
IsolationBuilder::addTrackIsoCorrections(
  size_t flavor,
  xAOD::Iso::IsolationFlavour isoFlav,
  TrackIsoHelpKey& tisoH,
  const std::vector<std::vector<int>>& corInts,
  bool corrsAreExtra,
  const std::string& prefix,
  const std::string& customConfig)
{

  if (!corrsAreExtra) {
    std::string bitsetName =
      prefix + xAOD::Iso::toString(isoFlav) + "CorrBitset";
    if (!customConfig.empty()) {
      bitsetName += "_" + customConfig;
=======
  xAOD::EgammaContainer::iterator it = egC->begin(), itE = egC->end();
  for (; it != itE; it++) {
    xAOD::Egamma *eg = *it; 
    //
    ATH_MSG_DEBUG(egType << " pt,eta,phi = " << eg->pt()/1e3 << " " << eg->eta() << " " << eg->phi());
    // 
    // Calo Isolation types
    //
    std::map<xAOD::Iso::IsolationFlavour,CaloIsoHelp>::iterator itc = m_egCaloIso.begin(), itcE = m_egCaloIso.end();
    if (egType == "fwdelectron") {
      itc  = m_feCaloIso.begin();
      itcE = m_feCaloIso.end();
    }
    for (; itc != itcE; itc++) {
      xAOD::CaloIsolation CaloIsoResult;
      CaloIsoHelp isoH = itc->second;
      xAOD::Iso::IsolationFlavour flav = itc->first;
      bool bsc = false;
      if (flav == xAOD::Iso::etcone && m_cellColl) 
	bsc = m_cellIsolationTool->caloCellIsolation(CaloIsoResult, *eg, isoH.isoTypes, isoH.CorrList, m_cellColl);
      else if (flav == xAOD::Iso::topoetcone)
	bsc = m_topoIsolationTool->caloTopoClusterIsolation(CaloIsoResult, *eg, isoH.isoTypes, isoH.CorrList);
      else if (flav == xAOD::Iso::neflowisol)
	bsc = m_pflowIsolationTool->neutralEflowIsolation(CaloIsoResult, *eg, isoH.isoTypes, isoH.CorrList);
      if (bsc) {
	for (unsigned int i = 0; i < isoH.isoTypes.size(); i++) {
	  float iso = CaloIsoResult.etcones[i];
	  ATH_MSG_DEBUG("custom Iso " << xAOD::Iso::toString(isoH.isoTypes[i]) << " = " << iso/1e3);
	  (*isoH.isoDeco[i])(*eg) = iso;
	}
      } else
	ATH_MSG_WARNING("Call to CaloIsolationTool failed for custom flavour " << xAOD::Iso::toString(flav));
    }
    if (egType == "fwdelectron")
      return StatusCode::SUCCESS;
    // 
    // Track Isolation types
    //
    std::map<xAOD::Iso::IsolationFlavour,TrackIsoHelp>::iterator itt = m_egTrackIso.begin(), ittE = m_egTrackIso.end();
    for (; itt != ittE; itt++) {
      xAOD::TrackIsolation TrackIsoResult;
      TrackIsoHelp isoH = itt->second;
      const std::set<const xAOD::TrackParticle*> tracksToExclude = xAOD::EgammaHelpers::getTrackParticles(eg, m_useBremAssoc);
      xAOD::Vertex *vx = 0;
      bool bsc = m_trackIsolationTool->trackIsolation(TrackIsoResult, *eg, isoH.isoTypes, isoH.CorrList, vx, &tracksToExclude);
      if (bsc) {
	unsigned int nI = isoH.isoTypes.size();
	for (unsigned int i = 0; i < nI; i++) {
	  float iso = TrackIsoResult.ptcones[i], isoV = TrackIsoResult.ptvarcones_10GeVDivPt[i];
	  ATH_MSG_DEBUG("custom Iso " << xAOD::Iso::toString(isoH.isoTypes[i]) << " = " << iso/1e3 << ", var cone = " << isoV/1e3);
	  (*isoH.isoDeco[2*i])(*eg)   = iso;
	  (*isoH.isoDeco[2*i+1])(*eg) = isoV;
	}
      } else
	ATH_MSG_WARNING("Call to custom TrackIsolationTool failed");
>>>>>>> release/21.0.127
    }

    tisoH.corrBitsetDeco = bitsetName;
    ATH_MSG_DEBUG("Initializing " << tisoH.corrBitsetDeco.key());
    ATH_CHECK(tisoH.corrBitsetDeco.initialize());
  }

  for (size_t corrType = 0; corrType < corInts[flavor].size(); corrType++) {
    const auto cor = static_cast<unsigned int>(corInts[flavor][corrType]);
    if (!corrsAreExtra)
      tisoH.CorrList.trackbitset.set(cor);
    const xAOD::Iso::IsolationTrackCorrection isoCor =
      static_cast<xAOD::Iso::IsolationTrackCorrection>(cor);

    // all pt corrections are core type
    std::string isoCorName = prefix + xAOD::Iso::toString(isoFlav) +
                             xAOD::Iso::toString(isoCor) + "Correction";

    if (!customConfig.empty()) {
      isoCorName += "_" + customConfig;
    }
    tisoH.coreCorDeco.emplace(isoCor, isoCorName);
    tisoH.coreCorDeco[isoCor].setOwner(this);
    ATH_MSG_DEBUG("initializing " << tisoH.coreCorDeco[isoCor].key());
    ATH_CHECK(tisoH.coreCorDeco[isoCor].initialize());
  }
  return StatusCode::SUCCESS;
}

StatusCode
IsolationBuilder::executeCaloIso(
  const std::vector<std::pair<xAOD::Iso::IsolationFlavour, CaloIsoHelpKey>>&
    caloIsoMap,
  const CaloCellContainer* cellColl) const
{
  for (const auto& pr : caloIsoMap) {

    const xAOD::Iso::IsolationFlavour flav = pr.first;
    const auto& keys = pr.second;
    CaloIsoHelpHandles handles(keys);

    ATH_MSG_DEBUG("Executing calo iso flavor: " << xAOD::Iso::toString(flav));

    if (handles.isoDeco.empty()) {
      ATH_MSG_FATAL("Have a CaloIsoHelpHandles with no actual isolations; "
                    "something wrong happened");
      return StatusCode::FAILURE;
    }
    auto& readHandle =
      handles.isoDeco[0]; // can treat the writeDecorHandle as a read handle;
    if (!readHandle.isValid()) {
      ATH_MSG_FATAL("Could not retrieve read handle for "
                    << keys.isoDeco[0].key());
      return StatusCode::FAILURE;
    }

    for (const auto *part : *readHandle) {
      xAOD::CaloIsolation CaloIsoResult;
<<<<<<< HEAD
      bool successfulCalc = false;
      if (flav == xAOD::Iso::IsolationFlavour::etcone && cellColl) {
        successfulCalc = m_cellIsolationTool->caloCellIsolation(
          CaloIsoResult, *part, keys.isoTypes, keys.CorrList, cellColl);
      } else if (flav == xAOD::Iso::IsolationFlavour::topoetcone) {
        successfulCalc = m_topoIsolationTool->caloTopoClusterIsolation(
          CaloIsoResult, *part, keys.isoTypes, keys.CorrList);
      } else if (flav == xAOD::Iso::IsolationFlavour::neflowisol) {
        successfulCalc = m_pflowIsolationTool->neutralEflowIsolation(
          CaloIsoResult, *part, keys.isoTypes, keys.CorrList);
      }

      if (successfulCalc) {
        for (unsigned int i = 0; i < keys.isoTypes.size(); i++) {
          float iso = CaloIsoResult.etcones[i];
          ATH_MSG_DEBUG("custom Iso " << xAOD::Iso::toCString(keys.isoTypes[i])
                                      << " = " << iso / 1e3);
          (handles.isoDeco[i])(*part) = iso;
        }
        // corrections
        (handles.corrBitsetDeco)(*part) = keys.CorrList.calobitset.to_ulong();

        // let's do the core corrections

        // iterate over the values we want to store
        for (auto& coreCorDecoPr : handles.coreCorDeco) {
          // find the matching result
          auto corIter =
            CaloIsoResult.coreCorrections.find(coreCorDecoPr.first);
          if (corIter == CaloIsoResult.coreCorrections.end()) {
            ATH_MSG_FATAL("Could not find core correction of required type: "
                          << xAOD::Iso::toCString(coreCorDecoPr.first));
            ATH_MSG_FATAL("Check configuration");
            return StatusCode::FAILURE;
          }
          // now that we have the match, let's find the energy
          std::map<xAOD::Iso::IsolationCorrectionParameter,
                   float>::const_iterator it =
            corIter->second.find(xAOD::Iso::coreEnergy);
          if (it == corIter->second.end()) {
            ATH_MSG_FATAL("Could not find coreEnergy correction for: "
                          << xAOD::Iso::toCString(coreCorDecoPr.first));
            ATH_MSG_FATAL("Check configuration");
            return StatusCode::FAILURE;
          }
          ATH_MSG_DEBUG("About to write core correction: "
                        << xAOD::Iso::toCString(coreCorDecoPr.first));
          (coreCorDecoPr.second)(*part) = it->second;
        }

        // let's do the noncore corrections
        for (auto& noncoreCorDecoPr : handles.noncoreCorDeco) {
          // find the matching result
          auto corIter =
            CaloIsoResult.noncoreCorrections.find(noncoreCorDecoPr.first);
          if (corIter == CaloIsoResult.noncoreCorrections.end()) {
            ATH_MSG_FATAL("Could not find noncore correction of required type: "
                          << xAOD::Iso::toCString(noncoreCorDecoPr.first));
            ATH_MSG_FATAL("Check configuration");
            return StatusCode::FAILURE;
          }

          ATH_MSG_DEBUG("About to write noncore correction: "
                        << xAOD::Iso::toCString(noncoreCorDecoPr.first));
          auto& vecHandles = noncoreCorDecoPr.second;
          for (size_t i = 0; i < vecHandles.size(); i++) {
            (vecHandles[i])(*part) = corIter->second[i];
          }
        }
      } else {
        ATH_MSG_FATAL("Call to CaloIsolationTool failed for flavor "
                      << xAOD::Iso::toCString(flav));
        return StatusCode::FAILURE;
      }
=======
      CaloIsoHelp isoH = itc->second;
      xAOD::Iso::IsolationFlavour flav = itc->first;
      bool bsc = false;
      if (flav == xAOD::Iso::IsolationFlavour::etcone && m_cellColl) 
	bsc = m_cellIsolationTool->caloCellIsolation(CaloIsoResult, *mu, isoH.isoTypes, isoH.CorrList, m_cellColl);
      else if (flav == xAOD::Iso::IsolationFlavour::topoetcone)
	bsc = m_topoIsolationTool->caloTopoClusterIsolation(CaloIsoResult, *mu, isoH.isoTypes, isoH.CorrList);
      else if (flav == xAOD::Iso::IsolationFlavour::neflowisol)
	bsc = m_pflowIsolationTool->neutralEflowIsolation(CaloIsoResult, *mu, isoH.isoTypes, isoH.CorrList);
      if (bsc) {
	for (unsigned int i = 0; i < isoH.isoTypes.size(); i++) {
	  float iso = CaloIsoResult.etcones[i];
	  ATH_MSG_DEBUG("custom Iso " << xAOD::Iso::toString(isoH.isoTypes[i]) << " = " << iso/1e3);
	  (*isoH.isoDeco[i])(*mu) = iso;
	}
	// not that nice. I expect a single core correction (e.g. for topoetcone, not coreMuon and coreCone together...)
	xAOD::Iso::IsolationCaloCorrection icc = xAOD::Iso::coreCone;
	bool ison = false;
	if (CaloIsoResult.corrlist.calobitset.test(static_cast<unsigned int>(xAOD::Iso::coreMuon))) {
	  icc  = xAOD::Iso::coreMuon;
	  ison = true;
	} else if (CaloIsoResult.corrlist.calobitset.test(static_cast<unsigned int>(xAOD::Iso::coreCone)))
	  ison = true;
	if (ison) {
	  if (CaloIsoResult.coreCorrections.find(icc) != CaloIsoResult.coreCorrections.end()) {
	    if (CaloIsoResult.coreCorrections[icc].find(xAOD::Iso::coreEnergy) != CaloIsoResult.coreCorrections[icc].end())
	      (*isoH.coreCorisoDeco)(*mu) = CaloIsoResult.coreCorrections[icc][xAOD::Iso::coreEnergy];
	    else
	      ATH_MSG_WARNING("Cannot find the core energy correction for custom flavour " << xAOD::Iso::toString(flav));
	  } else
	    ATH_MSG_WARNING("Cannot find the core correction for custom flavour " << xAOD::Iso::toString(flav));
	}
      } else
	ATH_MSG_WARNING("Call to CaloIsolationTool failed for custom flavour " << xAOD::Iso::toString(flav));
>>>>>>> release/21.0.127
    }
  }
  return StatusCode::SUCCESS;
}

StatusCode
IsolationBuilder::executeTrackIso(
  const std::vector<std::pair<xAOD::Iso::IsolationFlavour, TrackIsoHelpKey>>&
    trackIsoMap) const
{
  for (const auto& pr : trackIsoMap) {
    const xAOD::Iso::IsolationFlavour flav = pr.first;
    const auto& keys = pr.second;
    TrackIsoHelpHandles handles(keys);

    ATH_MSG_DEBUG("Executing track iso flavor: " << xAOD::Iso::toString(flav));

    if (handles.isoDeco.empty()) {
      ATH_MSG_FATAL("Have a TrackIsoHelpHandles with no actual isolations; "
                    "something wrong happened");
      return StatusCode::FAILURE;
    }
    auto& readHandle =
      handles.isoDeco[0]; // can treat the writeDecorHandle as a read handle;
    if (!readHandle.isValid()) {
      ATH_MSG_FATAL("Could not retrieve read handle for "
                    << keys.isoDeco[0].key());
      return StatusCode::FAILURE;
    }

    for (const auto *part : *readHandle) {
      xAOD::TrackIsolation TrackIsoResult;
      bool successfulCalc = false;
      // check to see if we are dealing with an electron
      const auto * eg = dynamic_cast<const xAOD::Egamma*>(part);
      if (eg) {
        ATH_MSG_DEBUG("Doing track isolation on an egamma particle");
        std::set<const xAOD::TrackParticle*> tracksToExclude;
        if (xAOD::EgammaHelpers::isElectron(eg)) {
          tracksToExclude =
            xAOD::EgammaHelpers::getTrackParticles(eg, m_useBremAssoc);
        } else {
          if (m_allTrackRemoval) { // New (from ??/??/16) : now this gives all
                                   // tracks
            tracksToExclude =
              xAOD::EgammaHelpers::getTrackParticles(eg, m_useBremAssoc);
          } else { // this is just to be able to have the 2015+2016 default case
                   // (only tracks from first vertex)
            const auto * gam = dynamic_cast<const xAOD::Photon*>(eg);
            if (gam && gam->nVertices() > 0) {
              const xAOD::Vertex* phvtx = gam->vertex(0);
              for (unsigned int itk = 0; itk < phvtx->nTrackParticles(); itk++)
                tracksToExclude.insert(
                  m_useBremAssoc
                    ? xAOD::EgammaHelpers::getOriginalTrackParticleFromGSF(
                        phvtx->trackParticle(itk))
                    : phvtx->trackParticle(itk));
            }
          }
        }
        successfulCalc = m_trackIsolationTool->trackIsolation(TrackIsoResult,
                                                              *part,
                                                              keys.isoTypes,
                                                              keys.CorrList,
                                                              nullptr,
                                                              &tracksToExclude);
      } else {
        ATH_MSG_DEBUG("Not doing track isolation on an egamma particle");
        successfulCalc = m_trackIsolationTool->trackIsolation(
          TrackIsoResult, *part, keys.isoTypes, keys.CorrList);
      }

      if (successfulCalc) {
        for (unsigned int i = 0; i < keys.isoTypes.size(); i++) {
          float iso = TrackIsoResult.ptcones[i];
          float isoV = TrackIsoResult.ptvarcones_10GeVDivPt[i];
          ATH_MSG_DEBUG("custom Iso " << xAOD::Iso::toCString(keys.isoTypes[i])
                                      << " = " << iso / 1e3
                                      << ", var cone = " << isoV / 1e3);
          (handles.isoDeco[i])(*part) = iso;
          (handles.isoDecoV[i])(*part) = isoV;
        }

        // corrections
        (handles.corrBitsetDeco)(*part) = keys.CorrList.trackbitset.to_ulong();
        // let's do the core corrections
        // iterate over the values we want to store
        for (auto& coreCorDecoPr : handles.coreCorDeco) {
          // find the matching result
          auto corIter =
            TrackIsoResult.coreCorrections.find(coreCorDecoPr.first);
          if (corIter == TrackIsoResult.coreCorrections.end()) {
            ATH_MSG_FATAL("Could not find core correction of required type: "
                          << xAOD::Iso::toCString(coreCorDecoPr.first));
            ATH_MSG_FATAL("Check configuration");
            return StatusCode::FAILURE;
          }
          ATH_MSG_DEBUG("About to write tracking core correction: "
                        << xAOD::Iso::toCString(coreCorDecoPr.first));
          (coreCorDecoPr.second)(*part) = corIter->second;
        }

      } else {
        ATH_MSG_FATAL("Call to TrackIsolationTool failed for flavor "
                      << xAOD::Iso::toCString(flav));
        return StatusCode::FAILURE;
      }
    }
  }
  return StatusCode::SUCCESS;
}

<<<<<<< HEAD
void
IsolationBuilder::declareIso(
  std::vector<std::pair<xAOD::Iso::IsolationFlavour, CaloIsoHelpKey>>& caloIso)
{
  for (auto& iso : caloIso) {
    iso.second.declare(this);
  }
}

void
IsolationBuilder::declareIso(
  std::vector<std::pair<xAOD::Iso::IsolationFlavour, TrackIsoHelpKey>>&
    trackIso)
{
  for (auto& iso : trackIso) {
    iso.second.declare(this);
  }
=======
StatusCode IsolationBuilder::runLeakage() {
  
  // Retrieve data
  if (m_PhotonContainerName.size()) {
    xAOD::PhotonContainer* photons = evtStore()->retrieve< xAOD::PhotonContainer >(m_PhotonContainerName);
    if( !photons ) {
      ATH_MSG_ERROR("Couldn't retrieve photon container with key: " << m_PhotonContainerName);
      return StatusCode::FAILURE;
    }
    for (auto ph : *photons)           
      m_leakTool->applyCorrection(*ph);
  }
    
  if (m_ElectronContainerName.size()) {
    xAOD::ElectronContainer* electrons = evtStore()->retrieve< xAOD::ElectronContainer >(m_ElectronContainerName);
    if( !electrons ) {
      ATH_MSG_ERROR("Couldn't retrieve electron container with key: " << m_ElectronContainerName);
      return StatusCode::FAILURE;
    }
    for (auto el : *electrons)
      m_leakTool->applyCorrection(*el);
  }

  return StatusCode::SUCCESS;
}


template< class CONTAINER, class AUXSTORE > StatusCode IsolationBuilder::deepCopy( const std::string& key ) const {
    
  // Let the user know what's happening:
  ATH_MSG_VERBOSE( "Running deepCopy on container: " << key );
  
  // Decide which implementation to call:
  if( evtStore()->template contains< AUXSTORE >( key + "Aux." ) ) {
    if( deepCopyImp< CONTAINER, AUXSTORE >( key ).isFailure() ) {
      ATH_MSG_FATAL( "Couldn't call deepCopyImp with concrete "
		     "auxiliary store" );
      return StatusCode::FAILURE;
    }
  } else if( evtStore()->template contains< xAOD::AuxContainerBase >( key +
								      "Aux." ) ) {
    if( deepCopyImp< CONTAINER,
	xAOD::AuxContainerBase >( key ).isFailure() ) {
      ATH_MSG_FATAL( "Couldn't call deepCopyImp with generic "
		     "auxiliary store" );
      return StatusCode::FAILURE;
    }
  } else {
    ATH_MSG_FATAL( "Couldn't discover auxiliary store type for container \""
		   << key << "\"" );
    return StatusCode::FAILURE;
  }
  
  // Return gracefully:
  return StatusCode::SUCCESS;
}
  
template< class CONTAINER, class AUXSTORE > StatusCode IsolationBuilder::deepCopyImp( const std::string& key ) const {
  
  // Retrieve the const container:
  ATH_MSG_VERBOSE( "Will retrieve " << key);
  const CONTAINER* c = 0;
  ATH_CHECK( evtStore()->retrieve( c, key ) );
  
  // Create the new container:
  ATH_MSG_VERBOSE( "Will create new containers" );
  CONTAINER* copy = new CONTAINER();
  AUXSTORE* copyAux = new AUXSTORE();
  copy->setStore( copyAux );
  
  // Do the deep copy:
  ATH_MSG_VERBOSE( "Will copy the object" );
  for( auto oldObj : *c ) {
    ATH_MSG_VERBOSE( "Now working on object " << oldObj);
    auto newObj = new typename CONTAINER::base_value_type();
    copy->push_back( newObj );
    *newObj = *oldObj;
  }
  
  // Do the overwrite:
  ATH_MSG_VERBOSE( "Will overwrite the container" );
  ATH_CHECK( evtStore()->overwrite( copy, key, true, false ) );
  ATH_MSG_VERBOSE( "Will overwrite the aux container" );
  ATH_CHECK( evtStore()->overwrite( copyAux, key + "Aux.", true, false ) );
  ATH_MSG_VERBOSE( "Done" );
  
  // Return gracefully:
  return StatusCode::SUCCESS;
>>>>>>> release/21.0.127
}
