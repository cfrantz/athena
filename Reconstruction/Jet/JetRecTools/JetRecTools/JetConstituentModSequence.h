// this file is -*- C++ -*-

/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

//JetConstituentModSequence.h

#ifndef JETRECTOOLS_JETCONSTITUENTMODSEQUENCE_H
#define JETRECTOOLS_JETCONSTITUENTMODSEQUENCE_H

//
// Michael Nelson, CERN & Univesity of Oxford
// February, 2016

#include <string>
#include "AsgTools/AsgTool.h"
#include "JetInterface/IJetExecuteTool.h"
#include "JetInterface/IJetConstituentModifier.h"
#include "AsgTools/ToolHandleArray.h"
#include "xAODCore/ShallowCopy.h"

#include "xAODBase/IParticleHelpers.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODPFlow/TrackCaloClusterContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODPFlow/PFOContainer.h"
#include "xAODPFlow/FlowElementContainer.h"

#ifndef XAOD_ANALYSIS
#include "AthenaMonitoringKernel/GenericMonitoringTool.h"
#endif

<<<<<<< HEAD
class JetConstituentModSequence: public asg::AsgTool, virtual public IJetExecuteTool {
  // Changed from IJetExecute
=======
class JetConstituentModSequence: public asg::AsgTool, virtual public IJetExecuteTool { // Changed from IJetExecute
>>>>>>> release/21.0.127
  ASG_TOOL_CLASS(JetConstituentModSequence, IJetExecuteTool)
  public:
  JetConstituentModSequence(const std::string &name); // MEN: constructor 
  StatusCode initialize();
  int execute() const;
  void setInputClusterCollection(const xAOD::IParticleContainer* cont);
  xAOD::IParticleContainer* getOutputClusterCollection();
  
protected:
  std::string m_inputContainer = "";
  std::string m_outputContainer = "";
<<<<<<< HEAD

  const xAOD::IParticleContainer* m_trigInputConstits; // used in trigger context only
  mutable xAOD::IParticleContainer* m_trigOutputConstits;
  bool m_trigger;
  
  // P-A : the actual type
  // Define as a basic integer type because Gaudi
  // doesn't support arbitrary property types
  unsigned short m_inputType; // 
=======
  const xAOD::IParticleContainer* m_trigInputClusters; // used in trigger context only
  mutable xAOD::IParticleContainer* m_trigOutputClusters;
  bool m_trigger;
  
  // P-A : a property defining the type name of the input constituent
  std::string m_inputTypeName = "CaloCluster"; // MEN: Check this 
  // P-A : the actual type
  xAOD::Type::ObjectType m_inputType; // 
>>>>>>> release/21.0.127
  
  
  ToolHandleArray<IJetConstituentModifier> m_modifiers;

<<<<<<< HEAD
#ifndef XAOD_ANALYSIS
  ToolHandle<GenericMonitoringTool> m_monTool{this,"MonTool","","Monitoring tool"};
#endif
  
  bool m_saveAsShallow = true;

  // note: not all keys will be used for a particular instantiation
  SG::ReadHandleKey<xAOD::CaloClusterContainer> m_inClusterKey{this, "InClusterKey", "", "ReadHandleKey for unmodified CaloClusters"};
  SG::WriteHandleKey<xAOD::CaloClusterContainer> m_outClusterKey{this, "OutClusterKey", "", "WriteHandleKey for modified CaloClusters"};

  SG::ReadHandleKey<xAOD::TrackCaloClusterContainer> m_inTCCKey{this, "InTCCKey", "", "ReadHandleKey for unmodified TrackCaloClusters"};
  SG::WriteHandleKey<xAOD::TrackCaloClusterContainer> m_outTCCKey{this, "OutTCCKey", "", "WriteHandleKey for modified TrackCaloClusters"};

  SG::ReadHandleKey<xAOD::PFOContainer> m_inChargedPFOKey{this, "InChargedPFOKey", "", "ReadHandleKey for modified Charged PFlow Objects"};
  SG::WriteHandleKey<xAOD::PFOContainer> m_outChargedPFOKey{this, "OutChargedPFOKey", "", "WriteHandleKey for modified Charged PFlow Objects"};

  SG::ReadHandleKey<xAOD::PFOContainer> m_inNeutralPFOKey{this, "InNeutralPFOKey", "", "ReadHandleKey for modified Neutral PFlow Objects"};
  SG::WriteHandleKey<xAOD::PFOContainer> m_outNeutralPFOKey{this, "OutNeutralPFOKey", "", "WriteHandleKey for modified Neutral PFlow Objects"};

  SG::WriteHandleKey<xAOD::PFOContainer> m_outAllPFOKey{this, "OutAllPFOKey", "", "WriteHandleKey for all modified PFlow Objects"};

  SG::ReadHandleKey<xAOD::FlowElementContainer> m_inChargedFEKey{this, "InChargedFEKey", "", "ReadHandleKey for modified Charged FlowElements"};
  SG::WriteHandleKey<xAOD::FlowElementContainer> m_outChargedFEKey{this, "OutChargedFEKey", "", "WriteHandleKey for modified Charged FlowElements"};

  SG::ReadHandleKey<xAOD::FlowElementContainer> m_inNeutralFEKey{this, "InNeutralFEKey", "", "ReadHandleKey for modified Neutral FlowElements"};
  SG::WriteHandleKey<xAOD::FlowElementContainer> m_outNeutralFEKey{this, "OutNeutralFEKey", "", "WriteHandleKey for modified Neutral FlowElements"};

  SG::WriteHandleKey<xAOD::FlowElementContainer> m_outAllFEKey{this, "OutAllFEKey", "", "WriteHandleKey for all modified FlowElements"};

  StatusCode copyModRecordPFO() const;
  StatusCode copyModRecordFE() const;

  /// helper function to cast, shallow copy and record a container.

  template<class T>
  StatusCode
  copyModRecord(const SG::ReadHandleKey<T>&,
                const SG::WriteHandleKey<T>&) const;

  // temporary trigger helper needed until we can
  // decommission TrigHLTJetRec classes
  template<class T, class Taux, class Tsingle>
  StatusCode
  copyModForTrigger(const T&) const;
=======
  bool m_saveAsShallow = true;

  /// helper function to cast, shallow copy and record a container (for offline) or deep copy and record a container (for trigger, where shallow copy isn't supported)
  template<class T, class Taux, class Tsingle>
  xAOD::IParticleContainer* copyAndRecord(const xAOD::IParticleContainer* cont, bool record) const {
    const T * clustCont = dynamic_cast<const T *>(cont);
    if(clustCont == 0) {
      ATH_MSG_ERROR( "Container "<<cont<< " is not of type "<< m_inputType);
      return NULL;
    }

    if(!m_trigger){
      std::pair< T*, xAOD::ShallowAuxContainer* > newclust = xAOD::shallowCopyContainer(*clustCont );    
      newclust.second->setShallowIO(m_saveAsShallow);
      if(record){
        if(evtStore()->record( newclust.first, m_outputContainer ).isFailure() || evtStore()->record( newclust.second, m_outputContainer+"Aux." ).isFailure() ){
          ATH_MSG_ERROR("Unable to record cluster collection" << m_outputContainer );
          return NULL;
        }
      }
      //newclust.second->setShallowIO(false);
      return newclust.first;
    }
    
    
    // This is the trigger case, revert to a deep copy
    
    // Create the new container and its auxiliary store.
    T* clusterCopy = new T();
    Taux* clusterCopyAux = new Taux();
    clusterCopy->setStore( clusterCopyAux ); //< Connect the two
    typename T::const_iterator clust_itr;
    clust_itr = clustCont->begin();
    typename T::const_iterator clust_end = clustCont->end();

    for( ; clust_itr != clust_end; ++clust_itr ) {
      Tsingle* cluster = new Tsingle();
      clusterCopy->push_back( cluster ); // jet acquires the goodJets auxstore
      *cluster= **clust_itr; // copies auxdata from one auxstore to the other
      //clusterCopy->push_back(new Tsingle(**clust_itr));
    }

    // Store and return the container
    m_trigOutputClusters = clusterCopy;
    return m_trigOutputClusters;

  }

>>>>>>> release/21.0.127
};

template<class T>
StatusCode
JetConstituentModSequence::copyModRecord(const SG::ReadHandleKey<T>& inKey,
                                         const SG::WriteHandleKey<T>& outKey) const {

  /* Read in a container of (type is template parameter),
     optionally modify the elements of this container, and store.
     This puts a (modified) copy of the container  into storegate.
  */
  
  auto inHandle = makeHandle(inKey);
  if(!inHandle.isValid()){
    ATH_MSG_WARNING("Unable to retrieve input container from " << inKey.key());
    return StatusCode::FAILURE;
  }

  std::pair< T*, xAOD::ShallowAuxContainer* > newconstit =
    xAOD::shallowCopyContainer(*inHandle);    
  newconstit.second->setShallowIO(m_saveAsShallow);

  for (auto t : m_modifiers) {ATH_CHECK(t->process(newconstit.first));}

  auto handle = makeHandle(outKey);
  ATH_CHECK(handle.record(std::unique_ptr<T>(newconstit.first),
                          std::unique_ptr<xAOD::ShallowAuxContainer>(newconstit.second)));
  
  xAOD::setOriginalObjectLink(*inHandle, *handle);
  
  return StatusCode::SUCCESS;
}

template<class T, class Taux, class Tsingle>
StatusCode
JetConstituentModSequence::copyModForTrigger(const T& originals) const
{
    
  // This is the trigger case, revert to a deep copy of the input container.
  // Create the new container and its auxiliary store.
  T* constitCopy = new T();
  Taux* constitCopyAux = new Taux();
  constitCopy->setStore( constitCopyAux ); //< Connect the two

  for(const Tsingle* orig_constit : originals) {
    Tsingle* theconstit = new Tsingle();
    constitCopy->push_back( theconstit );
    *theconstit= *orig_constit; // copies auxdata from one auxstore to the other
  }
  
  for (auto t : m_modifiers) {ATH_CHECK(t->process(constitCopy));}

  // Update the output container pointer
  m_trigOutputConstits = constitCopy;
  return StatusCode::SUCCESS;
}

#endif
