<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
# $Id: CMakeLists.txt 757523 2016-06-24 15:20:43Z krasznaa $
################################################################################
# Package: JetRecCalo
################################################################################
>>>>>>> release/21.0.127

# Declare the package name:
atlas_subdir( JetRecCalo )

<<<<<<< HEAD
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Calorimeter/CaloIdentifier
   Control/AthToolSupport/AsgTools
   Control/CxxUtils
   GaudiKernel
   Reconstruction/Jet/JetInterface
   Reconstruction/Jet/JetUtils
   TileCalorimeter/TileConditions
   PRIVATE
   Calorimeter/CaloDetDescr
   Calorimeter/CaloEvent
   Control/AthenaKernel
   LArCalorimeter/LArRecConditions )

>>>>>>> release/21.0.127
# Component(s) in the package:
atlas_add_library( JetRecCaloLib
   JetRecCalo/*.h
   INTERFACE
   PUBLIC_HEADERS JetRecCalo
<<<<<<< HEAD
   LINK_LIBRARIES GaudiKernel CaloIdentifier LArRecConditions TileConditionsLib JetUtils
   StoreGateLib AsgTools JetInterface )

atlas_add_component( JetRecCalo
   src/*.cxx src/components/*.cxx
   LINK_LIBRARIES AsgTools AthenaKernel CaloDetDescrLib CaloEvent CaloIdentifier GaudiKernel JetInterface JetRecCaloLib JetUtils LArRecConditions StoreGateLib TileConditionsLib )
=======
   LINK_LIBRARIES GaudiKernel CaloIdentifier TileConditionsLib CxxUtils JetUtils
   AsgTools JetInterface )

atlas_add_component( JetRecCalo
   src/*.cxx src/components/*.cxx
   LINK_LIBRARIES GaudiKernel CaloDetDescrLib CaloEvent AthenaKernel
   LArRecConditions JetRecCaloLib )
>>>>>>> release/21.0.127
