/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// JetConstitFourMomTool.h

#ifndef JetMomentTools_JetConstitFourMomTool_H
#define JetMomentTools_JetConstitFourMomTool_H

/// Russell Smith
/// May 2015
///
/// Tool to attach the LC constituent level 4-vector to EM Jets

<<<<<<< HEAD
#include "AsgDataHandles/ReadHandleKeyArray.h"
#include "AsgTools/AsgTool.h"
#include "JetInterface/IJetModifier.h"
#include "xAODCaloEvent/CaloClusterContainer.h"

#include <vector>

class JetConstitFourMomTool : public asg::AsgTool,
                              public IJetModifier {
=======
#include "JetRec/JetModifierBase.h"
#include <vector>
>>>>>>> release/21.0.127

  ASG_TOOL_CLASS(JetConstitFourMomTool, IJetModifier)

 public:

  // Constructor from tool name.
  JetConstitFourMomTool(std::string myname);

  StatusCode initialize();

<<<<<<< HEAD
  StatusCode modify(xAOD::JetContainer& jets) const;
=======
  // From IJetModifier base class
  int modify(xAOD::JetContainer& jets) const;
  int modifyJet(xAOD::Jet&) const {return 0;}
>>>>>>> release/21.0.127

 private:
  
  int m_constitScale;
  std::vector<std::string> m_jetScaleNames;
  std::vector<std::string> m_altColls;
  std::vector<int>         m_altConstitScales;
  std::vector<std::string> m_altJetScales;

  std::vector<bool> m_isDetectorEtaPhi;
<<<<<<< HEAD

  SG::ReadHandleKeyArray<xAOD::CaloClusterContainer> m_altColls_keys;
=======
>>>>>>> release/21.0.127

};

#endif
