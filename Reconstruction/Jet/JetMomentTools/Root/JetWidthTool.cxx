/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

// JetWidthTool.cxx

#include "JetMomentTools/JetWidthTool.h"
#include "AsgDataHandles/WriteDecorHandle.h"
#include "xAODJet/JetConstituentVector.h"
#include "JetUtils/JetDistances.h"
#include "PFlowUtils/IWeightPFOTool.h"

//**********************************************************************

JetWidthTool::JetWidthTool(std::string myname)
<<<<<<< HEAD
  : asg::AsgTool(myname)
{
=======
  : JetModifierBase(myname),
    m_weightpfoEM("WeightPFOTool/pflowweighter"),
    m_weightpfoLC("WeightPFOTool/pflowweighterLC")
{
  declareProperty( "WeightPFOToolEM", m_weightpfoEM );
  declareProperty( "WeightPFOToolLC", m_weightpfoLC );
>>>>>>> release/21.0.127
}

//**********************************************************************

StatusCode JetWidthTool::initialize(){

  if(m_jetContainerName.empty()){
    ATH_MSG_ERROR("JetWidthTool needs to have its input jet container name configured!");
    return StatusCode::FAILURE;
  }

  // Prepend jet container name
  m_widthKey = m_jetContainerName + "." + m_widthKey.key();
  m_widthPhiKey = m_jetContainerName + "." + m_widthPhiKey.key();

  ATH_CHECK(m_widthKey.initialize());
  ATH_CHECK(m_widthPhiKey.initialize());
  return StatusCode::SUCCESS;
}

//**********************************************************************

StatusCode JetWidthTool::decorate(const xAOD::JetContainer& jets) const {

<<<<<<< HEAD
  SG::WriteDecorHandle<xAOD::JetContainer, float> widthHandle(m_widthKey);
  SG::WriteDecorHandle<xAOD::JetContainer, float> widthPhiHandle(m_widthPhiKey);

  for(const xAOD::Jet* jet : jets){
    float widthEta = 0, widthPhi = 0;
    widthHandle(*jet) = width(*jet,widthEta,widthPhi);
    widthPhiHandle(*jet) = widthPhi;
  }
  return StatusCode::SUCCESS;
}

//**********************************************************************

float JetWidthTool::width(const xAOD::Jet& jet, float& widthEta, float& widthPhi) const {

=======
>>>>>>> release/21.0.127
  // Get the constituents of the jet
  // TODO: Switch to using helper function once JetUtils has been updated
  // Set the width
  // jetWidth = JetKinematics::ptWeightedWidth(iter,iEnd,&jet);
    
  // Calculate the pt weighted width
<<<<<<< HEAD
  const float jetEta = jet.eta();
  const float jetPhi = jet.phi();
  float weightedWidth = 0;
  float weightedWidthEta = 0;
  float weightedWidthPhi = 0;
  float ptSum = 0;

  const xAOD::JetConstituentVector constituents = jet.getConstituents();
  for (const auto constituent : constituents) {
    const float dR   = jet::JetDistances::deltaR(jetEta, jetPhi, constituent->eta(),  constituent->phi() );
    const float dEta = fabs(jet::JetDistances::deltaEta(jetEta, constituent->eta() ));
    const float dPhi = fabs(jet::JetDistances::deltaPhi(jetPhi, constituent->phi() ));
    const float pt   = constituent->pt();
=======
  const double jetEta = jet.eta();
  const double jetPhi = jet.phi();
  double weightedWidth = 0;
  double weightedWidthEta = 0;
  double weightedWidthPhi = 0;
  double ptSum = 0;

  const xAOD::JetInput::Type jetinput = jet.getInputType();
  const bool isPFlowEM = (jetinput == xAOD::JetInput::PFlow || jetinput == xAOD::JetInput::EMPFlow);
  const bool isPFlowLC = (jetinput == xAOD::JetInput::LCPFlow || jetinput == xAOD::JetInput::EMCPFlow);
>>>>>>> release/21.0.127

  const xAOD::JetConstituentVector constituents = jet.getConstituents();
  for (const auto& constituent : constituents) {
    const double dR   = jet::JetDistances::deltaR(jetEta, jetPhi, constituent->eta(),  constituent->phi() );
    const double dEta = fabs(jet::JetDistances::deltaEta(jetEta, constituent->eta() ));
    const double dPhi = fabs(jet::JetDistances::deltaPhi(jetPhi, constituent->phi() ));
    const double pt   = constituent->pt();

    float weight = 1.; 
    if(isPFlowEM || isPFlowLC) {
      const xAOD::PFO* pfo = static_cast<const xAOD::PFO*>(constituent->rawConstituent());
      if(pfo->charge()>FLT_MIN) {
	if(isPFlowEM) {
	  ATH_CHECK( m_weightpfoEM->fillWeight( *pfo, weight ) );
	} else if(isPFlowLC) {
	  ATH_CHECK( m_weightpfoLC->fillWeight( *pfo, weight ) );
	}
      }
    }

    weightedWidth += dR * pt * weight;
    weightedWidthEta += dEta * pt * weight;
    weightedWidthPhi += dPhi * pt * weight;
    
    ptSum += pt * weight;
  }

  widthEta = ptSum > 0 ? weightedWidthEta/ptSum : -1;
  widthPhi = ptSum > 0 ? weightedWidthPhi/ptSum : -1;

  return ptSum > 0 ? weightedWidth/ptSum : -1;
}
