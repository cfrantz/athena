# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# hook for PyUtils.scripts package

import PyUtils.acmdlib as acmdlib
acmdlib.register('chk-file', 'PyUtils.scripts.check_file')
acmdlib.register('chk-sg', 'PyUtils.scripts.check_sg')
acmdlib.register('chk-rflx', 'PyUtils.scripts.check_reflex')
acmdlib.register('diff-pool', 'PyUtils.scripts.diff_pool_files')
acmdlib.register('diff-root', 'PyUtils.scripts.diff_root_files')
acmdlib.register('dump-root', 'PyUtils.scripts.dump_root_file')
acmdlib.register('ath-dump', 'PyUtils.scripts.ath_dump')
acmdlib.register('gen-klass', 'PyUtils.scripts.gen_klass')

acmdlib.register('merge-files', 'PyUtils.scripts.merge_files')
acmdlib.register('filter-files', 'PyUtils.scripts.filter_files')

acmdlib.register('cmake.depends', 'PyUtils.scripts.cmake_depends')
acmdlib.register('cmake.new-skeleton', 'PyUtils.scripts.cmake_newskeleton')
acmdlib.register('cmake.new-pkg', 'PyUtils.scripts.cmake_newpkg')
acmdlib.register('cmake.new-analysisalg', 'PyUtils.scripts.cmake_newanalysisalg')


<<<<<<< HEAD
acmdlib.register('jira.issues', 'PyUtils.scripts.jira_issues')
=======
acmdlib.register('cmt.new-pkg', 'PyUtils.scripts.cmt_newpkg:main')
acmdlib.register('cmt.new-alg', 'PyUtils.scripts.cmt_newalg:main')
acmdlib.register('cmt.new-analysisalg', 'PyUtils.scripts.cmt_newanalysisalg:main')
acmdlib.register('cmt.new-asgtool', 'PyUtils.scripts.cmt_newasgtool:main')
acmdlib.register('cmt.new-pyalg', 'PyUtils.scripts.cmt_newpyalg:main')
acmdlib.register('cmt.new-jobo', 'PyUtils.scripts.cmt_newjobo:main')
acmdlib.register('cmt.new-analysisapp', 'PyUtils.scripts.cmt_newanalysisapp:main')

acmdlib.register('cmake.new-pkg', 'PyUtils.scripts.cmake_newpkg:main')
acmdlib.register('cmake.new-analysisalg', 'PyUtils.scripts.cmake_newanalysisalg:main')

acmdlib.register('jira.issues', 'PyUtils.scripts.jira_issues:main')
# acmdlib.register('jira.issue', 'PyUtils.scripts.jira_issue:main')
>>>>>>> release/21.0.127
##
