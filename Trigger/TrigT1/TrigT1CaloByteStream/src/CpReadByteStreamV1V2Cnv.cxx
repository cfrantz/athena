/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/


#include <vector>
#include <stdint.h>

#include "ByteStreamCnvSvcBase/ByteStreamAddress.h"
#include "ByteStreamCnvSvcBase/IByteStreamEventAccess.h"
#include "ByteStreamCnvSvcBase/IROBDataProviderSvc.h"

#include "ByteStreamData/RawEvent.h"
#include "ByteStreamData/ROBData.h"

#include "AthContainers/DataVector.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IOpaqueAddress.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/StatusCode.h"

#include "AthenaKernel/ClassID_traits.h"
#include "AthenaKernel/StorableConversions.h"
#include "AthenaKernel/errorcheck.h"

#include "TrigT1CaloEvent/CPMTower.h"

#include "CpReadByteStreamV1V2Cnv.h"
#include "CpByteStreamV1Tool.h"
#include "CpByteStreamV2Tool.h"

namespace LVL1BS {

CpReadByteStreamV1V2Cnv::CpReadByteStreamV1V2Cnv( ISvcLocator* svcloc )
    : AthConstConverter( storageType(), classID(), svcloc, "CpReadByteStreamV1V2Cnv" ),
      m_tool1("LVL1BS::CpByteStreamV1Tool/CpByteStreamV1Tool"),
      m_tool2("LVL1BS::CpByteStreamV2Tool/CpByteStreamV2Tool"),
      m_robDataProvider("ROBDataProviderSvc", name())
{
}

CpReadByteStreamV1V2Cnv::~CpReadByteStreamV1V2Cnv()
{
}

// CLID

const CLID& CpReadByteStreamV1V2Cnv::classID()
{
  return ClassID_traits<DataVector<LVL1::CPMTower> >::ID();
}

long CpReadByteStreamV1V2Cnv::storageType()
{
  return ByteStreamAddress::storageType();
}

//  Init method gets all necessary services etc.


StatusCode CpReadByteStreamV1V2Cnv::initialize()
{
<<<<<<< HEAD
  ATH_CHECK( Converter::initialize() );
  ATH_CHECK( m_tool1.retrieve() );
  ATH_CHECK( m_tool2.retrieve() );
  ATH_CHECK( m_robDataProvider.retrieve() );
=======
  m_debug = msgSvc()->outputLevel(m_name) <= MSG::DEBUG;
  m_log << MSG::DEBUG << "Initializing " << m_name << " - package version "
                      << PACKAGE_VERSION << endmsg;

  StatusCode sc = Converter::initialize();
  if ( sc.isFailure() )
    return sc;

  // Retrieve Tools
  sc = m_tool1.retrieve();
  if ( sc.isFailure() ) {
    m_log << MSG::ERROR << "Failed to retrieve tool " << m_tool1 << endmsg;
    return StatusCode::FAILURE;
  } else m_log << MSG::DEBUG << "Retrieved tool " << m_tool1 << endmsg;
  sc = m_tool2.retrieve();
  if ( sc.isFailure() ) {
    m_log << MSG::ERROR << "Failed to retrieve tool " << m_tool2 << endmsg;
    return StatusCode::FAILURE;
  } else m_log << MSG::DEBUG << "Retrieved tool " << m_tool2 << endmsg;

  // Get ROBDataProvider
  sc = m_robDataProvider.retrieve();
  if ( sc.isFailure() ) {
    m_log << MSG::WARNING << "Failed to retrieve service "
          << m_robDataProvider << endmsg;
    return sc ;
  } else {
    m_log << MSG::DEBUG << "Retrieved service "
          << m_robDataProvider << endmsg;
  }
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}

// createObj should create the RDO from bytestream.

StatusCode CpReadByteStreamV1V2Cnv::createObjConst( IOpaqueAddress* pAddr,
                                                    DataObject*& pObj ) const
{
<<<<<<< HEAD
  ByteStreamAddress *pBS_Addr;
  pBS_Addr = dynamic_cast<ByteStreamAddress *>( pAddr );
  if ( !pBS_Addr ) {
    ATH_MSG_ERROR( " Can not cast to ByteStreamAddress " );
=======
  if (m_debug) m_log << MSG::DEBUG << "createObj() called" << endmsg;

  ByteStreamAddress *pBS_Addr;
  pBS_Addr = dynamic_cast<ByteStreamAddress *>( pAddr );
  if ( !pBS_Addr ) {
    m_log << MSG::ERROR << " Can not cast to ByteStreamAddress " << endmsg;
>>>>>>> release/21.0.127
    return StatusCode::FAILURE;
  }

  const std::string nm = *( pBS_Addr->par() );

<<<<<<< HEAD
  ATH_MSG_DEBUG( " Creating Objects " << nm );
=======
  if (m_debug) m_log << MSG::DEBUG << " Creating Objects " << nm << endmsg;
>>>>>>> release/21.0.127

  // get SourceIDs
  const std::vector<uint32_t>& vID1(m_tool1->sourceIDs());
  const std::vector<uint32_t>& vID2(m_tool2->sourceIDs());

  // get ROB fragments
  IROBDataProviderSvc::VROBFRAG robFrags1;
  m_robDataProvider->getROBData( vID1, robFrags1 );
  IROBDataProviderSvc::VROBFRAG robFrags2;
  m_robDataProvider->getROBData( vID2, robFrags2 );

  // size check
<<<<<<< HEAD
  auto towerCollection = std::make_unique<DataVector<LVL1::CPMTower> >();
  ATH_MSG_DEBUG( " Number of ROB fragments is " << robFrags1.size()
                 << ", " << robFrags2.size() );

=======
  DataVector<LVL1::CPMTower>* const towerCollection = new DataVector<LVL1::CPMTower>;
  if (m_debug) {
    m_log << MSG::DEBUG << " Number of ROB fragments is " << robFrags1.size()
          << ", " << robFrags2.size() << endmsg;
  }
>>>>>>> release/21.0.127
  if (robFrags1.size() == 0 && robFrags2.size() == 0) {
    pObj = SG::asStorable(std::move(towerCollection)) ;
    return StatusCode::SUCCESS;
  }

  // Pre-LS1 data
  if (robFrags1.size() > 0) {
<<<<<<< HEAD
    ATH_CHECK( m_tool1->convert(nm, robFrags1, towerCollection.get()) );
  }
  // Post-LS1 data
  if (robFrags2.size() > 0) {
    ATH_CHECK( m_tool2->convert(nm, robFrags2, towerCollection.get()) ); 
 }
=======
    StatusCode sc = m_tool1->convert(robFrags1, towerCollection);
    if ( sc.isFailure() ) {
      m_log << MSG::ERROR << " Failed to create Objects   " << nm << endmsg;
      delete towerCollection;
      return sc;
    }
  }
  // Post-LS1 data
  if (robFrags2.size() > 0) {
    StatusCode sc = m_tool2->convert(robFrags2, towerCollection);
    if ( sc.isFailure() ) {
      m_log << MSG::ERROR << " Failed to create Objects   " << nm << endmsg;
      delete towerCollection;
      return sc;
    }
  }
>>>>>>> release/21.0.127

  pObj = SG::asStorable(std::move(towerCollection));

  return StatusCode::SUCCESS;
}

} // end namespace
