/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/


#include <vector>
#include <stdint.h>

#include "ByteStreamCnvSvcBase/ByteStreamAddress.h"
#include "ByteStreamCnvSvcBase/IByteStreamEventAccess.h"
#include "ByteStreamCnvSvcBase/IROBDataProviderSvc.h"

#include "ByteStreamData/RawEvent.h"
#include "ByteStreamData/ROBData.h"

#include "AthContainers/DataVector.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IOpaqueAddress.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/StatusCode.h"

#include "AthenaKernel/ClassID_traits.h"
#include "AthenaKernel/StorableConversions.h"
#include "AthenaKernel/errorcheck.h"

#include "TrigT1CaloEvent/CPMRoI.h"

#include "CpmRoiByteStreamCnv.h"
#include "CpmRoiByteStreamTool.h"

namespace LVL1BS {

CpmRoiByteStreamCnv::CpmRoiByteStreamCnv( ISvcLocator* svcloc )
    : AthConstConverter( storageType(), classID(), svcloc, "CpmRoiByteStreamCnv" ),
      m_tool("LVL1BS::CpmRoiByteStreamTool/CpmRoiByteStreamTool"),
      m_robDataProvider("ROBDataProviderSvc", name())
{
}

CpmRoiByteStreamCnv::~CpmRoiByteStreamCnv()
{
}

// CLID

const CLID& CpmRoiByteStreamCnv::classID()
{
  return ClassID_traits<DataVector<LVL1::CPMRoI> >::ID();
}

long CpmRoiByteStreamCnv::storageType()
{
  return ByteStreamAddress::storageType();
}

//  Init method gets all necessary services etc.


StatusCode CpmRoiByteStreamCnv::initialize()
{
<<<<<<< HEAD
  ATH_CHECK( Converter::initialize() );
  ATH_CHECK( m_tool.retrieve() );
=======
  m_debug = msgSvc()->outputLevel(m_name) <= MSG::DEBUG;
  m_log << MSG::DEBUG << "Initializing " << m_name << " - package version "
                      << PACKAGE_VERSION << endmsg;

  StatusCode sc = Converter::initialize();
  if ( sc.isFailure() )
    return sc;

  //Get ByteStreamCnvSvc
  sc = m_ByteStreamEventAccess.retrieve();
  if ( sc.isFailure() ) {
    m_log << MSG::ERROR << "Failed to retrieve service "
          << m_ByteStreamEventAccess << endmsg;
    return sc;
  } else {
    m_log << MSG::DEBUG << "Retrieved service "
          << m_ByteStreamEventAccess << endmsg;
  }

  // Retrieve Tool
  sc = m_tool.retrieve();
  if ( sc.isFailure() ) {
    m_log << MSG::ERROR << "Failed to retrieve tool " << m_tool << endmsg;
    return StatusCode::FAILURE;
  } else m_log << MSG::DEBUG << "Retrieved tool " << m_tool << endmsg;
>>>>>>> release/21.0.127

  // Get ROBDataProvider
  StatusCode sc = m_robDataProvider.retrieve();
  if ( sc.isFailure() ) {
<<<<<<< HEAD
    ATH_MSG_WARNING(  "Failed to retrieve service " << m_robDataProvider );
    // return is disabled for Write BS which does not require ROBDataProviderSvc
    // return sc ;
=======
    m_log << MSG::WARNING << "Failed to retrieve service "
          << m_robDataProvider << endmsg;
    // return is disabled for Write BS which does not require ROBDataProviderSvc
    // return sc ;
  } else {
    m_log << MSG::DEBUG << "Retrieved service "
          << m_robDataProvider << endmsg;
>>>>>>> release/21.0.127
  }

  return StatusCode::SUCCESS;
}

// createObj should create the RDO from bytestream.

StatusCode CpmRoiByteStreamCnv::createObjConst( IOpaqueAddress* pAddr,
                                                DataObject*& pObj ) const
{
<<<<<<< HEAD
  ByteStreamAddress *pBS_Addr;
  pBS_Addr = dynamic_cast<ByteStreamAddress *>( pAddr );
  if ( !pBS_Addr ) {
    ATH_MSG_ERROR(  " Can not cast to ByteStreamAddress " );
=======
  if (m_debug) m_log << MSG::DEBUG << "createObj() called" << endmsg;

  ByteStreamAddress *pBS_Addr;
  pBS_Addr = dynamic_cast<ByteStreamAddress *>( pAddr );
  if ( !pBS_Addr ) {
    m_log << MSG::ERROR << " Can not cast to ByteStreamAddress " << endmsg;
>>>>>>> release/21.0.127
    return StatusCode::FAILURE;
  }

  const std::string nm = *( pBS_Addr->par() );

<<<<<<< HEAD
  ATH_MSG_DEBUG( " Creating Objects " << nm );
=======
  if (m_debug) m_log << MSG::DEBUG << " Creating Objects " << nm << endmsg;
>>>>>>> release/21.0.127

  // get SourceIDs
  const std::vector<uint32_t>& vID(m_tool->sourceIDs(nm));

  // get ROB fragments
  IROBDataProviderSvc::VROBFRAG robFrags;
  m_robDataProvider->getROBData( vID, robFrags );

  // size check
<<<<<<< HEAD
  auto roiCollection = std::make_unique<DataVector<LVL1::CPMRoI> >();
  ATH_MSG_DEBUG( " Number of ROB fragments is " << robFrags.size() );

=======
  DataVector<LVL1::CPMRoI>* const roiCollection = new DataVector<LVL1::CPMRoI>;
  if (m_debug) {
    m_log << MSG::DEBUG << " Number of ROB fragments is " << robFrags.size()
          << endmsg;
  }
>>>>>>> release/21.0.127
  if (robFrags.size() == 0) {
    pObj = SG::asStorable(std::move(roiCollection)) ;
    return StatusCode::SUCCESS;
  }

<<<<<<< HEAD
  ATH_CHECK( m_tool->convert(robFrags, roiCollection.get()) );
=======
  StatusCode sc = m_tool->convert(robFrags, roiCollection);
  if ( sc.isFailure() ) {
    m_log << MSG::ERROR << " Failed to create Objects   " << nm << endmsg;
    delete roiCollection;
    return sc;
  }
>>>>>>> release/21.0.127

  pObj = SG::asStorable(std::move(roiCollection));

  return StatusCode::SUCCESS;
}

// createRep should create the bytestream from RDOs.

StatusCode CpmRoiByteStreamCnv::createRepConst( DataObject* pObj,
                                                IOpaqueAddress*& pAddr ) const
{
<<<<<<< HEAD
  DataVector<LVL1::CPMRoI>* roiCollection = 0;
  if( !SG::fromStorable( pObj, roiCollection ) ) {
    ATH_MSG_ERROR( " Cannot cast to DataVector<CPMRoI>" );
=======
  if (m_debug) m_log << MSG::DEBUG << "createRep() called" << endmsg;

  RawEventWrite* re = m_ByteStreamEventAccess->getRawEvent();

  DataVector<LVL1::CPMRoI>* roiCollection = 0;
  if( !SG::fromStorable( pObj, roiCollection ) ) {
    m_log << MSG::ERROR << " Cannot cast to DataVector<CPMRoI>" << endmsg;
>>>>>>> release/21.0.127
    return StatusCode::FAILURE;
  }

  const std::string nm = pObj->registry()->name();

  pAddr = new ByteStreamAddress( classID(), nm, "" );

  // Convert to ByteStream
  return m_tool->convert( roiCollection );
}

} // end namespace
