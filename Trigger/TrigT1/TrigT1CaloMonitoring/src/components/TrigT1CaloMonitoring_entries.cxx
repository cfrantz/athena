// Run 3
#include "../CpmMonitorAlgorithm.h"
//#include "../CpmSimMonitorAlgorithm.h"
//#include "../PpmSimBsMonitorAlgorithm.h"
#include "../PprMonitorAlgorithm.h"
//#include "../PprSpareMonitorAlgorithm.h"
//#include "../PprStabilityMonitorAlgorithm.h"
//#include "../LVL1_TagProbeEffMonitorAlgorithm.h"
#include "../JepJemMonitorAlgorithm.h"

// Run 2
#include "../CPMon.h"
#include "../CPSimMon.h"
#include "../JEPJEMMon.h"
#include "../JEPCMXMon.h"
#include "../JEPSimMon.h"
#include "../PPrMon.h"
#include "../MistimedStreamMon.h"
#include "../PPrStabilityMon.h"
#include "../PPrSpareMon.h"
#include "../PPMSimBSMon.h"
#include "../RODMon.h"
#include "../OverviewMon.h"
#include "../TagProbeEfficiencyMon.h"

// Run 1
#include "../CMMMon.h"
#include "../CPMSimBSMon.h"
#include "../JEMMon.h"
// #include "../JEPSimBSMon.h"
#include "../TrigT1CaloBSMon.h"
#include "../TrigT1CaloCpmMonTool.h"
#include "../TrigT1CaloGlobalMonTool.h"
// #include "../EmEfficienciesMonTool.h"
#include "../JetEfficienciesMonTool.h"
#include "../RODMonV1.h"

// Run 3
DECLARE_COMPONENT( CpmMonitorAlgorithm )
//DECLARE_COMPONENT( CpmSimMonitorAlgorithm )
//DECLARE_COMPONENT( PpmSimBsMonitorAlgorithm )
DECLARE_COMPONENT( PprMonitorAlgorithm )
//DECLARE_COMPONENT( PprSpareMonitorAlgorithm )
//DECLARE_COMPONENT( PprStabilityMonitorAlgorithm )
//DECLARE_COMPONENT( LVL1_TagProbeEffMonitorAlgorithm )
DECLARE_COMPONENT( JepJemMonitorAlgorithm )

<<<<<<< HEAD
// Run 2
DECLARE_COMPONENT( LVL1::OverviewMon )
DECLARE_COMPONENT( LVL1::CPMon )
DECLARE_COMPONENT( LVL1::CPSimMon )
DECLARE_COMPONENT( LVL1::JEPJEMMon )
DECLARE_COMPONENT( LVL1::JEPCMXMon )
DECLARE_COMPONENT( LVL1::JEPSimMon )
=======
DECLARE_NAMESPACE_TOOL_FACTORY(LVL1, TagProbeEfficiencyMon)

//Run 1
DECLARE_NAMESPACE_TOOL_FACTORY(LVL1, CMMMon)
DECLARE_NAMESPACE_TOOL_FACTORY(LVL1, CPMSimBSMon)
DECLARE_NAMESPACE_TOOL_FACTORY(LVL1, JEMMon)
//DECLARE_NAMESPACE_TOOL_FACTORY(LVL1, JEPSimBSMon)
DECLARE_NAMESPACE_TOOL_FACTORY(LVL1, TrigT1CaloBSMon)
DECLARE_NAMESPACE_TOOL_FACTORY(LVL1, TrigT1CaloCpmMonTool)
DECLARE_NAMESPACE_TOOL_FACTORY(LVL1, TrigT1CaloGlobalMonTool)
//DECLARE_NAMESPACE_TOOL_FACTORY(LVL1, EmEfficienciesMonTool)
DECLARE_NAMESPACE_TOOL_FACTORY(LVL1, JetEfficienciesMonTool)
DECLARE_NAMESPACE_TOOL_FACTORY(LVL1, RODMonV1)
>>>>>>> release/21.0.127

DECLARE_COMPONENT( LVL1::PPrMon )
DECLARE_COMPONENT( LVL1::MistimedStreamMon )
DECLARE_COMPONENT( LVL1::PPrStabilityMon )
DECLARE_COMPONENT( LVL1::PPrSpareMon )
DECLARE_COMPONENT( LVL1::PPMSimBSMon )
DECLARE_COMPONENT( LVL1::RODMon )

<<<<<<< HEAD
DECLARE_COMPONENT( LVL1::TagProbeEfficiencyMon )
=======
DECLARE_FACTORY_ENTRIES(TrigT1CaloMonitoring) {
  DECLARE_NAMESPACE_ALGTOOL(LVL1, OverviewMon)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, CPMon)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, CPSimMon)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, JEPJEMMon)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, JEPCMXMon)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, JEPSimMon)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, PPrMon)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, PPrStabilityMon)  
  DECLARE_NAMESPACE_ALGTOOL(LVL1, PPrSpareMon)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, PPMSimBSMon)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, RODMon)
 
  DECLARE_NAMESPACE_ALGTOOL(LVL1, TagProbeEfficiencyMon)
  //Run 1
  DECLARE_NAMESPACE_ALGTOOL(LVL1, CMMMon)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, CPMSimBSMon)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, JEMMon)
  //DECLARE_NAMESPACE_ALGTOOL(LVL1, JEPSimBSMon)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, TrigT1CaloBSMon)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, TrigT1CaloCpmMonTool)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, TrigT1CaloGlobalMonTool)
  // DECLARE_NAMESPACE_ALGTOOL(LVL1, EmEfficienciesMonTool)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, JetEfficienciesMonTool)
  DECLARE_NAMESPACE_ALGTOOL(LVL1, RODMonV1)
}
>>>>>>> release/21.0.127

// Run 1
DECLARE_COMPONENT( LVL1::CMMMon )
DECLARE_COMPONENT( LVL1::CPMSimBSMon )
DECLARE_COMPONENT( LVL1::JEMMon )
// DECLARE_COMPONENT( LVL1::JEPSimBSMon )
DECLARE_COMPONENT( LVL1::TrigT1CaloBSMon )
DECLARE_COMPONENT( LVL1::TrigT1CaloCpmMonTool )
DECLARE_COMPONENT( LVL1::TrigT1CaloGlobalMonTool )
// DECLARE_COMPONENT( LVL1::EmEfficienciesMonTool )
DECLARE_COMPONENT( LVL1::JetEfficienciesMonTool )
DECLARE_COMPONENT( LVL1::RODMonV1 )
