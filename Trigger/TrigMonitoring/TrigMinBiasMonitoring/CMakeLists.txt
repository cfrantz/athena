# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( TrigMinBiasMonitoring )

# External dependencies:
find_package( ROOT COMPONENTS Core Hist )

# Component(s) in the package:
<<<<<<< HEAD
=======
atlas_add_library( TrigMinBiasMonitoringLib
                   src/*.cxx
                   PUBLIC_HEADERS TrigMinBiasMonitoring
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES ${ROOT_LIBRARIES} CaloIdentifier xAODEventInfo ZdcIdentifier GaudiKernel AthenaMonitoringLib TrigDecisionToolLib TrigHLTMonitoringLib
                   PRIVATE_LINK_LIBRARIES CaloEvent CaloGeoHelpers xAODTracking xAODTrigMinBias LUCID_RawEvent ZdcEvent InDetBCM_RawData TileEvent TrigCaloEvent TrigInDetEvent InDetTrackSelectionToolLib )

>>>>>>> release/21.0.127
atlas_add_component( TrigMinBiasMonitoring
                     src/*.cxx src/components/*.cxx
                     INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                     LINK_LIBRARIES ${ROOT_LIBRARIES} AthenaMonitoringKernelLib AthenaMonitoringLib CaloEvent CaloGeoHelpers CaloIdentifier GaudiKernel InDetBCM_RawData InDetTrackSelectionToolLib LUCID_RawEvent TileEvent TrigCaloEvent TrigDecisionToolLib TrigHLTMonitoringLib TrigInDetEvent ZdcEvent ZdcIdentifier xAODEventInfo xAODTracking xAODTrigMinBias xAODTrigger )

# Install files from the package:
atlas_install_python_modules( python/*.py POST_BUILD_CMD ${ATLAS_FLAKE8} )
