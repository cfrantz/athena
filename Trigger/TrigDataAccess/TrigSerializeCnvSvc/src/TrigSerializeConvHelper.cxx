/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#include "TrigSerializeCnvSvc/TrigSerializeConvHelper.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ClassID.h"

#include "TrigSerializeCnvSvc/TrigStreamAddress.h"
#include "TrigSerializeResult/ITrigSerializerToolBase.h"
#include "TrigSerializeTP/TrigSerTPTool.h"
#include "TrigSerializeCnvSvc/ITrigSerGuidHelper.h"

#include "DataModelRoot/RootType.h"


TrigSerializeConvHelper::TrigSerializeConvHelper(const std::string& toolname, const std::string& type, const IInterface* parent) :
  AthAlgTool(toolname, type, parent),
  m_serializerTool("TrigTSerializer/TrigTSerializer"),
  m_TPTool("TrigSerTPTool/TrigSerTPTool"),
  m_guidTool("TrigSerializeGuidHelper/TrigSerializeGuidHelper"),
  m_doTP(true)
{
  declareInterface<ITrigSerializeConvHelper>( this );
  declareProperty("doTP", m_doTP, "allows to serialize/deserialize transient classes");
}

TrigSerializeConvHelper::~TrigSerializeConvHelper(){
}

StatusCode TrigSerializeConvHelper::initialize(){

  StatusCode sc = m_serializerTool.retrieve();

  if (!sc.isSuccess()){
<<<<<<< HEAD
    ATH_MSG_DEBUG("m_serializer not retrieved");
    return sc;
  } else {
    ATH_MSG_DEBUG("m_serializer retrieved");
    /*
      if (m_serializerTool->initialize().isSuccess()){
      ATH_MSG_DEBUG( "serializer initialized" );
=======
    msg(MSG::DEBUG) << "m_serializer not retrieved" << endmsg;
    return sc;
  } else {
    msg(MSG::DEBUG) << "m_serializer retrieved" << endmsg;
    /*
      if (m_serializerTool->initialize().isSuccess()){
      msg(MSG::DEBUG) << "serializer initialized" << endmsg;
>>>>>>> release/21.0.127
      }
    */
  }
  
  StatusCode sctp = m_TPTool.retrieve();
  if (!sctp.isSuccess()){
<<<<<<< HEAD
    ATH_MSG_DEBUG( "m_TPTool not retrieved" );
    return sctp;
  } else {
    ATH_MSG_DEBUG( "m_TPTool retrieved" );
    /*
      if (m_TPTool->initialize().isSuccess())
      ATH_MSG_DEBUG( "m_TPTool initialized" );
=======
    msg(MSG::DEBUG) << "m_TPTool not retrieved" << endmsg;
    return sctp;
  } else {
    msg(MSG::DEBUG) << "m_TPTool retrieved" << endmsg;
    /*
      if (m_TPTool->initialize().isSuccess())
      msg(MSG::DEBUG) << "m_TPTool initialized" << endmsg;
>>>>>>> release/21.0.127
      */
  }

  StatusCode scg = m_guidTool.retrieve();
  if (!scg.isSuccess()){
<<<<<<< HEAD
    ATH_MSG_DEBUG( m_guidTool << " not retrieved" );
    return scg;
  } else {
    ATH_MSG_DEBUG( m_guidTool << "retrieved" );
=======
    msg(MSG::DEBUG) << m_guidTool << " not retrieved" << endmsg;
    return scg;
  } else {
    msg(MSG::DEBUG) << m_guidTool << "retrieved" << endmsg;
>>>>>>> release/21.0.127
  }
  
  m_oldEDMmap[ "TrigRoiDescriptor" ]         = "TrigRoiDescriptorCollection_tlp1";
  m_oldEDMmap[ "TrigOperationalInfo" ]       = "TrigOperationalInfoCollection_tlp1";
  m_oldEDMmap[ "TrigInDetTrackCollection" ]  = "TrigInDetTrackCollection_tlp1";
  m_oldEDMmap[ "MuonFeature" ]               = "MuonFeatureContainer_tlp1";
  m_oldEDMmap[ "IsoMuonFeature" ]            = "IsoMuonFeatureContainer_tlp1";
  m_oldEDMmap[ "CombinedMuonFeature" ]       = "CombinedMuonFeatureContainer_tlp1";
  m_oldEDMmap[ "TileMuFeature" ]             = "TileMuFeatureContainer_tlp1";
  m_oldEDMmap[ "TileTrackMuFeature" ]        = "TileTrackMuFeatureContainer_tlp1";
  m_oldEDMmap[ "TrigEMCluster" ]             = "TrigEMClusterContainer_tlp1";
  //m_oldEDMmap[ "TrigEMCluster" ]             = "TrigEMClusterContainer_tlp2";
  m_oldEDMmap[ "RingerRings" ]               = "RingerRingsContainer_tlp1";
  m_oldEDMmap[ "TrigTauCluster" ]            = "TrigTauClusterContainer_tlp1";
  m_oldEDMmap[ "TrigTauClusterDetails" ]     = "TrigTauClusterDetailsContainer_tlp1";
  m_oldEDMmap[ "TrigTauTracksInfo" ]         = "TrigTauTracksInfoCollection_tlp1";
  m_oldEDMmap[ "TrigT2Jet" ]                 = "TrigT2JetContainer_tlp1";
  m_oldEDMmap[ "TrigElectronContainer" ]     = "TrigElectronContainer_tlp2";
  m_oldEDMmap[ "TrigPhotonContainer" ]       = "TrigPhotonContainer_tlp2";
  m_oldEDMmap[ "TrigTau" ]                   = "TrigTauContainer_tlp1";
  m_oldEDMmap[ "TrigL2BjetContainer" ]       = "TrigL2BjetContainer_tlp2";
  m_oldEDMmap[ "TrigMissingET" ]             = "TrigMissingETContainer_tlp1";
  m_oldEDMmap[ "TrigT2MbtsBits" ]            = "TrigT2MbtsBitsContainer_tlp1";
  m_oldEDMmap[ "TrigSpacePointCounts" ]      = "TrigSpacePointCountsCollection_tlp1";
  m_oldEDMmap[ "TrigTrtHitCounts" ]          = "TrigTrtHitCountsCollection_tlp1";
  m_oldEDMmap[ "CosmicMuonCollection" ]      = "CosmicMuonCollection_tlp1";
  m_oldEDMmap[ "MdtTrackSegmentCollection" ] = "MdtTrackSegmentCollection_tlp1";
  m_oldEDMmap[ "TrigVertexCollection" ]      = "TrigVertexCollection_tlp1";
  m_oldEDMmap[ "TrigL2BphysContainer" ]      = "TrigL2BphysContainer_tlp1";
  m_oldEDMmap[ "TrigEFBphysContainer" ]      = "TrigEFBphysContainer_tlp1";
  m_oldEDMmap[ "TrigEFBjetContainer" ]       = "TrigEFBjetContainer_tlp2";
  m_oldEDMmap[ "JetCollection" ]             = "JetCollection_tlp2";
  m_oldEDMmap[ "CaloClusterContainer" ]      = "CaloClusterContainer_p4";
  m_oldEDMmap[ "CaloShowerContainer" ]       = "CaloShowerContainer_p2";
  //m_oldEDMmap[ "CaloTowerContainer" ]        = "CaloTowerContainer_p1";
  m_oldEDMmap[ "TrigMuonEFContainer" ]       = "TrigMuonEFContainer_tlp1";
  m_oldEDMmap[ "TrigMuonEFInfoContainer" ]   = "TrigMuonEFInfoContainer_tlp1";
  m_oldEDMmap[ "egDetailContainer" ]         = "egDetailContainer_p1";
  m_oldEDMmap[ "egammaContainer" ]           = "egammaContainer_p1";
  m_oldEDMmap[ "Analysis::TauJetContainer" ] = "TauJetContainer_p3";
  m_oldEDMmap[ "TrigTrackCounts" ]           = "TrigTrackCountsCollection_tlp1";
  m_oldEDMmap[ "TrackCollection" ]           = "Trk::TrackCollection_tlp2";
  m_oldEDMmap[ "Rec::TrackParticleContainer" ]   = "Rec::TrackParticleContainer_tlp1";
  m_oldEDMmap[ "Analysis::TauDetailsContainer" ] ="TauDetailsContainer_tlp1";
  m_oldEDMmap[ "VxContainer" ]               = "";
  m_oldEDMmap[ "CaloCellContainer" ]         = "CaloCompactCellContainer";

  
  return StatusCode::SUCCESS;
}

StatusCode TrigSerializeConvHelper::createObj(const std::string &clname, IOpaqueAddress* iAddr, void *&ptr, bool isxAOD){
  ptr = 0;
<<<<<<< HEAD
  ATH_MSG_DEBUG("in TrigSerializeConvHelper::createObj for clname " << clname << " is xAOD? " << (isxAOD?"yes":"no"));

  //could alse get DATA (perhaps as boost::any) from the IOA
  TrigStreamAddress *addr = dynamic_cast<TrigStreamAddress*>(iAddr);
  if (!addr) {
    ATH_MSG_WARNING("createObj cast failed");
=======
  msg(MSG::DEBUG) << "in TrigSerializeConvHelper::createObj for clname" << clname << " is xAOD? " << (isxAOD?"yes":"no") << endmsg;
  
  //could alse get DATA (perhaps as boost::any) from the IOA
  TrigStreamAddress *addr = dynamic_cast<TrigStreamAddress*>(iAddr);
  if (!addr) {
    msg(MSG::WARNING) << "createObj cast failed" << endmsg;
>>>>>>> release/21.0.127
    return StatusCode::FAILURE;
  }
  
  ITrigSerializerToolBase* serializer = m_serializerTool.operator->();
  serializer->reset();
  
  std::vector<uint32_t> v = addr->get();
  
  //we need to find the name of the ob
  std::string cl = clname;

  if (m_doTP and !isxAOD)
    cl = m_TPTool->persClassName(clname);

  uint32_t guid[4];

  bool versionChange = false;

  if (cl!=""){
    StatusCode scid;
    scid = serializer->peekCLID(v, guid);
    if (scid.isFailure()){
      //BS has no hint on the pers class - use the original table
<<<<<<< HEAD
      ATH_MSG_DEBUG("BS does not hint on payload object " << cl);
      if (m_oldEDMmap.find(clname)!=m_oldEDMmap.end()){
        if (cl!=m_oldEDMmap[clname]){
          ATH_MSG_DEBUG("Using backward compatibility map with " <<  m_oldEDMmap[clname]
                        << " instead of " << cl);
          cl = m_oldEDMmap[clname];
        }
=======
      if (msgLvl(MSG::DEBUG))
	msg(MSG::DEBUG) << "BS does not hint on payload object " << cl << endmsg;
      if (m_oldEDMmap.find(clname)!=m_oldEDMmap.end()){
	if (cl!=m_oldEDMmap[clname]){
	  msg(MSG::DEBUG) << "Using backward compatibility map with " <<  m_oldEDMmap[clname]
			  << " instead of " << cl << endmsg;
	  cl = m_oldEDMmap[clname];
	}
>>>>>>> release/21.0.127
      }
    } else {
      //get the pers version from the BS
      std::string nclass = cl;
      StatusCode ai = m_guidTool->IntsToClassName(guid, nclass);
      if (ai.isFailure()) {
	//better do not decode
	return StatusCode::FAILURE;
      }
      if (cl != nclass){
<<<<<<< HEAD
        cl =  nclass;
        ATH_MSG_DEBUG("Got hint of " << cl << " different persistent class from the BS payload. Name from GUID: " << nclass);

        if(isxAOD){
          ATH_MSG_DEBUG("This is an xAOD so probably the BS version is an older version of the xAOD type.");
        }
        versionChange = true;
=======
	cl =  nclass;
	if (msgLvl(MSG::DEBUG))
	  msg(MSG::DEBUG) << "Got hint of " << cl
			  << " different persistent class from the BS payload. Name from GUID: " << nclass << endmsg;

	if(isxAOD){
	  msg(MSG::DEBUG) << "This is an xAOD so probably the BS version is an older version of the xAOD type." << endmsg;
	}
	versionChange = true;
>>>>>>> release/21.0.127
      }
    }

    ptr = serializer->deserialize(cl, v);

<<<<<<< HEAD
    ATH_MSG_DEBUG(cl << " deserialized to " << ptr << " version change detected: " << (versionChange ? "yes":"no"));
=======
    if (msgLvl(MSG::DEBUG)) msg(MSG::DEBUG) << cl << " deserialized to " << ptr << " version change detected: " << (versionChange ? "yes":"no") << endmsg;
>>>>>>> release/21.0.127
  }
   
  // T/P separation
  bool isViewVector = cl.substr(0, 11) == "ViewVector<";
  if (m_doTP and (!isxAOD or versionChange) and !isViewVector){
    std::string transclass;

<<<<<<< HEAD
    ATH_MSG_DEBUG("converting with pername " << cl);
    
    void *transObj = m_TPTool->convertPT(cl,ptr, transclass);

    ATH_MSG_DEBUG("was converted to " << transclass << " at " << transObj);
=======
    if (msgLvl(MSG::DEBUG)) msg(MSG::DEBUG) << "converting with pername " << cl << endmsg;
    
    void *transObj = m_TPTool->convertPT(cl,ptr, transclass);

    if (msgLvl(MSG::DEBUG)) msg(MSG::DEBUG) << "was converted to " << transclass << " at " << transObj << endmsg;
>>>>>>> release/21.0.127

    //persistent object not needed anymore
    RootType persClObj(cl);
    persClObj.Destruct(ptr);

    ptr = transObj;
  }

  return StatusCode::SUCCESS;
}



StatusCode TrigSerializeConvHelper::createRep(const std::string &clname,
					      void *ptr, std::vector<uint32_t> &out, bool isxAOD){
  
  StatusCode sc(StatusCode::SUCCESS);

<<<<<<< HEAD
  ATH_MSG_DEBUG("in TrigSerializeConvHelper::createRep for clname" << clname << " is xAOD? " << (isxAOD?"yes":"no"));
=======
  msg(MSG::DEBUG) << "in TrigSerializeConvHelper::createRep for clname" << clname << " is xAOD? " << (isxAOD?"yes":"no") << endmsg;
>>>>>>> release/21.0.127

  std::string cl = clname;
  void *pObj = ptr;

  // T/P separation
  if (m_doTP and !isxAOD){
    std::string persclass("");
    pObj = m_TPTool->convertTP(clname,ptr, persclass);
    cl = persclass;
  }

<<<<<<< HEAD
  ATH_MSG_DEBUG("convertTP: " << pObj << " of " << cl);

=======
  msg(MSG::DEBUG) << "convertTP: " << pObj << " of "
	 << cl << endmsg;
  
>>>>>>> release/21.0.127
  //void *serptr(0);
  //
  ITrigSerializerToolBase* serializer = m_serializerTool.operator->();
  serializer->reset();

<<<<<<< HEAD
  ATH_MSG_VERBOSE("About to get GUID for " << cl);

=======
  msg(MSG::VERBOSE) << "About to get GUID for " << cl << endmsg;
  
>>>>>>> release/21.0.127
  //opposite from string to class
  uint32_t irep[4];
  StatusCode ai = m_guidTool->ClassNameToInts(cl, irep);
  if (ai.isFailure()){
<<<<<<< HEAD
    ATH_MSG_WARNING("Cannot store class identification for " << cl << " to BS");
  }

  ATH_MSG_VERBOSE("got GUID: " << irep[0] << "-" << irep[1] << "-" << irep[2] << "-" << irep[3]);

=======
    msg(MSG::WARNING) << "Cannot store class identification for "
		      << cl << " to BS" << endmsg;
  }

  msg(MSG::VERBOSE) << "got GUID: " << irep[0] << "-" << irep[1] << "-" << irep[2] << "-" << irep[3] << endmsg;
  
>>>>>>> release/21.0.127
  if (cl != "" && pObj){
    serializer->setCLID(irep);
    serializer->serialize(cl, pObj, out);
  } else {
<<<<<<< HEAD
    ATH_MSG_WARNING("did not serialize " << ptr << " of " << clname);
=======
    msg(MSG::WARNING) << "did not serialize " << ptr << " of " << clname << endmsg;
>>>>>>> release/21.0.127
  }

  if (m_doTP and !isxAOD){
    //we don't need the persistent object anymore
     RootType persClObj(cl);
     persClObj.Destruct(pObj);
  }

<<<<<<< HEAD
  ATH_MSG_DEBUG("pObj: " << pObj << " of " << cl << " payload: " << out.size());
=======
  if (msgLvl(MSG::DEBUG)) msg(MSG::DEBUG) << "pObj: " << pObj << " of " << cl
	 << " payload: " << out.size() << endmsg;
>>>>>>> release/21.0.127

  return sc;
}
