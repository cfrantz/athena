/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#include "CscDataPreparator.h"

<<<<<<< HEAD
=======
#include "GaudiKernel/ToolFactory.h"
#include "StoreGate/StoreGateSvc.h"
>>>>>>> release/21.0.127
#include "StoreGate/ActiveStoreSvc.h"
#include "xAODTrigMuon/TrigMuonDefs.h"
<<<<<<< HEAD
=======
#include "TrigSteeringEvent/TrigRoiDescriptor.h"

#include "AthenaBaseComps/AthMsgStreamMacros.h"


using namespace Muon;
>>>>>>> release/21.0.127

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

<<<<<<< HEAD
bool IsUnspoiled ( Muon::CscClusterStatus status );
=======
static const InterfaceID IID_CscDataPreparator("IID_CscDataPreparator", 1, 0);
bool IsUnspoiled ( Muon::CscClusterStatus status );


const InterfaceID& TrigL2MuonSA::CscDataPreparator::interfaceID() { return IID_CscDataPreparator; }
>>>>>>> release/21.0.127

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

TrigL2MuonSA::CscDataPreparator::CscDataPreparator(const std::string& type, 
						   const std::string& name,
						   const IInterface*  parent): 
<<<<<<< HEAD
   AthAlgTool(type,name,parent)
=======
   AthAlgTool(type,name,parent),
   m_storeGateSvc( "StoreGateSvc", name ),
   p_ActiveStore(0),
   m_regionSelector(0),
   m_cscPrepDataProvider("Muon::CscRdoToCscPrepDataTool/CscPrepDataProviderTool"),
   m_cscClusterProvider("CscThresholdClusterBuilderTool")
{
   declareInterface<TrigL2MuonSA::CscDataPreparator>(this);

   declareProperty("CscPrepDataProvider", m_cscPrepDataProvider);
   declareProperty("CscClusterProvider",  m_cscClusterProvider);
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

TrigL2MuonSA::CscDataPreparator::~CscDataPreparator() 
>>>>>>> release/21.0.127
{
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode TrigL2MuonSA::CscDataPreparator::initialize()
{

<<<<<<< HEAD
   ATH_CHECK(m_idHelperSvc.retrieve());
=======

  ATH_MSG_DEBUG("Initializing CscDataPreparator - package version " << PACKAGE_VERSION);
   
   StatusCode sc;
   sc = AthAlgTool::initialize();
   if (!sc.isSuccess()) {
     ATH_MSG_ERROR("Could not initialize the AthAlgTool base class.");
      return sc;
   }
   
   // Locate the StoreGateSvc
   sc =  m_storeGateSvc.retrieve();
   if (!sc.isSuccess()) {
     ATH_MSG_ERROR("Could not find StoreGateSvc");
      return sc;
   }

   // Retrieve ActiveStore
   sc = serviceLocator()->service("ActiveStoreSvc", p_ActiveStore);
   if( !sc.isSuccess() || 0 == p_ActiveStore ){
     ATH_MSG_ERROR(" Could not find ActiveStoreSvc ");
     return sc;
   }

   // prepdataprovider
   sc = m_cscPrepDataProvider.retrieve();
   if (sc.isSuccess()) {
     ATH_MSG_INFO("Retrieved " << m_cscPrepDataProvider);
   } else {
     ATH_MSG_FATAL("Could not get " << m_cscPrepDataProvider);
     return sc;
   }

   // clusterization tool
   sc = m_cscClusterProvider.retrieve();
   if (sc.isSuccess()) {
     ATH_MSG_INFO("Retrieved " << m_cscClusterProvider);
   } else {
     ATH_MSG_FATAL("Could not get " << m_cscClusterProvider);
     return sc;
   }

   // Detector Store
   StoreGateSvc* detStore;
   sc = serviceLocator()->service("DetectorStore", detStore);
   if( sc.isFailure() ){
     ATH_MSG_ERROR("Could not retrieve  DetectorStore.");
     return sc;
   }
   ATH_MSG_DEBUG("Retrieved DetectorStore.");

   // CSC ID helper
   sc = detStore->retrieve( m_muonMgr, "Muon" );
   if( sc.isFailure() ){
     ATH_MSG_ERROR(" Cannot retrieve MuonGeoModel ");
     return sc;
   }
   ATH_MSG_DEBUG("Retrieved GeoModel from DetectorStore.");
   m_cscIdHelper = m_muonMgr->cscIdHelper();

   //
   std::string serviceName;

   // Locate RegionSelector
   serviceName = "RegionSelector";
   sc = service("RegSelSvc", m_regionSelector);
   if(sc.isFailure()) {
     ATH_MSG_ERROR("Could not retrieve " << serviceName);
      return sc;
   }
   ATH_MSG_DEBUG("Retrieved service " << serviceName);

   // 
   return StatusCode::SUCCESS; 
}
>>>>>>> release/21.0.127

   ATH_CHECK(m_cscPrepContainerKey.initialize(!m_cscPrepContainerKey.empty()));

   return StatusCode::SUCCESS; 
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode TrigL2MuonSA::CscDataPreparator::prepareData(TrigL2MuonSA::MuonRoad& muonRoad,
							TrigL2MuonSA::CscHits&  cscHits) const
{
<<<<<<< HEAD
=======
  const IRoiDescriptor* iroi = (IRoiDescriptor*) p_roids;


  // Select RoI hits
  std::vector<IdentifierHash> cscHashIDs;
  cscHashIDs.clear();
  if (m_use_RoIBasedDataAccess) {
    ATH_MSG_DEBUG("Use RoI based data access");
    m_regionSelector->DetHashIDList( CSC, *iroi, cscHashIDs );
  } else {
    ATH_MSG_DEBUG("Use full data access");
    //    m_regionSelector->DetHashIDList( CSC, cscHashIDs ); full decoding is executed with an empty vector
  }
  ATH_MSG_DEBUG("cscHashIDs.size()=" << cscHashIDs.size());

  bool to_full_decode=( fabs(p_roids->etaMinus())>1.7 || fabs(p_roids->etaPlus())>1.7 ) && !m_use_RoIBasedDataAccess;

  // Decode
  std::vector<IdentifierHash> cscHashIDs_decode;
  cscHashIDs_decode.clear();
  if( !cscHashIDs.empty() || to_full_decode ){
    if( m_cscPrepDataProvider->decode( cscHashIDs, cscHashIDs_decode ).isFailure() ){
      ATH_MSG_WARNING("Problems when preparing CSC PrepData");
    }
    cscHashIDs.clear();
  }
  ATH_MSG_DEBUG("cscHashIDs_decode.size()=" << cscHashIDs_decode.size());

  // Clustering
  std::vector<IdentifierHash> cscHashIDs_cluster;
  cscHashIDs_cluster.clear();
  if(to_full_decode) cscHashIDs_decode.clear();
  if( !cscHashIDs_decode.empty() || to_full_decode ){
    if( m_cscClusterProvider->getClusters( cscHashIDs_decode, cscHashIDs_cluster ).isFailure() ){
      ATH_MSG_WARNING("Problems when preparing CSC Clusters");
    }
    cscHashIDs_decode.clear();
  }

  // Debug info
  ATH_MSG_DEBUG("CSC cluster #hash = " << cscHashIDs_cluster.size());
>>>>>>> release/21.0.127

  // Clear the output
  cscHits.clear();

  // Get CSC container
<<<<<<< HEAD
  if(!m_cscPrepContainerKey.empty()){
    auto cscPrepContainerHandle = SG::makeHandle(m_cscPrepContainerKey);
    const Muon::CscPrepDataContainer* cscPrepContainer = cscPrepContainerHandle.cptr();
    if (!cscPrepContainerHandle.isValid()) {
      ATH_MSG_ERROR("Cannot retrieve CSC PRD Container key: " << m_cscPrepContainerKey.key());
      return StatusCode::FAILURE;
    }    
=======
  if( !cscHashIDs_cluster.empty() ){
    const CscPrepDataContainer* cscPrepContainer = 0;
    StatusCode sc = (*p_ActiveStore)->retrieve( cscPrepContainer, "CSC_Clusters" );
    if( sc.isFailure() ){
      ATH_MSG_ERROR(" Cannot retrieve CSC PRD Container ");
      return sc;
    }
    
    // Loop over collections
    CscPrepDataContainer::const_iterator it = cscPrepContainer->begin();
    CscPrepDataContainer::const_iterator it_end = cscPrepContainer->end();
    for( ; it != it_end; ++it ){
      const Muon::CscPrepDataCollection* col = *it;
      if( !col ) continue;
>>>>>>> release/21.0.127

    // Loop over collections
    for( const Muon::CscPrepDataCollection* cscCol : *cscPrepContainer ){
      if( cscCol==nullptr ) continue;
      cscHits.reserve( cscHits.size() + cscCol->size() );
      // Loop over data in the collection
      for( const Muon::CscPrepData* prepData : *cscCol ) {
    	if( prepData==nullptr ) continue;

	// Road info
	int chamber = xAOD::L2MuonParameters::Chamber::CSC;
	double aw = muonRoad.aw[chamber][0];
	double bw = muonRoad.bw[chamber][0];
	//double rWidth = muonRoad.rWidth[chamber][0];
	double phiw = muonRoad.phi[4][0];//roi_descriptor->phi(); //muonRoad.phi[chamber][0];

	//cluster status
<<<<<<< HEAD
	bool isunspoiled = IsUnspoiled (prepData->status());
=======
	bool isunspoiled = IsUnspoiled (prepData.status());
>>>>>>> release/21.0.127


	// Create new digit
	TrigL2MuonSA::CscHitData cscHit;
<<<<<<< HEAD
	cscHit.StationName  = m_idHelperSvc->cscIdHelper().stationName( prepData->identify() );
	cscHit.StationEta   = m_idHelperSvc->cscIdHelper().stationEta( prepData->identify() );
	cscHit.StationPhi   = m_idHelperSvc->cscIdHelper().stationPhi( prepData->identify() );
	cscHit.ChamberLayer = (true==isunspoiled) ? 1 : 0;
	cscHit.WireLayer    = m_idHelperSvc->cscIdHelper().wireLayer( prepData->identify() );
	cscHit.MeasuresPhi  = m_idHelperSvc->cscIdHelper().measuresPhi( prepData->identify() );
	cscHit.Strip        = m_idHelperSvc->cscIdHelper().strip( prepData->identify() );
=======
	cscHit.StationName  = m_cscIdHelper->stationName( prepData.identify() );
	cscHit.StationEta   = m_cscIdHelper->stationEta( prepData.identify() );
	cscHit.StationPhi   = m_cscIdHelper->stationPhi( prepData.identify() );
	cscHit.ChamberLayer = (true==isunspoiled) ? 1 : 0;//m_cscIdHelper->chamberLayer( prepData.identify() );
	cscHit.WireLayer    = m_cscIdHelper->wireLayer( prepData.identify() );
	cscHit.MeasuresPhi  = m_cscIdHelper->measuresPhi( prepData.identify() );
	cscHit.Strip        = m_cscIdHelper->strip( prepData.identify() );
>>>>>>> release/21.0.127
	cscHit.Chamber      = chamber;
	cscHit.StripId = (cscHit.StationName << 18)
	  | ((cscHit.StationEta + 2) << 16) | (cscHit.StationPhi << 12)
	  | (cscHit.WireLayer << 9) | (cscHit.MeasuresPhi << 8) | (cscHit.Strip);
<<<<<<< HEAD
	cscHit.eta = prepData->globalPosition().eta();
	cscHit.phi = prepData->globalPosition().phi();
	cscHit.r   = prepData->globalPosition().perp();
	cscHit.z   = prepData->globalPosition().z();
	cscHit.charge = prepData->charge();
	cscHit.time   = prepData->time();
	cscHit.resolution = std::sqrt( prepData->localCovariance()(0,0) );
	cscHit.Residual =  ( cscHit.MeasuresPhi==0 ) ? calc_residual( aw, bw, cscHit.z, cscHit.r ) : calc_residual_phi( aw,bw,phiw, cscHit.phi, cscHit.z);
	cscHit.isOutlier = 0;
	/*if( std::abs(cscHit.Residual) > rWidth ) {
	  cscHit.isOutlier = 2;
	  }*/
	double width = (cscHit.MeasuresPhi) ? 250. : 100.;
	if( std::abs(cscHit.Residual)>width ){
	  cscHit.isOutlier=1;
	  if( std::abs(cscHit.Residual)>3.*width ){
=======
	cscHit.eta = sqrt( prepData.localCovariance()(0,0) );
	cscHit.phi = prepData.globalPosition().phi();
	cscHit.r   = prepData.globalPosition().perp();
	cscHit.z   = prepData.globalPosition().z();
	cscHit.charge = prepData.charge();
	cscHit.time   = prepData.time();
	cscHit.Residual =  ( cscHit.MeasuresPhi==0 ) ? calc_residual( aw, bw, cscHit.z, cscHit.r ) : calc_residual_phi( aw,bw,phiw, cscHit.phi, cscHit.z);
	cscHit.isOutlier = 0;
	/*if( fabs(cscHit.Residual) > rWidth ) {
	  cscHit.isOutlier = 2;
	  }*/
	double width = (cscHit.MeasuresPhi) ? 250. : 100.;
	if( fabs(cscHit.Residual)>width ){
	  cscHit.isOutlier=1;
	  if( fabs(cscHit.Residual)>3.*width ){
>>>>>>> release/21.0.127
	    cscHit.isOutlier=2;
	  }
	}
	
	cscHits.push_back( cscHit );
	
	// Debug print
       	ATH_MSG_DEBUG("CSC Hits: "
		      << "SN="  << cscHit.StationName << ","
		      << "SE="  << cscHit.StationEta << ","
		      << "SP="  << cscHit.StationPhi << ","
		      << "CL="  << cscHit.ChamberLayer << ","
		      << "WL="  << cscHit.WireLayer << ","
		      << "MP="  << cscHit.MeasuresPhi << ","
		      << "St="  << cscHit.Strip << ","
		      << "ID="  << cscHit.StripId << ","
		      << "eta=" << cscHit.eta << ","
		      << "phi=" << cscHit.phi << ","
		      << "r="   << cscHit.r << ","
		      << "z="   << cscHit.z << ","
		      << "q="   << cscHit.charge << ","
		      << "t="   << cscHit.time << ","
		      << "Rs="  << cscHit.Residual << ","
		      << "OL="  << cscHit.isOutlier);
      }
    }
  }

  //
  return StatusCode::SUCCESS;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

double TrigL2MuonSA::CscDataPreparator::calc_residual(double aw,double bw,double x,double y) const
{
  const double ZERO_LIMIT = 1e-4;
  if( std::abs(aw) < ZERO_LIMIT ) return y-bw;
  double ia  = 1/aw;
  double iaq = ia*ia;
  double dz  = x - (y-bw)*ia;
<<<<<<< HEAD
  return dz/std::sqrt(1.+iaq);

}

=======
  return dz/sqrt(1.+iaq);

}


double TrigL2MuonSA::CscDataPreparator::calc_residual_phi( double aw, double bw, double phiw, double hitphi, double hitz){

  double roadr = hitz*aw + bw;

  return roadr*( cos(phiw)*sin(hitphi)-sin(phiw)*cos(hitphi) );

}


// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
>>>>>>> release/21.0.127

double TrigL2MuonSA::CscDataPreparator::calc_residual_phi( double aw, double bw, double phiw, double hitphi, double hitz) const 
{
<<<<<<< HEAD

  double roadr = hitz*aw + bw;

  return roadr*( std::cos(phiw)*std::sin(hitphi)-std::sin(phiw)*std::cos(hitphi) );

=======
  ATH_MSG_DEBUG("Finalizing CscDataPreparator - package version " << PACKAGE_VERSION);
   
   StatusCode sc = AthAlgTool::finalize(); 
   return sc;
>>>>>>> release/21.0.127
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------


bool IsUnspoiled ( Muon::CscClusterStatus status ) {
  if (status == Muon::CscStatusUnspoiled || status == Muon::CscStatusSplitUnspoiled )
    return true;

  return false;
}
