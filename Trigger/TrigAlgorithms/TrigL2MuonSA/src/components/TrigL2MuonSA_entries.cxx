
<<<<<<< HEAD
#include "../MuFastSteering.h"
#include "../MuFastDataPreparator.h"
#include "../TgcDataPreparator.h"
#include "../RpcDataPreparator.h"
#include "../MdtDataPreparator.h"
#include "../CscDataPreparator.h"
#include "../TgcRoadDefiner.h"
#include "../TgcFit.h"
#include "../RpcRoadDefiner.h"
#include "../RpcPatFinder.h"
#include "../MdtRegionDefiner.h"
#include "../MuFastPatternFinder.h"
#include "../MuFastTrackFitter.h"
#include "../MuFastStationFitter.h"
#include "../MuFastTrackExtrapolator.h"
#include "../AlphaBetaEstimate.h"
#include "../SagittaRadiusEstimate.h"
#include "../PtFromAlphaBeta.h"
#include "../PtFromRadius.h"
#include "../PtBarrelLUT.h"
#include "../PtEndcapLUT.h"
#include "../AlignmentBarrelLUT.h"
#include "../PtBarrelLUTSvc.h"
#include "../PtEndcapLUTSvc.h"
#include "../AlignmentBarrelLUTSvc.h"
#include "../MuCalStreamerTool.h"
#include "../CscSegmentMaker.h"
#include "../CscRegUtils.h"
#include "../StgcDataPreparator.h"
#include "../MmDataPreparator.h"
#include "../NswPatternFinder.h"
#include "../NswStationFitter.h"
#include "../FtfRoadDefiner.h"
#include "../RpcClusterPreparator.h"
#include "../ClusterPatFinder.h"
#include "../ClusterRoadDefiner.h"

using namespace TrigL2MuonSA;

DECLARE_COMPONENT( MuFastSteering )
DECLARE_COMPONENT( MuCalStreamerTool )
DECLARE_COMPONENT( MuFastDataPreparator )
DECLARE_COMPONENT( TgcDataPreparator )
DECLARE_COMPONENT( RpcDataPreparator )
DECLARE_COMPONENT( MdtDataPreparator )
DECLARE_COMPONENT( CscDataPreparator )
DECLARE_COMPONENT( TgcRoadDefiner )
DECLARE_COMPONENT( TgcFit )
DECLARE_COMPONENT( RpcRoadDefiner )
DECLARE_COMPONENT( RpcPatFinder )
DECLARE_COMPONENT( MdtRegionDefiner )
DECLARE_COMPONENT( MuFastPatternFinder )
DECLARE_COMPONENT( MuFastStationFitter )
DECLARE_COMPONENT( MuFastTrackFitter )
DECLARE_COMPONENT( MuFastTrackExtrapolator )
DECLARE_COMPONENT( AlphaBetaEstimate )
DECLARE_COMPONENT( SagittaRadiusEstimate )
DECLARE_COMPONENT( PtFromAlphaBeta )
DECLARE_COMPONENT( PtFromRadius )
DECLARE_COMPONENT( PtBarrelLUT )
DECLARE_COMPONENT( PtEndcapLUT )
DECLARE_COMPONENT( AlignmentBarrelLUT )
DECLARE_COMPONENT( CscSegmentMaker )
DECLARE_COMPONENT( CscRegDict )
DECLARE_COMPONENT( StgcDataPreparator )
DECLARE_COMPONENT( MmDataPreparator )
DECLARE_COMPONENT( NswPatternFinder )
DECLARE_COMPONENT( NswStationFitter )
DECLARE_COMPONENT( FtfRoadDefiner )
=======
#include "TrigL2MuonSA/MuFastSteering.h"
#include "TrigL2MuonSA/MuFastDataPreparator.h"
#include "TrigL2MuonSA/TgcDataPreparator.h"
#include "TrigL2MuonSA/RpcDataPreparator.h"
#include "TrigL2MuonSA/MdtDataPreparator.h"
#include "TrigL2MuonSA/CscDataPreparator.h"
#include "TrigL2MuonSA/TgcRoadDefiner.h"
#include "TrigL2MuonSA/TgcFit.h"
#include "TrigL2MuonSA/RpcRoadDefiner.h"
#include "TrigL2MuonSA/RpcPatFinder.h"
#include "TrigL2MuonSA/MdtRegionDefiner.h"
#include "TrigL2MuonSA/MuFastPatternFinder.h"
#include "TrigL2MuonSA/MuFastTrackFitter.h"
#include "TrigL2MuonSA/MuFastStationFitter.h"
#include "TrigL2MuonSA/MuFastTrackExtrapolator.h"
#include "TrigL2MuonSA/AlphaBetaEstimate.h"
#include "TrigL2MuonSA/SagittaRadiusEstimate.h"
#include "TrigL2MuonSA/PtFromAlphaBeta.h"
#include "TrigL2MuonSA/PtFromRadius.h"
#include "TrigL2MuonSA/PtBarrelLUT.h"
#include "TrigL2MuonSA/PtEndcapLUT.h"
#include "TrigL2MuonSA/AlignmentBarrelLUT.h"
#include "TrigL2MuonSA/PtBarrelLUTSvc.h"
#include "TrigL2MuonSA/PtEndcapLUTSvc.h"
#include "TrigL2MuonSA/AlignmentBarrelLUTSvc.h"
#include "TrigL2MuonSA/MuCalStreamerTool.h"
#include "TrigL2MuonSA/CscSegmentMaker.h"
#include "TrigL2MuonSA/CscRegUtils.h"

using namespace TrigL2MuonSA;

DECLARE_ALGORITHM_FACTORY(MuFastSteering)
DECLARE_TOOL_FACTORY(MuCalStreamerTool)
DECLARE_TOOL_FACTORY(MuFastDataPreparator)
DECLARE_TOOL_FACTORY(TgcDataPreparator)
DECLARE_TOOL_FACTORY(RpcDataPreparator)
DECLARE_TOOL_FACTORY(MdtDataPreparator)
DECLARE_TOOL_FACTORY(CscDataPreparator)
DECLARE_TOOL_FACTORY(TgcRoadDefiner)
DECLARE_TOOL_FACTORY(TgcFit)
DECLARE_TOOL_FACTORY(RpcRoadDefiner)
DECLARE_TOOL_FACTORY(RpcPatFinder)
DECLARE_TOOL_FACTORY(MdtRegionDefiner)
DECLARE_TOOL_FACTORY(MuFastPatternFinder)
DECLARE_TOOL_FACTORY(MuFastStationFitter)
DECLARE_TOOL_FACTORY(MuFastTrackFitter)
DECLARE_TOOL_FACTORY(MuFastTrackExtrapolator)
DECLARE_TOOL_FACTORY(AlphaBetaEstimate)
DECLARE_TOOL_FACTORY(SagittaRadiusEstimate)
DECLARE_TOOL_FACTORY(PtFromAlphaBeta)
DECLARE_TOOL_FACTORY(PtFromRadius)
DECLARE_TOOL_FACTORY(PtBarrelLUT)
DECLARE_TOOL_FACTORY(PtEndcapLUT)
DECLARE_TOOL_FACTORY(AlignmentBarrelLUT)
DECLARE_TOOL_FACTORY(CscSegmentMaker)
DECLARE_TOOL_FACTORY(CscRegDict)
>>>>>>> release/21.0.127

DECLARE_COMPONENT( PtBarrelLUTSvc )
DECLARE_COMPONENT( PtEndcapLUTSvc )
DECLARE_COMPONENT( AlignmentBarrelLUTSvc )

<<<<<<< HEAD
DECLARE_COMPONENT( RpcClusterPreparator )
DECLARE_COMPONENT( ClusterPatFinder )
DECLARE_COMPONENT( ClusterRoadDefiner )
=======
DECLARE_FACTORY_ENTRIES(TrigL2MuonSA)
{
  DECLARE_ALGORITHM(MuFastSteering);
  DECLARE_TOOL(MuCalStreamerTool)
  DECLARE_TOOL(MuFastDataPreparator);
  DECLARE_TOOL(TgcDataPreparator);
  DECLARE_TOOL(RpcDataPreparator);
  DECLARE_TOOL(MdtDataPreparator);
  DECLARE_TOOL(CscDataPreparator);
  DECLARE_TOOL(TgcRoadDefiner);
  DECLARE_TOOL(TgcFit);
  DECLARE_TOOL(RpcRoadDefiner);
  DECLARE_TOOL(RpcPatFinder);
  DECLARE_TOOL(MdtRegionDefiner);
  DECLARE_TOOL(MuFastPatternFinder);
  DECLARE_TOOL(MuFastStationFitter);
  DECLARE_TOOL(MuFastTrackFitter);
  DECLARE_TOOL(MuFastTrackExtrapolator);
  DECLARE_TOOL(AlphaBetaEstimate);
  DECLARE_TOOL(SagittaRadiusEstimate);
  DECLARE_TOOL(PtFromAlphaBeta);
  DECLARE_TOOL(PtFromRadius);
  DECLARE_TOOL(PtBarrelLUT);
  DECLARE_TOOL(PtEndcapLUT);
  DECLARE_TOOL(AlignmentBarrelLUT);
  DECLARE_TOOL(CscSegmentMaker);
  DECLARE_TOOL(CscRegDict);
>>>>>>> release/21.0.127

