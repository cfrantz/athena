/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

<<<<<<< HEAD
#include "MuFastSteering.h"
=======
#include <numeric>
#include <string>

#include "TrigL2MuonSA/MuFastSteering.h"
>>>>>>> release/21.0.127
#include "xAODTrigMuon/L2StandAloneMuonAuxContainer.h"
#include "xAODTrigMuon/L2CombinedMuonAuxContainer.h"
#include "xAODTrigMuon/TrigMuonDefs.h"
#include "xAODTrigger/MuonRoI.h"

#include "CxxUtils/phihelper.h"
#include "TrigTimeAlgs/TrigTimer.h"
#include "TrigSteeringEvent/TrigRoiDescriptor.h"
#include "TrigT1Interfaces/RecMuonRoI.h"

<<<<<<< HEAD
#include "AthenaMonitoringKernel/Monitored.h"

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

MuFastSteering::MuFastSteering(const std::string& name, ISvcLocator* svc)
  : AthAlgorithm(name, svc),
    m_timerSvc("TrigTimerSvc", name),
    m_recMuonRoIUtils()
{
}
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode MuFastSteering::initialize()
{
  ATH_MSG_DEBUG("Initializing MuFastSteering - package version " << PACKAGE_VERSION);

  if (m_use_timer) {
    if (m_timerSvc.retrieve().isFailure()) {
      ATH_MSG_ERROR("Unable to locate TrigTimer Service");
    } else {
      ATH_MSG_DEBUG("Retrieved TrigTimer Service");
      m_timingTimers.push_back(m_timerSvc->addItem(name()+":DataPreparator"));
      m_timingTimers.push_back(m_timerSvc->addItem(name()+":PatternFinder"));
      m_timingTimers.push_back(m_timerSvc->addItem(name()+":StationFitter"));
      m_timingTimers.push_back(m_timerSvc->addItem(name()+":TrackFitter"));
      m_timingTimers.push_back(m_timerSvc->addItem(name()+":TrackExtrapolator"));
      m_timingTimers.push_back(m_timerSvc->addItem(name()+":CalibrationStreamer"));
      m_timingTimers.push_back(m_timerSvc->addItem(name()+":TotalProcessing"));
    }
  }

  // Locate DataPreparator
  ATH_CHECK(m_dataPreparator.retrieve());

  // Locate PatternFinder
  ATH_CHECK(m_patternFinder.retrieve());

  // Locate StationFitter
  ATH_CHECK(m_stationFitter.retrieve());

  // Locate TrackFitter
  ATH_CHECK(m_trackFitter.retrieve());

  // Locate TrackExtrapolator
  ATH_CHECK(m_trackExtrapolator.retrieve());

  // BackExtrapolator services
  ATH_CHECK(m_backExtrapolatorTool.retrieve());

  // CscSegmentMaker
  ATH_CHECK(m_cscsegmaker.retrieve());

  // FtfRoadDefiner
  ATH_CHECK(m_ftfRoadDefiner.retrieve());

  // Set service tools
  m_trackExtrapolator->setExtrapolatorTool(&m_backExtrapolatorTool);
  m_dataPreparator->setExtrapolatorTool(&m_backExtrapolatorTool);

  // set road width in case TGC/RPC readout failure
  m_dataPreparator->setRoadWidthForFailure(m_rWidth_RPC_Failed, m_rWidth_TGC_Failed);

  m_dataPreparator->setRpcGeometry(m_use_rpc);
  m_dataPreparator->setRoIBasedDataAccess(m_use_RoIBasedDataAccess_MDT,
                                          m_use_RoIBasedDataAccess_RPC,
                                          m_use_RoIBasedDataAccess_TGC,
                                          m_use_RoIBasedDataAccess_CSC,
					  m_use_RoIBasedDataAccess_STGC,
					  m_use_RoIBasedDataAccess_MM);

  // set data or MC flag
  ATH_CHECK(m_dataPreparator->setMCFlag(m_use_mcLUT));

  ////stationfit mc flag
  ATH_CHECK(m_stationFitter->setMCFlag(m_use_mcLUT));
  ATH_CHECK(m_trackFitter->setMCFlag(m_use_mcLUT));
  m_trackFitter -> setUseEIFromBarrel( m_use_endcapInnerFromBarrel );

  // DataHandles for AthenaMT
  ATH_CHECK(m_eventInfoKey.initialize());
  ATH_CHECK(m_roiCollectionKey.initialize());
  ATH_CHECK(m_run2recRoiCollectionKey.initialize(!m_useRun3Config));
  ATH_CHECK(m_recRoiCollectionKey.initialize(m_useRun3Config));
  // for Inside-Out mode ---
  ATH_CHECK(m_FTFtrackKey.initialize(m_insideOut));
  ATH_CHECK(m_outputCBmuonCollKey.initialize(m_insideOut));
  // ----
  ATH_CHECK(m_muFastContainerKey.initialize());
  ATH_CHECK(m_muCompositeContainerKey.initialize());
  ATH_CHECK(m_muIdContainerKey.initialize());

  ATH_CHECK(m_muMsContainerKey.initialize());
  if (not m_monTool.name().empty()) {
    ATH_CHECK(m_monTool.retrieve());
  }
  ATH_MSG_DEBUG( "topoRoad = " << m_topoRoad);

  if (m_fill_FSIDRoI) {
    ATH_MSG_INFO("will fill " << m_muIdContainerKey.key() << " in Full Scan mode. Please check if it's correct.");
  }

  return StatusCode::SUCCESS;
}
=======
#include "AthenaBaseComps/AthMsgStreamMacros.h"
>>>>>>> release/21.0.127

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

<<<<<<< HEAD
const LVL1::RecMuonRoI* matchingRecRoI( uint32_t roiWord,
		      			const DataVector<LVL1::RecMuonRoI>& collection )
{
  for ( auto recRoI: collection ) {
    if ( recRoI->roiWord() == roiWord ){
      return recRoI;
    }
  }
  return nullptr;
=======
MuFastSteering::MuFastSteering(const std::string& name, ISvcLocator* svc) 
  : HLT::FexAlgo(name, svc), 
    m_storeGate("StoreGateSvc", this->name()), 
    m_timerSvc(0),
    m_regionSelector(0),
    m_dataPreparator("TrigL2MuonSA::MuFastDataPreparator"),
    m_patternFinder("TrigL2MuonSA::MuFastPatternFinder"),
    m_stationFitter("TrigL2MuonSA::MuFastStationFitter"),
    m_trackFitter("TrigL2MuonSA::MuFastTrackFitter"),
    m_trackExtrapolator("TrigL2MuonSA::MuFastTrackExtrapolator"),
    m_backExtrapolatorTool("TrigMuonBackExtrapolator"),
    m_calStreamer("TrigL2MuonSA::MuCalStreamerTool"),
    m_recMuonRoIUtils(),
    m_cscsegmaker("TrigL2MuonSA::CscSegmentMaker"),
    m_rpcHits(), m_tgcHits(),
    m_mdtRegion(), m_muonRoad(),
    m_rpcFitResult(), m_tgcFitResult(),
    m_mdtHits_normal(), m_mdtHits_overlap(),
    m_cscHits(),
    m_ptBarrelLUTSvc(0), m_ptEndcapLUTSvc(0),
    m_jobOptionsSvc(0), m_trigCompositeContainer(0)
{
  declareProperty("DataPreparator",    m_dataPreparator,    "data preparator");
  declareProperty("PatternFinder",     m_patternFinder,     "pattern finder");
  declareProperty("StationFitter",     m_stationFitter,     "station fitter");
  declareProperty("TrackFitter",       m_trackFitter,       "track fitter");
  declareProperty("TrackExtrapolator", m_trackExtrapolator, "track extrapolator");

  declareProperty("BackExtrapolator", m_backExtrapolatorTool, "public tool for back extrapolating the muon tracks to the IV");
  declareProperty("CscSegmentMaker", m_cscsegmaker);
  declareProperty("Timing", m_use_timer=false);
  declareProperty("UseLUTForMC", m_use_mcLUT=true);

  // options for the calibration stream
  declareProperty("DoCalibrationStream", m_doCalStream=false);
  declareProperty("AllowOksConfig", m_allowOksConfig=true);
  declareProperty("MuonCalBufferName", m_calBufferName="/tmp/testOutput");
  declareProperty("MuonCalBufferSize", m_calBufferSize=1024*1024);
  declareProperty("MuonCalDataScouting",m_calDataScouting=false);

  declareProperty("ESD_RPC_size",m_esd_rpc_size=100);
  declareProperty("ESD_TGC_size",m_esd_tgc_size=50);
  declareProperty("ESD_MDT_size",m_esd_mdt_size=100);
  declareProperty("ESD_CSC_size",m_esd_csc_size=100);

  declareProperty("R_WIDTH_RPC_FAILED",m_rWidth_RPC_Failed=400);
  declareProperty("R_WIDTH_TGC_FAILED",m_rWidth_TGC_Failed=200);

  declareProperty("USE_RPC", m_use_rpc = true);
  declareProperty("USE_MDTCSM", m_use_mdtcsm = true);
  declareProperty("USE_ROIBASEDACCESS_MDT", m_use_RoIBasedDataAccess_MDT = true);
  declareProperty("USE_ROIBASEDACCESS_TGC", m_use_RoIBasedDataAccess_TGC = true);
  declareProperty("USE_ROIBASEDACCESS_RPC", m_use_RoIBasedDataAccess_RPC = true);
  declareProperty("USE_ROIBASEDACCESS_CSC", m_use_RoIBasedDataAccess_CSC = true);

  declareProperty("USE_NEW_SEGMENTFIT", m_use_new_segmentfit = true);

  declareProperty("Scale_Road_BarrelInner",m_scaleRoadBarrelInner=1);
  declareProperty("Scale_Road_BarrelMiddle",m_scaleRoadBarrelMiddle=1);
  declareProperty("Scale_Road_BarrelOuter",m_scaleRoadBarrelOuter=1);
  
  declareProperty("WinPt",m_winPt=4.0);

  declareProperty("RpcErrToDebugStream",m_rpcErrToDebugStream = false);

  declareProperty("UseEndcapInnerFromBarrel",m_use_endcapInnerFromBarrel = false);

  declareMonitoredVariable("InnMdtHits", m_inner_mdt_hits);
  declareMonitoredVariable("MidMdtHits", m_middle_mdt_hits);
  declareMonitoredVariable("OutMdtHits", m_outer_mdt_hits);
  declareMonitoredStdContainer("FitResiduals", m_fit_residuals);
  declareMonitoredVariable("Efficiency", m_efficiency);
  declareMonitoredVariable("SagInv", m_sag_inverse);
  declareMonitoredVariable("Address", m_address);
  declareMonitoredVariable("AbsPt", m_absolute_pt);
  declareMonitoredVariable("Sagitta", m_sagitta);
  declareMonitoredVariable("TrackPt", m_track_pt);
  declareMonitoredStdContainer("ResInner", m_res_inner);
  declareMonitoredStdContainer("ResMiddle", m_res_middle);
  declareMonitoredStdContainer("ResOuter", m_res_outer);
  declareMonitoredStdContainer("TrackEta", m_track_eta);
  declareMonitoredStdContainer("TrackPhi", m_track_phi);
  declareMonitoredStdContainer("FailedRoIEta", m_failed_eta);
  declareMonitoredStdContainer("FailedRoIPhi", m_failed_phi);
>>>>>>> release/21.0.127
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

const xAOD::MuonRoI* matchingRecRoI( uint32_t roiWord,
				     const xAOD::MuonRoIContainer& collection )
{
  for ( auto recRoI: collection ) {
    if ( recRoI->roiWord() == roiWord ){
      return recRoI;
    }
  }
  return nullptr;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode MuFastSteering::execute()
{
<<<<<<< HEAD
  ATH_MSG_DEBUG("StatusCode MuFastSteering::execute start");
  ATH_MSG_DEBUG("InsideOutMode: " << m_insideOut);
  ATH_MSG_DEBUG("Multi-TrackMode: " << m_multiTrack << "/ run for endcap RoI -> " << m_doEndcapForl2mt);

  auto totalTimer = Monitored::Timer( "TIME_Total" );
  auto monitorIt	= Monitored::Group(m_monTool, totalTimer );
  totalTimer.start();

  auto ctx = getContext();
  ATH_MSG_DEBUG("Get event context << " << ctx );

  // retrieve with ReadHandle
  auto roiCollectionHandle = SG::makeHandle( m_roiCollectionKey, ctx );
  const TrigRoiDescriptorCollection *roiCollection = roiCollectionHandle.cptr();
  if (!roiCollectionHandle.isValid()){
    ATH_MSG_ERROR("ReadHandle for TrigRoiDescriptorCollection key:" << m_roiCollectionKey.key() << " isn't Valid");
    return StatusCode::FAILURE;
=======
  ATH_MSG_DEBUG("Initializing MuFastSteering - package version " << PACKAGE_VERSION);
  
  if (m_storeGate.retrieve().isFailure()) {
    ATH_MSG_ERROR("Cannot retrieve service StoreGateSvc");
    return HLT::BAD_JOB_SETUP;
>>>>>>> release/21.0.127
  }

<<<<<<< HEAD
  if(m_useRun3Config) {

  auto recRoiCollectionHandle = SG::makeHandle( m_recRoiCollectionKey, ctx );
  const xAOD::MuonRoIContainer *recRoiCollection = recRoiCollectionHandle.cptr();
  if (!recRoiCollectionHandle.isValid()){
    ATH_MSG_ERROR("ReadHandle for xAOD::MuonRoIContainer key:" << m_recRoiCollectionKey.key() << " isn't Valid");
    return StatusCode::FAILURE;
  }

  std::vector< const TrigRoiDescriptor* > internalRoI;
  TrigRoiDescriptorCollection::const_iterator p_roids = roiCollection->begin();
  TrigRoiDescriptorCollection::const_iterator p_roidsEn = roiCollection->end();

  for(; p_roids != p_roidsEn; ++p_roids ) {
    internalRoI.push_back(*p_roids);
    ATH_MSG_DEBUG("REGTEST: " << m_roiCollectionKey.key() << " eta = " << "(" << (*p_roids)->etaMinus() << ")" << (*p_roids)->eta() << "(" << (*p_roids)->etaPlus() << ")");
    ATH_MSG_DEBUG("REGTEST: " << m_roiCollectionKey.key() << " phi = " << "(" << (*p_roids)->phiMinus() << ")" << (*p_roids)->phi() << "(" << (*p_roids)->phiPlus() << ")");
    ATH_MSG_DEBUG("REGTEST: " << m_roiCollectionKey.key() << " zed = " << "(" << (*p_roids)->zedMinus() << ")" << (*p_roids)->zed() << "(" << (*p_roids)->zedPlus() << ")");
  }
  ATH_MSG_DEBUG("REGTEST: " << m_roiCollectionKey.key() << " size = " << internalRoI.size());

  // make RecMURoIs maching with MURoIs
  std::vector< const xAOD::MuonRoI* > recRoIVector;
  std::vector< const xAOD::MuonRoI* > surrRoIs;

  for (size_t size=0; size<roiCollection->size(); size++){
    const xAOD::MuonRoI* recRoI = matchingRecRoI( roiCollection->at(size)->roiWord(),  *recRoiCollection );
    if( recRoI == nullptr ) continue;
    recRoIVector.push_back(recRoI);
    ATH_MSG_DEBUG("REGTEST: " << m_recRoiCollectionKey.key() << " eta/phi = " << (recRoI)->eta() << "/" << (recRoI)->phi());
    ATH_MSG_DEBUG("REGTEST: " << m_recRoiCollectionKey.key() << " size = " << recRoIVector.size());
  }

  int nPassedBarrelSurrRoi = 0;
  if(m_topoRoad ){
    for( const auto recRoI: *recRoiCollection ){
      if(std::find(recRoIVector.begin(), recRoIVector.end(), recRoI) != recRoIVector.end()) continue;

      bool surrounding = false;
      for( const auto matchedRoI: recRoIVector ){
        float deta = std::abs(recRoI->eta() - matchedRoI->eta());
        float dphi = std::abs(recRoI->phi() - matchedRoI->phi());
        if( dphi > M_PI )dphi = 2.*M_PI - dphi;
        if( deta < m_dEtasurrRoI && dphi < m_dPhisurrRoI)
          surrounding = true;
      }

      if(surrounding)
        surrRoIs.push_back(recRoI);
    }

    ATH_MSG_DEBUG("surrRoI: " << " size = " << surrRoIs.size());
    for( const auto recRoI: surrRoIs ){
      ATH_MSG_DEBUG("surrRoI: " << " eta/phi = " << (recRoI)->eta() << "/" << (recRoI)->phi() );
      if( std::abs((recRoI)->eta()) <= 1.05 && (recRoI)->getThrNumber() >= 1 )nPassedBarrelSurrRoi++;
    }
    ATH_MSG_DEBUG( "nPassedBarrelSurrRoi = " << nPassedBarrelSurrRoi);
    //dynamicDeltaRpcMode
    if( m_topoRoad )m_dataPreparator->setMultiMuonTrigger( (nPassedBarrelSurrRoi >= 1)? true : false );
  }

  // record data objects with WriteHandle
  auto muFastContainer = SG::makeHandle(m_muFastContainerKey, ctx);
  ATH_CHECK(muFastContainer.record(std::make_unique<xAOD::L2StandAloneMuonContainer>(), std::make_unique<xAOD::L2StandAloneMuonAuxContainer>()));

  auto muCompositeContainer = SG::makeHandle(m_muCompositeContainerKey, ctx);
  ATH_CHECK(muCompositeContainer.record(std::make_unique<xAOD::TrigCompositeContainer>(), std::make_unique<xAOD::TrigCompositeAuxContainer>()));

  auto muIdContainer = SG::makeHandle(m_muIdContainerKey, ctx);
  ATH_CHECK(muIdContainer.record(std::make_unique<TrigRoiDescriptorCollection>()));

  auto muMsContainer = SG::makeHandle(m_muMsContainerKey, ctx);
  ATH_CHECK(muMsContainer.record(std::make_unique<TrigRoiDescriptorCollection>()));


  // Inside-out L2Muon mode
  if(m_insideOut) {
    ATH_MSG_DEBUG("start inside-out mode...");

    auto muonCBColl = SG::makeHandle (m_outputCBmuonCollKey, ctx);
    ATH_CHECK( muonCBColl.record (std::make_unique<xAOD::L2CombinedMuonContainer>(),
				  std::make_unique<xAOD::L2CombinedMuonAuxContainer>()) );

    auto trackHandle = SG::makeHandle( m_FTFtrackKey, ctx );
    if (!trackHandle.isValid()){
      ATH_MSG_ERROR("ReadHandle for TrackParticleContainer key:" << m_FTFtrackKey.key() << " isn't Valid");
      return StatusCode::FAILURE;
=======
  m_timerSvc = 0;
  if (m_use_timer) {
    StatusCode sc = service( "TrigTimerSvc", m_timerSvc, true); 
    if( sc.isFailure() ) {
      ATH_MSG_ERROR(": Unable to locate TrigTimer Service");
>>>>>>> release/21.0.127
    }
    const xAOD::TrackParticleContainer *tracks = trackHandle.cptr();

    ATH_CHECK(findMuonSignatureIO(*tracks, internalRoI, recRoIVector,
				  *muonCBColl, *muFastContainer ));

    ATH_MSG_DEBUG("REGTEST: xAOD::L2CombinedMuonContainer key:" << m_outputCBmuonCollKey.key() << " size = " << muonCBColl->size());
    for (const auto p_CBmuon : *muonCBColl){
      ATH_MSG_DEBUG("REGTEST: xAOD::L2CombinedMuonContainer key:" << m_outputCBmuonCollKey.key() << " pt = " << (*p_CBmuon).pt() << " GeV");
      ATH_MSG_DEBUG("REGTEST: xAOD::L2CombinedMuonContainer key:" << m_outputCBmuonCollKey.key() << " eta/phi = " << (*p_CBmuon).eta() << "/" << (*p_CBmuon).phi());
    }

  }
  else if(m_multiTrack){ //multi-track SA mode
    ATH_MSG_DEBUG("start multi-track SA mode...");
    ATH_CHECK(findMultiTrackSignature(internalRoI, recRoIVector, *muFastContainer));
  }
  else {
    // to StatusCode findMuonSignature()
    ATH_CHECK(findMuonSignature(internalRoI, recRoIVector,
				*muFastContainer, *muIdContainer, *muMsContainer// , *muCompositeContainer
				));
  }

<<<<<<< HEAD
  // DEBUG TEST: Recorded data objects
  ATH_MSG_DEBUG("Recorded data objects");
  ATH_MSG_DEBUG("REGTEST: xAOD::L2StandAloneMuonContainer key:" << m_muFastContainerKey.key() << " size = " << muFastContainer->size());

  for (auto  p_muon : *muFastContainer) {
    ATH_MSG_DEBUG("REGTEST: xAOD::L2StandAloneMuonContainer key:" << m_muFastContainerKey.key() << " pt = " << (*p_muon).pt() << " GeV");
    ATH_MSG_DEBUG("REGTEST: xAOD::L2StandAloneMuonContainer key:" << m_muFastContainerKey.key() << " eta/phi = " << (*p_muon).eta() << "/" << (*p_muon).phi());
  }

  ATH_MSG_DEBUG("REGTEST: TrigRoiDescriptorCollection key:" << m_muIdContainerKey.key() << " size = " << muIdContainer->size());
  for (auto  p_muonID : *muIdContainer) {
    ATH_MSG_DEBUG("REGTEST: TrigRoiDescriptorCollection key:" << m_muIdContainerKey.key() << " eta/phi = " << (*p_muonID).eta() << "/" << (*p_muonID).phi());
  }

  ATH_MSG_DEBUG("REGTEST: TrigRoiDescriptorCollection key:" << m_muMsContainerKey.key() << " size = " << muMsContainer->size());
  for (auto  p_muonMS : *muMsContainer) {
    ATH_MSG_DEBUG("REGTEST: TrigRoiDescriptorCollection key:" << m_muMsContainerKey.key() << " eta/phi = " << (*p_muonMS).eta() << "/" << (*p_muonMS).phi());
  }

=======
  // Locate RegionSelector
  sc = service("RegSelSvc", m_regionSelector);
  if( sc.isFailure() ) {
    ATH_MSG_ERROR("Could not retrieve the regionselector service");
    return HLT::ERROR;
  }
  ATH_MSG_DEBUG("Retrieved the RegionSelector service ");

  // 
  if (m_dataPreparator.retrieve().isFailure()) {
    ATH_MSG_ERROR("Cannot retrieve Tool DataPreparator");
    return HLT::BAD_JOB_SETUP;
  }
  
  // 
  if (m_patternFinder.retrieve().isFailure()) {
    ATH_MSG_ERROR("Cannot retrieve Tool DataPreparator");
    return HLT::BAD_JOB_SETUP;
  }

  // 
  if (m_stationFitter.retrieve().isFailure()) {
    ATH_MSG_ERROR("Cannot retrieve Tool StationFitter");
    return HLT::BAD_JOB_SETUP;
>>>>>>> release/21.0.127
  }
  else { // use Run2 L1Muon EDM

<<<<<<< HEAD
  auto recRoiCollectionHandle = SG::makeHandle( m_run2recRoiCollectionKey, ctx );
  const DataVector<LVL1::RecMuonRoI> *recRoiCollection = recRoiCollectionHandle.cptr();
  if (!recRoiCollectionHandle.isValid()){
    ATH_MSG_ERROR("ReadHandle for DataVector<LVL1::RecMuonRoI> key:" << m_run2recRoiCollectionKey.key() << " isn't Valid");
    return StatusCode::FAILURE;
  }

  std::vector< const TrigRoiDescriptor* > internalRoI;
  TrigRoiDescriptorCollection::const_iterator p_roids = roiCollection->begin();
  TrigRoiDescriptorCollection::const_iterator p_roidsEn = roiCollection->end();

  for(; p_roids != p_roidsEn; ++p_roids ) {
    internalRoI.push_back(*p_roids);
    ATH_MSG_DEBUG("REGTEST: " << m_roiCollectionKey.key() << " eta = " << "(" << (*p_roids)->etaMinus() << ")" << (*p_roids)->eta() << "(" << (*p_roids)->etaPlus() << ")");
    ATH_MSG_DEBUG("REGTEST: " << m_roiCollectionKey.key() << " phi = " << "(" << (*p_roids)->phiMinus() << ")" << (*p_roids)->phi() << "(" << (*p_roids)->phiPlus() << ")");
    ATH_MSG_DEBUG("REGTEST: " << m_roiCollectionKey.key() << " zed = " << "(" << (*p_roids)->zedMinus() << ")" << (*p_roids)->zed() << "(" << (*p_roids)->zedPlus() << ")");
  }
  ATH_MSG_DEBUG("REGTEST: " << m_roiCollectionKey.key() << " size = " << internalRoI.size());

  // make RecMURoIs maching with MURoIs
  std::vector< const LVL1::RecMuonRoI* > recRoIVector;
  std::vector< const LVL1::RecMuonRoI* > surrRoIs;

  for (size_t size=0; size<roiCollection->size(); size++){
    const LVL1::RecMuonRoI* recRoI = matchingRecRoI( roiCollection->at(size)->roiWord(),  *recRoiCollection );
    if( recRoI == nullptr ) continue;
    recRoIVector.push_back(recRoI);
    ATH_MSG_DEBUG("REGTEST: " << m_recRoiCollectionKey.key() << " eta/phi = " << (recRoI)->eta() << "/" << (recRoI)->phi());
    ATH_MSG_DEBUG("REGTEST: " << m_recRoiCollectionKey.key() << " size = " << recRoIVector.size());
  }

  int nPassedBarrelSurrRoi = 0;
  if(m_topoRoad ){
    for( const auto recRoI: *recRoiCollection ){
      if(std::find(recRoIVector.begin(), recRoIVector.end(), recRoI) != recRoIVector.end()) continue;

      bool surrounding = false;
      for( const auto matchedRoI: recRoIVector ){
        float deta = std::abs(recRoI->eta() - matchedRoI->eta());
        float dphi = std::abs(recRoI->phi() - matchedRoI->phi());
        if( dphi > M_PI )dphi = 2.*M_PI - dphi;
        if( deta < m_dEtasurrRoI && dphi < m_dPhisurrRoI)
          surrounding = true;
      }

      if(surrounding)
        surrRoIs.push_back(recRoI);
    }
=======
  // 
  if (m_trackFitter.retrieve().isFailure()) {
    ATH_MSG_ERROR("Cannot retrieve Tool TrackFitter");
    return HLT::BAD_JOB_SETUP;
  }

  // 
  if (m_trackExtrapolator.retrieve().isFailure()) {
    ATH_MSG_ERROR("Cannot retrieve Tool TrackExtrapolator");
    return HLT::BAD_JOB_SETUP;
  }

  // BackExtrapolator services
  if ( m_backExtrapolatorTool.retrieve().isFailure() ) {
    ATH_MSG_ERROR("Could not retrieve " << m_backExtrapolatorTool);
    return HLT::BAD_JOB_SETUP;
  } 

  // CscSegmentMaker
  if ( m_cscsegmaker.retrieve().isFailure() ) {
    ATH_MSG_ERROR("Could not retrieve " << m_cscsegmaker);
    return HLT::BAD_JOB_SETUP;
  }


  // Set service tools
  m_trackExtrapolator->setExtrapolatorTool(&m_backExtrapolatorTool);
  m_dataPreparator->setExtrapolatorTool(&m_backExtrapolatorTool);
>>>>>>> release/21.0.127

    ATH_MSG_DEBUG("surrRoI: " << " size = " << surrRoIs.size());
    for( const auto recRoI: surrRoIs ){
      ATH_MSG_DEBUG("surrRoI: " << " eta/phi = " << (recRoI)->eta() << "/" << (recRoI)->phi() );
      if( std::abs((recRoI)->eta()) <= 1.05 && (recRoI)->getThresholdNumber() >= 1 )nPassedBarrelSurrRoi++;
    }
    ATH_MSG_DEBUG( "nPassedBarrelSurrRoi = " << nPassedBarrelSurrRoi);
    //dynamicDeltaRpcMode
    if( m_topoRoad )m_dataPreparator->setMultiMuonTrigger( (nPassedBarrelSurrRoi >= 1)? true : false );
  }

  // record data objects with WriteHandle
  auto muFastContainer = SG::makeHandle(m_muFastContainerKey, ctx);
  ATH_CHECK(muFastContainer.record(std::make_unique<xAOD::L2StandAloneMuonContainer>(), std::make_unique<xAOD::L2StandAloneMuonAuxContainer>()));

  auto muCompositeContainer = SG::makeHandle(m_muCompositeContainerKey, ctx);
  ATH_CHECK(muCompositeContainer.record(std::make_unique<xAOD::TrigCompositeContainer>(), std::make_unique<xAOD::TrigCompositeAuxContainer>()));

<<<<<<< HEAD
  auto muIdContainer = SG::makeHandle(m_muIdContainerKey, ctx);
  ATH_CHECK(muIdContainer.record(std::make_unique<TrigRoiDescriptorCollection>()));

  auto muMsContainer = SG::makeHandle(m_muMsContainerKey, ctx);
  ATH_CHECK(muMsContainer.record(std::make_unique<TrigRoiDescriptorCollection>()));


  // Inside-out L2Muon mode
  if(m_insideOut) {
    ATH_MSG_DEBUG("start inside-out mode...");

    auto muonCBColl = SG::makeHandle (m_outputCBmuonCollKey, ctx);
    ATH_CHECK( muonCBColl.record (std::make_unique<xAOD::L2CombinedMuonContainer>(),
				  std::make_unique<xAOD::L2CombinedMuonAuxContainer>()) );

    auto trackHandle = SG::makeHandle( m_FTFtrackKey, ctx );
    if (!trackHandle.isValid()){
      ATH_MSG_ERROR("ReadHandle for TrackParticleContainer key:" << m_FTFtrackKey.key() << " isn't Valid");
      return StatusCode::FAILURE;
    }
    const xAOD::TrackParticleContainer *tracks = trackHandle.cptr();

    ATH_CHECK(findMuonSignatureIO(*tracks, internalRoI, recRoIVector,
				  *muonCBColl, *muFastContainer ));

    ATH_MSG_DEBUG("REGTEST: xAOD::L2CombinedMuonContainer key:" << m_outputCBmuonCollKey.key() << " size = " << muonCBColl->size());
    for (const auto p_CBmuon : *muonCBColl){
      ATH_MSG_DEBUG("REGTEST: xAOD::L2CombinedMuonContainer key:" << m_outputCBmuonCollKey.key() << " pt = " << (*p_CBmuon).pt() << " GeV");
      ATH_MSG_DEBUG("REGTEST: xAOD::L2CombinedMuonContainer key:" << m_outputCBmuonCollKey.key() << " eta/phi = " << (*p_CBmuon).eta() << "/" << (*p_CBmuon).phi());
    }

  }
  else if(m_multiTrack){ //multi-track SA mode
    ATH_MSG_DEBUG("start multi-track SA mode...");
    ATH_CHECK(findMultiTrackSignature(internalRoI, recRoIVector, *muFastContainer));
  }
  else {
    // to StatusCode findMuonSignature()
    ATH_CHECK(findMuonSignature(internalRoI, recRoIVector,
				*muFastContainer, *muIdContainer, *muMsContainer// , *muCompositeContainer
				));
  }

  // DEBUG TEST: Recorded data objects
  ATH_MSG_DEBUG("Recorded data objects");
  ATH_MSG_DEBUG("REGTEST: xAOD::L2StandAloneMuonContainer key:" << m_muFastContainerKey.key() << " size = " << muFastContainer->size());

  for (auto  p_muon : *muFastContainer) {
    ATH_MSG_DEBUG("REGTEST: xAOD::L2StandAloneMuonContainer key:" << m_muFastContainerKey.key() << " pt = " << (*p_muon).pt() << " GeV");
    ATH_MSG_DEBUG("REGTEST: xAOD::L2StandAloneMuonContainer key:" << m_muFastContainerKey.key() << " eta/phi = " << (*p_muon).eta() << "/" << (*p_muon).phi());
  }

  ATH_MSG_DEBUG("REGTEST: TrigRoiDescriptorCollection key:" << m_muIdContainerKey.key() << " size = " << muIdContainer->size());
  for (auto  p_muonID : *muIdContainer) {
    ATH_MSG_DEBUG("REGTEST: TrigRoiDescriptorCollection key:" << m_muIdContainerKey.key() << " eta/phi = " << (*p_muonID).eta() << "/" << (*p_muonID).phi());
  }

  ATH_MSG_DEBUG("REGTEST: TrigRoiDescriptorCollection key:" << m_muMsContainerKey.key() << " size = " << muMsContainer->size());
  for (auto  p_muonMS : *muMsContainer) {
    ATH_MSG_DEBUG("REGTEST: TrigRoiDescriptorCollection key:" << m_muMsContainerKey.key() << " eta/phi = " << (*p_muonMS).eta() << "/" << (*p_muonMS).phi());
  }

  }

  totalTimer.stop();

  ATH_MSG_DEBUG("StatusCode MuFastSteering::execute() success");
  return StatusCode::SUCCESS;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode MuFastSteering::findMuonSignature(const std::vector<const TrigRoiDescriptor*>& roids,
                                             const std::vector<const LVL1::RecMuonRoI*>&  muonRoIs,
				             DataVector<xAOD::L2StandAloneMuon>& 	outputTracks,
					     TrigRoiDescriptorCollection& 		outputID,
					     TrigRoiDescriptorCollection&		outputMS// ,
					     // DataVector<xAOD::TrigComposite>&          	outputComposite
					     ) const
{
  ATH_MSG_DEBUG("StatusCode MuFastSteering::findMuonSignature start");
  StatusCode sc = StatusCode::SUCCESS;

  auto prepTimer           = Monitored::Timer( "TIME_Data_Preparator" );
  auto patternTimer        = Monitored::Timer( "TIME_Pattern_Finder" );
  auto stationFitterTimer  = Monitored::Timer( "TIME_Station_Fitter" );
  auto trackFitterTimer    = Monitored::Timer( "TIME_Track_Fitter" );
  auto trackExtraTimer     = Monitored::Timer( "TIME_Track_Extrapolator" );
  auto calibrationTimer    = Monitored::Timer( "TIME_Calibration_Streamer" );

  auto monitorIt	= Monitored::Group(m_monTool, prepTimer, patternTimer, stationFitterTimer,
                                                trackFitterTimer, trackExtraTimer, calibrationTimer );

  if (m_use_timer) {
    for (unsigned int i_timer=0; i_timer<m_timingTimers.size(); i_timer++) {
      m_timingTimers[i_timer]->start();
      m_timingTimers[i_timer]->pause();
    }
  }

  if (m_use_timer) m_timingTimers[ITIMER_TOTAL_PROCESSING]->resume();
  if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->resume();

  TrigL2MuonSA::RpcHits      rpcHits;
  TrigL2MuonSA::TgcHits      tgcHits;
  TrigL2MuonSA::MdtRegion    mdtRegion;
  TrigL2MuonSA::MuonRoad     muonRoad;
  TrigL2MuonSA::RpcFitResult rpcFitResult;
  TrigL2MuonSA::TgcFitResult tgcFitResult;
  TrigL2MuonSA::MdtHits      mdtHits_normal;
  TrigL2MuonSA::MdtHits      mdtHits_overlap;
  TrigL2MuonSA::CscHits      cscHits;
  TrigL2MuonSA::StgcHits     stgcHits;
  TrigL2MuonSA::MmHits       mmHits;

  DataVector<const TrigRoiDescriptor>::const_iterator p_roids;
  DataVector<const LVL1::RecMuonRoI>::const_iterator p_roi;

  // muonRoIs = RecMURoIs, roids = MURoIs
  p_roids = roids.begin();
  for (p_roi=(muonRoIs).begin(); p_roi!=(muonRoIs).end(); ++p_roi) {

    prepTimer.start();
    std::vector<TrigL2MuonSA::TrackPattern> trackPatterns;
    rpcHits.clear();
    tgcHits.clear();
    mdtRegion.Clear();
    muonRoad.Clear();
    rpcFitResult.Clear();
    tgcFitResult.Clear();
    mdtHits_normal.clear();
    mdtHits_overlap.clear();
    cscHits.clear();
    stgcHits.clear();
    mmHits.clear();

    if ( m_recMuonRoIUtils.isBarrel(*p_roi) ) { // Barrel
      ATH_MSG_DEBUG("Barrel");

      muonRoad.setScales(m_scaleRoadBarrelInner,
			 m_scaleRoadBarrelMiddle,
			 m_scaleRoadBarrelOuter);

      // Data preparation
      sc = m_dataPreparator->prepareData(*p_roi,
                                         *p_roids,
                                         m_insideOut,
                                         rpcHits,
                                         muonRoad,
                                         mdtRegion,
                                         rpcFitResult,
                                         mdtHits_normal,
                                         mdtHits_overlap);
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Data preparation failed");
	TrigL2MuonSA::TrackPattern trackPattern;
	trackPatterns.push_back(trackPattern);
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
      }
      if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->pause();
      prepTimer.stop();

      // Pattern finding
      if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->resume();
      patternTimer.start();
      sc = m_patternFinder->findPatterns(muonRoad,
                                         mdtHits_normal,
                                         trackPatterns);
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Pattern finder failed");
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
      }
      if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->pause();
      patternTimer.stop();

      // Superpoint fit
      if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->resume();
      stationFitterTimer.start();
      sc = m_stationFitter->findSuperPoints(*p_roids,
                                            muonRoad,
                                            rpcFitResult,
                                            trackPatterns);
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Super point fitter failed");
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
      }
      if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->pause();
      stationFitterTimer.stop();

      // Track fitting
      if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->resume();
      trackFitterTimer.start();
      sc = m_trackFitter->findTracks(*p_roids,
                                      rpcFitResult,
                                      trackPatterns);

      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Track fitter failed");
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
      }
      if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->pause();
      trackFitterTimer.stop();

    } else { // Endcap
      ATH_MSG_DEBUG("Endcap");

      prepTimer.start();
      // Data preparation
      sc = m_dataPreparator->prepareData(*p_roi,
                                         *p_roids,
                                         m_insideOut,
                                         tgcHits,
                                         muonRoad,
                                         mdtRegion,
                                         tgcFitResult,
                                         mdtHits_normal,
                                         mdtHits_overlap,
                                         cscHits,
					 stgcHits,
					 mmHits);
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Data preparation failed");
 	TrigL2MuonSA::TrackPattern trackPattern;
	trackPatterns.push_back(trackPattern);
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
      }
      if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->pause();
      prepTimer.stop();

      // Pattern finding
      if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->resume();
      patternTimer.start();
      sc = m_patternFinder->findPatterns(muonRoad,
                                         mdtHits_normal,
					 stgcHits,
					 mmHits,
                                         trackPatterns);



      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Pattern finder failed");
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
      }
      if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->pause();
      patternTimer.stop();

      // Superpoint fit
      if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->resume();
      stationFitterTimer.start();
      if(!m_use_new_segmentfit){
        sc = m_stationFitter->findSuperPointsSimple(*p_roids,
						    muonRoad,
						    tgcFitResult,
						    trackPatterns);
      }else{
        sc = m_stationFitter->findSuperPoints(*p_roids,
                                              muonRoad,
                                              tgcFitResult,
                                              trackPatterns);
      }
      /////csc SuperPoint
      m_cscsegmaker->FindSuperPointCsc(cscHits,trackPatterns,tgcFitResult,muonRoad);

      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Super point fitter failed");
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
      }

      if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->pause();
      stationFitterTimer.stop();

      // Track fittingh
      if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->resume();
      trackFitterTimer.start();
      sc = m_trackFitter->findTracks(*p_roids,
                                     tgcFitResult,
                                     trackPatterns,
                                     muonRoad);

      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Track fitter failed");
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
        }
      if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->pause();
      trackFitterTimer.stop();
    }

    // fix if eta is strange
    const float ETA_LIMIT       = 2.8;
    const float DELTA_ETA_LIMIT = 1.0;
    const float ZERO_LIMIT = 1.e-5;
    for (TrigL2MuonSA::TrackPattern& track : trackPatterns) {
       float roiEta = (*p_roi)->eta();
       if (std::abs(track.pt) > ZERO_LIMIT
           && ( std::abs(track.etaMap) > ETA_LIMIT || std::abs(track.etaMap-roiEta) > DELTA_ETA_LIMIT ) ) {
          track.etaMap = roiEta;
       }
    }

    // Track extrapolation for ID combined
    if (m_use_timer) m_timingTimers[ITIMER_TRACK_EXTRAPOLATOR]->resume();
    trackExtraTimer.start();

    sc = m_trackExtrapolator->extrapolateTrack(trackPatterns, m_winPt);

    if (sc != StatusCode::SUCCESS) {
      ATH_MSG_WARNING("Track extrapolator failed");
      // Update output trigger element
      updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
      	                  rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			  stgcHits, mmHits,
                          trackPatterns, outputTracks, outputID, outputMS);
      continue;
    }
    if (m_use_timer) m_timingTimers[ITIMER_TRACK_EXTRAPOLATOR]->pause();
    trackExtraTimer.stop();

    // Update monitoring variables
    sc = updateMonitor(*p_roi, mdtHits_normal, trackPatterns );
    if (sc != StatusCode::SUCCESS) {
      ATH_MSG_WARNING("Failed to update monitoring variables");
      // Update output trigger element
      updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
       	                  rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			  stgcHits, mmHits,
                          trackPatterns, outputTracks, outputID, outputMS);
      continue;
    }

    // Update output trigger element
    updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
    	                rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			stgcHits, mmHits,
                        trackPatterns, outputTracks, outputID, outputMS);

    p_roids++;
    if (p_roids==roids.end()) break;
  }

  if (m_use_timer) m_timingTimers[ITIMER_TOTAL_PROCESSING]->pause();

  int nRoI = muonRoIs.size();

  if (m_use_timer) {
     for (unsigned int i_timer=0; i_timer<m_timingTimers.size(); i_timer++) {
         m_timingTimers[i_timer]->propVal(nRoI);
         m_timingTimers[i_timer]->stop();
     }
=======
  // set data or MC flag
  sc = m_dataPreparator->setMCFlag(m_use_mcLUT);
  if (!sc.isSuccess()) {
    ATH_MSG_ERROR("Failed to set MC flag to DataPreparator");
    return HLT::ERROR;
        }
  
  ////stationfit mc flag
  sc = m_stationFitter->setMCFlag(m_use_mcLUT);
  if (!sc.isSuccess()) {
    ATH_MSG_ERROR("Failed to set MC flag to StationFitter");
    return HLT::ERROR;
  }

  sc = m_trackFitter->setMCFlag(m_use_mcLUT);
  if (!sc.isSuccess()) {
    ATH_MSG_ERROR("Failed to set MC flag to TrackFitter");
    return HLT::ERROR;
>>>>>>> release/21.0.127
  }
  m_trackFitter -> setUseEIFromBarrel( m_use_endcapInnerFromBarrel );

<<<<<<< HEAD
  ATH_MSG_DEBUG("StatusCode MuFastSteering::findMuonSignature success");
  return StatusCode::SUCCESS;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode MuFastSteering::findMuonSignature(const std::vector<const TrigRoiDescriptor*>& roids,
                                             const std::vector<const xAOD::MuonRoI*>&   muonRoIs,
				             DataVector<xAOD::L2StandAloneMuon>& 	outputTracks,
					     TrigRoiDescriptorCollection& 		outputID,
					     TrigRoiDescriptorCollection&		outputMS// ,
					     // DataVector<xAOD::TrigComposite>&          	outputComposite
					     ) const
{
  ATH_MSG_DEBUG("StatusCode MuFastSteering::findMuonSignature start");
  StatusCode sc = StatusCode::SUCCESS;

  auto prepTimer           = Monitored::Timer( "TIME_Data_Preparator" );
  auto patternTimer        = Monitored::Timer( "TIME_Pattern_Finder" );
  auto stationFitterTimer  = Monitored::Timer( "TIME_Station_Fitter" );
  auto trackFitterTimer    = Monitored::Timer( "TIME_Track_Fitter" );
  auto trackExtraTimer     = Monitored::Timer( "TIME_Track_Extrapolator" );
  auto calibrationTimer    = Monitored::Timer( "TIME_Calibration_Streamer" );

  auto monitorIt	= Monitored::Group(m_monTool, prepTimer, patternTimer, stationFitterTimer,
                                                trackFitterTimer, trackExtraTimer, calibrationTimer );

  if (m_use_timer) {
    for (unsigned int i_timer=0; i_timer<m_timingTimers.size(); i_timer++) {
      m_timingTimers[i_timer]->start();
      m_timingTimers[i_timer]->pause();
    }
  }

  if (m_use_timer) m_timingTimers[ITIMER_TOTAL_PROCESSING]->resume();
  if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->resume();

  TrigL2MuonSA::RpcHits      rpcHits;
  TrigL2MuonSA::TgcHits      tgcHits;
  TrigL2MuonSA::MdtRegion    mdtRegion;
  TrigL2MuonSA::MuonRoad     muonRoad;
  TrigL2MuonSA::RpcFitResult rpcFitResult;
  TrigL2MuonSA::TgcFitResult tgcFitResult;
  TrigL2MuonSA::MdtHits      mdtHits_normal;
  TrigL2MuonSA::MdtHits      mdtHits_overlap;
  TrigL2MuonSA::CscHits      cscHits;
  TrigL2MuonSA::StgcHits     stgcHits;
  TrigL2MuonSA::MmHits       mmHits;

  DataVector<const TrigRoiDescriptor>::const_iterator p_roids;
  DataVector<const xAOD::MuonRoI>::const_iterator p_roi;

  // muonRoIs = RecMURoIs, roids = MURoIs
  p_roids = roids.begin();
  for (p_roi=(muonRoIs).begin(); p_roi!=(muonRoIs).end(); ++p_roi) {

    prepTimer.start();
    std::vector<TrigL2MuonSA::TrackPattern> trackPatterns;
    rpcHits.clear();
    tgcHits.clear();
    mdtRegion.Clear();
    muonRoad.Clear();
    rpcFitResult.Clear();
    tgcFitResult.Clear();
    mdtHits_normal.clear();
    mdtHits_overlap.clear();
    cscHits.clear();
    stgcHits.clear();
    mmHits.clear();

    if ( m_recMuonRoIUtils.isBarrel(*p_roi) ) { // Barrel
      ATH_MSG_DEBUG("Barrel");

      muonRoad.setScales(m_scaleRoadBarrelInner,
			 m_scaleRoadBarrelMiddle,
			 m_scaleRoadBarrelOuter);

      // Data preparation
      sc = m_dataPreparator->prepareData(*p_roi,
                                         *p_roids,
                                         m_insideOut,
                                         rpcHits,
                                         muonRoad,
                                         mdtRegion,
                                         rpcFitResult,
                                         mdtHits_normal,
                                         mdtHits_overlap);
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Data preparation failed");
	TrigL2MuonSA::TrackPattern trackPattern;
	trackPatterns.push_back(trackPattern);
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
      }
      if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->pause();
      prepTimer.stop();

      // Pattern finding
      if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->resume();
      patternTimer.start();
      sc = m_patternFinder->findPatterns(muonRoad,
                                         mdtHits_normal,
                                         trackPatterns);
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Pattern finder failed");
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
      }
      if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->pause();
      patternTimer.stop();

      // Superpoint fit
      if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->resume();
      stationFitterTimer.start();
      sc = m_stationFitter->findSuperPoints(*p_roids,
                                            muonRoad,
                                            rpcFitResult,
                                            trackPatterns);
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Super point fitter failed");
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
      }
      if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->pause();
      stationFitterTimer.stop();

      // Track fitting
      if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->resume();
      trackFitterTimer.start();
      sc = m_trackFitter->findTracks(*p_roids,
                                      rpcFitResult,
                                      trackPatterns);

      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Track fitter failed");
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
      }
      if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->pause();
      trackFitterTimer.stop();

    } else { // Endcap
      ATH_MSG_DEBUG("Endcap");

      prepTimer.start();
      // Data preparation
      sc = m_dataPreparator->prepareData(*p_roi,
                                         *p_roids,
                                         m_insideOut,
                                         tgcHits,
                                         muonRoad,
                                         mdtRegion,
                                         tgcFitResult,
                                         mdtHits_normal,
                                         mdtHits_overlap,
                                         cscHits,
					 stgcHits,
					 mmHits);
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Data preparation failed");
 	TrigL2MuonSA::TrackPattern trackPattern;
	trackPatterns.push_back(trackPattern);
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
      }
      if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->pause();
      prepTimer.stop();

      // Pattern finding
      if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->resume();
      patternTimer.start();
      sc = m_patternFinder->findPatterns(muonRoad,
                                         mdtHits_normal,
					 stgcHits,
					 mmHits,
                                         trackPatterns);



      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Pattern finder failed");
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
      }
      if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->pause();
      patternTimer.stop();

      // Superpoint fit
      if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->resume();
      stationFitterTimer.start();
      if(!m_use_new_segmentfit){
        sc = m_stationFitter->findSuperPointsSimple(*p_roids,
						    muonRoad,
						    tgcFitResult,
						    trackPatterns);
      }else{
        sc = m_stationFitter->findSuperPoints(*p_roids,
                                              muonRoad,
                                              tgcFitResult,
                                              trackPatterns);
      }
      /////csc SuperPoint
      m_cscsegmaker->FindSuperPointCsc(cscHits,trackPatterns,tgcFitResult,muonRoad);

      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Super point fitter failed");
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
      }

      if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->pause();
      stationFitterTimer.stop();

      // Track fittingh
      if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->resume();
      trackFitterTimer.start();
      sc = m_trackFitter->findTracks(*p_roids,
                                     tgcFitResult,
                                     trackPatterns,
                                     muonRoad);

      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Track fitter failed");
        // Update output trigger element
        updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
        	            rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			    stgcHits, mmHits,
                            trackPatterns, outputTracks, outputID, outputMS);
	continue;
        }
      if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->pause();
      trackFitterTimer.stop();
    }

    // fix if eta is strange
    const float ETA_LIMIT       = 2.8;
    const float DELTA_ETA_LIMIT = 1.0;
    const float ZERO_LIMIT = 1.e-5;
    for (TrigL2MuonSA::TrackPattern& track : trackPatterns) {
       float roiEta = (*p_roi)->eta();
       if (std::abs(track.pt) > ZERO_LIMIT
           && ( std::abs(track.etaMap) > ETA_LIMIT || std::abs(track.etaMap-roiEta) > DELTA_ETA_LIMIT ) ) {
          track.etaMap = roiEta;
       }
    }

    // Track extrapolation for ID combined
    if (m_use_timer) m_timingTimers[ITIMER_TRACK_EXTRAPOLATOR]->resume();
    trackExtraTimer.start();

    sc = m_trackExtrapolator->extrapolateTrack(trackPatterns, m_winPt);

    if (sc != StatusCode::SUCCESS) {
      ATH_MSG_WARNING("Track extrapolator failed");
      // Update output trigger element
      updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
      	                  rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			  stgcHits, mmHits,
                          trackPatterns, outputTracks, outputID, outputMS);
      continue;
    }
    if (m_use_timer) m_timingTimers[ITIMER_TRACK_EXTRAPOLATOR]->pause();
    trackExtraTimer.stop();

    // Update monitoring variables
    sc = updateMonitor(*p_roi, mdtHits_normal, trackPatterns );
    if (sc != StatusCode::SUCCESS) {
      ATH_MSG_WARNING("Failed to update monitoring variables");
      // Update output trigger element
      updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
       	                  rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			  stgcHits, mmHits,
                          trackPatterns, outputTracks, outputID, outputMS);
      continue;
    }

    // Update output trigger element
    updateOutputObjects(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
    	                rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
			stgcHits, mmHits,
                        trackPatterns, outputTracks, outputID, outputMS);

    p_roids++;
    if (p_roids==roids.end()) break;
  }

  if (m_use_timer) m_timingTimers[ITIMER_TOTAL_PROCESSING]->pause();

  int nRoI = muonRoIs.size();

  if (m_use_timer) {
     for (unsigned int i_timer=0; i_timer<m_timingTimers.size(); i_timer++) {
         m_timingTimers[i_timer]->propVal(nRoI);
         m_timingTimers[i_timer]->stop();
     }
  }

  ATH_MSG_DEBUG("StatusCode MuFastSteering::findMuonSignature success");
  return StatusCode::SUCCESS;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

// findMuonSignature of L2 inside-out version
// try to find MS tracks seeded by FTF tracks
StatusCode MuFastSteering::findMuonSignatureIO(const xAOD::TrackParticleContainer&         idtracks,
					       const std::vector<const TrigRoiDescriptor*> roids,
					       const std::vector<const LVL1::RecMuonRoI*>  muonRoIs,
					       DataVector<xAOD::L2CombinedMuon>&           outputCBs,
					       DataVector<xAOD::L2StandAloneMuon>&         outputSAs) const
{
  ATH_MSG_DEBUG("StatusCode MuFastSteering::findMuonSignatureIO start");
  StatusCode sc = StatusCode::SUCCESS;
  const float ZERO_LIMIT = 1.e-5;

  auto prepTimer           = Monitored::Timer( "TIME_Data_Preparator" );
  auto patternTimer        = Monitored::Timer( "TIME_Pattern_Finder" );
  auto stationFitterTimer  = Monitored::Timer( "TIME_Station_Fitter" );
  auto trackFitterTimer    = Monitored::Timer( "TIME_Track_Fitter" );
  auto trackExtraTimer     = Monitored::Timer( "TIME_Track_Extrapolator" );
  auto calibrationTimer    = Monitored::Timer( "TIME_Calibration_Streamer" );

  auto monitorIt       = Monitored::Group(m_monTool, prepTimer, patternTimer, stationFitterTimer,
					  trackFitterTimer, trackExtraTimer, calibrationTimer );

  if (m_use_timer){
    for (int i_timer = m_timingTimers.size()-1; i_timer>=0; --i_timer){
      m_timingTimers[i_timer]->start();
      if (i_timer != ITIMER_TOTAL_PROCESSING && i_timer != ITIMER_TOTAL_PROCESSING) m_timingTimers[i_timer]->pause();
    }
  }

  TrigL2MuonSA::RpcHits      rpcHits;
  TrigL2MuonSA::TgcHits      tgcHits;
  TrigL2MuonSA::MdtRegion    mdtRegion;
  TrigL2MuonSA::MuonRoad     muonRoad;
  TrigL2MuonSA::RpcFitResult rpcFitResult;
  TrigL2MuonSA::TgcFitResult tgcFitResult;
  TrigL2MuonSA::MdtHits      mdtHits_normal;
  TrigL2MuonSA::MdtHits      mdtHits_overlap;
  TrigL2MuonSA::CscHits      cscHits;
  TrigL2MuonSA::StgcHits     stgcHits;
  TrigL2MuonSA::MmHits       mmHits;

  DataVector<const TrigRoiDescriptor>::const_iterator p_roids;

  p_roids = roids.begin();
  for (const auto p_roi : muonRoIs) {
    ATH_MSG_DEBUG("roi eta/phi: " << (*p_roi).eta() << "/" << (*p_roi).phi());

    // idtracks loop
    if ( (idtracks).empty() ) ATH_MSG_DEBUG("IO TEST: xAOD::TrackParticleContainer has 0 tracks --> Can not use FTF tracks...");
    else  ATH_MSG_DEBUG("IO TEST: xAOD::TrackParticleContainer has " << (idtracks).size() << " tracks --> Start inside-out mode!");

    std::vector<TrigL2MuonSA::TrackPattern> trackPatterns;
    int idtrack_idx = -1;
    for (auto idtrack : idtracks) {

      idtrack_idx++;
      ATH_MSG_DEBUG("IO TEST: FTF track key:" << m_FTFtrackKey.key() << " pt = " << idtrack->pt()/1000 << " GeV");
      ATH_MSG_DEBUG("IO TEST: FTF track key:" << m_FTFtrackKey.key() << " eta/phi = " << idtrack->eta() << "/" << idtrack->phi());

      if(idtrack->pt() < m_ftfminPt) {
	ATH_MSG_DEBUG("IO TEST: skip FTF track due to pT threshold: " << m_ftfminPt << " MeV");
	continue;
      }

      prepTimer.start();
      rpcHits.clear();
      tgcHits.clear();
      mdtRegion.Clear();
      muonRoad.Clear();
      rpcFitResult.Clear();
      tgcFitResult.Clear();
      mdtHits_normal.clear();
      mdtHits_overlap.clear();
      cscHits.clear();
      stgcHits.clear();
      mmHits.clear();
      bool hasSP = false;

      sc = m_ftfRoadDefiner->defineRoad(idtrack, muonRoad);
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("FtfRoadDefiner failed");
	continue;
      } else {
	ATH_MSG_DEBUG("FtfRoadDefiner::defineRoad success");
      }

      if ( std::abs(idtrack->eta()) < 1.05 ){
	ATH_MSG_DEBUG("FTF track at IP is in  Barrel: " << idtrack->eta());
      } else {
	ATH_MSG_DEBUG("FTF track at IP is in  Endcap: " << idtrack->eta());
      }

      if ( std::abs(muonRoad.extFtfMiddleEta) < 1.05 ){ // Barrel Inside-out
	ATH_MSG_DEBUG("muonRoad.extFtfMiddleEta Barrel: " << muonRoad.extFtfMiddleEta);

	ATH_MSG_DEBUG("Barrel algorithm of IOmode starts");

	muonRoad.setScales(m_scaleRoadBarrelInner,
			   m_scaleRoadBarrelMiddle,
			   m_scaleRoadBarrelOuter);

	// Data preparation
	sc = m_dataPreparator->prepareData(p_roi,
					   *p_roids,
					   m_insideOut,
					   rpcHits,
					   muonRoad,
					   mdtRegion,
					   rpcFitResult,
					   mdtHits_normal,
					   mdtHits_overlap);
	if (!sc.isSuccess()) {
	  ATH_MSG_WARNING("Data preparation failed");
	  continue;
	} else {
	  ATH_MSG_DEBUG("Data preparation success");
	}
	if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->pause();
	prepTimer.stop();

	// Pattern finding
	if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->resume();
	patternTimer.start();
	sc = m_patternFinder->findPatterns(muonRoad,
					   mdtHits_normal,
					   trackPatterns);
	if (!sc.isSuccess()) {
	  ATH_MSG_WARNING("Pattern finder failed");
	  continue;
	}
	if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->pause();
	patternTimer.stop();

	// Superpoint fit
	if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->resume();
	stationFitterTimer.start();
	sc = m_stationFitter->findSuperPoints(*p_roids,
					      muonRoad,
					      rpcFitResult,
					      trackPatterns);
	if (!sc.isSuccess()) {
	  ATH_MSG_WARNING("Super point fitter failed");
	  continue;
	}
	if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->pause();
	stationFitterTimer.stop();

	// Check if at least 1 SP
	for (size_t i=0; i<std::extent<decltype(trackPatterns.back().superPoints), 0>::value; i++) {
	  if ( std::abs(trackPatterns.back().superPoints[i].R) > ZERO_LIMIT && std::abs(trackPatterns.back().superPoints[i].Z) > ZERO_LIMIT ) { // if R and Z exist
	    hasSP = true;
	  }
	}

	// Track fitting
	if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->resume();
	trackFitterTimer.start();
	sc = m_trackFitter->findTracks(*p_roids,
				       rpcFitResult,
				       trackPatterns);
	if (!sc.isSuccess()) {
	  ATH_MSG_WARNING("Track fitter failed");
	  if(hasSP) {
	    storeMuonSA(p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
	    		rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
	    		stgcHits, mmHits,
	    		trackPatterns.back(), outputSAs);
	    xAOD::L2CombinedMuon* muonCB = new xAOD::L2CombinedMuon();
	    muonCB->makePrivateStore();
	    muonCB->setStrategy(0);
	    muonCB->setErrorFlag(-9);
	    muonCB->setPt(idtrack->pt());
	    muonCB->setEta(idtrack->eta());
	    muonCB->setPhi(idtrack->phi());
	    muonCB->setCharge(idtrack->charge());
	    ElementLink<xAOD::L2StandAloneMuonContainer> muonSAEL(outputSAs, outputSAs.size()-1);
	    muonCB->setMuSATrackLink(muonSAEL);
	    ElementLink<xAOD::TrackParticleContainer> idtrkEL(idtracks, idtrack_idx);
	    muonCB->setIdTrackLink(idtrkEL);
	    outputCBs.push_back(muonCB);
	  }
	  continue;
        }
	if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->pause();
        trackFitterTimer.stop();

      } else { // Endcap Inside-out
	ATH_MSG_DEBUG("muonRoad.extFtfMiddleEta Endcap: " << muonRoad.extFtfMiddleEta);
	ATH_MSG_DEBUG("Endcap algorithm of IOmode starts");

	prepTimer.start();
	// Data preparation
	sc = m_dataPreparator->prepareData(p_roi,
					   *p_roids,
					   m_insideOut,
					   tgcHits,
					   muonRoad,
					   mdtRegion,
					   tgcFitResult,
					   mdtHits_normal,
					   mdtHits_overlap,
					   cscHits,
					   stgcHits,
					   mmHits);
	if (!sc.isSuccess()) {
	  ATH_MSG_WARNING("Data preparation failed");
	  continue;
	} else{
	  ATH_MSG_DEBUG("Data preparation success");
	}
	if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->pause();
	prepTimer.stop();

	// Pattern finding
	if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->resume();
	patternTimer.start();
	sc = m_patternFinder->findPatterns(muonRoad,
					   mdtHits_normal,
					   stgcHits,
					   mmHits,
					   trackPatterns);
	if (!sc.isSuccess()) {
	  ATH_MSG_WARNING("Pattern finder failed");
	  continue;
	}
	if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->pause();
	patternTimer.stop();

	// Superpoint fit
	if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->resume();
	stationFitterTimer.start();
	sc = m_stationFitter->findSuperPointsSimple(*p_roids,
						    muonRoad,
						    tgcFitResult,
						    trackPatterns);
	/////csc SuperPoint
	m_cscsegmaker->FindSuperPointCsc(cscHits,trackPatterns,tgcFitResult,muonRoad);
	if (!sc.isSuccess()) {
	  ATH_MSG_WARNING("Super point fitter failed");
	  continue;
	}
	if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->pause();
	stationFitterTimer.stop();

	// Check if at least 1 SP
	for (size_t i=0; i<std::extent<decltype(trackPatterns.back().superPoints), 0>::value; i++) {
	  if ( std::abs(trackPatterns.back().superPoints[i].R) > ZERO_LIMIT && std::abs(trackPatterns.back().superPoints[i].Z) > ZERO_LIMIT ) { // if R and Z exist
	    hasSP = true;
	  }
	}

	// Track fittingh
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->resume();
	trackFitterTimer.start();
        sc = m_trackFitter->findTracks(*p_roids,
				       tgcFitResult,
				       trackPatterns,
				       muonRoad);
        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Track fitter failed");
	  if(hasSP) {
	    storeMuonSA(p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
	    		rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
	    		stgcHits, mmHits,
	    		trackPatterns.back(), outputSAs);
	    xAOD::L2CombinedMuon* muonCB = new xAOD::L2CombinedMuon();
	    muonCB->makePrivateStore();
	    muonCB->setStrategy(0);
	    muonCB->setErrorFlag(-9);
	    muonCB->setPt(idtrack->pt());
	    muonCB->setEta(idtrack->eta());
	    muonCB->setPhi(idtrack->phi());
	    muonCB->setCharge(idtrack->charge());
	    ElementLink<xAOD::L2StandAloneMuonContainer> muonSAEL(outputSAs, outputSAs.size()-1);
	    muonCB->setMuSATrackLink(muonSAEL);
	    ElementLink<xAOD::TrackParticleContainer> idtrkEL(idtracks, idtrack_idx);
	    muonCB->setIdTrackLink(idtrkEL);
	    outputCBs.push_back(muonCB);
	  }
	  continue;
	}
	if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->pause();
	trackFitterTimer.stop();

      }

      // fix if eta is strange
      for (unsigned int i=0 ;i<trackPatterns.size(); i++) {
	TrigL2MuonSA::TrackPattern track = trackPatterns[i];
	const float ETA_LIMIT       = 2.8;
	const float DELTA_ETA_LIMIT = 1.0;
	float roiEta = (*p_roi).eta();
	if (std::abs(track.pt) > ZERO_LIMIT
	    && ( std::abs(track.etaMap) > ETA_LIMIT || std::abs(track.etaMap-roiEta) > DELTA_ETA_LIMIT ) ) {
          trackPatterns[i].etaMap = roiEta;
        }
      }

      // Update monitoring variables
      sc = updateMonitor(p_roi, mdtHits_normal, trackPatterns );
      if (sc != StatusCode::SUCCESS) {
	ATH_MSG_WARNING("Failed to update monitoring variables");
      }

      // Update output trigger element
      if(hasSP) {
	storeMuonSA(p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
		    rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
		    stgcHits, mmHits,
		    trackPatterns.back(), outputSAs);
	xAOD::L2CombinedMuon* muonCB = new xAOD::L2CombinedMuon();
	muonCB->makePrivateStore();
	muonCB->setStrategy(0);
	muonCB->setErrorFlag(-9);
	muonCB->setPt(idtrack->pt());
	muonCB->setEta(idtrack->eta());
	muonCB->setPhi(idtrack->phi());
	muonCB->setCharge(idtrack->charge());
	ElementLink<xAOD::L2StandAloneMuonContainer> muonSAEL(outputSAs, outputSAs.size()-1);
	muonCB->setMuSATrackLink(muonSAEL);
	ElementLink<xAOD::TrackParticleContainer> idtrkEL(idtracks, idtrack_idx);
	muonCB->setIdTrackLink(idtrkEL);
	outputCBs.push_back(muonCB);
      }

    }

    if(outputSAs.size()==0) {
      ATH_MSG_DEBUG("outputSAs size = 0 -> push_back dummy");
      muonRoad.Clear();
      mdtRegion.Clear();
      rpcHits.clear();
      tgcHits.clear();
      rpcFitResult.Clear();
      tgcFitResult.Clear();
      mdtHits_normal.clear();
      cscHits.clear();
      stgcHits.clear();
      mmHits.clear();
      trackPatterns.clear();
      TrigL2MuonSA::TrackPattern trackPattern;
      storeMuonSA(p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
      		  rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
      		  stgcHits, mmHits,
      		  trackPattern, outputSAs);
      xAOD::L2CombinedMuon* muonCB = new xAOD::L2CombinedMuon();
      muonCB->makePrivateStore();
      muonCB->setStrategy(-9);
      muonCB->setErrorFlag(-9);
      muonCB->setPt(0);
      muonCB->setEta(0);
      muonCB->setPhi(0);
      ElementLink<xAOD::L2StandAloneMuonContainer> muonSAEL(outputSAs, outputSAs.size()-1);
      muonCB->setMuSATrackLink(muonSAEL);
      outputCBs.push_back(muonCB);
    }


    ATH_MSG_DEBUG("outputSAs size: " << outputSAs.size());
    ATH_MSG_DEBUG("idtracks size: " << idtracks.size());
    for (auto outputSA : outputSAs){
      ATH_MSG_DEBUG("outputSA pt/eta/phi: " << outputSA->pt() << "/" << outputSA->etaMS() << "/" << outputSA->phiMS());
    }

    ATH_MSG_DEBUG("outputCBs size: " << outputCBs.size());
    for (auto outputCB : outputCBs){
      ATH_MSG_DEBUG("outputCB pt/eta/phi: " << outputCB->pt() << "/" << outputCB->eta() << "/" << outputCB->phi());
    }

    p_roids++;
    if (p_roids==roids.end()) break;
  }

  if (m_use_timer) m_timingTimers[ITIMER_TOTAL_PROCESSING]->pause();

  if (m_use_timer) {
    for (unsigned int i_timer=0; i_timer<m_timingTimers.size(); i_timer++) {
      m_timingTimers[i_timer]->stop();
    }
  }

  ATH_MSG_DEBUG("StatusCode MuFastSteering::findMuonSignatureIO success");
  return StatusCode::SUCCESS;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

// findMuonSignature of L2 inside-out version
// try to find MS tracks seeded by FTF tracks
StatusCode MuFastSteering::findMuonSignatureIO(const xAOD::TrackParticleContainer&         idtracks,
					       const std::vector<const TrigRoiDescriptor*> roids,
					       const std::vector<const xAOD::MuonRoI*>     muonRoIs,
					       DataVector<xAOD::L2CombinedMuon>&           outputCBs,
					       DataVector<xAOD::L2StandAloneMuon>&         outputSAs) const
{
  ATH_MSG_DEBUG("StatusCode MuFastSteering::findMuonSignatureIO start");
  StatusCode sc = StatusCode::SUCCESS;
  const float ZERO_LIMIT = 1.e-5;

  auto prepTimer           = Monitored::Timer( "TIME_Data_Preparator" );
  auto patternTimer        = Monitored::Timer( "TIME_Pattern_Finder" );
  auto stationFitterTimer  = Monitored::Timer( "TIME_Station_Fitter" );
  auto trackFitterTimer    = Monitored::Timer( "TIME_Track_Fitter" );
  auto trackExtraTimer     = Monitored::Timer( "TIME_Track_Extrapolator" );
  auto calibrationTimer    = Monitored::Timer( "TIME_Calibration_Streamer" );

  auto monitorIt       = Monitored::Group(m_monTool, prepTimer, patternTimer, stationFitterTimer,
					  trackFitterTimer, trackExtraTimer, calibrationTimer );

  if (m_use_timer){
    for (int i_timer = m_timingTimers.size()-1; i_timer>=0; --i_timer){
      m_timingTimers[i_timer]->start();
      if (i_timer != ITIMER_TOTAL_PROCESSING && i_timer != ITIMER_TOTAL_PROCESSING) m_timingTimers[i_timer]->pause();
    }
  }

  TrigL2MuonSA::RpcHits      rpcHits;
  TrigL2MuonSA::TgcHits      tgcHits;
  TrigL2MuonSA::MdtRegion    mdtRegion;
  TrigL2MuonSA::MuonRoad     muonRoad;
  TrigL2MuonSA::RpcFitResult rpcFitResult;
  TrigL2MuonSA::TgcFitResult tgcFitResult;
  TrigL2MuonSA::MdtHits      mdtHits_normal;
  TrigL2MuonSA::MdtHits      mdtHits_overlap;
  TrigL2MuonSA::CscHits      cscHits;
  TrigL2MuonSA::StgcHits     stgcHits;
  TrigL2MuonSA::MmHits       mmHits;

  DataVector<const TrigRoiDescriptor>::const_iterator p_roids;

  p_roids = roids.begin();
  for (const auto p_roi : muonRoIs) {
    ATH_MSG_DEBUG("roi eta/phi: " << (*p_roi).eta() << "/" << (*p_roi).phi());

    // idtracks loop
    if ( (idtracks).empty() ) ATH_MSG_DEBUG("IO TEST: xAOD::TrackParticleContainer has 0 tracks --> Can not use FTF tracks...");
    else  ATH_MSG_DEBUG("IO TEST: xAOD::TrackParticleContainer has " << (idtracks).size() << " tracks --> Start inside-out mode!");

    std::vector<TrigL2MuonSA::TrackPattern> trackPatterns;
    int idtrack_idx = -1;
    for (auto idtrack : idtracks) {

      idtrack_idx++;
      ATH_MSG_DEBUG("IO TEST: FTF track key:" << m_FTFtrackKey.key() << " pt = " << idtrack->pt()/1000 << " GeV");
      ATH_MSG_DEBUG("IO TEST: FTF track key:" << m_FTFtrackKey.key() << " eta/phi = " << idtrack->eta() << "/" << idtrack->phi());

      if(idtrack->pt() < m_ftfminPt) {
	ATH_MSG_DEBUG("IO TEST: skip FTF track due to pT threshold: " << m_ftfminPt << " MeV");
	continue;
      }

      prepTimer.start();
      rpcHits.clear();
      tgcHits.clear();
      mdtRegion.Clear();
      muonRoad.Clear();
      rpcFitResult.Clear();
      tgcFitResult.Clear();
      mdtHits_normal.clear();
      mdtHits_overlap.clear();
      cscHits.clear();
      stgcHits.clear();
      mmHits.clear();
      bool hasSP = false;

      sc = m_ftfRoadDefiner->defineRoad(idtrack, muonRoad);
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("FtfRoadDefiner failed");
	continue;
      } else {
	ATH_MSG_DEBUG("FtfRoadDefiner::defineRoad success");
      }

      if ( std::abs(idtrack->eta()) < 1.05 ){
	ATH_MSG_DEBUG("FTF track at IP is in  Barrel: " << idtrack->eta());
      } else {
	ATH_MSG_DEBUG("FTF track at IP is in  Endcap: " << idtrack->eta());
      }

      if ( std::abs(muonRoad.extFtfMiddleEta) < 1.05 ){ // Barrel Inside-out
	ATH_MSG_DEBUG("muonRoad.extFtfMiddleEta Barrel: " << muonRoad.extFtfMiddleEta);

	ATH_MSG_DEBUG("Barrel algorithm of IOmode starts");

	muonRoad.setScales(m_scaleRoadBarrelInner,
			   m_scaleRoadBarrelMiddle,
			   m_scaleRoadBarrelOuter);

	// Data preparation
	sc = m_dataPreparator->prepareData(p_roi,
					   *p_roids,
					   m_insideOut,
					   rpcHits,
					   muonRoad,
					   mdtRegion,
					   rpcFitResult,
					   mdtHits_normal,
					   mdtHits_overlap);
	if (!sc.isSuccess()) {
	  ATH_MSG_WARNING("Data preparation failed");
	  continue;
	} else {
	  ATH_MSG_DEBUG("Data preparation success");
	}
	if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->pause();
	prepTimer.stop();

	// Pattern finding
	if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->resume();
	patternTimer.start();
	sc = m_patternFinder->findPatterns(muonRoad,
					   mdtHits_normal,
					   trackPatterns);
	if (!sc.isSuccess()) {
	  ATH_MSG_WARNING("Pattern finder failed");
	  continue;
	}
	if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->pause();
	patternTimer.stop();

	// Superpoint fit
	if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->resume();
	stationFitterTimer.start();
	sc = m_stationFitter->findSuperPoints(*p_roids,
					      muonRoad,
					      rpcFitResult,
					      trackPatterns);
	if (!sc.isSuccess()) {
	  ATH_MSG_WARNING("Super point fitter failed");
	  continue;
	}
	if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->pause();
	stationFitterTimer.stop();

	// Check if at least 1 SP
	for (size_t i=0; i<std::extent<decltype(trackPatterns.back().superPoints), 0>::value; i++) {
	  if ( std::abs(trackPatterns.back().superPoints[i].R) > ZERO_LIMIT && std::abs(trackPatterns.back().superPoints[i].Z) > ZERO_LIMIT ) { // if R and Z exist
	    hasSP = true;
	  }
	}

	// Track fitting
	if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->resume();
	trackFitterTimer.start();
	sc = m_trackFitter->findTracks(*p_roids,
				       rpcFitResult,
				       trackPatterns);
	if (!sc.isSuccess()) {
	  ATH_MSG_WARNING("Track fitter failed");
	  if(hasSP) {
	    storeMuonSA(p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
	    		rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
	    		stgcHits, mmHits,
	    		trackPatterns.back(), outputSAs);
	    xAOD::L2CombinedMuon* muonCB = new xAOD::L2CombinedMuon();
	    muonCB->makePrivateStore();
	    muonCB->setStrategy(0);
	    muonCB->setErrorFlag(-9);
	    muonCB->setPt(idtrack->pt());
	    muonCB->setEta(idtrack->eta());
	    muonCB->setPhi(idtrack->phi());
	    muonCB->setCharge(idtrack->charge());
	    ElementLink<xAOD::L2StandAloneMuonContainer> muonSAEL(outputSAs, outputSAs.size()-1);
	    muonCB->setMuSATrackLink(muonSAEL);
	    ElementLink<xAOD::TrackParticleContainer> idtrkEL(idtracks, idtrack_idx);
	    muonCB->setIdTrackLink(idtrkEL);
	    outputCBs.push_back(muonCB);
	  }
	  continue;
        }
	if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->pause();
        trackFitterTimer.stop();

      } else { // Endcap Inside-out
	ATH_MSG_DEBUG("muonRoad.extFtfMiddleEta Endcap: " << muonRoad.extFtfMiddleEta);
	ATH_MSG_DEBUG("Endcap algorithm of IOmode starts");

	prepTimer.start();
	// Data preparation
	sc = m_dataPreparator->prepareData(p_roi,
					   *p_roids,
					   m_insideOut,
					   tgcHits,
					   muonRoad,
					   mdtRegion,
					   tgcFitResult,
					   mdtHits_normal,
					   mdtHits_overlap,
					   cscHits,
					   stgcHits,
					   mmHits);
	if (!sc.isSuccess()) {
	  ATH_MSG_WARNING("Data preparation failed");
	  continue;
	} else{
	  ATH_MSG_DEBUG("Data preparation success");
	}
	if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->pause();
	prepTimer.stop();

	// Pattern finding
	if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->resume();
	patternTimer.start();
	sc = m_patternFinder->findPatterns(muonRoad,
					   mdtHits_normal,
					   stgcHits,
					   mmHits,
					   trackPatterns);
	if (!sc.isSuccess()) {
	  ATH_MSG_WARNING("Pattern finder failed");
	  continue;
	}
	if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->pause();
	patternTimer.stop();

	// Superpoint fit
	if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->resume();
	stationFitterTimer.start();
	sc = m_stationFitter->findSuperPointsSimple(*p_roids,
						    muonRoad,
						    tgcFitResult,
						    trackPatterns);
	/////csc SuperPoint
	m_cscsegmaker->FindSuperPointCsc(cscHits,trackPatterns,tgcFitResult,muonRoad);
	if (!sc.isSuccess()) {
	  ATH_MSG_WARNING("Super point fitter failed");
	  continue;
	}
	if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->pause();
	stationFitterTimer.stop();

	// Check if at least 1 SP
	for (size_t i=0; i<std::extent<decltype(trackPatterns.back().superPoints), 0>::value; i++) {
	  if ( std::abs(trackPatterns.back().superPoints[i].R) > ZERO_LIMIT && std::abs(trackPatterns.back().superPoints[i].Z) > ZERO_LIMIT ) { // if R and Z exist
	    hasSP = true;
	  }
	}

	// Track fittingh
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->resume();
	trackFitterTimer.start();
        sc = m_trackFitter->findTracks(*p_roids,
				       tgcFitResult,
				       trackPatterns,
				       muonRoad);
        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Track fitter failed");
	  if(hasSP) {
	    storeMuonSA(p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
	    		rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
	    		stgcHits, mmHits,
	    		trackPatterns.back(), outputSAs);
	    xAOD::L2CombinedMuon* muonCB = new xAOD::L2CombinedMuon();
	    muonCB->makePrivateStore();
	    muonCB->setStrategy(0);
	    muonCB->setErrorFlag(-9);
	    muonCB->setPt(idtrack->pt());
	    muonCB->setEta(idtrack->eta());
	    muonCB->setPhi(idtrack->phi());
	    muonCB->setCharge(idtrack->charge());
	    ElementLink<xAOD::L2StandAloneMuonContainer> muonSAEL(outputSAs, outputSAs.size()-1);
	    muonCB->setMuSATrackLink(muonSAEL);
	    ElementLink<xAOD::TrackParticleContainer> idtrkEL(idtracks, idtrack_idx);
	    muonCB->setIdTrackLink(idtrkEL);
	    outputCBs.push_back(muonCB);
	  }
	  continue;
	}
	if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->pause();
	trackFitterTimer.stop();

      }

      // fix if eta is strange
      for (unsigned int i=0 ;i<trackPatterns.size(); i++) {
	TrigL2MuonSA::TrackPattern track = trackPatterns[i];
	const float ETA_LIMIT       = 2.8;
	const float DELTA_ETA_LIMIT = 1.0;
	float roiEta = (*p_roi).eta();
	if (std::abs(track.pt) > ZERO_LIMIT
	    && ( std::abs(track.etaMap) > ETA_LIMIT || std::abs(track.etaMap-roiEta) > DELTA_ETA_LIMIT ) ) {
          trackPatterns[i].etaMap = roiEta;
        }
      }

      // Update monitoring variables
      sc = updateMonitor(p_roi, mdtHits_normal, trackPatterns );
      if (sc != StatusCode::SUCCESS) {
	ATH_MSG_WARNING("Failed to update monitoring variables");
      }

      // Update output trigger element
      if(hasSP) {
	storeMuonSA(p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
		    rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
		    stgcHits, mmHits,
		    trackPatterns.back(), outputSAs);
	xAOD::L2CombinedMuon* muonCB = new xAOD::L2CombinedMuon();
	muonCB->makePrivateStore();
	muonCB->setStrategy(0);
	muonCB->setErrorFlag(-9);
	muonCB->setPt(idtrack->pt());
	muonCB->setEta(idtrack->eta());
	muonCB->setPhi(idtrack->phi());
	muonCB->setCharge(idtrack->charge());
	ElementLink<xAOD::L2StandAloneMuonContainer> muonSAEL(outputSAs, outputSAs.size()-1);
	muonCB->setMuSATrackLink(muonSAEL);
	ElementLink<xAOD::TrackParticleContainer> idtrkEL(idtracks, idtrack_idx);
	muonCB->setIdTrackLink(idtrkEL);
	outputCBs.push_back(muonCB);
      }

    }

    if(outputSAs.size()==0) {
      ATH_MSG_DEBUG("outputSAs size = 0 -> push_back dummy");
      muonRoad.Clear();
      mdtRegion.Clear();
      rpcHits.clear();
      tgcHits.clear();
      rpcFitResult.Clear();
      tgcFitResult.Clear();
      mdtHits_normal.clear();
      cscHits.clear();
      stgcHits.clear();
      mmHits.clear();
      trackPatterns.clear();
      TrigL2MuonSA::TrackPattern trackPattern;
      storeMuonSA(p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
      		  rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
      		  stgcHits, mmHits,
      		  trackPattern, outputSAs);
      xAOD::L2CombinedMuon* muonCB = new xAOD::L2CombinedMuon();
      muonCB->makePrivateStore();
      muonCB->setStrategy(-9);
      muonCB->setErrorFlag(-9);
      muonCB->setPt(0);
      muonCB->setEta(0);
      muonCB->setPhi(0);
      ElementLink<xAOD::L2StandAloneMuonContainer> muonSAEL(outputSAs, outputSAs.size()-1);
      muonCB->setMuSATrackLink(muonSAEL);
      outputCBs.push_back(muonCB);
    }


    ATH_MSG_DEBUG("outputSAs size: " << outputSAs.size());
    ATH_MSG_DEBUG("idtracks size: " << idtracks.size());
    for (auto outputSA : outputSAs){
      ATH_MSG_DEBUG("outputSA pt/eta/phi: " << outputSA->pt() << "/" << outputSA->etaMS() << "/" << outputSA->phiMS());
    }

    ATH_MSG_DEBUG("outputCBs size: " << outputCBs.size());
    for (auto outputCB : outputCBs){
      ATH_MSG_DEBUG("outputCB pt/eta/phi: " << outputCB->pt() << "/" << outputCB->eta() << "/" << outputCB->phi());
    }

    p_roids++;
    if (p_roids==roids.end()) break;
  }

  if (m_use_timer) m_timingTimers[ITIMER_TOTAL_PROCESSING]->pause();

  if (m_use_timer) {
    for (unsigned int i_timer=0; i_timer<m_timingTimers.size(); i_timer++) {
      m_timingTimers[i_timer]->stop();
    }
  }

  ATH_MSG_DEBUG("StatusCode MuFastSteering::findMuonSignatureIO success");
  return StatusCode::SUCCESS;
}

// findMuonSignature of L2 multi-track SA version
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode MuFastSteering::findMultiTrackSignature(const std::vector<const TrigRoiDescriptor*>& roids,
                                                  const std::vector<const LVL1::RecMuonRoI*>&  muonRoIs,
                                                  DataVector<xAOD::L2StandAloneMuon>&          outputTracks) const
{
  ATH_MSG_DEBUG("StatusCode MuFastSteering::findMultiTrackSignature start");
  StatusCode sc = StatusCode::SUCCESS;
  const float ZERO_LIMIT = 1.e-5;

  // for RPC clustering and clusterRoad
  std::vector<TrigL2MuonSA::RpcFitResult>   clusterFitResults;
  std::vector< TrigL2MuonSA::MuonRoad >     clusterRoad;
  std::vector<TrigL2MuonSA::MdtHits>        mdtHits_cluster_normal;


  auto prepTimer           = Monitored::Timer( "TIME_Data_Preparator" );
  auto patternTimer        = Monitored::Timer( "TIME_Pattern_Finder" );
  auto stationFitterTimer  = Monitored::Timer( "TIME_Station_Fitter" );
  auto trackFitterTimer    = Monitored::Timer( "TIME_Track_Fitter" );
  auto trackExtraTimer     = Monitored::Timer( "TIME_Track_Extrapolator" );
  auto calibrationTimer    = Monitored::Timer( "TIME_Calibration_Streamer" );

  auto monitorIt	= Monitored::Group(m_monTool, prepTimer, patternTimer, stationFitterTimer,
                                                trackFitterTimer, trackExtraTimer, calibrationTimer );

  if (m_use_timer) {
    for (unsigned int i_timer=0; i_timer<m_timingTimers.size(); i_timer++) {
      m_timingTimers[i_timer]->start();
      m_timingTimers[i_timer]->pause();
    }
  }

  if (m_use_timer) m_timingTimers[ITIMER_TOTAL_PROCESSING]->resume();
  if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->resume();

  TrigL2MuonSA::RpcHits      rpcHits;
  TrigL2MuonSA::TgcHits      tgcHits;
  TrigL2MuonSA::MdtRegion    mdtRegion;
  TrigL2MuonSA::MuonRoad     muonRoad;
  TrigL2MuonSA::RpcFitResult rpcFitResult;
  TrigL2MuonSA::TgcFitResult tgcFitResult;
  TrigL2MuonSA::MdtHits      mdtHits_normal;
  TrigL2MuonSA::MdtHits      mdtHits_overlap;
  TrigL2MuonSA::CscHits      cscHits;
  TrigL2MuonSA::StgcHits     stgcHits;
  TrigL2MuonSA::MmHits       mmHits;

  DataVector<const TrigRoiDescriptor>::const_iterator p_roids;
  DataVector<const LVL1::RecMuonRoI>::const_iterator p_roi;

  // muonRoIs = RecMURoIs, roids = MURoIs
  p_roids = roids.begin();
  for (p_roi=(muonRoIs).begin(); p_roi!=(muonRoIs).end(); ++p_roi) {

    prepTimer.start();
    std::vector<TrigL2MuonSA::TrackPattern> trackPatterns;
    rpcHits.clear();
    tgcHits.clear();
    mdtRegion.Clear();
    muonRoad.Clear();
    rpcFitResult.Clear();
    tgcFitResult.Clear();
    mdtHits_normal.clear();
    mdtHits_overlap.clear();
    cscHits.clear();
    stgcHits.clear();
    mmHits.clear();

    clusterFitResults.clear();
    clusterRoad.clear();
    mdtHits_cluster_normal.clear();

    if ( m_recMuonRoIUtils.isBarrel(*p_roi) ) { // Barrel
      ATH_MSG_DEBUG("Barrel");

      muonRoad.setScales(m_scaleRoadBarrelInner,
			 m_scaleRoadBarrelMiddle,
			 m_scaleRoadBarrelOuter);

      // Data preparation
      sc = m_dataPreparator->prepareData(*p_roi,
                                         *p_roids,
                                         clusterRoad,
                                         clusterFitResults,
                                         mdtHits_normal,
                                         mdtHits_overlap,
                                         mdtHits_cluster_normal);

      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Data preparation failed");
	continue;
      }
      ATH_MSG_DEBUG("clusterRoad size = " << clusterRoad.size());

      if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->pause();
      prepTimer.stop();

      for(int i_road = 0; i_road < (int)clusterRoad.size(); i_road++){
        // Pattern finding
        std::vector<TrigL2MuonSA::TrackPattern> tmp_trkPats; tmp_trkPats.clear();

        if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->resume();
        patternTimer.start();
        sc = m_patternFinder->findPatterns(clusterRoad.at(i_road),
                                           mdtHits_cluster_normal.at(i_road),
                                           tmp_trkPats);
        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Pattern finder failed");
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->pause();
        patternTimer.stop();

        // Superpoint fit
        if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->resume();
        stationFitterTimer.start();
        sc = m_stationFitter->findSuperPoints(*p_roids,
                                              clusterRoad.at(i_road),
                                              clusterFitResults.at(i_road),
                                              tmp_trkPats);
        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Super point fitter failed");
          // Update output trigger element
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->pause();
        stationFitterTimer.stop();

        // Track fitting
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->resume();
        trackFitterTimer.start();
        sc = m_trackFitter->findTracks(*p_roids,
                                       clusterFitResults.at(i_road),
                                       tmp_trkPats);

        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Track fitter failed");
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->pause();
        trackFitterTimer.stop();

        // fix if eta is strange
        const float ETA_LIMIT       = 2.8;
        const float DELTA_ETA_LIMIT = 1.0;
        for (TrigL2MuonSA::TrackPattern& track : tmp_trkPats) {
          float roiEta = (*p_roi)->eta();
          if (std::abs(track.pt) > ZERO_LIMIT
              && ( std::abs(track.etaMap) > ETA_LIMIT || std::abs(track.etaMap-roiEta) > DELTA_ETA_LIMIT ) ) {
            track.etaMap = roiEta;
          }
        }

        // Track extrapolation for ID combined
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_EXTRAPOLATOR]->resume();
        trackExtraTimer.start();

        sc = m_trackExtrapolator->extrapolateTrack(tmp_trkPats, m_winPt);
        ATH_MSG_DEBUG("test trackExtrapolator end");

        if (sc != StatusCode::SUCCESS) {
          ATH_MSG_WARNING("Track extrapolator failed");
          // Update output trigger element
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_EXTRAPOLATOR]->pause();
        trackExtraTimer.stop();

        if(tmp_trkPats.size() > 0){
          ATH_MSG_DEBUG("temp pT calculated 2mu-in-1RoI alg = " << tmp_trkPats[0].pt << " GeV");
          if( (std::abs(tmp_trkPats[0].barrelSagitta) < ZERO_LIMIT &&
               std::abs(tmp_trkPats[0].barrelRadius) < ZERO_LIMIT) ||
               std::abs(tmp_trkPats[0].pt) < ZERO_LIMIT )
            continue;
          trackPatterns.push_back(tmp_trkPats[0]);
        }

	storeMuonSA(*p_roi, *p_roids, clusterRoad.at(i_road), mdtRegion, rpcHits, tgcHits,
		    clusterFitResults.at(i_road), tgcFitResult, mdtHits_cluster_normal.at(i_road), cscHits,
		    stgcHits, mmHits, trackPatterns.back(), outputTracks);

      } // end the clusterRoad loop
      if(trackPatterns.empty()){
	ATH_MSG_DEBUG("multi-track SA falied to reconstruct muons");
 	TrigL2MuonSA::TrackPattern trackPattern;
	trackPatterns.push_back(trackPattern);
	storeMuonSA(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
		    rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
		    stgcHits, mmHits, trackPatterns.back(), outputTracks);

	continue;
      }
    } else { // Endcap
      ATH_MSG_DEBUG("Endcap");
      if(!m_doEndcapForl2mt){
        ATH_MSG_DEBUG("multi-track SA does nothings and skips for EndcapRoI");
      } else {
        prepTimer.start();
        // Data preparation
        sc = m_dataPreparator->prepareData(*p_roi,
                                           *p_roids,
                                           m_insideOut,
                                           tgcHits,
                                           muonRoad,
                                           mdtRegion,
                                           tgcFitResult,
                                           mdtHits_normal,
                                           mdtHits_overlap,
                                           cscHits,
                                           stgcHits,
                                           mmHits);
        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Data preparation failed");
          TrigL2MuonSA::TrackPattern trackPattern;
          trackPatterns.push_back(trackPattern);
          // Update output trigger element
          storeMuonSA(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
                      rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
                      stgcHits, mmHits, trackPatterns.back(), outputTracks);
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->pause();
        prepTimer.stop();

        // Pattern finding
        if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->resume();
        patternTimer.start();
        sc = m_patternFinder->findPatterns(muonRoad,
                                           mdtHits_normal,
                                           stgcHits,
                                           mmHits,
                                           trackPatterns);



        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Pattern finder failed");
          // Update output trigger element
          storeMuonSA(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
		      rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
                      stgcHits, mmHits, trackPatterns.back(), outputTracks);
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->pause();
        patternTimer.stop();

        // Superpoint fit
        if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->resume();
        stationFitterTimer.start();
        if(!m_use_new_segmentfit){
          sc = m_stationFitter->findSuperPointsSimple(*p_roids,
	       					      muonRoad,
                                                      tgcFitResult,
                                                      trackPatterns);
        }else{
          sc = m_stationFitter->findSuperPoints(*p_roids,
                                                muonRoad,
                                                tgcFitResult,
                                                trackPatterns);
        }
        /////csc SuperPoint
        m_cscsegmaker->FindSuperPointCsc(cscHits,trackPatterns,tgcFitResult,muonRoad);

        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Super point fitter failed");
          storeMuonSA(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
		      rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
                      stgcHits, mmHits, trackPatterns.back(), outputTracks);
          continue;
        }

        if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->pause();
        stationFitterTimer.stop();

        // Track fittingh
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->resume();
        trackFitterTimer.start();
        sc = m_trackFitter->findTracks(*p_roids,
                                       tgcFitResult,
                                       trackPatterns,
                                       muonRoad);

        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Track fitter failed");
          storeMuonSA(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
		      rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
                      stgcHits, mmHits, trackPatterns.back(), outputTracks);
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->pause();
        trackFitterTimer.stop();

        // fix if eta is strange
        const float ETA_LIMIT       = 2.8;
        const float DELTA_ETA_LIMIT = 1.0;
        for (TrigL2MuonSA::TrackPattern& track : trackPatterns) {
          float roiEta = (*p_roi)->eta();
          if (std::abs(track.pt) > ZERO_LIMIT
              && ( std::abs(track.etaMap) > ETA_LIMIT || std::abs(track.etaMap-roiEta) > DELTA_ETA_LIMIT ) ) {
            track.etaMap = roiEta;
          }
        }

        // Track extrapolation for ID combined
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_EXTRAPOLATOR]->resume();
        trackExtraTimer.start();

        sc = m_trackExtrapolator->extrapolateTrack(trackPatterns, m_winPt);

        if (sc != StatusCode::SUCCESS) {
          ATH_MSG_WARNING("Track extrapolator failed");
          storeMuonSA(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
                      rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
                      stgcHits, mmHits, trackPatterns.back(), outputTracks);
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_EXTRAPOLATOR]->pause();
        trackExtraTimer.stop();

	storeMuonSA(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
		    rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
		    stgcHits, mmHits, trackPatterns.back(), outputTracks);
      }
    }
    // Update monitoring variables
    sc = updateMonitor(*p_roi, mdtHits_normal, trackPatterns );
    if (sc != StatusCode::SUCCESS) {
      ATH_MSG_WARNING("Failed to update monitoring variables");
    }

    p_roids++;
    if (p_roids==roids.end()) break;
  }

  if (m_use_timer) m_timingTimers[ITIMER_TOTAL_PROCESSING]->pause();

  int nRoI = muonRoIs.size();

  if (m_use_timer) {
     for (unsigned int i_timer=0; i_timer<m_timingTimers.size(); i_timer++) {
         m_timingTimers[i_timer]->propVal(nRoI);
         m_timingTimers[i_timer]->stop();
     }
  }

  ATH_MSG_DEBUG("StatusCode MuFastSteering::findMultiTrackSignature success");
  return StatusCode::SUCCESS;
}

// findMuonSignature of L2 multi-track SA version
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode MuFastSteering::findMultiTrackSignature(const std::vector<const TrigRoiDescriptor*>& roids,
                                                  const std::vector<const xAOD::MuonRoI*>&     muonRoIs,
                                                  DataVector<xAOD::L2StandAloneMuon>&          outputTracks) const
{
  ATH_MSG_DEBUG("StatusCode MuFastSteering::findMultiTrackSignature start");
  StatusCode sc = StatusCode::SUCCESS;
  const float ZERO_LIMIT = 1.e-5;

  // for RPC clustering and clusterRoad
  std::vector<TrigL2MuonSA::RpcFitResult>   clusterFitResults;
  std::vector< TrigL2MuonSA::MuonRoad >     clusterRoad;
  std::vector<TrigL2MuonSA::MdtHits>        mdtHits_cluster_normal;


  auto prepTimer           = Monitored::Timer( "TIME_Data_Preparator" );
  auto patternTimer        = Monitored::Timer( "TIME_Pattern_Finder" );
  auto stationFitterTimer  = Monitored::Timer( "TIME_Station_Fitter" );
  auto trackFitterTimer    = Monitored::Timer( "TIME_Track_Fitter" );
  auto trackExtraTimer     = Monitored::Timer( "TIME_Track_Extrapolator" );
  auto calibrationTimer    = Monitored::Timer( "TIME_Calibration_Streamer" );

  auto monitorIt	= Monitored::Group(m_monTool, prepTimer, patternTimer, stationFitterTimer,
                                                trackFitterTimer, trackExtraTimer, calibrationTimer );

  if (m_use_timer) {
    for (unsigned int i_timer=0; i_timer<m_timingTimers.size(); i_timer++) {
      m_timingTimers[i_timer]->start();
      m_timingTimers[i_timer]->pause();
    }
  }

  if (m_use_timer) m_timingTimers[ITIMER_TOTAL_PROCESSING]->resume();
  if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->resume();

  TrigL2MuonSA::RpcHits      rpcHits;
  TrigL2MuonSA::TgcHits      tgcHits;
  TrigL2MuonSA::MdtRegion    mdtRegion;
  TrigL2MuonSA::MuonRoad     muonRoad;
  TrigL2MuonSA::RpcFitResult rpcFitResult;
  TrigL2MuonSA::TgcFitResult tgcFitResult;
  TrigL2MuonSA::MdtHits      mdtHits_normal;
  TrigL2MuonSA::MdtHits      mdtHits_overlap;
  TrigL2MuonSA::CscHits      cscHits;
  TrigL2MuonSA::StgcHits     stgcHits;
  TrigL2MuonSA::MmHits       mmHits;

  DataVector<const TrigRoiDescriptor>::const_iterator p_roids;
  DataVector<const xAOD::MuonRoI>::const_iterator p_roi;

  // muonRoIs = RecMURoIs, roids = MURoIs
  p_roids = roids.begin();
  for (p_roi=(muonRoIs).begin(); p_roi!=(muonRoIs).end(); ++p_roi) {

    prepTimer.start();
    std::vector<TrigL2MuonSA::TrackPattern> trackPatterns;
    rpcHits.clear();
    tgcHits.clear();
    mdtRegion.Clear();
    muonRoad.Clear();
    rpcFitResult.Clear();
    tgcFitResult.Clear();
    mdtHits_normal.clear();
    mdtHits_overlap.clear();
    cscHits.clear();
    stgcHits.clear();
    mmHits.clear();

    clusterFitResults.clear();
    clusterRoad.clear();
    mdtHits_cluster_normal.clear();

    if ( m_recMuonRoIUtils.isBarrel(*p_roi) ) { // Barrel
      ATH_MSG_DEBUG("Barrel");

      muonRoad.setScales(m_scaleRoadBarrelInner,
			 m_scaleRoadBarrelMiddle,
			 m_scaleRoadBarrelOuter);

      // Data preparation
      sc = m_dataPreparator->prepareData(*p_roi,
                                         *p_roids,
                                         clusterRoad,
                                         clusterFitResults,
                                         mdtHits_normal,
                                         mdtHits_overlap,
                                         mdtHits_cluster_normal);

      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Data preparation failed");
	continue;
      }
      ATH_MSG_DEBUG("clusterRoad size = " << clusterRoad.size());

      if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->pause();
      prepTimer.stop();

      for(int i_road = 0; i_road < (int)clusterRoad.size(); i_road++){
        // Pattern finding
        std::vector<TrigL2MuonSA::TrackPattern> tmp_trkPats; tmp_trkPats.clear();

        if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->resume();
        patternTimer.start();
        sc = m_patternFinder->findPatterns(clusterRoad.at(i_road),
                                           mdtHits_cluster_normal.at(i_road),
                                           tmp_trkPats);
        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Pattern finder failed");
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->pause();
        patternTimer.stop();

        // Superpoint fit
        if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->resume();
        stationFitterTimer.start();
        sc = m_stationFitter->findSuperPoints(*p_roids,
                                              clusterRoad.at(i_road),
                                              clusterFitResults.at(i_road),
                                              tmp_trkPats);
        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Super point fitter failed");
          // Update output trigger element
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->pause();
        stationFitterTimer.stop();

        // Track fitting
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->resume();
        trackFitterTimer.start();
        sc = m_trackFitter->findTracks(*p_roids,
                                       clusterFitResults.at(i_road),
                                       tmp_trkPats);

        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Track fitter failed");
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->pause();
        trackFitterTimer.stop();

        // fix if eta is strange
        const float ETA_LIMIT       = 2.8;
        const float DELTA_ETA_LIMIT = 1.0;
        for (TrigL2MuonSA::TrackPattern& track : tmp_trkPats) {
          float roiEta = (*p_roi)->eta();
          if (std::abs(track.pt) > ZERO_LIMIT
              && ( std::abs(track.etaMap) > ETA_LIMIT || std::abs(track.etaMap-roiEta) > DELTA_ETA_LIMIT ) ) {
            track.etaMap = roiEta;
          }
        }

        // Track extrapolation for ID combined
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_EXTRAPOLATOR]->resume();
        trackExtraTimer.start();

        sc = m_trackExtrapolator->extrapolateTrack(tmp_trkPats, m_winPt);
        ATH_MSG_DEBUG("test trackExtrapolator end");

        if (sc != StatusCode::SUCCESS) {
          ATH_MSG_WARNING("Track extrapolator failed");
          // Update output trigger element
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_EXTRAPOLATOR]->pause();
        trackExtraTimer.stop();

        if(tmp_trkPats.size() > 0){
          ATH_MSG_DEBUG("temp pT calculated 2mu-in-1RoI alg = " << tmp_trkPats[0].pt << " GeV");
          if( (std::abs(tmp_trkPats[0].barrelSagitta) < ZERO_LIMIT &&
               std::abs(tmp_trkPats[0].barrelRadius) < ZERO_LIMIT) ||
               std::abs(tmp_trkPats[0].pt) < ZERO_LIMIT )
            continue;
          trackPatterns.push_back(tmp_trkPats[0]);
        }

	storeMuonSA(*p_roi, *p_roids, clusterRoad.at(i_road), mdtRegion, rpcHits, tgcHits,
		    clusterFitResults.at(i_road), tgcFitResult, mdtHits_cluster_normal.at(i_road), cscHits,
		    stgcHits, mmHits, trackPatterns.back(), outputTracks);

      } // end the clusterRoad loop
      if(trackPatterns.empty()){
	ATH_MSG_DEBUG("multi-track SA falied to reconstruct muons");
 	TrigL2MuonSA::TrackPattern trackPattern;
	trackPatterns.push_back(trackPattern);
	storeMuonSA(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
		    rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
		    stgcHits, mmHits, trackPatterns.back(), outputTracks);

	continue;
      }
    } else { // Endcap
      ATH_MSG_DEBUG("Endcap");
      if(!m_doEndcapForl2mt){
        ATH_MSG_DEBUG("multi-track SA does nothings and skips for EndcapRoI");
      } else {
        prepTimer.start();
        // Data preparation
        sc = m_dataPreparator->prepareData(*p_roi,
                                           *p_roids,
                                           m_insideOut,
                                           tgcHits,
                                           muonRoad,
                                           mdtRegion,
                                           tgcFitResult,
                                           mdtHits_normal,
                                           mdtHits_overlap,
                                           cscHits,
                                           stgcHits,
                                           mmHits);
        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Data preparation failed");
          TrigL2MuonSA::TrackPattern trackPattern;
          trackPatterns.push_back(trackPattern);
          // Update output trigger element
          storeMuonSA(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
                      rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
                      stgcHits, mmHits, trackPatterns.back(), outputTracks);
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_DATA_PREPARATOR]->pause();
        prepTimer.stop();

        // Pattern finding
        if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->resume();
        patternTimer.start();
        sc = m_patternFinder->findPatterns(muonRoad,
                                           mdtHits_normal,
                                           stgcHits,
                                           mmHits,
                                           trackPatterns);



        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Pattern finder failed");
          // Update output trigger element
          storeMuonSA(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
		      rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
                      stgcHits, mmHits, trackPatterns.back(), outputTracks);
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_PATTERN_FINDER]->pause();
        patternTimer.stop();

        // Superpoint fit
        if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->resume();
        stationFitterTimer.start();
        if(!m_use_new_segmentfit){
          sc = m_stationFitter->findSuperPointsSimple(*p_roids,
	       					      muonRoad,
                                                      tgcFitResult,
                                                      trackPatterns);
        }else{
          sc = m_stationFitter->findSuperPoints(*p_roids,
                                                muonRoad,
                                                tgcFitResult,
                                                trackPatterns);
        }
        /////csc SuperPoint
        m_cscsegmaker->FindSuperPointCsc(cscHits,trackPatterns,tgcFitResult,muonRoad);

        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Super point fitter failed");
          storeMuonSA(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
		      rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
                      stgcHits, mmHits, trackPatterns.back(), outputTracks);
          continue;
        }

        if (m_use_timer) m_timingTimers[ITIMER_STATION_FITTER]->pause();
        stationFitterTimer.stop();

        // Track fittingh
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->resume();
        trackFitterTimer.start();
        sc = m_trackFitter->findTracks(*p_roids,
                                       tgcFitResult,
                                       trackPatterns,
                                       muonRoad);

        if (!sc.isSuccess()) {
          ATH_MSG_WARNING("Track fitter failed");
          storeMuonSA(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
		      rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
                      stgcHits, mmHits, trackPatterns.back(), outputTracks);
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_FITTER]->pause();
        trackFitterTimer.stop();

        // fix if eta is strange
        const float ETA_LIMIT       = 2.8;
        const float DELTA_ETA_LIMIT = 1.0;
        for (TrigL2MuonSA::TrackPattern& track : trackPatterns) {
          float roiEta = (*p_roi)->eta();
          if (std::abs(track.pt) > ZERO_LIMIT
              && ( std::abs(track.etaMap) > ETA_LIMIT || std::abs(track.etaMap-roiEta) > DELTA_ETA_LIMIT ) ) {
            track.etaMap = roiEta;
          }
        }

        // Track extrapolation for ID combined
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_EXTRAPOLATOR]->resume();
        trackExtraTimer.start();

        sc = m_trackExtrapolator->extrapolateTrack(trackPatterns, m_winPt);

        if (sc != StatusCode::SUCCESS) {
          ATH_MSG_WARNING("Track extrapolator failed");
          storeMuonSA(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
                      rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
                      stgcHits, mmHits, trackPatterns.back(), outputTracks);
          continue;
        }
        if (m_use_timer) m_timingTimers[ITIMER_TRACK_EXTRAPOLATOR]->pause();
        trackExtraTimer.stop();

	storeMuonSA(*p_roi, *p_roids, muonRoad, mdtRegion, rpcHits, tgcHits,
		    rpcFitResult, tgcFitResult, mdtHits_normal, cscHits,
		    stgcHits, mmHits, trackPatterns.back(), outputTracks);
      }
    }
    // Update monitoring variables
    sc = updateMonitor(*p_roi, mdtHits_normal, trackPatterns );
    if (sc != StatusCode::SUCCESS) {
      ATH_MSG_WARNING("Failed to update monitoring variables");
    }

    p_roids++;
    if (p_roids==roids.end()) break;
  }

  if (m_use_timer) m_timingTimers[ITIMER_TOTAL_PROCESSING]->pause();

  int nRoI = muonRoIs.size();

  if (m_use_timer) {
     for (unsigned int i_timer=0; i_timer<m_timingTimers.size(); i_timer++) {
         m_timingTimers[i_timer]->propVal(nRoI);
         m_timingTimers[i_timer]->stop();
     }
  }

  ATH_MSG_DEBUG("StatusCode MuFastSteering::findMultiTrackSignature success");
  return StatusCode::SUCCESS;
=======
  // initialize the joboptions service
  sc = service("JobOptionsSvc", m_jobOptionsSvc);
  if (sc.isFailure()) {
    ATH_MSG_ERROR("Could not find JobOptionsSvc");
    return HLT::ERROR;
  } else {
    IService* svc = dynamic_cast<IService*>(m_jobOptionsSvc);
    if(svc != 0 ) {
      ATH_MSG_DEBUG(" Algorithm = " << name() << " is connected to JobOptionsSvc Service = " << svc->name());
    }  
  }

  //
  // Initialize the calibration streamer and the incident 
  // service for its initialization
  //
  if (m_doCalStream) {
    // retrieve the calibration streamer
    if (m_calStreamer.retrieve().isFailure()) {
      ATH_MSG_ERROR("Cannot retrieve Tool CalStreamer");
      return HLT::BAD_JOB_SETUP;
    }
    // set properties
    m_calStreamer->setBufferName(m_calBufferName);
    ATH_MSG_DEBUG("Initialized the Muon Calibration Streamer. Buffer name: " << m_calBufferName 
		  << ", buffer size: " << m_calBufferSize 
		  << " doDataScouting: "  << m_calDataScouting);
    
    ServiceHandle<IIncidentSvc> p_incidentSvc("IncidentSvc",name());
    sc =  p_incidentSvc.retrieve();
    if (!sc.isSuccess()) {
      ATH_MSG_ERROR("Could not find IncidentSvc");
      return HLT::ERROR;
    } else {
      long int pri=100;
      p_incidentSvc->addListener(this,"UpdateAfterFork",pri);
      p_incidentSvc.release().ignore();
    }
  }





  
  ATH_MSG_DEBUG("initialize success");
  
  return HLT::OK;
>>>>>>> release/21.0.127
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

<<<<<<< HEAD
bool MuFastSteering::updateOutputObjects(const LVL1::RecMuonRoI*                        roi,
                                         const TrigRoiDescriptor*                       roids,
                                         const TrigL2MuonSA::MuonRoad&                  muonRoad,
                                         const TrigL2MuonSA::MdtRegion&                 mdtRegion,
                                         const TrigL2MuonSA::RpcHits&                   rpcHits,
                                         const TrigL2MuonSA::TgcHits&                   tgcHits,
                                         const TrigL2MuonSA::RpcFitResult&              rpcFitResult,
                                         const TrigL2MuonSA::TgcFitResult&              tgcFitResult,
                                         const TrigL2MuonSA::MdtHits&                   mdtHits,
                                         const TrigL2MuonSA::CscHits&                   cscHits,
                                         const TrigL2MuonSA::StgcHits&                  stgcHits,
                                         const TrigL2MuonSA::MmHits&                    mmHits,
                                         const std::vector<TrigL2MuonSA::TrackPattern>& trackPatterns,
				         DataVector<xAOD::L2StandAloneMuon>&	        outputTracks,
				         TrigRoiDescriptorCollection&  	                outputID,
				         TrigRoiDescriptorCollection&   	        outputMS) const
{

  if( trackPatterns.size() > 0 ) {
=======
HLT::ErrorCode MuFastSteering::hltBeginRun() {
  ATH_MSG_DEBUG("hltBeginRun");
  return HLT::OK;
}

HLT::ErrorCode MuFastSteering::hltEndRun() {
  ATH_MSG_DEBUG("hltEndRun");
   // close the calibration stream 
   if (m_doCalStream) { 
     if ( !m_calDataScouting ) {
       StatusCode sc = m_calStreamer->closeStream();
       if ( sc != StatusCode::SUCCESS ) {
	 ATH_MSG_ERROR("Failed to close the calibration stream");
       }
     }
   } 
>>>>>>> release/21.0.127

    const TrigL2MuonSA::TrackPattern& pattern = trackPatterns.back();

    // Update output trigger element
    storeMuonSA(roi, roids, muonRoad, mdtRegion, rpcHits, tgcHits,
    	        rpcFitResult, tgcFitResult, mdtHits, cscHits,
		stgcHits, mmHits,
                pattern, outputTracks);
    storeMSRoiDescriptor(roids, pattern, outputTracks, outputMS);
    storeIDRoiDescriptor(roids, pattern, outputTracks, outputID);

<<<<<<< HEAD
  } else {
    ATH_MSG_DEBUG("Not update output objects because trackPatterns has no object");
  }

  return true;
=======
HLT::ErrorCode MuFastSteering::hltFinalize() {
  ATH_MSG_DEBUG("hltFinalize()");
   return HLT::OK;
>>>>>>> release/21.0.127
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

bool MuFastSteering::updateOutputObjects(const xAOD::MuonRoI*                           roi,
                                         const TrigRoiDescriptor*                       roids,
                                         const TrigL2MuonSA::MuonRoad&                  muonRoad,
                                         const TrigL2MuonSA::MdtRegion&                 mdtRegion,
                                         const TrigL2MuonSA::RpcHits&                   rpcHits,
                                         const TrigL2MuonSA::TgcHits&                   tgcHits,
                                         const TrigL2MuonSA::RpcFitResult&              rpcFitResult,
                                         const TrigL2MuonSA::TgcFitResult&              tgcFitResult,
                                         const TrigL2MuonSA::MdtHits&                   mdtHits,
                                         const TrigL2MuonSA::CscHits&                   cscHits,
                                         const TrigL2MuonSA::StgcHits&                  stgcHits,
                                         const TrigL2MuonSA::MmHits&                    mmHits,
                                         const std::vector<TrigL2MuonSA::TrackPattern>& trackPatterns,
				         DataVector<xAOD::L2StandAloneMuon>&	        outputTracks,
				         TrigRoiDescriptorCollection&  	                outputID,
				         TrigRoiDescriptorCollection&   	        outputMS) const
{
<<<<<<< HEAD
=======
  StatusCode sc = StatusCode::SUCCESS;
  // Initialize monitored variables;
  m_inner_mdt_hits  = -1;
  m_middle_mdt_hits = -1;
  m_outer_mdt_hits  = -1;
  
  m_fit_residuals.clear();
  m_res_inner.clear();
  m_res_middle.clear();
  m_res_outer.clear();
  
  m_efficiency  = 0;
  m_sag_inverse = 9999.;
  m_sagitta     = 9999.;
  m_address     = 9999;
  m_absolute_pt = 9999.;

  m_track_pt    = 9999.;  
  m_track_eta.clear();
  m_track_phi.clear();
  m_failed_eta.clear();
  m_failed_phi.clear();

  if (m_timerSvc) {
    for (unsigned int i_timer=0; i_timer<m_timers.size(); i_timer++) {
      m_timers[i_timer]->start();
      m_timers[i_timer]->pause();
    }
  }
  clearEvent();
  
  if (m_timerSvc) m_timers[ITIMER_TOTAL_PROCESSING]->resume();
  if (m_timerSvc) m_timers[ITIMER_DATA_PREPARATOR]->resume();
  
  ATH_MSG_DEBUG("hltExecute called");
  
  std::vector<const TrigRoiDescriptor*> roids;
  std::vector<const TrigRoiDescriptor*>::const_iterator p_roids;
  HLT::ErrorCode hec2 = getFeatures(inputTE, roids);
  
  std::vector<const LVL1::RecMuonRoI*> muonRoIs;
  std::vector<const LVL1::RecMuonRoI*>::const_iterator p_roi;
  HLT::ErrorCode hec = getFeatures(inputTE, muonRoIs);

  if (hec != HLT::OK && hec2 != HLT::OK) {
    ATH_MSG_ERROR("Could not find input TE");
    return hec2;
  }
  
  ATH_MSG_DEBUG("RecMuonRoI size: " << muonRoIs.size());
  ATH_MSG_DEBUG("RoIdescriptor size: " << roids.size());
>>>>>>> release/21.0.127

  if( trackPatterns.size() > 0 ) {

    const TrigL2MuonSA::TrackPattern& pattern = trackPatterns.back();

<<<<<<< HEAD
    // Update output trigger element
    storeMuonSA(roi, roids, muonRoad, mdtRegion, rpcHits, tgcHits,
    	        rpcFitResult, tgcFitResult, mdtHits, cscHits,
		stgcHits, mmHits,
                pattern, outputTracks);
    storeMSRoiDescriptor(roids, pattern, outputTracks, outputMS);
    storeIDRoiDescriptor(roids, pattern, outputTracks, outputID);
=======
    ATH_MSG_DEBUG("RoI eta/phi=" << roiEta << "/" << roiPhi);
    
    std::vector<TrigL2MuonSA::TrackPattern> m_trackPatterns;
    m_mdtHits_normal.clear();
    m_mdtHits_overlap.clear();
    m_cscHits.clear();
    
    m_rpcFitResult.Clear();
    m_tgcFitResult.Clear();
>>>>>>> release/21.0.127

  } else {
    ATH_MSG_DEBUG("Not update output objects because trackPatterns has no object");
  }

<<<<<<< HEAD
  return true;
}


bool MuFastSteering::storeMuonSA(const LVL1::RecMuonRoI*             roi,
                                 const TrigRoiDescriptor*            roids,
             	                 const TrigL2MuonSA::MuonRoad&       muonRoad,
             	                 const TrigL2MuonSA::MdtRegion&      mdtRegion,
             	                 const TrigL2MuonSA::RpcHits&        rpcHits,
             	                 const TrigL2MuonSA::TgcHits&        tgcHits,
             	                 const TrigL2MuonSA::RpcFitResult&   rpcFitResult,
             	                 const TrigL2MuonSA::TgcFitResult&   tgcFitResult,
             	                 const TrigL2MuonSA::MdtHits&        mdtHits,
             	                 const TrigL2MuonSA::CscHits&        cscHits,
				 const TrigL2MuonSA::StgcHits&       stgcHits,
				 const TrigL2MuonSA::MmHits&         mmHits,
             	                 const TrigL2MuonSA::TrackPattern&   pattern,
                                 DataVector<xAOD::L2StandAloneMuon>& outputTracks ) const
{
  const float ZERO_LIMIT = 1.e-5;

  const int currentRoIId = roids->roiId();

  const EventContext& ctx = getContext();
  const EventIDBase& eventID = ctx.eventID();
  auto eventInfo = SG::makeHandle(m_eventInfoKey, ctx);
  if (!eventInfo.isValid()) {
    ATH_MSG_ERROR("Failed to retrieve xAOD::EventInfo object");
    return false;
  }

  int inner  = 0;
  int middle = 1;
  int outer  = 2;
  int ee     = 6;
  int csc    = 7;
  int barrelinner = 0;
  int endcapinner = 3;
  int bee = 8;
  int bme = 9;
  // int bmg = 10;

  // define inner, middle, outer
  if (pattern.s_address==-1) {
    inner  = xAOD::L2MuonParameters::Chamber::EndcapInner;
    middle = xAOD::L2MuonParameters::Chamber::EndcapMiddle;
    outer  = xAOD::L2MuonParameters::Chamber::EndcapOuter;
    ee     = xAOD::L2MuonParameters::Chamber::EndcapExtra;
    barrelinner     = xAOD::L2MuonParameters::Chamber::BarrelInner;
    bee = xAOD::L2MuonParameters::Chamber::BEE;
  } else {
    inner  = xAOD::L2MuonParameters::Chamber::BarrelInner;
    middle = xAOD::L2MuonParameters::Chamber::BarrelMiddle;
    outer  = xAOD::L2MuonParameters::Chamber::BarrelOuter;
    bme = xAOD::L2MuonParameters::Chamber::BME;
    endcapinner  = xAOD::L2MuonParameters::Chamber::EndcapInner;
  }

  ATH_MSG_DEBUG("### Hit patterns at the Muon Spectrometer ###");
  ATH_MSG_DEBUG("pattern#0: # of hits at inner  =" << pattern.mdtSegments[inner].size());
  ATH_MSG_DEBUG("pattern#0: # of hits at middle =" << pattern.mdtSegments[middle].size());
  ATH_MSG_DEBUG("pattern#0: # of hits at outer  =" << pattern.mdtSegments[outer].size());
  if (pattern.s_address==-1){
    ATH_MSG_DEBUG("pattern#0: # of hits at ee  =" << pattern.mdtSegments[ee].size());
    ATH_MSG_DEBUG("pattern#0: # of hits at endcap barrel inner  =" << pattern.mdtSegments[barrelinner].size());
    ATH_MSG_DEBUG("pattern#0: # of hits at BEE  =" << pattern.mdtSegments[bee].size());
  } else {
    ATH_MSG_DEBUG("pattern#0: # of hits at BME  =" << pattern.mdtSegments[bme].size());
    ATH_MSG_DEBUG("pattern#0: # of hits at barrel endcap inner  =" << pattern.mdtSegments[endcapinner].size());
  }
  ATH_MSG_DEBUG("### ************************************* ###");
  ATH_MSG_DEBUG("Estimated muon pt = " << pattern.pt << " GeV");

  // ---------
  // store xAOD

  xAOD::L2StandAloneMuon* muonSA = new xAOD::L2StandAloneMuon();
  muonSA->makePrivateStore();

  // add pT
  muonSA->setPt(pattern.pt*pattern.charge);
  muonSA->setPtEndcapAlpha(pattern.ptEndcapAlpha*pattern.charge);
  muonSA->setPtEndcapBeta(pattern.ptEndcapBeta*pattern.charge);
  muonSA->setPtEndcapRadius(pattern.ptEndcapRadius*pattern.charge);
  muonSA->setPtCSC(pattern.ptCSC*pattern.charge);

  muonSA->setEta(pattern.etaVtx);
  muonSA->setPhi(pattern.phiVtx);
  muonSA->setDeltaPt(pattern.deltaPt);
  muonSA->setDeltaEta(pattern.deltaEtaVtx);
  muonSA->setDeltaPhi(pattern.deltaPhiVtx);

  // add s_address
  muonSA->setSAddress(pattern.s_address);

  // add positions at MS
  muonSA->setEtaMS(pattern.etaMap);
  muonSA->setPhiMS(pattern.phiMS);
  muonSA->setDirPhiMS(pattern.phiMSDir);
  muonSA->setRMS(pattern.superPoints[inner].R);
  muonSA->setZMS(pattern.superPoints[inner].Z);
  muonSA->setDirZMS(pattern.superPoints[inner].Alin);

  // add pt variables
  // Endcap
  muonSA->setEndcapAlpha(pattern.endcapAlpha);
  muonSA->setEndcapBeta(pattern.endcapBeta);
  muonSA->setEndcapRadius(pattern.endcapRadius3P);
  // Barrel
  muonSA->setBarrelRadius(pattern.barrelRadius);
  muonSA->setBarrelSagitta(pattern.barrelSagitta);

  // store eta and phi used as argument to pT LUT
  muonSA->setEtaMap(pattern.etaMap);
  muonSA->setPhiMap(pattern.phiMap);
  muonSA->setEtaBin(pattern.etaBin);
  muonSA->setPhiBin(pattern.phiBin);

  // store TGC/RPC readout failure flags
  muonSA->setIsTgcFailure((int)pattern.isTgcFailure);
  muonSA->setIsRpcFailure((int)pattern.isRpcFailure);

  // add superpoints
  muonSA->setSuperPoint(inner, pattern.superPoints[inner].R, pattern.superPoints[inner].Z,
                        pattern.superPoints[inner].Alin, pattern.superPoints[inner].Blin, pattern.superPoints[inner].Chi2);
  muonSA->setSuperPoint(middle, pattern.superPoints[middle].R, pattern.superPoints[middle].Z,
                        pattern.superPoints[middle].Alin, pattern.superPoints[middle].Blin, pattern.superPoints[middle].Chi2);
  muonSA->setSuperPoint(outer, pattern.superPoints[outer].R, pattern.superPoints[outer].Z,
                        pattern.superPoints[outer].Alin, pattern.superPoints[outer].Blin, pattern.superPoints[outer].Chi2);
  if (pattern.s_address==-1){
    muonSA->setSuperPoint(ee, pattern.superPoints[ee].R, pattern.superPoints[ee].Z,
                          pattern.superPoints[ee].Alin, pattern.superPoints[ee].Blin, pattern.superPoints[ee].Chi2);
    muonSA->setSuperPoint(barrelinner, pattern.superPoints[barrelinner].R, pattern.superPoints[barrelinner].Z,
                          pattern.superPoints[barrelinner].Alin, pattern.superPoints[barrelinner].Blin, pattern.superPoints[barrelinner].Chi2);
    muonSA->setSuperPoint(csc, pattern.superPoints[csc].R, pattern.superPoints[csc].Z,
      		    pattern.superPoints[csc].Alin, pattern.superPoints[csc].Blin, pattern.superPoints[csc].Chi2);
  } else {
    muonSA->setSuperPoint(endcapinner, pattern.superPoints[endcapinner].R, pattern.superPoints[endcapinner].Z,
                          pattern.superPoints[endcapinner].Alin, pattern.superPoints[endcapinner].Blin, pattern.superPoints[endcapinner].Chi2);
  }

  ///////////////////////////////
  // Below are detailed information

  uint32_t muondetmask = 0;

  /// Set L2 muon algorithm ID
  muonSA->setAlgoId( L2MuonAlgoMap(name()) );
  /// Set input TE ID
  //muonSA->setTeId( inputTE->getId() );	// move to hltExecute()
  /// Set level-1 ID
  muonSA->setLvl1Id( eventInfo->extendedLevel1ID() );
  /// Set lumi block
  muonSA->setLumiBlock( eventID.lumi_block() );
  /// Set muon detector mask
  muonSA->setMuonDetMask( muondetmask );
  /// Set RoI ID
  muonSA->setRoiId( currentRoIId );
  /// Set RoI system ID
  muonSA->setRoiSystem( roi->sysID() );
  /// Set RoI subsystem ID
  muonSA->setRoiSubsystem( roi->subsysID() );
  /// Set RoI sector ID
  muonSA->setRoiSector( roi->sectorID() );
  /// Set RoI number
  muonSA->setRoiNumber( roi->getRoINumber() );
  /// Set RoI threshold number
  muonSA->setRoiThreshold( roi->getThresholdNumber() );
  /// Set RoI eta
  muonSA->setRoiEta( roi->eta() );
  /// Set RoIp phi
  muonSA->setRoiPhi( roi->phi() );
  /// Set RoI word
  muonSA->setRoIWord( roi->roiWord() );

  /// Set size of storages to be reserved
  muonSA->setRpcHitsCapacity( m_esd_rpc_size );
  muonSA->setTgcHitsCapacity( m_esd_tgc_size );
  muonSA->setMdtHitsCapacity( m_esd_mdt_size );
  muonSA->setCscHitsCapacity( m_esd_csc_size );
  muonSA->setStgcClustersCapacity( m_esd_stgc_size );
  muonSA->setMmClustersCapacity( m_esd_mm_size );

  // MDT hits
  std::vector<std::string> mdtId;
  for (const TrigL2MuonSA::MdtHitData& mdtHit : mdtHits) {
    if ( mdtHit.isOutlier==0 || mdtHit.isOutlier==1 ) {
      muonSA->setMdtHit(mdtHit.OnlineId, mdtHit.isOutlier, mdtHit.Chamber,
                        mdtHit.R, mdtHit.Z, mdtHit.cPhi0, mdtHit.Residual,
                        mdtHit.DriftTime, mdtHit.DriftSpace, mdtHit.DriftSigma);
      mdtId.push_back(mdtHit.Id.getString());
    }
  }
  SG::AuxElement::Accessor< std::vector<std::string> > accessor_mdthitid( "mdtHitId" );
  accessor_mdthitid( *muonSA ) = mdtId;

  //CSC hits
  std::vector<float> cscResol;
  for (const TrigL2MuonSA::CscHitData& cscHit : cscHits) {
    if ( 1/*cscHit.MeasuresPhi==0*/ ){
      if ( cscHit.isOutlier==0 || cscHit.isOutlier==1 ) {
        muonSA->setCscHit(cscHit.isOutlier, cscHit.Chamber, cscHit.StationName,
                          cscHit.StationEta, cscHit.StationPhi,
                          cscHit.ChamberLayer, cscHit.WireLayer, cscHit.MeasuresPhi, cscHit.Strip,
                          cscHit.eta, cscHit.phi, cscHit.r, cscHit.z,
                          cscHit.charge, cscHit.time, cscHit.Residual);
	cscResol.push_back(cscHit.resolution);
        ATH_MSG_VERBOSE("CSC Hits stored in xAOD: "
      		<< "OL=" << cscHit.isOutlier << ","
      		<< "Ch=" << cscHit.Chamber << ","
      		<< "StationName=" << cscHit.StationName << ","
      		<< "StationEta=" << cscHit.StationEta << ","
      		<< "StationPhi=" << cscHit.StationPhi << ","
      		<< "ChamberLayer=" << cscHit.ChamberLayer << ","
      		<< "WireLayer=" << cscHit.WireLayer << ","
      		<< "MeasuresPhi=" << cscHit.MeasuresPhi << ","
      		<< "Strip=" << cscHit.Strip << ","
      		<< "eta="  << cscHit.eta << ","
      		<< "phi="  << cscHit.phi << ","
      		<< "r="  << cscHit.r << ","
      		<< "z="  << cscHit.z << ","
      		<< "charge=" << cscHit.charge << ","
      		<< "Rs=" << cscHit.Residual << ","
      		<< "t="  << cscHit.time);
=======
    if ( m_recMuonRoIUtils.isBarrel(*p_roi) ) { // Barrel
      ATH_MSG_DEBUG("Barrel");
      
      m_muonRoad.setScales(m_scaleRoadBarrelInner,
                           m_scaleRoadBarrelMiddle,
                           m_scaleRoadBarrelOuter);      

      // Data preparation
      m_rpcHits.clear();
      sc = m_dataPreparator->prepareData(*p_roi,
                                         *p_roids,
                                         m_rpcHits,
                                         m_muonRoad,
                                         m_mdtRegion,
                                         m_rpcFitResult,
                                         m_mdtHits_normal,
                                         m_mdtHits_overlap);
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Data preparation failed");
	TrigL2MuonSA::TrackPattern trackPattern;
	m_trackPatterns.push_back(trackPattern);
	updateOutputTE(outputTE, inputTE, *p_roi, *p_roids, m_muonRoad, m_mdtRegion, m_rpcHits, m_tgcHits,
		       m_rpcFitResult, m_tgcFitResult, m_mdtHits_normal, m_cscHits, m_trackPatterns);
	return HLT::OK;
      }
      if (m_timerSvc) m_timers[ITIMER_DATA_PREPARATOR]->pause();

      if ( m_rpcErrToDebugStream && m_dataPreparator->isRpcFakeRoi() ) {
        ATH_MSG_ERROR("Invalid RoI in RPC data found: event to debug stream");
	TrigL2MuonSA::TrackPattern trackPattern;
	m_trackPatterns.push_back(trackPattern);
	updateOutputTE(outputTE, inputTE, *p_roi, *p_roids, m_muonRoad, m_mdtRegion, m_rpcHits, m_tgcHits,
		       m_rpcFitResult, m_tgcFitResult, m_mdtHits_normal, m_cscHits, m_trackPatterns);
	return HLT::ErrorCode(HLT::Action::ABORT_CHAIN, HLT::Reason::UNKNOWN);
      } 

      // Pattern finding
      if (m_timerSvc) m_timers[ITIMER_PATTERN_FINDER]->resume();
      sc = m_patternFinder->findPatterns(m_muonRoad,
                                         m_mdtHits_normal,
                                         m_trackPatterns);
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Pattern finder failed");
         updateOutputTE(outputTE, inputTE, *p_roi, *p_roids, m_muonRoad, m_mdtRegion, m_rpcHits, m_tgcHits,
                        m_rpcFitResult, m_tgcFitResult, m_mdtHits_normal, m_cscHits, m_trackPatterns);
         return HLT::OK;
      }
      if (m_timerSvc) m_timers[ITIMER_PATTERN_FINDER]->pause();

      // Superpoint fit
      if (m_timerSvc) m_timers[ITIMER_STATION_FITTER]->resume();      
      sc = m_stationFitter->findSuperPoints(*p_roi,
                                            m_rpcFitResult,
                                            m_trackPatterns);
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Super point fitter failed");
         updateOutputTE(outputTE, inputTE, *p_roi, *p_roids, m_muonRoad, m_mdtRegion, m_rpcHits, m_tgcHits,
                        m_rpcFitResult, m_tgcFitResult, m_mdtHits_normal, m_cscHits, m_trackPatterns);
         return HLT::OK;
      }
      if (m_timerSvc) m_timers[ITIMER_STATION_FITTER]->pause();      

      // Track fitting
      if (m_timerSvc) m_timers[ITIMER_TRACK_FITTER]->resume();      
      sc = m_trackFitter->findTracks(*p_roi,
                                     m_rpcFitResult,
                                     m_trackPatterns);
                                     
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Track fitter failed");
         updateOutputTE(outputTE, inputTE, *p_roi, *p_roids, m_muonRoad, m_mdtRegion, m_rpcHits, m_tgcHits,
                        m_rpcFitResult, m_tgcFitResult, m_mdtHits_normal, m_cscHits, m_trackPatterns);
         return HLT::OK;
      }
      if (m_timerSvc) m_timers[ITIMER_TRACK_FITTER]->pause();      

    } else { // Endcap
      ATH_MSG_DEBUG("Endcap");

      // Data preparation
      m_tgcHits.clear();     
      sc = m_dataPreparator->prepareData(*p_roi,
                                         *p_roids,
                                         m_tgcHits,
                                         m_muonRoad,
                                         m_mdtRegion,
                                         m_tgcFitResult,
                                         m_mdtHits_normal,
                                         m_mdtHits_overlap,
                                         m_cscHits);
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Data preparation failed");
	TrigL2MuonSA::TrackPattern trackPattern;
	m_trackPatterns.push_back(trackPattern);
	updateOutputTE(outputTE, inputTE, *p_roi, *p_roids, m_muonRoad, m_mdtRegion, m_rpcHits, m_tgcHits,
		       m_rpcFitResult, m_tgcFitResult, m_mdtHits_normal, m_cscHits, m_trackPatterns);
	return HLT::OK;
>>>>>>> release/21.0.127
      }
    }
  }
  SG::AuxElement::Accessor< std::vector<float> > accessor_cschitresol( "cscHitResolution" );
  accessor_cschitresol( *muonSA ) = cscResol;

  // RPC hits
  float sumbeta[8]={0};
  float nhit_layer[8]={0};
  for (const TrigL2MuonSA::RpcHitData& rpcHit : rpcHits) {
    muonSA->setRpcHit(rpcHit.layer, rpcHit.measuresPhi,
                      rpcHit.x, rpcHit.y, rpcHit.z,
                      rpcHit.time, rpcHit.distToEtaReadout, rpcHit.distToPhiReadout,
                      rpcHit.stationName);
    ATH_MSG_VERBOSE("RPC hits stored in xAOD: "
      	    << "stationName=" << rpcHit.stationName << ","
      	    << "layer=" << rpcHit.layer << ","
      	    << "measuresPhi=" << rpcHit.measuresPhi << ","
      	    << "x=" << rpcHit.x << ","
      	    << "y=" << rpcHit.y << ","
      	    << "y=" << rpcHit.z);

    float dRMS = std::sqrt( std::abs(pattern.etaMap-rpcHit.eta)*std::abs(pattern.etaMap-rpcHit.eta) + std::acos(std::cos(pattern.phiMS-rpcHit.phi))*std::acos(std::cos(pattern.phiMS-rpcHit.phi)) );
    if(dRMS>0.05) continue;
    float muToF = rpcHit.l/1000/(CLHEP::c_light/1000);
    float Tprop = rpcHit.distToPhiReadout/1000*4.8;
    float beta = rpcHit.l/1000/(muToF+rpcHit.time-Tprop+3.125/2)/(CLHEP::c_light/1000);
    sumbeta[rpcHit.layer]=sumbeta[rpcHit.layer]+beta;
    nhit_layer[rpcHit.layer]=nhit_layer[rpcHit.layer]+1;
  }

  std::vector<float> Avebeta_layer;
  for(int i_layer=0;i_layer<8;i_layer++){
    if(nhit_layer[i_layer]!=0)Avebeta_layer.push_back( sumbeta[i_layer]/nhit_layer[i_layer] );
  }
  if(Avebeta_layer.size()>0) muonSA->setBeta( std::accumulate(Avebeta_layer.begin(),Avebeta_layer.end(),0.0)/Avebeta_layer.size() );
  else muonSA->setBeta( 9999 );
  Avebeta_layer.clear();

  // TGC hits
  for (const TrigL2MuonSA::TgcHitData& tgcHit : tgcHits) {
    muonSA->setTgcHit(tgcHit.eta, tgcHit.phi, tgcHit.r, tgcHit.z,
                      tgcHit.width, tgcHit.sta, tgcHit.isStrip,
                      tgcHit.bcTag, tgcHit.inRoad);
    ATH_MSG_VERBOSE("TGC hits stored in xAOD: "
      	    << "eta=" << tgcHit.eta << ","
      	    << "phi=" << tgcHit.phi << ","
      	    << "r=" << tgcHit.r << ","
      	    << "z=" << tgcHit.z << ","
      	    << "width=" << tgcHit.width << ","
      	    << "stationNum=" << tgcHit.sta << ","
      	    << "isStrip=" << tgcHit.isStrip << ","
      	    << "bcTag=" << tgcHit.bcTag << ","
      	    << "inRoad=" << tgcHit.inRoad);
  }

<<<<<<< HEAD

  // sTGC clusters
  for(unsigned int i_hit=0; i_hit<stgcHits.size(); i_hit++) {
    if ( stgcHits[i_hit].isOutlier==0 || stgcHits[i_hit].isOutlier==1 ) {
=======

      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Pattern finder failed");
         updateOutputTE(outputTE, inputTE, *p_roi, *p_roids, m_muonRoad, m_mdtRegion, m_rpcHits, m_tgcHits,
                        m_rpcFitResult, m_tgcFitResult, m_mdtHits_normal, m_cscHits, m_trackPatterns);
         return HLT::OK;
      }
      if (m_timerSvc) m_timers[ITIMER_PATTERN_FINDER]->pause();
      
      // Superpoint fit
      if (m_timerSvc) m_timers[ITIMER_STATION_FITTER]->resume();
      if(!m_use_new_segmentfit){
        sc = m_stationFitter->findSuperPoints(*p_roi,
                                              m_tgcFitResult,
                                              m_trackPatterns);
      }else{
        sc = m_stationFitter->findSuperPoints(*p_roi,
                                              m_muonRoad,
                                              m_tgcFitResult,
                                              m_trackPatterns);
      }
      /////csc SuperPoint
      m_cscsegmaker->FindSuperPointCsc(m_cscHits,m_trackPatterns,m_tgcFitResult,m_muonRoad);

      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Super point fitter failed");
         updateOutputTE(outputTE, inputTE, *p_roi, *p_roids, m_muonRoad, m_mdtRegion, m_rpcHits, m_tgcHits,
                        m_rpcFitResult, m_tgcFitResult, m_mdtHits_normal, m_cscHits, m_trackPatterns);
         return HLT::OK;
      }

      if (m_timerSvc) m_timers[ITIMER_STATION_FITTER]->pause();      
>>>>>>> release/21.0.127


<<<<<<< HEAD
      muonSA->setStgcCluster(stgcHits[i_hit].layerNumber, stgcHits[i_hit].isOutlier, stgcHits[i_hit].channelType,
      			     stgcHits[i_hit].eta, stgcHits[i_hit].phi, stgcHits[i_hit].r, stgcHits[i_hit].z,
      			     stgcHits[i_hit].ResidualR, stgcHits[i_hit].ResidualPhi,
      			     stgcHits[i_hit].stationEta, stgcHits[i_hit].stationPhi, stgcHits[i_hit].stationName);

      ATH_MSG_VERBOSE("sTGC hits stored in xAOD: "
		      << "eta=" << stgcHits[i_hit].eta << ","
		      << "phi=" << stgcHits[i_hit].phi << ","
		      << "r=" << stgcHits[i_hit].r << ","
		      << "z=" << stgcHits[i_hit].z << ","
		      << "z=" << stgcHits[i_hit].ResidualR << ","
		      << "z=" << stgcHits[i_hit].ResidualPhi);
=======
      if (!sc.isSuccess()) {
	ATH_MSG_WARNING("Track fitter failed");
         updateOutputTE(outputTE, inputTE, *p_roi, *p_roids, m_muonRoad, m_mdtRegion, m_rpcHits, m_tgcHits,
                        m_rpcFitResult, m_tgcFitResult, m_mdtHits_normal, m_cscHits, m_trackPatterns);
         return HLT::OK;
      }
      if (m_timerSvc) m_timers[ITIMER_TRACK_FITTER]->pause();      
>>>>>>> release/21.0.127
    }
  }

  // MM clusters
  for(unsigned int i_hit=0; i_hit<mmHits.size(); i_hit++) {
    if ( mmHits[i_hit].isOutlier==0 || mmHits[i_hit].isOutlier==1 ) {


      muonSA->setMmCluster(mmHits[i_hit].layerNumber, mmHits[i_hit].isOutlier,
      			   mmHits[i_hit].eta, mmHits[i_hit].phi, mmHits[i_hit].r, mmHits[i_hit].z,
      			   mmHits[i_hit].ResidualR, mmHits[i_hit].ResidualPhi,
      			   mmHits[i_hit].stationEta, mmHits[i_hit].stationPhi, mmHits[i_hit].stationName);

      ATH_MSG_VERBOSE("mm hits stored in xAOD: "
		      << "eta=" << tgcHits[i_hit].eta << ","
		      << "phi=" << tgcHits[i_hit].phi << ","
		      << "r=" << tgcHits[i_hit].r << ","
		      << "z=" << tgcHits[i_hit].z << ","
		      << "width=" << tgcHits[i_hit].width << ","
		      << "stationNum=" << tgcHits[i_hit].sta << ","
		      << "isStrip=" << tgcHits[i_hit].isStrip << ","
		      << "bcTag=" << tgcHits[i_hit].bcTag << ","
		      << "inRoad=" << tgcHits[i_hit].inRoad);
    }
  }



<<<<<<< HEAD

  // Muon road
  for (int i_station=0; i_station<8; i_station++) {
    for (int i_sector=0; i_sector<2; i_sector++) {
      muonSA->setRoad(i_station, i_sector, muonRoad.aw[i_station][i_sector], muonRoad.bw[i_station][i_sector]);
      muonSA->setRegionZ(i_station, i_sector, mdtRegion.zMin[i_station][i_sector], mdtRegion.zMax[i_station][i_sector]);
      muonSA->setRegionR(i_station, i_sector, mdtRegion.rMin[i_station][i_sector], mdtRegion.rMax[i_station][i_sector]);
      muonSA->setRegionEta(i_station, i_sector, mdtRegion.etaMin[i_station][i_sector], mdtRegion.etaMax[i_station][i_sector]);
      muonSA->setChamberType1(i_station, i_sector, mdtRegion.chamberType[i_station][i_sector][0]);
      muonSA->setChamberType2(i_station, i_sector, mdtRegion.chamberType[i_station][i_sector][1]);
    }
  }

  if ( muonRoad.isEndcap ) {
    // TGC fit results
    if (tgcFitResult.isSuccess ) {
      muonSA->setTgcPt(tgcFitResult.tgcPT);

      muonSA->setTgcInn(tgcFitResult.tgcInn[0], tgcFitResult.tgcInn[1],
      		  tgcFitResult.tgcInn[2], tgcFitResult.tgcInn[3]);
      muonSA->setTgcInnF(tgcFitResult.tgcInnRhoStd, tgcFitResult.tgcInnRhoNin,
      		   tgcFitResult.tgcInnPhiStd, tgcFitResult.tgcInnPhiNin);

      muonSA->setTgcMid1(tgcFitResult.tgcMid1[0], tgcFitResult.tgcMid1[1],
      		   tgcFitResult.tgcMid1[2], tgcFitResult.tgcMid1[3]);
      muonSA->setTgcMid2(tgcFitResult.tgcMid2[0], tgcFitResult.tgcMid2[1],
      		   tgcFitResult.tgcMid2[2], tgcFitResult.tgcMid2[3]);
      muonSA->setTgcMidF(tgcFitResult.tgcMidRhoChi2, tgcFitResult.tgcMidRhoNin,
      		   tgcFitResult.tgcMidPhiChi2, tgcFitResult.tgcMidPhiNin);
    }
  } else {
    // RPC fit results
    if (rpcFitResult.isSuccess ) {
      // Fill middle fit results for the moment

      muonSA->setRpcFitInn(rpcFitResult.phi_inner, rpcFitResult.slope_inner, rpcFitResult.offset_inner);
      muonSA->setRpcFitMid(rpcFitResult.phi_middle, rpcFitResult.slope_middle, rpcFitResult.offset_middle);
      muonSA->setRpcFitOut(rpcFitResult.phi_outer, rpcFitResult.slope_outer, rpcFitResult.offset_outer);
=======
    if (sc != StatusCode::SUCCESS) {
      ATH_MSG_WARNING("Track extrapolator failed");
       updateOutputTE(outputTE, inputTE, *p_roi, *p_roids, m_muonRoad, m_mdtRegion, m_rpcHits, m_tgcHits,
                      m_rpcFitResult, m_tgcFitResult, m_mdtHits_normal, m_cscHits, m_trackPatterns);
       return HLT::OK;
    }
    if (m_timerSvc) m_timers[ITIMER_TRACK_EXTRAPOLATOR]->pause();
    
    // Update monitoring variables
    sc = updateMonitor(*p_roi, m_mdtHits_normal, m_trackPatterns);
    if (sc != StatusCode::SUCCESS) {
      ATH_MSG_WARNING("Failed to update monitoring variables");
       updateOutputTE(outputTE, inputTE, *p_roi, *p_roids, m_muonRoad, m_mdtRegion, m_rpcHits, m_tgcHits,
                      m_rpcFitResult, m_tgcFitResult, m_mdtHits_normal, m_cscHits, m_trackPatterns);
       return HLT::OK;
>>>>>>> release/21.0.127
    }
  }

<<<<<<< HEAD
  // Store track positions if set of (R, Z, eta, phi) are all available
  if (pattern.s_address==-1) { // endcap

    // Inner
    if ( std::abs(pattern.superPoints[inner].R) > ZERO_LIMIT && std::abs(pattern.superPoints[inner].Z) > ZERO_LIMIT ) { // if R and Z exist
      if ( tgcFitResult.isSuccess && std::abs(tgcFitResult.tgcInn[3]) > ZERO_LIMIT ) { // if phi exist
        float theta = std::atan(pattern.superPoints[inner].R/std::abs(pattern.superPoints[inner].Z));
        float eta = (std::tan(theta/2.)!=0.)? -std::log(std::tan(theta/2.))*pattern.superPoints[inner].Z/std::abs(pattern.superPoints[inner].Z): 0.;
        muonSA->setTrackPosition( pattern.superPoints[inner].R, pattern.superPoints[inner].Z, eta, tgcFitResult.tgcInn[1] );
      }
    }
=======
    // Update output trigger element
    updateOutputTE(outputTE, inputTE, *p_roi, *p_roids, m_muonRoad, m_mdtRegion, m_rpcHits, m_tgcHits,
                   m_rpcFitResult, m_tgcFitResult, m_mdtHits_normal, m_cscHits, m_trackPatterns);
            

    // call the calibration streamer 
    if (m_doCalStream && m_trackPatterns.size()>0 ) { 
      TrigL2MuonSA::TrackPattern tp = m_trackPatterns[0];
      if (m_timerSvc) m_timers[ITIMER_CALIBRATION_STREAMER]->resume();                                                    
      //      m_calStreamer->setInstanceName(this->name());
      
      bool updateTriggerElement = false;
      sc = m_calStreamer->createRoiFragment(*p_roi,tp,m_mdtHits_normal,
					    m_rpcHits,
					    m_tgcHits,
					    m_calBufferSize,
					    m_calDataScouting,
					    updateTriggerElement); 

      if (sc != StatusCode::SUCCESS ) {  
        ATH_MSG_WARNING("Calibration streamer: create Roi Fragment failed");
      }
      if (m_timerSvc) m_timers[ITIMER_CALIBRATION_STREAMER]->pause(); 

      // if it's a data scouting chain check the buffer length
      if ( m_calDataScouting ) {
        
        if ( updateTriggerElement ) {
          
          ATH_MSG_INFO("Updating the trigger element");
          ATH_MSG_INFO(">> Retrieved the buffer, with size: " << m_calStreamer->getLocalBufferSize());
	  // create the TrigCompositeContainer to store the calibration buffer
	  m_trigCompositeContainer = new xAOD::TrigCompositeContainer();
	  xAOD::TrigCompositeAuxContainer aux;
	  m_trigCompositeContainer->setStore(&aux);

	  // add the trigcomposite object to the container
	  xAOD::TrigComposite* tc = new xAOD::TrigComposite();
	  m_trigCompositeContainer->push_back(tc);

	  ATH_MSG_DEBUG("The size of the TrigCompositeContainer is: " << m_trigCompositeContainer->size() );
	  	  
	  // set the detail of the trigcomposite object
	  //	  xAOD::TrigComposite* tc = m_trigCompositeContainer->at(0);
	  tc->setDetail("MuonCalibrationStream", *(m_calStreamer->getLocalBuffer()) );
	  
	  outputTE->setActiveState(true);
	  HLT::ErrorCode status = attachFeature( outputTE, m_trigCompositeContainer, "MuonCalibrationStream" );
	  if( status != HLT::OK ) {
	    ATH_MSG_ERROR("Record of MuonCalibrationStream in TriggerElement failed");
	    outputTE->setActiveState(false);
	    return false;
	  }
	  
	  m_calStreamer->clearLocalBuffer();
>>>>>>> release/21.0.127

    // Middle
    if ( std::abs(pattern.superPoints[middle].R) > ZERO_LIMIT && std::abs(pattern.superPoints[middle].Z) > ZERO_LIMIT ) { // if R and Z exist
      float phi = 0;
      if (tgcFitResult.isSuccess && ( std::abs(tgcFitResult.tgcMid1[3]) > ZERO_LIMIT || std::abs(tgcFitResult.tgcMid2[3]) > ZERO_LIMIT )) { // if phi exist
        double phi1 = tgcFitResult.tgcMid1[1];
        double phi2 = tgcFitResult.tgcMid2[1];
        if ( tgcFitResult.tgcMid1[3]==0. || tgcFitResult.tgcMid2[3]==0. ) {
          if ( std::abs(tgcFitResult.tgcMid1[3]) > ZERO_LIMIT ) phi = phi1;
          if ( std::abs(tgcFitResult.tgcMid2[3]) > ZERO_LIMIT ) phi = phi2;
        } else if( phi1*phi2 < 0 && std::abs(phi1)>(M_PI/2.) ) {
          double tmp1 = (phi1>0)? phi1 - M_PI : phi1 + M_PI;
          double tmp2 = (phi2>0)? phi2 - M_PI : phi2 + M_PI;
          double tmp  = (tmp1+tmp2)/2.;
          phi  = (tmp>0.)? tmp - M_PI : tmp + M_PI;
        } else {
          phi  = (phi2+phi1)/2.;
        }
      } else {
        phi = roi->phi();
      }
      float theta = std::atan(pattern.superPoints[middle].R/std::abs(pattern.superPoints[middle].Z));
      float eta = (std::tan(theta/2.)!=0.)? -std::log(std::tan(theta/2.))*pattern.superPoints[middle].Z/std::abs(pattern.superPoints[middle].Z): 0.;
      muonSA->setTrackPosition( pattern.superPoints[middle].R, pattern.superPoints[middle].Z, eta, phi );
    }

  } else { // barrel

    // Middle
    if ( std::abs(pattern.superPoints[middle].R) > ZERO_LIMIT && std::abs(pattern.superPoints[middle].Z) > ZERO_LIMIT ) { // if R and Z exist
      float phi = 0;
      if (rpcFitResult.isSuccess) {
        phi = rpcFitResult.phi;
      } else {
        phi = roi->phi();
      }
      float theta = std::atan(pattern.superPoints[middle].R/std::abs(pattern.superPoints[middle].Z));
      float eta = (std::tan(theta/2.)!=0.)? -std::log(std::tan(theta/2.))*pattern.superPoints[middle].Z/std::abs(pattern.superPoints[middle].Z): 0.;
      muonSA->setTrackPosition( pattern.superPoints[middle].R, pattern.superPoints[middle].Z, eta, phi );
    }

    // Not stored outer position for the moment as the phi is not available

  }
  outputTracks.push_back(muonSA);

  return true;
}

bool MuFastSteering::storeMuonSA(const xAOD::MuonRoI*                roi,
                                 const TrigRoiDescriptor*            roids,
             	                 const TrigL2MuonSA::MuonRoad&       muonRoad,
             	                 const TrigL2MuonSA::MdtRegion&      mdtRegion,
             	                 const TrigL2MuonSA::RpcHits&        rpcHits,
             	                 const TrigL2MuonSA::TgcHits&        tgcHits,
             	                 const TrigL2MuonSA::RpcFitResult&   rpcFitResult,
             	                 const TrigL2MuonSA::TgcFitResult&   tgcFitResult,
             	                 const TrigL2MuonSA::MdtHits&        mdtHits,
             	                 const TrigL2MuonSA::CscHits&        cscHits,
				 const TrigL2MuonSA::StgcHits&       stgcHits,
				 const TrigL2MuonSA::MmHits&         mmHits,
             	                 const TrigL2MuonSA::TrackPattern&   pattern,
                                 DataVector<xAOD::L2StandAloneMuon>& outputTracks ) const
{
  const float ZERO_LIMIT = 1.e-5;

  const int currentRoIId = roids->roiId();

<<<<<<< HEAD
  const EventContext& ctx = getContext();
  const EventIDBase& eventID = ctx.eventID();
  auto eventInfo = SG::makeHandle(m_eventInfoKey, ctx);
  if (!eventInfo.isValid()) {
    ATH_MSG_ERROR("Failed to retrieve xAOD::EventInfo object");
    return false;
  }

=======
  const double scalePhiWidthForFailure = 2;
  const double scaleRoIforZeroPt = 2;   
    
  const EventInfo* pEventInfo(0);
  StatusCode sc = m_storeGate->retrieve(pEventInfo);
  if (sc.isFailure()){
    ATH_MSG_FATAL("Can't get EventInfo object");
    return HLT::SG_ERROR;
  }
  
  const EventID* pEventId = pEventInfo->event_ID();
  if (pEventId==0) {
    ATH_MSG_ERROR("Could not find EventID object");
    return HLT::SG_ERROR;
  }
  
  const TriggerInfo* pTriggerInfo = pEventInfo->trigger_info();
  if (pTriggerInfo==0) {
    ATH_MSG_ERROR("Could not find TriggerInfo object");
    return HLT::SG_ERROR;
  }
  
>>>>>>> release/21.0.127
  int inner  = 0;
  int middle = 1;
  int outer  = 2;
  int ee     = 6;
  int csc    = 7;
  int barrelinner = 0;
  int endcapinner = 3;
  int bee = 8;
  int bme = 9;
  // int bmg = 10;

  // define inner, middle, outer
  if (pattern.s_address==-1) {
    inner  = xAOD::L2MuonParameters::Chamber::EndcapInner;
    middle = xAOD::L2MuonParameters::Chamber::EndcapMiddle;
    outer  = xAOD::L2MuonParameters::Chamber::EndcapOuter;
    ee     = xAOD::L2MuonParameters::Chamber::EndcapExtra;
    barrelinner     = xAOD::L2MuonParameters::Chamber::BarrelInner;
    bee = xAOD::L2MuonParameters::Chamber::BEE;
  } else {
    inner  = xAOD::L2MuonParameters::Chamber::BarrelInner;
    middle = xAOD::L2MuonParameters::Chamber::BarrelMiddle;
    outer  = xAOD::L2MuonParameters::Chamber::BarrelOuter;
    bme = xAOD::L2MuonParameters::Chamber::BME;
    endcapinner  = xAOD::L2MuonParameters::Chamber::EndcapInner;
  }

  ATH_MSG_DEBUG("### Hit patterns at the Muon Spectrometer ###");
  ATH_MSG_DEBUG("pattern#0: # of hits at inner  =" << pattern.mdtSegments[inner].size());
  ATH_MSG_DEBUG("pattern#0: # of hits at middle =" << pattern.mdtSegments[middle].size());
  ATH_MSG_DEBUG("pattern#0: # of hits at outer  =" << pattern.mdtSegments[outer].size());
  if (pattern.s_address==-1){
    ATH_MSG_DEBUG("pattern#0: # of hits at ee  =" << pattern.mdtSegments[ee].size());
    ATH_MSG_DEBUG("pattern#0: # of hits at endcap barrel inner  =" << pattern.mdtSegments[barrelinner].size());
    ATH_MSG_DEBUG("pattern#0: # of hits at BEE  =" << pattern.mdtSegments[bee].size());
  } else {
    ATH_MSG_DEBUG("pattern#0: # of hits at BME  =" << pattern.mdtSegments[bme].size());
    ATH_MSG_DEBUG("pattern#0: # of hits at barrel endcap inner  =" << pattern.mdtSegments[endcapinner].size());
  }
  ATH_MSG_DEBUG("### ************************************* ###");
  ATH_MSG_DEBUG("Estimated muon pt = " << pattern.pt << " GeV");

  // ---------
  // store xAOD

  xAOD::L2StandAloneMuon* muonSA = new xAOD::L2StandAloneMuon();
  muonSA->makePrivateStore();

  // add pT
  muonSA->setPt(pattern.pt*pattern.charge);
  muonSA->setPtEndcapAlpha(pattern.ptEndcapAlpha*pattern.charge);
  muonSA->setPtEndcapBeta(pattern.ptEndcapBeta*pattern.charge);
  muonSA->setPtEndcapRadius(pattern.ptEndcapRadius*pattern.charge);
  muonSA->setPtCSC(pattern.ptCSC*pattern.charge);

  muonSA->setEta(pattern.etaVtx);
  muonSA->setPhi(pattern.phiVtx);
  muonSA->setDeltaPt(pattern.deltaPt);
  muonSA->setDeltaEta(pattern.deltaEtaVtx);
  muonSA->setDeltaPhi(pattern.deltaPhiVtx);

  // add s_address
  muonSA->setSAddress(pattern.s_address);

  // add positions at MS
  muonSA->setEtaMS(pattern.etaMap);
  muonSA->setPhiMS(pattern.phiMS);
  muonSA->setDirPhiMS(pattern.phiMSDir);
  muonSA->setRMS(pattern.superPoints[inner].R);
  muonSA->setZMS(pattern.superPoints[inner].Z);
  muonSA->setDirZMS(pattern.superPoints[inner].Alin);

  // add pt variables
  // Endcap
  muonSA->setEndcapAlpha(pattern.endcapAlpha);
  muonSA->setEndcapBeta(pattern.endcapBeta);
  muonSA->setEndcapRadius(pattern.endcapRadius3P);
  // Barrel
  muonSA->setBarrelRadius(pattern.barrelRadius);
  muonSA->setBarrelSagitta(pattern.barrelSagitta);

  // store eta and phi used as argument to pT LUT
  muonSA->setEtaMap(pattern.etaMap);
  muonSA->setPhiMap(pattern.phiMap);
  muonSA->setEtaBin(pattern.etaBin);
  muonSA->setPhiBin(pattern.phiBin);

  // store TGC/RPC readout failure flags
  muonSA->setIsTgcFailure((int)pattern.isTgcFailure);
  muonSA->setIsRpcFailure((int)pattern.isRpcFailure);

  // add superpoints
  muonSA->setSuperPoint(inner, pattern.superPoints[inner].R, pattern.superPoints[inner].Z,
                        pattern.superPoints[inner].Alin, pattern.superPoints[inner].Blin, pattern.superPoints[inner].Chi2);
  muonSA->setSuperPoint(middle, pattern.superPoints[middle].R, pattern.superPoints[middle].Z,
                        pattern.superPoints[middle].Alin, pattern.superPoints[middle].Blin, pattern.superPoints[middle].Chi2);
  muonSA->setSuperPoint(outer, pattern.superPoints[outer].R, pattern.superPoints[outer].Z,
                        pattern.superPoints[outer].Alin, pattern.superPoints[outer].Blin, pattern.superPoints[outer].Chi2);
  if (pattern.s_address==-1){
    muonSA->setSuperPoint(ee, pattern.superPoints[ee].R, pattern.superPoints[ee].Z,
                          pattern.superPoints[ee].Alin, pattern.superPoints[ee].Blin, pattern.superPoints[ee].Chi2);
    muonSA->setSuperPoint(barrelinner, pattern.superPoints[barrelinner].R, pattern.superPoints[barrelinner].Z,
                          pattern.superPoints[barrelinner].Alin, pattern.superPoints[barrelinner].Blin, pattern.superPoints[barrelinner].Chi2);
    muonSA->setSuperPoint(csc, pattern.superPoints[csc].R, pattern.superPoints[csc].Z,
      		    pattern.superPoints[csc].Alin, pattern.superPoints[csc].Blin, pattern.superPoints[csc].Chi2);
  } else {
    muonSA->setSuperPoint(endcapinner, pattern.superPoints[endcapinner].R, pattern.superPoints[endcapinner].Z,
                          pattern.superPoints[endcapinner].Alin, pattern.superPoints[endcapinner].Blin, pattern.superPoints[endcapinner].Chi2);
  }

<<<<<<< HEAD
  ///////////////////////////////
  // Below are detailed information

  uint32_t muondetmask = 0;

  /// Set L2 muon algorithm ID
  muonSA->setAlgoId( L2MuonAlgoMap(name()) );
  /// Set input TE ID
  //muonSA->setTeId( inputTE->getId() );	// move to hltExecute()
  /// Set level-1 ID
  muonSA->setLvl1Id( eventInfo->extendedLevel1ID() );
  /// Set lumi block
  muonSA->setLumiBlock( eventID.lumi_block() );
  /// Set muon detector mask
  muonSA->setMuonDetMask( muondetmask );
  /// Set RoI ID
  muonSA->setRoiId( currentRoIId );
  /// Set RoI system ID (or system ID; Barrel=0, Endcap=1, Forward=2)
  muonSA->setRoiSystem( roi->getSource() );
  /// Set RoI subsystem ID (0=-z,1=+z)
  muonSA->setRoiSubsystem( 1 - roi->getHemisphere() );
  /// Set RoI sector ID
  muonSA->setRoiSector( roi->getSectorID() );
  /// Set RoI number
  muonSA->setRoiNumber( roi->getRoI() );
  /// Set RoI threshold number
  muonSA->setRoiThreshold( roi->getThrNumber() );
  /// Set RoI eta
  muonSA->setRoiEta( roi->eta() );
  /// Set RoIp phi
  muonSA->setRoiPhi( roi->phi() );
  /// Set RoI word
  muonSA->setRoIWord( roi->roiWord() );

  /// Set size of storages to be reserved
  muonSA->setRpcHitsCapacity( m_esd_rpc_size );
  muonSA->setTgcHitsCapacity( m_esd_tgc_size );
  muonSA->setMdtHitsCapacity( m_esd_mdt_size );
  muonSA->setCscHitsCapacity( m_esd_csc_size );
  muonSA->setStgcClustersCapacity( m_esd_stgc_size );
  muonSA->setMmClustersCapacity( m_esd_mm_size );

  // MDT hits
  std::vector<std::string> mdtId;
  for (const TrigL2MuonSA::MdtHitData& mdtHit : mdtHits) {
    if ( mdtHit.isOutlier==0 || mdtHit.isOutlier==1 ) {
      muonSA->setMdtHit(mdtHit.OnlineId, mdtHit.isOutlier, mdtHit.Chamber,
                        mdtHit.R, mdtHit.Z, mdtHit.cPhi0, mdtHit.Residual,
                        mdtHit.DriftTime, mdtHit.DriftSpace, mdtHit.DriftSigma);
      mdtId.push_back(mdtHit.Id.getString());
    }
  }
  SG::AuxElement::Accessor< std::vector<std::string> > accessor_mdthitid( "mdtHitId" );
  accessor_mdthitid( *muonSA ) = mdtId;

  //CSC hits
  std::vector<float> cscResol;
  for (const TrigL2MuonSA::CscHitData& cscHit : cscHits) {
    if ( 1/*cscHit.MeasuresPhi==0*/ ){
      if ( cscHit.isOutlier==0 || cscHit.isOutlier==1 ) {
        muonSA->setCscHit(cscHit.isOutlier, cscHit.Chamber, cscHit.StationName,
                          cscHit.StationEta, cscHit.StationPhi,
                          cscHit.ChamberLayer, cscHit.WireLayer, cscHit.MeasuresPhi, cscHit.Strip,
                          cscHit.eta, cscHit.phi, cscHit.r, cscHit.z,
                          cscHit.charge, cscHit.time, cscHit.Residual);
	cscResol.push_back(cscHit.resolution);
        ATH_MSG_VERBOSE("CSC Hits stored in xAOD: "
      		<< "OL=" << cscHit.isOutlier << ","
      		<< "Ch=" << cscHit.Chamber << ","
      		<< "StationName=" << cscHit.StationName << ","
      		<< "StationEta=" << cscHit.StationEta << ","
      		<< "StationPhi=" << cscHit.StationPhi << ","
      		<< "ChamberLayer=" << cscHit.ChamberLayer << ","
      		<< "WireLayer=" << cscHit.WireLayer << ","
      		<< "MeasuresPhi=" << cscHit.MeasuresPhi << ","
      		<< "Strip=" << cscHit.Strip << ","
      		<< "eta="  << cscHit.eta << ","
      		<< "phi="  << cscHit.phi << ","
      		<< "r="  << cscHit.r << ","
      		<< "z="  << cscHit.z << ","
      		<< "charge=" << cscHit.charge << ","
      		<< "Rs=" << cscHit.Residual << ","
      		<< "t="  << cscHit.time);
=======
  
  if( trackPatterns.size() > 0 ) {
    
    const TrigL2MuonSA::TrackPattern& pattern = trackPatterns[0]; 

    // define inner, middle, outer
    if (pattern.s_address==-1) {
      inner  = xAOD::L2MuonParameters::Chamber::EndcapInner;
      middle = xAOD::L2MuonParameters::Chamber::EndcapMiddle;
      outer  = xAOD::L2MuonParameters::Chamber::EndcapOuter;
      ee     = xAOD::L2MuonParameters::Chamber::EndcapExtra;
      barrelinner     = xAOD::L2MuonParameters::Chamber::BarrelInner;
      bee = xAOD::L2MuonParameters::Chamber::BEE;
    } else {
      inner  = xAOD::L2MuonParameters::Chamber::BarrelInner;
      middle = xAOD::L2MuonParameters::Chamber::BarrelMiddle;
      outer  = xAOD::L2MuonParameters::Chamber::BarrelOuter;
      bme = xAOD::L2MuonParameters::Chamber::BME;
      endcapinner  = xAOD::L2MuonParameters::Chamber::EndcapInner;
    }

    ATH_MSG_DEBUG("pattern#0: # of hits at inner  =" << pattern.mdtSegments[inner].size());
    ATH_MSG_DEBUG("pattern#0: # of hits at middle =" << pattern.mdtSegments[middle].size());
    ATH_MSG_DEBUG("pattern#0: # of hits at outer  =" << pattern.mdtSegments[outer].size());
    if (pattern.s_address==-1){
      ATH_MSG_DEBUG("pattern#0: # of hits at ee  =" << pattern.mdtSegments[ee].size());
      ATH_MSG_DEBUG("pattern#0: # of hits at endcap barrel inner  =" << pattern.mdtSegments[barrelinner].size());
      ATH_MSG_DEBUG("pattern#0: # of hits at BEE  =" << pattern.mdtSegments[bee].size());
    } else {
      ATH_MSG_DEBUG("pattern#0: # of hits at BME  =" << pattern.mdtSegments[bme].size());
      ATH_MSG_DEBUG("pattern#0: # of hits at barrel endcap inner  =" << pattern.mdtSegments[endcapinner].size());
    }
    ATH_MSG_DEBUG("pt=" << pattern.pt);

    // ---------
    // store xAOD

    xAOD::L2StandAloneMuon* muonSA = new xAOD::L2StandAloneMuon();
    muonSA->makePrivateStore();

    // add pT
    muonSA->setPt(pattern.pt*pattern.charge);
    muonSA->setPtEndcapAlpha(pattern.ptEndcapAlpha*pattern.charge);
    muonSA->setPtEndcapBeta(pattern.ptEndcapBeta*pattern.charge);
    muonSA->setPtEndcapRadius(pattern.ptEndcapRadius*pattern.charge);
    muonSA->setPtCSC(pattern.ptCSC*pattern.charge);

    muonSA->setEta(pattern.etaVtx);
    muonSA->setPhi(pattern.phiVtx);
    muonSA->setDeltaPt(pattern.deltaPt);
    muonSA->setDeltaEta(pattern.deltaEtaVtx);
    muonSA->setDeltaPhi(pattern.deltaPhiVtx);

    // add s_address
    muonSA->setSAddress(pattern.s_address);

    // add positions at MS
    muonSA->setEtaMS(pattern.etaMap);
    muonSA->setPhiMS(pattern.phiMS);
    muonSA->setDirPhiMS(pattern.phiMSDir);
    muonSA->setRMS(pattern.superPoints[inner].R);
    muonSA->setZMS(pattern.superPoints[inner].Z);
    muonSA->setDirZMS(pattern.superPoints[inner].Alin);

    // add pt variables
    // Endcap
    muonSA->setEndcapAlpha(pattern.endcapAlpha);
    muonSA->setEndcapBeta(pattern.endcapBeta);
    muonSA->setEndcapRadius(pattern.endcapRadius3P);
    // Barrel
    muonSA->setBarrelRadius(pattern.barrelRadius);
    muonSA->setBarrelSagitta(pattern.barrelSagitta);    
    
    // store eta and phi used as argument to pT LUT
    muonSA->setEtaMap(pattern.etaMap);
    muonSA->setPhiMap(pattern.phiMap);
    muonSA->setEtaBin(pattern.etaBin);
    muonSA->setPhiBin(pattern.phiBin);
    
    // store TGC/RPC readout failure flags
    muonSA->setIsTgcFailure((int)pattern.isTgcFailure);
    muonSA->setIsRpcFailure((int)pattern.isRpcFailure);

    // add superpoints
    muonSA->setSuperPoint(inner, pattern.superPoints[inner].R, pattern.superPoints[inner].Z,
                          pattern.superPoints[inner].Alin, pattern.superPoints[inner].Blin, pattern.superPoints[inner].Chi2);
    muonSA->setSuperPoint(middle, pattern.superPoints[middle].R, pattern.superPoints[middle].Z,
                          pattern.superPoints[middle].Alin, pattern.superPoints[middle].Blin, pattern.superPoints[middle].Chi2);
    muonSA->setSuperPoint(outer, pattern.superPoints[outer].R, pattern.superPoints[outer].Z,
                          pattern.superPoints[outer].Alin, pattern.superPoints[outer].Blin, pattern.superPoints[outer].Chi2);
    if (pattern.s_address==-1){
      muonSA->setSuperPoint(ee, pattern.superPoints[ee].R, pattern.superPoints[ee].Z,
                            pattern.superPoints[ee].Alin, pattern.superPoints[ee].Blin, pattern.superPoints[ee].Chi2);
      muonSA->setSuperPoint(barrelinner, pattern.superPoints[barrelinner].R, pattern.superPoints[barrelinner].Z,
                            pattern.superPoints[barrelinner].Alin, pattern.superPoints[barrelinner].Blin, pattern.superPoints[barrelinner].Chi2);
      muonSA->setSuperPoint(csc, pattern.superPoints[csc].R, pattern.superPoints[csc].Z,
			    pattern.superPoints[csc].Alin, pattern.superPoints[csc].Blin, pattern.superPoints[csc].Chi2);
    } else {
      muonSA->setSuperPoint(endcapinner, pattern.superPoints[endcapinner].R, pattern.superPoints[endcapinner].Z,
                            pattern.superPoints[endcapinner].Alin, pattern.superPoints[endcapinner].Blin, pattern.superPoints[endcapinner].Chi2);
    }

    ///////////////////////////////
    // Below are detailed information

    uint32_t muondetmask = 0;

    /// Set L2 muon algorithm ID
    muonSA->setAlgoId( L2MuonAlgoMap(name()) );
    /// Set input TE ID
    muonSA->setTeId( inputTE->getId() );
    /// Set level-1 ID
    muonSA->setLvl1Id( pTriggerInfo->extendedLevel1ID() );
    /// Set lumi block
    muonSA->setLumiBlock( pEventId->lumi_block() );
    /// Set muon detector mask
    muonSA->setMuonDetMask( muondetmask );
    /// Set RoI ID
    muonSA->setRoiId( currentRoIId );
    /// Set RoI system ID
    muonSA->setRoiSystem( roi->sysID() );
    /// Set RoI subsystem ID
    muonSA->setRoiSubsystem( roi->subsysID() );
    /// Set RoI sector ID
    muonSA->setRoiSector( roi->sectorID() );
    /// Set RoI number
    muonSA->setRoiNumber( roi->getRoINumber() );
    /// Set RoI threshold number
    muonSA->setRoiThreshold( roi->getThresholdNumber() );
    /// Set RoI eta
    muonSA->setRoiEta( roi->eta() );
    /// Set RoIp phi
    muonSA->setRoiPhi( roi->phi() );

    /// Set size of storages to be reserved
    muonSA->setRpcHitsCapacity( m_esd_rpc_size );
    muonSA->setTgcHitsCapacity( m_esd_tgc_size );
    muonSA->setMdtHitsCapacity( m_esd_mdt_size );
    muonSA->setCscHitsCapacity( m_esd_csc_size );

    // MDT hits
    for (unsigned int i_hit=0; i_hit<mdtHits.size(); i_hit++) {
      if ( mdtHits[i_hit].isOutlier==0 || mdtHits[i_hit].isOutlier==1 ) {
        muonSA->setMdtHit(mdtHits[i_hit].OnlineId, mdtHits[i_hit].isOutlier, mdtHits[i_hit].Chamber,
                          mdtHits[i_hit].R, mdtHits[i_hit].Z, mdtHits[i_hit].cPhi0, mdtHits[i_hit].Residual, 
                          mdtHits[i_hit].DriftTime, mdtHits[i_hit].DriftSpace, mdtHits[i_hit].DriftSigma);  
      }
    }
    
    //CSC hits
    for(unsigned int i_hit=0; i_hit<cscHits.size(); i_hit++) {
      if ( 1/*cscHits[i_hit].MeasuresPhi==0*/ ){
        if ( cscHits[i_hit].isOutlier==0 || cscHits[i_hit].isOutlier==1 ) {
          muonSA->setCscHit(cscHits[i_hit].isOutlier, cscHits[i_hit].Chamber, cscHits[i_hit].StationName,
                            cscHits[i_hit].StationEta, cscHits[i_hit].StationPhi,
                            cscHits[i_hit].ChamberLayer, cscHits[i_hit].WireLayer, cscHits[i_hit].MeasuresPhi, cscHits[i_hit].Strip,
                            cscHits[i_hit].eta, cscHits[i_hit].phi, cscHits[i_hit].r, cscHits[i_hit].z,
                            cscHits[i_hit].charge, cscHits[i_hit].time, cscHits[i_hit].Residual);
          ATH_MSG_DEBUG("CSC Hits stored in xAOD: "
			<< "OL=" << cscHits[i_hit].isOutlier << ","
			<< "Ch=" << cscHits[i_hit].Chamber << ","
			<< "StationName=" << cscHits[i_hit].StationName << ","
			<< "StationEta=" << cscHits[i_hit].StationEta << ","
			<< "StationPhi=" << cscHits[i_hit].StationPhi << ","
			<< "ChamberLayer=" << cscHits[i_hit].ChamberLayer << ","
			<< "WireLayer=" << cscHits[i_hit].WireLayer << ","
			<< "MeasuresPhi=" << cscHits[i_hit].MeasuresPhi << ","
			<< "Strip=" << cscHits[i_hit].Strip << ","
			<< "eta="  << cscHits[i_hit].eta << ","
			<< "phi="  << cscHits[i_hit].phi << ","
			<< "r="  << cscHits[i_hit].r << ","
			<< "z="  << cscHits[i_hit].z << ","
			<< "charge=" << cscHits[i_hit].charge << ","
			<< "Rs=" << cscHits[i_hit].Residual << ","
			<< "t="  << cscHits[i_hit].time);
        }
>>>>>>> release/21.0.127
      }
    }
  }
  SG::AuxElement::Accessor< std::vector<float> > accessor_cschitresol( "cscHitResolution" );
  accessor_cschitresol( *muonSA ) = cscResol;

  // RPC hits
  float sumbeta[8]={0};
  float nhit_layer[8]={0};
  for (const TrigL2MuonSA::RpcHitData& rpcHit : rpcHits) {
    muonSA->setRpcHit(rpcHit.layer, rpcHit.measuresPhi,
                      rpcHit.x, rpcHit.y, rpcHit.z,
                      rpcHit.time, rpcHit.distToEtaReadout, rpcHit.distToPhiReadout,
                      rpcHit.stationName);
    ATH_MSG_VERBOSE("RPC hits stored in xAOD: "
      	    << "stationName=" << rpcHit.stationName << ","
      	    << "layer=" << rpcHit.layer << ","
      	    << "measuresPhi=" << rpcHit.measuresPhi << ","
      	    << "x=" << rpcHit.x << ","
      	    << "y=" << rpcHit.y << ","
      	    << "y=" << rpcHit.z);

    float dRMS = std::sqrt( std::abs(pattern.etaMap-rpcHit.eta)*std::abs(pattern.etaMap-rpcHit.eta) + std::acos(std::cos(pattern.phiMS-rpcHit.phi))*std::acos(std::cos(pattern.phiMS-rpcHit.phi)) );
    if(dRMS>0.05) continue;
    float muToF = rpcHit.l/1000/(CLHEP::c_light/1000);
    float Tprop = rpcHit.distToPhiReadout/1000*4.8;
    float beta = rpcHit.l/1000/(muToF+rpcHit.time-Tprop+3.125/2)/(CLHEP::c_light/1000);
    sumbeta[rpcHit.layer]=sumbeta[rpcHit.layer]+beta;
    nhit_layer[rpcHit.layer]=nhit_layer[rpcHit.layer]+1;
  }

<<<<<<< HEAD
  std::vector<float> Avebeta_layer;
  for(int i_layer=0;i_layer<8;i_layer++){
    if(nhit_layer[i_layer]!=0)Avebeta_layer.push_back( sumbeta[i_layer]/nhit_layer[i_layer] );
  }
  if(Avebeta_layer.size()>0) muonSA->setBeta( std::accumulate(Avebeta_layer.begin(),Avebeta_layer.end(),0.0)/Avebeta_layer.size() );
  else muonSA->setBeta( 9999 );
  Avebeta_layer.clear();

  // TGC hits
  for (const TrigL2MuonSA::TgcHitData& tgcHit : tgcHits) {
    muonSA->setTgcHit(tgcHit.eta, tgcHit.phi, tgcHit.r, tgcHit.z,
                      tgcHit.width, tgcHit.sta, tgcHit.isStrip,
                      tgcHit.bcTag, tgcHit.inRoad);
    ATH_MSG_VERBOSE("TGC hits stored in xAOD: "
      	    << "eta=" << tgcHit.eta << ","
      	    << "phi=" << tgcHit.phi << ","
      	    << "r=" << tgcHit.r << ","
      	    << "z=" << tgcHit.z << ","
      	    << "width=" << tgcHit.width << ","
      	    << "stationNum=" << tgcHit.sta << ","
      	    << "isStrip=" << tgcHit.isStrip << ","
      	    << "bcTag=" << tgcHit.bcTag << ","
      	    << "inRoad=" << tgcHit.inRoad);
  }


  // sTGC clusters
  for(unsigned int i_hit=0; i_hit<stgcHits.size(); i_hit++) {
    if ( stgcHits[i_hit].isOutlier==0 || stgcHits[i_hit].isOutlier==1 ) {


      muonSA->setStgcCluster(stgcHits[i_hit].layerNumber, stgcHits[i_hit].isOutlier, stgcHits[i_hit].channelType,
      			     stgcHits[i_hit].eta, stgcHits[i_hit].phi, stgcHits[i_hit].r, stgcHits[i_hit].z,
      			     stgcHits[i_hit].ResidualR, stgcHits[i_hit].ResidualPhi,
      			     stgcHits[i_hit].stationEta, stgcHits[i_hit].stationPhi, stgcHits[i_hit].stationName);

      ATH_MSG_VERBOSE("sTGC hits stored in xAOD: "
		      << "eta=" << stgcHits[i_hit].eta << ","
		      << "phi=" << stgcHits[i_hit].phi << ","
		      << "r=" << stgcHits[i_hit].r << ","
		      << "z=" << stgcHits[i_hit].z << ","
		      << "z=" << stgcHits[i_hit].ResidualR << ","
		      << "z=" << stgcHits[i_hit].ResidualPhi);
=======
    // RPC hits
    float sumbeta[8]={0};
    float nhit_layer[8]={0};
    for(unsigned int i_hit=0; i_hit<rpcHits.size(); i_hit++) {
      muonSA->setRpcHit(rpcHits[i_hit].layer, rpcHits[i_hit].measuresPhi, 
                        rpcHits[i_hit].x, rpcHits[i_hit].y, rpcHits[i_hit].z,
                        rpcHits[i_hit].time, rpcHits[i_hit].distToEtaReadout, rpcHits[i_hit].distToPhiReadout,
                        rpcHits[i_hit].stationName);
      ATH_MSG_DEBUG("RPC hits stored in xAOD: "
		    << "stationName=" << rpcHits[i_hit].stationName << ","
		    << "layer=" << rpcHits[i_hit].layer << ","
		    << "measuresPhi=" << rpcHits[i_hit].measuresPhi << ","
		    << "x=" << rpcHits[i_hit].x << ","
		    << "y=" << rpcHits[i_hit].y << ","
		    << "y=" << rpcHits[i_hit].z);
      
      float dRMS = sqrt( fabs(pattern.etaMap-rpcHits[i_hit].eta)*fabs(pattern.etaMap-rpcHits[i_hit].eta) + acos(cos(pattern.phiMS-rpcHits[i_hit].phi))*acos(cos(pattern.phiMS-rpcHits[i_hit].phi)) );
      if(dRMS>0.05) continue;
      float muToF = rpcHits[i_hit].l/1000/(CLHEP::c_light/1000);
      float Tprop = rpcHits[i_hit].distToPhiReadout/1000*4.8;
      float beta = rpcHits[i_hit].l/1000/(muToF+rpcHits[i_hit].time-Tprop+3.125/2)/(CLHEP::c_light/1000);
      sumbeta[rpcHits[i_hit].layer]=sumbeta[rpcHits[i_hit].layer]+beta;
      nhit_layer[rpcHits[i_hit].layer]=nhit_layer[rpcHits[i_hit].layer]+1;
    }

    std::vector<float> Avebeta_layer;
    for(int i_layer=0;i_layer<8;i_layer++){
      if(nhit_layer[i_layer]!=0)Avebeta_layer.push_back( sumbeta[i_layer]/nhit_layer[i_layer] );
    }
    if(Avebeta_layer.size()>0) muonSA->setBeta( std::accumulate(Avebeta_layer.begin(),Avebeta_layer.end(),0.0)/Avebeta_layer.size() );
    else muonSA->setBeta( 9999 );
    Avebeta_layer.clear();
    
    // TGC hits
    for(unsigned int i_hit=0; i_hit<tgcHits.size(); i_hit++) {
      muonSA->setTgcHit(tgcHits[i_hit].eta, tgcHits[i_hit].phi, tgcHits[i_hit].r, tgcHits[i_hit].z,
                        tgcHits[i_hit].width, tgcHits[i_hit].sta, tgcHits[i_hit].isStrip,
                        tgcHits[i_hit].bcTag, tgcHits[i_hit].inRoad);
      ATH_MSG_DEBUG("TGC hits stored in xAOD: "
		    << "eta=" << tgcHits[i_hit].eta << ","
		    << "phi=" << tgcHits[i_hit].phi << ","
		    << "r=" << tgcHits[i_hit].r << ","
		    << "z=" << tgcHits[i_hit].z << ","
		    << "width=" << tgcHits[i_hit].width << ","
		    << "stationNum=" << tgcHits[i_hit].sta << ","
		    << "isStrip=" << tgcHits[i_hit].isStrip << ","
		    << "bcTag=" << tgcHits[i_hit].bcTag << ","
		    << "inRoad=" << tgcHits[i_hit].inRoad);
    }

    // Muon road
    for (int i_station=0; i_station<8; i_station++) {
      for (int i_sector=0; i_sector<2; i_sector++) {
        muonSA->setRoad(i_station, i_sector, muonRoad.aw[i_station][i_sector], muonRoad.bw[i_station][i_sector]);
        muonSA->setRegionZ(i_station, i_sector, mdtRegion.zMin[i_station][i_sector], mdtRegion.zMax[i_station][i_sector]);
        muonSA->setRegionR(i_station, i_sector, mdtRegion.rMin[i_station][i_sector], mdtRegion.rMax[i_station][i_sector]);
        muonSA->setRegionEta(i_station, i_sector, mdtRegion.etaMin[i_station][i_sector], mdtRegion.etaMax[i_station][i_sector]);
        muonSA->setChamberType1(i_station, i_sector, mdtRegion.chamberType[i_station][i_sector][0]);
        muonSA->setChamberType2(i_station, i_sector, mdtRegion.chamberType[i_station][i_sector][1]);
      }
>>>>>>> release/21.0.127
    }
  }

  // MM clusters
  for(unsigned int i_hit=0; i_hit<mmHits.size(); i_hit++) {
    if ( mmHits[i_hit].isOutlier==0 || mmHits[i_hit].isOutlier==1 ) {


      muonSA->setMmCluster(mmHits[i_hit].layerNumber, mmHits[i_hit].isOutlier,
      			   mmHits[i_hit].eta, mmHits[i_hit].phi, mmHits[i_hit].r, mmHits[i_hit].z,
      			   mmHits[i_hit].ResidualR, mmHits[i_hit].ResidualPhi,
      			   mmHits[i_hit].stationEta, mmHits[i_hit].stationPhi, mmHits[i_hit].stationName);

      ATH_MSG_VERBOSE("mm hits stored in xAOD: "
		      << "eta=" << tgcHits[i_hit].eta << ","
		      << "phi=" << tgcHits[i_hit].phi << ","
		      << "r=" << tgcHits[i_hit].r << ","
		      << "z=" << tgcHits[i_hit].z << ","
		      << "width=" << tgcHits[i_hit].width << ","
		      << "stationNum=" << tgcHits[i_hit].sta << ","
		      << "isStrip=" << tgcHits[i_hit].isStrip << ","
		      << "bcTag=" << tgcHits[i_hit].bcTag << ","
		      << "inRoad=" << tgcHits[i_hit].inRoad);
    }
  }




  // Muon road
  for (int i_station=0; i_station<8; i_station++) {
    for (int i_sector=0; i_sector<2; i_sector++) {
      muonSA->setRoad(i_station, i_sector, muonRoad.aw[i_station][i_sector], muonRoad.bw[i_station][i_sector]);
      muonSA->setRegionZ(i_station, i_sector, mdtRegion.zMin[i_station][i_sector], mdtRegion.zMax[i_station][i_sector]);
      muonSA->setRegionR(i_station, i_sector, mdtRegion.rMin[i_station][i_sector], mdtRegion.rMax[i_station][i_sector]);
      muonSA->setRegionEta(i_station, i_sector, mdtRegion.etaMin[i_station][i_sector], mdtRegion.etaMax[i_station][i_sector]);
      muonSA->setChamberType1(i_station, i_sector, mdtRegion.chamberType[i_station][i_sector][0]);
      muonSA->setChamberType2(i_station, i_sector, mdtRegion.chamberType[i_station][i_sector][1]);
    }
  }

  if ( muonRoad.isEndcap ) {
    // TGC fit results
    if (tgcFitResult.isSuccess ) {
      muonSA->setTgcPt(tgcFitResult.tgcPT);

      muonSA->setTgcInn(tgcFitResult.tgcInn[0], tgcFitResult.tgcInn[1],
      		  tgcFitResult.tgcInn[2], tgcFitResult.tgcInn[3]);
      muonSA->setTgcInnF(tgcFitResult.tgcInnRhoStd, tgcFitResult.tgcInnRhoNin,
      		   tgcFitResult.tgcInnPhiStd, tgcFitResult.tgcInnPhiNin);

      muonSA->setTgcMid1(tgcFitResult.tgcMid1[0], tgcFitResult.tgcMid1[1],
      		   tgcFitResult.tgcMid1[2], tgcFitResult.tgcMid1[3]);
      muonSA->setTgcMid2(tgcFitResult.tgcMid2[0], tgcFitResult.tgcMid2[1],
      		   tgcFitResult.tgcMid2[2], tgcFitResult.tgcMid2[3]);
      muonSA->setTgcMidF(tgcFitResult.tgcMidRhoChi2, tgcFitResult.tgcMidRhoNin,
      		   tgcFitResult.tgcMidPhiChi2, tgcFitResult.tgcMidPhiNin);
    }
  } else {
    // RPC fit results
    if (rpcFitResult.isSuccess ) {
      // Fill middle fit results for the moment

      muonSA->setRpcFitInn(rpcFitResult.phi_inner, rpcFitResult.slope_inner, rpcFitResult.offset_inner);
      muonSA->setRpcFitMid(rpcFitResult.phi_middle, rpcFitResult.slope_middle, rpcFitResult.offset_middle);
      muonSA->setRpcFitOut(rpcFitResult.phi_outer, rpcFitResult.slope_outer, rpcFitResult.offset_outer);
    }
  }

  // Store track positions if set of (R, Z, eta, phi) are all available
  if (pattern.s_address==-1) { // endcap

    // Inner
    if ( std::abs(pattern.superPoints[inner].R) > ZERO_LIMIT && std::abs(pattern.superPoints[inner].Z) > ZERO_LIMIT ) { // if R and Z exist
      if ( tgcFitResult.isSuccess && std::abs(tgcFitResult.tgcInn[3]) > ZERO_LIMIT ) { // if phi exist
        float theta = std::atan(pattern.superPoints[inner].R/std::abs(pattern.superPoints[inner].Z));
        float eta = (std::tan(theta/2.)!=0.)? -std::log(std::tan(theta/2.))*pattern.superPoints[inner].Z/std::abs(pattern.superPoints[inner].Z): 0.;
        muonSA->setTrackPosition( pattern.superPoints[inner].R, pattern.superPoints[inner].Z, eta, tgcFitResult.tgcInn[1] );
      }
    }

    // Middle
    if ( std::abs(pattern.superPoints[middle].R) > ZERO_LIMIT && std::abs(pattern.superPoints[middle].Z) > ZERO_LIMIT ) { // if R and Z exist
      float phi = 0;
      if (tgcFitResult.isSuccess && ( std::abs(tgcFitResult.tgcMid1[3]) > ZERO_LIMIT || std::abs(tgcFitResult.tgcMid2[3]) > ZERO_LIMIT )) { // if phi exist
        double phi1 = tgcFitResult.tgcMid1[1];
        double phi2 = tgcFitResult.tgcMid2[1];
        if ( tgcFitResult.tgcMid1[3]==0. || tgcFitResult.tgcMid2[3]==0. ) {
          if ( std::abs(tgcFitResult.tgcMid1[3]) > ZERO_LIMIT ) phi = phi1;
          if ( std::abs(tgcFitResult.tgcMid2[3]) > ZERO_LIMIT ) phi = phi2;
        } else if( phi1*phi2 < 0 && std::abs(phi1)>(M_PI/2.) ) {
          double tmp1 = (phi1>0)? phi1 - M_PI : phi1 + M_PI;
          double tmp2 = (phi2>0)? phi2 - M_PI : phi2 + M_PI;
          double tmp  = (tmp1+tmp2)/2.;
          phi  = (tmp>0.)? tmp - M_PI : tmp + M_PI;
        } else {
          phi  = (phi2+phi1)/2.;
        }
      } else {
        phi = roi->phi();
      }
      float theta = std::atan(pattern.superPoints[middle].R/std::abs(pattern.superPoints[middle].Z));
      float eta = (std::tan(theta/2.)!=0.)? -std::log(std::tan(theta/2.))*pattern.superPoints[middle].Z/std::abs(pattern.superPoints[middle].Z): 0.;
      muonSA->setTrackPosition( pattern.superPoints[middle].R, pattern.superPoints[middle].Z, eta, phi );
    }

  } else { // barrel

    // Middle
    if ( std::abs(pattern.superPoints[middle].R) > ZERO_LIMIT && std::abs(pattern.superPoints[middle].Z) > ZERO_LIMIT ) { // if R and Z exist
      float phi = 0;
      if (rpcFitResult.isSuccess) {
        phi = rpcFitResult.phi;
      } else {
        phi = roi->phi();
      }
      float theta = std::atan(pattern.superPoints[middle].R/std::abs(pattern.superPoints[middle].Z));
      float eta = (std::tan(theta/2.)!=0.)? -std::log(std::tan(theta/2.))*pattern.superPoints[middle].Z/std::abs(pattern.superPoints[middle].Z): 0.;
      muonSA->setTrackPosition( pattern.superPoints[middle].R, pattern.superPoints[middle].Z, eta, phi );
    }

<<<<<<< HEAD
    // Not stored outer position for the moment as the phi is not available
=======
      // Not stored outer position for the moment as the phi is not available

    }
  
    muonColl->push_back(muonSA);

    // -------
    // store TrigRoiDescriptor
    if (fabs(muonSA->pt()) > ZERO_LIMIT ) {

      TrigRoiDescriptor* MSroiDescriptor = new TrigRoiDescriptor(roids->l1Id(),
                                                                 roids->roiId(),
                                                                 pattern.etaMap,
                                                                 pattern.etaMap,
                                                                 pattern.etaMap,
                                                                 pattern.phiMS,
                                                                 pattern.phiMS,
                                                                 pattern.phiMS);
      ATH_MSG_DEBUG("...TrigRoiDescriptor for MS "
		    << "pattern.etaMap/pattern.phiMS="
		    << pattern.etaMap << "/" << pattern.phiMS);

      // patch for the ID RoI descriptor
      float phiHalfWidth = 0.1;
      float etaHalfWidth = 0.1;

      // 2010 runs
      //      if ( std::fabs(pattern.etaVtx)>1 && std::fabs(pattern.etaVtx)<1.5 ) {
      //        phiHalfWidth = 0.25;
      //        etaHalfWidth = 0.4;
      //      } else {
      //        phiHalfWidth = 0.1;
      //        etaHalfWidth = 0.15;
      //      }
      
      // 2011a tuning
      phiHalfWidth = getRoiSizeForID(false,muonSA);
      etaHalfWidth = getRoiSizeForID(true, muonSA);

      if (pattern.isTgcFailure || pattern.isRpcFailure) 
        phiHalfWidth *= scalePhiWidthForFailure;

      TrigRoiDescriptor* IDroiDescriptor = new TrigRoiDescriptor(roids->l1Id(),
                                                                 roids->roiId(),
                                                                 pattern.etaVtx,
                                                                 pattern.etaVtx - etaHalfWidth,
                                                                 pattern.etaVtx + etaHalfWidth,
                                                                 pattern.phiVtx,
                                                                 pattern.phiVtx - phiHalfWidth,
                                                                 pattern.phiVtx + phiHalfWidth);
      ATH_MSG_DEBUG("...TrigRoiDescriptor for ID "
		    << "pattern.etaVtx/pattern.phiVtx="
		    << pattern.etaVtx << "/" << pattern.phiVtx);
      ATH_MSG_DEBUG("old RoI:        " << *roids);
      ATH_MSG_DEBUG("updated ID RoI: " << *IDroiDescriptor);      

      HLT::ErrorCode attached;

      // attach roi descriptor for TrigMoore
      attached = attachFeature(outputTE, MSroiDescriptor, "forMS");

      if ( attached!=HLT::OK) {

        ATH_MSG_WARNING ("Could not attach the roi descriptor for TrigMoore.");
>>>>>>> release/21.0.127

  }
  outputTracks.push_back(muonSA);

<<<<<<< HEAD
  return true;
}
=======
	ATH_MSG_DEBUG("Recorded an RoiDescriptor for TrigMoore:"
		      << " phi=" << MSroiDescriptor->phi()
		      << ",  eta=" << MSroiDescriptor->eta());
      }
>>>>>>> release/21.0.127


bool MuFastSteering::storeMSRoiDescriptor(const TrigRoiDescriptor*                  roids,
             	                          const TrigL2MuonSA::TrackPattern&         pattern,
                                          const DataVector<xAOD::L2StandAloneMuon>& outputTracks,
		                          TrigRoiDescriptorCollection&	 	    outputMS) const
{
  const float ZERO_LIMIT = 1.e-5;

<<<<<<< HEAD
  const xAOD::L2StandAloneMuon* muonSA = outputTracks[0];
=======
        ATH_MSG_WARNING("Could not attach the roi descriptor for Inner Detector.");
>>>>>>> release/21.0.127

  float mseta = pattern.etaMap;
  float msphi = pattern.phiMS;

<<<<<<< HEAD
  // store TrigRoiDescriptor
  if (std::abs(muonSA->pt()) < ZERO_LIMIT ) {
    mseta = roids->eta();
    msphi = roids->phi();
  }

  // set width of 0.1 so that ID tracking monitoring works
  const float phiHalfWidth = 0.1;
  const float etaHalfWidth = 0.1;
=======
	ATH_MSG_DEBUG("Recorded an RoiDescriptor for Inner Detector:"
		      << " phi=" << IDroiDescriptor->phi()
		      << ",  eta=" << IDroiDescriptor->eta());

      }
    } else { // pt = 0.
      
      TrigRoiDescriptor* IDroiDescriptor = new TrigRoiDescriptor(roids->l1Id(),
                                                                 roids->roiId(),
                                                                 roids->eta(),
                                                                 roids->eta() - (roids->eta() - roids->etaMinus()) * scaleRoIforZeroPt,
                                                                 roids->eta() + (roids->etaPlus() - roids->eta()) * scaleRoIforZeroPt,
                                                                 roids->phi(),
                                                                 HLT::wrapPhi(roids->phi() - HLT::wrapPhi(roids->phiPlus() - roids->phiMinus())/2. * scaleRoIforZeroPt),
                                                                 HLT::wrapPhi(roids->phi() + HLT::wrapPhi(roids->phiPlus() - roids->phiMinus())/2. * scaleRoIforZeroPt));
      ATH_MSG_DEBUG("...TrigRoiDescriptor for ID (zero pT) ");
>>>>>>> release/21.0.127

  TrigRoiDescriptor* MSroiDescriptor = new TrigRoiDescriptor(roids->roiWord(),
							     roids->l1Id(),
							     roids->roiId(),
							     mseta,
							     mseta - etaHalfWidth,
							     mseta + etaHalfWidth,
							     msphi,
							     msphi - phiHalfWidth,
							     msphi + phiHalfWidth);

  ATH_MSG_VERBOSE("...TrigRoiDescriptor for MS "
		  << "mseta/msphi="
		  << mseta << "/" << msphi);

  ATH_MSG_VERBOSE("will Record an RoiDescriptor for TrigMoore:"
		  << " phi=" << MSroiDescriptor->phi()
		  << ",  eta=" << MSroiDescriptor->eta());

<<<<<<< HEAD
  outputMS.push_back(MSroiDescriptor);
=======
        ATH_MSG_WARNING ("Could not attach the roi descriptor for Inner Detector.");
>>>>>>> release/21.0.127

  return true;
}

<<<<<<< HEAD

bool MuFastSteering::storeIDRoiDescriptor(const TrigRoiDescriptor*                  roids,
		                          const TrigL2MuonSA::TrackPattern&         pattern,
                                          const DataVector<xAOD::L2StandAloneMuon>& outputTracks,
		                          TrigRoiDescriptorCollection&	 	    outputID) const
{

  if (m_fill_FSIDRoI) {  // this mode will be used in cosmic run, if ID expert want to run full scan FTF.
    TrigRoiDescriptor* IDroiDescriptor = new TrigRoiDescriptor(true);
    outputID.push_back(IDroiDescriptor);
    return true;
  }

  const float ZERO_LIMIT = 1.e-5;

  const double scalePhiWidthForFailure = 2;
  const double scaleRoIforZeroPt = 2;

  const xAOD::L2StandAloneMuon* muonSA = outputTracks[0];

  // store TrigRoiDescriptor
  if (std::abs(muonSA->pt()) > ZERO_LIMIT ) {

    // patch for the ID RoI descriptor
    float phiHalfWidth = 0.1;
    float etaHalfWidth = 0.1;

    // 2010 runs
    //      if ( std::abs(pattern.etaVtx)>1 && std::abs(pattern.etaVtx)<1.5 ) {
    //        phiHalfWidth = 0.25;
    //        etaHalfWidth = 0.4;
    //      } else {
    //        phiHalfWidth = 0.1;
    //        etaHalfWidth = 0.15;
    //      }

    // 2011a tuning
    phiHalfWidth = getRoiSizeForID(false,muonSA);
    etaHalfWidth = getRoiSizeForID(true, muonSA);

    if (pattern.isTgcFailure || pattern.isRpcFailure)
      phiHalfWidth *= scalePhiWidthForFailure;

    TrigRoiDescriptor* IDroiDescriptor = new TrigRoiDescriptor(roids->roiWord(),
                                                               roids->l1Id(),
                                                               roids->roiId(),
                                                               pattern.etaVtx,
                                                               pattern.etaVtx - etaHalfWidth,
                                                               pattern.etaVtx + etaHalfWidth,
                                                               pattern.phiVtx,
                                                               pattern.phiVtx - phiHalfWidth,
                                                               pattern.phiVtx + phiHalfWidth);

    ATH_MSG_VERBOSE("...TrigRoiDescriptor for ID "
      	    << "pattern.etaVtx/pattern.phiVtx="
      	    << pattern.etaVtx << "/" << pattern.phiVtx);

    ATH_MSG_VERBOSE("will Record an RoiDescriptor for Inner Detector:"
                  << " phi=" << IDroiDescriptor->phi()
                  << ",  eta=" << IDroiDescriptor->eta());

    outputID.push_back(IDroiDescriptor);

 } else { // pt = 0.

    TrigRoiDescriptor* IDroiDescriptor = new TrigRoiDescriptor(roids->roiWord(),
                                                               roids->l1Id(),
                                                               roids->roiId(),
                                                               roids->eta(),
                                                               roids->eta() - (roids->eta() - roids->etaMinus()) * scaleRoIforZeroPt,
                                                               roids->eta() + (roids->etaPlus() - roids->eta()) * scaleRoIforZeroPt,
                                                               roids->phi(),
                                                               CxxUtils::wrapToPi(roids->phi() - CxxUtils::wrapToPi(roids->phiPlus() - roids->phiMinus())/2. * scaleRoIforZeroPt),
                                                               CxxUtils::wrapToPi(roids->phi() + CxxUtils::wrapToPi(roids->phiPlus() - roids->phiMinus())/2. * scaleRoIforZeroPt));

    ATH_MSG_VERBOSE("will Record an RoiDescriptor for Inner Detector in case with zero pT:"
      	     << " phi=" << IDroiDescriptor->phi()
      	     << ", phi min=" << IDroiDescriptor->phiMinus()
      	     << ", phi max=" << IDroiDescriptor->phiPlus()
      	     << ", eta=" << IDroiDescriptor->eta()
      	     << ", eta min=" << IDroiDescriptor->etaMinus()
      	     << ", eta max=" << IDroiDescriptor->etaPlus());

    outputID.push_back(IDroiDescriptor);
=======
	ATH_MSG_DEBUG ("Recorded an RoiDescriptor for Inner Detector in case with zero pT:"
		       << " phi=" << IDroiDescriptor->phi()
		       << ", phi min=" << IDroiDescriptor->phiMinus()
		       << ", phi max=" << IDroiDescriptor->phiPlus()
		       << ", eta=" << IDroiDescriptor->eta()
		       << ", eta min=" << IDroiDescriptor->etaMinus()
		       << ", eta max=" << IDroiDescriptor->etaPlus());
      }
    }
  }

  if (muonColl != 0 && muonColl->size() > 0) {
    outputTE->setActiveState(true);
    HLT::ErrorCode status = attachFeature( outputTE, muonColl, muonCollKey );
    if( status != HLT::OK ) {
      ATH_MSG_ERROR("Record of L2StandAloneMuon in TriggerElement failed");
      outputTE->setActiveState(false);
      delete muonColl;
      return false;
    }
>>>>>>> release/21.0.127
  }

  return true;
}


// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

int MuFastSteering::L2MuonAlgoMap(const std::string& name) const
{
  int algoId = 0;
  if (name == "MuFastSteering_Muon")  {
    algoId = xAOD::L2MuonParameters::L2MuonAlgoId::MUONID;
  } else if (name == "MuFastSteering_900GeV")  {
    algoId = xAOD::L2MuonParameters::L2MuonAlgoId::GEV900ID;
  } else {
    algoId = xAOD::L2MuonParameters::L2MuonAlgoId::NULLID;
  }

  return algoId;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

float MuFastSteering::getRoiSizeForID(bool isEta, const xAOD::L2StandAloneMuon* muonSA) const
{
   bool  isBarrel = (muonSA->sAddress()==-1) ? true : false;
   float eta = muonSA->etaMS();
   float phi = muonSA->phiMS();
   float pt  = muonSA->pt();

   //
   const int N_PARAMS = 2;

   //
   const float etaMinWin_brl = 0.10;
   const float etaMinWin_ec1 = 0.10;
   const float etaMinWin_ec2 = 0.10;
   const float etaMinWin_ec3 = 0.10;
   const float etaMinWin_ecA = 0.10;
   const float etaMinWin_ecB = 0.10;

   const float etaMaxWin_brl = 0.20;
   const float etaMaxWin_ec1 = 0.20;
   const float etaMaxWin_ec2 = 0.20;
   const float etaMaxWin_ec3 = 0.20;
   const float etaMaxWin_ecA = 0.20;
   const float etaMaxWin_ecB = 0.20;

   const float etaParams_brl[N_PARAMS] = { 0.038,  0.284};
   const float etaParams_ec1[N_PARAMS] = { 0.011,  0.519};
   const float etaParams_ec2[N_PARAMS] = { 0.023,  0.253};
   const float etaParams_ec3[N_PARAMS] = { 0.018,  0.519};
   const float etaParams_ecA[N_PARAMS] = { 0.010,  0.431};
   const float etaParams_ecB[N_PARAMS] = { 0.023,  0.236};

   //
   const float phiMinWin_brl = 0.125;
   const float phiMinWin_ec1 = 0.125;
   const float phiMinWin_ec2 = 0.125;
   const float phiMinWin_ec3 = 0.10;
   const float phiMinWin_ecA = 0.15;
   const float phiMinWin_ecB = 0.15;

   const float phiMaxWin_brl = 0.20;
   const float phiMaxWin_ec1 = 0.20;
   const float phiMaxWin_ec2 = 0.20;
   const float phiMaxWin_ec3 = 0.20;
   const float phiMaxWin_ecA = 0.25;
   const float phiMaxWin_ecB = 0.20;

   const float phiParams_brl[N_PARAMS] = { 0.000,  0.831};
   const float phiParams_ec1[N_PARAMS] = { 0.000,  0.885};
   const float phiParams_ec2[N_PARAMS] = { 0.015,  0.552};
   const float phiParams_ec3[N_PARAMS] = { 0.008,  0.576};
   const float phiParams_ecA[N_PARAMS] = { 0.000,  0.830};
   const float phiParams_ecB[N_PARAMS] = { 0.006,  1.331};

   //
   float minWin;
   float maxWin;
   float params[N_PARAMS];
   if( isBarrel ) {
      if( isEta ) {
         memcpy(params,etaParams_brl,sizeof(params));
         minWin = etaMinWin_brl;
         maxWin = etaMaxWin_brl;
      }
      else {
         memcpy(params,phiParams_brl,sizeof(params));
         minWin = phiMinWin_brl;
         maxWin = phiMaxWin_brl;
      }
   }
   else { // endcap
   xAOD::L2MuonParameters::ECRegions reg = xAOD::L2MuonParameters::whichECRegion(eta,phi);
   if( reg == xAOD::L2MuonParameters::ECRegions::WeakBFieldA ) {

         if( isEta ) {
            memcpy(params,etaParams_ecA,sizeof(params));
            minWin = etaMinWin_ecA;
            maxWin = etaMaxWin_ecA;
         }
         else {
            memcpy(params,phiParams_ecA,sizeof(params));
            minWin = phiMinWin_ecA;
            maxWin = phiMaxWin_ecA;
         }
      }
      else if( reg == xAOD::L2MuonParameters::ECRegions::WeakBFieldB ) {
         if( isEta ) {
            memcpy(params,etaParams_ecB,sizeof(params));
            minWin = etaMinWin_ecB;
            maxWin = etaMaxWin_ecB;
         }
         else {
            memcpy(params,phiParams_ecB,sizeof(params));
            minWin = phiMinWin_ecB;
            maxWin = phiMaxWin_ecB;
         }
      }
      else {
         if( std::abs(eta) < 1.5 ) {
            if( isEta ) {
               memcpy(params,etaParams_ec1,sizeof(params));
               minWin = etaMinWin_ec1;
               maxWin = etaMaxWin_ec1;
            }
            else {
               memcpy(params,phiParams_ec1,sizeof(params));
               minWin = phiMinWin_ec1;
               maxWin = phiMaxWin_ec1;
            }
         }
         else if( std::abs(eta) < 2.0 ) {
            if( isEta ) {
               memcpy(params,etaParams_ec2,sizeof(params));
               minWin = etaMinWin_ec2;
               maxWin = etaMaxWin_ec2;
            }
            else {
               memcpy(params,phiParams_ec2,sizeof(params));
               minWin = phiMinWin_ec2;
               maxWin = phiMaxWin_ec2;
            }
         }
         else {
            if( isEta ) {
               memcpy(params,etaParams_ec3,sizeof(params));
               minWin = etaMinWin_ec3;
               maxWin = etaMaxWin_ec3;
            }
            else {
               memcpy(params,phiParams_ec3,sizeof(params));
               minWin = phiMinWin_ec3;
               maxWin = phiMaxWin_ec3;
            }
         }
      }
   }

   //
   float x = params[0] + params[1] / pt;
   float retval = x;
   if( x < minWin ) retval = minWin;
   if( x > maxWin ) retval = maxWin;

   return retval;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode MuFastSteering::updateMonitor(const LVL1::RecMuonRoI*                    roi,
                                         const TrigL2MuonSA::MdtHits&               mdtHits,
                                         std::vector<TrigL2MuonSA::TrackPattern>&   trackPatterns ) const
{
  // initialize monitored variable
  auto inner_mdt_hits 	= Monitored::Scalar("InnMdtHits", -1);
  auto middle_mdt_hits 	= Monitored::Scalar("MidMdtHits", -1);
  auto outer_mdt_hits 	= Monitored::Scalar("OutMdtHits", -1);
  auto invalid_rpc_roi_number = Monitored::Scalar("InvalidRpcRoINumber", -1);

  auto efficiency 	= Monitored::Scalar("Efficiency", 0);
  auto sag_inverse 	= Monitored::Scalar("SagInv", 9999.);
  auto address 		= Monitored::Scalar("Address", 9999.);
  auto absolute_pt 	= Monitored::Scalar("AbsPt", 9999.);
  auto sagitta	 	= Monitored::Scalar("Sagitta", 9999.);
  auto track_pt 	= Monitored::Scalar("TrackPt", 9999.);

  std::vector<float> t_eta, t_phi;
  std::vector<float> f_eta, f_phi;
  std::vector<float> r_inner, r_middle, r_outer;
  std::vector<float> f_residuals;

  t_eta.clear();
  t_phi.clear();
  f_eta.clear();
  f_phi.clear();
  r_inner.clear();
  r_middle.clear();
  r_outer.clear();
  f_residuals.clear();

  auto track_eta	= Monitored::Collection("TrackEta", t_eta);
  auto track_phi	= Monitored::Collection("TrackPhi", t_phi);
  auto failed_eta	= Monitored::Collection("FailedRoIEta", f_eta);
  auto failed_phi	= Monitored::Collection("FailedRoIPhi", f_phi);
  auto res_inner	= Monitored::Collection("ResInner", r_inner);
  auto res_middle	= Monitored::Collection("ResMiddle", r_middle);
  auto res_outer	= Monitored::Collection("ResOuter", r_outer);
  auto fit_residuals	= Monitored::Collection("FitResiduals", f_residuals);

  auto monitorIt	= Monitored::Group(m_monTool, inner_mdt_hits, middle_mdt_hits, outer_mdt_hits,
                                                invalid_rpc_roi_number,
                                                efficiency, sag_inverse, address, absolute_pt, sagitta, track_pt,
                                                track_eta, track_phi, failed_eta, failed_phi,
                                                res_inner, res_middle, res_outer, fit_residuals );

  const float ZERO_LIMIT = 1e-5;

  if( trackPatterns.size() > 0 ) {

    efficiency  = 1;

    const TrigL2MuonSA::TrackPattern& pattern = trackPatterns[0];
    float norm = 10.;

    float count_inner  = 0;
    float count_middle = 0;
    float count_outer  = 0;

    for (const TrigL2MuonSA::MdtHitData& mdtHit : mdtHits) {

      if (std::abs(mdtHit.DriftSpace) < ZERO_LIMIT) continue;

      char st = mdtHit.cType[1];

      if (st=='I') {
        count_inner++;
        r_inner.push_back(mdtHit.Residual/norm);
        if (mdtHit.isOutlier==0) f_residuals.push_back(mdtHit.Residual/norm);
      }

      if (st=='M') {
        count_middle++;
        r_middle.push_back(mdtHit.Residual/norm);
        if (mdtHit.isOutlier==0) f_residuals.push_back(mdtHit.Residual/norm);
      }

      if (st=='O') {
        count_outer++;
        r_outer.push_back(mdtHit.Residual/norm);
        if (mdtHit.isOutlier==0) f_residuals.push_back(mdtHit.Residual/norm);
      }
    }

    inner_mdt_hits  = count_inner;
    middle_mdt_hits = count_middle;
    outer_mdt_hits  = count_outer;

    track_pt    = (std::abs(pattern.pt ) > ZERO_LIMIT)? pattern.charge*pattern.pt: 9999.;
    absolute_pt = std::abs(track_pt);

    if ( std::abs(pattern.etaMap) > ZERO_LIMIT || std::abs(pattern.phiMS) > ZERO_LIMIT ) {
      t_eta.push_back(pattern.etaMap);
      t_phi.push_back(pattern.phiMS);
    }
    if ( std::abs(pattern.pt ) < ZERO_LIMIT){
      f_eta.push_back(roi->eta());
      f_phi.push_back(roi->phi());
    }

    sagitta     = (std::abs(pattern.barrelSagitta) > ZERO_LIMIT)? pattern.barrelSagitta: 9999.;
    sag_inverse = (std::abs(pattern.barrelSagitta) > ZERO_LIMIT)? 1./pattern.barrelSagitta: 9999.;
    address     = pattern.s_address;
  }

  return StatusCode::SUCCESS;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
<<<<<<< HEAD

StatusCode MuFastSteering::updateMonitor(const xAOD::MuonRoI*                       roi,
                                         const TrigL2MuonSA::MdtHits&               mdtHits,
                                         std::vector<TrigL2MuonSA::TrackPattern>&   trackPatterns ) const
{
  // initialize monitored variable
  auto inner_mdt_hits 	= Monitored::Scalar("InnMdtHits", -1);
  auto middle_mdt_hits 	= Monitored::Scalar("MidMdtHits", -1);
  auto outer_mdt_hits 	= Monitored::Scalar("OutMdtHits", -1);
  auto invalid_rpc_roi_number = Monitored::Scalar("InvalidRpcRoINumber", -1);

  auto efficiency 	= Monitored::Scalar("Efficiency", 0);
  auto sag_inverse 	= Monitored::Scalar("SagInv", 9999.);
  auto address 		= Monitored::Scalar("Address", 9999.);
  auto absolute_pt 	= Monitored::Scalar("AbsPt", 9999.);
  auto sagitta	 	= Monitored::Scalar("Sagitta", 9999.);
  auto track_pt 	= Monitored::Scalar("TrackPt", 9999.);

  std::vector<float> t_eta, t_phi;
  std::vector<float> f_eta, f_phi;
  std::vector<float> r_inner, r_middle, r_outer;
  std::vector<float> f_residuals;

  t_eta.clear();
  t_phi.clear();
  f_eta.clear();
  f_phi.clear();
  r_inner.clear();
  r_middle.clear();
  r_outer.clear();
  f_residuals.clear();

  auto track_eta	= Monitored::Collection("TrackEta", t_eta);
  auto track_phi	= Monitored::Collection("TrackPhi", t_phi);
  auto failed_eta	= Monitored::Collection("FailedRoIEta", f_eta);
  auto failed_phi	= Monitored::Collection("FailedRoIPhi", f_phi);
  auto res_inner	= Monitored::Collection("ResInner", r_inner);
  auto res_middle	= Monitored::Collection("ResMiddle", r_middle);
  auto res_outer	= Monitored::Collection("ResOuter", r_outer);
  auto fit_residuals	= Monitored::Collection("FitResiduals", f_residuals);

  auto monitorIt	= Monitored::Group(m_monTool, inner_mdt_hits, middle_mdt_hits, outer_mdt_hits,
                                                invalid_rpc_roi_number,
                                                efficiency, sag_inverse, address, absolute_pt, sagitta, track_pt,
                                                track_eta, track_phi, failed_eta, failed_phi,
                                                res_inner, res_middle, res_outer, fit_residuals );

  const float ZERO_LIMIT = 1e-5;
=======
// handler for "UpdateAfterFork")
void MuFastSteering::handle(const Incident& incident) {
  
  if (incident.type()!="UpdateAfterFork") return;
  ATH_MSG_DEBUG("+-----------------------------------+");
  ATH_MSG_DEBUG("| handle for UpdateAfterFork called |");
  ATH_MSG_DEBUG("+-----------------------------------+");
  
  // Find the Worker ID and create an individual muon buffer name for each worker
  StringProperty worker_id;
  std::string worker_name;

  worker_id.setName("worker_id");
  worker_id = std::string("");
  const std::vector<const Property*>* dataFlowProps = m_jobOptionsSvc->getProperties("DataFlowConfig");
  if ( dataFlowProps ) {
    ATH_MSG_DEBUG(" Properties available for 'DataFlowConfig': number = " << dataFlowProps->size());
    ATH_MSG_DEBUG(" --------------------------------------------------- ");
    for ( std::vector<const Property*>::const_iterator cur = dataFlowProps->begin();
          cur != dataFlowProps->end(); cur++) {
      ATH_MSG_DEBUG((*cur)->name() << " = " << (*cur)->toString());
      // the application name is found
      if ( (*cur)->name() == "DF_WorkerId" ) {
        if (worker_id.assign(**cur)) {
          ATH_MSG_DEBUG(" ---> got worker ID = " << worker_id.value());
          worker_name = worker_id.value() ;
        } else {
          ATH_MSG_WARNING(" ---> set property failed.");
        }
      }
    }
    
    if ( worker_id.value() == "" ) {
      ATH_MSG_DEBUG(" Property for DF_WorkerId not found.");
    }
  } else {
    ATH_MSG_DEBUG(" No Properties for 'DataFlowConfig' found.");
  }

  ATH_MSG_DEBUG(" MuonCalBufferSize     = " << m_calBufferSize);
  ATH_MSG_DEBUG("=================================================");
  
  // release JobOptionsSvc
  unsigned long mjcounter = m_jobOptionsSvc->release();
  ATH_MSG_DEBUG(" --> Release JobOptionsSvc Service, Counter = " << mjcounter);

  
  //
  // Create the calibration stream
  if (m_doCalStream) {
    // set a fixed name for the buffer
    m_calBufferName = "/tmp/muonCalStreamOutput";
    m_calStreamer->setBufferName(m_calBufferName);
    m_calStreamer->setInstanceName(worker_name);

    // if it's not a data scouting chain, open the circular buffer
    if (!m_calDataScouting) {
      StatusCode sc = m_calStreamer->openStream(m_calBufferSize);
      if ( sc != StatusCode::SUCCESS ) {  
	ATH_MSG_ERROR("Failed to open the connection to the circular buffer");
      }
      else {
	ATH_MSG_INFO("Opened the connection to the circular buffer");
      }
    }
  }

}
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
>>>>>>> release/21.0.127

  if( trackPatterns.size() > 0 ) {

<<<<<<< HEAD
    efficiency  = 1;

    const TrigL2MuonSA::TrackPattern& pattern = trackPatterns[0];
    float norm = 10.;
=======
  ATH_MSG_DEBUG("prepareRobRequests called");
  
  HLT::RobRequestInfo* RRInfo = config()->robRequestInfo();

  if (!RRInfo) {
    ATH_MSG_ERROR("Null pointer to RobRequestInfo");
    return HLT::ERROR;
  }
  
  std::vector<uint32_t> MdtRobList;
  std::vector<uint32_t> RpcRobList;
  std::vector<uint32_t> TgcRobList;
  std::vector<uint32_t> CscRobList;
  
  std::vector<const TrigRoiDescriptor*> roids;
  HLT::ErrorCode hec = getFeatures(inputTE, roids);

  if (hec != HLT::OK) {
    ATH_MSG_ERROR("Could not find input TE");
    return hec;
  }
  
  // RoI base data access
  for (unsigned int i=0; i < roids.size(); i++) {
    
    if ( m_use_RoIBasedDataAccess_MDT) {
>>>>>>> release/21.0.127

    float count_inner  = 0;
    float count_middle = 0;
    float count_outer  = 0;

    for (const TrigL2MuonSA::MdtHitData& mdtHit : mdtHits) {

<<<<<<< HEAD
      if (std::abs(mdtHit.DriftSpace) < ZERO_LIMIT) continue;
=======
      MdtRobList.clear();
      if ( iroi ) m_regionSelector->DetROBIDListUint(MDT, *iroi, MdtRobList);
      RRInfo->addRequestScheduledRobIDs(MdtRobList);
      ATH_MSG_DEBUG("prepareRobRequests, find " << MdtRobList.size() << " Mdt Rob's,");
>>>>>>> release/21.0.127

      char st = mdtHit.cType[1];

      if (st=='I') {
        count_inner++;
        r_inner.push_back(mdtHit.Residual/norm);
        if (mdtHit.isOutlier==0) f_residuals.push_back(mdtHit.Residual/norm);
      }

      if (st=='M') {
        count_middle++;
        r_middle.push_back(mdtHit.Residual/norm);
        if (mdtHit.isOutlier==0) f_residuals.push_back(mdtHit.Residual/norm);
      }

<<<<<<< HEAD
      if (st=='O') {
        count_outer++;
        r_outer.push_back(mdtHit.Residual/norm);
        if (mdtHit.isOutlier==0) f_residuals.push_back(mdtHit.Residual/norm);
      }
=======
      RpcRobList.clear();
      if ( iroi ) m_regionSelector->DetROBIDListUint(RPC, *iroi, RpcRobList);
      RRInfo->addRequestScheduledRobIDs(RpcRobList);
      ATH_MSG_DEBUG("prepareRobRequests, find " << RpcRobList.size() << " Rpc Rob's,");
>>>>>>> release/21.0.127
    }

    inner_mdt_hits  = count_inner;
    middle_mdt_hits = count_middle;
    outer_mdt_hits  = count_outer;

    track_pt    = (std::abs(pattern.pt ) > ZERO_LIMIT)? pattern.charge*pattern.pt: 9999.;
    absolute_pt = std::abs(track_pt);

<<<<<<< HEAD
    if ( std::abs(pattern.etaMap) > ZERO_LIMIT || std::abs(pattern.phiMS) > ZERO_LIMIT ) {
      t_eta.push_back(pattern.etaMap);
      t_phi.push_back(pattern.phiMS);
    }
    if ( std::abs(pattern.pt ) < ZERO_LIMIT){
      f_eta.push_back(roi->eta());
      f_phi.push_back(roi->phi());
    }

    sagitta     = (std::abs(pattern.barrelSagitta) > ZERO_LIMIT)? pattern.barrelSagitta: 9999.;
    sag_inverse = (std::abs(pattern.barrelSagitta) > ZERO_LIMIT)? 1./pattern.barrelSagitta: 9999.;
    address     = pattern.s_address;
  }

  return StatusCode::SUCCESS;
=======
      TrigRoiDescriptor* roi = new TrigRoiDescriptor( roi_eta, etaMin, etaMax, roi_phi, phiMin, phiMax ); 
      const IRoiDescriptor* iroi = (IRoiDescriptor*) roi;

      TgcRobList.clear();
      if ( iroi ) m_regionSelector->DetROBIDListUint(TGC, *iroi, TgcRobList);
      RRInfo->addRequestScheduledRobIDs(TgcRobList);
      ATH_MSG_DEBUG("prepareRobRequests, find " << TgcRobList.size() << " Tgc Rob's,");

      if(roi) delete roi;
    }

    if ( m_use_RoIBasedDataAccess_CSC) {

      const IRoiDescriptor* iroi = (IRoiDescriptor*) roids[i];

      CscRobList.clear();
      if ( iroi ) m_regionSelector->DetROBIDListUint(CSC, *iroi, CscRobList);
      RRInfo->addRequestScheduledRobIDs(CscRobList);
      ATH_MSG_DEBUG("prepareRobRequests, find " << CscRobList.size() << " Csc Rob's,");
    }
  }
  
  // Full data access
  if ( !m_use_RoIBasedDataAccess_MDT ) {
    MdtRobList.clear();
    m_regionSelector->DetROBIDListUint(MDT, MdtRobList);
    RRInfo->addRequestScheduledRobIDs(MdtRobList);
    ATH_MSG_DEBUG("prepareRobRequests, find " << MdtRobList.size() << " Mdt Rob's,");
  }

  if ( !m_use_RoIBasedDataAccess_RPC ) {
    RpcRobList.clear();
    m_regionSelector->DetROBIDListUint(RPC, RpcRobList);
    RRInfo->addRequestScheduledRobIDs(RpcRobList);
    ATH_MSG_DEBUG("prepareRobRequests, find " << RpcRobList.size() << " Rpc Rob's,");
  }

  if ( !m_use_RoIBasedDataAccess_TGC ) {
    TgcRobList.clear();
    m_regionSelector->DetROBIDListUint(TGC, TgcRobList);
    RRInfo->addRequestScheduledRobIDs(TgcRobList);
    ATH_MSG_DEBUG("prepareRobRequests, find " << TgcRobList.size() << " Tgc Rob's,");
  }

  if ( !m_use_RoIBasedDataAccess_CSC ) {
    CscRobList.clear();
    m_regionSelector->DetROBIDListUint(CSC, CscRobList);
    RRInfo->addRequestScheduledRobIDs(CscRobList);
    ATH_MSG_DEBUG("prepareRobRequests, find " << CscRobList.size() << " Csc Rob's,");
  }
  
  return HLT::OK;
>>>>>>> release/21.0.127
}
