/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#include "RpcDataPreparator.h"
#include "RpcData.h"
#include "RecMuonRoIUtils.h"

<<<<<<< HEAD
#include "MuonReadoutGeometry/MuonDetectorManager.h"
#include "MuonReadoutGeometry/RpcReadoutElement.h"
#include "CxxUtils/phihelper.h"
=======
#include "TrigL2MuonSA/RpcDataPreparator.h"

#include "GaudiKernel/ToolFactory.h"
#include "StoreGate/StoreGateSvc.h"

#include "CLHEP/Units/PhysicalConstants.h"

#include "MuonPrepRawData/MuonPrepDataContainer.h"
#include "MuonReadoutGeometry/MuonDetectorManager.h"
#include "MuonReadoutGeometry/RpcReadoutElement.h"
#include "MuonContainerManager/MuonRdoContainerAccess.h"
#include "Identifier/IdentifierHash.h"

#include "RPCcablingInterface/IRPCcablingServerSvc.h"

#include "TrigL2MuonSA/RpcData.h"
#include "TrigL2MuonSA/RecMuonRoIUtils.h"

#include "AthenaBaseComps/AthMsgStreamMacros.h"

using namespace Muon;
using namespace MuonGM;

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

static const InterfaceID IID_RpcDataPreparator("IID_RpcDataPreparator", 1, 0);

const InterfaceID& TrigL2MuonSA::RpcDataPreparator::interfaceID() { return IID_RpcDataPreparator; }
>>>>>>> release/21.0.127

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

TrigL2MuonSA::RpcDataPreparator::RpcDataPreparator(const std::string& type, 
                                                   const std::string& name,
                                                   const IInterface*  parent): 
<<<<<<< HEAD
   AthAlgTool(type,name,parent) {
=======
   AthAlgTool(type,name,parent),
   m_storeGateSvc( "StoreGateSvc", name ),
   m_activeStore(0),
   m_regionSelector(0),
   m_rpcPrepDataProvider("Muon::RpcRdoToPrepDataTool/RpcPrepDataProviderTool"),
   m_idHelperTool("Muon::MuonIdHelperTool/MuonIdHelperTool")
{
   declareInterface<TrigL2MuonSA::RpcDataPreparator>(this);
   declareProperty("RpcPrepDataProvider", m_rpcPrepDataProvider);
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

TrigL2MuonSA::RpcDataPreparator::~RpcDataPreparator() 
{
>>>>>>> release/21.0.127
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode TrigL2MuonSA::RpcDataPreparator::initialize()
{
<<<<<<< HEAD

   // Locate RegionSelector
   ATH_CHECK( m_regionSelector.retrieve() );

   ATH_CHECK( m_recRPCRoiTool.retrieve() );

   ATH_CHECK(m_idHelperSvc.retrieve());
   ATH_MSG_DEBUG("Retrieved " << m_idHelperSvc);

   ATH_CHECK(m_rpcPrepContainerKey.initialize());

   ATH_CHECK(m_clusterPreparator.retrieve());
   ATH_MSG_DEBUG("Retrieved service " << m_clusterPreparator);

=======
   // Get a message stream instance
  ATH_MSG_DEBUG("Initializing RpcDataPreparator - package version " << PACKAGE_VERSION);
   
   StatusCode sc;
   sc = AthAlgTool::initialize();
   if (!sc.isSuccess()) {
     ATH_MSG_ERROR("Could not initialize the AthAlgTool base class.");
      return sc;
   }
   
   // Locate the StoreGateSvc
   sc =  m_storeGateSvc.retrieve();
   if (!sc.isSuccess()) {
     ATH_MSG_ERROR("Could not find StoreGateSvc");
      return sc;
   }

   // Locate RegionSelector
   sc = service("RegSelSvc", m_regionSelector);
   if(sc.isFailure()) {
     ATH_MSG_ERROR("Could not retrieve RegionSelector");
      return sc;
   }
   ATH_MSG_DEBUG("Retrieved service RegionSelector");

   StoreGateSvc* detStore;
   sc = serviceLocator()->service("DetectorStore", detStore);
   if (sc.isFailure()) {
     ATH_MSG_ERROR("Could not retrieve DetectorStore.");
     return sc;
   }
   ATH_MSG_DEBUG("Retrieved DetectorStore.");
 
   sc = detStore->retrieve( m_muonMgr );
   if (sc.isFailure()) return sc;
   ATH_MSG_DEBUG("Retrieved GeoModel from DetectorStore.");
   m_rpcIdHelper = m_muonMgr->rpcIdHelper();

   sc = m_rpcPrepDataProvider.retrieve();
   if (sc.isSuccess()) {
     ATH_MSG_DEBUG("Retrieved " << m_rpcPrepDataProvider);
   } else {
     ATH_MSG_FATAL("Could not get " << m_rpcPrepDataProvider);
     return sc;
   }

   sc = m_idHelperTool.retrieve();
   if (sc.isSuccess()) {
     ATH_MSG_DEBUG("Retrieved " << m_idHelperTool);
   } else {
     ATH_MSG_FATAL("Could not get " << m_idHelperTool); 
     return sc;
   }

   // Retrieve ActiveStore
   sc = serviceLocator()->service("ActiveStoreSvc", m_activeStore);
   if (sc.isFailure() || m_activeStore == 0) {
     ATH_MSG_ERROR(" Cannot get ActiveStoreSvc.");
     return sc ;
   }
   ATH_MSG_DEBUG("Retrieved ActiveStoreSvc."); 

   // Retrieve the RPC cabling service
   ServiceHandle<IRPCcablingServerSvc> RpcCabGet ("RPCcablingServerSvc", name());
   sc = RpcCabGet.retrieve();
   if ( sc != StatusCode::SUCCESS ) {
     ATH_MSG_ERROR("Could not retrieve the RPCcablingServerSvc");
     return sc;
   }
   sc = RpcCabGet->giveCabling(m_rpcCabling);
   if ( sc != StatusCode::SUCCESS ) {
     ATH_MSG_ERROR("Could not retrieve the RPC Cabling Server");
     return sc;
   }
   m_rpcCablingSvc = m_rpcCabling->getRPCCabling();
   if ( !m_rpcCablingSvc ) {
     ATH_MSG_ERROR("Could not retrieve the RPC cabling svc");
     return StatusCode::FAILURE;
   } 
   
   // 
>>>>>>> release/21.0.127
   return StatusCode::SUCCESS; 
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

void TrigL2MuonSA::RpcDataPreparator::setRoIBasedDataAccess(bool use_RoIBasedDataAccess)
{
  m_use_RoIBasedDataAccess = use_RoIBasedDataAccess;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

void TrigL2MuonSA::RpcDataPreparator::setMultiMuonTrigger( const bool multiMuonTrigger )
{
  m_doMultiMuon = multiMuonTrigger;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode TrigL2MuonSA::RpcDataPreparator::prepareData(const TrigRoiDescriptor*    p_roids,
                                                        TrigL2MuonSA::RpcHits&      rpcHits,
<<<<<<< HEAD
                                                        TrigL2MuonSA::RpcLayerHits& rpcLayerHits,
                                                        const ToolHandle<RpcPatFinder>*   rpcPatFinder) const
{
  // RPC data extraction referring TrigMuonEFStandaloneTrackTool and MuonHoughPatternFinderTool
  rpcHits.clear();

  if( m_emulateNoRpcHit )
    return StatusCode::SUCCESS;
=======
                                                        ToolHandle<RpcPatFinder>*   rpcPatFinder)
{
  // RPC data extraction referring TrigMuonEFStandaloneTrackTool and MuonHoughPatternFinderTool
  rpcHits.clear();
  
  // set to false the flag indicating whether the roi is a fake one.
  m_isFakeRoi = false;

  // check the roi ID
  
  //  decode  roIWord
  unsigned int sectorAddress = (roiWord & 0x003FC000) >> 14;
  unsigned int sectorRoIOvl  = (roiWord & 0x000007FC) >> 2;
  unsigned int side =  sectorAddress & 0x00000001;
  unsigned int sector = (sectorAddress & 0x0000003e) >> 1;
  unsigned int roiNumber =  sectorRoIOvl & 0x0000001F;
  //  unsigned int padNumber = roiNumber/4; 
  
  unsigned int logic_sector;
  unsigned short int PADId;
  unsigned int padIdHash;
  if ( !m_rpcCablingSvc->give_PAD_address( side, sector, roiNumber, logic_sector, PADId, padIdHash) ) {
    ATH_MSG_WARNING("Roi Number: " << roiNumber << " not compatible with side, sector: "
		    << side <<  " " << sector);
    // set the bool flag to send the event to the debug stream
    m_isFakeRoi = true;
    //    return StatusCode::FAILURE;
  }
  else {
    ATH_MSG_DEBUG("Roi Number: " << roiNumber << " side, sector: " << side <<  " " << sector
		  << " corresp. to log_sector, padId: " << logic_sector << " " << PADId);
  }
>>>>>>> release/21.0.127

   const IRoiDescriptor* iroi = (IRoiDescriptor*) p_roids;

   std::vector<const Muon::RpcPrepDataCollection*> rpcCols;
   std::vector<IdentifierHash> rpcHashList;
   std::vector<IdentifierHash> rpcHashListWithData;
   std::vector<IdentifierHash> rpcHashList_cache;

   if (m_use_RoIBasedDataAccess) {

     ATH_MSG_DEBUG("Use RoI based data access");
     
<<<<<<< HEAD
     if (iroi) m_regionSelector->HashIDList(*iroi, rpcHashList);
     else {
       TrigRoiDescriptor fullscan_roi( true );
       m_regionSelector->HashIDList(fullscan_roi, rpcHashList);
=======
     if (iroi) m_regionSelector->DetHashIDList(RPC, *iroi, rpcHashList);
     else m_regionSelector->DetHashIDList(RPC, rpcHashList);
     ATH_MSG_DEBUG("rpcHashList.size()=" << rpcHashList.size());
     
     std::vector<uint32_t> rpcRobList;
     m_regionSelector->DetROBIDListUint(RPC, *iroi, rpcRobList);
     if ( m_rpcPrepDataProvider->decode(rpcRobList).isFailure() ) {
       ATH_MSG_WARNING("Problems when preparing RPC PrepData ");
>>>>>>> release/21.0.127
     }
     ATH_MSG_DEBUG("rpcHashList.size()=" << rpcHashList.size());

     std::vector<uint32_t> rpcRobList;
     m_regionSelector->ROBIDList(*iroi, rpcRobList);
   } else {
     
     ATH_MSG_DEBUG("Use full data access");
     
<<<<<<< HEAD
     TrigRoiDescriptor fullscan_roi( true );
     m_regionSelector->HashIDList(fullscan_roi, rpcHashList);
     ATH_MSG_DEBUG("rpcHashList.size()=" << rpcHashList.size());
     
     std::vector<uint32_t> rpcRobList;
     m_regionSelector->ROBIDList(fullscan_roi, rpcRobList);
=======
     m_regionSelector->DetHashIDList(RPC, rpcHashList);
     ATH_MSG_DEBUG("rpcHashList.size()=" << rpcHashList.size());
     
     std::vector<uint32_t> rpcRobList;
     m_regionSelector->DetROBIDListUint(RPC, rpcRobList);
     if ( m_rpcPrepDataProvider->decode(rpcRobList).isFailure() ) {
       ATH_MSG_WARNING("Problems when preparing RPC PrepData ");
     }
>>>>>>> release/21.0.127
     
   }
   
   if (!rpcHashList.empty()) {
     
     // Get RPC container
<<<<<<< HEAD
     const Muon::RpcPrepDataContainer* rpcPrds;
     auto rpcPrepContainerHandle = SG::makeHandle(m_rpcPrepContainerKey);
     rpcPrds = rpcPrepContainerHandle.cptr();
     if (!rpcPrepContainerHandle.isValid()) {
       ATH_MSG_ERROR("Cannot retrieve RPC PRD Container key: " << m_rpcPrepContainerKey.key());
       return StatusCode::FAILURE;
     } else {
       ATH_MSG_DEBUG("RPC PRD Container retrieved with key: " << m_rpcPrepContainerKey.key());
=======
     const RpcPrepDataContainer* rpcPrds = 0;
     std::string rpcKey = "RPC_Measurements";
     
     if (m_activeStore) {
       StatusCode sc = (*m_activeStore)->retrieve(rpcPrds, rpcKey);
       if ( sc.isFailure() ) {
         ATH_MSG_ERROR(" Cannot retrieve RPC PRD Container " << rpcKey);
         return StatusCode::FAILURE;;
       } else {       
         ATH_MSG_DEBUG(" RPC PRD Container retrieved with key " << rpcKey);
       }
     } else {
       ATH_MSG_ERROR("Null pointer to ActiveStore");
       return StatusCode::FAILURE;;
>>>>>>> release/21.0.127
     }

     // Get RPC collections
     for(const IdentifierHash& id : rpcHashList) {
       auto RPCcoll = rpcPrds->indexFindPtr(id);

       if( RPCcoll == nullptr ) {
         continue;
       }

<<<<<<< HEAD
       if( RPCcoll->size() == 0) {
=======
       if( (*RPCcoll)->size() == 0)    {
>>>>>>> release/21.0.127
         ATH_MSG_DEBUG("Empty RPC list");
         continue;
       }

<<<<<<< HEAD
       rpcHashList_cache.push_back(id);
       rpcCols.push_back(RPCcoll);
=======
       rpcHashList_cache.push_back(*idit);
       
       rpcCols.push_back(*RPCcoll);

       if (rpcCols.empty()) {
         ATH_MSG_DEBUG("No Rpc data collections selected");
       }
>>>>>>> release/21.0.127
     }
   }

   for( const Muon::RpcPrepDataCollection* rpc : rpcCols ){

     rpcHits.reserve( rpcHits.size() + rpc->size() );
     for( const Muon::RpcPrepData* prd : *rpc ) {

       const Identifier id = prd->identify();

       const int doubletR      = m_idHelperSvc->rpcIdHelper().doubletR(id);
       const int doubletPhi    = m_idHelperSvc->rpcIdHelper().doubletPhi(id);
       const int doubletZ      = m_idHelperSvc->rpcIdHelper().doubletZ(id);
       const int gasGap        = m_idHelperSvc->rpcIdHelper().gasGap(id);
       const bool measuresPhi  = m_idHelperSvc->rpcIdHelper().measuresPhi(id);
       const int stationEta    = m_idHelperSvc->rpcIdHelper().stationEta(id);
       std::string stationName = m_idHelperSvc->rpcIdHelper().stationNameString(m_idHelperSvc->rpcIdHelper().stationName(id));

<<<<<<< HEAD
=======
   for( ;it!=it_end;++it ){
     Muon::RpcPrepDataCollection::const_iterator cit_begin = (*it)->begin();
     Muon::RpcPrepDataCollection::const_iterator cit_end = (*it)->end();
     if (cit_begin == cit_end) return StatusCode::SUCCESS;
     
     Muon::RpcPrepDataCollection::const_iterator cit = cit_begin;   
     
     cit = cit_begin;   
     for( ; cit!=cit_end;++cit ) {
       const Muon::RpcPrepData* prd = *cit;
       Identifier id = prd->identify();

       const int doubletR      = m_rpcIdHelper->doubletR(id);
       const int doubletPhi    = m_rpcIdHelper->doubletPhi(id);
       const int doubletZ      = m_rpcIdHelper->doubletZ(id);
       const int gasGap        = m_rpcIdHelper->gasGap(id);
       const bool measuresPhi  = m_rpcIdHelper->measuresPhi(id);
       const int stationEta    = m_rpcIdHelper->stationEta(id);
       std::string stationName = m_rpcIdHelper->stationNameString(m_rpcIdHelper->stationName(id));

>>>>>>> release/21.0.127
       int layer = 0;
       // BO
       if (stationName.substr(0,2)=="BO") layer = 4;
       // doubletR
       layer += 2*(doubletR-1);
       // BML7 special chamber with 1 RPC doublet (doubletR=1 but RPC2) :
       if (stationName.substr(0,3)=="BML"&&stationEta==7) layer+=2;
       // gasGap
       layer += gasGap - 1;

       const Amg::Vector3D globalpos = prd->globalPosition();
       const double hitx=globalpos.x();
       const double hity=globalpos.y();
       const double hitz=globalpos.z();

       const double hittime = prd->time();
       const MuonGM::RpcReadoutElement* detEl = prd->detectorElement();
       const double distToPhiReadout = detEl->distanceToPhiReadout(globalpos);
       const double distToEtaReadout = detEl->distanceToEtaReadout(globalpos);

       ATH_MSG_DEBUG("Selected Rpc Collection: station name:" << stationName
		     << " global positions x/y/z=" << hitx << "/" << hity << "/" << hitz
		     << " doubletR: " << doubletR << " doubletZ: " << doubletZ << " doubletPhi " << doubletPhi
		     << " gasGap " << gasGap << " layer " << layer << " time " << hittime
		     << " distToEtaReadout " << distToEtaReadout << " distToPhiReadout " << distToPhiReadout);
       
       TrigL2MuonSA::RpcHitData lutDigit;
       
       lutDigit.x           = hitx;
       lutDigit.y           = hity;
       lutDigit.z           = hitz;
       lutDigit.time        = hittime;
       lutDigit.distToEtaReadout = distToEtaReadout;
       lutDigit.distToPhiReadout = distToPhiReadout;
       lutDigit.gasGap      = gasGap;
       lutDigit.doubletR    = doubletR;
       lutDigit.doubletPhi  = doubletPhi;
       lutDigit.doubletZ    = doubletZ;
       lutDigit.measuresPhi = measuresPhi;
       lutDigit.stationName = stationName;
       lutDigit.layer       = layer;
       
       const float r2 = hitx*hitx+hity*hity;
       float phi = std::atan2(hity,hitx);
       const float l = std::sqrt(hitz*hitz+r2);
       const float tan = std::sqrt( (l-hitz)/(l+hitz) );
       const float eta = -std::log(tan);
       const float deta = std::abs(p_roids->eta() - eta);
       const float dphi = std::abs(CxxUtils::wrapToPi(p_roids->phi() - phi));

       lutDigit.eta = eta;
       lutDigit.phi = phi;
       lutDigit.l = l;
       rpcHits.push_back(lutDigit);

       float deta_thr = 0.1;
       float dphi_thr = 0.1;
       float dynamic_add = 0.02;

       //Determine deta, dphi threshold in case of dynamicDeltaRpcMode
       if( m_doMultiMuon ){
         ATH_MSG_DEBUG("Collected RPC hits by MultiMuonTriggerMode");
         m_recRPCRoiTool->roiData( p_roids->roiWord() );
         double RoiPhiMin(0);
         double RoiPhiMax(0);
         double RoiEtaMin(0);
         double RoiEtaMax(0);
         m_recRPCRoiTool->RoIsize(p_roids->roiWord(), RoiEtaMin, RoiEtaMax, RoiPhiMin, RoiPhiMax);
         ATH_MSG_DEBUG( "RoI Phi min = " << RoiPhiMin << " RoI Phi max = " << RoiPhiMax << " RoI Eta min = " << RoiEtaMin << " RoI Eta max = " << RoiEtaMax );
         deta_thr = std::abs( RoiEtaMax - RoiEtaMin )/2. + dynamic_add;
         dphi_thr = std::abs( std::acos( std::cos( RoiPhiMax - RoiPhiMin ) ) )/2. + dynamic_add;
         ATH_MSG_DEBUG( "deta threshold = " << deta_thr);
         ATH_MSG_DEBUG( "dphi threshold = " << dphi_thr);
       }

       if (m_use_RoIBasedDataAccess) {
<<<<<<< HEAD
         if ( deta<deta_thr && dphi<dphi_thr)
           (*rpcPatFinder)->addHit(stationName, stationEta, measuresPhi, gasGap, doubletR, hitx, hity, hitz, rpcLayerHits);
       } else {
         if ( deta<0.15 && dphi<0.1)
           (*rpcPatFinder)->addHit(stationName, stationEta, measuresPhi, gasGap, doubletR, hitx, hity, hitz, rpcLayerHits);
=======
         if ( fabs(deta)<0.1 && fabs(dphi)<0.1)
           (*rpcPatFinder)->addHit(stationName, stationEta, measuresPhi, gasGap, doubletR, hitx, hity, hitz);
       } else {
         if ( fabs(deta)<0.15 && fabs(dphi)<0.1)
           (*rpcPatFinder)->addHit(stationName, stationEta, measuresPhi, gasGap, doubletR, hitx, hity, hitz);
>>>>>>> release/21.0.127
       }
     }
   }

  return StatusCode::SUCCESS;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode TrigL2MuonSA::RpcDataPreparator::prepareData(const TrigRoiDescriptor*         p_roids,
                                                        TrigL2MuonSA::RpcLayerClusters&  rpcLayerClusters,
                                                        const ToolHandle<ClusterPatFinder>*    clusterPatFinder) const
{
<<<<<<< HEAD
  // RPC data extraction referring TrigMuonEFStandaloneTrackTool and MuonHoughPatternFinderTool

  if( m_emulateNoRpcHit )
    return StatusCode::SUCCESS;

   const IRoiDescriptor* iroi = (IRoiDescriptor*) p_roids;

   std::vector<const Muon::RpcPrepDataCollection*> rpcCols;
   std::vector<IdentifierHash> rpcHashList;
   std::vector<IdentifierHash> rpcHashList_cache;

   if (m_use_RoIBasedDataAccess) {

     ATH_MSG_DEBUG("Use RoI based data access");
     
     if (iroi) m_regionSelector->HashIDList(*iroi, rpcHashList);
     else {
       TrigRoiDescriptor fullscan_roi( true );
       m_regionSelector->HashIDList(fullscan_roi, rpcHashList);
     }
     ATH_MSG_DEBUG("rpcHashList.size()=" << rpcHashList.size());
     
   } else {
     
     ATH_MSG_DEBUG("Use full data access");
     
     TrigRoiDescriptor fullscan_roi( true );
     m_regionSelector->HashIDList(fullscan_roi, rpcHashList);
     ATH_MSG_DEBUG("rpcHashList.size()=" << rpcHashList.size());
     
   }
=======
  ATH_MSG_DEBUG("Finalizing RpcDataPreparator - package version " << PACKAGE_VERSION);
>>>>>>> release/21.0.127
   
   if (!rpcHashList.empty()) {
     
     // Get RPC container
     const Muon::RpcPrepDataContainer* rpcPrds;
     auto rpcPrepContainerHandle = SG::makeHandle(m_rpcPrepContainerKey);
     rpcPrds = rpcPrepContainerHandle.cptr();
     if (!rpcPrepContainerHandle.isValid()) {
       ATH_MSG_ERROR("Cannot retrieve RPC PRD Container key: " << m_rpcPrepContainerKey.key());
       return StatusCode::FAILURE;
     } else {
       ATH_MSG_DEBUG("RPC PRD Container retrieved with key: " << m_rpcPrepContainerKey.key());
     }

     // Get RPC collections
     for(const IdentifierHash& id : rpcHashList) {

       auto RPCcoll = rpcPrds->indexFindPtr(id);

       if( RPCcoll == nullptr ) {
         continue;
       }

       if( RPCcoll->size() == 0) {
         ATH_MSG_DEBUG("Empty RPC list");
         continue;
       }

       rpcHashList_cache.push_back(id);
       rpcCols.push_back(RPCcoll);
     }
   }
   ATH_MSG_DEBUG("Do rpc clustering");
   ATH_CHECK( m_clusterPreparator->clusteringRPCs(m_doMultiMuon, rpcCols, p_roids, clusterPatFinder, rpcLayerClusters) );

  return StatusCode::SUCCESS;
}

