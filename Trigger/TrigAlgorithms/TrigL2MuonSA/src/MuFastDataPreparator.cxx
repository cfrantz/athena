/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

<<<<<<< HEAD
#include "MuFastDataPreparator.h"
#include "xAODTrigMuon/TrigMuonDefs.h"
=======
#include "TrigL2MuonSA/MuFastDataPreparator.h"

#include "GaudiKernel/ToolFactory.h"
#include "StoreGate/StoreGateSvc.h"

#include "CLHEP/Units/PhysicalConstants.h"

#include "MuonContainerManager/MuonRdoContainerAccess.h"

#include "Identifier/IdentifierHash.h"
#include "MuonReadoutGeometry/MuonDetectorManager.h"
#include "MuonIdHelpers/MdtIdHelper.h"

#include "AthenaBaseComps/AthMsgStreamMacros.h"
>>>>>>> release/21.0.127

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

TrigL2MuonSA::MuFastDataPreparator::MuFastDataPreparator(const std::string& type,
                                                         const std::string& name,
<<<<<<< HEAD
                                                         const IInterface*  parent):
  AthAlgTool(type,name,parent)
=======
                                                         const IInterface*  parent): 
  AthAlgTool(type,name,parent),
  m_recRPCRoiSvc("LVL1RPC::RPCRecRoiSvc",""),
  m_options(),
  m_rpcDataPreparator("TrigL2MuonSA::RpcDataPreparator"),
  m_tgcDataPreparator("TrigL2MuonSA::TgcDataPreparator"),
  m_mdtDataPreparator("TrigL2MuonSA::MdtDataPreparator"),
  m_cscDataPreparator("TrigL2MuonSA::CscDataPreparator"),
  m_rpcRoadDefiner("TrigL2MuonSA::RpcRoadDefiner"),
  m_tgcRoadDefiner("TrigL2MuonSA::TgcRoadDefiner"),
  m_rpcPatFinder("TrigL2MuonSA::RpcPatFinder")
>>>>>>> release/21.0.127
{
}


// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode TrigL2MuonSA::MuFastDataPreparator::initialize()
{
   // Get a message stream instance
<<<<<<< HEAD
   ATH_MSG_DEBUG("Initializing MuFastDataPreparator - package version " << PACKAGE_VERSION);

   ATH_CHECK(AthAlgTool::initialize());

   ATH_CHECK(m_recRPCRoiTool.retrieve());

   if (m_use_rpc) {
     ATH_CHECK(m_rpcDataPreparator.retrieve());
     ATH_MSG_DEBUG("Retrieved service " << m_rpcDataPreparator);
   }

   ATH_CHECK(m_tgcDataPreparator.retrieve());
   ATH_MSG_DEBUG("Retrieved service " << m_tgcDataPreparator);

   ATH_CHECK(m_mdtDataPreparator.retrieve());
   ATH_MSG_DEBUG("Retrieved service " << m_mdtDataPreparator);

   ATH_CHECK(m_cscDataPreparator.retrieve(DisableTool{m_cscDataPreparator.empty()}));
   ATH_MSG_DEBUG("Retrieved service " << m_cscDataPreparator);

   ATH_CHECK(m_stgcDataPreparator.retrieve(DisableTool{m_stgcDataPreparator.empty()}));
   ATH_MSG_DEBUG("Retrieved service " << m_stgcDataPreparator);

   ATH_CHECK(m_mmDataPreparator.retrieve(DisableTool{m_mmDataPreparator.empty()}));
   ATH_MSG_DEBUG("Retrieved service " << m_mmDataPreparator);

   ATH_CHECK(m_rpcRoadDefiner.retrieve());
   ATH_MSG_DEBUG("Retrieved service " << m_rpcRoadDefiner);

   ATH_CHECK(m_tgcRoadDefiner.retrieve());
   ATH_MSG_DEBUG("Retrieved service " << m_tgcRoadDefiner);

   ATH_CHECK(m_rpcPatFinder.retrieve());
=======
  ATH_MSG_DEBUG("Initializing MuFastDataPreparator - package version " << PACKAGE_VERSION);
   
   StatusCode sc;
   sc = AthAlgTool::initialize();
   if (!sc.isSuccess()) {
     ATH_MSG_ERROR("Could not initialize the AthAlgTool base class.");
      return sc;
   }

  sc = m_recRPCRoiSvc.retrieve();
  if ( sc.isFailure() ) {
    ATH_MSG_ERROR("Couldn't connect to " << m_recRPCRoiSvc);
    return sc;
  } else {
    ATH_MSG_INFO("Retrieved Service " << m_recRPCRoiSvc);
  }
  
  // retrieve the ID helper and the region selector
   StoreGateSvc* detStore(0);
   const MuonGM::MuonDetectorManager* muonMgr;
   sc = serviceLocator()->service("DetectorStore", detStore);
   if (sc.isFailure()) {
     ATH_MSG_ERROR("Could not retrieve DetectorStore.");
     return sc;
   }
   ATH_MSG_DEBUG("Retrieved DetectorStore.");
   
   sc = detStore->retrieve( muonMgr,"Muon" );
   if (sc.isFailure()) return sc;
   ATH_MSG_DEBUG("Retrieved GeoModel from DetectorStore.");
   m_mdtIdHelper = muonMgr->mdtIdHelper();
   
   // Locate RegionSelector
   sc = service("RegSelSvc", m_regionSelector);
   if(sc.isFailure()) {
     ATH_MSG_ERROR("Could not retrieve the regionselector service");
      return sc;
   }
   ATH_MSG_DEBUG("Retrieved the RegionSelector service ");

   if (m_use_rpc) {
     sc = m_rpcDataPreparator.retrieve();
     if ( sc.isFailure() ) {
       ATH_MSG_ERROR("Could not retrieve " << m_rpcDataPreparator);
       return sc;
     }
     ATH_MSG_DEBUG("Retrieved service " << m_rpcDataPreparator);
   }

   sc = m_tgcDataPreparator.retrieve();
   if ( sc.isFailure() ) {
     ATH_MSG_ERROR("Could not retrieve " << m_tgcDataPreparator);
      return sc;
   }
   ATH_MSG_DEBUG("Retrieved service " << m_tgcDataPreparator);

   sc =m_mdtDataPreparator.retrieve();
   if ( sc.isFailure() ) {
     ATH_MSG_ERROR("Could not retrieve " << m_mdtDataPreparator);
      return sc;
   }
   ATH_MSG_DEBUG("Retrieved service " << m_mdtDataPreparator);
   
   sc =m_cscDataPreparator.retrieve();
   if ( sc.isFailure() ) {
     ATH_MSG_ERROR("Could not retrieve " << m_cscDataPreparator);
     return sc;
   }
   ATH_MSG_DEBUG("Retrieved service " << m_cscDataPreparator);

   sc =m_rpcRoadDefiner.retrieve();
   if ( sc.isFailure() ) {
     ATH_MSG_ERROR("Could not retrieve " << m_rpcRoadDefiner);
     return sc;
   }
   ATH_MSG_DEBUG("Retrieved service " << m_rpcRoadDefiner);

   sc =m_tgcRoadDefiner.retrieve();
   if ( sc.isFailure() ) {
     ATH_MSG_ERROR("Could not retrieve " << m_tgcRoadDefiner);
     return sc;
   }
   ATH_MSG_DEBUG("Retrieved service " << m_tgcRoadDefiner);

   sc =m_rpcPatFinder.retrieve();
   if ( sc.isFailure() ) {
     ATH_MSG_ERROR("Could not retrieve " << m_rpcPatFinder);
     return sc;
   }
>>>>>>> release/21.0.127
   ATH_MSG_DEBUG("Retrieved service " << m_rpcPatFinder);

   ATH_CHECK(m_clusterRoadDefiner.retrieve());
   ATH_MSG_DEBUG("Retrieved service " << m_clusterRoadDefiner);

   ATH_CHECK(m_clusterPatFinder.retrieve());
   ATH_MSG_DEBUG("Retrieved service " << m_clusterPatFinder);

   return StatusCode::SUCCESS;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

void TrigL2MuonSA::MuFastDataPreparator::setOptions(const TrigL2MuonSA::MuFastDataPreparatorOptions& options)
{
   m_options = options;
   m_tgcDataPreparator->setOptions(options.tgcOptions());
   return;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode TrigL2MuonSA::MuFastDataPreparator::setMCFlag(BooleanProperty use_mcLUT)
{
  m_use_mcLUT = use_mcLUT;

  if (m_use_mcLUT) {
    const ServiceHandle<TrigL2MuonSA::PtEndcapLUTSvc> ptEndcapLUTSvc("PtEndcapLUTSvc_MC", name());
    if ( ptEndcapLUTSvc.retrieve().isFailure() ) {
      ATH_MSG_DEBUG("Could not retrieve PtEndcapLUTSvc_MC");
      return StatusCode::FAILURE;
    }
    m_tgcRoadDefiner->setPtLUT(&*ptEndcapLUTSvc);
  } else {
<<<<<<< HEAD
    const ServiceHandle<TrigL2MuonSA::PtEndcapLUTSvc> ptEndcapLUTSvc("PtEndcapLUTSvc", name());
    if ( ptEndcapLUTSvc.retrieve().isFailure() ) {
      ATH_MSG_DEBUG("Could not retrieve PtEndcapLUTSvc");
      return StatusCode::FAILURE;
    }
    m_tgcRoadDefiner->setPtLUT(&*ptEndcapLUTSvc);
=======
    sc = serviceLocator()->service("PtEndcapLUTSvc",    m_ptEndcapLUTSvc);
  }
  if (!sc.isSuccess()) {
    ATH_MSG_ERROR("Could not find PtEndcapLUTSvc");
    return sc;
>>>>>>> release/21.0.127
  }

  return StatusCode::SUCCESS;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

void TrigL2MuonSA::MuFastDataPreparator::setRoadWidthForFailure(double rWidth_RPC_Failed,
                                                                double rWidth_TGC_Failed)
{
  m_rpcRoadDefiner->setRoadWidthForFailure(rWidth_RPC_Failed);
  m_tgcRoadDefiner->setRoadWidthForFailure(rWidth_TGC_Failed);
  return;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

void TrigL2MuonSA::MuFastDataPreparator::setRpcGeometry(bool use_rpc)
{
  m_rpcRoadDefiner->setRpcGeometry(use_rpc);

  m_use_rpc = use_rpc;
  return;
}

// --------------------------------------------------------------------------------

void TrigL2MuonSA::MuFastDataPreparator::setRoIBasedDataAccess(bool use_RoIBasedDataAccess_MDT,
                                                               bool use_RoIBasedDataAccess_RPC,
                                                               bool use_RoIBasedDataAccess_TGC,
                                                               bool use_RoIBasedDataAccess_CSC,
							       bool use_RoIBasedDataAccess_STGC,
							       bool use_RoIBasedDataAccess_MM)
{
  m_mdtDataPreparator ->setRoIBasedDataAccess(use_RoIBasedDataAccess_MDT);
  m_rpcDataPreparator ->setRoIBasedDataAccess(use_RoIBasedDataAccess_RPC);
  m_tgcDataPreparator ->setRoIBasedDataAccess(use_RoIBasedDataAccess_TGC);
  if(!m_cscDataPreparator.empty()) m_cscDataPreparator->setRoIBasedDataAccess(use_RoIBasedDataAccess_CSC);
  if(!m_stgcDataPreparator.empty()) m_stgcDataPreparator->setRoIBasedDataAccess(use_RoIBasedDataAccess_STGC);
  if(!m_mmDataPreparator.empty()) m_mmDataPreparator->setRoIBasedDataAccess(use_RoIBasedDataAccess_MM);
  return;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

void TrigL2MuonSA::MuFastDataPreparator::setExtrapolatorTool(ToolHandle<ITrigMuonBackExtrapolator>* backExtrapolator)
{
  m_backExtrapolatorTool = backExtrapolator;
  m_tgcRoadDefiner->setExtrapolatorTool(m_backExtrapolatorTool);
  return;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

void TrigL2MuonSA::MuFastDataPreparator::setMultiMuonTrigger( const bool multiMuonTrigger )
{
  m_rpcDataPreparator->setMultiMuonTrigger(multiMuonTrigger);
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode TrigL2MuonSA::MuFastDataPreparator::prepareData(const LVL1::RecMuonRoI*     p_roi,
                                                           const TrigRoiDescriptor*    p_roids,
                                                           const bool                  insideOut,
                                                           TrigL2MuonSA::RpcHits&      rpcHits,
                                                           TrigL2MuonSA::MuonRoad&     muonRoad,
                                                           TrigL2MuonSA::MdtRegion&    mdtRegion,
                                                           TrigL2MuonSA::RpcFitResult& rpcFitResult,
                                                           TrigL2MuonSA::MdtHits&      mdtHits_normal,
                                                           TrigL2MuonSA::MdtHits&      mdtHits_overlap) const
{

<<<<<<< HEAD
  ATH_MSG_DEBUG("RoI eta/phi=" << p_roi->eta() << "/" << p_roi->phi());

=======
  ATH_MSG_DEBUG("RoI eta/phi=" << p_roi->eta() << "/" << p_roi->phi());  
  
>>>>>>> release/21.0.127
  StatusCode sc = StatusCode::SUCCESS;

  //Storing rpc hits by each layers and eta/phi strip for creating road
  //RpcLayerHits class is defined in RpcPatFinder.h
  TrigL2MuonSA::RpcLayerHits rpcLayerHits;
  rpcLayerHits.clear();

  if(m_use_rpc && !insideOut) {

    sc = m_rpcDataPreparator->prepareData(p_roids,
                                          rpcHits,
                                          rpcLayerHits,
                                          &m_rpcPatFinder);

    if (!sc.isSuccess()) {
      ATH_MSG_DEBUG("Error in RPC data prepapration. Continue using RoI");
    }
  } else {
    ATH_MSG_DEBUG("Skip RpcDataPreparator");
  }

  auto data = m_recRPCRoiTool->roiData(p_roi->roiWord());
  double roiEtaMinLow = 0.;
  double roiEtaMaxLow = 0.;
  double roiEtaMinHigh = 0.;
  double roiEtaMaxHigh = 0.;
  m_recRPCRoiTool->etaDimLow(data, roiEtaMinLow, roiEtaMaxLow);
  m_recRPCRoiTool->etaDimHigh(data, roiEtaMinHigh, roiEtaMaxHigh);

  ATH_MSG_DEBUG("nr of RPC hits=" << rpcHits.size());

  sc = m_rpcRoadDefiner->defineRoad(p_roi,
                                    insideOut,
                                    muonRoad,
                                    rpcHits,
                                    rpcLayerHits,
                                    &m_rpcPatFinder,
                                    rpcFitResult,
                                    roiEtaMinLow,
                                    roiEtaMaxLow,
                                    roiEtaMinHigh,
                                    roiEtaMaxHigh);
  if (!sc.isSuccess()) {
    ATH_MSG_WARNING("Error in road definition.");
    return sc;
  }

  sc = m_mdtDataPreparator->prepareData(p_roids,
                                        rpcFitResult,
                                        muonRoad,
                                        mdtRegion,
                                        mdtHits_normal);


  if (!sc.isSuccess()) {
    ATH_MSG_WARNING("Error in MDT data preparation.");
    return sc;
  }
  ATH_MSG_DEBUG("nr of MDT (normal)  hits=" << mdtHits_normal.size());
  ATH_MSG_DEBUG("nr of MDT (overlap) hits=" << mdtHits_overlap.size());

  return StatusCode::SUCCESS;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode TrigL2MuonSA::MuFastDataPreparator::prepareData(const xAOD::MuonRoI*        p_roi,
                                                           const TrigRoiDescriptor*    p_roids,
                                                           const bool                  insideOut,
                                                           TrigL2MuonSA::RpcHits&      rpcHits,
                                                           TrigL2MuonSA::MuonRoad&     muonRoad,
                                                           TrigL2MuonSA::MdtRegion&    mdtRegion,
                                                           TrigL2MuonSA::RpcFitResult& rpcFitResult,
                                                           TrigL2MuonSA::MdtHits&      mdtHits_normal,
                                                           TrigL2MuonSA::MdtHits&      mdtHits_overlap) const
{

  ATH_MSG_DEBUG("RoI eta/phi=" << p_roi->eta() << "/" << p_roi->phi());

  StatusCode sc = StatusCode::SUCCESS;

  //Storing rpc hits by each layers and eta/phi strip for creating road
  //RpcLayerHits class is defined in RpcPatFinder.h
  TrigL2MuonSA::RpcLayerHits rpcLayerHits;
  rpcLayerHits.clear();

  if(m_use_rpc && !insideOut) {

    sc = m_rpcDataPreparator->prepareData(p_roids,
                                          rpcHits,
<<<<<<< HEAD
                                          rpcLayerHits,
                                          &m_rpcPatFinder);

=======
                                          &m_rpcPatFinder);

    // check if the RoI is fake and se the flag
    m_isRpcFakeRoi = m_rpcDataPreparator->isFakeRoi();

>>>>>>> release/21.0.127
    if (!sc.isSuccess()) {
      ATH_MSG_DEBUG("Error in RPC data prepapration. Continue using RoI");
    }
  } else {
    ATH_MSG_DEBUG("Skip RpcDataPreparator");
  }

  auto data = m_recRPCRoiTool->roiData(p_roi->roiWord());
  double roiEtaMinLow = 0.;
  double roiEtaMaxLow = 0.;
  double roiEtaMinHigh = 0.;
  double roiEtaMaxHigh = 0.;
  m_recRPCRoiTool->etaDimLow(data, roiEtaMinLow, roiEtaMaxLow);
  m_recRPCRoiTool->etaDimHigh(data, roiEtaMinHigh, roiEtaMaxHigh);

  ATH_MSG_DEBUG("nr of RPC hits=" << rpcHits.size());

  sc = m_rpcRoadDefiner->defineRoad(p_roi,
                                    insideOut,
                                    muonRoad,
                                    rpcHits,
<<<<<<< HEAD
                                    rpcLayerHits,
=======
>>>>>>> release/21.0.127
                                    &m_rpcPatFinder,
                                    rpcFitResult,
                                    roiEtaMinLow,
                                    roiEtaMaxLow,
                                    roiEtaMinHigh,
                                    roiEtaMaxHigh);
  if (!sc.isSuccess()) {
    ATH_MSG_WARNING("Error in road definition.");
    return sc;
  }

  sc = m_mdtDataPreparator->prepareData(p_roids,
                                        rpcFitResult,
                                        muonRoad,
                                        mdtRegion,
                                        mdtHits_normal);


  if (!sc.isSuccess()) {
    ATH_MSG_WARNING("Error in MDT data preparation.");
    return sc;
  }
  ATH_MSG_DEBUG("nr of MDT (normal)  hits=" << mdtHits_normal.size());
  ATH_MSG_DEBUG("nr of MDT (overlap) hits=" << mdtHits_overlap.size());

  return StatusCode::SUCCESS;
}

//for multi-track SA mode
//do Rpc clustering and create multi muon candidate
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode TrigL2MuonSA::MuFastDataPreparator::prepareData(const LVL1::RecMuonRoI*              p_roi,
                                                           const TrigRoiDescriptor*             p_roids,
                                                           std::vector<TrigL2MuonSA::MuonRoad>& clusterRoad,
                                                           std::vector<TrigL2MuonSA::RpcFitResult>&  clusterFitResults,
                                                           TrigL2MuonSA::MdtHits&               mdtHits_normal,
                                                           TrigL2MuonSA::MdtHits&               mdtHits_overlap,
                                                           std::vector<TrigL2MuonSA::MdtHits>&  mdtHits_cluster_normal) const
{

  ATH_MSG_DEBUG("RoI eta/phi=" << p_roi->eta() << "/" << p_roi->phi());
  //RpcLayerClusters class is defined in ClusterPatFinder.h
  TrigL2MuonSA::RpcLayerClusters rpcLayerClusters;
  rpcLayerClusters.clear();

  // for MdtDataPreparator's input
  TrigL2MuonSA::MdtRegion             mdtRegion;
  mdtRegion.Clear();

  StatusCode sc = StatusCode::SUCCESS;

  if(!m_use_rpc){

  } else {

    sc = m_rpcDataPreparator->prepareData(p_roids,
                                          rpcLayerClusters,
                                          &m_clusterPatFinder);

    if (!sc.isSuccess()) {
      ATH_MSG_DEBUG("Error in RPC data prepapration and clustering. Continue using RoI");
      return sc;
    }
  }

  auto data = m_recRPCRoiTool->roiData(p_roi->roiWord());
  double roiEtaMinLow = 0.;
  double roiEtaMaxLow = 0.;
  double roiEtaMinHigh = 0.;
  double roiEtaMaxHigh = 0.;
  m_recRPCRoiTool->etaDimLow(data, roiEtaMinLow, roiEtaMaxLow);
  m_recRPCRoiTool->etaDimHigh(data, roiEtaMinHigh, roiEtaMaxHigh);

  sc = m_clusterRoadDefiner->defineRoad(p_roi,
                                        clusterRoad,
                                        rpcLayerClusters,
                                        &m_clusterPatFinder,
                                        clusterFitResults,
                                        roiEtaMinLow,
                                        roiEtaMaxLow,
                                        roiEtaMinHigh,
                                        roiEtaMaxHigh);
  if (!sc.isSuccess()) {
    ATH_MSG_WARNING("Error in clusterRoad definition.");
    return sc;

  }
  if(!clusterRoad.empty()){
    sc = m_mdtDataPreparator->prepareData(p_roids,
                                          clusterFitResults.back(),
                                          clusterRoad.back(),
                                          mdtRegion,
                                          mdtHits_normal);

    if (!sc.isSuccess()) {
      ATH_MSG_WARNING("Error in MDT data preparation.");
      return sc;
    }

    ATH_MSG_DEBUG("nr of MDT (normal)  hits=" << mdtHits_normal.size());
    ATH_MSG_DEBUG("nr of MDT (overlap) hits=" << mdtHits_overlap.size());

    for(int i_road = 0; i_road < (int)clusterRoad.size(); i_road++){
      TrigL2MuonSA::MdtHits mdt_normal;
      for(int i_hit = 0; i_hit < (int)mdtHits_normal.size(); i_hit++){
        unsigned int chamber = mdtHits_normal[i_hit].Chamber;

        if (chamber >= xAOD::L2MuonParameters::MaxChamber) continue;
        double Z = mdtHits_normal[i_hit].Z;
        double R = mdtHits_normal[i_hit].R;
        double residual = 999999;
        int clusterRoadID = 9999;
        for(int j_road = 0; j_road < (int)clusterRoad.size(); j_road++){
          double aw = clusterRoad.at(j_road).aw[chamber][0];
          double bw = clusterRoad.at(j_road).bw[chamber][0];
          double tmp_residual;
          const double ZERO_LIMIT = 1e-4;
          if( std::abs(aw) < ZERO_LIMIT ){
            tmp_residual = R-bw;
          } else {
            double ia  = 1/aw;
            double iaq = ia*ia;
            double dz  = Z - (R-bw)*ia;
            tmp_residual = dz/std::sqrt(1.+iaq);
          }
          if(std::abs(residual) > std::abs(tmp_residual)){
            residual = tmp_residual;
            clusterRoadID = j_road;
          }
        }
        if(clusterRoadID == i_road){
          mdt_normal.push_back(mdtHits_normal[i_hit]);
        }
      }
      mdtHits_cluster_normal.push_back(mdt_normal);
    }
  }

  return StatusCode::SUCCESS;
}

//for multi-track SA mode
//do Rpc clustering and create multi muon candidate
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode TrigL2MuonSA::MuFastDataPreparator::prepareData(const xAOD::MuonRoI*                 p_roi,
                                                           const TrigRoiDescriptor*             p_roids,
                                                           std::vector<TrigL2MuonSA::MuonRoad>& clusterRoad,
                                                           std::vector<TrigL2MuonSA::RpcFitResult>&  clusterFitResults,
                                                           TrigL2MuonSA::MdtHits&               mdtHits_normal,
                                                           TrigL2MuonSA::MdtHits&               mdtHits_overlap,
                                                           std::vector<TrigL2MuonSA::MdtHits>&  mdtHits_cluster_normal) const
{

  ATH_MSG_DEBUG("RoI eta/phi=" << p_roi->eta() << "/" << p_roi->phi());
  //RpcLayerClusters class is defined in ClusterPatFinder.h
  TrigL2MuonSA::RpcLayerClusters rpcLayerClusters;
  rpcLayerClusters.clear();

  // for MdtDataPreparator's input
  TrigL2MuonSA::MdtRegion             mdtRegion;
  mdtRegion.Clear();

  StatusCode sc = StatusCode::SUCCESS;

  if(!m_use_rpc){

  } else {

    sc = m_rpcDataPreparator->prepareData(p_roids,
                                          rpcLayerClusters,
                                          &m_clusterPatFinder);

    if (!sc.isSuccess()) {
      ATH_MSG_DEBUG("Error in RPC data prepapration and clustering. Continue using RoI");
      return sc;
    }
  }

  auto data = m_recRPCRoiTool->roiData(p_roi->roiWord());
  double roiEtaMinLow = 0.;
  double roiEtaMaxLow = 0.;
  double roiEtaMinHigh = 0.;
  double roiEtaMaxHigh = 0.;
  m_recRPCRoiTool->etaDimLow(data, roiEtaMinLow, roiEtaMaxLow);
  m_recRPCRoiTool->etaDimHigh(data, roiEtaMinHigh, roiEtaMaxHigh);

  sc = m_clusterRoadDefiner->defineRoad(p_roi,
                                        clusterRoad,
                                        rpcLayerClusters,
                                        &m_clusterPatFinder,
                                        clusterFitResults,
                                        roiEtaMinLow,
                                        roiEtaMaxLow,
                                        roiEtaMinHigh,
                                        roiEtaMaxHigh);
  if (!sc.isSuccess()) {
<<<<<<< HEAD
    ATH_MSG_WARNING("Error in clusterRoad definition.");
=======
    ATH_MSG_WARNING("Error in MDT data preparation.");
>>>>>>> release/21.0.127
    return sc;

  }
<<<<<<< HEAD
  if(!clusterRoad.empty()){
    sc = m_mdtDataPreparator->prepareData(p_roids,
                                          clusterFitResults.back(),
                                          clusterRoad.back(),
                                          mdtRegion,
                                          mdtHits_normal);

    if (!sc.isSuccess()) {
      ATH_MSG_WARNING("Error in MDT data preparation.");
      return sc;
    }

    ATH_MSG_DEBUG("nr of MDT (normal)  hits=" << mdtHits_normal.size());
    ATH_MSG_DEBUG("nr of MDT (overlap) hits=" << mdtHits_overlap.size());

    for(int i_road = 0; i_road < (int)clusterRoad.size(); i_road++){
      TrigL2MuonSA::MdtHits mdt_normal;
      for(int i_hit = 0; i_hit < (int)mdtHits_normal.size(); i_hit++){
        unsigned int chamber = mdtHits_normal[i_hit].Chamber;

        if (chamber >= xAOD::L2MuonParameters::MaxChamber) continue;
        double Z = mdtHits_normal[i_hit].Z;
        double R = mdtHits_normal[i_hit].R;
        double residual = 999999;
        int clusterRoadID = 9999;
        for(int j_road = 0; j_road < (int)clusterRoad.size(); j_road++){
          double aw = clusterRoad.at(j_road).aw[chamber][0];
          double bw = clusterRoad.at(j_road).bw[chamber][0];
          double tmp_residual;
          const double ZERO_LIMIT = 1e-4;
          if( std::abs(aw) < ZERO_LIMIT ){
            tmp_residual = R-bw;
          } else {
            double ia  = 1/aw;
            double iaq = ia*ia;
            double dz  = Z - (R-bw)*ia;
            tmp_residual = dz/std::sqrt(1.+iaq);
          }
          if(std::abs(residual) > std::abs(tmp_residual)){
            residual = tmp_residual;
            clusterRoadID = j_road;
          }
        }
        if(clusterRoadID == i_road){
          mdt_normal.push_back(mdtHits_normal[i_hit]);
        }
      }
      mdtHits_cluster_normal.push_back(mdt_normal);
    }
  }

  return StatusCode::SUCCESS;
=======
  ATH_MSG_DEBUG("nr of MDT (normal)  hits=" << mdtHits_normal.size());
  ATH_MSG_DEBUG("nr of MDT (overlap) hits=" << mdtHits_overlap.size());
  
  return StatusCode::SUCCESS; 
>>>>>>> release/21.0.127
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode TrigL2MuonSA::MuFastDataPreparator::prepareData(const LVL1::RecMuonRoI*     p_roi,
                                                           const TrigRoiDescriptor*    p_roids,
                                                           const bool                  insideOut,
                                                           TrigL2MuonSA::TgcHits&      tgcHits,
                                                           TrigL2MuonSA::MuonRoad&     muonRoad,
                                                           TrigL2MuonSA::MdtRegion&    mdtRegion,
                                                           TrigL2MuonSA::TgcFitResult& tgcFitResult,
                                                           TrigL2MuonSA::MdtHits&      mdtHits_normal,
                                                           TrigL2MuonSA::MdtHits&      mdtHits_overlap,
                                                           TrigL2MuonSA::CscHits&      cscHits,
							   TrigL2MuonSA::StgcHits&     stgcHits,
							   TrigL2MuonSA::MmHits&       mmHits) const
{
  StatusCode sc = StatusCode::SUCCESS;
  ATH_MSG_DEBUG("RoI eta/phi=" << p_roi->eta() << "/" << p_roi->phi());
<<<<<<< HEAD

  if(!insideOut) {
    sc = m_tgcDataPreparator->prepareData(p_roi,
                                          tgcHits);
  } else {
    ATH_MSG_DEBUG("Skip TgcDataPreparator");
  }

=======
  
  sc = m_tgcDataPreparator->prepareData(p_roi,
                                        tgcHits);
>>>>>>> release/21.0.127
  if (!sc.isSuccess()) {
    ATH_MSG_DEBUG("Error in TGC data preparation. Continue using RoI");
  }
  ATH_MSG_DEBUG("nr of TGC hits=" << tgcHits.size());
<<<<<<< HEAD

  sc = m_tgcRoadDefiner->defineRoad(p_roids,
                                    insideOut,
=======
  
  sc = m_tgcRoadDefiner->defineRoad(p_roi,
>>>>>>> release/21.0.127
                                    tgcHits,
                                    muonRoad,
                                    tgcFitResult);
  if (!sc.isSuccess()) {
    ATH_MSG_WARNING("Error in road definition.");
    return sc;
  }

  sc = m_mdtDataPreparator->prepareData(p_roids,
                                        tgcFitResult,
                                        muonRoad,
                                        mdtRegion,
                                        mdtHits_normal);

  if (!sc.isSuccess()) {
    ATH_MSG_WARNING("Error in MDT data preparation.");
    return sc;
  }
  ATH_MSG_DEBUG("nr of MDT (normal)  hits=" << mdtHits_normal.size());
  ATH_MSG_DEBUG("nr of MDT (overlap) hits=" << mdtHits_overlap.size());
<<<<<<< HEAD

  if(!m_cscDataPreparator.empty()) {
    sc = m_cscDataPreparator->prepareData(muonRoad,
					  cscHits);
    if (!sc.isSuccess()) {
      ATH_MSG_WARNING("Error in CSC data preparation.");
      return sc;
    }
    ATH_MSG_DEBUG("nr of CSC hits=" << cscHits.size());
  }

  if(!m_stgcDataPreparator.empty()){
    sc = m_stgcDataPreparator->prepareData(p_roids,
					   stgcHits);
    if (!sc.isSuccess()) {
      ATH_MSG_WARNING("Error in sTGC data preparation.");
      return sc;
    }
    ATH_MSG_DEBUG("nr of sTGC hits=" << stgcHits.size());
  }

  if(!m_mmDataPreparator.empty()){
    sc = m_mmDataPreparator->prepareData(p_roids,
					 mmHits);
    if (!sc.isSuccess()) {
      ATH_MSG_WARNING("Error in MM data preparation.");
      return sc;
    }
    ATH_MSG_DEBUG("nr of MM hits=" << mmHits.size());
  }
=======
  
  sc = m_cscDataPreparator->prepareData(p_roids,
                                        muonRoad,
                                        cscHits);
  if (!sc.isSuccess()) {
    ATH_MSG_WARNING("Error in CSC data preparation.");
    return sc;
  }
  ATH_MSG_DEBUG("nr of CSC hits=" << cscHits.size());
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode TrigL2MuonSA::MuFastDataPreparator::prepareData(const xAOD::MuonRoI*        p_roi,
                                                           const TrigRoiDescriptor*    p_roids,
                                                           const bool                  insideOut,
                                                           TrigL2MuonSA::TgcHits&      tgcHits,
                                                           TrigL2MuonSA::MuonRoad&     muonRoad,
                                                           TrigL2MuonSA::MdtRegion&    mdtRegion,
                                                           TrigL2MuonSA::TgcFitResult& tgcFitResult,
                                                           TrigL2MuonSA::MdtHits&      mdtHits_normal,
                                                           TrigL2MuonSA::MdtHits&      mdtHits_overlap,
                                                           TrigL2MuonSA::CscHits&      cscHits,
							   TrigL2MuonSA::StgcHits&     stgcHits,
							   TrigL2MuonSA::MmHits&       mmHits) const
{
<<<<<<< HEAD
  StatusCode sc = StatusCode::SUCCESS;
  ATH_MSG_DEBUG("RoI eta/phi=" << p_roi->eta() << "/" << p_roi->phi());

  if(!insideOut) {
    sc = m_tgcDataPreparator->prepareData(p_roi,
                                          tgcHits);
  } else {
    ATH_MSG_DEBUG("Skip TgcDataPreparator");
  }

  if (!sc.isSuccess()) {
    ATH_MSG_DEBUG("Error in TGC data preparation. Continue using RoI");
  }
  ATH_MSG_DEBUG("nr of TGC hits=" << tgcHits.size());

  sc = m_tgcRoadDefiner->defineRoad(p_roids,
                                    insideOut,
                                    tgcHits,
                                    muonRoad,
                                    tgcFitResult);
  if (!sc.isSuccess()) {
    ATH_MSG_WARNING("Error in road definition.");
    return sc;
  }

  sc = m_mdtDataPreparator->prepareData(p_roids,
                                        tgcFitResult,
                                        muonRoad,
                                        mdtRegion,
                                        mdtHits_normal);

  if (!sc.isSuccess()) {
    ATH_MSG_WARNING("Error in MDT data preparation.");
    return sc;
  }
  ATH_MSG_DEBUG("nr of MDT (normal)  hits=" << mdtHits_normal.size());
  ATH_MSG_DEBUG("nr of MDT (overlap) hits=" << mdtHits_overlap.size());

  if(!m_cscDataPreparator.empty()) {
    sc = m_cscDataPreparator->prepareData(muonRoad,
					  cscHits);
    if (!sc.isSuccess()) {
      ATH_MSG_WARNING("Error in CSC data preparation.");
      return sc;
    }
    ATH_MSG_DEBUG("nr of CSC hits=" << cscHits.size());
  }
=======
  ATH_MSG_DEBUG("Finalizing MuFastDataPreparator - package version " << PACKAGE_VERSION);
   
   StatusCode sc = AthAlgTool::finalize(); 
   if (!sc.isSuccess()) {
     ATH_MSG_ERROR("Could not finalize the AthAlgTool base class.");
     return sc;
   }
   return sc;
}
>>>>>>> release/21.0.127

  if(!m_stgcDataPreparator.empty()){
    sc = m_stgcDataPreparator->prepareData(p_roids,
					   stgcHits);
    if (!sc.isSuccess()) {
      ATH_MSG_WARNING("Error in sTGC data preparation.");
      return sc;
    }
    ATH_MSG_DEBUG("nr of sTGC hits=" << stgcHits.size());
  }

  if(!m_mmDataPreparator.empty()){
    sc = m_mmDataPreparator->prepareData(p_roids,
					 mmHits);
    if (!sc.isSuccess()) {
      ATH_MSG_WARNING("Error in MM data preparation.");
      return sc;
    }
    ATH_MSG_DEBUG("nr of MM hits=" << mmHits.size());
  }

  return StatusCode::SUCCESS;
}
