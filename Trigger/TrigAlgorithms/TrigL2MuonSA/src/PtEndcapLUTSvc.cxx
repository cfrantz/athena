/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "GaudiKernel/ISvcLocator.h"

#include "PtEndcapLUTSvc.h"
#include "PathResolver/PathResolver.h"

#include "AthenaBaseComps/AthMsgStreamMacros.h"

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

TrigL2MuonSA::PtEndcapLUTSvc::PtEndcapLUTSvc(const std::string& name, ISvcLocator* sl) :
<<<<<<< HEAD
  AthService(name,sl)
=======
  AthService(name,sl),
  m_ptEndcapLUT("TrigL2MuonSA::PtEndcapLUT")
>>>>>>> release/21.0.127
{
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode TrigL2MuonSA::PtEndcapLUTSvc::queryInterface(const InterfaceID& riid, void** ppvIF)
{
  if (TrigL2MuonSA::PtEndcapLUTSvc::interfaceID().versionMatch(riid)) {
    *ppvIF = (PtEndcapLUTSvc*)this;
    return StatusCode::SUCCESS;
  } else {
    ATH_MSG_DEBUG(name() << " cannot find the interface! Query the interface of the base class.");
    return AthService::queryInterface(riid, ppvIF);
  }
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

StatusCode TrigL2MuonSA::PtEndcapLUTSvc::initialize()
{   
<<<<<<< HEAD
  // implement the search of LUT trought the pathresolver Tool.
  std::string lut_fileName = PathResolver::find_file(m_lut_fileNameRun2, "DATAPATH");
  ATH_MSG_INFO(lut_fileName);
  
  if (lut_fileName.empty()) {
    ATH_MSG_ERROR("Cannot find EndcapLUT file " << m_lut_fileNameRun2);
    return StatusCode::FAILURE;
  }
  
  ATH_CHECK( m_ptEndcapLUT.retrieve() );
  ATH_MSG_DEBUG("Retrieved service " << m_ptEndcapLUT);

  // read LUT
  ATH_CHECK( m_ptEndcapLUT->readLUT(lut_fileName) );
  ATH_MSG_DEBUG("Read endcap LUT" << m_lut_fileNameRun2);
=======
  StatusCode sc;
  
  sc = AthService::initialize();
  if ( sc.isFailure() ) return sc;
 
  // implement the search of LUT trought the pathresolver Tool.
  std::string lut_fileName = PathResolver::find_file(m_lut_fileName, "DATAPATH");
  ATH_MSG_INFO(lut_fileName);
  
  if (lut_fileName.empty()) {
    ATH_MSG_ERROR("Cannot find EndcapLUT file " << m_lut_fileName);
    return StatusCode::FAILURE;
  }
  
  sc = m_ptEndcapLUT.retrieve();
  if ( sc.isFailure() ) {
    ATH_MSG_ERROR("Could not retrieve " << m_ptEndcapLUT);
    return sc;
  }
  ATH_MSG_DEBUG("Retrieved service " << m_ptEndcapLUT);

  // read LUT
  sc = m_ptEndcapLUT->readLUT(lut_fileName);
  if ( sc.isFailure() ) {
    ATH_MSG_ERROR("Failed to read endcap LUT" << m_lut_fileName);
    return sc;
  }
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

<<<<<<< HEAD
=======
StatusCode TrigL2MuonSA::PtEndcapLUTSvc::finalize()
{
  return StatusCode::SUCCESS;
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
>>>>>>> release/21.0.127
