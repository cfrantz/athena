#include "../TrigMuonEFTrackIsolationTool.h"
<<<<<<< HEAD
#include "../TrigMuonEFTrackIsolationAlgMT.h"
#include "../MuonFilterAlg.h"
#include "../MuonChainFilterAlg.h"
#include "../MergeEFMuonsAlg.h"

DECLARE_COMPONENT( TrigMuonEFTrackIsolationTool )
DECLARE_COMPONENT( TrigMuonEFTrackIsolationAlgMT )
DECLARE_COMPONENT( MuonFilterAlg )
DECLARE_COMPONENT( MuonChainFilterAlg )
DECLARE_COMPONENT( MergeEFMuonsAlg )
=======
#include "../TrigMuonEFTrackIsolation.h"
#include "../TrigMuonEFStandaloneTrackTool.h"
#include "../TrigMuonEFRoiAggregator.h"
#include "../TrigMuonEFFSRoiMaker.h"
#include "../InDetTrkRoiMaker.h"
#include "../TrigMuonEFCaloIsolation.h"
#include "../TrigMuonEFIDTrackRoiMaker.h"

DECLARE_TOOL_FACTORY(TrigMuonEFTrackIsolationTool)
DECLARE_TOOL_FACTORY(TrigMuonEFStandaloneTrackTool)

DECLARE_ALGORITHM_FACTORY(TrigMuonEFTrackIsolation)
DECLARE_ALGORITHM_FACTORY(TrigMuonEFRoiAggregator)
DECLARE_ALGORITHM_FACTORY(InDetTrkRoiMaker)
DECLARE_ALGORITHM_FACTORY(TrigMuonEFCaloIsolation)

DECLARE_ALGORITHM_FACTORY(TrigMuonEFFSRoiMaker)

DECLARE_ALGORITHM_FACTORY(TrigMuonEFIDTrackRoiMaker)

DECLARE_FACTORY_ENTRIES(TrigMuonEF) {
  DECLARE_TOOL( TrigMuonEFTrackIsolationTool )
  DECLARE_TOOL( TrigMuonEFStandaloneTrackTool )

  DECLARE_ALGORITHM( TrigMuonEFTrackIsolation )
  DECLARE_ALGORITHM( TrigMuonEFRoiAggregator )
  DECLARE_ALGORITHM( InDetTrkRoiMaker )
  DECLARE_ALGORITHM( TrigMuonEFFSRoiMaker )
  DECLARE_ALGORITHM( TrigMuonEFCaloIsolation )
  DECLARE_ALGORITHM( TrigMuonEFIDTrackRoiMaker )
}
>>>>>>> release/21.0.127

