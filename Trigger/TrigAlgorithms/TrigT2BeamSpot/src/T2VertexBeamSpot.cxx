/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/
//============================================================
//
<<<<<<< HEAD
=======
// $Id: T2VertexBeamSpot.cxx 760210 2016-07-07 09:33:44Z hartj $
//
>>>>>>> release/21.0.127
// T2VertexBeamSpot.cxx, (c) ATLAS Detector software
// Trigger/TrigAlgorithms/TrigT2BeamSpot/T2VertexBeamSpot
//
// Online beam spot measurement and monitoring using
// L2 recontructed primary vertices.
//
// Authors : David W. Miller, Rainer Bartoldus,   
//           Su Dong
//============================================================

#include "T2VertexBeamSpot.h"

// General ATHENA/Trigger stuff
#include "GaudiKernel/StatusCode.h"
#include "AthenaMonitoringKernel/Monitored.h"

using namespace PESA;

// Constructor
T2VertexBeamSpot::T2VertexBeamSpot( const std::string& name, ISvcLocator* pSvcLocator )
  : AthReentrantAlgorithm(name, pSvcLocator)
{
   declareProperty("doTrackBeamSpot",     m_doTrackBeamSpot  = true);
}

StatusCode T2VertexBeamSpot::initialize() {

   ATH_CHECK(m_beamSpotTool.retrieve());
   ATH_CHECK(m_trackBSTool.retrieve());
   ATH_CHECK(m_monTool.retrieve());

   ATH_CHECK( m_trackCollectionKey.initialize() );

<<<<<<< HEAD
   return StatusCode::SUCCESS;
=======
        impl->eventStage( enoughTracks );

        //-----------------------
        // Vertex reconstruction
        //-----------------------

        // Cluster tracks in z around seed track and reconstruct vertices
        impl->reconstructVertices( mySelectedTrackCollection, myVertexCollection, mySplitVertexCollections );
      }
    else {
        TrackCollection mySelectedTrackCollection;
        mySelectedTrackCollection.clear( SG::VIEW_ELEMENTS );
        // FIXME: TrigInDetTrackCollection neglects to forward the
        // DataVector<TrigInDetTrack>(SG::OwnershipPolicy) constructor

        // Process all TEs (and in turn all ROIs in the TE, all tracks in the ROI)
        impl->processTEs( tes_in, mySelectedTrackCollection );

        //-------------------------
        // Requirements for tracks
        //-------------------------

        // Check for seed tracks (= high-pT tracks)
        if ( impl->m_TotalHiPTTracks < 1 )
        {
          if (msgLvl()<=MSG::DEBUG) msg() << MSG::DEBUG << " No seed tracks for vertex" << endreq;
          break;
        }

        impl->eventStage( hasSeedTrack );

        // Check for the total number of available tracks
        if ( impl->m_TotalTracksPass < impl->m_totalNTrkMin )
        {
          if (msgLvl()<=MSG::DEBUG) msg() << MSG::DEBUG << " Not enough total passed tracks to vertex" << endreq;
          break;
        }

        impl->eventStage( enoughTracks );

        //-----------------------
        // Vertex reconstruction
        //-----------------------

        // Cluster tracks in z around seed track and reconstruct vertices
        impl->reconstructVertices( mySelectedTrackCollection, myVertexCollection, mySplitVertexCollections );
      }
    } while (false);

  //------------------------------------
  // Store output (if any) in the event
  //------------------------------------

  // Add output TEs and attach vertex collections as features if requested
  // JKirk 7/7/16 Need to attach to input TEs
  impl->createOutputTEs( myVertexCollection, mySplitVertexCollections, type_out , tes_in );

  // Stop monitoring
  afterExecMonitors().ignore();

  // Return cause you're done!
  return errorCode;
>>>>>>> release/21.0.127
}

StatusCode T2VertexBeamSpot::execute(const EventContext& ctx) const{
   // Start the overall timer
   auto tTotal = Monitored::Timer("TIME_TotalTime");

   // Retrieve tracks
   SG::ReadHandle<TrackCollection> trackCollection(m_trackCollectionKey, ctx);
   ATH_CHECK(trackCollection.isValid());
   const TrackCollection* tracks = trackCollection.cptr();

   // call track-based tool
   if (m_doTrackBeamSpot) {
      m_trackBSTool->updateBS(*tracks, ctx.eventID());
   }

   // call vertex-based tool
   m_beamSpotTool->updateBS(*tracks, ctx);

   //What should go as an output? SelectedTrackCollection and Vertices?
   //Atm just try add vertex
   //TODO: adding split vertices as well, will need an array

  auto mon = Monitored::Group(m_monTool, tTotal);

   return StatusCode::SUCCESS;
} 
