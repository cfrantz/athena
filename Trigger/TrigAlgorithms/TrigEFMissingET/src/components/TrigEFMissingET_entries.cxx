<<<<<<< HEAD

#include "../TrkMHTFex.h"
#include "../CellFex.h"
#include "../MHTFex.h"
#include "../TCFex.h"
#include "../TCPufitFex.h"
#include "../PFSumFex.h"
#include "../PUSplitPufitFex.h"
#include "../CVFAlg.h"
#include "../CVFPrepAlg.h"
#include "../PFOPrepAlg.h"
#include "../ApproximateTrackToLayerTool.h"
#include "../ExtendTrackToLayerTool.h"
#include "../MHTPufitFex.h"

DECLARE_COMPONENT(HLT::MET::TrkMHTFex)
DECLARE_COMPONENT(HLT::MET::CellFex)
DECLARE_COMPONENT(HLT::MET::MHTFex)
DECLARE_COMPONENT(HLT::MET::TCFex)
DECLARE_COMPONENT(HLT::MET::TCPufitFex)
DECLARE_COMPONENT(HLT::MET::PFSumFex)
DECLARE_COMPONENT(HLT::MET::PUSplitPufitFex)
DECLARE_COMPONENT(HLT::MET::CVFAlg)
DECLARE_COMPONENT(HLT::MET::CVFPrepAlg)
DECLARE_COMPONENT(HLT::MET::PFOPrepAlg)
DECLARE_COMPONENT(ApproximateTrackToLayerTool)
DECLARE_COMPONENT(ExtendTrackToLayerTool)
DECLARE_COMPONENT(HLT::MET::MHTPufitFex)
=======
#include "TrigEFMissingET/EFMissingET.h"
#include "TrigEFMissingET/EFMissingETBaseTool.h"
#include "TrigEFMissingET/EFMissingETFromCells.h"
#include "TrigEFMissingET/EFMissingETFromClusters.h"
#include "TrigEFMissingET/EFMissingETFromClustersPS.h"
#include "TrigEFMissingET/EFMissingETFromClustersPUC.h"
#include "TrigEFMissingET/EFMissingETFromFEBHeader.h"
#include "TrigEFMissingET/EFMissingETFromJets.h"
#include "TrigEFMissingET/EFMissingETFromTrackAndJets.h"
//#include "TrigEFMissingET/EFMissingETFromLvl1Ppr.h"
#include "TrigEFMissingET/EFMissingETFlags.h"
#include "TrigEFMissingET/EFMissingETFromHelper.h"
#include "GaudiKernel/DeclareFactoryEntries.h"

DECLARE_ALGORITHM_FACTORY( EFMissingET )
DECLARE_TOOL_FACTORY( EFMissingETBaseTool )
DECLARE_TOOL_FACTORY( EFMissingETFromCells )
DECLARE_TOOL_FACTORY( EFMissingETFromClusters )
DECLARE_TOOL_FACTORY( EFMissingETFromClustersPS )
DECLARE_TOOL_FACTORY( EFMissingETFromClustersPUC )
DECLARE_TOOL_FACTORY( EFMissingETFromFEBHeader )
DECLARE_TOOL_FACTORY( EFMissingETFromJets )
DECLARE_TOOL_FACTORY( EFMissingETFromTrackAndJets )
//DECLARE_TOOL_FACTORY( EFMissingETFromLvl1Ppr )
DECLARE_TOOL_FACTORY( EFMissingETFlags )
DECLARE_TOOL_FACTORY( EFMissingETFromHelper )

DECLARE_FACTORY_ENTRIES(TrigEFMissingET) {
    DECLARE_ALGORITHM( EFMissingET );
    DECLARE_TOOL( EFMissingETBaseTool );
    DECLARE_TOOL( EFMissingETFromCells );
    DECLARE_TOOL( EFMissingETFromClusters );
    DECLARE_TOOL( EFMissingETFromClustersPS );
    DECLARE_TOOL( EFMissingETFromClustersPUC );
    DECLARE_TOOL( EFMissingETFromFEBHeader );
    DECLARE_TOOL( EFMissingETFromJets );
    DECLARE_TOOL( EFMissingETFromTrackAndJets );
    //DECLARE_TOOL( EFMissingETFromLvl1Ppr );
    DECLARE_TOOL( EFMissingETFlags );
    DECLARE_TOOL( EFMissingETFromHelper );
}
>>>>>>> release/21.0.127
