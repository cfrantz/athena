# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

from AthenaCommon.Logging import logging
log = logging.getLogger( __name__ )

AllowedGroups = ['Muon',
                 'Jet',
                 'BJet',
                 'MET',
                 'Tau',
                 'Egamma',
                 'Bphysics',
                 'BeamSpot',
                 'MinBias',
                 'Detector',
                 'Other',
                 'DISCARD',
                 'ID',
                 'TauOverlay',
                 'ZeroBias',
                 ]



AllowedGroup_HI = ['UltraCentral',
                   'EventShape',
                   'UPC',
                   'MinBiasOverlay',
                   'SingleMuon',
                   'SingleElectron',
                   ]

def getAllAllowedGroups(menu):    
<<<<<<< HEAD:Trigger/TriggerCommon/TriggerMenuMT/python/HLTMenuConfig/Menu/GroupInfo.py
    if 'pp_v6' in menu or 'pp_v7' in menu or 'LS2_v' in menu:
=======
    if 'pp_v6' in menu or 'pp_v7' in menu:
>>>>>>> release/21.0.127:Trigger/TriggerCommon/TriggerMenu/python/menu/GroupInfo.py
        return AllowedGroups
    elif 'HI' in menu:
        return AllowedGroups+AllowedGroup_HI
    else:
        log.error("No list of allowed groups for %s", menu)
