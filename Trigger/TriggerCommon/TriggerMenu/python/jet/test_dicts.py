# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

triggerMenuXML_dicts = [
    # {'EBstep': -1,
    # 'signatures': '',
    # 'stream': ['Main'],
    # 'chainParts': [
    #     {'trigType': 'j',
    #      # 'extra': 'test4',
    #      'extra': '',
    #      'etaRange': '0eta490',
    #      'gscThreshold': '',
    #      # 'threshold': '40',
    #      'threshold': '8',
    #      # 'chainPartName': '2j40_0eta490_invm250_test4',
    #      'chainPartName': '2j8_0eta490_invm250_test4',
    #      'recoAlg': 'a4',
    #      'bTag': '',
    # 'scan': 'FS',
    #      'calib': 'em',
    #      'bMatching': [],
    #      'L1item': '',
    #      'bTracking': '',
    #      'dataType': 'tc',
    #      'jetCalib': 'subjes',
    #      'topo': ['invm250'],
    #      'TLA': '',
    #      'cleaning': 'noCleaning',
    #      'bConfig': [],
    #      'multiplicity': '2',
    #      'signature': 'Jet',
    #      'addInfo': [],
    #      'dataScouting': ''}
    # ],
    # 
    # 'topo': [],
    # 'chainCounter': 842,
    # 'groups': ['RATE:MultiJet', 'BW:Jet'],
    # 'signature': 'Jet',
    # 'topoThreshold': None,
    # 'topoStartFrom': False,
    # 'L1item': 'L1_XE35_MJJ-200',
    # 'chainName': '2j8_0eta490_invm250_test4'},

    # {'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'chainParts': [{'trigType': 'j', 'extra': '', 'etaRange': '0eta490', 'gscThreshold': '', 'threshold': '40', 'chainPartName': '2j40_0eta490_invm250', 'recoAlg': 'a4', 'bTag': '', 'scan': 'FS', 'calib': 'em', 'bMatching': [], 'L1item': '', 'bTracking': '', 'dataType': 'tc', 'jetCalib': 'subjes', 'topo': ['invm250'], 'TLA': '', 'cleaning': 'noCleaning', 'bConfig': [], 'multiplicity': '2', 'signature': 'Jet', 'addInfo': [], 'dataScouting': ''}], 'topo': [], 'chainCounter': 841, 'groups': ['RATE:MultiJet', 'BW:Jet'], 'signature': 'Jet', 'topoThreshold': None, 'topoStartFrom': False, 'L1item': 'L1_XE35_MJJ-200', 'chainName': '2j40_0eta490_invm250'},
    
#    {'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'chainParts': [{'bTag': '', 'scan': 'FS', 'etaRange': '0eta320', 'threshold': '0', 'chainPartName': 'j0_1i2c500m700TLA', 'recoAlg': 'a4', 'trigType': 'j', 'extra': 'test4', 'calib': 'em', 'bMatching': [], 'L1item': '', 'bTracking': '', 'dataType': 'tc', 'jetCalib': 'subjes', 'topo': [], 'TLA': '1i2c500m700TLA', 'cleaning': 'noCleaning', 'bConfig': [], 'multiplicity': '1', 'signature': 'Jet', 'addInfo': [], 'dataScouting': ''}], 'topo': [], 'groups': ['RATE:MultiJet', 'BW:Jet'], 'topoThreshold': None, 'topoStartFrom': False, 'chainCounter': 658, 'signature': 'Jet', 'L1item': 'L1_J100', 'chainName': 'j0_1i2c500m700TLA_test4'},

#    {'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'chainParts': [{'bTag': '', 'scan': 'FS', 'etaRange': '0eta320', 'threshold': '85', 'chainPartName': 'j85', 'recoAlg': 'a4', 'trigType': 'j', 'extra': 'test4', 'calib': 'em', 'bMatching': [], 'L1item': '', 'bTracking': '', 'dataType': 'tc', 'jetCalib': 'subjes', 'topo': [], 'TLA': '', 'cleaning': 'noCleaning', 'bConfig': [], 'multiplicity': '1', 'signature': 'Jet', 'addInfo': [], 'dataScouting': ''}], 'topo': [], 'groups': ['RATE:SingleJet', 'BW:Jet'], 'topoThreshold': None, 'topoStartFrom': False, 'chainCounter': 475, 'signature': 'Jet', 'L1item': 'L1_J20', 'chainName': 'j85_test4'},

#    {'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'chainParts': [{'bTag': '', 'scan': 'FS', 'etaRange': '0eta490', 'threshold': '40', 'chainPartName': '2j40_0eta490_invm250', 'recoAlg': 'a4', 'trigType': 'j', 'extra': 'test4', 'calib': 'em', 'bMatching': [], 'L1item': '', 'bTracking': '', 'dataType': 'tc', 'jetCalib': 'subjes', 'topo': ['invm250'], 'TLA': '', 'cleaning': 'noCleaning', 'bConfig': [], 'multiplicity': '2', 'signature': 'Jet', 'addInfo': [], 'dataScouting': ''}], 'mergingOrder': ['2j40_0eta490_invm250', 'xe80_L1XE70'], 'topo': [], 'mergingOffset': -1, 'groups': ['RATE:JetMET', 'BW:Jets'], 'topoThreshold': None, 'topoStartFrom': False, 'mergingStrategy': 'serial', 'chainCounter': 2354, 'signature': 'Jet', 'L1item': 'L1_XE70', 'chainName': '2j40_0eta490_invm250_xe80_L1XE70_test4'}

    # test 1 set
    # {'EBstep': -1, 'signatures': '', 'stream': ['Main'],
    # 
    #  'chainParts': [
    #     {'trigType': 'j',
    #          'scan': 'FS',
    #          'etaRange':'0eta320',
    #          'threshold': '85',
    #          'chainPartName': 'j85',
    #          'recoAlg': 'a4',
    #          'bTag': '',
    #          # 'extra': 'test1',
    #          'extra': '',
    #          'calib': 'em',
    #          'jetCalib': 'subjesIS',
    #          'L1item': '',
    #          'bTracking': '',
    #          'dataType': 'tc',
    #          'bMatching': [],
    #          'topo': [],
    #          'TLA': '',
    #          'cleaning': 'noCleaning',
    #          'bConfig': [],
    #          'multiplicity': '1',
    #          'signature': 'Jet',
    #          'addInfo': [], 'dataScouting': '',
    #          'smc': 'nosmc',
    #          # 'recoCutUncalib': 'rcc30'
    #      }
    #     ],
    #
    #     'topo': [], 'groups': ['RATE:SingleJet', 'BW:Jet'], 'topoThreshold': None, 'topoStartFrom': False, 'chainCounter': 646, 'signature': 'Jet', 'L1item': 'L1_J20', 'chainName': 'j85'},

    j85 = {'run_rtt_diags':False, 'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'chainParts': [{'smc': 'nosmc', 'trkopt': 'notrk','trigType': 'j', 'scan': 'FS', 'etaRange': '0eta320', 'threshold': '85', 'chainPartName': 'j85', 'recoAlg': 'a4', 'bTag': '', 'extra': '', 'calib': 'em', 'jetCalib': 'subjes', 'L1item': '', 'bTracking': '', 'dataType': 'tc', 'bMatching': [], 'topo': [], 'TLA': '', 'cleaning': 'noCleaning', 'bConfig': [], 'multiplicity': '1', 'signature': 'Jet', 'addInfo': [], 'dataScouting': ''}], 'topo': [], 'groups': ['RATE:SingleJet', 'BW:Jet'], 'topoThreshold': None, 'topoStartFrom': False, 'chainCounter': 475, 'signature': 'Jet', 'L1item': 'L1_J20', 'chainName': 'j85'},

    j460_a10r = {'run_rtt_diags':False, 'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'chainParts': [{'smc': 'nosmc', 'trkopt': 'notrk', 'bTracking': '', 'bTag': '', 'scan': 'FS', 'dataType': 'tc', 'bMatching': [], 'etaRange': '0eta320', 'topo': [], 'TLA': '', 'cleaning': 'noCleaning', 'threshold': '460', 'chainPartName': 'j460_a10r', 'recoAlg': 'a10r', 'trigType': 'j', 'bConfig': [], 'multiplicity': '1', 'extra': '', 'dataScouting': '', 'signature': 'Jet', 'calib': 'em', 'addInfo': [], 'jetCalib': 'subjesIS', 'L1item': ''}], 'topo': [], 'chainCounter': 864, 'groups': ['RATE:SingleJet', 'BW:Jet'], 'signature': 'Jet', 'topoThreshold': None, 'topoStartFrom': False, 'L1item': 'L1_HT150-J20s5.ETA31', 'chainName': 'j460_a10r'},

    _3j30 = {'run_rtt_diags':False, 'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'chainParts': [{'smc': 'nosmc', 'trkopt': 'notrk', 'bTracking': '', 'bTag': '', 'scan': 'FS', 'dataType': 'tc', 'bMatching': [], 'etaRange': '0eta320', 'topo': [], 'TLA': '', 'cleaning': 'noCleaning', 'threshold': '30', 'chainPartName': '3j30', 'recoAlg': 'a4', 'trigType': 'j', 'bConfig': [], 'multiplicity': '3', 'extra': '', 'dataScouting': '', 'signature': 'Jet', 'calib': 'em', 'addInfo': [], 'jetCalib': 'subjesIS', 'L1item': ''}], 'topo': [], 'chainCounter': 783, 'groups': ['RATE:MultiJet', 'BW:Jet'], 'signature': 'Jet', 'topoThreshold': None, 'topoStartFrom': False, 'L1item': 'L1_TE10', 'chainName': '3j30'},

    j150_2j55_boffperf_split = {'run_rtt_diags':False, 'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'chainParts': [{'smc': 'nosmc', 'trkopt': 'notrk', 'bTracking': '', 'bTag': '', 'scan': 'FS', 'dataType': 'tc', 'bMatching': [], 'etaRange': '0eta320', 'topo': [], 'TLA': '', 'cleaning': 'noCleaning', 'threshold': '150', 'chainPartName': 'j150', 'recoAlg': 'a4', 'trigType': 'j', 'bConfig': [], 'multiplicity': '1', 'extra': '', 'dataScouting': '', 'signature': 'Jet', 'calib': 'em', 'addInfo': [], 'jetCalib': 'subjesIS', 'L1item': ''}, {'smc': 'nosmc', 'trkopt': 'notrk', 'bTracking': '', 'bTag': 'boffperf', 'scan': 'FS', 'dataType': 'tc', 'bMatching': [], 'etaRange': '0eta320', 'topo': [], 'TLA': '', 'cleaning': 'noCleaning', 'threshold': '55', 'chainPartName': '2j55_boffperf_split', 'recoAlg': 'a4', 'trigType': 'j', 'bConfig': ['split'], 'multiplicity': '2', 'extra': '', 'dataScouting': '', 'signature': 'Jet', 'calib': 'em', 'addInfo': [], 'jetCalib': 'subjesIS', 'L1item': ''}], 'topo': [], 'chainCounter': 1170, 'groups': ['RATE:MultiBJet', 'BW:BJet'], 'signature': 'Jet', 'topoThreshold': None, 'topoStartFrom': False, 'L1item': 'L1_J75_3J20', 'chainName': 'j150_2j55_boffperf_split'},

    j0_0i1c200m400TLA = {'run_rtt_diags':False, 'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'chainParts': [{'smc': 'nosmc', 'trkopt': 'notrk', 'bTracking': '', 'bTag': '', 'scan': 'FS', 'dataType': 'tc', 'bMatching': [], 'etaRange': '0eta320', 'topo': [], 'TLA': '0i1c200m400TLA', 'cleaning': 'noCleaning', 'threshold': '0', 'chainPartName': 'j0_0i1c200m400TLA', 'recoAlg': 'a4', 'trigType': 'j', 'bConfig': [], 'multiplicity': '1', 'extra': '', 'dataScouting': '', 'signature': 'Jet', 'calib': 'em', 'addInfo': [], 'jetCalib': 'subjesIS', 'L1item': ''}], 'topo': [], 'chainCounter': 732, 'groups': ['RATE:MultiJet', 'BW:Jet'], 'signature': 'Jet', 'topoThreshold': None, 'topoStartFrom': False, 'L1item': 'L1_J100', 'chainName': 'j0_0i1c200m400TLA'},

    j0_0i1c200m400TLA_1 = {'run_rtt_diags':False, 'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'chainParts': [{'recoCutCalib': 'rcc0', 'smc': 'nosmc', 'trkopt': 'notrk', 'bTracking': '', 'bTag': '', 'scan': 'FS', 'dataType': 'tc', 'bMatching': [], 'etaRange': '0eta320', 'topo': [], 'TLA': '0i1c200m400TLA', 'cleaning': 'noCleaning', 'threshold': '0', 'chainPartName': 'j0_0i1c200m400TLA', 'recoAlg': 'a4', 'trigType': 'j', 'bConfig': [], 'multiplicity': '1', 'extra': '', 'dataScouting': '', 'signature': 'Jet', 'calib': 'em', 'addInfo': [], 'jetCalib': 'subjesIS', 'L1item': ''}], 'topo': [], 'chainCounter': 732, 'groups': ['RATE:MultiJet', 'BW:Jet'], 'signature': 'Jet', 'topoThreshold': None, 'topoStartFrom': False, 'L1item': 'L1_J100', 'chainName': 'j0_0i1c200m400TLA'}

    j0_0i1c200m400TLA_2 = {'run_rtt_diags':False, 'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'chainParts': [{'recoCutCalib': 'rcc0', 'recoCutUncalib': 'rcu0', 'smc': 'nosmc', 'trkopt': 'notrk', 'bTracking': '', 'bTag': '', 'scan': 'FS', 'dataType': 'tc', 'bMatching': [], 'etaRange': '0eta320', 'topo': [], 'TLA': '0i1c200m400TLA', 'cleaning': 'noCleaning', 'threshold': '0', 'chainPartName': 'j0_0i1c200m400TLA', 'recoAlg': 'a4', 'trigType': 'j', 'bConfig': [], 'multiplicity': '1', 'extra': '', 'dataScouting': '', 'signature': 'Jet', 'calib': 'em', 'addInfo': [], 'jetCalib': 'subjesIS', 'L1item': ''}], 'topo': [], 'chainCounter': 732, 'groups': ['RATE:MultiJet', 'BW:Jet'], 'signature': 'Jet', 'topoThreshold': None, 'topoStartFrom': False, 'L1item': 'L1_J100', 'chainName': 'j0_0i1c200m400TLA'},

    _2j10_deta20_L1J12 = {'run_rtt_diags':False, 'EBstep': -1, 'signatures': '', 'stream': ['MinBias'], 'chainParts': [{'smc': 'nosmc', 'trkopt': 'notrk', 'bTracking': '', 'bTag': '', 'scan': 'FS', 'dataType': 'tc', 'bMatching': [], 'etaRange': '0eta320', 'topo': ['deta20'], 'TLA': '', 'cleaning': 'noCleaning', 'threshold': '10', 'chainPartName': '2j10_deta20_L1J12', 'recoAlg': 'a4', 'trigType': 'j', 'bConfig': [], 'multiplicity': '2', 'extra': '', 'dataScouting': '', 'signature': 'Jet', 'calib': 'em', 'addInfo': [], 'jetCalib': 'subjesIS', 'L1item': ''}], 'topo': [], 'chainCounter': 721, 'groups': ['BW:MinBias', 'RATE:MinBias'], 'signature': 'Jet', 'topoThreshold': None, 'topoStartFrom': False, 'L1item': 'L1_J12', 'chainName': '2j10_deta20_L1J12'},

    _2j55_bmedium_ht300_L14J20 = {'run_rtt_diags':False, 'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'mergingOffset': -1, 'chainParts': [{'trkopt': 'notrk', 'smc': 'nosmc', 'bTracking': '', 'bTag': '', 'extra': '', 'dataType': 'tc', 'bMatching': [], 'etaRange': '0eta320', 'topo': [], 'TLA': '', 'cleaning': 'noCleaning', 'threshold': '300', 'chainPartName': 'ht300_L14J20', 'recoAlg': 'a4', 'trigType': 'ht', 'bConfig': [], 'multiplicity': '1', 'scan': 'FS', 'L1item': '', 'signature': 'HT', 'calib': 'em', 'addInfo': [], 'jetCalib': 'subjesIS', 'dataScouting': ''}], 'signature': 'HT', 'mergingPreserveL2EFOrder': True, 'topo': [], 'chainCounter': 4032, 'L1item': 'L1_4J20', 'groups': ['RATE:MultiBJet', 'BW:BJet', 'BW:Jet'], 'mergingOrder': ['2j55_bmedium', 'ht300_L14J20'], 'topoThreshold': None, 'topoStartFrom': False, 'mergingStrategy': 'serial', 'chainName': '2j55_bmedium_ht300_L14J20'},

    j460_a10t_lcw_nojcalib_L1J100 = {'run_rtt_diags':False, 'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'chainParts': [{'smc': 'nosmc', 'trkopt': 'notrk', 'bTracking': '', 'bTag': '', 'scan': 'FS', 'dataType': 'tc', 'bMatching': [], 'etaRange': '0eta320', 'topo': [], 'TLA': '', 'cleaning': 'noCleaning', 'threshold': '460', 'chainPartName': 'j460_a10t_lcw_nojcalib_L1J100', 'recoAlg': 'a10t', 'trigType': 'j', 'bConfig': [], 'multiplicity': '1', 'extra': '', 'dataScouting': '', 'signature': 'Jet', 'calib': 'lcw', 'addInfo': [], 'jetCalib': 'nojcalib', 'L1item': ''}], 'topo': [], 'chainCounter': 757, 'groups': ['Rate:SingleJet', 'BW:Jet'], 'signature': 'Jet', 'topoThreshold': None, 'topoStartFrom': False, 'L1item': 'L1_J100', 'chainName': 'j460_a10t_lcw_nojcalib_L1J100'},

    j440_a10r_L1J100 = {'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'chainParts': [{'smc': 'nosmc', 'trkopt': 'notrk', 'bTracking': '', 'bTag': '', 'scan': 'FS', 'dataType': 'tc', 'bMatching': [], 'etaRange': '0eta320', 'topo': [], 'TLA': '', 'cleaning': 'noCleaning', 'threshold': '440', 'chainPartName': 'j440_a10r_L1J100', 'recoAlg': 'a10r', 'trigType': 'j', 'bConfig': [], 'multiplicity': '1', 'extra': '', 'dataScouting': '', 'signature': 'Jet', 'calib': 'em', 'addInfo': [], 'jetCalib': 'subjesIS', 'L1item': ''}], 'topo': [], 'chainCounter': 663, 'groups': ['RATE:SingleJet', 'BW:Jet'], 'signature': 'Jet', 'topoThreshold': None, 'topoStartFrom': False, 'L1item': 'L1_J100', 'chainName': 'j440_a10r_L1J100'},

    j440_a10r_L1J100_1 = {'run_rtt_diags':False,'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'chainParts': [{'recoCutCalib': 'rccDefault', 'smc': 'nosmc', 'trkopt': 'notrk', 'bTracking': '', 'bTag': '', 'scan': 'FS', 'dataType': 'tc', 'bMatching': [], 'etaRange': '0eta320', 'topo': [], 'TLA': '', 'cleaning': 'noCleaning', 'threshold': '440', 'chainPartName': 'j440_a10r_L1J100', 'recoAlg': 'a10r', 'trigType': 'j', 'bConfig': [], 'multiplicity': '1', 'extra': '', 'dataScouting': '', 'signature': 'Jet', 'calib': 'em', 'addInfo': [], 'jetCalib': 'subjesIS', 'L1item': ''}], 'topo': [], 'chainCounter': 663, 'groups': ['RATE:SingleJet', 'BW:Jet'], 'signature': 'Jet', 'topoThreshold': None, 'topoStartFrom': False, 'L1item': 'L1_J100', 'chainName': 'j440_a10r_L1J100'},


    j440_a10r_L1J100_2 = {'run_rtt_diags':False,'EBstep': -1, 'signatures': '', 'stream': ['Main'], 'chainParts': [{'recoCutCalib': 'rcc0', 'smc': 'nosmc', 'trkopt': 'notrk', 'bTracking': '', 'bTag': '', 'scan': 'FS', 'dataType': 'tc', 'bMatching': [], 'etaRange': '0eta320', 'topo': [], 'TLA': '', 'cleaning': 'noCleaning', 'threshold': '440', 'chainPartName': 'j440_a10r_L1J100', 'recoAlg': 'a10r', 'trigType': 'j', 'bConfig': [], 'multiplicity': '1', 'extra': '', 'dataScouting': '', 'signature': 'Jet', 'calib': 'em', 'addInfo': [], 'jetCalib': 'subjesIS', 'L1item': ''}], 'topo': [], 'chainCounter': 663, 'groups': ['RATE:SingleJet', 'BW:Jet'], 'signature': 'Jet', 'topoThreshold': None, 'topoStartFrom': False, 'L1item': 'L1_J100', 'chainName': 'j440_a10r_L1J100'},

]
