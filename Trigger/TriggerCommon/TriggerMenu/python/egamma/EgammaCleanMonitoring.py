# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration


EgammaChainsToKeepMonitoring={
# ES triggers
"e28_lhtight_nod0_ivarloose",
"e24_lhvloose_nod0",
"e28_lhtight_nod0_e15_etcut_L1EM7_Zee",
"e5_lhtight_nod0_e4_etcut_Jpsiee",
"e26_lhtight_idperf",
"g60_loose_L1EM15VH",
"g140_tight",
"g200_loose",
# Combined items
"g35_medium_g25_medium_L12EM20VH",
"g35_loose_g25_loose_L12EM20VH",
"2g22_tight",
"2g20_tight_icalovloose_L12EM15VHI",
"3g20_loose",
"e17_lhloose_nod0_2e10_lhloose_nod0_L1EM15VH_3EM8VH",
"e17_lhloose_nod0_mu14",
"e7_lhmedium_nod0_mu24",
"e12_lhloose_nod0_2mu10",
"2e12_lhloose_nod0_mu10",
"e20_lhmedium_nod0_g35_loose",
"e24_lhmedium_nod0_L1EM15VH_g25_medium",
"e20_lhmedium_nod0_2g10_loose_L1EM15VH_3EM8VH",
# Single items
"e28_lhtight",
"e60_lhmedium_nod0_L1EM24VHI",
"e60_lhmedium_nod0_L1EM24VHIM",
"e17_lhvloose_nod0",
"e12_lhloose_nod0",
"e60_lhmedium_nod0",
"e140_lhloose_nod0",
"g22_tight",
"g20_tight_icalovloose_L1EM15VHI",
"g25_loose_L1EM20VH",
"g25_medium_L1EM20VH",
"g35_loose_L1EM24VHIM",
"g35_medium_L1EM20VH",
"g25_loose",
"g25_medium",
"g35_loose",
"g35_medium",
"g200_loose_L1EM24VHIM",
# Support triggers
"e10_etcut_L1EM7",
"e15_etcut_L1EM7",
"e30_etcut_L1EM15",
"e40_etcut_L1EM15",
"e50_etcut_L1EM15",
"e60_etcut",
"e70_etcut",
"e26_lhmedium_nod0_L1EM20VH",
# HI
"e10_etcut_ion",
"e12_etcut_ion",
"e15_etcut_ion",
"g12_etcut_ion",
"g18_etcut_ion",
"g20_etcut_ion",
"e15_loose_ion",
"e15_medium_ion",
"e15_lhloose_ion",
"e15_lhmedium_ion",
"e18_loose_ion",
"e18_medium_ion",
"e18_lhloose_ion",
"e18_lhmedium_ion",
"2e10_loose_ion",
"e15_loose_ion_idperf",
"e15_medium_ion_idperf",
"e20_etcut",
"e20_loose",
"e20_lhloose",
"e20_etcut_ion",
"e20_loose_ion",
"e20_lhloose_ion",
"g15_loose",
"g15_medium",
"g20_loose",
"g20_medium",
"g15_loose_ion",
"g15_medium_ion",
"g20_loose_ion",
"g20_medium_ion",
"2g15_loose",
"2g15_loose_ion"
}
