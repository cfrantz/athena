<<<<<<< HEAD
/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/
=======
// $Id: TrigBunchCrossingTool_entries.cxx 762070 2016-07-15 08:39:37Z krasznaa $
>>>>>>> release/21.0.127

#include "TrigBunchCrossingTool/StaticBunchCrossingTool.h"
#include "TrigBunchCrossingTool/D3PDBunchCrossingTool.h"
#include "TrigBunchCrossingTool/xAODBunchCrossingTool.h"
#include "TrigBunchCrossingTool/TrigConfBunchCrossingTool.h"

#include "../MCBunchCrossingTool.h"
#include "../LHCBunchCrossingTool.h"

DECLARE_COMPONENT( Trig::StaticBunchCrossingTool )
DECLARE_COMPONENT( Trig::D3PDBunchCrossingTool )
DECLARE_COMPONENT( Trig::xAODBunchCrossingTool )

DECLARE_COMPONENT( Trig::TrigConfBunchCrossingTool )
DECLARE_COMPONENT( Trig::MCBunchCrossingTool )
DECLARE_COMPONENT( Trig::LHCBunchCrossingTool )
