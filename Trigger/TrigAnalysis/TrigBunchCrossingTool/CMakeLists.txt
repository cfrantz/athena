<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
# $Id: CMakeLists.txt 782843 2016-11-07 20:39:27Z krasznaa $
################################################################################
# Package: TrigBunchCrossingTool
################################################################################
>>>>>>> release/21.0.127

# Declare the package name:
atlas_subdir( TrigBunchCrossingTool )

<<<<<<< HEAD
=======
# Declare the package's dependencies:
if( XAOD_STANDALONE )
   atlas_depends_on_subdirs(
      PUBLIC
      Control/AthToolSupport/AsgTools
      Trigger/TrigAnalysis/TrigAnalysisInterfaces
      PRIVATE
      Event/xAOD/xAODEventInfo
      Event/xAOD/xAODTrigger
      Trigger/TrigConfiguration/TrigConfInterfaces
      Trigger/TrigConfiguration/TrigConfL1Data )
else()
   atlas_depends_on_subdirs(
      PUBLIC
      Control/AthToolSupport/AsgTools
      GaudiKernel
      Trigger/TrigAnalysis/TrigAnalysisInterfaces
      PRIVATE
      Control/AthenaKernel
      Database/AthenaPOOL/AthenaPoolUtilities
      Event/xAOD/xAODEventInfo
      Event/xAOD/xAODTrigger
      Trigger/TrigConfiguration/TrigConfInterfaces
      Trigger/TrigConfiguration/TrigConfL1Data )
endif()

>>>>>>> release/21.0.127
# External dependencies:
find_package( CORAL QUIET COMPONENTS CoralBase CoralKernel RelationalAccess )
find_package( ROOT COMPONENTS Net Core Tree MathCore Hist RIO pthread )

# Component(s) in the package:
if( XAOD_STANDALONE )
   atlas_add_library( TrigBunchCrossingTool
      TrigBunchCrossingTool/*.h Root/*.h Root/*.cxx
      Root/json/*.h Root/json/*.inl
      PUBLIC_HEADERS TrigBunchCrossingTool
      LINK_LIBRARIES AsgTools TrigAnalysisInterfaces
      PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} xAODEventInfo
<<<<<<< HEAD
      xAODTrigger TrigConfL1Data TrigConfInterfaces )
else()
   atlas_add_library( TrigBunchCrossingToolLib
      TrigBunchCrossingTool/*.h
      INTERFACE
      PUBLIC_HEADERS TrigBunchCrossingTool
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} AsgTools TrigAnalysisInterfaces TrigConfInterfaces )

=======
      xAODTrigger TrigConfL1Data )
else()
>>>>>>> release/21.0.127
   atlas_add_component( TrigBunchCrossingTool
      TrigBunchCrossingTool/*.h src/*.cxx Root/*.h Root/*.cxx
      Root/json/*.h Root/json/*.inl
      src/components/*.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${CORAL_INCLUDE_DIRS}
<<<<<<< HEAD
      LINK_LIBRARIES ${CORAL_LIBRARIES} AthenaKernel AthenaPoolUtilities CxxUtils
      GaudiKernel TrigBunchCrossingToolLib TrigConfL1Data xAODEventInfo xAODTrigger )
=======
      LINK_LIBRARIES ${ROOT_LIBRARIES} ${CORAL_LIBRARIES} AsgTools
      TrigAnalysisInterfaces GaudiKernel AthenaKernel
      AthenaPoolUtilities xAODEventInfo xAODTrigger TrigConfL1Data )
>>>>>>> release/21.0.127
endif()

atlas_add_test( ut_static_bunch_tool_test
   SOURCES
   test/ut_static_bunch_tool_test.cxx
   Root/BunchCrossing.cxx
   Root/BunchTrain.cxx
   Root/BunchCrossingToolBase.cxx
   Root/StaticBunchCrossingTool.cxx
   Root/count_bunch_neighbors.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} AsgTools xAODEventInfo xAODTrigger
<<<<<<< HEAD
   TrigConfL1Data TrigAnalysisInterfaces )
=======
   TrigConfL1Data )

atlas_add_test( ut_web_bunch_tool_test
   SOURCES
   test/ut_web_bunch_tool_test.cxx
   Root/BunchCrossing.cxx
   Root/BunchTrain.cxx
   Root/BunchCrossingToolBase.cxx
   Root/WebBunchCrossingTool.cxx
   Root/count_bunch_neighbors.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} AsgTools xAODEventInfo xAODTrigger
   TrigConfL1Data )
>>>>>>> release/21.0.127

# Install files from the package:
atlas_install_python_modules( python/*.py POST_BUILD_CMD ${ATLAS_FLAKE8} )
