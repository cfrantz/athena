<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
# $Id: CMakeLists.txt 770884 2016-08-30 18:04:17Z gbesjes $
################################################################################
# Package: TrigTauEmulation
################################################################################
>>>>>>> release/21.0.127

# Declare the package name:
atlas_subdir( TrigTauEmulation )

<<<<<<< HEAD
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs(
  PUBLIC
  Control/AthToolSupport/AsgTools
  Event/xAOD/xAODEventInfo
  Event/xAOD/xAODTau
  Event/xAOD/xAODTracking
  Event/xAOD/xAODTrigger
  PhysicsAnalysis/AnalysisCommon/PATCore
  PhysicsAnalysis/AnalysisCommon/PATInterfaces
  Trigger/TrigAnalysis/TrigDecisionTool
  PRIVATE
  Control/AthContainers
  Control/AthenaBaseComps
  Event/xAOD/xAODBase
  Event/xAOD/xAODCore
  Event/xAOD/xAODJet
  GaudiKernel )

>>>>>>> release/21.0.127
# External dependencies:
find_package( ROOT COMPONENTS Core MathCore Hist )

# Component(s) in the package:
atlas_add_library( TrigTauEmulationLib
   TrigTauEmulation/*.h Root/*.cxx
   PUBLIC_HEADERS TrigTauEmulation
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} AsgTools xAODEventInfo xAODTau xAODTracking
   xAODTrigger PATCoreLib PATInterfaces TrigDecisionToolLib
   PRIVATE_LINK_LIBRARIES AthContainers xAODBase xAODCore )

atlas_add_component( TrigTauEmulation
<<<<<<< HEAD
  src/*.h src/*.cxx src/components/*.cxx
  LINK_LIBRARIES AthenaBaseComps GaudiKernel TrigTauEmulationLib xAODJet )
=======
  TrigTauEmulation/*.h Root/*.cxx src/*.h src/*.cxx src/components/*.cxx
  INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
  LINK_LIBRARIES ${ROOT_LIBRARIES} AsgTools xAODEventInfo xAODTau
  xAODTracking xAODTrigger PATCoreLib PATInterfaces TrigDecisionToolLib
  AthContainers AthenaBaseComps xAODJet xAODBase xAODCore GaudiKernel )
>>>>>>> release/21.0.127

atlas_add_dictionary( TrigTauEmulationDict
  TrigTauEmulation/TrigTauEmulationDict.h
  TrigTauEmulation/selection.xml
<<<<<<< HEAD
  LINK_LIBRARIES TrigTauEmulationLib )
=======
  INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
  LINK_LIBRARIES ${ROOT_LIBRARIES} AsgTools xAODEventInfo xAODTau
  xAODTracking xAODTrigger PATCoreLib PATInterfaces TrigDecisionToolLib
  AthContainers AthenaBaseComps xAODJet xAODBase xAODCore GaudiKernel )
>>>>>>> release/21.0.127

# Install files from the package:
atlas_install_python_modules( python/*.py POST_BUILD_CMD ${ATLAS_FLAKE8} )
