/*
<<<<<<< HEAD
 *   Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
=======
 *   Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
 *   */


#ifndef ITrigEgammaEmulationTool_H_
#define ITrigEgammaEmulationTool_H_

#include "AsgTools/IAsgTool.h"
#include "xAODEgamma/Egamma.h"
#include "xAODTrigCalo/TrigEMCluster.h"
<<<<<<< HEAD
#include "TrigNavStructure/TriggerElement.h"
#include "PATCore/AcceptInfo.h"
#include "PATCore/AcceptData.h"

#include <string>
=======
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODCaloEvent/CaloClusterAuxContainer.h"
#include "xAODTrigger/EmTauRoIContainer.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "PATCore/TAccept.h"
#include <vector>
#include <map>
>>>>>>> release/21.0.127


namespace Trig{
    class ITrigEgammaEmulationTool : public virtual asg::IAsgTool {
        ASG_TOOL_INTERFACE( Trig::ITrigEgammaEmulationTool )

        public:
            virtual StatusCode initialize(void)=0;
            virtual StatusCode execute()=0;
            virtual StatusCode finalize()=0;

            virtual bool EventWiseContainer()=0;

<<<<<<< HEAD
            virtual asg::AcceptData executeTool( const HLT::TriggerElement *, const std::string & )=0;
            virtual asg::AcceptData executeTool( const std::string & )=0;
            virtual bool isPassed(const std::string&)=0;
            virtual bool isPassed(const std::string&, const std::string&)=0;
            virtual const asg::AcceptInfo& getAccept() const =0;
=======
            virtual const Root::TAccept& executeTool( const HLT::TriggerElement *, const std::string & )=0;
            virtual const Root::TAccept& executeTool( const std::string & )=0;
            virtual bool isPassed(const std::string&)=0;
            virtual bool isPassed(const std::string&, const std::string&)=0;
            virtual const Root::TAccept& getAccept()=0;
 
>>>>>>> release/21.0.127
            /* Experimental methods */
            virtual void ExperimentalAndExpertMethods()=0;
            virtual void match( const xAOD::Egamma *, const HLT::TriggerElement *&)=0;
            virtual const HLT::TriggerElement* getTEMatched()=0;
<<<<<<< HEAD
            virtual bool emulationL2Calo(const xAOD::TrigEMCluster *,  std::string)=0;
=======
>>>>>>> release/21.0.127
        private:
    };
}
#endif
