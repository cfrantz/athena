# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( TriggerMatchingTool )

<<<<<<< HEAD
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/AthToolSupport/AsgTools
                          Event/xAOD/xAODBase
                          GaudiKernel
                          Trigger/TrigAnalysis/TrigDecisionTool
                          Trigger/TrigEvent/TrigNavStructure
                          PRIVATE
                          Control/AthAnalysisBaseComps
                          Event/FourMomUtils )


>>>>>>> release/21.0.127
# External dependencies:
find_package( Boost )
find_package( ROOT COMPONENTS Core Hist RIO )

# Component(s) in the package:
atlas_add_library( TriggerMatchingToolLib
<<<<<<< HEAD
   TriggerMatchingTool/*.h Root/*.cxx
   PUBLIC_HEADERS TriggerMatchingTool
   PRIVATE_INCLUDE_DIRS ${Boost_INCLUDE_DIRS}
   LINK_LIBRARIES AsgTools xAODBase TrigNavStructure TrigDecisionToolLib
   PRIVATE_LINK_LIBRARIES ${Boost_LIBRARIES} FourMomUtils xAODTrigger xAODEgamma)

if( NOT XAOD_STANDALONE )
   atlas_add_component( TriggerMatchingTool
      src/*.h src/*.cxx src/components/*.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} GaudiKernel
      AthAnalysisBaseCompsLib TriggerMatchingToolLib )
endif()

atlas_add_dictionary( TriggerMatchingToolDict
   TriggerMatchingTool/TriggerMatchingToolDict.h
   TriggerMatchingTool/selection.xml
   LINK_LIBRARIES TriggerMatchingToolLib )
=======
                   Root/*.cxx
                   src/*.cxx
                   PUBLIC_HEADERS TriggerMatchingTool
                   PRIVATE_INCLUDE_DIRS ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES AsgTools xAODBase GaudiKernel TrigNavStructure TrigDecisionToolLib
                   PRIVATE_LINK_LIBRARIES ${Boost_LIBRARIES} ${ROOT_LIBRARIES} AthAnalysisBaseCompsLib FourMomUtils )

atlas_add_component( TriggerMatchingTool
                     src/components/*.cxx
                     INCLUDE_DIRS ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
                     LINK_LIBRARIES ${Boost_LIBRARIES} ${ROOT_LIBRARIES} AsgTools xAODBase GaudiKernel TrigDecisionToolLib TrigNavStructure AthAnalysisBaseCompsLib FourMomUtils TriggerMatchingToolLib )

atlas_add_dictionary( TriggerMatchingToolDict
                      TriggerMatchingTool/TriggerMatchingToolDict.h
                      TriggerMatchingTool/selection.xml
                      INCLUDE_DIRS ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${Boost_LIBRARIES} ${ROOT_LIBRARIES} AsgTools xAODBase GaudiKernel TrigDecisionToolLib TrigNavStructure AthAnalysisBaseCompsLib FourMomUtils TriggerMatchingToolLib )
>>>>>>> release/21.0.127

# Install files from the package:
atlas_install_joboptions( share/*.py )
