# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( TrigEgammaAnalysisTools )

<<<<<<< HEAD
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/AthToolSupport/AsgTools
                          Event/xAOD/xAODCaloEvent
                          Event/xAOD/xAODEgamma
                          Event/xAOD/xAODEventInfo
                          Event/xAOD/xAODJet
                          Event/xAOD/xAODPrimitives
                          Event/xAOD/xAODTracking
                          Event/xAOD/xAODTrigCalo
                          Event/xAOD/xAODTrigEgamma
                          Event/xAOD/xAODTrigRinger
                          Event/xAOD/xAODTrigger
                          Event/xAOD/xAODTruth
                          LumiBlock/LumiBlockComps
                          PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools
                          Reconstruction/RecoTools/RecoToolInterfaces
                          Reconstruction/egamma/egammaMVACalib
                          Trigger/TrigAnalysis/TrigDecisionTool
                          Trigger/TrigAnalysis/TrigEgammaMatchingTool
                          Trigger/TrigAnalysis/TrigEgammaEmulationTool
                          Trigger/TrigConfiguration/TrigConfHLTData
                          Trigger/TrigEvent/TrigSteeringEvent
                          Trigger/TrigMonitoring/TrigHLTMonitoring
                          PhysicsAnalysis/AnalysisCommon/PATCore
                          PRIVATE
                          Control/AthenaBaseComps
                          Control/AthenaMonitoring
                          Control/StoreGate
                          GaudiKernel
                          Trigger/TrigConfiguration/TrigConfxAOD )

>>>>>>> release/21.0.127
# External dependencies:
find_package( Boost )
find_package( ROOT COMPONENTS Core Hist Tree )

# Component(s) in the package:
atlas_add_library( TrigEgammaAnalysisToolsLib
   TrigEgammaAnalysisTools/*.h Root/*.cxx
   PUBLIC_HEADERS TrigEgammaAnalysisTools
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   PRIVATE_INCLUDE_DIRS ${Boost_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} AsgTools EgammaAnalysisInterfacesLib GaudiKernel LumiBlockCompsLib LumiBlockData PATCoreLib StoreGateLib TrigDecisionToolLib TrigEgammaEmulationToolLib TrigEgammaMatchingToolLib TrigHLTMonitoringLib TrigNavStructure xAODCaloRings xAODEgamma xAODEventInfo xAODJet xAODMissingET xAODTracking xAODTrigCalo xAODTrigger xAODTruth
   PRIVATE_LINK_LIBRARIES ${Boost_LIBRARIES} AthenaMonitoringLib TrigConfxAODLib TrigSteeringEvent )

atlas_add_component( TrigEgammaAnalysisTools
<<<<<<< HEAD
   src/*.cxx
   src/components/*.cxx
   LINK_LIBRARIES AsgTools AthenaBaseComps AthenaMonitoringLib TrigEgammaAnalysisToolsLib xAODEgamma )

# Install files from the package:
atlas_install_python_modules( python/*.py POST_BUILD_CMD ${ATLAS_FLAKE8} )
atlas_install_generic( share/trigEgammaDQ.py share/get_trigEgammaDQ.sh 
   DESTINATION share
   EXECUTABLE )
=======
                     src/*.cxx
                     Root/*.cxx
                     src/components/*.cxx
                     INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
                     LINK_LIBRARIES ${ROOT_LIBRARIES} ${Boost_LIBRARIES} AsgTools xAODCaloEvent xAODEgamma xAODEventInfo xAODJet xAODTracking xAODTrigCalo xAODTrigEgamma xAODTrigRinger xAODTrigger xAODTruth LumiBlockCompsLib ElectronPhotonSelectorToolsLib egammaMVACalibLib TrigDecisionToolLib TrigEgammaMatchingToolLib TrigEgammaEmulationTool TrigConfHLTData TrigSteeringEvent TrigHLTMonitoringLib AthenaBaseComps AthenaMonitoringLib StoreGateLib SGtests GaudiKernel PATCoreLib )

# Install files from the package:
atlas_install_headers( TrigEgammaAnalysisTools )
atlas_install_python_modules( python/TrigEgamma*.py )
atlas_install_joboptions( share/test*.py )
atlas_install_generic( share/trigEgammaDQ.py share/get_trigEgammaDQ.sh 
                        DESTINATION share
                        EXECUTABLE )

>>>>>>> release/21.0.127
