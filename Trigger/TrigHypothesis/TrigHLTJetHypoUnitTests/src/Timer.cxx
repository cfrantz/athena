/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

<<<<<<< HEAD
=======
#include "TrigHLTJetHypo/TrigHLTJetHypoUtils/MaximumBipartiteGroupsMatcher.h"
#include "TrigHLTJetHypo/TrigHLTJetHypoUtils/HypoJetDefs.h"
#include "TrigHLTJetHypo/TrigHLTJetHypoUtils/conditionsFactory2.h"
#include "./TLorentzVectorAsIJet.h"
#include "./TLorentzVectorFactory.h"
#include <TLorentzVector.h>
>>>>>>> release/21.0.127
#include <chrono>
#include <iostream>

using namespace std::chrono;

struct Timer{

  void timeit(){
<<<<<<< HEAD
 
    auto t0 = high_resolution_clock::now();
    int ndo = 1000000;
    int i{0};
    for (int i = 0; i != ndo; ++i){
      i += 1;
      i -= 1;
=======
    HypoJetVector jets;
    TLorentzVectorAsIJet  j0 (m_tl0);
    TLorentzVectorAsIJet  j1 (m_tl1);
    HypoJetVector jets0;
    jets0.push_back(&j0);

    HypoJetVector jets1;
    jets1.push_back(&j1);
    HypoJetGroupVector jetGroups{jets0, jets1};

    std::vector<double> etaMins{-1., -1., -1.};
    std::vector<double> etaMaxs{1., 1, 1.};
    std::vector<double> thresholds{100., 120, 140.};
    std::vector<int> asymmetricEtas{0, 0, 0, 0};

    auto conditions = conditionsFactoryEtaEt(etaMins, 
                                             etaMaxs, 
                                             thresholds,
                                             asymmetricEtas);

    MaximumBipartiteGroupsMatcher matcher(conditions);

    auto t0 = high_resolution_clock::now();
    for(int i = 0; i != 1000000; ++i){
      matcher.match(jetGroups.begin(), jetGroups.end());
>>>>>>> release/21.0.127
    }
    auto t1 = high_resolution_clock::now();
    std::cout << i << '\n';
    std::cout << duration_cast<milliseconds>(t1-t0).count()
	      <<"ms\n";
  }
};

int main(){
  Timer timer;
  timer.timeit();
}
