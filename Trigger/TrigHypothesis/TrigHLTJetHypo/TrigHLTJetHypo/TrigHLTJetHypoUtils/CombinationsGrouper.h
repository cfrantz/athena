/*
<<<<<<< HEAD
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#ifndef TRIGHLTJETHYPO_COMBINATIONSGROUPER_H
#define TRIGHLTJETHYPO_COMBINATIONSGROUPER_H

#include "TrigHLTJetHypo/TrigHLTJetHypoUtils/IJetGrouper.h"

class CombinationsGrouper: public IJetGrouper{
 public:
<<<<<<< HEAD
  CombinationsGrouper();
  CombinationsGrouper(unsigned int);
  CombinationsGrouper(unsigned int, const HypoJetVector&);
  CombinationsGrouper(unsigned int,
		      const HypoJetCIter& b,
		      const HypoJetCIter& e
		      );
  
  virtual HypoJetVector next() override;
  virtual std::string getName() const override; 
  virtual std::string toString() const override;

private:
  unsigned int m_groupSize{0u};
  HypoJetVector m_jets;

=======
  CombinationsGrouper(unsigned int);
  HypoJetGroupVector group(HypoJetIter&,
                           HypoJetIter&) const override;
  std::string getName() const override; 
  std::string toString() const override;
 private:
  unsigned int m_groupSize;
>>>>>>> release/21.0.127
};
#endif
