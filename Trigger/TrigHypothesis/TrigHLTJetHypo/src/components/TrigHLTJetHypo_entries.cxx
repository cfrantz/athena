<<<<<<< HEAD
/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#include "../TrigJetHypoAlgMT.h"
#include "../TrigJetHypoToolConfig_fastreduction.h"
//
#include "../TrigJetConditionConfig_abs_eta.h"
#include "../TrigJetConditionConfig_signed_eta.h"
#include "../TrigJetConditionConfig_phi.h"
#include "../TrigJetConditionConfig_et.h"
#include "../TrigJetConditionConfig_htfr.h"
#include "../TrigJetConditionConfig_dijet_mass.h"
#include "../TrigJetConditionConfig_dijet_dphi.h"
#include "../TrigJetConditionConfig_dijet_deta.h"
#include "../TrigJetConditionConfig_qjet_mass.h"
#include "../TrigJetConditionConfig_smc.h"
#include "../TrigJetConditionConfig_jvt.h"
#include "../TrigJetConditionConfig_acceptAll.h"
#include "../TrigJetConditionConfig_moment.h"
#include "../TrigJetConditionConfig_repeated.h"

//
#include "../TrigJetHypoToolMT.h"
#include "../TrigJetHypoToolHelperNoGrouper.h"
#include "../TrigJetTLAHypoAlgMT.h"
#include "../TrigJetTLAHypoToolMT.h"

DECLARE_COMPONENT(TrigJetHypoToolConfig_fastreduction)
DECLARE_COMPONENT(TrigJetConditionConfig_phi)
DECLARE_COMPONENT(TrigJetConditionConfig_signed_eta)
DECLARE_COMPONENT(TrigJetConditionConfig_abs_eta)
DECLARE_COMPONENT(TrigJetConditionConfig_et)
DECLARE_COMPONENT(TrigJetConditionConfig_htfr)
DECLARE_COMPONENT(TrigJetConditionConfig_dijet_mass)
DECLARE_COMPONENT(TrigJetConditionConfig_dijet_deta)
DECLARE_COMPONENT(TrigJetConditionConfig_dijet_dphi)
DECLARE_COMPONENT(TrigJetConditionConfig_smc)
DECLARE_COMPONENT(TrigJetConditionConfig_jvt)
DECLARE_COMPONENT(TrigJetConditionConfig_acceptAll)
DECLARE_COMPONENT(TrigJetConditionConfig_moment)
DECLARE_COMPONENT(TrigJetConditionConfig_repeated)
DECLARE_COMPONENT(TrigJetConditionConfig_qjet_mass)

DECLARE_COMPONENT(TrigJetHypoAlgMT)
DECLARE_COMPONENT(TrigJetHypoToolMT)
DECLARE_COMPONENT(TrigJetTLAHypoAlgMT)
DECLARE_COMPONENT(TrigJetTLAHypoToolMT)
DECLARE_COMPONENT(TrigJetHypoToolHelperNoGrouper)
=======
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "TrigHLTJetHypo/TrigHLTJetHypo2.h"
#include "TrigHLTJetHypo/TrigEFRazorAllTE.h"
#include "TrigHLTJetHypo/TrigEFDPhiMetJetAllTE.h"
#include "TrigHLTJetHypo/TrigHLTJetHypo_DijetMassDEta.h"
#include "TrigHLTJetHypo/TrigHLTJetHypo_EtaEt.h"
#include "TrigHLTJetHypo/TrigHLTJetHypo_HT.h"
#include "TrigHLTJetHypo/TrigHLTJetHypo_TLA.h"
#include "TrigHLTJetHypo/TrigHLTJetHypo_SMC.h"

DECLARE_ALGORITHM_FACTORY(TrigHLTJetHypo2)

DECLARE_ALGORITHM_FACTORY(TrigEFRazorAllTE)
DECLARE_ALGORITHM_FACTORY(TrigEFDPhiMetJetAllTE)
DECLARE_ALGORITHM_FACTORY(TrigHLTJetHypo_DijetMassDEta)
DECLARE_ALGORITHM_FACTORY(TrigHLTJetHypo_EtaEt)
DECLARE_ALGORITHM_FACTORY(TrigHLTJetHypo_SMC)
DECLARE_ALGORITHM_FACTORY(TrigHLTJetHypo_HT)
DECLARE_ALGORITHM_FACTORY(TrigHLTJetHypo_TLA)

DECLARE_FACTORY_ENTRIES(TrigHLTJetHypo) {
    DECLARE_ALGORITHM(TrigHLTJetHypo2)
    DECLARE_ALGORITHM(TrigEFRazorAllTE)
    DECLARE_ALGORITHM(TrigEFDPhiMetJetAllTE)

    DECLARE_ALGORITHM(TrigHLTJetHypo_DijetMassDEta)
    DECLARE_ALGORITHM(TrigHLTJetHypo_EtaEt)
    DECLARE_ALGORITHM(TrigHLTJetHypo_SMC)
    DECLARE_ALGORITHM(TrigHLTJetHypo_HT)
    DECLARE_ALGORITHM(TrigHLTJetHypo_TLA)
}
>>>>>>> release/21.0.127

