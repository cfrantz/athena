/*
<<<<<<< HEAD
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#include "TrigHLTJetHypo/TrigHLTJetHypoUtils/AllJetsGrouper.h"

<<<<<<< HEAD
AllJetsGrouper:: AllJetsGrouper(){}

AllJetsGrouper:: AllJetsGrouper(const HypoJetCIter& b,
				const HypoJetCIter& e): m_jets(b, e){
}

AllJetsGrouper:: AllJetsGrouper(const HypoJetVector& jets): m_jets{jets}{
}



HypoJetVector AllJetsGrouper::next(){
  if (m_done) {return HypoJetVector{};}
  
  m_done = true;
  return HypoJetVector{m_jets};
=======
HypoJetGroupVector AllJetsGrouper::group(HypoJetIter& begin,
                                           HypoJetIter& end
                                           ) const {
  return HypoJetGroupVector{HypoJetVector(begin, end)};
>>>>>>> release/21.0.127
}

std::string AllJetsGrouper::getName() const {
  return "AllJetsGrouper";
}

std::string AllJetsGrouper::toString() const {
  return "AllJetsGrouper - repack all input jets into vector of vectors, sole element is vector of all jets";
}


