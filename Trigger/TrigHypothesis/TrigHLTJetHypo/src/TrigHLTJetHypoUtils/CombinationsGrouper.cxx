/*
<<<<<<< HEAD
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#include "TrigHLTJetHypo/TrigHLTJetHypoUtils/CombinationsGrouper.h"
#include "TrigHLTJetHypo/TrigHLTJetHypoUtils/CombinationsGen.h"

#include <sstream>

<<<<<<< HEAD
CombinationsGrouper::CombinationsGrouper(){}

CombinationsGrouper::CombinationsGrouper(unsigned int groupSize):
  m_groupSize{groupSize}{}


CombinationsGrouper::CombinationsGrouper(unsigned int groupSize,
					 const HypoJetVector& jets):
  m_groupSize(groupSize), m_jets{jets}{
}


CombinationsGrouper::CombinationsGrouper(unsigned int groupSize,
					 const HypoJetCIter& b,
					 const HypoJetCIter& e):
  m_groupSize(groupSize), m_jets{b, e}{
}


HypoJetVector CombinationsGrouper::next() {
  HypoJetGroupVector hjgv;
  
  // create a combinations generator. Used to select the jets
  // to be tested by the condition objects
  CombinationsGen combgen(m_jets.size(), m_groupSize);
  
  auto combs = combgen.next();
  if (combs.second == false){
    return HypoJetVector{};
  }
  
  HypoJetVector v;
  for(auto i : combs.first){ v.push_back(*(m_jets.begin() + i));}
  
  return HypoJetVector(v);
=======
CombinationsGrouper::CombinationsGrouper(unsigned int groupSize):
  m_groupSize(groupSize){
}


HypoJetGroupVector CombinationsGrouper::group(HypoJetIter& begin,
                                              HypoJetIter& end) const {
  HypoJetGroupVector result;
  
  // create a combinations generator. Used to select the jets
  // to be tested by the condition objects
  CombinationsGen combgen(end-begin, m_groupSize);
  
  while(true){
    auto combs = combgen.next();
    if (combs.second == false) {break;}
    
    HypoJetVector v;
    for(auto i : combs.first){ v.push_back(*(begin + i));}
    result.push_back(v);
  }

  return result;
>>>>>>> release/21.0.127
}

std::string CombinationsGrouper::getName() const {
  return "CombinationsGrouper";
}

std::string CombinationsGrouper::toString() const {
<<<<<<< HEAD

  std::stringstream ss;

  ss << "CombinationsGrouper - create all combinations of ";
  ss << "jets of length " ;
  ss << m_groupSize << '\n';

=======
  std::stringstream ss;
  ss << "CombinationsGrouper - create all combinations of ";
  ss << "jets of length " ;
  ss << m_groupSize << '\n';
>>>>>>> release/21.0.127
  return ss.str();
}


