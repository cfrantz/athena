/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include <ZdcAnalysis/ZDCDataAnalyzer.h>
#include <ZdcAnalysis/ZDCPulseAnalyzer.h>

#include <sstream>

ZDCDataAnalyzer::ZDCDataAnalyzer(int nSample, float deltaTSample, size_t preSampleIdx, std::string fitFunction,
				 const ZDCModuleFloatArray& peak2ndDerivMinSamples, 
				 const ZDCModuleFloatArray& peak2ndDerivMinThresholdsHG, 
				 const ZDCModuleFloatArray& peak2ndDerivMinThresholdsLG,
				 bool forceLG) : 
<<<<<<< HEAD
  m_nSample(nSample), m_deltaTSample(deltaTSample), m_preSampleIdx(preSampleIdx),
  m_fitFunction(fitFunction),
  m_forceLG(forceLG),
  m_debugLevel(-1),
  m_eventCount(0),
  m_haveECalib(false),
  m_haveT0Calib(false),
  m_currentLB(-1),
  m_moduleMask(0),
  m_moduleSum({{0, 0}}), 
  m_moduleSumErrSq({{0, 0}}), 
  m_moduleSumPreSample({{0,0}}),
  m_calibModuleSum({{0, 0}}), 
  m_calibModuleSumErrSq({{0,0}}),
  m_averageTime({{0, 0}}), 
  m_fail({{false, false}})
{
  m_moduleDisabled[0] = {{false, false, false, false}};
  m_moduleDisabled[1] = {{false, false, false, false}};

  m_moduleAnalyzers[0] = {{0, 0, 0, 0}};
  m_moduleAnalyzers[1] = {{0, 0, 0, 0}};

  m_calibAmplitude[0] = {{0, 0, 0, 0}};
  m_calibAmplitude[1] = {{0, 0, 0, 0}};
  
  m_calibTime[0] = {{0, 0, 0, 0}};
  m_calibTime[1] = {{0, 0, 0, 0}};

  m_dataLoaded[0] = {{false, false, false, false}};
  m_dataLoaded[1] = {{false, false, false, false}};

  // For now we are using hard-coded gain factors and pedestals
  //
  m_HGGains[0] = {{9.51122, 9.51980, 9.51122, 9.51122}};
  m_HGGains[1] = {{9.50842, 9.49662, 9.50853, 9.50842}};
  
  m_pedestals[0] = {{100, 100, 100, 100}};
  m_pedestals[1] = {{100, 100, 100, 100}};
  
  // Default "calibrations"
  //
  m_currentECalibCoeff = {{{{1, 1, 1, 1}}, {{1, 1, 1, 1}}}};

  m_currentT0OffsetsHG = {{{{0, 0, 0, 0}}, {{0, 0, 0, 0}}}};
  m_currentT0OffsetsLG = {{{{0, 0, 0, 0}}, {{0, 0, 0, 0}}}};
=======
  _nSample(nSample), _deltaTSample(deltaTSample), _preSampleIdx(preSampleIdx),
  _fitFunction(fitFunction),
  _forceLG(forceLG),
  _debugLevel(-1),
  _eventCount(0),
  _moduleMask(0),
  _moduleSum({0, 0}), 
  _moduleSumErrSq({0, 0}), 
  _moduleSumPreSample({0,0}),
  _calibModuleSum({0, 0}), 
  _calibModuleSumErrSq({0,0}),
  _averageTime({0, 0}), 
  _fail({false, false}),
  _haveT0Calib(false),
  _haveECalib(false),
  _currentLB(-1)
{
  _moduleDisabled[0] = {false, false, false, false};
  _moduleDisabled[1] = {false, false, false, false};

  _moduleAnalyzers[0] = {0, 0, 0, 0};
  _moduleAnalyzers[1] = {0, 0, 0, 0};

  _calibAmplitude[0] = {0, 0, 0, 0};
  _calibAmplitude[1] = {0, 0, 0, 0};
  
  _calibTime[0] = {0, 0, 0, 0};
  _calibTime[1] = {0, 0, 0, 0};

  // For now we are using hard-coded gain factors and pedestals
  //
  _HGGains[0] = {9.51122, 9.51980, 9.51122, 9.51122};
  _HGGains[1] = {9.50842, 9.49662, 9.50853, 9.50842};
  
  _pedestals[0] = {100, 100, 100, 100};
  _pedestals[1] = {100, 100, 100, 100};
  
  // Default "calibrations"
  //
  _currentECalibCoeff = {1, 1, 1, 1, 1, 1, 1, 1};

  _currentT0OffsetsHG = {0, 0, 0, 0, 0, 0, 0, 0};
  _currentT0OffsetsLG = {0, 0, 0, 0, 0, 0, 0, 0};
>>>>>>> release/21.0.127

  // Construct the per-module pulse analyzers
  //
  for (size_t side : {0, 1}) {
    for (size_t module : {0, 1, 2, 3}) {
      std::ostringstream moduleTag;
      moduleTag << "_s" << side << "_m" << module;
      
<<<<<<< HEAD
      m_moduleAnalyzers[side][module] = new ZDCPulseAnalyzer(moduleTag.str().c_str(), m_nSample, m_deltaTSample, m_preSampleIdx, 
							    m_pedestals[side][module], m_HGGains[side][module], m_fitFunction, 
							    peak2ndDerivMinSamples[side][module], 
							    peak2ndDerivMinThresholdsHG[side][module],
							    peak2ndDerivMinThresholdsLG[side][module]);
      if (m_forceLG) m_moduleAnalyzers[side][module]->SetForceLG(true);
=======
      _moduleAnalyzers[side][module] = new ZDCPulseAnalyzer(moduleTag.str().c_str(), _nSample, _deltaTSample, _preSampleIdx, 
							    _pedestals[side][module], _HGGains[side][module], _fitFunction, 
							    peak2ndDerivMinSamples[side][module], 
							    peak2ndDerivMinThresholdsHG[side][module],
							    peak2ndDerivMinThresholdsLG[side][module]);
      if (_forceLG) _moduleAnalyzers[side][module]->SetForceLG(true);
>>>>>>> release/21.0.127
    }
  }

  // By default we perform quiet pulse fits
  //
  ZDCPulseAnalyzer::SetQuietFits(true);
}

ZDCDataAnalyzer::~ZDCDataAnalyzer() 
{
  //  Delete the per-module pulse analyzers
  //
  for (size_t side : {0, 1}) {
    for (size_t module : {0, 1, 2, 3}) {
<<<<<<< HEAD
      if (!m_moduleAnalyzers[side][module]) delete m_moduleAnalyzers[side][module];
=======
      if (!_moduleAnalyzers[side][module]) delete _moduleAnalyzers[side][module];
>>>>>>> release/21.0.127
    }
  }
}

bool ZDCDataAnalyzer::DisableModule(size_t side, size_t module)
{
  if (side < 2 && module < 4) {
    //
    // Can't disable in the middle of analysis
    //
<<<<<<< HEAD
    if (m_dataLoaded[side][module]) return false;
    else {
      m_moduleDisabled[side][module] = true;
      return true;
    }
  }
  return false;
=======
    if (_dataLoaded[side][module]) return false;
    else {
      _moduleDisabled[side][module] = true;
      return true;
    }
  }
  else {
    return false;
  }
>>>>>>> release/21.0.127
}

void ZDCDataAnalyzer::EnableDelayed(float deltaT, const ZDCModuleFloatArray& undelayedDelayedPedestalDiff)
{
  for (size_t side : {0, 1}) {
    for (size_t module : {0, 1, 2, 3}) {
<<<<<<< HEAD
      m_moduleAnalyzers[side][module]->EnableDelayed(deltaT, undelayedDelayedPedestalDiff[side][module]);
=======
      _moduleAnalyzers[side][module]->EnableDelayed(deltaT, undelayedDelayedPedestalDiff[side][module]);
>>>>>>> release/21.0.127
    }
  }
}

void ZDCDataAnalyzer::SetPeak2ndDerivMinTolerances(size_t tolerance) {
  for (size_t side : {0, 1}) {
    for (size_t module : {0, 1, 2, 3}) {
<<<<<<< HEAD
      m_moduleAnalyzers[side][module]->SetPeak2ndDerivMinTolerance(tolerance);
=======
      _moduleAnalyzers[side][module]->SetPeak2ndDerivMinTolerance(tolerance);
>>>>>>> release/21.0.127
    }
  }
}

void ZDCDataAnalyzer::SetTauT0Values(const ZDCModuleBoolArray& fixTau1, const ZDCModuleBoolArray& fixTau2, 
				     const ZDCModuleFloatArray& tau1, const ZDCModuleFloatArray& tau2, 
				     const ZDCModuleFloatArray& t0HG, const ZDCModuleFloatArray& t0LG)
{
  for (size_t side : {0, 1}) {
    for (size_t module : {0, 1, 2, 3}) {
<<<<<<< HEAD
      m_moduleAnalyzers[side][module]->SetTauT0Values(fixTau1[side][module], fixTau2[side][module], 
=======
      _moduleAnalyzers[side][module]->SetTauT0Values(fixTau1[side][module], fixTau2[side][module], 
>>>>>>> release/21.0.127
						     tau1[side][module], tau2[side][module], t0HG[side][module], t0LG[side][module]);
    }
  }
}

void ZDCDataAnalyzer::SetADCOverUnderflowValues(const ZDCModuleFloatArray& HGOverflowADC, const ZDCModuleFloatArray& HGUnderflowADC, 
						const ZDCModuleFloatArray& LGOverflowADC)
{
  for (size_t side : {0, 1}) {
    for (size_t module : {0, 1, 2, 3}) {
<<<<<<< HEAD
      m_moduleAnalyzers[side][module]->SetADCOverUnderflowValues(HGOverflowADC[side][module], HGUnderflowADC[side][module], LGOverflowADC[side][module]);
=======
      _moduleAnalyzers[side][module]->SetADCOverUnderflowValues(HGOverflowADC[side][module], HGUnderflowADC[side][module], LGOverflowADC[side][module]);
>>>>>>> release/21.0.127
    }
  }
}

void ZDCDataAnalyzer::SetCutValues(const ZDCModuleFloatArray& chisqDivAmpCutHG, const ZDCModuleFloatArray& chisqDivAmpCutLG,
				   const ZDCModuleFloatArray& deltaT0MinHG, const ZDCModuleFloatArray& deltaT0MaxHG, 
				   const ZDCModuleFloatArray&  deltaT0MinLG, const ZDCModuleFloatArray& deltaT0MaxLG)
{
  for (size_t side : {0, 1}) {
    for (size_t module : {0, 1, 2, 3}) {
<<<<<<< HEAD
      m_moduleAnalyzers[side][module]->SetCutValues(chisqDivAmpCutHG[side][module], chisqDivAmpCutLG[side][module],
=======
      _moduleAnalyzers[side][module]->SetCutValues(chisqDivAmpCutHG[side][module], chisqDivAmpCutLG[side][module],
>>>>>>> release/21.0.127
						   deltaT0MinHG[side][module], deltaT0MaxHG[side][module], 
						   deltaT0MinLG[side][module], deltaT0MaxLG[side][module]);
    }
  }
}

void ZDCDataAnalyzer::SetTimingCorrParams(const std::array<std::array<std::vector<float>, 4>, 2>& HGParamArr,
					  const std::array<std::array<std::vector<float>, 4>, 2>& LGParamArr)
{
  for (size_t side : {0, 1}) {
    for (size_t module : {0, 1, 2, 3}) {
<<<<<<< HEAD
      m_moduleAnalyzers[side][module]->SetTimingCorrParams(HGParamArr.at(side).at(module), LGParamArr.at(side).at(module)); 
=======
      _moduleAnalyzers[side][module]->SetTimingCorrParams(HGParamArr.at(side).at(module), LGParamArr.at(side).at(module)); 
>>>>>>> release/21.0.127
    }
  }

}

void ZDCDataAnalyzer::SetNonlinCorrParams(const std::array<std::array<std::vector<float>, 4>, 2>& HGNonlinCorrParams)
{
  for (size_t side : {0, 1}) {
    for (size_t module : {0, 1, 2, 3}) {
<<<<<<< HEAD
      m_moduleAnalyzers[side][module]->SetNonlinCorrParams(HGNonlinCorrParams.at(side).at(module));
=======
      _moduleAnalyzers[side][module]->SetNonlinCorrParams(HGNonlinCorrParams.at(side).at(module));
>>>>>>> release/21.0.127
    }
  }
}

void ZDCDataAnalyzer::StartEvent(int lumiBlock) 
{
<<<<<<< HEAD
  if (m_debugLevel > 0) {
    std::cout << "Starting new event, event index = " << m_eventCount << std::endl;
=======
  if (_debugLevel > 0) {
    std::cout << "Starting new event, event index = " << _eventCount << std::endl;
>>>>>>> release/21.0.127
  }

  //  See if we have to load up new calibrations
  //
<<<<<<< HEAD
  if (lumiBlock != m_currentLB) {
    if (m_debugLevel > 0) {
      std::cout << "Starting new luminosity block " << lumiBlock << std::endl;
    }

    if (m_haveECalib) {
      if (m_debugLevel > 1) {
	std::cout << "Loading energy calibrations for event " << m_eventCount << ", lumi block " << lumiBlock << std::endl;
=======
  if (lumiBlock != _currentLB) {
    if (_debugLevel > 0) {
      std::cout << "Starting new luminosity block " << lumiBlock << std::endl;
    }

    if (_haveECalib) {
      if (_debugLevel > 1) {
	std::cout << "Loading energy calibrations for event " << _eventCount << ", lumi block " << lumiBlock << std::endl;
>>>>>>> release/21.0.127
      }

      for (size_t side : {0, 1}) {
	for (size_t module : {0, 1, 2, 3}) {
<<<<<<< HEAD
	  float splineLBMin = m_LBDepEcalibSplines[side][module]->GetXmin();
	  float splineLBMax = m_LBDepEcalibSplines[side][module]->GetXmax();
	  
	  if (lumiBlock >= splineLBMin && lumiBlock <= splineLBMax) { 
	    m_currentECalibCoeff[side][module] = m_LBDepEcalibSplines[side][module]->Eval(lumiBlock);
	  }
	  else if (lumiBlock < splineLBMin) {
	    m_currentECalibCoeff[side][module] = m_LBDepEcalibSplines[side][module]->Eval(splineLBMin);
	  }
	  else {
	    m_currentECalibCoeff[side][module] = m_LBDepEcalibSplines[side][module]->Eval(splineLBMax);
=======
	  float splineLBMin = _LBDepEcalibSplines[side][module]->GetXmin();
	  float splineLBMax = _LBDepEcalibSplines[side][module]->GetXmax();
	  
	  if (lumiBlock >= splineLBMin && lumiBlock <= splineLBMax) { 
	    _currentECalibCoeff[side][module] = _LBDepEcalibSplines[side][module]->Eval(lumiBlock);
	  }
	  else if (lumiBlock < splineLBMin) {
	    _currentECalibCoeff[side][module] = _LBDepEcalibSplines[side][module]->Eval(splineLBMin);
	  }
	  else {
	    _currentECalibCoeff[side][module] = _LBDepEcalibSplines[side][module]->Eval(splineLBMax);
>>>>>>> release/21.0.127
	  }
	}
      }
    } // end of if (_haveEcalib) {

<<<<<<< HEAD
    if (m_haveT0Calib) {
      if (m_debugLevel > 1) {
	std::cout << "Loading timing calibrations for event " << m_eventCount << ", lumi block " << lumiBlock << std::endl;
=======
    if (_haveT0Calib) {
      if (_debugLevel > 1) {
	std::cout << "Loading timing calibrations for event " << _eventCount << ", lumi block " << lumiBlock << std::endl;
>>>>>>> release/21.0.127
      }

      for (size_t side : {0, 1}) {
	for (size_t module : {0, 1, 2, 3}) {
<<<<<<< HEAD
	  float splineLBMin = m_T0HGOffsetSplines[side][module]->GetXmin();
	  float splineLBMax = m_T0HGOffsetSplines[side][module]->GetXmax();
	  
	  if (lumiBlock >= splineLBMin && lumiBlock <= splineLBMax) { 
	    m_currentT0OffsetsHG[side][module] = m_T0HGOffsetSplines[side][module]->Eval(lumiBlock);
	    m_currentT0OffsetsLG[side][module] = m_T0LGOffsetSplines[side][module]->Eval(lumiBlock);
	  }
	  else if (lumiBlock < splineLBMin) {
	    m_currentT0OffsetsHG[side][module] = m_T0HGOffsetSplines[side][module]->Eval(splineLBMin);
	    m_currentT0OffsetsLG[side][module] = m_T0LGOffsetSplines[side][module]->Eval(splineLBMin);
	  }
	  else {
	    m_currentT0OffsetsHG[side][module] = m_T0HGOffsetSplines[side][module]->Eval(splineLBMax);
	    m_currentT0OffsetsLG[side][module] = m_T0LGOffsetSplines[side][module]->Eval(splineLBMax);
	  }
	}
      }
    } // end of if (m_haveT0Calib)
=======
	  float splineLBMin = _T0HGOffsetSplines[side][module]->GetXmin();
	  float splineLBMax = _T0HGOffsetSplines[side][module]->GetXmax();
	  
	  if (lumiBlock >= splineLBMin && lumiBlock <= splineLBMax) { 
	    _currentT0OffsetsHG[side][module] = _T0HGOffsetSplines[side][module]->Eval(lumiBlock);
	    _currentT0OffsetsLG[side][module] = _T0LGOffsetSplines[side][module]->Eval(lumiBlock);
	  }
	  else if (lumiBlock < splineLBMin) {
	    _currentT0OffsetsHG[side][module] = _T0HGOffsetSplines[side][module]->Eval(splineLBMin);
	    _currentT0OffsetsLG[side][module] = _T0LGOffsetSplines[side][module]->Eval(splineLBMin);
	  }
	  else {
	    _currentT0OffsetsHG[side][module] = _T0HGOffsetSplines[side][module]->Eval(splineLBMax);
	    _currentT0OffsetsLG[side][module] = _T0LGOffsetSplines[side][module]->Eval(splineLBMax);
	  }
	}
      }
    } // end of if (_haveT0Calib)
>>>>>>> release/21.0.127
  }

  // Initialize transient results
  //
  for (size_t side : {0, 1}) {
    for (size_t module : {0, 1, 2, 3}) {
<<<<<<< HEAD
      m_dataLoaded[side][module] = false;
      m_moduleStatus[side][module] = 0;
      m_calibAmplitude[side][module] = 0;
      m_calibTime[side][module] = 0;
      //      _moduleFail[side][module] = false;
    }

    m_moduleSum[side] = 0;
    m_moduleSumErrSq[side] = 0;
    m_moduleSumPreSample[side] = 0;

    m_calibModuleSum[side] = 0;
    m_calibModuleSumErrSq[side] = 0;

    m_averageTime[side] = 0;
    m_fail[side] = false;
  }

  m_moduleMask = 0;
  m_currentLB = lumiBlock;
=======
      _dataLoaded[side][module] = false;
      _moduleStatus[side][module] = 0;
      _calibAmplitude[side][module] = 0;
      _calibTime[side][module] = 0;
      //      _moduleFail[side][module] = false;
    }

    _moduleSum[side] = 0;
    _moduleSumErrSq[side] = 0;
    _moduleSumPreSample[side] = 0;

    _calibModuleSum[side] = 0;
    _calibModuleSumErrSq[side] = 0;

    _averageTime[side] = 0;
    _fail[side] = false;
  }

  _moduleMask = 0;
  _currentLB = lumiBlock;
>>>>>>> release/21.0.127
}

void ZDCDataAnalyzer::LoadAndAnalyzeData(size_t side, size_t module, const std::vector<float> HGSamples, const std::vector<float> LGSamples)
{
  // We immediately return if this module is disabled
  //
<<<<<<< HEAD
  if (m_moduleDisabled[side][module]) {
    if (m_debugLevel > 2) {
      std::cout << "Skipping analysis of disabled mofule for event index " << m_eventCount << ", side, module = " << side << ", " << module << std::endl;
=======
  if (_moduleDisabled[side][module]) {
    if (_debugLevel > 2) {
      std::cout << "Skipping analysis of disabled mofule for event index " << _eventCount << ", side, module = " << side << ", " << module << std::endl;
>>>>>>> release/21.0.127
    }

    return;
  }

<<<<<<< HEAD
  if (m_debugLevel > 1) {
    std::cout << "/n Loading data for event index " << m_eventCount << ", side, module = " << side << ", " << module << std::endl;
    
    if (m_debugLevel > 2) {
      std::cout << " Number of HG and LG samples = " << HGSamples.size() << ", " << LGSamples.size() << std::endl;
      if (m_debugLevel > 3) {
=======
  if (_debugLevel > 1) {
    std::cout << "/n Loading data for event index " << _eventCount << ", side, module = " << side << ", " << module << std::endl;
    
    if (_debugLevel > 2) {
      std::cout << " Number of HG and LG samples = " << HGSamples.size() << ", " << LGSamples.size() << std::endl;
      if (_debugLevel > 3) {
>>>>>>> release/21.0.127
	for (size_t sample = 0; sample < HGSamples.size(); sample++) {
	  std::cout << "HGSample[" << sample << "] = " << HGSamples[sample] << std::endl;
	}
      }
    }
  }

<<<<<<< HEAD
  ZDCPulseAnalyzer* pulseAna_p = m_moduleAnalyzers[side][module];

  bool result = pulseAna_p->LoadAndAnalyzeData(HGSamples, LGSamples);
  m_dataLoaded[side][module] = true;
=======
  ZDCPulseAnalyzer* pulseAna_p = _moduleAnalyzers[side][module];

  bool result = pulseAna_p->LoadAndAnalyzeData(HGSamples, LGSamples);
  _dataLoaded[side][module] = true;
>>>>>>> release/21.0.127

  if (result) {
    if (!pulseAna_p->BadT0() && !pulseAna_p->BadChisq()) {
      int moduleMaskBit = 4*side + module;
<<<<<<< HEAD
      m_moduleMask |= 1<< moduleMaskBit;
=======
      _moduleMask |= 1<< moduleMaskBit;
>>>>>>> release/21.0.127
      
      float amplitude = pulseAna_p->GetAmplitude();
      float ampError = pulseAna_p->GetAmpError();

<<<<<<< HEAD
      m_calibAmplitude[side][module] = amplitude*m_currentECalibCoeff[side][module];

      float calibAmpError = ampError * m_currentECalibCoeff[side][module];

      float timeCalib = pulseAna_p->GetT0Corr();
      if (pulseAna_p->UseLowGain()) timeCalib -= m_currentT0OffsetsLG[side][module];
      else timeCalib -= m_currentT0OffsetsHG[side][module];

      m_calibTime[side][module] = timeCalib;

      m_moduleSum[side] += amplitude;
      m_moduleSumErrSq[side] += ampError*ampError;

      m_moduleSumPreSample[side] += pulseAna_p->GetPreSampleAmp();

      m_calibModuleSum[side] += m_calibAmplitude[side][module];
      m_calibModuleSumErrSq[side] += calibAmpError*calibAmpError;

      m_averageTime[side] += m_calibTime[side][module]*m_calibAmplitude[side][module];
=======
      _calibAmplitude[side][module] = amplitude*_currentECalibCoeff[side][module];

      float calibAmpError = ampError * _currentECalibCoeff[side][module];

      float timeCalib = pulseAna_p->GetT0Corr();
      if (pulseAna_p->UseLowGain()) timeCalib -= _currentT0OffsetsLG[side][module];
      else timeCalib -= _currentT0OffsetsHG[side][module];

      _calibTime[side][module] = timeCalib;

      _moduleSum[side] += amplitude;
      _moduleSumErrSq[side] += ampError*ampError;

      _moduleSumPreSample[side] += pulseAna_p->GetPreSampleAmp();

      _calibModuleSum[side] += _calibAmplitude[side][module];
      _calibModuleSumErrSq[side] += calibAmpError*calibAmpError;

      _averageTime[side] += _calibTime[side][module]*_calibAmplitude[side][module];
>>>>>>> release/21.0.127
    }
  }
  else {
    if (pulseAna_p->Failed()) {
<<<<<<< HEAD
      if (m_debugLevel >= 0) {
	std::cout << "ZDCPulseAnalyzer::LoadData() returned fail for event " << m_eventCount << ", side, module = " << side << ", " << module << std::endl;
      }

      m_fail[side] = true;
    }
  }

  m_moduleStatus[side][module] = pulseAna_p->GetStatusMask();
=======
      if (_debugLevel >= 0) {
	std::cout << "ZDCPulseAnalyzer::LoadData() returned fail for event " << _eventCount << ", side, module = " << side << ", " << module << std::endl;
      }

      _fail[side] = true;
    }
  }

  _moduleStatus[side][module] = pulseAna_p->GetStatusMask();
>>>>>>> release/21.0.127
}

void ZDCDataAnalyzer::LoadAndAnalyzeData(size_t side, size_t module, const std::vector<float> HGSamples, const std::vector<float> LGSamples,
					 const std::vector<float> HGSamplesDelayed, const std::vector<float> LGSamplesDelayed)
{
  // We immediately return if this module is disabled
  //
<<<<<<< HEAD
  if (m_moduleDisabled[side][module]) {
    if (m_debugLevel > 2) {
      std::cout << "Skipping analysis of disabled mofule for event index " << m_eventCount << ", side, module = " << side << ", " << module << std::endl;
=======
  if (_moduleDisabled[side][module]) {
    if (_debugLevel > 2) {
      std::cout << "Skipping analysis of disabled mofule for event index " << _eventCount << ", side, module = " << side << ", " << module << std::endl;
>>>>>>> release/21.0.127
    }

    return;
  }

<<<<<<< HEAD
  if (m_debugLevel > 1) {
    std::cout << "Loading undelayed and delayed data for event index " << m_eventCount << ", side, module = " << side << ", " << module << std::endl;
    
    if (m_debugLevel > 2) {
      std::cout << " Number of HG and LG samples = " << HGSamples.size() << ", " << LGSamples.size() << std::endl;
      if (m_debugLevel > 3) {
=======
  if (_debugLevel > 1) {
    std::cout << "Loading undelayed and delayed data for event index " << _eventCount << ", side, module = " << side << ", " << module << std::endl;
    
    if (_debugLevel > 2) {
      std::cout << " Number of HG and LG samples = " << HGSamples.size() << ", " << LGSamples.size() << std::endl;
      if (_debugLevel > 3) {
>>>>>>> release/21.0.127
	for (size_t sample = 0; sample < HGSamples.size(); sample++) {
	  std::cout << "HGSample[" << sample << "] = " << HGSamples[sample] << std::endl;
	}

	for (size_t sample = 0; sample < HGSamples.size(); sample++) {
	  std::cout << "HGSampleDelayed[" << sample << "] = " << HGSamplesDelayed[sample] << std::endl;
	}
      }

    }
  }

<<<<<<< HEAD
  ZDCPulseAnalyzer* pulseAna_p = m_moduleAnalyzers[side][module];

  bool result = pulseAna_p->LoadAndAnalyzeData(HGSamples, LGSamples, HGSamplesDelayed, LGSamplesDelayed);
  m_dataLoaded[side][module] = true;
=======
  ZDCPulseAnalyzer* pulseAna_p = _moduleAnalyzers[side][module];

  bool result = pulseAna_p->LoadAndAnalyzeData(HGSamples, LGSamples, HGSamplesDelayed, LGSamplesDelayed);
  _dataLoaded[side][module] = true;
>>>>>>> release/21.0.127

  if (result) {
    if (!pulseAna_p->BadT0() && !pulseAna_p->BadChisq()) {
      int moduleMaskBit = 4*side + module;
<<<<<<< HEAD
      m_moduleMask |= 1<< moduleMaskBit;
=======
      _moduleMask |= 1<< moduleMaskBit;
>>>>>>> release/21.0.127
      
      float amplitude = pulseAna_p->GetAmplitude();
      float ampError = pulseAna_p->GetAmpError();

<<<<<<< HEAD
      m_calibAmplitude[side][module] = amplitude*m_currentECalibCoeff[side][module];

      float calibAmpError = ampError * m_currentECalibCoeff[side][module];

      float timeCalib = pulseAna_p->GetT0Corr();
      if (pulseAna_p->UseLowGain()) timeCalib -= m_currentT0OffsetsLG[side][module];
      else timeCalib -= m_currentT0OffsetsHG[side][module];

      m_calibTime[side][module] = timeCalib;

      m_moduleSum[side] += amplitude;
      m_moduleSumErrSq[side] += ampError*ampError;

      m_moduleSumPreSample[side] += pulseAna_p->GetPreSampleAmp();

      m_calibModuleSum[side] += m_calibAmplitude[side][module];
      m_calibModuleSumErrSq[side] += calibAmpError*calibAmpError;

      m_averageTime[side] += m_calibTime[side][module]*m_calibAmplitude[side][module];
=======
      _calibAmplitude[side][module] = amplitude*_currentECalibCoeff[side][module];

      float calibAmpError = ampError * _currentECalibCoeff[side][module];

      float timeCalib = pulseAna_p->GetT0Corr();
      if (pulseAna_p->UseLowGain()) timeCalib -= _currentT0OffsetsLG[side][module];
      else timeCalib -= _currentT0OffsetsHG[side][module];

      _calibTime[side][module] = timeCalib;

      _moduleSum[side] += amplitude;
      _moduleSumErrSq[side] += ampError*ampError;

      _moduleSumPreSample[side] += pulseAna_p->GetPreSampleAmp();

      _calibModuleSum[side] += _calibAmplitude[side][module];
      _calibModuleSumErrSq[side] += calibAmpError*calibAmpError;

      _averageTime[side] += _calibTime[side][module]*_calibAmplitude[side][module];
>>>>>>> release/21.0.127
    }
  }
  else {
    if (pulseAna_p->Failed()) {
<<<<<<< HEAD
      if (m_debugLevel >= 0) {
	std::cout << "ZDCPulseAnalyzer::LoadData() returned fail for event " << m_eventCount << ", side, module = " << side << ", " << module << std::endl;
      }

      m_fail[side] = true;
    }
  }

  m_moduleStatus[side][module] = pulseAna_p->GetStatusMask();
=======
      if (_debugLevel >= 0) {
	std::cout << "ZDCPulseAnalyzer::LoadData() returned fail for event " << _eventCount << ", side, module = " << side << ", " << module << std::endl;
      }

      _fail[side] = true;
    }
  }

  _moduleStatus[side][module] = pulseAna_p->GetStatusMask();
>>>>>>> release/21.0.127
}

bool ZDCDataAnalyzer::FinishEvent()
{
  // First make sure that all data is loaded
  //
  for (size_t side : {0, 1}) {
    for (size_t module : {0, 1, 2, 3}) {
<<<<<<< HEAD
      if (!m_dataLoaded[side][module] && !m_moduleDisabled[side][module]) return false;
=======
      if (!_dataLoaded[side][module] && !_moduleDisabled[side][module]) return false;
>>>>>>> release/21.0.127
    }

    // Divide the average times by the calibrated module sums  
    //
<<<<<<< HEAD
    if (m_calibModuleSum[side] > 1e-6) {
      m_averageTime[side] /= m_calibModuleSum[side]; 
    }
    else {
      m_averageTime[side] = 0;
    }
  }

  m_eventCount++;
=======
    if (_calibModuleSum[side] > 1e-6) {
      _averageTime[side] /= _calibModuleSum[side]; 
    }
    else {
      _averageTime[side] = 0;
    }
  }

  _eventCount++;
>>>>>>> release/21.0.127
  return true;
}
