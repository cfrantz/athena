/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

/*
 * ZdcRecV3.cxx
 *
 *  Created on: Sept 11, 2016 (never forget)
 *      Author: Peter.Steinberg@bnl.gov
 */


#include <memory>

#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/StatusCode.h"
#include "StoreGate/StoreGateSvc.h"
<<<<<<< HEAD
#include "StoreGate/WriteHandle.h"
#include "StoreGate/ReadHandle.h"
=======
>>>>>>> release/21.0.127
//#include "Identifier/Identifier.h"

#include "xAODForward/ZdcModuleAuxContainer.h"
#include "xAODForward/ZdcModuleContainer.h"
#include "ZdcRec/ZdcRecV3.h"
#include "ZdcRec/ZdcRecChannelToolV2.h"
#include "xAODForward/ZdcModuleToString.h"
#include "ZdcAnalysis/ZdcAnalysisTool.h"
#include "ZdcByteStream/ZdcToString.h"

//==================================================================================================
ZdcRecV3::ZdcRecV3(const std::string& name, ISvcLocator* pSvcLocator) :

<<<<<<< HEAD
	AthAlgorithm(name, pSvcLocator)
{
=======
	AthAlgorithm(name, pSvcLocator),
	m_storeGate("StoreGateSvc", name),
	m_ownPolicy(static_cast<int> (SG::OWN_ELEMENTS)),
	m_ttContainerName("ZdcTriggerTowers"),
	m_zdcModuleContainerName("ZdcModules"),
	m_zdcModuleAuxContainerName("ZdcModulesAux."),
	m_ttContainer(0),
	m_eventCount(0),
	m_complainContain(1),
	m_complainRetrieve(1),
	m_zdcTool("ZDC::ZdcAnalysisTool/ZdcAnalysisTool")
{
	declareProperty("OwnPolicy",m_ownPolicy) ;

	declareProperty("DigitsContainerName", m_ttContainerName, "ZdcTriggerTowers");
	declareProperty("ZdcModuleContainerName",    m_zdcModuleContainerName,    "ZdcModules");
	declareProperty("ZdcModuleAuxContainerName",    m_zdcModuleAuxContainerName,    "ZdcModulesAux.");
>>>>>>> release/21.0.127
}
//==================================================================================================

//==================================================================================================
ZdcRecV3::~ZdcRecV3() {}
//==================================================================================================

//==================================================================================================
StatusCode ZdcRecV3::initialize()
{
<<<<<<< HEAD
  ATH_CHECK( m_ChannelTool.retrieve() );
  ATH_CHECK( m_zdcTool.retrieve() );

  // Container output name
  //TODO: change MESSAGE !!
  ATH_MSG_DEBUG( " Output Container Name " << m_zdcModuleContainerName );

  ATH_CHECK( m_zdcModuleContainerName.initialize() );
  ATH_CHECK( m_ttContainerName.initialize(SG::AllowEmpty) );

  ATH_MSG_DEBUG( "--> ZDC: ZdcRecV3 initialization complete" );

  return StatusCode::SUCCESS;
=======
	MsgStream mLog(msgSvc(), name());

	// Look up the Storegate service
	StatusCode sc = m_storeGate.retrieve();
	if (sc.isFailure())
	{
		mLog << MSG::FATAL << "--> ZDC: Unable to retrieve pointer to StoreGateSvc" << endreq;
		return sc;
	}


	// Reconstruction Tool
	StatusCode scTool = m_ChannelTool.retrieve();
	if (scTool.isFailure())
	{
		mLog << MSG::WARNING << "--> ZDC: Could not retrieve " << m_ChannelTool << endreq;
		return StatusCode::FAILURE;
	}
	mLog << MSG::DEBUG << "--> ZDC: SUCCESS retrieving " << m_ChannelTool << endreq;

	// Reconstruction Tool
	StatusCode zdcTool = m_zdcTool.retrieve();
	if (zdcTool.isFailure())
	{
		mLog << MSG::WARNING << "--> ZDC: Could not retrieve " << m_zdcTool << endreq;
		return StatusCode::FAILURE;
	}
	mLog << MSG::DEBUG << "--> ZDC: SUCCESS retrieving " << m_zdcTool << endreq;

	// Container output name
	//TODO: change MESSAGE !!
	mLog << MSG::DEBUG << " Output Container Name " << m_zdcModuleContainerName << endreq;
	if (m_ownPolicy == SG::OWN_ELEMENTS)
		mLog << MSG::DEBUG << "...will OWN its cells." << endreq;
	else
		mLog << MSG::DEBUG << "...will VIEW its cells." << endreq;


	mLog << MSG::DEBUG << "--> ZDC: ZdcRecV3 initialization complete" << endreq;

	return StatusCode::SUCCESS;
>>>>>>> release/21.0.127
}
//==================================================================================================

//==================================================================================================
StatusCode ZdcRecV3::execute()
{
<<<<<<< HEAD
  const EventContext& ctx = Gaudi::Hive::currentContext();
  ATH_MSG_DEBUG ("--> ZDC: ZdcRecV3 execute starting on "
                 << ctx.evt()
                 << "th event");

	//Look for the container presence
        if (m_ttContainerName.empty()) {
=======

  MsgStream mLog(msgSvc(), name());
	mLog << MSG::DEBUG
	     << "--> ZDC: ZdcRecV3 execute starting on "
	     << m_eventCount
	     << "th event"
		 << endreq;

	m_eventCount++;

	//Look for the container presence
	bool dg = m_storeGate->contains<xAOD::TriggerTowerContainer>( m_ttContainerName);
	if (!dg) {
	  if (m_complainContain) mLog << MSG::WARNING << "--> ZDC: StoreGate does not contain " << m_ttContainerName << endreq;
	  m_complainContain = 0;
>>>>>>> release/21.0.127
	  return StatusCode::SUCCESS;
	}

	// Look up the Digits "TriggerTowerContainer" in Storegate
<<<<<<< HEAD
        SG::ReadHandle<xAOD::TriggerTowerContainer> ttContainer
          (m_ttContainerName, ctx);
=======
	StatusCode digitsLookupSC = m_storeGate->retrieve(m_ttContainer, m_ttContainerName);
	if (digitsLookupSC.isFailure())
	{
	  if (m_complainRetrieve)
	    mLog << MSG::WARNING
		 << "--> ZDC: Could not retrieve "
		 << m_ttContainerName
		 << " from StoreGate"
		 << endreq;
	  m_complainRetrieve = 0;
	  return StatusCode::SUCCESS;
	}

	if (digitsLookupSC.isSuccess() && !m_ttContainer)
	{
		mLog << MSG::ERROR
			 << "--> ZDC: Storegate returned zero pointer for "
			 << m_ttContainerName
			 << endreq;
		return StatusCode::SUCCESS;
	}
>>>>>>> release/21.0.127

    	//Create the containers to hold the reconstructed information (you just pass the pointer and the converter does the work)	
	std::unique_ptr<xAOD::ZdcModuleContainer> moduleContainer( new xAOD::ZdcModuleContainer());
	std::unique_ptr<xAOD::ZdcModuleAuxContainer> moduleAuxContainer( new xAOD::ZdcModuleAuxContainer() );
	moduleContainer->setStore( moduleAuxContainer.get() );

	// rearrange ZDC channels and perform fast reco on all channels (including non-big tubes)
<<<<<<< HEAD
	int ncha = m_ChannelTool->convertTT2ZM(ttContainer.get(), moduleContainer.get() );
	
	msg( MSG::DEBUG ) << "Channel tool returns " << ncha << endmsg;
	msg( MSG::DEBUG ) << ZdcModuleToString(*moduleContainer) << endmsg;

	// re-reconstruct big tubes

	ATH_CHECK( m_zdcTool->recoZdcModules(*moduleContainer) );

        SG::WriteHandle<xAOD::ZdcModuleContainer> moduleContainerH
          (m_zdcModuleContainerName, ctx);
        ATH_CHECK( moduleContainerH.record (std::move(moduleContainer),
                                            std::move(moduleAuxContainer)) );
=======
	int ncha = m_ChannelTool->convertTT2ZM(m_ttContainer, moduleContainer.get() );
	
	msg( MSG::DEBUG ) << "Channel tool returns " << ncha << endreq;
	msg( MSG::DEBUG ) << ZdcModuleToString(*moduleContainer) << endreq;

	// re-reconstruct big tubes
 
	ATH_CHECK( evtStore()->record( std::move(moduleContainer), m_zdcModuleContainerName) );
	ATH_CHECK( evtStore()->record( std::move(moduleAuxContainer), m_zdcModuleAuxContainerName) );

	ATH_CHECK( m_zdcTool->reprocessZdc() ); // pulls the modules out of the event store
>>>>>>> release/21.0.127

	return StatusCode::SUCCESS;

}
//==================================================================================================

//==================================================================================================
StatusCode ZdcRecV3::finalize()
{
<<<<<<< HEAD
  ATH_MSG_DEBUG( "--> ZDC: ZdcRecV3 finalize complete" );
=======

  MsgStream mLog(msgSvc(),name());

  mLog << MSG::DEBUG
		  << "--> ZDC: ZdcRecV3 finalize complete"
		  << endreq;

>>>>>>> release/21.0.127
  return StatusCode::SUCCESS;

}
//==================================================================================================

