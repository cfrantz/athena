from AthenaCommon.AlgSequence import AlgSequence
topSeq = AlgSequence()

ServiceMgr.MessageSvc.OutputLevel = DEBUG

from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
athenaCommonFlags.PoolEvgenInput = ['evgen.pgun.ALFA.pool.root']
athenaCommonFlags.PoolHitsOutput.set_Off()
athenaCommonFlags.EvtMax = -1

from G4AtlasApps.SimFlags import SimFlags
SimFlags.load_atlas_flags()
SimFlags.EventFilter.set_Off()
SimFlags.MagneticField.set_Off()

<<<<<<< HEAD
include("G4AtlasApps/G4Atlas.flat.configuration.py")

=======
from G4AtlasApps.PyG4Atlas import PyG4AtlasAlg
topSeq += PyG4AtlasAlg()
>>>>>>> release/21.0.127
from AthenaCommon.CfgGetter import getAlgorithm
topSeq += getAlgorithm("G4AtlasAlg",tryDefaultConfigurable=True)

include("ALFA_BeamTransport/ALFA_BeamTransportConfig.py")
