/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "AFP_Monitoring/AFPHitsMonitorTool.h"
#include "AFP_Monitoring/IAFPSiStationMonitor.h"
#include "AFP_Monitoring/IAFPSiLayerMonitor.h"
#include "AFP_Monitoring/AFPSiLayerSummaryManager.h"

#include <xAODForward/AFPSiHit.h>
#include <xAODForward/AFPStationID.h>
#include <xAODForward/AFPSiHitContainer.h>

#include "StoreGate/ReadHandleKey.h"
#include <AthenaMonitoring/AthenaMonManager.h>
#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/StatusCode.h>

#include <LWHists/TH1F_LW.h>
#include <TH1.h>
#include <TProfile.h>
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include <GaudiKernel/IJobOptionsSvc.h>
#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/StatusCode.h>

#include <AthenaMonitoring/AthenaMonManager.h>

//#include <xAODEventInfo/EventInfo.h>
#include <xAODForward/AFPSiHit.h>
#include <xAODForward/AFPSiHitContainer.h>

#include <AFP_Monitoring/AFPHitsMonitorTool.h>

const int AFPHitsMonitorTool::s_cNearStationIndex = 2;
const int AFPHitsMonitorTool::s_cFarStationIndex = 3;

>>>>>>> release/21.0.127

AFPHitsMonitorTool::
AFPHitsMonitorTool( const std::string & type, const std::string & name,
      const IInterface* parent )
<<<<<<< HEAD
  : ManagedMonitorToolBase( type, name, parent ),
    m_histsDirectoryName ("AFP/"),
    m_summaryManager(new AFPSiLayerSummaryManager),
    m_lumiBlockMuTool("LumiBlockMuTool/LumiBlockMuTool")
{
  declareProperty("stationsMonitors", m_stationsMonitors, "Array of station monitors.");
  declareProperty("luminosityTool", m_lumiBlockMuTool, "Luminosity tool used for reading pile-up value.");
=======
   : ManagedMonitorToolBase( type, name, parent )
   , m_histsDirectoryName ("AFP/")
   , m_cNearStation (s_cNearStationIndex)
   , m_cFarStation (s_cFarStationIndex)
{
  m_stationsMonitors.push_back(&m_cNearStation);
  m_stationsMonitors.push_back(&m_cFarStation);
>>>>>>> release/21.0.127
}


AFPHitsMonitorTool::
~AFPHitsMonitorTool()
{
<<<<<<< HEAD
  delete m_summaryManager;
}

StatusCode AFPHitsMonitorTool::initialize()
{
  if (m_stationsMonitors.size() != 0) {
    // loop over tools
    for (ToolHandle<IAFPSiStationMonitor>& stationMon : m_stationsMonitors) {
      if (stationMon.retrieve().isFailure())
	ATH_MSG_WARNING("Failed to retrieve AlgTool " << stationMon);
      else
	stationMon->setAllLayersParent(this);
    }
  }
  else {
    ATH_MSG_ERROR("No station monitors defined. Aborting.");
    return StatusCode::FAILURE;
  }


  ManagedMonitorToolBase::MonGroup managedBookingLumiBlock(this, histsDirectoryName() + "shifter/", lumiBlock, ManagedMonitorToolBase::ATTRIB_MANAGED);   // to re-booked every luminosity block
  AFPSiLayerSummaryProfileBase* timeOverThresholdMean = m_summaryManager->createSummaryHits(this, 
											    managedBookingLumiBlock, 
											    "h_timeOverThresholdMeanp", 
											    "Mean hitstime over threshold per plane",
											    &xAOD::AFPSiHit::timeOverThreshold);
  timeOverThresholdMean->profile()->SetYTitle("mean time-over-threshold");

  AFPSiLayerSummaryProfileBase* hitsPerPlaneScaledMean = 
    m_summaryManager->createSummaryEventEnd(this, 
					    managedBookingLumiBlock, 
					    "h_hitsPerPlaneScaledMean",
					    "Mean number of hits in an event per plane",
					    &IAFPSiLayerMonitor::hitsInEventScaled);
  hitsPerPlaneScaledMean->profile()->SetYTitle("Mean number of hits in an event.");

  AFPSiLayerSummaryProfileBase* hitsPerPlaneScaledHotSpotMean = 
    m_summaryManager->createSummaryEventEnd(this, 
					    managedBookingLumiBlock, 
					    "h_hitsPerPlaneScaledHotSpotMean",
					    "Mean number of hits in a hotspot in an event per plane",
					    &IAFPSiLayerMonitor::hitsInEventHotSpotScaled);
  hitsPerPlaneScaledHotSpotMean->profile()->SetYTitle("Mean number of hits in hotspot in an event");

  ATH_CHECK( m_afpHitContainerKey.initialize() );
  
  return StatusCode::SUCCESS;
=======
>>>>>>> release/21.0.127
}

// Description: Used for rebooking unmanaged histograms       
StatusCode AFPHitsMonitorTool::bookHistogramsRecurrent()
{
<<<<<<< HEAD
=======
  // for (AFPSiStationMonitor* station : m_stationsMonitors)
  //   station->bookHistogramsRecurrent(this);

>>>>>>> release/21.0.127
   return StatusCode::SUCCESS;
}

// Description: Used for re-booking managed histograms       
<<<<<<< HEAD
StatusCode AFPHitsMonitorTool::bookHistograms()
{
  for (ToolHandle<IAFPSiStationMonitor>& station : m_stationsMonitors)
    station->bookHistograms(this);

  m_summaryManager->book();

  return StatusCode::SUCCESS;
}

StatusCode AFPHitsMonitorTool::fillHistograms()
{
  SG::ReadHandle<xAOD::AFPSiHitContainer> afpHitContainer (m_afpHitContainerKey);

  for (const xAOD::AFPSiHit* hit : *afpHitContainer) {
    bool matchStation = false;
    for (ToolHandle<IAFPSiStationMonitor>& station : m_stationsMonitors)
      if (station->stationID() == hit->stationID()) {
  	station->fillHistograms(*hit);
  	matchStation = true;
  	break;
      }

    if (matchStation == false)
      ATH_MSG_WARNING("Unrecognised station index: "<<hit->stationID());
  }


  for (ToolHandle<IAFPSiStationMonitor>& station : m_stationsMonitors)
    station->eventEnd();

=======
StatusCode AFPHitsMonitorTool::bookHistograms( )
{
  for (AFPSiStationMonitor* station : m_stationsMonitors)
    station->bookHistograms(this);

  return StatusCode::SUCCESS;
}


StatusCode AFPHitsMonitorTool::fillHistograms()
{
  // const xAOD::EventInfo* eventInfo = 0;
  // CHECK( evtStore()->retrieve( eventInfo) );

  const xAOD::AFPSiHitContainer* afpHitContainer = 0;
  CHECK( evtStore()->retrieve( afpHitContainer, "AFPSiHitContainer") );

  for (const xAOD::AFPSiHit* hit : *afpHitContainer) {
    switch (hit->stationID()) {
    case s_cNearStationIndex:
      m_cNearStation.fillHistograms(*hit);
      break;
	  
    case s_cFarStationIndex:
      m_cFarStation.fillHistograms(*hit);
      break;

    default:
      ATH_MSG_WARNING("Unrecognised station index: "<<hit->stationID());
    }
  }

  m_cNearStation.eventEnd();
  m_cFarStation.eventEnd();
      
>>>>>>> release/21.0.127
  return StatusCode::SUCCESS;
}


<<<<<<< HEAD
void AFPHitsMonitorTool::makeLayerSummaryHist (const std::string inputHistName, const std::string outputHistName, const std::string outputHistTitle) 
{
  ManagedMonitorToolBase::MonGroup managedBookingLumiBlock(this, histsDirectoryName(), lumiBlock, ManagedMonitorToolBase::ATTRIB_MANAGED);   // to re-booked every luminosity block

  // make histogram
  int totalPlanes = 0;
  for (const ToolHandle<IAFPSiStationMonitor>& station : m_stationsMonitors)
    totalPlanes += station->layersMonitors().size();
  
  TProfile* outputHist = new TProfile(outputHistName.data(),
				      outputHistTitle.data(),
				      totalPlanes, - 0.5, totalPlanes + 0.5);    
  
  regHist( outputHist, managedBookingLumiBlock ).ignore();

  TAxis* axis = outputHist->GetXaxis();
  int binCounter = 1;
  for (const ToolHandle<IAFPSiStationMonitor>& station : m_stationsMonitors)
    for (const ToolHandle<IAFPSiLayerMonitor>& layer : station->layersMonitors()) {
      std::stringstream binName;
      binName<<"st."<<station->stationID();
      binName<<"/lay."<<layer->layerID();
      axis->SetBinLabel (binCounter, binName.str().data());
      TH1* hist = nullptr;
      if (getHist(hist, layer->makeHistName(inputHistName), layer->histsDirName(), lumiBlock).isSuccess()) {
	const double mean = hist->GetMean()*hist->GetEntries();
      
	const double errorSq = hist->GetMeanError()*hist->GetMeanError();
	const double entriesSq = hist->GetEntries()*hist->GetEntries();
	const double valueSq = mean * mean;
	const double meanError = sqrt( errorSq*entriesSq + (valueSq/hist->GetEntries()) );

	outputHist->SetBinEntries(binCounter, hist->GetEntries());
	outputHist->SetBinContent(binCounter, mean);
	outputHist->SetBinError(binCounter, meanError);
      }
      else
	ATH_MSG_WARNING ("Missing "<<layer->makeHistName(inputHistName)<<" does not exist, but is needed for a summary.");

      
      ++binCounter;
    }
} // close m_hitsPerPlaneSummary



StatusCode AFPHitsMonitorTool::procHistograms( )
{
  if( endOfLumiBlockFlag() ) {

    for (ToolHandle<IAFPSiStationMonitor>& station : m_stationsMonitors)
      station->endOfLumiBlock(this);
  }


  // if ( endOfEventsBlockFlag() ) {
  // }

  // if ( endOfRunFlag() ) {
  // }
  
  return StatusCode::SUCCESS;
=======
StatusCode AFPHitsMonitorTool::procHistograms( )
{
  if( endOfLumiBlockFlag() ) {
    for (AFPSiStationMonitor* station : m_stationsMonitors)
      station->endOfLumiBlock(this);
   }

   return StatusCode::SUCCESS;
>>>>>>> release/21.0.127
}


