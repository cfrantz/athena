<<<<<<< HEAD
#Copyright (C) 2020 CERN for the benefit of the ATLAS collaboration
=======
################################################################################
# Package: AthContainersRoot
################################################################################
>>>>>>> release/21.0.127

# Declare the package name:
atlas_subdir( AthContainersRoot )

<<<<<<< HEAD
# External dependencies:
find_package( ROOT COMPONENTS Core Tree )
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          GaudiKernel
                          Control/AthContainersInterfaces
                          Control/RootUtils
                          PRIVATE
                          AtlasTest/TestTools
                          Control/AthContainers
                          Control/CxxUtils )

# External dependencies:
find_package( ROOT COMPONENTS Core )
>>>>>>> release/21.0.127

# Component(s) in the package:
atlas_add_library( AthContainersRoot
                   src/*.cxx
                   PUBLIC_HEADERS AthContainersRoot
                   PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
<<<<<<< HEAD
                   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} AthContainers AthLinks CxxUtils RootUtils
                   LINK_LIBRARIES AthenaKernel GaudiKernel AthContainersInterfaces )
=======
                   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} AthContainers RootUtils )
>>>>>>> release/21.0.127


atlas_add_dictionary( AthContainersRootTestDict
   AthContainersRoot/AthContainersRootTestDict.h
   AthContainersRoot/selection.xml
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} AthContainersRoot AthContainers )


atlas_add_test( RootAuxVectorFactory_test
                SOURCES
                test/RootAuxVectorFactory_test.cxx
                INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                LINK_LIBRARIES ${ROOT_LIBRARIES} AthContainers CxxUtils RootUtils AthContainersRoot )


atlas_add_test( getDynamicAuxID_test
                SOURCES
                test/getDynamicAuxID_test.cxx
                INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                LINK_LIBRARIES ${ROOT_LIBRARIES} AthContainers CxxUtils RootUtils AthContainersRoot )


atlas_add_test( AuxStoreRoot_test
                SOURCES
                test/AuxStoreRoot_test.cxx
                INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
<<<<<<< HEAD
                LINK_LIBRARIES ${ROOT_LIBRARIES} AthContainers CxxUtils RootUtils AthContainersRoot TestTools )
=======
                LINK_LIBRARIES ${ROOT_LIBRARIES} AthContainers CxxUtils RootUtils AthContainersRoot )
>>>>>>> release/21.0.127

