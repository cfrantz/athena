/*  -*- C++ -*- */

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef PILEUPCOMPS_NOPROFILESVC
#define PILEUPCOMPS_NOPROFILESVC 1
/** @file NoProfileSvc.h
 * @brief A IBeamLuminosity service configured flat.
 *
 * @author atarce
 */
#include "PileUpTools/IBeamLuminosity.h"
#include "AthenaBaseComps/AthService.h"
<<<<<<< HEAD
#include "Gaudi/Property.h"

class NoProfileSvc : public extends<AthService, IBeamLuminosity>
=======
#include "GaudiKernel/Property.h"

class NoProfileSvc : virtual public IBeamLuminosity, public AthService
>>>>>>> release/21.0.127
{
public:
  /// \name Constructor / Destructor
  //@{
  NoProfileSvc(const std::string& name, ISvcLocator* svc);
  virtual ~NoProfileSvc();
  //@}
  /// \name AthService methods
  //@{
  virtual StatusCode initialize() override final;
<<<<<<< HEAD
=======
  virtual StatusCode queryInterface( const InterfaceID& riid, void** ppvInterface ) override final;
>>>>>>> release/21.0.127
  //@}
  /// \name IBeamLuminosity methods
  //@{
  virtual float scaleFactor(unsigned int run, unsigned int lumi, bool & updated) override final;
  //@}
};
#endif

