/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "AthenaMonitoring/DQBadLBFilterTool.h"
#include "AthenaPoolUtilities/AthenaAttributeList.h"
#include "AthenaPoolUtilities/CondAttrListCollection.h"
#include "CoralBase/AttributeListException.h"

DQBadLBFilterTool::DQBadLBFilterTool(const std::string& type,const std::string& name,const IInterface* parent)
: AthAlgTool( type, name, parent )
, m_fallbackValue(true)
, m_alwaysReturnTrue(false)
, m_key("")
{
  declareInterface<IDQFilterTool>(this);
  declareProperty("fallbackValue", m_fallbackValue);
  declareProperty("alwaysReturnTrue", m_alwaysReturnTrue);
  declareProperty("ReadKey", m_key);
}
        
DQBadLBFilterTool::~DQBadLBFilterTool () {}
        
StatusCode DQBadLBFilterTool::initialize()
{
  ATH_MSG_DEBUG("DQ Bad LB Filter tool: fallback and force: " << m_fallbackValue << "; " << m_alwaysReturnTrue );

  ATH_MSG_VERBOSE("ATLAS Ready initialize");
  // don't register key if we always return true anyway
  if (m_alwaysReturnTrue) return StatusCode::SUCCESS;

  ATH_CHECK( m_key.initialize() );
  return StatusCode::SUCCESS;
}

<<<<<<< HEAD
=======
StatusCode DQBadLBFilterTool::updateCache() {

  const CondAttrListCollection* attrListCol(0);
  StatusCode sc = detStore()->retrieve(attrListCol, "/GLOBAL/DETSTATUS/DEFECTS");
  if (sc.isFailure()) {
    ATH_MSG_WARNING("Unable to retrieve defect information; falling back to" << m_fallbackValue);
    m_valueCache = m_fallbackValue;
    return StatusCode::SUCCESS;
  }

  m_valueCache = m_fallbackValue;
  if (attrListCol != 0) {

          ATH_MSG_DEBUG("Col range " <<  " iov min " << attrListCol->minRange().start().event() << " iov max " << attrListCol->minRange().stop().event());

      std::vector<int>::const_iterator defect_iter = m_listofdefects.begin();

      for ( ; defect_iter != m_listofdefects.end();++defect_iter){

          if (attrListCol->chanAttrListPair(*defect_iter) != attrListCol->end()){

              const CondAttrListCollection::AttributeList* attrList = 
                  &(attrListCol->attributeList( (*defect_iter) ));

              ATH_MSG_DEBUG("FOUND CHANNEL " << (*defect_iter) << " is: " << (*attrList)["present"].data<bool>());
              ATH_MSG_DEBUG("current channel : " << (*defect_iter) << " Name : " << attrListCol->chanName(*defect_iter) << " : iovrange " << attrListCol->iovRange(*defect_iter).start().event() << " : " <<  attrListCol->iovRange(*defect_iter).stop().event());
              ATH_MSG_DEBUG( "Check address " << attrList << " : " << *attrList);
              if ((*attrList)["present"].data<bool>() != 0){
                if ((*attrList)["recoverable"].data<bool>() == 1 && m_ignoreRecoverable){
                    continue;
                }
                m_valueCache =  0;
                return StatusCode::SUCCESS;
              }
          }
      }    
    
  }
  return StatusCode::SUCCESS;
}
        
>>>>>>> release/21.0.127
bool DQBadLBFilterTool::accept() const {
  if (m_alwaysReturnTrue) {
    return true;
  } else {
    SG::ReadCondHandle<AthenaAttributeList> rch(m_key);
    const AthenaAttributeList* attrList{*rch};
    if (attrList == 0) {
      ATH_MSG_WARNING("Unable to retrieve DataTakingMode information; falling back to" << m_fallbackValue);
      return m_fallbackValue;
    }
    bool value = (*attrList)["Accept"].data<bool>();
    ATH_MSG_VERBOSE("Bad LB accept called, value " << value);
    return value;
  }
}
