/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

// $Id$
/**
 * @file DataModelTestDataWrite/src/xAODTestWriteHVec.cxx
 * @author snyder@bnl.gov
 * @date Oct, 2016
 * @brief Algorithm to test writing xAOD classes for schema evolution (hvec/hview).
 */


#include "xAODTestWriteHVec.h"
#include "xAODTestWriteHelper.h"
<<<<<<< HEAD
=======
#include "EventInfo/EventID.h"
>>>>>>> release/21.0.127
#include "DataModelTestDataWrite/HVec.h"
#include "DataModelTestDataWrite/HView.h"
#include "DataModelTestDataWrite/H.h"
#include "DataModelTestDataWrite/HAuxContainer.h"


namespace DMTest {


/**
 * @brief Constructor.
 * @param name The algorithm name.
 * @param svc The service locator.
 */
xAODTestWriteHVec::xAODTestWriteHVec (const std::string &name,
                                      ISvcLocator *pSvcLocator)
  : AthReentrantAlgorithm (name, pSvcLocator),
<<<<<<< HEAD
    m_hvecKey ("hvec"),
    m_hviewKey ("hview")
{
=======
    m_eventInfoKey ("McEventInfo"),
    m_hvecKey ("hvec"),
    m_hviewKey ("hview")
{
  declareProperty ("EventInfoKey", m_eventInfoKey);
>>>>>>> release/21.0.127
  declareProperty ("HVecKey", m_hvecKey);
  declareProperty ("HViewKey", m_hviewKey);
}
  

/**
 * @brief Algorithm initialization; called at the beginning of the job.
 */
StatusCode xAODTestWriteHVec::initialize()
{
<<<<<<< HEAD
=======
  ATH_CHECK( m_eventInfoKey.initialize() );
>>>>>>> release/21.0.127
  ATH_CHECK( m_hvecKey.initialize() );
  ATH_CHECK( m_hviewKey.initialize() );
  return StatusCode::SUCCESS;
}


/**
 * @brief Algorithm event processing.
 */
<<<<<<< HEAD
StatusCode xAODTestWriteHVec::execute (const EventContext& ctx) const
{
  unsigned int count = ctx.eventID().event_number() + 1;
=======
StatusCode xAODTestWriteHVec::execute_r (const EventContext& ctx) const
{
  SG::ReadHandle<EventInfo> eventInfo (m_eventInfoKey, ctx);
  unsigned int count = eventInfo->event_ID()->event_number() + 1;
>>>>>>> release/21.0.127

  auto hvec = std::make_unique<DMTest::HVec>();
  auto store = std::make_unique<DMTest::HAuxContainer>();
  hvec->setStore (store.get());
  auto hview = std::make_unique<DMTest::HView>();

  for (int i = 0; i < 20; i++) {
    hvec->push_back (new DMTest::H);
    hvec->back()->setAnInt (i+1 + count * 400);
  }

  for (int i = 0; i < 20; i++) {
    hview->push_back (hvec->at (19-i));
  }

<<<<<<< HEAD
  ATH_CHECK( SG::makeHandle(m_hvecKey, ctx).record (std::move(hvec),
                                                    std::move(store)) );
=======
  ATH_CHECK( SG::makeHandle(m_hvecKey).record (std::move(hvec),
                                               std::move(store)) );
>>>>>>> release/21.0.127

  SG::WriteHandle<DMTest::HView> hviewH (m_hviewKey, ctx);
  ATH_CHECK( DMTest::recordView2 (hviewH, std::move(hview)) );

  return StatusCode::SUCCESS;
}


/**
 * @brief Algorithm finalization; called at the end of the job.
 */
StatusCode xAODTestWriteHVec::finalize()
{
  return StatusCode::SUCCESS;
}


} // namespace DMTest

