// This file's extension implies that it's C, but it's really -*- C++ -*-.

/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

// $Id$
/**
 * @file DataModelTestDataWrite/src/xAODTestWriteHVec.h
 * @author scott snyder <snyder@bnl.gov>
 * @date Oct, 2016
 * @brief Algorithm to test writing xAOD classes for schema evolution (hvec/hview).
 */


#ifndef DATAMODELTESTDATAWRITE_XAODTESTWRITEHVEC_H
#define DATAMODELTESTDATAWRITE_XAODTESTWRITEHVEC_H


<<<<<<< HEAD
=======
//#include "xAODEventInfo/EventInfo.h"
#include "EventInfo/EventInfo.h"
>>>>>>> release/21.0.127
#include "DataModelTestDataWrite/HVec.h"
#include "DataModelTestDataWrite/HView.h"
#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "StoreGate/WriteHandleKey.h"
#include "StoreGate/ReadHandleKey.h"


namespace DMTest {


/**
 * @brief Algorithm to test writing xAOD classes for schema evolution (hvec/hview).
 */
class xAODTestWriteHVec
  : public AthReentrantAlgorithm
{
public:
  /**
   * @brief Constructor.
   * @param name The algorithm name.
   * @param svc The service locator.
   */
  xAODTestWriteHVec (const std::string &name, ISvcLocator *pSvcLocator);
  

  /**
   * @brief Algorithm initialization; called at the beginning of the job.
   */
  virtual StatusCode initialize() override;


  /**
   * @brief Algorithm event processing.
   */
<<<<<<< HEAD
  virtual StatusCode execute (const EventContext& ctx) const override;
=======
  virtual StatusCode execute_r (const EventContext& ctx) const override;
>>>>>>> release/21.0.127


  /**
   * @brief Algorithm finalization; called at the end of the job.
   */
  virtual StatusCode finalize() override;


private:
<<<<<<< HEAD
=======
  //SG::ReadHandleKey<xAOD::EventInfo> m_eventInfoKey;
  SG::ReadHandleKey<EventInfo> m_eventInfoKey;
>>>>>>> release/21.0.127
  SG::WriteHandleKey<DMTest::HVec> m_hvecKey;
  SG::WriteHandleKey<DMTest::HView> m_hviewKey;
};


} // namespace DMTest


#endif // not DATAMODELTESTDATAWRITE_XAODTESTWRITEHVEC_H
