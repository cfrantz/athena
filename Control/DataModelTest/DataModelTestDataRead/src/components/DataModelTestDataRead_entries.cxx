// $Id: DataModelTestDataRead_entries.cxx,v 1.2 2006-04-07 18:30:38 ssnyder Exp $
/**
 * @file  src/components/DataModelTestDataRead_entries.cxx
 * @author snyder@bnl.gov
 * @date Nov 2005
 * @brief Gaudi algorithm factory declarations.
 */

#include "../DMTestRead.h"
#include "../AuxDataTestRead.h"
#include "../AuxDataTestDecor.h"
#include "../AuxDataTestClearDecor.h"
#include "../AuxDataTestTypelessRead.h"
#include "../xAODTestRead.h"
<<<<<<< HEAD
#include "../xAODTestReadCInfo.h"
#include "../xAODTestReadCView.h"
#include "../xAODTestReadHVec.h"
#include "../xAODTestFilterCVec.h"
#include "../xAODTestClearDecor.h"
#include "../xAODTestTypelessRead.h"
#include "../xAODTestShallowCopyHVec.h"
#include "../HLTResultReader.h"

DECLARE_COMPONENT( DMTest::DMTestRead )
DECLARE_COMPONENT( DMTest::AuxDataTestRead )
DECLARE_COMPONENT( DMTest::AuxDataTestDecor )
DECLARE_COMPONENT( DMTest::AuxDataTestClearDecor )
DECLARE_COMPONENT( DMTest::AuxDataTestTypelessRead )
DECLARE_COMPONENT( DMTest::xAODTestRead )
DECLARE_COMPONENT( DMTest::xAODTestReadCInfo )
DECLARE_COMPONENT( DMTest::xAODTestReadCView )
DECLARE_COMPONENT( DMTest::xAODTestReadHVec )
DECLARE_COMPONENT( DMTest::xAODTestFilterCVec )
DECLARE_COMPONENT( DMTest::xAODTestClearDecor )
DECLARE_COMPONENT( DMTest::xAODTestTypelessRead )
DECLARE_COMPONENT( DMTest::xAODTestShallowCopyHVec )
DECLARE_COMPONENT( DMTest::HLTResultReader )
=======
#include "../xAODTestReadCVec.h"
#include "../xAODTestReadCView.h"
#include "../xAODTestReadHVec.h"
#include "../xAODTestFilterCVec.h"
#include "../xAODTestDecor.h"
#include "../xAODTestClearDecor.h"
#include "../xAODTestTypelessRead.h"
#include "../xAODTestShallowCopy.h"
#include "../HLTResultReader.h"

DECLARE_NAMESPACE_ALGORITHM_FACTORY(DMTest, DMTestRead)
DECLARE_NAMESPACE_ALGORITHM_FACTORY(DMTest, AuxDataTestRead)
DECLARE_NAMESPACE_ALGORITHM_FACTORY(DMTest, AuxDataTestDecor)
DECLARE_NAMESPACE_ALGORITHM_FACTORY(DMTest, AuxDataTestClearDecor)
DECLARE_NAMESPACE_ALGORITHM_FACTORY(DMTest, AuxDataTestTypelessRead)
DECLARE_NAMESPACE_ALGORITHM_FACTORY(DMTest, xAODTestRead)
DECLARE_NAMESPACE_ALGORITHM_FACTORY(DMTest, xAODTestReadCVec)
DECLARE_NAMESPACE_ALGORITHM_FACTORY(DMTest, xAODTestReadCView)
DECLARE_NAMESPACE_ALGORITHM_FACTORY(DMTest, xAODTestReadHVec)
DECLARE_NAMESPACE_ALGORITHM_FACTORY(DMTest, xAODTestFilterCVec)
DECLARE_NAMESPACE_ALGORITHM_FACTORY(DMTest, xAODTestDecor)
DECLARE_NAMESPACE_ALGORITHM_FACTORY(DMTest, xAODTestClearDecor)
DECLARE_NAMESPACE_ALGORITHM_FACTORY(DMTest, xAODTestTypelessRead)
DECLARE_NAMESPACE_ALGORITHM_FACTORY(DMTest, xAODTestShallowCopy)
DECLARE_NAMESPACE_ALGORITHM_FACTORY(DMTest, HLTResultReader)
>>>>>>> release/21.0.127

