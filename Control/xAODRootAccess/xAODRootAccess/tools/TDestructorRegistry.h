// Dear emacs, this is -*- c++ -*-
<<<<<<< HEAD
//
// Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
//
=======

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: TDestructorRegistry.h 599851 2014-06-02 12:32:51Z krasznaa $
>>>>>>> release/21.0.127
#ifndef XAODROOTACCESS_TOOLS_TDESTRUCTORREGISTRY_H
#define XAODROOTACCESS_TOOLS_TDESTRUCTORREGISTRY_H

// System include(s):
#include <map>
<<<<<<< HEAD
#include <shared_mutex>
#include <memory>
=======
>>>>>>> release/21.0.127

// Forward declaration(s):
namespace std {
   class type_info;
}

namespace xAOD {

   // Forward declaration(s):
   class TVirtualDestructor;

   /// Application-wide registry of destructor objects
   ///
   /// This registry is used to keep track of how to destruct objects
   /// that are kept in the transient store, and don't have a ROOT dictionary
   /// available for them.
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
<<<<<<< HEAD
=======
   /// $Revision: 599851 $
   /// $Date: 2014-06-02 14:32:51 +0200 (Mon, 02 Jun 2014) $
   ///
>>>>>>> release/21.0.127
   class TDestructorRegistry {

   public:
      /// Function accessing the singleton instance of this type
      static TDestructorRegistry& instance();

      /// Get the destructor for a given type
      const TVirtualDestructor* get( const std::type_info& ti ) const;
      /// Add a new destructor object
      template< class T >
      void add();

   private:
<<<<<<< HEAD
      /// Hide the constructor of the type
      TDestructorRegistry();
      /// Hide the copy-constructor
      TDestructorRegistry( const TDestructorRegistry& ) = delete;

      /// Type of the internal map
      typedef std::map< const std::type_info*,
                        std::unique_ptr< TVirtualDestructor > > Map_t;
      /// Internal map of known destructor objects
      Map_t m_types;
      /// Mutex for the destructor map
      ///
      /// This type is used because the registry is filled mostly at the very
      /// beginning of a job, and is just read from there on. For the reading
      /// the clients don't need exclusive locks on the store.
      ///
      mutable std::shared_timed_mutex m_mutex;
=======
      /// Type used internally to clean up memory at the end of the process
      class TDestructorHolder {
      public:
         /// Constructor with a new TVirtualDestructor pointer
         TDestructorHolder( TVirtualDestructor* d = 0 );
         /// Destructor
         ~TDestructorHolder();

         /// The managed object
         TVirtualDestructor* m_destructor;
      };

      /// Type of the internal map
      typedef std::map< const std::type_info*, TDestructorHolder > Map_t;
      /// Internal map of known destructor objects
      Map_t m_types;
>>>>>>> release/21.0.127

   }; // class TDestructorRegistry

} // namespace xAOD

// Include the template implementation:
#include "TDestructorRegistry.icc"

#endif // XAODROOTACCESS_TOOLS_TDESTRUCTORREGISTRY_H
