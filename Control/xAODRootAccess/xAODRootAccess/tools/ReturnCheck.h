// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef XAODROOTACCESS_TOOLS_RETURNCHECK_H
#define XAODROOTACCESS_TOOLS_RETURNCHECK_H

// ROOT include(s):
#include <TError.h>

// Local include(s):
<<<<<<< HEAD
#include "AsgMessaging/StatusCode.h"
=======
#include "xAODRootAccess/tools/TReturnCode.h"
>>>>>>> release/21.0.127
#include "xAODRootAccess/tools/Message.h"

/// Helper macro for checking return codes in a compact form in the code
///
/// This is pretty much a rip-off of the (ATH_)CHECK macros of the offline
<<<<<<< HEAD
/// code. It is used in the package in functions that return a StatusCode,
/// and themselves call functions returning StatusCode.
=======
/// code. It is used in the package in functions that return a TReturnCode,
/// and themselves call functions returning TReturnCode.
>>>>>>> release/21.0.127
///
/// @param CONTEXT A context string to print an error message on failure
/// @param EXP The expression to execute in a checked manner
///
#define RETURN_CHECK( CONTEXT, EXP )                                 \
   do {                                                              \
      const auto result = EXP;                                       \
      if( ! result.isSuccess() ) {                                   \
         ::Error( CONTEXT, XAOD_MESSAGE( "Failed to execute: %s" ),  \
                  #EXP );                                            \
         return result;                                              \
      }                                                              \
   } while( false )

#endif // XAODROOTACCESS_TOOLS_RETURNCHECK_H
