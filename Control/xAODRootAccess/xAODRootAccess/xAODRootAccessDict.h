// Dear emacs, this is -*- c++ -*-
<<<<<<< HEAD
//
//  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
//
=======

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: xAODRootAccessDict.h 746122 2016-05-11 10:11:31Z krasznaa $
>>>>>>> release/21.0.127
#ifndef XAODROOTACCESS_XAODROOTACCESSDICT_H
#define XAODROOTACCESS_XAODROOTACCESSDICT_H

// Local includude(s):
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODRootAccess/TActiveStore.h"
#include "xAODRootAccess/TPyEvent.h"
#include "xAODRootAccess/TPyStore.h"
#include "xAODRootAccess/TTreeMgr.h"
#include "xAODRootAccess/tools/TVirtualManager.h"

#endif // XAODROOTACCESS_XAODROOTACCESSDICT_H
