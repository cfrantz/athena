// Dear emacs, this is -*- c++ -*-
<<<<<<< HEAD
//
// Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
//
=======

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: TTreeMgr.h 746122 2016-05-11 10:11:31Z krasznaa $
>>>>>>> release/21.0.127
#ifndef XAODROOTACCESS_TTREEMGR_H
#define XAODROOTACCESS_TTREEMGR_H

// System include(s):
#include <memory>
#include <string>
#include <vector>

// Local include(s):
#include "xAODRootAccess/TEvent.h"
<<<<<<< HEAD
#include "AsgMessaging/StatusCode.h"
#include "xAODRootAccess/tools/xAODTEventTree.h"
#include "xAODRootAccess/tools/xAODTMetaTree.h"
=======
#include "xAODRootAccess/tools/TReturnCode.h"
#include "xAODRootAccess/tools/TEventTree.h"
#include "xAODRootAccess/tools/TMetaTree.h"
>>>>>>> release/21.0.127

// Forward declaration(s):
class TTree;
class TFile;

namespace xAOD {

   /// Class creating (a) transient tree(s) from xAOD files
   ///
   /// This class is a replacement for the many MakeTransientTree(...)
   /// functions used earlier. It can be used to create a transient tree
   /// describing the event tree, and one describing the metadata tree
   /// of the input file(s).
   ///
   /// @author Attila Krasznahorkay <Attila.Krasznahorkay@cern.ch>
   ///
<<<<<<< HEAD
=======
   /// $Revision: 746122 $
   /// $Date: 2016-05-11 12:11:31 +0200 (Wed, 11 May 2016) $
   ///
>>>>>>> release/21.0.127
   class TTreeMgr {

   public:
      /// Constructor, with an optional access mode selector
      TTreeMgr( TEvent::EAuxMode mode = TEvent::kUndefinedAccess );

      /// @name Function(s) setting up the input(s) for the tree(s)
      /// @{

      /// Read from the file given to the function
<<<<<<< HEAD
      StatusCode readFrom( ::TFile* file, ::Bool_t useTreeCache = kTRUE,
=======
      TReturnCode readFrom( ::TFile* file, ::Bool_t useTreeCache = kTRUE,
>>>>>>> release/21.0.127
                            const char* treeName = "CollectionTree" );

      /// @}

      /// @name Function(s) used for filtering the contents of the trees
      /// @{

      /// Object/container names that should be used in the event tree
<<<<<<< HEAD
      StatusCode enableEventObj( const std::vector< std::string >& names );
      /// Object/container names that should be vetoed from the event tree
      StatusCode suppressEventObj( const std::vector< std::string >& names );

      /// Object/container names that should be used in the metadata tree
      StatusCode enableMetaObj( const std::vector< std::string >& names );
      /// Object/container names that should be suppressed in the metadata tree
      StatusCode suppressMetaObj( const std::vector< std::string >& names );
=======
      TReturnCode enableEventObj( const std::vector< std::string >& names );
      /// Object/container names that should be vetoed from the event tree
      TReturnCode suppressEventObj( const std::vector< std::string >& names );

      /// Object/container names that should be used in the metadata tree
      TReturnCode enableMetaObj( const std::vector< std::string >& names );
      /// Object/container names that should be suppressed in the metadata tree
      TReturnCode suppressMetaObj( const std::vector< std::string >& names );
>>>>>>> release/21.0.127

      /// @}

      /// @name Transient tree accessors
      /// @{

      /// Get a pointer to the transient event tree
      ::TTree* eventTree();

      /// Get a pointer to the transient metadata tree
      ::TTree* metaTree();

      /// @}

      /// Get the TEvent object being used by the transient tree(s)
      TEvent& event();

   private:
      /// The transient event tree
<<<<<<< HEAD
      std::unique_ptr< xAODTEventTree > m_eventTree;
      /// The transient metadata tree
      std::unique_ptr< xAODTMetaTree > m_metaTree;
=======
      std::unique_ptr< TEventTree > m_eventTree;
      /// The transient metadata tree
      std::unique_ptr< TMetaTree > m_metaTree;
>>>>>>> release/21.0.127

      /// The internal TEvent object
      TEvent m_event;

      /// The name of the event tree to create
      std::string m_eventTreeName;

      /// Names to select for the event tree
      std::vector< std::string > m_enableEventObj;
      /// Names to suppress from the event tree
      std::vector< std::string > m_suppressEventObj;

      /// Names to select for the metatada tree
      std::vector< std::string > m_enableMetaObj;
      /// Names to suppress from the metadata tree
      std::vector< std::string > m_suppressMetaObj;

   }; // class TTreeMgr

} // namespace xAOD

#endif // XAODROOTACCESS_TTREEMGR_H
