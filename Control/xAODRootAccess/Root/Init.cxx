<<<<<<< HEAD
// Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: Init.cxx 687014 2015-08-03 09:30:27Z krasznaa $
>>>>>>> release/21.0.127

// System include(s):
#include <iostream>
#include <iomanip>
#include <cstring>
#include <string>
#include <cstdlib>
#include <sstream>

// ROOT include(s):
#include <TApplication.h>
<<<<<<< HEAD
#include <TClass.h>
#include <TError.h>
#include <TSystem.h>
=======
#include <TError.h>
#include <TSystem.h>
#if ROOT_VERSION_CODE < ROOT_VERSION( 5, 99, 0 )
#   include <Cintex/Cintex.h>
#endif // ROOT_VERSION
>>>>>>> release/21.0.127

// Local include(s):
#include "xAODRootAccess/Init.h"

<<<<<<< HEAD
=======
// Integrate with Apple's crash reporter. Taken directly from ROOT's TError.cxx.
// Disabled for now, as it doesn't seem to make any difference on top of 6.02/12
// on MacOS X 10.10.4.
#ifdef __APPLE__
/*
extern "C" {
   static const char* __crashreporter_info__ = 0;
   asm( ".desc ___crashreporter_info__, 0x10" );
}
*/
#endif // __APPLE__

>>>>>>> release/21.0.127
namespace xAOD {

   /// Pointer to the original error handler function if there was one
   static ErrorHandlerFunc_t sErrorHandler = 0;
   /// Function filtering the warnings coming from ROOT
   void ErrorHandler( Int_t level, Bool_t abort, const char* location,
                      const char* message );

   /// Internal status flag
   static bool sInitialised = false;

   /// Width of the message source strings
   static size_t sMessageSourceWidth = 25;

<<<<<<< HEAD
   StatusCode Init( const char* appname ) {
=======
   TReturnCode Init( const char* appname ) {
>>>>>>> release/21.0.127

      return Init( appname, 0, 0 );
   }

<<<<<<< HEAD
   StatusCode Init( const char* appname, int* argc, char** argv ) {

      // Check if we need to do anything:
      if( sInitialised ) return StatusCode::SUCCESS;
=======
   TReturnCode Init( const char* appname, int* argc, char** argv ) {

      // Check if we need to do anything:
      if( sInitialised ) return TReturnCode::kSuccess;
>>>>>>> release/21.0.127

      // Set up our own error handler function:
      sErrorHandler = ::SetErrorHandler( ErrorHandler );
      if( ! sErrorHandler ) {
         std::cerr << "<xAOD::Init> ERROR Couldn't set up ROOT message "
                   << "filtering" << std::endl;
<<<<<<< HEAD
         return StatusCode::FAILURE;
=======
         return TReturnCode::kFailure;
>>>>>>> release/21.0.127
      }

      // Create an application. This is needed to ensure the auto-loading
      // of the xAOD dictionaries.
      if( ! gApplication ) {
         if( argc && argv ) {
<<<<<<< HEAD
            [[maybe_unused]]
            static ::TApplication sApplication( appname, argc, argv );
=======
            static ::TApplication sApplication( appname, argc, argv );
            // This is just here to avoid a warning about not using this
            // static variable:
            sApplication.Argc();
>>>>>>> release/21.0.127
         } else {
            ::TApplication::CreateApplication();
         }
      }

<<<<<<< HEAD
      // Load the libraries in a carefully selected order.
      // This is a temporary work-around (26 Oct 20) until the current
      // xAOD dictionary issues are worked out.
      for (const char *name : {
            "xAOD::TruthParticle_v1",
            "xAOD::MuonRoI_v1",
            "xAOD::CaloCluster_v1",
            "xAOD::TrackParticle_v1",
            "xAOD::Electron_v1",
            "xAOD::Muon_v1",
            "xAOD::Jet_v1",
            "xAOD::TauJet_v1",
            "xAOD::PFO_v1",
            "xAOD::TrigElectron_v1",
            "xAOD::L2CombinedMuon_v1",
            "xAOD::Particle_v1"}) {
        // silently ignore missing classes, because this gets used in
        // all projects, and not all projects contain all xAOD classes
        static constexpr Bool_t LOAD = kTRUE;
        static constexpr Bool_t SILENT = kTRUE;
        TClass::GetClass( name, LOAD, SILENT );
      }
=======
#if ROOT_VERSION_CODE < ROOT_VERSION( 5, 99, 0 )
      // Enable Cintex:
      ROOT::Cintex::Cintex::Enable();
#endif // ROOT_VERSION
>>>>>>> release/21.0.127

      // Let the user know what happened:
      ::Info( appname, "Environment initialised for data access" );

      // Return gracefully:
      sInitialised = true;
<<<<<<< HEAD
      return StatusCode::SUCCESS;
=======
      return TReturnCode::kSuccess;
>>>>>>> release/21.0.127
   }

   void SetMessageSourceWidth( size_t value ) {

      sMessageSourceWidth = value;
      return;
   }

   /// All ROOT/xAOD messages are set up to pass through this function.
   /// It is a good place to selectively turn off printing some warnings
   /// coming from ROOT that the xAOD users should just not be bothered
   /// with.
   ///
   /// @param level The message level
   /// @param abort Whether the job needs to abort
   /// @param location The source of the message
   /// @param message The message that needs to be printed
   ///
   void ErrorHandler( Int_t level, Bool_t abort, const char* location,
                      const char* message ) {

      // Check if we need to print anything for this level:
      if( level < gErrorIgnoreLevel ) {
         return;
      }

      // Source of the missing dictionary warnings:
<<<<<<< HEAD
      static const char* DICT_WARNING_SOURCE = "TClass::Init";
=======
#if ROOT_VERSION_CODE < ROOT_VERSION( 6, 0, 2 )
      static const char* DICT_WARNING_SOURCE = "TClass::TClass";
#else
      static const char* DICT_WARNING_SOURCE = "TClass::Init";
#endif // ROOT_VERSION
>>>>>>> release/21.0.127

      // Filter out warnings about missing dictionaries. As these are relatively
      // common. In case a problem occurs because of a missing dictionary, some
      // other piece of code will complain anyway.
      if( ( level == kWarning ) &&
          ( ! strcmp( location, DICT_WARNING_SOURCE ) ) ) {
         return;
      }

      // Construct a string version of the message's level:
      const char* msgLevel = 0;
      if( level >= kFatal ) {
         msgLevel = "FATAL  ";
      } else if( level >= kError ) {
         msgLevel = "ERROR  ";
      } else if( level >= kWarning ) {
         msgLevel = "WARNING";
      } else {
         msgLevel = "INFO   ";
      }

      // Make sure that the message's source/location is not longer than a
      // pre-set maximum value:
      std::string source( location );
      if( source.size() > sMessageSourceWidth ) {
         source = source.substr( 0, sMessageSourceWidth - 3 );
         source += "...";
      }

      // Print the message to stdout/std::cout:
      std::ostringstream output;
      output << std::setiosflags( std::ios::left )
             << std::setw( sMessageSourceWidth ) << source << " " << msgLevel
             << " " << message;
      std::cout << output.str() << std::endl;

      // If we don't need to abort, return now:
      if( ! abort ) {
         return;
      }

<<<<<<< HEAD
=======
#ifdef __APPLE__
      /*
      if( __crashreporter_info__ ) {
         delete[] __crashreporter_info__;
      }
      __crashreporter_info__ = StrDup( output.str().c_str() );
      */
#endif // __APPLE__

>>>>>>> release/21.0.127
      // Abort with a stack trace if possible:
      std::cout << std::endl << "Aborting..." << std::endl;
      if( gSystem ) {
         gSystem->StackTrace();
         gSystem->Abort();
      } else {
         std::abort();
      }

      return;
   }

} // namespace xAOD
