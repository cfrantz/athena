#!/usr/bin/env python
<<<<<<< HEAD
#
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======

# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
#
# $Id: ut_xaodrootaccess_tpyevent_test.py 741414 2016-04-19 17:06:16Z krasznaa $
>>>>>>> release/21.0.127
#
# Unit test for the xAOD::TPyEvent class.
#

## C/C++ style main function
def main():

    # The name of the application:
    APP_NAME = "ut_xaodrootaccess_tpyevent_test"

    # Set up a logger object:
    import logging
    logger = logging.getLogger( APP_NAME )
    logger.setLevel( logging.INFO )
    hdlr = logging.StreamHandler( sys.stdout )
    frmt = logging.Formatter( "%(name)-14s%(levelname)8s %(message)s" )
    hdlr.setFormatter( frmt )
    logger.addHandler( hdlr )

    # Set up the environment:
    import ROOT
<<<<<<< HEAD
    if not ROOT.xAOD.Init( APP_NAME ).isSuccess():
        logger.error( "Failed to call xAOD::Init(...)" )
        return 1

    # Pre-load some dictionaries. To avoid ROOT-10940.
    ROOT.xAOD.L2CombinedMuonContainer()
    ROOT.xAOD.TrigElectronContainer()
    ROOT.xAOD.MuonContainer()

=======
    if ROOT.gROOT.Macro( "$ROOTCOREDIR/scripts/load_packages.C" ):
        logger.error( "Couldn't load the RootCore packages" )
        return 1
    if ROOT.xAOD.Init( APP_NAME ).isFailure():
        logger.error( "Failed to call xAOD::Init(...)" )
        return 1

>>>>>>> release/21.0.127
    # Create the objects to test:
    from xAODRootAccess.TPyEvent import TPyEvent
    event = TPyEvent()
    from xAODRootAccess.TPyStore import TPyStore
    store = TPyStore()

    # Create a transient tree from a test file:
<<<<<<< HEAD
    import os
    ifile = ROOT.TFile.Open( "$ASG_TEST_FILE_MC", "READ" )
    if not ifile:
        logger.error( "Couldn't open input file: %s" %
                      os.environ.get( "ASG_TEST_FILE_MC",
                                      "!ASG_TEST_FILE_MC!" ) )
        return 1
    tree = ROOT.xAOD.MakeTransientTree( ifile )
    if not tree:
        logger.error( "Failed to make transient tree from file: %s" %
                      os.environ.get( "ASG_TEST_FILE_MC",
                                      "!ASG_TEST_FILE_MC!" ) )
        return 1
    import xAODRootAccess.GenerateDVIterators
=======
    FNAME = "/afs/cern.ch/atlas/project/PAT/xAODs/r5591/" \
            "mc14_8TeV.117050.PowhegPythia_P2011C_ttbar.recon." \
            "AOD.e1727_s1933_s1911_r5591/AOD.01494881._105458.pool.root.1"
    ifile = ROOT.TFile.Open( FNAME, "READ" )
    if not ifile:
        logger.error( "Couldn't open input file: %s" % FNAME )
        return 1
    tree = ROOT.xAOD.MakeTransientTree( ifile )
    if not tree:
        logger.error( "Failed to make transient tree from file: %s" % FNAME )
        return 1
>>>>>>> release/21.0.127

    # Connect the TPyEvent object to an output file:
    ofile = ROOT.TFile.Open( "test.root", "RECREATE" )
    if not ofile:
        logger.error( "Couldn't create test output file" )
        return 1
    if not event.writeTo( ofile ).isSuccess():
        logger.error( "Couldn't connect xAOD::TPyEvent object to the output "
                      "file" )
        return 1

    # Loop over 10 events from the input file:
<<<<<<< HEAD
    for entry in range( 10 ):
=======
    for entry in xrange( 10 ):
>>>>>>> release/21.0.127

        # Load the event:
        tree.GetEntry( entry )
        logger.info( "Processing entry %i" % entry )

        # Clear the store:
        store.clear()

<<<<<<< HEAD
        # Copy the Kt4EMTopoOriginEventShape payload:
        if not event.record( tree.Kt4EMTopoOriginEventShape,
                             "Kt4EMTopoOriginEventShape" ).isSuccess():
            logger.error( "Failed to record xAOD::EventShape from the "
                          "input file" )
            return 1
        if not event.record( tree.Kt4EMTopoOriginEventShape.getConstStore(),
                             "Kt4EMTopoOriginEventShapeAux." ).isSuccess():
            logger.error( "Faiedl to record xAOD::EventShapeAuxInfo from the "
                          "input file" )
            return 1

        # Check that the muons can be accessed:
        logger.info( "  Number of muons: %i" %
                     tree.Muons.size() )

        # Copy the muons into the output:
        if not event.record( tree.Muons,
                             "AllMuons" ).isSuccess():
            logger.error( "Failed to record xAOD::MuonContainer from the "
                          "input file" )
            return 1
        if not event.record( tree.Muons.getConstStore(),
                             "AllMuonsAux." ).isSuccess():
            logger.error( "Failed to record xAOD::MuonAuxContainer from "
                          "the input file" )
            return 1

        # Create a container of just the central muons:
        cMuons = ROOT.xAOD.MuonContainer()

        # And record the container into the output right away. (In order to get
        # a proper auxiliary container for it.)
        cMuons.setNonConstStore( event.recordAux( "CentralMuonsAux." ) )
        if not event.record( cMuons,
                             "CentralMuons" ).isSuccess():
            logger.error( "Failed to record central muons into the output" )
            return 1

        # Now put all central muons into this container, with just a few
        # properties. Since deep copying doesn't work this easily... :-(
        for mu in tree.Muons:
            if abs( mu.eta() ) < 1.0:
                newMu = ROOT.xAOD.Muon()
                cMuons.push_back( newMu )
                newMu.setP4( mu.pt(), mu.eta(), mu.phi() )
                pass
            pass

        # Print how many central muons got selected:
        logger.info( "  Number of central muons: %i" %
                     cMuons.size() )

        # Put an object into the transient store:
        trMuons = ROOT.xAOD.MuonContainer()
        if not store.record( trMuons, "TransientMuons" ).isSuccess():
            logger.error( "Failed to record transient muons into the "
=======
        # Copy the EventInfo payload:
        if not event.record( tree.EventInfo,
                             "EventInfo" ).isSuccess():
            logger.error( "Failed to record xAOD::EventInfo from the "
                          "input file" )
            return 1
        if not event.record( tree.EventInfo.getConstStore(),
                             "EventInfoAux." ).isSuccess():
            logger.error( "Faiedl to record xAOD::EventAuxInfo from the "
                          "input file" )
            return 1

        # Check that the electrons can be accessed:
        logger.info( "  Number of electrons: %i" %
                     tree.ElectronCollection.size() )

        # Copy the electrons into the output:
        if not event.record( tree.ElectronCollection,
                             "AllElectrons" ).isSuccess():
            logger.error( "Failed to record xAOD::ElectronContainer from the "
                          "input file" )
            return 1
        if not event.record( tree.ElectronCollection.getConstStore(),
                             "AllElectronsAux." ).isSuccess():
            logger.error( "Failed to record xAOD::ElectronAuxContainer from "
                          "the input file" )
            return 1

        # Create a container of just the central electrons:
        cElectrons = ROOT.xAOD.ElectronContainer_v1()

        # And record the container into the output right away. (In order to get
        # a proper auxiliary container for it.)
        cElectrons.setNonConstStore( event.recordAux( "CentralElectronsAux." ) )
        if not event.record( cElectrons,
                             "CentralElectrons" ).isSuccess():
            logger.error( "Failed to record central electrons into the output" )
            return 1

        # Now put all central electrons into this container, with just a few
        # properties. Since deep copying doesn't work this easily... :-(
        for i in xrange( tree.ElectronCollection.size() ):
            el = tree.ElectronCollection.at( i )
            if abs( el.eta() ) < 1.0:
                newEl = ROOT.xAOD.Electron_v1()
                cElectrons.push_back( newEl )
                newEl.setP4( el.pt(), el.eta(), el.phi(), el.m() )
                pass
            pass

        # Print how many central electrons got selected:
        logger.info( "  Number of central electrons: %i" %
                     cElectrons.size() )

        # Put an object into the transient store:
        trElectrons = ROOT.xAOD.ElectronContainer_v1()
        if not store.record( trElectrons, "TransientElectrons" ).isSuccess():
            logger.error( "Failed to record transient electrons into the "
>>>>>>> release/21.0.127
                          "transient store" )
            return 1

        # Check that it is now available through TPyEvent:
<<<<<<< HEAD
        if not event.contains( "TransientMuons",
                               ROOT.xAOD.MuonContainer ):
            logger.error( "Transient muons not visible through TPyEvent" )
=======
        if not event.contains( "TransientElectrons",
                               ROOT.xAOD.ElectronContainer_v1 ):
            logger.error( "Transient electrons not visible through TPyEvent" )
>>>>>>> release/21.0.127
            return 1

        # Record the event:
        event.fill()
        pass

    # Close the output file:
    if not event.finishWritingTo( ofile ).isSuccess():
        logger.error( "Couldn't call finishWritingTo(...)" )
        return 1
    ofile.Close()

    # Clean up:
    ROOT.xAOD.ClearTransientTrees()

    # Return gracefully:
    return 0

# Run the main() function:
if __name__ == "__main__":
    import sys
    sys.exit( main() )
