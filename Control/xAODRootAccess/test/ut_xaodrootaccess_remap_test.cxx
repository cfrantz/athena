/*
<<<<<<< HEAD
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id: ut_xaodrootaccess_remap_test.cxx 646132 2015-02-11 15:17:44Z krasznaa $

>>>>>>> release/21.0.127
// System include(s):
#include <memory>

// ROOT include(s):
#include <TFile.h>
#include <TError.h>
#include <TUUID.h>
#include <TString.h>
#include <TSystem.h>
#include <TTree.h>

// EDM include(s):
#include "AthContainers/AuxVectorBase.h"

<<<<<<< HEAD
#include "AsgMessaging/MessageCheck.h"

=======
>>>>>>> release/21.0.127
// Local include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/ReturnCheck.h"

int main() {

<<<<<<< HEAD
   ANA_CHECK_SET_TYPE (int);
   using namespace asg::msgUserCode;

=======
>>>>>>> release/21.0.127
   // The application's name:
   static const char* APP_NAME = "ut_xaodrootaccess_remap_test";

   // Initialise the environment:
<<<<<<< HEAD
   ANA_CHECK( xAOD::Init( APP_NAME ) );
=======
   RETURN_CHECK( APP_NAME, xAOD::Init( APP_NAME ) );
>>>>>>> release/21.0.127

   // Create the object(s) used for the testing:
   xAOD::TEvent event;

   // Declare some name remappings:
<<<<<<< HEAD
   ANA_CHECK( event.addNameRemap( "Muons",
                                               "MyMuons" ) );
   ANA_CHECK( event.addNameRemap( "Taus", "MyTaus" ) );
=======
   RETURN_CHECK( APP_NAME, event.addNameRemap( "ElectronCollection",
                                               "Electrons" ) );
   RETURN_CHECK( APP_NAME, event.addNameRemap( "TauJetCollection", "Taus" ) );
>>>>>>> release/21.0.127

   // Print the definitions:
   event.printNameRemap();

   // Connect the TEvent object to an input file:
<<<<<<< HEAD
   static const char* FNAME = "${ASG_TEST_FILE_DATA}";
   std::unique_ptr< ::TFile > ifile( ::TFile::Open( FNAME, "READ" ) );
   if( ! ifile.get() ) {
      ::Error( APP_NAME, XAOD_MESSAGE( "Couldn't open file %s" ), FNAME );
      return 1;
   }
   ANA_CHECK( event.readFrom( ifile.get() ) );
=======
   static const char* INFILE = "/afs/cern.ch/atlas/project/PAT/xAODs/r5787/"
      "mc14_13TeV.110401.PowhegPythia_P2012_ttbar_nonallhad.merge.AOD."
      "e2928_s1982_s2008_r5787_r5853_tid01597980_00/"
      "AOD.01597980._000098.pool.root.1";
   std::unique_ptr< ::TFile > ifile( ::TFile::Open( INFILE, "READ" ) );
   if( ! ifile.get() ) {
      ::Error( APP_NAME, XAOD_MESSAGE( "Couldn't open file %s" ), INFILE );
      return 1;
   }
   RETURN_CHECK( APP_NAME, event.readFrom( ifile.get() ) );
>>>>>>> release/21.0.127

   // Load the first event:
   if( event.getEntry( 0 ) < 0 ) {
      ::Error( APP_NAME, XAOD_MESSAGE( "Couldn't load event 0 from file %s" ),
<<<<<<< HEAD
               FNAME );
=======
               INFILE );
>>>>>>> release/21.0.127
      return 1;
   }

   // Retrieve a DataVector using an alias, and the actual name:
   const SG::AuxVectorBase* vec = 0;
<<<<<<< HEAD
   ANA_CHECK( event.retrieve( vec, "Muons" ) );
   ANA_CHECK( event.retrieve( vec, "MyMuons" ) );
=======
   RETURN_CHECK( APP_NAME, event.retrieve( vec, "Electrons" ) );
   RETURN_CHECK( APP_NAME, event.retrieve( vec, "ElectronCollection" ) );
>>>>>>> release/21.0.127

   // Create a dummy, temporary file to test the object copying:
   TUUID uuid;
   const ::TString tempFName( ::TString::Format( "%s-%s.root", APP_NAME,
                                                 uuid.AsString() ) );
   std::unique_ptr< ::TFile > ofile( ::TFile::Open( tempFName, "CREATE" ) );
   if( ! ofile.get() ) {
      ::Error( APP_NAME, XAOD_MESSAGE( "Couldn't open temporary file with name "
                                       "\"%s\"" ),
               tempFName.Data() );
      return 1;
   }
<<<<<<< HEAD
   ANA_CHECK( event.writeTo( ofile.get() ) );

   // Copy the electrons to the output:
   ANA_CHECK( event.copy( "MyMuons" ) );
=======
   RETURN_CHECK( APP_NAME, event.writeTo( ofile.get() ) );

   // Copy the electrons to the output:
   RETURN_CHECK( APP_NAME, event.copy( "Electrons" ) );
>>>>>>> release/21.0.127

   // Write the event:
   if( event.fill() < 0 ) {
      ::Error( APP_NAME, XAOD_MESSAGE( "There was an error writing the event "
                                       "to the in-memory file") );
      return 1;
   }

   // Finish writing to the file:
<<<<<<< HEAD
   ANA_CHECK( event.finishWritingTo( ofile.get() ) );
=======
   RETURN_CHECK( APP_NAME, event.finishWritingTo( ofile.get() ) );
>>>>>>> release/21.0.127

   ::TTree* otree = dynamic_cast< ::TTree* >( ofile->Get( "CollectionTree" ) );
   if( ! otree ) {
      ::Error( APP_NAME, XAOD_MESSAGE( "No CollectionTree found in test output "
                                       "file" ) );
      return 1;
   }
   otree->Print();

   // Now start reading from this temporary file to test its health:
<<<<<<< HEAD
   ANA_CHECK( event.readFrom( ofile.get() ) );
=======
   RETURN_CHECK( APP_NAME, event.readFrom( ofile.get() ) );
>>>>>>> release/21.0.127

   // Load the first, and only event:
   if( event.getEntry( 0 ) < 0 ) {
      ::Error( APP_NAME, XAOD_MESSAGE( "Couldn't load event 0 from file %s" ),
               tempFName.Data() );
      return 1;
   }

   // Retrieve a DataVector using the actual name this time:
<<<<<<< HEAD
   ANA_CHECK( event.retrieve( vec, "MyMuons" ) );

   // And this should fail:
   if( event.retrieve( vec, "Muons" ).isSuccess() ) {
=======
   RETURN_CHECK( APP_NAME, event.retrieve( vec, "Electrons" ) );

   // And this should fail:
   if( event.retrieve( vec, "ElectronCollection" ).isSuccess() ) {
>>>>>>> release/21.0.127
      ::Error( APP_NAME, XAOD_MESSAGE( "Failure is retrieval logic" ) );
      return 1;
   }

   // Clean up:
   ofile.reset();
   gSystem->Unlink( tempFName );

   // Return gracefully:
   return 0;
}
