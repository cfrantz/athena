<<<<<<< HEAD
/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

=======
// Dear emacs, this is -*- c++ -*-
// $Id: IAsgTool.h 804869 2017-05-15 20:14:34Z krumnack $
>>>>>>> release/21.0.127
#ifndef ASGTOOLS_IASGTOOL_H
#define ASGTOOLS_IASGTOOL_H

// System include(s):
#include <string>

// Local include(s):
#include "AsgTools/AsgToolMacros.h"
#include "AsgTools/INamedInterface.h"

// Environment specific include(s):
#ifndef XAOD_STANDALONE
#   include "GaudiKernel/IAlgTool.h"
#else
#   include "AsgMessaging/StatusCode.h"
#   include "AsgMessaging/INamedInterface.h"
#endif

namespace asg {

   /// Base class for the dual-use tool interface classes
   ///
   /// This class is used like IAlgTool is used for Athena-only
   /// interfaces. It is used as the base class for the pure virtual
   /// tool interface classes.
   ///
   /// @author David Adams <dladams@bnl.gov>
   ///
   /// $Revision: 804869 $
   /// $Date: 2017-05-15 22:14:34 +0200 (Mon, 15 May 2017) $
   ///
   class IAsgTool
#ifndef XAOD_STANDALONE
      : virtual public ::IAlgTool
#else
<<<<<<< HEAD
      : virtual public INamedInterface
#endif // not XAOD_STANDALONE
=======
   : virtual public INamedInterface
#endif // ASGTOOL_ATHENA
>>>>>>> release/21.0.127
   {

   public:
      /// Virtual destructor, to make vtable happy...
      virtual ~IAsgTool() {}

#ifdef XAOD_STANDALONE

      /// @name Functions coming from IAlgTool in Athena
      /// @{

      /// Function initialising the tool
      virtual StatusCode initialize() = 0;

      /// Set the name of the tool
      virtual void setName( const std::string& name ) = 0;

      /// @}

#endif // XAOD_STANDALONE

      /// Print the state of the tool
      virtual void print() const = 0;

   }; // class IAsgTool

} // namespace asg

#endif // ASGTOOLS_IASGTOOL_H
