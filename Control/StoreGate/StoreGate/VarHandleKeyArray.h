/*
<<<<<<< HEAD
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#ifndef STOREGATE_VARHANDLEKEYARRAY_H
#define STOREGATE_VARHANDLEKEYARRAY_H 1

/**
 * @file StoreGate/VarHandleKeyArray.h
 * @author C. Leggett
 * @date Updated: Jun 17, 2016
 * @brief Base class for VarHandleKeyArray for reading from StoreGate.
 */

#include "GaudiKernel/DataHandle.h"
<<<<<<< HEAD
#include "GaudiKernel/IDataHandleHolder.h"
=======
>>>>>>> release/21.0.127
#include "StoreGate/VarHandleKey.h"

#include <vector>
#include <string>

namespace SG {

  /**
   * @class SG::VarHandleKeyArray<T>
   * @brief untemplated base class for VarHandleKeyArrays
   */
  class VarHandleKeyArray {
  public:
    VarHandleKeyArray(){};
<<<<<<< HEAD
    virtual ~VarHandleKeyArray() = default;
=======
    virtual ~VarHandleKeyArray(){};
>>>>>>> release/21.0.127
    virtual StatusCode assign(const std::vector<std::string>& vs)=0;
    virtual std::string toString() const = 0;
    virtual Gaudi::DataHandle::Mode mode() const = 0;

    virtual std::vector<SG::VarHandleKey*> keys() const = 0;

<<<<<<< HEAD
    virtual void renounce() = 0;
    virtual bool renounced() const = 0;
    virtual void declare(IDataHandleHolder*)  = 0;

    virtual void setOwner( IDataHandleHolder* o ) = 0;
    virtual const IDataHandleHolder* owner() const = 0;
    virtual IDataHandleHolder* owner() = 0;

=======
>>>>>>> release/21.0.127
  };

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
// mixin class for common functionality
//

  /**
   * @class SG::VarHandleKeyArrayCommon<T>
   * @brief mixin base class for VarHandleKeyArrays, inheriting from
   * std::vector as well as VarHandleKeyArray to provide vector-like
   * access
   *
   */

  template <class Base>
  class VarHandleKeyArrayCommon : public VarHandleKeyArray, 
                                  public std::vector<Base> {
  public:
    /**
     * @brief default base Constructor of mixin
     *
     */
    VarHandleKeyArrayCommon() : std::vector<Base>() {};
    
    /**
     * @brief base Constructor from a VarHandleKeyArray that takes a vector
     * @param v vector of Read/Write/UpdateHandleKey
     */
    VarHandleKeyArrayCommon( const std::vector<Base>& v ):
      std::vector<Base>(v) {};
    
    /**
     * @brief base Constructor from a VarHandleKeyArray that takes an 
     * initializer list of VarHandleKeys
     * @param l initializer list of Read/Write/UpdateHandleKey
     */
    VarHandleKeyArrayCommon( std::initializer_list<Base> l ):
      std::vector<Base>{l} {};
    
    /**
     * @brief base Constructor from a VarHandleKeyArray that takes an 
     * initializer list of std::strings.
     * @param l initializer list of std::strings used to create the
     *          VarHandleKeys
     */
    VarHandleKeyArrayCommon( std::initializer_list<std::string> l ) {
      for (auto &e : l) {
        this->push_back( Base{e} );
      }
    }    
    
    /**
     * @brief forward the initialization to the member VarHandleKeys
<<<<<<< HEAD
     * @param used If false, then this handle is not to be used.
     *             Instead of normal initialization, the key will be cleared.
     */
    StatusCode initialize (bool used = true);
=======
     */
    StatusCode initialize();
>>>>>>> release/21.0.127

    /**
     * @brief Set the contents of the VarHandleKeyArray from a 
     * vector of std::strings
     * @param vs vector of initializer strings
     */
<<<<<<< HEAD
    virtual StatusCode assign(const std::vector<std::string>& vs) override;
=======
    StatusCode assign(const std::vector<std::string>& vs);
>>>>>>> release/21.0.127

    /**
     * @brief string representation of the VarHandleKeyArray
     */
<<<<<<< HEAD
    virtual std::string toString() const override;
=======
    std::string toString() const;
>>>>>>> release/21.0.127

    /**
     * @brief create array of all base VarHandleKeys in the Array
     */
<<<<<<< HEAD
    virtual std::vector<SG::VarHandleKey*> keys() const override;
    

    /**
     * @brief if called, handles will not be declared in the algorithm I/O
     */
    virtual void renounce() override { m_isRenounced = true; }
    
    /**
     * @brief query renounced state
     **/ 
    virtual bool renounced() const override { return m_isRenounced; }

    virtual void declare( IDataHandleHolder* ) override;

    virtual void setOwner( IDataHandleHolder* o ) override { m_owner = o; }
    virtual const IDataHandleHolder* owner() const override { return m_owner; }
    virtual IDataHandleHolder* owner() override { return m_owner; }

  private:
    
    bool m_isRenounced{ false };
    IDataHandleHolder* m_owner{ nullptr };

=======
    std::vector<SG::VarHandleKey*> keys() const;
    
>>>>>>> release/21.0.127
  };
  
} // namespace SG

<<<<<<< HEAD
namespace std {
  ostream& operator<<(ostream& s, const SG::VarHandleKeyArray& m);
}


=======
>>>>>>> release/21.0.127
#include "StoreGate/VarHandleKeyArray.icc"

#endif
