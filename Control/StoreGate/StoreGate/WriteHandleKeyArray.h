/*
<<<<<<< HEAD
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#ifndef STOREGATE_WRITEHANDLEKEYARRAY_H
#define STOREGATE_WRITEHANDLEKEYARRAY_H 1

<<<<<<< HEAD
#include "StoreGate/HandleKeyArray.h"
=======
#include "StoreGate/VarHandleKeyArray.h"
>>>>>>> release/21.0.127

#include "StoreGate/WriteHandleKey.h"
#include "StoreGate/WriteHandle.h"

<<<<<<< HEAD
=======
#include <vector>
#include <string>

>>>>>>> release/21.0.127
namespace SG {

  /**
   * @class SG::WriteHandleKeyArray<T>
   * @brief class to hold an array of WriteHandleKeys
   *
<<<<<<< HEAD
   * since it inherits from std::vector, all vector operations are
=======
   * since it inherits from std::vector, all vector operations are 
>>>>>>> release/21.0.127
   * permitted.
   *
   * initialization can be done in three ways.
   * 1: with an std::vector<WriteHandleKey> as a parameter
   *    SG::WriteHandleKeyArray<foo> m_foo ( std::vector<WriteHandleKey> );
   * 2: with an initializer list of WriteHandleKeys
   *    SG::WriteHandleKeyArray<foo> m_foo { WriteHandleKey<foo> k1, WriteHandleKey<foo> k2 };
   * 3: with an initializer list of std::strings, that will be used to
   *    internally create WriteHandleKeys with those initializers
   *    SG::WriteHandleKeyArray<foo> m_foo { "key1", "key2", "key3" };
   */
<<<<<<< HEAD
  template <class T>
  using WriteHandleKeyArray = HandleKeyArray<WriteHandle<T>,WriteHandleKey<T>, Gaudi::DataHandle::Writer >;
=======

  template <class T>
  class WriteHandleKeyArray : public VarHandleKeyArrayCommon< WriteHandleKey<T> > {
  public:
    /**
     * @brief default Constructor from a WriteHandleKeyArray
     */
    WriteHandleKeyArray(){};

    /**
     * @brief Constructor from a WriteHandleKeyArray that takes a vector
     * of ReaDHandleKeys
     * @param v vector of WriteHandleKey
     */
    WriteHandleKeyArray( const std::vector<WriteHandleKey<T>>& v ) :
      VarHandleKeyArrayCommon<WriteHandleKey<T>> ( v ) {};

    /**
     * @brief Constructor from a WriteHandleKeyArray that takes an 
     * initializer list of WriteHandleKeys
     * @param l initializer list of WriteHandleKey
     */
    WriteHandleKeyArray( std::initializer_list<WriteHandleKey<T>> l ):
      VarHandleKeyArrayCommon<WriteHandleKey<T>> {l} {};

    /**
     * @brief Constructor from a WriteHandleKeyArray that takes an 
     * initializer list of std::strings.
     * @param l initializer list of std::strings used to create the
     *          WriteHandleKeys
     */
    WriteHandleKeyArray( std::initializer_list<std::string> l ):
      VarHandleKeyArrayCommon<WriteHandleKey<T>> {l} {};

    /**
     * @brief return the type (Read/Write/Update) of handle
     */
    Gaudi::DataHandle::Mode mode() const { return Gaudi::DataHandle::Writer; }

    /**
     * @brief create a vector of WriteHandles from the WriteHandleKeys
     * in the array
     */
    std::vector< WriteHandle<T> > makeHandles() const {
      std::vector< WriteHandle<T> > hndl;
      typename std::vector<WriteHandleKey<T>>::const_iterator itr;
      for (itr = this->begin(); itr != this->end(); ++itr) {
        hndl.push_back ( WriteHandle<T>( *itr) );
      }
      return ( std::move( hndl ) );
    }

  };

>>>>>>> release/21.0.127

} // namespace SG

#endif
