/*
<<<<<<< HEAD
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#ifndef STOREGATE_READHANDLEKEYARRAY_H
#define STOREGATE_READHANDLEKEYARRAY_H 1

<<<<<<< HEAD
#include "StoreGate/HandleKeyArray.h"
=======
#include "StoreGate/VarHandleKeyArray.h"
>>>>>>> release/21.0.127

#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/ReadHandle.h"

<<<<<<< HEAD
=======
#include <vector>
#include <string>

>>>>>>> release/21.0.127
namespace SG {

  /**
   * @class SG::ReadHandleKeyArray<T>
   * @brief class to hold an array of ReadHandleKeys
   *
<<<<<<< HEAD
   * since it inherits from std::vector, all vector operations are
=======
   * since it inherits from std::vector, all vector operations are 
>>>>>>> release/21.0.127
   * permitted.
   *
   * initialization can be done in three ways.
   * 1: with an std::vector<ReadHandleKey> as a parameter
   *    SG::ReadHandleKeyArray<foo> m_foo ( std::vector<ReadHandleKey> );
   * 2: with an initializer list of ReadHandleKeys
   *    SG::ReadHandleKeyArray<foo> m_foo { ReadHandleKey<foo> k1, ReadHandleKey<foo> k2 };
   * 3: with an initializer list of std::strings, that will be used to
   *    internally create ReadHandleKeys with those initializers
   *    SG::ReadHandleKeyArray<foo> m_foo { "key1", "key2", "key3" };
   */
<<<<<<< HEAD
  template <class T>
  using ReadHandleKeyArray = HandleKeyArray<ReadHandle<T>, ReadHandleKey<T>, Gaudi::DataHandle::Reader >;
=======

  template <class T>
  class ReadHandleKeyArray : public VarHandleKeyArrayCommon< ReadHandleKey<T> > {
  public:
    /**
     * @brief default Constructor from a ReadHandleKeyArray
     */
    ReadHandleKeyArray(){};

    /**
     * @brief Constructor from a ReadHandleKeyArray that takes a vector
     * of ReaDHandleKeys
     * @param v vector of ReadHandleKey
     */
    ReadHandleKeyArray( const std::vector<ReadHandleKey<T>>& v ):
      VarHandleKeyArrayCommon<ReadHandleKey<T>> ( v ) {};

    /**
     * @brief Constructor from a ReadHandleKeyArray that takes an 
     * initializer list of ReadHandleKeys
     * @param l initializer list of ReadHandleKey
     */
    ReadHandleKeyArray( std::initializer_list<ReadHandleKey<T>> l ):
      VarHandleKeyArrayCommon<ReadHandleKey<T>> {l} {};

    /**
     * @brief Constructor from a ReadHandleKeyArray that takes an 
     * initializer list of std::strings.
     * @param l initializer list of std::strings used to create the
     *          ReadHandleKeys
     */
    ReadHandleKeyArray( std::initializer_list<std::string> l ):
      VarHandleKeyArrayCommon<ReadHandleKey<T>> {l} {};

    /**
     * @brief return the type (Read/Write/Update) of handle
     */
    Gaudi::DataHandle::Mode mode() const { return Gaudi::DataHandle::Reader; }

    /**
     * @brief create a vector of ReadHandles from the ReadHandleKeys
     * in the array
     */
    std::vector< ReadHandle<T> > makeHandles() const {
      std::vector< ReadHandle<T> > hndl;
      typename std::vector<ReadHandleKey<T>>::const_iterator itr;
      for (itr = this->begin(); itr != this->end(); ++itr) {
        hndl.push_back ( ReadHandle<T>( *itr) );
      }
      return ( std::move( hndl ) );
    }

  };
>>>>>>> release/21.0.127


} // namespace SG

#endif
