# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# Declare the package name.
atlas_subdir( TrkVKalVrtFitter )

<<<<<<< HEAD
# External dependencies.
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/AthenaBaseComps
                          DetectorDescription/GeoPrimitives
                          Event/EventPrimitives
                          Event/xAOD/xAODTracking
                          GaudiKernel
                          MagneticField/MagFieldInterfaces
                          Tracking/TrkEvent/TrkNeutralParameters
                          Tracking/TrkEvent/TrkParameters
                          Tracking/TrkEvent/TrkParticleBase
                          Tracking/TrkEvent/TrkTrack
                          Tracking/TrkEvent/VxSecVertex
                          Tracking/TrkVertexFitter/TrkVKalVrtCore
                          Tracking/TrkVertexFitter/TrkVertexFitterInterfaces
                          PRIVATE
                          Tracking/TrkDetDescr/TrkSurfaces )
                          #Tracking/TrkEvent/VxVertex

# External dependencies:
>>>>>>> release/21.0.127
find_package( CLHEP )

# Component(s) in the package.
atlas_add_library( TrkVKalVrtFitterLib
   TrkVKalVrtFitter/*.h src/*.cxx
   PUBLIC_HEADERS TrkVKalVrtFitter
   PRIVATE_INCLUDE_DIRS ${CLHEP_INCLUDE_DIRS}
   PRIVATE_DEFINITIONS ${CLHEP_DEFINITIONS}
   LINK_LIBRARIES AthenaBaseComps GeoPrimitives EventPrimitives xAODTracking
   GaudiKernel MagFieldInterfaces MagFieldElements MagFieldConditions
   TrkNeutralParameters TrkParameters
   TrkParticleBase VxSecVertex VxVertex TrkExInterfaces TrkVKalVrtCore
   TrkVertexFitterInterfaces
   PRIVATE_LINK_LIBRARIES ${CLHEP_LIBRARIES} TrkSurfaces TestTools )

atlas_add_component( TrkVKalVrtFitter
   src/components/*.cxx
   LINK_LIBRARIES TrkVKalVrtFitterLib )

# Install files from the package.
atlas_install_python_modules( python/*.py POST_BUILD_CMD ${ATLAS_FLAKE8} )
atlas_install_joboptions( share/*.py )

# Test(s) in the package.
atlas_add_test( TrkVKalVrtFitter_test
   SCRIPT athena.py TrkVKalVrtFitter/TrkVKalVrtFitter_test.py
   PROPERTIES TIMEOUT 300
   LOG_IGNORE_PATTERN " INFO |WARNING |found service|Adding private|^ +[+]|HepPDT Version|frontier.c" )
