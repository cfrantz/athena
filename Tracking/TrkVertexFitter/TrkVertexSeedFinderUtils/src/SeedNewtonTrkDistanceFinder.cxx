/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/*********************************************************************
    SeedNewtonTrkDistanceFinder.cxx - Description in header file
*********************************************************************/

//#define SEEDNEWTONTRKDISTANCEFINDER_DEBUG


#include "TrkVertexSeedFinderUtils/SeedNewtonTrkDistanceFinder.h"
#include "TrkEventPrimitives/ParamDefs.h"
#include "TrkParameters/TrackParameters.h"
#include "TrkParticleBase/TrackParticleBase.h"
#include "TrkTrack/Track.h"
#include "TrkVertexSeedFinderUtils/NewtonTrkDistanceFinder.h"
#include "TrkVertexSeedFinderUtils/SeedFinderParamDefs.h"
#include "TrkVertexSeedFinderUtils/Trk2dDistanceSeeder.h"
#include <cmath>


<<<<<<< HEAD
=======
namespace {
#if 0
  inline const Amg::Vector3D operator + ( const Amg::Vector3D & first, const Amg::Vector3D & second) {
    return Amg::Vector3D( first.x()+second.x(),first.y()+second.y(),first.z()+second.z());
  }
#endif
  inline const Amg::Vector3D operator - ( const Amg::Vector3D & first, const Amg::Vector3D & second) {
    return Amg::Vector3D( first.x()-second.x(),first.y()-second.y(),first.z()-second.z());
  }
#if 0
  inline const Amg::Vector3D operator / ( const Amg::Vector3D & first, const double second) {
    return Amg::Vector3D( first.x()/second,first.y()/second,first.z()/second);
  }
#endif
  inline double square(const double tosquare) {
    return std::pow(tosquare,2);
  }
  double dist(std::pair<const Amg::Vector3D,const Amg::Vector3D> pairofpos) {
    Amg::Vector3D diff(pairofpos.first-pairofpos.second);
    return std::sqrt(square(diff.x())+square(diff.y())+square(diff.z()));
  }

}

  
>>>>>>> release/21.0.127
namespace Trk
{
    

  SeedNewtonTrkDistanceFinder::SeedNewtonTrkDistanceFinder(const std::string& t, const std::string& n, const IInterface*  p) : 
    base_class(t,n,p),
    m_2ddistanceseeder("Trk::Trk2dDistanceSeeder"),
    m_distancefinder("Trk::NewtonTrkDistanceFinder"),
    m_numberOfMinimizationFailures(0)
  {   
    declareProperty("Trk2dDistanceSeeder",     m_2ddistanceseeder);
    declareProperty("TrkDistanceFinderImplementation",     m_distancefinder);
  }

  SeedNewtonTrkDistanceFinder::~SeedNewtonTrkDistanceFinder() = default;

  StatusCode SeedNewtonTrkDistanceFinder::initialize() 
  { 

    //initialize number of failures to 0
    m_numberOfMinimizationFailures=0;
    
<<<<<<< HEAD
    ATH_CHECK( AlgTool::initialize() );
    ATH_CHECK( m_2ddistanceseeder.retrieve() );
    ATH_CHECK( m_distancefinder.retrieve() );
    ATH_MSG_DEBUG( "Initialize successful" );
=======
     StatusCode s = AlgTool::initialize();
    if (s.isFailure() )
    {
      msg(MSG::FATAL) << "AlgTool::initialize() initialize failed!" << endmsg;
      return StatusCode::FAILURE;
    }

    s = m_2ddistanceseeder.retrieve();
    if (s.isFailure())
      {
	msg(MSG::FATAL)<<"Could not find 2d distance seeder tool." << endmsg;
	return StatusCode::FAILURE;
      }
    s = m_distancefinder.retrieve();
    if (s.isFailure())
      {
	msg(MSG::FATAL)<<"Could not find newton distance finder implementation tool." << endmsg;
	return StatusCode::FAILURE;
      }
    msg(MSG::INFO) << "Initialize successful" << endmsg;
>>>>>>> release/21.0.127
    return StatusCode::SUCCESS;
  }

  StatusCode SeedNewtonTrkDistanceFinder::finalize() 
  {
    
<<<<<<< HEAD
    ATH_MSG_DEBUG( "Finalize successful. Number of failed minimizations: " << m_numberOfMinimizationFailures << ". Few per events is OK!" );
=======
    msg(MSG::INFO) << "Finalize successful. Number of failed minimizations: " << m_numberOfMinimizationFailures << ". Few per events is OK!" << endmsg;
>>>>>>> release/21.0.127
    return StatusCode::SUCCESS;
  }


  /** method to do the calculation starting from two MeasuredPerigees*/
  /** return value is true if calculation is successfull */
  std::optional<ITrkDistanceFinder::TwoPoints>
  SeedNewtonTrkDistanceFinder::CalculateMinimumDistance(const Trk::Perigee & a,
                                                        const Trk::Perigee & b)  const
  {
    //defragmenting the meory: local variable instead of private data member    
    std::pair<PointOnTrack,PointOnTrack> minpoints;
   
    //try first to get the minimum directly with the Newton method
    try {
<<<<<<< HEAD
      return m_distancefinder->GetClosestPoints(a,b);
    }
    catch (Error::NewtonProblem e) {
=======
//      m_minpoints=m_distancefinder->GetClosestPoints(a,b); 
      minpoints=m_distancefinder->GetClosestPoints(a,b); 
    } catch (Error::NewtonProblem e) {
>>>>>>> release/21.0.127
      if(msgLvl(MSG::DEBUG)) msg(MSG::DEBUG) << "Problem with Newton finder: " << e.p << endmsg;
      try {
        minpoints=m_2ddistanceseeder->GetSeed(TwoTracks(a,b));
<<<<<<< HEAD
        return m_distancefinder->GetClosestPoints(minpoints);
      }
      catch (Error::NewtonProblem e) {
	ATH_MSG_DEBUG( "Problem with Newton finder, even after 2d seeder: no minimum between tracks found" << e.p);
=======
        minpoints=m_distancefinder->GetClosestPoints(minpoints);
 
      } catch (Error::NewtonProblem e) {
	if(msgLvl(MSG::DEBUG)) msg(MSG::DEBUG) << "Problem with Newton finder, even after 2d seeder: no minimum between tracks found" << e.p << endmsg;
>>>>>>> release/21.0.127
        m_numberOfMinimizationFailures+=1;
      } catch (...) {
        m_numberOfMinimizationFailures+=1;
      }
    }
<<<<<<< HEAD

    return std::nullopt;
  }
=======
    
#ifdef SEEDNEWTONTRKDISTANCEFINDER_DEBUG
//    m_log(MSG::DEBUG) << "Returned a_phi " << m_minpoints.first.getPhiPoint() << endmsg;
//    m_log(MSG::DEBUG) << "Returned b_phi " << m_minpoints.second.getPhiPoint() << endmsg;

 if(msgLvl(MSG::DEBUG)) msg(MSG::DEBUG) << "Returned a_phi " << minpoints.first.getPhiPoint() << endmsg;
 if(msgLvl(MSG::DEBUG)) msg(MSG::DEBUG) << "Returned b_phi " << minpoints.second.getPhiPoint() << endmsg;
#endif
>>>>>>> release/21.0.127


  /** method to do the calculation starting from two tracks */
  std::optional<ITrkDistanceFinder::TwoPoints>
  SeedNewtonTrkDistanceFinder::CalculateMinimumDistance(const Trk::Track & a,
                                                        const Trk::Track & b) const
  {
    if (std::isnan(a.perigeeParameters()->parameters()[Trk::d0])||std::isnan(b.perigeeParameters()->parameters()[Trk::d0])) {
<<<<<<< HEAD
      ATH_MSG_ERROR( "Nan parameters in tracks. Cannot use them" );
      return std::nullopt;
=======
      msg(MSG::ERROR) << "Nan parameters in tracks. Cannot use them" << endmsg;
      return false;
>>>>>>> release/21.0.127
    }
    
    return CalculateMinimumDistance(*(a.perigeeParameters()),*(b.perigeeParameters()));
    
  }

  /** method to do the calculation starting from two track particles */
  std::optional<ITrkDistanceFinder::TwoPoints>
  SeedNewtonTrkDistanceFinder::CalculateMinimumDistance(const  Trk::TrackParticleBase & a,
                                                        const Trk::TrackParticleBase & b) const
  {
    const Trk::TrackParameters& para=a.definingParameters();
    const Trk::TrackParameters& parb=b.definingParameters();

    const Trk::Perigee* parpera=dynamic_cast<const Trk::Perigee*>(&para);
    const Trk::Perigee* parperb=dynamic_cast<const Trk::Perigee*>(&parb);

<<<<<<< HEAD
    if (parpera==nullptr||parperb==nullptr) {
      ATH_MSG_WARNING( "Cannot cast to perigee. Neutral will be supported soon" );
      return std::nullopt;
    }

    if (std::isnan(parpera->parameters()[Trk::d0])||std::isnan(parperb->parameters()[Trk::d0])) {
      ATH_MSG_ERROR( "Nan parameters in tracks. Cannot use them" );
      return std::nullopt;
=======
    if (parpera==0||parperb==0) {
      msg(MSG::WARNING) << "Cannot cast to perigee. Neutral will be supported soon" << endmsg;
      return false;
    }

    if (std::isnan(parpera->parameters()[Trk::d0])||std::isnan(parperb->parameters()[Trk::d0])) {
      msg(MSG::ERROR) << "Nan parameters in tracks. Cannot use them" << endmsg;
      return false;
>>>>>>> release/21.0.127
    }
    
    return CalculateMinimumDistance(*(parpera),*(parperb));
    
  }
  
} // namespace Trk
