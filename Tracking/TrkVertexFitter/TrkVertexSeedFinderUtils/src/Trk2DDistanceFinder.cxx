/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/*********************************************************************
    Trk2DDistanceFinder.cxx - Description in header file
*********************************************************************/

//#define SEEDNEWTONTRKDISTANCEFINDER_DEBUG


#include "TrkVertexSeedFinderUtils/Trk2DDistanceFinder.h"
#include "EventPrimitives/EventPrimitives.h"
#include "GeoPrimitives/GeoPrimitives.h"
#include "TrkEventPrimitives/ParamDefs.h"
#include "TrkParameters/TrackParameters.h"
#include "TrkParticleBase/TrackParticleBase.h"
#include "TrkTrack/Track.h"
#include "TrkVertexSeedFinderUtils/SeedFinderParamDefs.h"
#include "TrkVertexSeedFinderUtils/Trk2dDistanceSeeder.h"
#include <cmath>


namespace Trk
{
    

  Trk2DDistanceFinder::Trk2DDistanceFinder(const std::string& t, const std::string& n, const IInterface*  p) : 
    base_class(t,n,p),
    m_2ddistanceseeder("Trk::Trk2dDistanceSeeder"),
    m_numberOfMinimizationFailures(0)
  {   
    declareProperty("Trk2dDistanceSeeder",     m_2ddistanceseeder);
  }

  Trk2DDistanceFinder::~Trk2DDistanceFinder() = default;

  StatusCode Trk2DDistanceFinder::initialize() 
  { 

    //initialize number of failures to 0
    m_numberOfMinimizationFailures=0;
    
<<<<<<< HEAD
    ATH_CHECK( AlgTool::initialize() );
    ATH_CHECK( m_2ddistanceseeder.retrieve() );
    ATH_MSG_DEBUG( "Initialize successful" );
=======
     StatusCode s = AlgTool::initialize();
    if (s.isFailure() )
    {
      msg(MSG::FATAL) << "AlgTool::initialize() initialize failed!" << endmsg;
      return StatusCode::FAILURE;
    }

    s = m_2ddistanceseeder.retrieve();
    if (s.isFailure())
      {
	msg(MSG::FATAL)<<"Could not find 2d distance seeder tool." << endmsg;
	return StatusCode::FAILURE;
      }
    msg(MSG::INFO) << "Initialize successful" << endmsg;
>>>>>>> release/21.0.127
    return StatusCode::SUCCESS;
  }

  StatusCode Trk2DDistanceFinder::finalize() 
  {
<<<<<<< HEAD
    ATH_MSG_DEBUG( "Finalize successful. Number of failed minimizations: " << m_numberOfMinimizationFailures << ". Few per events is OK!" );
=======
    
    msg(MSG::INFO) << "Finalize successful. Number of failed minimizations: " << m_numberOfMinimizationFailures << ". Few per events is OK!" << endmsg;
>>>>>>> release/21.0.127
    return StatusCode::SUCCESS;
  }


  /** method to do the calculation starting from two MeasuredPerigees*/
  /** return value is true if calculation is successfull */
  std::optional<ITrkDistanceFinder::TwoPoints>
  Trk2DDistanceFinder::CalculateMinimumDistance(const Trk::Perigee & a,
                                                const Trk::Perigee & b) const
  {
    std::pair<PointOnTrack,PointOnTrack> minpoints;
    Trk::TwoPoints points;

    try {
      minpoints=m_2ddistanceseeder->GetSeed(TwoTracks(a,b), &points);
    } catch (...) {
<<<<<<< HEAD
      ATH_MSG_WARNING( "Problem with 2d analytic minimum distance finder" );
=======
      if(msgLvl(MSG::WARNING)) msg(MSG::WARNING) << "Problem with 2d analytic minimum distance finder" << endmsg;
>>>>>>> release/21.0.127
      m_numberOfMinimizationFailures+=1;
      return std::nullopt;
    }
    
    
#ifdef SEEDNEWTONTRKDISTANCEFINDER_DEBUG
<<<<<<< HEAD
    ATH_MSG_DEBUG( "Returned a_phi " << minpoints.first.getPhiPoint() );
    ATH_MSG_DEBUG( "Returned b_phi " << minpoints.second.getPhiPoint() );
=======
//    m_log(MSG::DEBUG) << "Returned a_phi " << minpoints.first.getPhiPoint() << endmsg;
//    m_log(MSG::DEBUG) << "Returned b_phi " << minpoints.second.getPhiPoint() << endmsg;
    
    if(msgLvl(MSG::DEBUG)) msg(MSG::DEBUG) << "Returned a_phi " << minpoints.first.getPhiPoint() << endmsg;
    if(msgLvl(MSG::DEBUG)) msg(MSG::DEBUG) << "Returned b_phi " << minpoints.second.getPhiPoint() << endmsg;
>>>>>>> release/21.0.127
#endif
    
    return points;
    
  }
    
  /** method to do the calculation starting from two tracks */
  std::optional<ITrkDistanceFinder::TwoPoints>
  Trk2DDistanceFinder::CalculateMinimumDistance(const  Trk::Track & a,
                                                const Trk::Track & b) const
  {
    if (std::isnan(a.perigeeParameters()->parameters()[Trk::d0])||std::isnan(b.perigeeParameters()->parameters()[Trk::d0])) {
<<<<<<< HEAD
      ATH_MSG_ERROR( "Nan parameters in tracks. Cannot use them" );
      return std::nullopt;
=======
      msg(MSG::ERROR) << "Nan parameters in tracks. Cannot use them" << endmsg;
      return false;
>>>>>>> release/21.0.127
    }
    
    return CalculateMinimumDistance(*(a.perigeeParameters()),*(b.perigeeParameters()));
  }


  /** method to do the calculation starting from two track particles */
  std::optional<ITrkDistanceFinder::TwoPoints>
  Trk2DDistanceFinder::CalculateMinimumDistance(const  Trk::TrackParticleBase & a,
                                                const Trk::TrackParticleBase & b) const
  {
    const Trk::TrackParameters& para=a.definingParameters();
    const Trk::TrackParameters& parb=b.definingParameters();

    const Trk::Perigee* parpera=dynamic_cast<const Trk::Perigee*>(&para);
    const Trk::Perigee* parperb=dynamic_cast<const Trk::Perigee*>(&parb);

<<<<<<< HEAD
    if (parpera==nullptr||parperb==nullptr) {
      ATH_MSG_WARNING("Cannot cast to perigee. Neutral will be supported soon" );
      return std::nullopt;
    }

    if (std::isnan(parpera->parameters()[Trk::d0])||std::isnan(parperb->parameters()[Trk::d0])) {
      ATH_MSG_ERROR( "Nan parameters in tracks. Cannot use them" );
      return std::nullopt;
=======
    if (parpera==0||parperb==0) {
      msg(MSG::WARNING) << "Cannot cast to perigee. Neutral will be supported soon" << endmsg;
      return false;
    }

    if (std::isnan(parpera->parameters()[Trk::d0])||std::isnan(parperb->parameters()[Trk::d0])) {
      msg(MSG::ERROR) << "Nan parameters in tracks. Cannot use them" << endmsg;
      return false;
>>>>>>> release/21.0.127
    }
    
    return CalculateMinimumDistance(*(parpera),*(parperb));
    
  }
  

} // namespace Trk

