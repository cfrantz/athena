<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
# $Id: CMakeLists.txt 769975 2016-08-25 07:41:26Z krasznaa $
################################################################################
# Package: TrkVertexSeedFinderUtils
################################################################################
>>>>>>> release/21.0.127

# Declare the package name:
atlas_subdir( TrkVertexSeedFinderUtils )

<<<<<<< HEAD
# External dependencies:
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Control/AthenaBaseComps
   DetectorDescription/GeoPrimitives
   Event/xAOD/xAODTracking
   GaudiKernel
   Tracking/TrkEvent/TrkParameters
   Tracking/TrkEvent/TrkParticleBase
   Tracking/TrkEvent/VxVertex
   PRIVATE
   Event/EventPrimitives
   Event/xAOD/xAODTruth
   InnerDetector/InDetConditions/InDetBeamSpotService
   InnerDetector/InDetRecTools/InDetTrackSelectionTool
   MagneticField/MagFieldInterfaces
   Tracking/TrkEvent/TrkEventPrimitives
   Tracking/TrkEvent/TrkLinks
   Tracking/TrkEvent/TrkTrack
   Tracking/TrkVertexFitter/TrkVertexFitterInterfaces )

# External dependencies:
find_package( FFTW )
>>>>>>> release/21.0.127
find_package( ROOT COMPONENTS Core Tree Hist )

# Component(s) in the package:
atlas_add_library( TrkVertexSeedFinderUtilsLib
   TrkVertexSeedFinderUtils/*.h
   INTERFACE
   PUBLIC_HEADERS TrkVertexSeedFinderUtils
<<<<<<< HEAD
   LINK_LIBRARIES  GaudiKernel AthenaBaseComps
   TrkVertexFitterInterfaces TrkParameters GeoPrimitives xAODTracking
   MagFieldConditions )

atlas_add_component( TrkVertexSeedFinderUtils
   src/*.h src/*.cxx src/components/*.cxx
   INCLUDE_DIRS  ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES  ${ROOT_LIBRARIES} GaudiKernel
   AthenaBaseComps TrkParticleBase TrkParameters TrkLinks TrkTrack
   TrkVertexFitterInterfaces TrkEventPrimitives xAODTracking xAODTruth
   InDetTrackSelectionToolLib BeamSpotConditionsData EventPrimitives
   GeoPrimitives MagFieldElements TrkVertexSeedFinderUtilsLib )

# Test(s) in the package.
atlas_add_test( NewtonTrkDistanceFinder_test
   SOURCES test/NewtonTrkDistanceFinder_test.cxx
           src/NewtonTrkDistanceFinder.cxx
           src/PointOnTrack.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} TestTools MagFieldConditions
   MagFieldElements CxxUtils GaudiKernel PathResolver SGTools StoreGateLib
   TrkEventPrimitives TrkVertexSeedFinderUtilsLib
   LOG_IGNORE_PATTERN "reading the map|field map" )

atlas_add_test( Trk2dDistanceSeeder_test
   SOURCES test/Trk2dDistanceSeeder_test.cxx
           src/Trk2dDistanceSeeder.cxx
           src/PointOnTrack.cxx
           src/TwoTracks.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} TestTools CxxUtils GaudiKernel PathResolver
   SGTools StoreGateLib MagFieldConditions MagFieldElements TrkParameters
   TrkVertexSeedFinderUtilsLib
   LOG_IGNORE_PATTERN "reading the map|field map" )


atlas_add_test( SeedNewtonTrkDistanceFinder_test
   SOURCES test/SeedNewtonTrkDistanceFinder_test.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} TestTools CxxUtils GaudiKernel PathResolver
   SGTools StoreGateLib MagFieldConditions MagFieldElements
   TrkVertexSeedFinderUtilsLib
   LOG_IGNORE_PATTERN "reading the map|field map" )

atlas_add_test( Trk2DDistanceFinder_test
   SOURCES test/Trk2DDistanceFinder_test.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} TestTools CxxUtils GaudiKernel PathResolver
   SGTools StoreGateLib MagFieldConditions MagFieldElements
   TrkVertexSeedFinderUtilsLib
   LOG_IGNORE_PATTERN "reading the map|field map" )

atlas_add_test( GaussianTrackDensity_test
   SOURCES test/GaussianTrackDensity_test.cxx
           src/GaussianTrackDensity.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} TestTools CxxUtils GaudiKernel TrkTrack
   TrkEventPrimitives TrkVertexSeedFinderUtilsLib )

# Needed to avoid spurious ubsan warnings.
set_target_properties( TrkVertexSeedFinderUtils_GaussianTrackDensity_test PROPERTIES ENABLE_EXPORTS True )

atlas_add_test( Mode3dFromFsmw1dFinder_test
   SOURCES test/Mode3dFromFsmw1dFinder_test.cxx
           src/Mode3dFromFsmw1dFinder.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} TestTools CxxUtils GaudiKernel TrkParameters
   TrkVertexSeedFinderUtilsLib )

# Needed to avoid spurious ubsan warnings.
set_target_properties( TrkVertexSeedFinderUtils_Mode3dFromFsmw1dFinder_test PROPERTIES ENABLE_EXPORTS True )

atlas_add_test( Mode3dTo1dFinder_test
   SOURCES test/Mode3dTo1dFinder_test.cxx
           src/Mode3dTo1dFinder.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} TestTools CxxUtils GaudiKernel TrkParameters
   TrkVertexSeedFinderUtilsLib )

# Install files from the package.
atlas_install_joboptions( share/*.py share/*.txt )
=======
   LINK_LIBRARIES AthenaBaseComps GaudiKernel TrkParameters GeoPrimitives
   VxVertex TrkParticleBase xAODTracking )

atlas_add_component( TrkVertexSeedFinderUtils
   src/*.cxx src/components/*.cxx
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${FFTW_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} ${FFTW_LIBRARIES} EventPrimitives
   xAODTruth InDetTrackSelectionToolLib MagFieldInterfaces TrkEventPrimitives
   TrkLinks TrkTrack TrkVertexFitterInterfaces TrkVertexSeedFinderUtilsLib )
>>>>>>> release/21.0.127
