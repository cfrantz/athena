/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/**
 * @file   MultiComponentStateAssembler.cxx
 * @date   Monday 20th December 2004
 * @author Atkinson,Anthony Morley, Christos Anastopoulos
 *
 * Implementation code for MultiComponentStateAssembler
 */

#include "TrkGaussianSumFilter/MultiComponentStateAssembler.h"
#include "TrkMultiComponentStateOnSurface/ComponentParameters.h"
#include <limits>

<<<<<<< HEAD
namespace {

using namespace Trk::MultiComponentStateAssembler;
=======
Trk::MultiComponentStateAssembler::MultiComponentStateAssembler (const std::string& type, const std::string& name, const IInterface* parent)
  :
  AthAlgTool(type, name, parent),
  m_outputlevel(0),
  m_assemblyDone(false),
  m_minimumFractionalWeight(1.e-9),
  m_minimumValidFraction(0.01),
  m_validWeightSum(0.),
  m_invalidWeightSum(0.),
  m_multiComponentState( new Trk::MultiComponentState() )
{
  
  declareInterface<IMultiComponentStateAssembler>(this);
  
  declareProperty("minimumFractionalWeight", m_minimumFractionalWeight);
  declareProperty("minimumValidFraction",    m_minimumValidFraction);

}

Trk::MultiComponentStateAssembler::~MultiComponentStateAssembler () 
{}

StatusCode Trk::MultiComponentStateAssembler::initialize(){
  
  m_outputlevel = msg().level()-MSG::DEBUG;   // save the threshold for debug printout in private member
  
  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "Initialisation of " << name() << " successful" << endmsg;
  
  return StatusCode::SUCCESS;
}

StatusCode Trk::MultiComponentStateAssembler::finalize(){
 
  delete m_multiComponentState;
 
  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "Finalisation of " << name() << " successful" << endmsg;
  
  return StatusCode::SUCCESS;
  
}
>>>>>>> release/21.0.127

/** @brief Helper for ordering by larger to smaller weight*/
class SortByLargerSimpleComponentWeight
{
<<<<<<< HEAD
public:
  SortByLargerSimpleComponentWeight() = default;
  bool operator()(const Trk::ComponentParameters& firstComponent,
                  const Trk::ComponentParameters& secondComponent) const
  {
    return firstComponent.second > secondComponent.second;
=======

  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "Resetting the MultiComponentStateAssembler: " << name() << endmsg;

  m_assemblyDone = false;

  if ( !m_multiComponentState->empty() ){
    
    Trk::MultiComponentState::const_iterator component = m_multiComponentState->begin();

    for ( ; component != m_multiComponentState->end(); ++component )
      delete component->first;
    
    m_multiComponentState->clear();

>>>>>>> release/21.0.127
  }
};

/** Method to check the validity of of the cached state */
inline bool
isStateValid(const Cache& cache)
{
  return !cache.multiComponentState.empty();
}

/** Method to assemble state with correct weightings */
Trk::MultiComponentState
doStateAssembly(Cache&& cache, const double newWeight)
{
  if (!isStateValid(cache)) {
    return {};
  }
  const size_t cacheSize = cache.multiComponentState.size();
  if (cache.validWeightSum <= 0.) {
    if (!cache.multiComponentState.empty()) {
      const double fixedWeights = 1. / static_cast<double>(cacheSize);
      for (auto& component : cache.multiComponentState) {
        component.second = fixedWeights;
      }
    }
    Trk::MultiComponentState assembledState =
      std::move(cache.multiComponentState);
    // Reset the cache before leaving
    return assembledState;
  }

  Trk::MultiComponentState assembledState =
    std::move(cache.multiComponentState);
  const double scalingFactor =
    cache.validWeightSum > 0. ? newWeight / cache.validWeightSum : 1.;
  for (auto& component : assembledState) {
    component.second *= scalingFactor;
  }
  // Reset the cache before leaving
  return assembledState;
}

<<<<<<< HEAD
bool
prepareStateForAssembly(Cache& cache)
{
  // Protect against empty state
  if (!isStateValid(cache)) {
    return false;
  }

  // Check for minimum fraction of valid states
  double den = cache.validWeightSum + cache.invalidWeightSum;
  double validWeightFraction = den > 0 ? cache.validWeightSum / den : 0;
  if (cache.invalidWeightSum > 0. &&
      validWeightFraction < cache.minimumValidFraction) {
    return false;
  }
  // Check to see assembly has not already been done
  if (cache.assemblyDone) {
    return true;
=======
bool Trk::MultiComponentStateAssembler::addComponent (const ComponentParameters& componentParameters){
  
  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "Adding single component to mixture" << endmsg;

  if ( m_assemblyDone ){
    msg(MSG::WARNING) << "Trying to add state after assembly... returning false" << endmsg;
    return false;
  }
  
  const Trk::ComponentParameters* clonedComponentParameters = new Trk::ComponentParameters( (componentParameters.first)->clone(), componentParameters.second);

  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "Creating multiple component state from single component. Weight of state is: " << componentParameters.second << endmsg;

  Trk::MultiComponentState* singleComponentList = new Trk::MultiComponentState(*clonedComponentParameters);
  this->addComponentsList(singleComponentList);
  delete clonedComponentParameters;
  singleComponentList->clear();
  delete singleComponentList;

  return true;
}

bool Trk::MultiComponentStateAssembler::addMultiState (const MultiComponentState& multiComponentState){
  
  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "Adding multiple component state to mixture" << endmsg;

  if ( m_assemblyDone ){
    msg(MSG::WARNING) << "Trying to add state after assembly... returning false" << endmsg;
    return false;
  }
  
  Trk::MultiComponentState* clonedMultiComponentState = const_cast<Trk::MultiComponentState*>( multiComponentState.clone() );

  this->addComponentsList(clonedMultiComponentState);
  clonedMultiComponentState->clear();
  delete clonedMultiComponentState;
  return true;
}

bool Trk::MultiComponentStateAssembler::addInvalidComponentWeight (const double& invalidComponentWeight){
  
  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "Adding the weight of an invalid state to the mixture" << endmsg;

  m_invalidWeightSum += invalidComponentWeight;

  return true;

}

void Trk::MultiComponentStateAssembler::addComponentsList (const MultiComponentState* multiComponentState){
  
  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "Add multiple component state to exisiting mixture" << endmsg;

  if ( m_assemblyDone ){
    msg(MSG::WARNING) << "Trying to add state after assembly" << endmsg;
    return;
>>>>>>> release/21.0.127
  }
  // Sort Multi-Component State by weights
  std::sort(cache.multiComponentState.begin(),
            cache.multiComponentState.end(),
            SortByLargerSimpleComponentWeight());

<<<<<<< HEAD
  double totalWeight(cache.validWeightSum + cache.invalidWeightSum);
  if (totalWeight != 0.) {
=======
  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "Successfully inserted state" << endmsg;
>>>>>>> release/21.0.127

    // ordered in descending order
    // return the 1st element where (element<value)

<<<<<<< HEAD
    const double minimumWeight =
      std::max(cache.minimumFractionalWeight * totalWeight,
               std::numeric_limits<double>::min());

    const Trk::ComponentParameters dummySmallestWeight(nullptr, minimumWeight);

    auto lower_than = std::upper_bound(cache.multiComponentState.begin(),
                                       cache.multiComponentState.end(),
                                       dummySmallestWeight,
                                       SortByLargerSimpleComponentWeight());

    // reverse iterate , so as to delete removing the last
    auto lower_than_reverse = std::make_reverse_iterator(lower_than);
    for (auto itr = cache.multiComponentState.rbegin();
         itr != lower_than_reverse;
         ++itr) {
      cache.multiComponentState.erase(itr.base() - 1);
    }
=======
bool Trk::MultiComponentStateAssembler::prepareStateForAssembly (){
  
  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "Preparing state for assembly" << endmsg;

  // Protect against empty state
  if ( !isStateValid () ){
    if (m_outputlevel <= 0) 
      msg(MSG::DEBUG) << "State is not valid... returning false" << endmsg;
    return false;
  }
  
  // Check for minimum fraction of valid states
  double validWeightFraction = m_validWeightSum / ( m_validWeightSum + m_invalidWeightSum );

  if (m_invalidWeightSum > 0. && validWeightFraction < m_minimumValidFraction){
    if (m_outputlevel <= 0) 
      msg(MSG::DEBUG) << "Insufficient valid states in the state... returning false" << endmsg;
    return false;
  }

  // Check to see assembly has not already been done
  if ( m_assemblyDone ){
    if (m_outputlevel < 0) 
      msg(MSG::VERBOSE) << "Assembly of state already complete... returning true" << endmsg;
    return true;
>>>>>>> release/21.0.127
  }
  // Now recheck to make sure the state is now still valid
<<<<<<< HEAD
  if (!isStateValid(cache)) {
    return false;
  }
=======
  if ( !isStateValid () ){
    if (m_outputlevel <= 0) 
      msg(MSG::DEBUG) << "After removal of small weights, state is invalid... returning false" << endmsg;
    return false;
  }
  
  // Sort Multi-Component State by weights
  m_multiComponentState->sort( SortByLargerComponentWeight() );
  
  // Set assembly flag
  m_assemblyDone = true;

  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "State is prepared for assembly... returning true" << endmsg;
>>>>>>> release/21.0.127

  // Set assembly flag
  cache.assemblyDone = true;
  return true;
}

<<<<<<< HEAD
} // end anonymous namespace

void
Trk::MultiComponentStateAssembler::reset(Cache& cache)
{
  cache.assemblyDone = false;
  if (!cache.multiComponentState.empty()) {
    cache.multiComponentState.clear();
=======
const Trk::MultiComponentState*
Trk::MultiComponentStateAssembler::assembledState () {
  
  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "Finalising assembly... no specified reweighting" << endmsg;

  if ( !prepareStateForAssembly() ) {
    if (m_outputlevel <= 0) 
      msg(MSG::DEBUG) << "Unable to prepare state for assembly... returning 0" << endmsg;
    return 0;
>>>>>>> release/21.0.127
  }
  cache.validWeightSum = 0.;
  cache.invalidWeightSum = 0.;
}

<<<<<<< HEAD
bool
Trk::MultiComponentStateAssembler::addComponent(
  Cache& cache,
  ComponentParameters&& componentParameters)
{
  if (cache.assemblyDone) {
    return false;
=======
  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "Successful preparation for assembly" << endmsg;
  
  if ( m_invalidWeightSum > 0. || m_validWeightSum <= 0.) {
    if (m_outputlevel < 0) 
      msg(MSG::VERBOSE) << "Assembling state with invalid weight components" << endmsg;
    double totalWeight = m_validWeightSum + m_invalidWeightSum;
    const Trk::MultiComponentState* stateAssembly = doStateAssembly(totalWeight);
    return stateAssembly;
>>>>>>> release/21.0.127
  }
  cache.validWeightSum += componentParameters.second;
  cache.multiComponentState.emplace_back(std::move(componentParameters.first),
                                         componentParameters.second);
  return true;
}

<<<<<<< HEAD
bool
Trk::MultiComponentStateAssembler::addMultiState(
  Cache& cache,
  Trk::MultiComponentState&& multiComponentState)
{
  if (cache.assemblyDone) {
    return false;
=======
const Trk::MultiComponentState*
Trk::MultiComponentStateAssembler::assembledState (const double& newWeight) {
  
  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "Finalising assembly with reweighting of components" << endmsg;

  if ( !prepareStateForAssembly() ) {
    if (m_outputlevel <= 0) 
      msg(MSG::DEBUG) << "Unable to prepare state for assembly... returing 0" << endmsg;
    return 0;
>>>>>>> release/21.0.127
  }
  double sumW(0.);
  for (auto& component : multiComponentState) {
    sumW += component.second;
    cache.multiComponentState.emplace_back(std::move(component.first),
                                           component.second);
  }
  multiComponentState.clear();
  cache.validWeightSum += sumW;
  return true;
}

bool
Trk::MultiComponentStateAssembler::addInvalidComponentWeight(
  Cache& cache,
  const double invalidComponentWeight)
{
  cache.invalidWeightSum += invalidComponentWeight;
  return true;
}

<<<<<<< HEAD
Trk::MultiComponentState
Trk::MultiComponentStateAssembler::assembledState(
  MultiComponentStateAssembler::Cache&& cache)
{
  if (!prepareStateForAssembly(cache)) {
    return {};
=======
const Trk::MultiComponentState*
Trk::MultiComponentStateAssembler::doStateAssembly (const double& newWeight) {
  
  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "Do state assembly" << endmsg;
  
  if ( !isStateValid() ) {
    if (m_outputlevel < 0) 
      msg(MSG::VERBOSE) << "Cached state is empty... returning 0" << endmsg;
    return 0;
>>>>>>> release/21.0.127
  }
  double totalWeight = cache.validWeightSum;
  if (cache.invalidWeightSum > 0. || cache.validWeightSum <= 0.) {
    totalWeight = cache.validWeightSum + cache.invalidWeightSum;
  }

  return doStateAssembly(std::move(cache), totalWeight);
}

<<<<<<< HEAD
Trk::MultiComponentState
Trk::MultiComponentStateAssembler::assembledState(Cache&& cache,
                                                  const double newWeight)
{
  if (!prepareStateForAssembly(cache)) {
    return {};
  }
  return doStateAssembly(std::move(cache), newWeight);
=======
void Trk::MultiComponentStateAssembler::removeSmallWeights () {
  
  if (m_outputlevel < 0) 
    msg(MSG::VERBOSE) << "Removing small weights" << endmsg;

  double totalWeight( m_validWeightSum + m_invalidWeightSum );
  
  if ( totalWeight == 0. ) {
    if (m_outputlevel <= 0) 
      msg(MSG::DEBUG) << "Total weight of state is zero... exiting" << endmsg;
    return;
  }
  
  // Loop over states and remove those with insiginificant weights fractions
  bool continueToRemoveComponents;
  
  do {
    continueToRemoveComponents = false;
    MultiComponentState::iterator component;
    for ( component  = m_multiComponentState->begin() ;
	  component != m_multiComponentState->end()   ;
	  ++component ) {
      if ( (*component).second / totalWeight < m_minimumFractionalWeight ) {
        delete component->first;
        m_multiComponentState->erase(component);
        if (m_outputlevel <= 0) 
          msg(MSG::DEBUG) << "State with weight " << (*component).second << " has been removed from mixture" << endmsg;
        continueToRemoveComponents = true;
        break;
      } // end if
    } // end for
  } while ( continueToRemoveComponents );

>>>>>>> release/21.0.127
}
