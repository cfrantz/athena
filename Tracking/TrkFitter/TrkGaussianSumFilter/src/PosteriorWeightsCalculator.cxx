/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/**
 * @file   PosteriorWeightsCalculator.cxx
 * @date   Wednesday 22nd December 2004
 * @author Tom Atkinson Anthony Morley, Christos Anastopoulos
 *
 * Implementation of PosteriorWeightsCalculator.cxx
 */
#include "TrkGaussianSumFilter/PosteriorWeightsCalculator.h"
#include "TrkEventPrimitives/FitQuality.h"
#include "TrkEventPrimitives/LocalParameters.h"
#include "TrkEventPrimitives/ProjectionMatricesSet.h"
#include "TrkGaussianSumFilter/GsfConstants.h"
#include "TrkParameters/TrackParameters.h"
#include <array>
#include <stdexcept>
namespace {

using namespace Trk;
// expansion and reduction matrices set
const ProjectionMatricesSet reMatrices(5);

template<int DIM>
std::pair<double, double>
calculateWeight_T(const TrackParameters* componentTrackParameters,
                  const AmgSymMatrix(5) * predictedCov,
                  const AmgVector(DIM) & measPar,
                  const AmgSymMatrix(DIM) & measCov,
                  int paramKey)
{
  // Define the expansion matrix
  const AmgMatrix(DIM, 5) H =
    reMatrices.expansionMatrix(paramKey).block<DIM, 5>(0, 0);

  // Calculate the residual
  AmgVector(DIM) r = measPar - H * componentTrackParameters->parameters();
  // Residual covariance. Posterior weights is calculated used predicted state
  // and measurement. Therefore add covariances
  AmgSymMatrix(DIM) R(measCov + H * (*predictedCov) * H.transpose());
  // compute determinant of residual
  const double det = R.determinant();
  if (det == 0) {
    return std::pair<double, double>(0, 0);
  }
  // Compute Chi2
  return std::pair<double, double>(
    det, (1. / (double)DIM) * ((r.transpose() * R.inverse() * r)(0, 0)));
}

std::pair<double, double>
calculateWeight_1D(const TrackParameters* componentTrackParameters,
                   const AmgSymMatrix(5) * predictedCov,
                   const double measPar,
                   const double measCov,
                   int paramKey)
{
  // use measuring coordinate (variable "mk") instead of reduction matrix
  int mk = 0;
  if (paramKey != 1) {
    for (int i = 0; i < 5; ++i) {
      if (paramKey & (1 << i)) {
        mk = i;
        break;
      }
    }
  }

<<<<<<< HEAD
  // Calculate the residual
  const double r = measPar - (componentTrackParameters->parameters())(mk);
  // Residual covariance. Posterior weights is calculated used predicted state
  // and measurement. Therefore add covariances
  const double R = measCov + (*predictedCov)(mk, mk);
  if (R == 0) {
    return std::pair<double, double>(0, 0);
  }
  // Compute Chi2
  return std::pair<double, double>(R, r * r / R);
=======
  msg(MSG::INFO) << "Initialisation of " << name() << " was successful" << endmsg; 

  return StatusCode::SUCCESS;

>>>>>>> release/21.0.127
}

std::pair<double, double>
calculateWeight_2D_3(const TrackParameters* componentTrackParameters,
                     const AmgSymMatrix(5) * predictedCov,
                     const AmgVector(2) & measPar,
                     const AmgSymMatrix(2) & measCov)
{
  // Calculate the residual
  AmgVector(2) r =
    measPar - componentTrackParameters->parameters().block<2, 1>(0, 0);
  // Residual covariance. Posterior weights is calculated used predicted state
  // and measurement. Therefore add covariances
  AmgSymMatrix(2) R(measCov + predictedCov->block<2, 2>(0, 0));
  // compute determinant of residual
  const double det = R.determinant();
  if (det == 0) {
    return std::pair<double, double>(0, 0);
  }
  // Compute Chi2
  return std::pair<double, double>(
    det, 0.5 * ((r.transpose() * R.inverse() * r)(0, 0)));
}

<<<<<<< HEAD
struct componentsCache
{
  struct element
  {
    double determinantR;
    double chi2;
  };
  std::array<element, GSFConstants::maxComponentsAfterConvolution> elements;
  size_t numElements = 0;
};

} // end of anonymous namespace

std::vector<Trk::ComponentParameters>
Trk::PosteriorWeightsCalculator::weights(MultiComponentState&& predictedState,
                                         const MeasurementBase& measurement)
{
=======
  msg(MSG::INFO) << "Finalisation of " << name() << " was successful" << endmsg;
>>>>>>> release/21.0.127

  const size_t predictedStateSize = predictedState.size();
  if (predictedStateSize == 0) {
    return {};
  }
  if (predictedStateSize > GSFConstants::maxComponentsAfterConvolution) {
    throw std::runtime_error(
      "PosteriorWeightsCalculator :Invalid predictedState size");
  }
  const Trk::LocalParameters& measurementLocalParameters =
    measurement.localParameters();
  int nLocCoord = measurement.localParameters().dimension();
  if (nLocCoord < 1 || nLocCoord > 5) {
    return {};
  }

  // Move  to output and update
  std::vector<Trk::ComponentParameters> returnMultiComponentState =
    std::move(predictedState);

<<<<<<< HEAD
=======
const Trk::MultiComponentState*
Trk::PosteriorWeightsCalculator::weights ( const MultiComponentState& predictedState, const MeasurementBase& measurement ){
  
  if (msgLvl(MSG::VERBOSE)) msg() << "Calculating Posterior Weights" << endmsg;
  
  if ( predictedState.empty() ) {
    msg(MSG::WARNING) << "Predicted state is empty... Exiting!" << endmsg;
    return 0;
  }
  
  if (msgLvl(MSG::VERBOSE)) msg() << "State for update is valid!" << endmsg;
    
  Trk::MultiComponentState* returnMultiComponentState = new Trk::MultiComponentState();
  
  std::vector<double> componentDeterminantR;
  std::vector<double> componentChi2;
  
>>>>>>> release/21.0.127
  // Calculate chi2 and determinant of each component.
  componentsCache determinantRandChi2{};
  double minimumChi2(10.e10); // Initalise high
  // Loop over all components
  for (const auto& component : returnMultiComponentState) {

<<<<<<< HEAD
    const Trk::TrackParameters* componentTrackParameters =
      component.first.get();
    if (!componentTrackParameters) {
      continue;
    }
    const AmgSymMatrix(5)* predictedCov =
      componentTrackParameters->covariance();
    if (!predictedCov) {
      continue;
    }

    std::pair<double, double> result(0, 0);
    switch (nLocCoord) {
      case 1: {
        result = calculateWeight_1D(componentTrackParameters,
                                    predictedCov,
                                    measurementLocalParameters(0),
                                    measurement.localCovariance()(0, 0),
                                    measurementLocalParameters.parameterKey());
      } break;
      case 2: {
        if (measurementLocalParameters.parameterKey() == 3) {
          result = calculateWeight_2D_3(
            componentTrackParameters,
            predictedCov,
            measurementLocalParameters.block<2, 1>(0, 0),
            measurement.localCovariance().block<2, 2>(0, 0));
        } else {
          result = calculateWeight_T<2>(
            componentTrackParameters,
            predictedCov,
            measurementLocalParameters.block<2, 1>(0, 0),
            measurement.localCovariance().block<2, 2>(0, 0),
            measurementLocalParameters.parameterKey());
        }
      } break;
      case 3: {
        result =
          calculateWeight_T<3>(componentTrackParameters,
                               predictedCov,
                               measurementLocalParameters.block<3, 1>(0, 0),
                               measurement.localCovariance().block<3, 3>(0, 0),
                               measurementLocalParameters.parameterKey());
      } break;
      case 4: {
        result =
          calculateWeight_T<4>(componentTrackParameters,
                               predictedCov,
                               measurementLocalParameters.block<4, 1>(0, 0),
                               measurement.localCovariance().block<4, 4>(0, 0),
                               measurementLocalParameters.parameterKey());
      } break;
      case 5: {
        result =
          calculateWeight_T<5>(componentTrackParameters,
                               predictedCov,
                               measurementLocalParameters.block<5, 1>(0, 0),
                               measurement.localCovariance().block<5, 5>(0, 0),
                               measurementLocalParameters.parameterKey());
      } break;
      default: {
      }
    }

    if (result.first == 0) {
      continue;
    }
    // Cache R and Chi2
    determinantRandChi2.elements[determinantRandChi2.numElements] = {
      result.first, result.second
    };
    ++determinantRandChi2.numElements;
    if (result.second < minimumChi2) {
      minimumChi2 = result.second;
    }
  } // end loop over components

  // If something went wrong in the loop return empty
  if (determinantRandChi2.numElements != predictedStateSize) {
    return {};
=======
    if ( !componentTrackParameters ) {
      if (msgLvl(MSG::DEBUG)) msg() << "Component in the state prepared for update is invalid... Ignoring!" << endmsg;
      continue;
    }
    
    const AmgSymMatrix(5)* predictedCov = componentTrackParameters->covariance();
    
    if (!predictedCov){
      msg(MSG::WARNING) << "No measurement associated with track parameters... Ignoring!" << endmsg;
      continue;
    }

    if (msgLvl(MSG::VERBOSE)) msg() << "Component for update is valid!" << endmsg;
    
    // Extract the LocalParameters from the MeasurementBase
    const Trk::LocalParameters& measurementLocalParameters = measurement.localParameters();
    
    // Extract predicted state track parameters
    Amg::VectorX trackParameters = componentTrackParameters->parameters();

    // Define the expansion matrix
    const Amg::MatrixX& H = measurementLocalParameters.expansionMatrix();

    // Calculate the residual
    Amg::VectorX r = measurementLocalParameters - H * trackParameters;

    // Extract the CovarianceMatrix of the MeasurementBase
    const Amg::MatrixX& measurementCovariance   = measurement.localCovariance();

    // Residual covariance. Posterior weights is calculated used predicted state and measurement. Therefore add covariances
    Amg::MatrixX R ( measurementCovariance + H * (*predictedCov) * H.transpose() );

    //compute determinant of residual
    double determinantR = R.determinant();

    if (determinantR==0){
      msg(MSG::WARNING) << "Determinant is 0, cannot invert matrix... Ignoring component" << endmsg;
      continue;
    }
    // Compute Chi2
    double size = measurementCovariance.innerSize();

    double chi2 = (1./size)*((r.transpose() * R.inverse() * r)(0,0));
 
    if (msgLvl(MSG::VERBOSE)) msg() << "determinant R / chiSquared: " << determinantR << '\t' << chi2 << endmsg;

    componentDeterminantR.push_back(determinantR);
    componentChi2.push_back(chi2);
    
    if ( chi2 < minimumChi2 )
      minimumChi2 = chi2;
    
  } // end loop over components
  
  if ( componentDeterminantR.size() != predictedState.size() || componentChi2.size() != predictedState.size() ){
    msg(MSG::WARNING) << "Inconsistent number of components in chi2 and detR vectors... Exiting!" << endmsg;
    return 0;
>>>>>>> release/21.0.127
  }

  // Calculate posterior weights.
  size_t index(0);
  double sumWeights(0.);
  std::array<double, GSFConstants::maxComponentsAfterConvolution>
    fallBackWeights;
  auto componentItr = returnMultiComponentState.begin();
  for (; componentItr != returnMultiComponentState.end();
       ++componentItr, ++index) {
    // Extract common factor to avoid numerical problems during exponentiation
    double chi2 = determinantRandChi2.elements[index].chi2 - minimumChi2;
    const double priorWeight = componentItr->second;
    fallBackWeights[index] = priorWeight;
    double updatedWeight(0.);
    // Determinant can not be below 1e-19. Rather ugly but protect
    // against 0 determinants Normally occur when the component is a poor fit
    if (determinantRandChi2.elements[index].determinantR > 1e-20) {
      updatedWeight =
        priorWeight *
        sqrt(1. / determinantRandChi2.elements[index].determinantR) *
        exp(-0.5 * chi2);
    } else {
      updatedWeight = 1e-10;
    }
    componentItr->second = updatedWeight;
    sumWeights += updatedWeight;
  }
<<<<<<< HEAD
  if (sumWeights > 0.) {
    double invertSumWeights = 1. / sumWeights;
    // Renormalise the state to total weight = 1
    for (auto& returnComponent : returnMultiComponentState) {
      returnComponent.second *= invertSumWeights;
=======
  
  if ( returnMultiComponentState->size() != predictedState.size() ){
    msg(MSG::WARNING) << "Inconsistent number of components between initial and final states... Exiting!" << endmsg;
    return 0;
  }
  
  // Renormalise the state to total weight = 1
  Trk::MultiComponentState::iterator returnComponent = returnMultiComponentState->begin();
  component = predictedState.begin();

  for ( ; returnComponent != returnMultiComponentState->end() ; ++returnComponent, ++component ){ 
    if (sumWeights > 0. ){
      (*returnComponent).second /= sumWeights;
    } else {
      (*returnComponent).second  = component->second;
>>>>>>> release/21.0.127
    }
  } else {
    // If the sum weights is less than 0 revert them back
    size_t fallbackIndex(0);
    for (auto& returnComponent : returnMultiComponentState) {
      returnComponent.second = fallBackWeights[fallbackIndex];
      ++fallbackIndex;
    }
  }
  return returnMultiComponentState;
}
