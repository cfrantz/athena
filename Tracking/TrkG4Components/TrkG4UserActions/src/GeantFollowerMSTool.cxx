/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "TrkG4UserActions/GeantFollowerMSTool.h"
#include "TrkG4UserActions/IGeantFollowerMSHelper.h"

namespace G4UA
{

  GeantFollowerMSTool::GeantFollowerMSTool(const std::string& type,
                                           const std::string& name,
                                           const IInterface* parent)
    : UserActionToolBase<GeantFollowerMS>(type, name, parent)
  {
    declareProperty("HelperTool", m_config.helper);
  }

  std::unique_ptr<GeantFollowerMS>
  GeantFollowerMSTool::makeAndFillAction(G4AtlasUserActions& actionList)
  {
    ATH_MSG_DEBUG("Constructing a GeantFollowerMS action");
    auto action = std::make_unique<GeantFollowerMS>(m_config);
    actionList.runActions.push_back( action.get() );
    actionList.eventActions.push_back( action.get() );
    actionList.steppingActions.push_back( action.get() );
    return action;
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "CxxUtils/make_unique.h"
#include "TrkG4UserActions/GeantFollowerMSTool.h"
#include "TrkG4UserActions/IGeantFollowerMSHelper.h"

namespace G4UA{


  GeantFollowerMSTool::GeantFollowerMSTool(const std::string& type, const std::string& name,const IInterface* parent):
    ActionToolBase<GeantFollowerMS>(type, name, parent), m_config(){
    declareProperty("HelperTool",m_config.helper);
  }

  std::unique_ptr<GeantFollowerMS>  GeantFollowerMSTool::makeAction(){
    ATH_MSG_DEBUG("makeAction");
    auto action = CxxUtils::make_unique<GeantFollowerMS>(m_config);
    return std::move(action);
  }

  StatusCode GeantFollowerMSTool::queryInterface(const InterfaceID& riid, void** ppvIf){

    if(riid == IG4EventActionTool::interfaceID()) {
      *ppvIf = (IG4EventActionTool*) this;
      addRef();
      return StatusCode::SUCCESS;
    }
    if(riid == IG4RunActionTool::interfaceID()) {
      *ppvIf = (IG4RunActionTool*) this;
      addRef();
      return StatusCode::SUCCESS;
    }
    if(riid == IG4SteppingActionTool::interfaceID()) {
      *ppvIf = (IG4SteppingActionTool*) this;
      addRef();
      return StatusCode::SUCCESS;
    }
    return ActionToolBase<GeantFollowerMS>::queryInterface(riid, ppvIf);
>>>>>>> release/21.0.127
  }

} // namespace G4UA
