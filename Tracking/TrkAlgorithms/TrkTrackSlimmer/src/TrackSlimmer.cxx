/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// TrackSlimmer.cxx, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#include "TrkTrackSlimmer/TrackSlimmer.h"
#include "TrkToolInterfaces/ITrackSlimmingTool.h"
#include "AthContainers/ConstDataVector.h"

//================ Constructor =================================================

Trk::TrackSlimmer::TrackSlimmer(const std::string& name, ISvcLocator* pSvcLocator) :
    AthAlgorithm(name,pSvcLocator),
    m_slimTool("Trk::TrkTrackSlimmingTool/TrkTrackSlimmingTool"),
    m_ptCut(0.0),
    m_trackLocation{"ConvertedMooreTracks"},
    m_slimmedTracks{"SlimmedMooreTracks"},
    m_numSlimmedTracks(0),
    m_numOriginalTracks(0)
{
    //  template for property decalration
    declareProperty("TrackSlimmingTool", m_slimTool);
    declareProperty("TrackLocation", m_trackLocation);
    declareProperty("SlimmedTrackLocation", m_slimmedTracks);
    declareProperty("PtCut", m_ptCut);
    declareProperty("OnlySetPersistificationHints", m_setPersistificationHints, "Only set persistification hints in each track state on surface");
}

//================ Destructor =================================================

Trk::TrackSlimmer::~TrackSlimmer()
{}


//================ Initialisation =================================================

StatusCode Trk::TrackSlimmer::initialize()
{
    // Code entered here will be executed once at program start.

    ATH_MSG_INFO(name() << " initialize()" );

    if ( m_slimTool.retrieve().isFailure() ) {
        ATH_MSG_ERROR("Failed to retrieve TrkTrackSlimmingTool tool "<< m_slimTool );
        return StatusCode::FAILURE;
    } else {
        ATH_MSG_INFO("Retrieved tool " << m_slimTool );
    }
    ATH_CHECK(m_trackLocation.initialize());
<<<<<<< HEAD
    ATH_CHECK(m_slimmedTracks.initialize(m_ptCut>0. || !m_setPersistificationHints));

    ATH_MSG_INFO("initialize() successful in " << name() );
    bool error = (m_ptCut>0. || !m_setPersistificationHints) &&  m_trackLocation.size() != m_slimmedTracks.size();
=======
    ATH_CHECK(m_slimmedTracks.initialize());

    ATH_MSG_INFO("initialize() successful in " << name() );
    bool error = m_trackLocation.size() != m_slimmedTracks.size();
>>>>>>> release/21.0.127
    if(error){
       ATH_MSG_ERROR("Inconsistent number of inputs and output in " << name() );
       ATH_MSG_ERROR("Inputs " << m_trackLocation.size() << " outputs " << m_slimmedTracks.size() );
    }
    if(msgLvl(MSG::DEBUG) || error){
       auto level = error  ? MSG::ERROR : MSG::DEBUG;
       for(auto &key : m_trackLocation) msg(level) << "TrackLocation " << key.key() << endmsg;
       for(auto &key : m_slimmedTracks) msg(level) << "SlimmedTrackLocation " << key.key() << endmsg;
    }
    if(error) return StatusCode::FAILURE;
    
    return StatusCode::SUCCESS;
}

//================ Finalisation =================================================

StatusCode Trk::TrackSlimmer::finalize()
{
    ATH_MSG_INFO("Produced "<< m_numSlimmedTracks<<" slimmed tracks from "<<m_numOriginalTracks<<" original tracks");
    // Code entered here will be executed once at the end of the program run.
    return StatusCode::SUCCESS;
}

//================ Execution ====================================================

StatusCode Trk::TrackSlimmer::execute()
{
    // Code entered here will be executed once per event
    int numTracksFailingPtCut=0;


    using namespace std;
    auto InputHandles = m_trackLocation.makeHandles();
<<<<<<< HEAD
    std::vector<SG::WriteHandle<TrackCollection> > OutputHandles = ( (m_ptCut>0. || !m_setPersistificationHints)
                                                                     ? m_slimmedTracks.makeHandles()
                                                                     : std::vector<SG::WriteHandle<TrackCollection> >(m_trackLocation.size()));
    if (OutputHandles.size() != InputHandles.size()) {
      ATH_MSG_ERROR("Failed to create output collection with correct size." );
      return StatusCode::FAILURE;
    }
=======
    auto OutputHandles = m_slimmedTracks.makeHandles();
>>>>>>> release/21.0.127
    auto InputIt  = InputHandles.begin();
    auto InputE   = InputHandles.end();
    auto OutputIt = OutputHandles.begin();

    for(; InputIt!= InputE ; ++InputIt, ++OutputIt){
      auto &trackLocation = *InputIt;
      auto &slimmedTracksHandle = *OutputIt;
      if (trackLocation.isValid())
        {
          //create container for slimmed tracks
<<<<<<< HEAD
          std::unique_ptr<ConstDataVector<TrackCollection> >slimmedTracks;
          if ( m_setPersistificationHints) {
            if (m_ptCut>0) {
              slimmedTracks = std::make_unique<ConstDataVector<TrackCollection> >(SG::VIEW_ELEMENTS);
            }
            // No output container if m_setPersistificationHints && m_ptCut<=0.
          }
          else {
            slimmedTracks = std::make_unique<ConstDataVector<TrackCollection> >();
          }
=======
          std::unique_ptr<TrackCollection>slimmedTracks( m_setPersistificationHints
                                                            ? (m_ptCut>0 ? new TrackCollection(SG::VIEW_ELEMENTS) : nullptr)
                                                            : new TrackCollection);
>>>>>>> release/21.0.127

          //loop through tracks, slimming them as you go.
          TrackCollection::const_iterator it    = trackLocation->begin();
          TrackCollection::const_iterator itEnd = trackLocation->end();
          for (; it!=itEnd; it++)
            { 
	      const DataVector< const Trk::TrackParameters > * params = (**it).trackParameters();
	      if ( params!=0 && params->size()>0 && params->front()->pT()>m_ptCut )
                {
		  Track* slimmed = m_slimTool->slim(**it);
                  if (m_setPersistificationHints) {
                    if (slimmed) {
                      ATH_MSG_FATAL("Track slimmer is configured to expect the slimming tool to only set persistification hints"
                                    " rather than creating slimmed tracks. But the tool created a track.");
                      delete slimmed;
                    }
                    if (slimmedTracks) {
                      slimmedTracks->push_back(*it);
                    }
                  }
                  else {
                    if (!slimmed) {
                      ATH_MSG_ERROR("Track slimmer is configured to expect the slimming tool to create slimmed tracks. "
                                    " But the tool returned a nullptr for the one track.");
                    }
<<<<<<< HEAD
                    else if (slimmedTracks) {
                      slimmedTracks->push_back(slimmed);
                    }
                  }
                  //m_log<<MSG::INFO<<"Original Track"<<(**it)<<endmsg;
                  //m_log<<MSG::INFO<<"Slimmed Track"<<(*slimmed)<<endmsg;
=======
                    else {
                      slimmedTracks->push_back(slimmed);
                    }
                  }
                  //m_log<<MSG::INFO<<"Original Track"<<(**it)<<endreq;
                  //m_log<<MSG::INFO<<"Slimmed Track"<<(*slimmed)<<endreq;
>>>>>>> release/21.0.127
                } else {
                if (params!=0 && params->size()>0) numTracksFailingPtCut++;
              }
            }

          if (slimmedTracks) {
            // info for stats in finalise
            m_numSlimmedTracks+=slimmedTracks->size();
            m_numOriginalTracks+=trackLocation->size();
<<<<<<< HEAD
            slimmedTracksHandle.put (std::move (slimmedTracks));
=======
            slimmedTracksHandle=std::move(slimmedTracks);
>>>>>>> release/21.0.127

            ATH_MSG_VERBOSE( "Saved "<<slimmedTracksHandle->size()<<" slimmed tracks (to "<<slimmedTracksHandle.name()
                             <<"), from input ("<<trackLocation.name()<<") of "<<trackLocation->size()<<" original tracks (which makes "
                             << m_numSlimmedTracks<<"/"<<m_numOriginalTracks<<" so far). "
                             << numTracksFailingPtCut<<" tracks failed pT cut of "<<m_ptCut );

          }
        }
    }

    return StatusCode::SUCCESS;
}

//============================================================================================
