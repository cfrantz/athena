/*
<<<<<<< HEAD
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

// Dear emacs, this is -*-c++-*-

// Andrei.Gaponenko@cern.ch, 2008
// Olivier.Arnaez@cern.ch, 2015


#ifndef DETAILEDTRACKTRUTH_P3_H
#define DETAILEDTRACKTRUTH_P3_H

#include "TrkTruthTPCnv/SubDetHitStatistics_p0.h"
#include "TrkTruthTPCnv/TruthTrajectory_p2.h"

<<<<<<< HEAD
#include "AthenaKernel/CLASS_DEF.h"
=======
#include "CLIDSvc/CLASS_DEF.h"
>>>>>>> release/21.0.127

namespace Trk {

  class DetailedTrackTruth_p3 {
  public:
    SubDetHitStatistics_p0 m_hitsCommon;
    SubDetHitStatistics_p0 m_hitsTrack;
<<<<<<< HEAD
    SubDetHitStatistics_p0 m_hitsTruth;
=======
    SubDetHitStatistics_p0 m_hitsTruth; //! This is a new member in p3
>>>>>>> release/21.0.127
    TruthTrajectory_p2 m_trajectory;
  };
}

CLASS_DEF( Trk::DetailedTrackTruth_p3 , 104129964 , 1 )

#endif/*DETAILEDTRACKTRUTH_P3_H*/
