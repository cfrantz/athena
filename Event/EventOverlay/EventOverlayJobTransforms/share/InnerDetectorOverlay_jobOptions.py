include.block ( "EventOverlayJobTransforms/InnerDetectorOverlay_jobOptions.py" )

from Digitization.DigitizationFlags import digitizationFlags
from AthenaCommon.DetFlags import DetFlags
from AthenaCommon import CfgGetter
from OverlayCommonAlgs.OverlayFlags import overlayFlags


if DetFlags.overlay.pixel_on() or DetFlags.overlay.SCT_on() or DetFlags.overlay.TRT_on():

    digitizationFlags.doInDetNoise=False # FIXME THIS SHOULD BE SET EARLIER IN THE CONFIGURATION

    #if overlayFlags.isDataOverlay():
    #   include( "InDetCosmicRecExample/InDetCosmicFlags_jobOptions.py" )

    if DetFlags.overlay.pixel_on():
        job += CfgGetter.getAlgorithm("PixelOverlayDigitization")
        job += CfgGetter.getAlgorithm("PixelOverlay")
        if DetFlags.overlay.Truth_on():
            job += CfgGetter.getAlgorithm("PixelSDOOverlay")

<<<<<<< HEAD
        if overlayFlags.isDataOverlay():
            if overlayFlags.isOverlayMT():
                job.InDetPixelRawDataProvider.RDOKey = overlayFlags.bkgPrefix()+"PixelRDOs"
            else:
                job.InDetPixelRawDataProvider.RDOKey = overlayFlags.dataStore()+"+PixelRDOs"

            from RegionSelector.RegSelToolConfig import makeRegSelTool_Pixel
            job.InDetPixelRawDataProvider.RegSelTool = makeRegSelTool_Pixel()

            #ServiceMgr.ByteStreamAddressProviderSvc.TypeNames += [ "PixelRDO_Container/PixelRDOs" ]
            #ServiceMgr.ByteStreamAddressProviderSvc.TypeNames += [ "Trk::PixelClusterContainer/PixelOnlineClusters" ]
        else:
            if not conddb.folderRequested('PIXEL/PixReco'):
                conddb.addFolder('PIXEL_OFL','/PIXEL/PixReco')
=======
        indetovl.do_Pixel = True
        if readBS and isRealData:
           job.InDetPixelRawDataProvider.EvtStore = "OriginalEvent_SG"
           #ServiceMgr.ByteStreamAddressProviderSvc.TypeNames += [ "PixelRDO_Container/PixelRDOs" ]
           #ServiceMgr.ByteStreamAddressProviderSvc.TypeNames += [ "Trk::PixelClusterContainer/PixelOnlineClusters" ]
        else:
            if not conddb.folderRequested('PIXEL/PixReco'):
                conddb.addFolder('PIXEL_OFL','/PIXEL/PixReco')
    else:
        indetovl.do_Pixel = False
>>>>>>> release/21.0.127

    if DetFlags.overlay.SCT_on():

        # Setup the ReadCalibChip folders and Svc
        if overlayFlags.isDataOverlay():
            #conddb.blockFolder("/SCT/DAQ/Calibration/ChipGain")
            #conddb.blockFolder("/SCT/DAQ/Calibration/ChipNoise")
            #conddb.addFolder("SCT_OFL","/SCT/DAQ/Calibration/ChipGain",forceMC=True)
            #conddb.addFolder("SCT_OFL","/SCT/DAQ/Calibration/ChipNoise",forceMC=True)
            conddb.addFolder("SCT_OFL","/SCT/DAQ/Calibration/ChipGain<tag>SctDaqCalibrationChipGain-Apr10-01</tag>",forceMC=True, className="CondAttrListCollection")
            conddb.addFolder("SCT_OFL","/SCT/DAQ/Calibration/ChipNoise<tag>SctDaqCalibrationChipNoise-Apr10-01</tag>",forceMC=True, className="CondAttrListCollection")

            #if not conddb.folderRequested('/SCT/DAQ/Calibration/ChipGain'):
            #   conddb.addFolderSplitOnline("SCT","/SCT/DAQ/Calibration/ChipGain","/SCT/DAQ/Calibration/ChipGain",forceMC=True)
            #if not conddb.folderRequested('/SCT/DAQ/Calibration/ChipNoise'):
            #   conddb.addFolderSplitOnline("SCT","/SCT/DAQ/Calibration/ChipNoise","/SCT/DAQ/Calibration/ChipNoise",forceMC=True)

        job += CfgGetter.getAlgorithm("SCT_OverlayDigitization")
<<<<<<< HEAD
        job += CfgGetter.getAlgorithm("SCTOverlay")
        if DetFlags.overlay.Truth_on():
            job += CfgGetter.getAlgorithm("SCTSDOOverlay")

        if overlayFlags.isDataOverlay():
            include("InDetRecExample/InDetRecConditionsAccess.py")

            if overlayFlags.isOverlayMT():
                job.InDetSCTRawDataProvider.RDOKey = overlayFlags.bkgPrefix()+"SCT_RDOs"
                job.InDetSCTRawDataProvider.LVL1IDKey = overlayFlags.bkgPrefix()+"SCT_LVL1ID"
                job.InDetSCTRawDataProvider.BCIDKey = overlayFlags.bkgPrefix()+"SCT_BCID"
            else:
                job.InDetSCTRawDataProvider.RDOKey = overlayFlags.dataStore()+"+SCT_RDOs"
                job.InDetSCTRawDataProvider.LVL1IDKey = overlayFlags.dataStore()+"+SCT_LVL1ID"
                job.InDetSCTRawDataProvider.BCIDKey = overlayFlags.dataStore()+"+SCT_BCID"
            #ServiceMgr.ByteStreamAddressProviderSvc.TypeNames += [ "SCT_RDO_Container/SCT_RDOs" ]
            #ServiceMgr.ByteStreamAddressProviderSvc.TypeNames += [ "Trk::SCT_ClusterContainer/SCT_OnlineClusters" ]
=======
        CfgGetter.getPublicTool("SCT_DigitizationTool").InputObjectName="SCT_Hits"
        indetovl.do_SCT = True
        if readBS and isRealData:
           #job.InDetSCTRawDataProvider.EvtStore = "OriginalEvent_SG"
           job.InDetSCTRawDataProvider.RDOKey = "OriginalEvent_SG/SCT_RDOs"
           job.InDetSCTRawDataProvider.LVL1IDKey = "OriginalEvent_SG/SCT_LVL1ID"
           job.InDetSCTRawDataProvider.BCIDKey = "OriginalEvent_SG/SCT_BCID"
           #ServiceMgr.ByteStreamAddressProviderSvc.TypeNames += [ "SCT_RDO_Container/SCT_RDOs" ]
           #ServiceMgr.ByteStreamAddressProviderSvc.TypeNames += [ "Trk::SCT_ClusterContainer/SCT_OnlineClusters" ]
    else:
        indetovl.do_SCT = False
>>>>>>> release/21.0.127

    if DetFlags.overlay.TRT_on():
        if overlayFlags.isDataOverlay():
            conddb.blockFolder("/TRT/Cond/DigVers")
            #conddb.addFolderWithTag("TRT_OFL","/TRT/Cond/DigVers","TRTCondDigVers-Collisions-01",force=True,forceMC=True)
            conddb.addFolder("TRT_OFL","/TRT/Cond/DigVers",forceMC=True,
                             className = 'AthenaAttributeList')

        from TRT_ElectronPidTools.TRT_ElectronPidToolsConf import InDet__TRT_LocalOccupancy
        TRT_LocalOccupancy = InDet__TRT_LocalOccupancy(name="TRT_LocalOccupancy", isTrigger= False,
                                                       TRT_RDOContainerName="",
                                                       TRT_DriftCircleCollection="")

        job += CfgGetter.getAlgorithm("TRT_OverlayDigitization")
<<<<<<< HEAD
        job += CfgGetter.getAlgorithm("TRTOverlay")
        if DetFlags.overlay.Truth_on():
            job += CfgGetter.getAlgorithm("TRTSDOOverlay")

        job.TRTOverlay.TRT_LocalOccupancyTool  = TRT_LocalOccupancy

        from InDetRecExample.InDetJobProperties import InDetFlags
        include("InDetRecExample/InDetRecConditionsAccess.py")
        
        if overlayFlags.isDataOverlay():
            if overlayFlags.isOverlayMT():
                job.InDetTRTRawDataProvider.RDOKey = overlayFlags.bkgPrefix()+"TRT_RDOs"
            else:
                job.InDetTRTRawDataProvider.RDOKey = overlayFlags.dataStore()+"+TRT_RDOs"
            #ServiceMgr.ByteStreamAddressProviderSvc.TypeNames += [ "TRT_RDO_Container/TRT_RDOs" ]

            from RegionSelector.RegSelToolConfig import makeRegSelTool_TRT
            InDetTRTRawDataProvider.RegSelTool = makeRegSelTool_TRT()

            #from IOVDbSvc.CondDB import conddb
            #conddb.addFolder("TRT","/TRT/Calib/T0","<tag>TrtCalibt0-UPD2-FDR2-01</tag>")
            #conddb.addFolder("TRT","/TRT/Calib/RT","<tag>TrtCalibRt-UPD2-FDR2-01</tag>")
            #conddb.addFolder("TRT","/TRT/Calib/T0","<tag>TrtCalibRt-HLT-UPD1-01</tag>")
            #conddb.addFolder("TRT","/TRT/Calib/RT","<tag>TrtCalibT0-HLT-UPD1-01</tag>")
            conddb.addFolder("TRT_ONL","/TRT/Onl/ROD/Compress",className='CondAttrListCollection')
=======
  
        indetovl.do_TRT = True
        if readBS and isRealData:
           job.InDetTRTRawDataProvider.EvtStore = "OriginalEvent_SG"
           #ServiceMgr.ByteStreamAddressProviderSvc.TypeNames += [ "TRT_RDO_Container/TRT_RDOs" ]
  
           from TRT_ConditionsServices.TRT_ConditionsServicesConf import TRT_CalDbSvc
           InDetTRTCalDbSvc = TRT_CalDbSvc()
           ServiceMgr += InDetTRTCalDbSvc
          #from IOVDbSvc.CondDB import conddb
#           conddb.addFolder("TRT","/TRT/Calib/T0","<tag>TrtCalibt0-UPD2-FDR2-01</tag>")
#           conddb.addFolder("TRT","/TRT/Calib/RT","<tag>TrtCalibRt-UPD2-FDR2-01</tag>")
      #     conddb.addFolder("TRT","/TRT/Calib/T0","<tag>TrtCalibRt-HLT-UPD1-01</tag>")
       #    conddb.addFolder("TRT","/TRT/Calib/RT","<tag>TrtCalibT0-HLT-UPD1-01</tag>")
           conddb.addFolder("TRT_ONL","/TRT/Onl/ROD/Compress")
    else:
        indetovl.do_TRT = False

    if overlayFlags.doSignal==True:
       include ("EventOverlayJobTransforms/InDetMcSignal_jobOptions.py")

    job += indetovl
    
    if DetFlags.overlay.Truth_on():
        from InDetOverlay.InDetOverlayConf import InDetSDOOverlay
        sdoovl = InDetSDOOverlay()
        sdoovl.do_Pixel = DetFlags.overlay.pixel_on()
        sdoovl.do_Pixel_background = not isRealData
        sdoovl.do_SCT = DetFlags.overlay.SCT_on()
        sdoovl.do_SCT_background = not isRealData
        sdoovl.do_TRT = DetFlags.overlay.TRT_on()
        sdoovl.do_TRT_background = not isRealData

        job += sdoovl
>>>>>>> release/21.0.127
