/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#ifndef XAODCOREATHENAPOOL_XAODSHALLOWAUXCONTAINERCNV_H
#define XAODCOREATHENAPOOL_XAODSHALLOWAUXCONTAINERCNV_H

// Gaudi/Athena include(s):
#include "AthenaPoolCnvSvc/T_AthenaPoolCustomCnv.h"

// EDM include(s):
<<<<<<< HEAD
#include "xAODCore/ShallowAuxContainer.h"

/// Base class for the converter
typedef T_AthenaPoolCustomCnvWithKey< xAOD::ShallowAuxContainer,
                                      xAOD::ShallowAuxContainer >
=======
#define private public
#include "xAODCore/ShallowAuxContainer.h"

/// Base class for the converter
typedef T_AthenaPoolCustomCnv< xAOD::ShallowAuxContainer,
                               xAOD::ShallowAuxContainer >
>>>>>>> release/21.0.127
   xAODShallowAuxContainerCnvBase;

/**
 *  @short POOL converter for the xAOD::ShallowAuxContainer class
 *
 *         This is the converter doing the actual schema evolution
 *         of the package... 
 *
 * @author Will Buttinger <will@cern.ch>
 *
 */
class xAODShallowAuxContainerCnv :
   public xAODShallowAuxContainerCnvBase {

<<<<<<< HEAD
public:
   /// Converter constructor
   xAODShallowAuxContainerCnv( ISvcLocator* svcLoc );

protected:
   /// Function preparing the container to be written out
   virtual
   xAOD::ShallowAuxContainer* createPersistentWithKey( xAOD::ShallowAuxContainer* trans,
                                                       const std::string& key) override;
   /// Function reading in the object from the input file
   virtual
   xAOD::ShallowAuxContainer* createTransientWithKey (const std::string& key) override;
=======
   // Declare the factory as our friend:
   friend class CnvFactory< xAODShallowAuxContainerCnv >;

protected:
   /// Converter constructor
   xAODShallowAuxContainerCnv( ISvcLocator* svcLoc );

   /// Function preparing the container to be written out
   virtual xAOD::ShallowAuxContainer* createPersistent( xAOD::ShallowAuxContainer* trans );
   /// Function reading in the object from the input file
   virtual xAOD::ShallowAuxContainer* createTransient();
>>>>>>> release/21.0.127


}; // class xAODShallowAuxContainerCnv

#endif // XAODCOREATHENAPOOL_XAODSHALLOWAUXCONTAINERCNV_H

