/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

<<<<<<< HEAD
=======
// $Id: xAODAuxContainerBaseCnv.cxx 660871 2015-04-16 08:20:17Z krasznaa $

// System include(s):
#include <stdexcept>

// Gaudi/Athena include(s):
#include "AthenaKernel/IThinningSvc.h"
#include "AthContainers/tools/copyThinned.h"

>>>>>>> release/21.0.127
// Local include(s):
#include "xAODAuxContainerBaseCnv.h"

xAOD::AuxContainerBase*
<<<<<<< HEAD
xAODAuxContainerBaseCnv::createPersistentWithKey( xAOD::AuxContainerBase* trans,
                                                  const std::string& key )
{
   xAOD::AuxContainerBase* result =
     xAODAuxContainerBaseCnvBase::createPersistentWithKey( trans, key );

=======
xAODAuxContainerBaseCnv::createPersistent( xAOD::AuxContainerBase* trans ) {

   // Access the thinning svc, if it is defined for the current object:
   IThinningSvc* thinSvc = IThinningSvc::instance();

   // Create the persistent object using the helper function from AthContainers:
   xAOD::AuxContainerBase* result = SG::copyThinned( *trans, thinSvc );
   // Make sure that the variable selection gets copied:
   result->m_selection = trans->m_selection;
>>>>>>> release/21.0.127
   // Return the new object:
   return result;
}

