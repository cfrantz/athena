// Dear emacs, this is -*- c++ -*-
/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/
<<<<<<< HEAD
=======

// $Id: xAODEgammaDict.h 789134 2016-12-11 02:15:53Z christos $
>>>>>>> release/21.0.127
#ifndef XAODEGAMMA_XAODEGAMMADICT_H
#define XAODEGAMMA_XAODEGAMMADICT_H

// Local include(s):
#include "xAODEgamma/Egamma.h"
#include "xAODEgamma/EgammaContainer.h"
#include "xAODEgamma/EgammaAuxContainer.h"
#include "xAODEgamma/versions/Egamma_v1.h"
#include "xAODEgamma/versions/EgammaContainer_v1.h"
#include "xAODEgamma/versions/EgammaAuxContainer_v1.h"

#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODEgamma/versions/Electron_v1.h"
#include "xAODEgamma/versions/ElectronContainer_v1.h"
#include "xAODEgamma/versions/ElectronAuxContainer_v1.h"
#include "xAODEgamma/versions/ElectronAuxContainer_v2.h"
#include "xAODEgamma/versions/ElectronAuxContainer_v3.h"

#include "xAODEgamma/Photon.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/PhotonAuxContainer.h"
#include "xAODEgamma/versions/Photon_v1.h"
#include "xAODEgamma/versions/PhotonContainer_v1.h"
#include "xAODEgamma/versions/PhotonAuxContainer_v1.h"
#include "xAODEgamma/versions/PhotonAuxContainer_v2.h"
#include "xAODEgamma/versions/PhotonAuxContainer_v3.h"

#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODEgamma/ElectronxAODHelpers.h"
#include "xAODEgamma/PhotonxAODHelpers.h"
#include "xAODEgamma/EgammaTruthxAODHelpers.h"
<<<<<<< HEAD
=======
#include "xAODEgamma/EgammaEnums.h"
#include "xAODEgamma/EgammaDefs.h"
#include "AthContainers/tools/AuxTypeVectorFactory.h"
>>>>>>> release/21.0.127

// EDM include(s).
#include "xAODCore/tools/DictHelpers.h"

// Instantiate all necessary types for the dictionary.
namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODEGAMMA {
<<<<<<< HEAD
      // Local type(s).
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, EgammaContainer_v1 );
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, ElectronContainer_v1 );
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, PhotonContainer_v1 );
      // Type(s) needed for the dictionary generation to succeed.
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, TrackParticleContainer );
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, CaloClusterContainer );
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, VertexContainer );
      // Type returned by helper function(s).
      std::set< const xAOD::TrackParticle* > setTP;
=======
      xAOD::EgammaContainer_v1 eg_c1;
      DataLink< xAOD::EgammaContainer_v1 > eg_l1;
      ElementLink< xAOD::EgammaContainer_v1 > eg_l2;
      ElementLinkVector< xAOD::EgammaContainer_v1 > eg_l3;
      std::vector< DataLink< xAOD::EgammaContainer_v1 > > eg_l4;
      std::vector< ElementLink< xAOD::EgammaContainer_v1 > > eg_l5;
      std::vector< ElementLinkVector< xAOD::EgammaContainer_v1 > > eg_l6;
      SG::AuxTypeVectorFactory<ElementLink< xAOD::EgammaContainer_v1 > > eg_l7;

      xAOD::ElectronContainer_v1 el_c1;
      DataLink< xAOD::ElectronContainer_v1 > el_l1;
      ElementLink< xAOD::ElectronContainer_v1 > el_l2;
      ElementLinkVector< xAOD::ElectronContainer_v1 > el_l3;
      std::vector< DataLink< xAOD::ElectronContainer_v1 > > el_l4;
      std::vector< ElementLink< xAOD::ElectronContainer_v1 > > el_l5;
      std::vector< ElementLinkVector< xAOD::ElectronContainer_v1 > > el_l6;

      xAOD::PhotonContainer_v1 ph_c1;
      DataLink< xAOD::PhotonContainer_v1 > ph_l1;
      ElementLink< xAOD::PhotonContainer_v1 > ph_l2;
      ElementLinkVector< xAOD::PhotonContainer_v1 > ph_l3;
      std::vector< DataLink< xAOD::PhotonContainer_v1 > > ph_l4;
      std::vector< ElementLink< xAOD::PhotonContainer_v1 > > ph_l5;
      std::vector< ElementLinkVector< xAOD::PhotonContainer_v1 > > ph_l6;

     // Instantiate the classes used by xAOD::Electron, xAODPhoton so that
     // Reflex would see them with their "correct type". Note that the
     // dictionary for these types comes from other places. This instantiation
     // is just needed for "Reflex related technical reasons"...
     ElementLink< xAOD::TrackParticleContainer > auxlink1;
     std::vector< ElementLink< xAOD::TrackParticleContainer > > auxlink2;
     ElementLink< xAOD::CaloClusterContainer > auxlink3;
     std::vector< ElementLink< xAOD::CaloClusterContainer > > auxlink4;
     ElementLink< xAOD::VertexContainer > auxlink5;
     std::vector< ElementLink< xAOD::VertexContainer > > auxlink6;
     
     std::set<const xAOD::TrackParticle*> setTP;

>>>>>>> release/21.0.127
   };
}

#endif // XAODEGAMMA_XAODEGAMMADICT_H
