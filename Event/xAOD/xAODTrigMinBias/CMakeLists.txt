<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
# $Id: CMakeLists.txt 751124 2016-05-31 12:55:34Z krasznaa $
################################################################################
# Package: xAODTrigMinBias
################################################################################
>>>>>>> release/21.0.127

# Declare the package name.
atlas_subdir( xAODTrigMinBias )

<<<<<<< HEAD
# Pull in the helper CMake code.
find_package( xAODUtilities )

# Component(s) in the package.
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Control/AthContainers
   Event/xAOD/xAODCore
   PRIVATE
   Control/AthLinks )

# Component(s) in the package:
>>>>>>> release/21.0.127
atlas_add_library( xAODTrigMinBias
   xAODTrigMinBias/*.h xAODTrigMinBias/versions/*.h Root/*.cxx
   PUBLIC_HEADERS xAODTrigMinBias
   LINK_LIBRARIES AthContainers xAODCore )
<<<<<<< HEAD

atlas_add_xaod_smart_pointer_dicts(
   INPUT xAODTrigMinBias/selection.xml
   OUTPUT _selectionFile
   CONTAINERS "xAOD::TrigSpacePointCountsContainer_v1"
              "xAOD::TrigT2MbtsBitsContainer_v1" "xAOD::TrigHisto2DContainer_v1"
              "xAOD::TrigTrackCountsContainer_v1"
              "xAOD::TrigVertexCountsContainer_v1"
              "xAOD::TrigT2ZdcSignalsContainer_v1" )

atlas_add_dictionary( xAODTrigMinBiasDict
   xAODTrigMinBias/xAODTrigMinBiasDict.h
   ${_selectionFile}
   LINK_LIBRARIES xAODCore xAODTrigMinBias
   EXTRA_FILES Root/dict/*.cxx )

# Generate CLIDs from the library.
=======

atlas_add_dictionary( xAODTrigMinBiasDict
   xAODTrigMinBias/xAODTrigMinBiasDict.h
   xAODTrigMinBias/selection.xml
   LINK_LIBRARIES AthLinks xAODTrigMinBias
   EXTRA_FILES Root/dict/*.cxx )

# Generate CLIDs from the library:
>>>>>>> release/21.0.127
atlas_generate_cliddb( xAODTrigMinBias )
