<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
# $Id: CMakeLists.txt 744251 2016-05-02 15:37:06Z krasznaa $
################################################################################
# Package: xAODEventShape
################################################################################
>>>>>>> release/21.0.127

# Declare the package name.
atlas_subdir( xAODEventShape )

<<<<<<< HEAD
# Pull in the helper CMake code.
find_package( xAODUtilities )

# Component(s) in the package.
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Control/AthContainers
   Event/xAOD/xAODCore )

# Component(s) in the package:
>>>>>>> release/21.0.127
atlas_add_library( xAODEventShape
   xAODEventShape/*.h xAODEventShape/versions/*.h Root/*.cxx
   PUBLIC_HEADERS xAODEventShape
   LINK_LIBRARIES AthContainers xAODCore )

<<<<<<< HEAD
atlas_add_xaod_smart_pointer_dicts(
   INPUT xAODEventShape/selection.xml
   OUTPUT _selectionFile
   OBJECTS "xAOD::EventShape_v1" )

atlas_add_dictionary( xAODEventShapeDict
   xAODEventShape/xAODEventShapeDict.h
   ${_selectionFile}
   LINK_LIBRARIES xAODCore xAODEventShape )
=======
atlas_add_dictionary( xAODEventShapeDict
   xAODEventShape/xAODEventShapeDict.h
   xAODEventShape/selection.xml
   LINK_LIBRARIES xAODEventShape )
>>>>>>> release/21.0.127
