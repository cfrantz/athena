<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
# $Id: CMakeLists.txt 774256 2016-09-21 08:28:51Z jchapman $
################################################################################
# Package: xAODJet
################################################################################
>>>>>>> release/21.0.127

# Declare the package name:
atlas_subdir( xAODJet )

# Optional dependencies:
<<<<<<< HEAD
set( extra_libs )
=======
set( extra_deps )
set( extra_libs )
set( extra_private_deps )
>>>>>>> release/21.0.127
set( extra_test_libs )

# Extra dependencies, when we are not in a standalone build:
if( NOT XAOD_STANDALONE )
<<<<<<< HEAD
   set( extra_test_libs SGTools )
else()
   set( extra_test_libs xAODRootAccess )
endif()

set( extra_srcs )
if( NOT SIMULATIONBASE AND NOT GENERATIONBASE )
   set( extra_srcs Root/JetTrigAuxContainer*.cxx )
   set( extra_libs xAODPFlow xAODTrigger )
endif()

# External dependencies:
find_package( ROOT COMPONENTS Core GenVector )
find_package( xAODUtilities )
=======
   set( extra_private_deps Control/SGTools )
   set( extra_test_libs SGTools )
endif()

if( NOT SIMULATIONBASE )
   set( extra_deps Event/xAOD/xAODBTagging Event/xAOD/xAODTrigger Event/xAOD/xAODPFlow)
   set( extra_libs xAODBTagging xAODPFlow xAODTrigger )
endif()

# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Control/AthContainers
   Control/AthLinks
   Event/xAOD/xAODBase
   Event/xAOD/xAODCore
   ${extra_deps}
   PRIVATE
   Control/CxxUtils
   Event/xAOD/xAODCaloEvent
   ${extra_private_deps} )

# External dependencies:
find_package( ROOT COMPONENTS Core GenVector )
>>>>>>> release/21.0.127

# Component(s) in the package:
atlas_add_library( xAODJet
   xAODJet/*.h xAODJet/*.icc xAODJet/versions/*.h xAODJet/versions/*.icc
<<<<<<< HEAD
   Root/JetA*.cxx Root/JetC*.cxx Root/Jet_v*.cxx Root/xAODJetCLIDs.cxx
   ${extra_srcs}
=======
   Root/*.cxx
>>>>>>> release/21.0.127
   PUBLIC_HEADERS xAODJet
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} AthContainers AthLinks
   xAODBase xAODCore ${extra_libs}
   PRIVATE_LINK_LIBRARIES CxxUtils xAODCaloEvent )
<<<<<<< HEAD

atlas_add_xaod_smart_pointer_dicts(
   INPUT xAODJet/selection.xml
   OUTPUT _selectionFile
   CONTAINERS "xAOD::JetContainer_v1" )

atlas_add_dictionary( xAODJetDict
   xAODJet/xAODJetDict.h
   ${_selectionFile}
   LINK_LIBRARIES xAODCore xAODJet
=======

atlas_add_dictionary( xAODJetDict
   xAODJet/xAODJetDict.h
   xAODJet/selection.xml
   LINK_LIBRARIES xAODJet
>>>>>>> release/21.0.127
   EXTRA_FILES Root/dict/*.cxx )

# Test(s) in the package:
atlas_add_test( xAODJet_Jet_test
<<<<<<< HEAD
   SOURCES test/xAODJet_Jet_test.cxx test/JetFactory.h test/JetFactory.cxx
   LINK_LIBRARIES xAODCore xAODCaloEvent xAODJet CxxUtils ${extra_test_libs}
   LOG_IGNORE_PATTERN "xAOD::Init" )
=======
   SOURCES test/xAODJet_Jet_test.cxx
   LINK_LIBRARIES xAODCore xAODCaloEvent xAODJet CxxUtils ${extra_test_libs} )
>>>>>>> release/21.0.127
