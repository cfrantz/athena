<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
# $Id: CMakeLists.txt 744481 2016-05-03 13:36:43Z krasznaa $
################################################################################
# Package: xAODBTagging
################################################################################
>>>>>>> release/21.0.127

# Declare the package name.
atlas_subdir( xAODBTagging )

<<<<<<< HEAD
# Pull in the helper CMake code.
find_package( xAODUtilities )
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Control/AthContainers
   Event/xAOD/xAODBase
   Event/xAOD/xAODCore
   Event/xAOD/xAODTracking
   Control/AthLinks )
>>>>>>> release/21.0.127

# Component(s) in the package:
atlas_add_library( xAODBTagging
   xAODBTagging/*.h xAODBTagging/versions/*.h Root/*.cxx
   PUBLIC_HEADERS xAODBTagging
<<<<<<< HEAD
   LINK_LIBRARIES AthContainers AthLinks xAODBase xAODCore xAODTracking xAODJet
   CxxUtils )

atlas_add_xaod_smart_pointer_dicts(
   INPUT xAODBTagging/selection.xml
   OUTPUT _selectionFile
   CONTAINERS "xAOD::BTaggingContainer_v1" "xAOD::BTagVertexContainer_v1" )

atlas_add_dictionary( xAODBTaggingDict
   xAODBTagging/xAODBTaggingDict.h
   ${_selectionFile}
   LINK_LIBRARIES xAODCore xAODBTagging
=======
   LINK_LIBRARIES AthContainers AthLinks xAODBase xAODCore xAODTracking )

atlas_add_dictionary( xAODBTaggingDict
   xAODBTagging/xAODBTaggingDict.h
   xAODBTagging/selection.xml
   LINK_LIBRARIES xAODBTagging
>>>>>>> release/21.0.127
   EXTRA_FILES Root/dict/*.cxx )
