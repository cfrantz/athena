// Dear emacs, this is -*- c++ -*-

/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

// $Id$
#ifndef XAODCUTFLOW_XAODCUTFLOWHELPERS_H
#define XAODCUTFLOW_XAODCUTFLOWHELPERS_H

// STL includes
#include <string>

<<<<<<< HEAD
=======
// ROOT includes
// #include <TH1.h>

>>>>>>> release/21.0.127
// Local include(s):
#include "xAODCutFlow/CutBookkeeperContainer.h"

// forward declarations
class TH1;


/// Namespace holding all the xAOD EDM classes
namespace xAOD {
  /// Namespace for all helpers
  namespace CutFlowHelpers {
    /// Make a histogram of the number of accepted events from a container.
    ::TH1* nAcceptedEvents( const xAOD::CutBookkeeperContainer* cbkCont,
                            const std::string& histName,
                            int minCycle=0, int maxCycle=-1 );

<<<<<<< HEAD
    /// Helper function to update a container with information from another one
    void updateContainer( xAOD::CutBookkeeperContainer *contToUpdate,
                          const xAOD::CutBookkeeperContainer *otherCont );
=======
>>>>>>> release/21.0.127
  } // End: namespace CutFlowHelpers

} // End: namespace xAOD

#endif // XAODCUTFLOW_XAODCUTFLOWHELPERS_H
