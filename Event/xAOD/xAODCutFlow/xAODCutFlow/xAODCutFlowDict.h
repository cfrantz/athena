// Dear emacs, this is -*- c++ -*-
/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODCUTFLOW_XAODCUTFLOWDICT_H
#define XAODCUTFLOW_XAODCUTFLOWDICT_H

<<<<<<< HEAD
// Local include(s).
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODCutFlow/CutBookkeeperAuxContainer.h"
#include "xAODCutFlow/versions/CutBookkeeper_v1.h"
#include "xAODCutFlow/versions/CutBookkeeperContainer_v1.h"
#include "xAODCutFlow/versions/CutBookkeeperAuxContainer_v1.h"
=======

// STL
#include <vector>

// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLink.h"
#include "AthLinks/ElementLinkVector.h"

// Local include(s):
#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODCutFlow/CutBookkeeperAuxContainer.h"
#include "xAODCutFlow/xAODCutFlowHelpers.h"
>>>>>>> release/21.0.127

#include "xAODCutFlow/xAODCutFlowHelpers.h"

// EDM include(s).
#include "xAODCore/tools/DictHelpers.h"

// Instantiate all necessary types for the dictionary.
namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODCUTFLOW {
      // Local type(s).
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, CutBookkeeperContainer_v1 );
   };
}

#endif // XAODCUTFLOW_XAODCUTFLOWDICT_H
