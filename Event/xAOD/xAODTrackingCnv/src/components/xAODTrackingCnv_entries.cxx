//
// Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
//

#ifndef XAOD_ANALYSIS
#   include "../TrackCollectionCnvTool.h"
#   include "../RecTrackParticleContainerCnvTool.h"
#   include "../TrackParticleCnvAlg.h"
#   include "../VertexCnvAlg.h"
#endif // NOT XAOD_ANALYSIS
#include "../TrackParticleCompressorTool.h"

<<<<<<< HEAD
#ifndef XAOD_ANALYSIS
   DECLARE_COMPONENT( xAODMaker::RecTrackParticleContainerCnvTool )
   DECLARE_COMPONENT( xAODMaker::TrackCollectionCnvTool )
   DECLARE_COMPONENT( xAODMaker::TrackParticleCnvAlg )
   DECLARE_COMPONENT( xAODMaker::VertexCnvAlg )
#endif // NOT XAOD_ANALYSIS
DECLARE_COMPONENT( xAODMaker::TrackParticleCompressorTool )
=======
// Local include(s):
#include "../TrackCollectionCnvTool.h"
#include "../RecTrackParticleContainerCnvTool.h"
#include "../TrackParticleCnvAlg.h"
#include "../VertexCnvAlg.h"
#include "../TrackParticleCompressorTool.h"

DECLARE_NAMESPACE_TOOL_FACTORY( xAODMaker, RecTrackParticleContainerCnvTool )
DECLARE_NAMESPACE_TOOL_FACTORY( xAODMaker, TrackCollectionCnvTool )
DECLARE_NAMESPACE_TOOL_FACTORY( xAODMaker, TrackParticleCompressorTool )
DECLARE_NAMESPACE_ALGORITHM_FACTORY( xAODMaker, TrackParticleCnvAlg )
DECLARE_NAMESPACE_ALGORITHM_FACTORY( xAODMaker, VertexCnvAlg )

DECLARE_FACTORY_ENTRIES( xAODCreatorAlgs ) {
   DECLARE_NAMESPACE_TOOL( xAODMaker, RecTrackParticleContainerCnvTool )
   DECLARE_NAMESPACE_TOOL( xAODMaker, TrackCollectionCnvTool )
   DECLARE_NAMESPACE_TOOL( xAODMaker, TrackParticleCompressorTool )
   DECLARE_NAMESPACE_ALGORITHM( xAODMaker, TrackParticleCnvAlg )
   DECLARE_NAMESPACE_ALGORITHM( xAODMaker, VertexCnvAlg )
}
>>>>>>> release/21.0.127
