// Dear emacs, this is -*- c++ -*-
/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/
<<<<<<< HEAD
=======

// $Id: xAODTauDict.h 796007 2017-02-07 15:38:04Z griffith $
>>>>>>> release/21.0.127
#ifndef XAODTAU_XAODTAUDICT_H
#define XAODTAU_XAODTAUDICT_H

// Local include(s).
#include "xAODTau/TauJet.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJetAuxContainer.h"
#include "xAODTau/versions/TauJet_v1.h"
#include "xAODTau/versions/TauJetContainer_v1.h"
#include "xAODTau/versions/TauJetAuxContainer_v1.h"
#include "xAODTau/versions/TauJet_v2.h"
#include "xAODTau/versions/TauJetContainer_v2.h"
#include "xAODTau/versions/TauJetAuxContainer_v2.h"
<<<<<<< HEAD
#include "xAODTau/versions/TauJet_v3.h"
#include "xAODTau/versions/TauJetContainer_v3.h"
#include "xAODTau/versions/TauJetAuxContainer_v3.h"

#include "xAODTau/DiTauJet.h"
#include "xAODTau/DiTauJetContainer.h"
#include "xAODTau/DiTauJetAuxContainer.h"
#include "xAODTau/versions/DiTauJet_v1.h"
#include "xAODTau/versions/DiTauJetContainer_v1.h"
#include "xAODTau/versions/DiTauJetAuxContainer_v1.h"
=======
#include "xAODTau/versions/TauJetContainer_v3.h"
#include "xAODTau/versions/TauJetAuxContainer_v3.h"
#include "xAODTau/versions/DiTauJetContainer_v1.h"
#include "xAODTau/versions/DiTauJetAuxContainer_v1.h"
#include "xAODTau/versions/TauTrackContainer_v1.h"
#include "xAODTau/versions/TauTrackAuxContainer_v1.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/TauJetAuxContainer.h"
#include "xAODTau/DiTauJetContainer.h"
#include "xAODTau/DiTauJetAuxContainer.h"
#include "xAODTau/TauTrackContainer.h"
#include "xAODTau/TauTrackAuxContainer.h"
>>>>>>> release/21.0.127

#include "xAODTau/TauTrack.h"
#include "xAODTau/TauTrackContainer.h"
#include "xAODTau/TauTrackAuxContainer.h"
#include "xAODTau/versions/TauTrack_v1.h"
#include "xAODTau/versions/TauTrackContainer_v1.h"
#include "xAODTau/versions/TauTrackAuxContainer_v1.h"

<<<<<<< HEAD
#include "xAODTau/TauxAODHelpers.h"
=======
/// Declare a dummy CLID for xAOD::TauJetContainer_v1. This is only necessary
/// to get DataLink<xAOD::TauJetContainer_v1> and
/// ElementLink<xAOD::TauJetContainer_v1> compiling in the dictionary. The CLID
/// is not needed in the "real" code, since users are never exposed to the _v1
/// classes in Athena anymore.
CLASS_DEF( xAOD::TauJetContainer_v1, 12345678, 10 )
CLASS_DEF( xAOD::TauJetContainer_v2, 123456789, 10 )

namespace {
  struct GCCXML_DUMMY_INSTANTIATION_XAODTAU {
    xAOD::TauJetContainer_v1 c1;
    DataLink< xAOD::TauJetContainer_v1 > l1;
    ElementLink< xAOD::TauJetContainer_v1 > l2;
    ElementLinkVector< xAOD::TauJetContainer_v1 > l3;
    std::vector< DataLink< xAOD::TauJetContainer_v1 > > l4;
    std::vector< ElementLink< xAOD::TauJetContainer_v1 > > l5;
    std::vector< std::vector< ElementLink< xAOD::TauJetContainer_v1 > > > l6;
    std::vector< ElementLinkVector< xAOD::TauJetContainer_v1 > > l7;

    xAOD::TauJetContainer_v2 c2;
    DataLink< xAOD::TauJetContainer_v2 > l8;
    ElementLink< xAOD::TauJetContainer_v2 > l9;
    ElementLinkVector< xAOD::TauJetContainer_v2 > l10;
    std::vector< DataLink< xAOD::TauJetContainer_v2 > > l11;
    std::vector< ElementLink< xAOD::TauJetContainer_v2 > > l12;
    std::vector< std::vector< ElementLink< xAOD::TauJetContainer_v2 > > > l13;
    std::vector< ElementLinkVector< xAOD::TauJetContainer_v2 > > l14;
>>>>>>> release/21.0.127

// EDM include(s).
#include "xAODCore/tools/DictHelpers.h"

<<<<<<< HEAD
// Instantiate all necessary types for the dictionary.
namespace {
   struct GCCXML_DUMMY_INSTANTIATION_XAODTAU {
      // Local type(s).
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, TauJetContainer_v1 );
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, TauJetContainer_v2 );
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, TauJetContainer_v3 );
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, DiTauJetContainer_v1 );
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, TauTrackContainer_v1 );
      // Type(s) needed for the dictionary generation to succeed.
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, TrackParticleContainer );
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, VertexContainer );
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, JetContainer );
      XAOD_INSTANTIATE_NS_CONTAINER_TYPES( xAOD, PFOContainer );
=======
    xAOD::TauTrackContainer_v1 c4;
    DataLink< xAOD::TauTrackContainer_v1 > l22;
    ElementLink< xAOD::TauTrackContainer_v1 > l23;
    ElementLinkVector< xAOD::TauTrackContainer_v1 > l24;
    std::vector< DataLink< xAOD::TauTrackContainer_v1 > > l25;
    std::vector< ElementLink< xAOD::TauTrackContainer_v1 > > l26;
    std::vector< std::vector< ElementLink< xAOD::TauTrackContainer_v1 > > > l27;
    std::vector< ElementLinkVector< xAOD::TauTrackContainer_v1 > > l28;

    xAOD::TauJetContainer_v2 c5;
    DataLink< xAOD::TauJetContainer_v2 > l29;
    ElementLink< xAOD::TauJetContainer_v2 > l30;
    ElementLinkVector< xAOD::TauJetContainer_v2 > l31;
    std::vector< DataLink< xAOD::TauJetContainer_v2 > > l32;
    std::vector< ElementLink< xAOD::TauJetContainer_v2 > > l33;
    std::vector< std::vector< ElementLink< xAOD::TauJetContainer_v2 > > > l34;
    std::vector< ElementLinkVector< xAOD::TauJetContainer_v2 > > l35;


    // Instantiate the classes used by xAOD::TauJetAuxContainer, so that
    // Reflex would see them with their "correct type". Note that the
    // dictionary for these types comes from other places. This instantiation
    // is just needed for "Reflex related technical reasons"...
    ElementLink< xAOD::TrackParticleContainer > auxlink1;
    std::vector< ElementLink< xAOD::TrackParticleContainer > > auxlink2;
    ElementLink< xAOD::JetContainer > auxlink3;
    std::vector< ElementLink< xAOD::JetContainer > > auxlink4;
    ElementLink< xAOD::PFOContainer > auxlink5;
    std::vector< ElementLink< xAOD::PFOContainer > > auxlink6;
    ElementLink< xAOD::VertexContainer > auxlink7;
    std::vector< ElementLink< xAOD::VertexContainer > > auxlink8;
    ElementLink< xAOD::TauTrackContainer > auxlink9;
    std::vector< ElementLink< xAOD::TauTrackContainer > > auxlink10;
>>>>>>> release/21.0.127
   };
}

#endif // XAODTAU_XAODTAUDICT_H
