/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

// Implementation file for class BookkeeperTool
// Authors: Tadej Novak <tadej@cern.ch>
//          Jack Cranshaw <Jack.Cranshaw@cern.ch>

#include <xAODCutFlow/xAODCutFlowHelpers.h>

#ifndef GENERATIONBASE
#include <xAODMetaData/FileMetaData.h>
#include <xAODTruth/TruthMetaDataContainer.h>
#endif

#ifndef XAOD_STANDALONE
#include <AthenaPoolUtilities/CondAttrListCollection.h>
#include <IOVDbDataModel/IOVMetaDataContainer.h>
#endif

#include <EventBookkeeperTools/BookkeeperTool.h>
#include <EventBookkeeperTools/CutFlowSvc.h>


BookkeeperTool::BookkeeperTool(const std::string& name)
  : asg::AsgMetadataTool(name),
    m_cutflowTaken(false)
{
<<<<<<< HEAD
#ifndef XAOD_STANDALONE
=======
  declareProperty("OutputCollName", m_outputCollName="CutBookkeepers",  
    "The default name of the xAOD::CutBookkeeperContainer for output files");
  declareProperty("InputCollName", m_inputCollName = "CutBookkeepers",
    "The default name of the xAOD::CutBookkeeperContainer for input files");
  declareProperty("CutFlowCollName", m_cutflowCollName = "CutBookkeepersFile",
    "The default name of the xAOD::CutBookkeeperContainer for CutFlowSvc");
#ifdef ASGTOOL_ATHENA
>>>>>>> release/21.0.127
  declareInterface< ::IMetaDataTool >( this );
#endif // XAOD_STANDALONE
}


StatusCode BookkeeperTool::initialize()
{
  ATH_MSG_DEBUG("Initializing " << name());

  ATH_MSG_DEBUG("InputCollName = " << m_inputCollName);
  ATH_MSG_DEBUG("OutputCollName = " << m_outputCollName);

<<<<<<< HEAD
#ifndef XAOD_STANDALONE
  ATH_CHECK(m_cutFlowSvc.retrieve());
  // Access "private" CutFlowSvc methods
  m_cutFlowSvcPrivate = dynamic_cast<CutFlowSvc *>(&*(m_cutFlowSvc));
#endif
=======

StatusCode
BookkeeperTool::initialize()
{
  ATH_MSG_DEBUG( "Initializing " << name() << " - package version " << PACKAGE_VERSION );

  ATH_MSG_DEBUG("InputCollName = " << m_inputCollName);
  ATH_MSG_DEBUG("OutputCollName = " << m_outputCollName);
  ATH_MSG_DEBUG("CutFlowCollName = " << m_cutflowCollName);
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}


#ifdef XAOD_STANDALONE
StatusCode BookkeeperTool::beginInputFile()
#else
StatusCode BookkeeperTool::beginInputFile(const SG::SourceID &source)
#endif
{
  // OPENING NEW INPUT FILE
  // Things to do:
  // 1) Load CutBookkeepers from input file
  //    a) if incomplete from input, directly propagate to output
  //    b) if complete from input, wait for EndInputFile to decide what to do in output

  // Get number of event weight variations if needed
  if (m_numberOfWeightVariations == 0) {
    ATH_MSG_DEBUG("Determining number of weight variations");
#ifndef GENERATIONBASE
    if (inputMetaStore()->contains<xAOD::TruthMetaDataContainer>("TruthMetaData")) {
      StatusCode status = loadXAODMetaData();
      if (!status.isSuccess()) {
        if (status.isRecoverable()) {
          ATH_CHECK(loadPOOLMetaData());
        } else {
          return StatusCode::FAILURE;
        }
      }
    } else {
      ATH_CHECK(loadPOOLMetaData());
    }
#else
    ATH_CHECK(loadPOOLMetaData());
#endif

<<<<<<< HEAD
    if (m_numberOfWeightVariations == 0) {
      ATH_MSG_ERROR("Could not determine the number of weight variations");
      return StatusCode::FAILURE;
    }

    ATH_MSG_INFO("Using number of event weight variations " << m_numberOfWeightVariations);

#ifndef XAOD_STANDALONE
    ATH_CHECK(m_cutFlowSvcPrivate->setNumberOfWeightVariations(m_numberOfWeightVariations));
#endif
  }

#ifdef XAOD_STANDALONE
  SG::SourceID source{"file"};
#endif

  // Prepare local cache
  CutBookkeepersLocalCache::prepareContainers(m_completeContainers, m_numberOfWeightVariations);
  CutBookkeepersLocalCache::prepareContainers(m_incompleteContainers, m_numberOfWeightVariations);

  // Check if a local container is there. IT SHOULD NOT BE
  auto sit = m_inputContainers.find(source);
  if (sit != m_inputContainers.end()) {
    ATH_MSG_ERROR("Undefined behaviour: this file has already been processed");
    return StatusCode::FAILURE;
  }

  // Get the incomplete bookkeeper collection of the input metadata store
  const xAOD::CutBookkeeperContainer *inIncomplete{};
  // Construct input and output incomplete names
  std::string incompleteCollName = "Incomplete" + m_inputCollName;
  if (inputMetaStore()->contains<xAOD::CutBookkeeperContainer>(incompleteCollName)) {
    if (inputMetaStore()->retrieve(inIncomplete, incompleteCollName).isSuccess()) {
      // update incomplete output with any incomplete input
      xAOD::CutFlowHelpers::updateContainer(m_incompleteContainers.at(0), inIncomplete);
      ATH_MSG_DEBUG("Successfully merged input incomplete bookkeepers with output");
=======
  // reset cutflow taken marker
  m_cutflowTaken = false;

  // Get the incomplete bookkeeper collection of the input metadata store
  const xAOD::CutBookkeeperContainer* input_inc = 0;
  // Construct input and output incomplete names
  std::string inCollName = "Incomplete" + m_inputCollName;
  std::string outCollName = "Incomplete" + m_outputCollName;
  if (inputMetaStore()->contains<xAOD::CutBookkeeperContainer>(inCollName) ) {
    StatusCode ssc = inputMetaStore()->retrieve( input_inc, inCollName );
    if (ssc.isSuccess()) {
      // First make sure there is an incomplete container in the output store
      if( !(outputMetaStore()->contains<xAOD::CutBookkeeperContainer>(outCollName)) ) {
        xAOD::CutBookkeeperContainer* inc = new xAOD::CutBookkeeperContainer();
        xAOD::CutBookkeeperAuxContainer* auxinc = new xAOD::CutBookkeeperAuxContainer();
        inc->setStore(auxinc);
        ATH_CHECK(outputMetaStore()->record(inc,outCollName));
        ATH_CHECK(outputMetaStore()->record(auxinc,outCollName+"Aux."));
      }
      // retrieve the incomplete output container
      xAOD::CutBookkeeperContainer* incompleteBook(NULL);
      ATH_CHECK(outputMetaStore()->retrieve( incompleteBook, outCollName));
      // update incomplete output with any incomplete input
      ATH_CHECK(this->updateContainer(incompleteBook,input_inc));
      ATH_MSG_DEBUG("Successfully merged input incomplete bookkeepers with output");
    }
  }
  else {
    ATH_MSG_INFO("No incomplete bookkeepers in this file " << inCollName);
  }

  // Get the complete bookkeeper collection of the input metadata store
  const xAOD::CutBookkeeperContainer* input_com = 0;
  inCollName = m_inputCollName;
  outCollName = m_outputCollName;
  if (inputMetaStore()->contains<xAOD::CutBookkeeperContainer>(inCollName) ) {
    if ( (inputMetaStore()->retrieve( input_com, inCollName )).isSuccess() ) {
      // Check if a tmp is there. IT SHOULD NOT BE
      //xAOD::CutBookkeeperContainer* incompleteBook(NULL);
      if( !(outputMetaStore()->contains<xAOD::CutBookkeeperContainer>(outCollName+"tmp")) ) {
        // Now create the tmp container
        xAOD::CutBookkeeperContainer* tmp = new xAOD::CutBookkeeperContainer();
        xAOD::CutBookkeeperAuxContainer* auxtmp = new xAOD::CutBookkeeperAuxContainer();
        tmp->setStore(auxtmp);
        if (updateContainer(tmp,input_com).isSuccess()) {
          ATH_CHECK(outputMetaStore()->record(tmp,outCollName+"tmp"));
          ATH_CHECK(outputMetaStore()->record(auxtmp,outCollName+"tmpAux."));
        }
        else {
          ATH_MSG_WARNING("Could not update tmp container from input complete conatiner");
        }
      }
>>>>>>> release/21.0.127
    }
  } else {
    ATH_MSG_INFO("No incomplete bookkeepers in this file with name " << incompleteCollName);
  }

  // Get the complete bookkeeper collection of the input metadata store
  const xAOD::CutBookkeeperContainer *inComplete{};
  if (inputMetaStore()->contains<xAOD::CutBookkeeperContainer>(m_inputCollName)) {
    if (inputMetaStore()->retrieve(inComplete, m_inputCollName).isSuccess()) {
      // Create the temporary container
      auto [it, status] = m_inputContainers.emplace(source, CutBookkeepersLocalCache());
      CutBookkeepersLocalCache::prepareContainers(it->second, m_numberOfWeightVariations);

      xAOD::CutFlowHelpers::updateContainer(it->second.at(0), inComplete);
      ATH_MSG_DEBUG("Successfully copied complete bookkeepers to temp container");
    } else {
      ATH_MSG_INFO("No complete bookkeepers in this file with name " << m_inputCollName);
    }
  }
<<<<<<< HEAD

  return StatusCode::SUCCESS;
}

#ifdef XAOD_STANDALONE
StatusCode BookkeeperTool::endInputFile()
#else
StatusCode BookkeeperTool::endInputFile(const SG::SourceID &source)
#endif
{
#ifdef XAOD_STANDALONE
  SG::SourceID source{"file"};
#endif
  ATH_CHECK(copyInputContainersToOutput(m_completeContainers, source));

=======

  //  Now make sure the output containers are in the output store
  //
  //  Make sure complete container exists in output
  if( !(outputMetaStore()->contains<xAOD::CutBookkeeperContainer>(m_outputCollName)) ) {
      // Now create the complete container
    xAOD::CutBookkeeperContainer* inc = new xAOD::CutBookkeeperContainer();
    xAOD::CutBookkeeperAuxContainer* auxinc = new xAOD::CutBookkeeperAuxContainer();
    inc->setStore(auxinc);
    ATH_CHECK(outputMetaStore()->record(inc,m_outputCollName));
    ATH_CHECK(outputMetaStore()->record(auxinc,m_outputCollName+"Aux."));
  }
  else {
    ATH_MSG_INFO("complete collection already exists");
    //return StatusCode::SUCCESS;
  }
  //  Make sure incomplete container exists in output
  std::string inc_name = "Incomplete"+m_outputCollName;
  if( !(outputMetaStore()->contains<xAOD::CutBookkeeperContainer>(inc_name)) ) {
      // Now create the complete container
    xAOD::CutBookkeeperContainer* coll = new xAOD::CutBookkeeperContainer();
    xAOD::CutBookkeeperAuxContainer* auxcoll = new xAOD::CutBookkeeperAuxContainer();
    coll->setStore(auxcoll);
    ATH_CHECK(outputMetaStore()->record(coll,inc_name));
    ATH_CHECK(outputMetaStore()->record(auxcoll,inc_name+"Aux."));
  }
  else {
    ATH_MSG_INFO("incomplete collection already exists");
  }
  
  return StatusCode::SUCCESS;
}


StatusCode BookkeeperTool::endInputFile()
{

  // Get the complete bookkeeper collection of the output meta-data store
  xAOD::CutBookkeeperContainer* completeBook(NULL); 
  if( !(outputMetaStore()->retrieve( completeBook, m_outputCollName) ).isSuccess() ) {
    ATH_MSG_ERROR( "Could not get complete CutBookkeepers from output MetaDataStore" );
    return StatusCode::FAILURE;
  }

  // Get the tmp bookkeeper from the input
  const xAOD::CutBookkeeperContainer* tmpCompleteBook(NULL);
  if ( outputMetaStore()->contains<xAOD::CutBookkeeperContainer>(m_outputCollName+"tmp") ) {
    if( !(outputMetaStore()->retrieve( tmpCompleteBook, m_outputCollName+"tmp") ).isSuccess() ) {
      ATH_MSG_WARNING( "Could not get tmp CutBookkeepers from output MetaDataStore" );
    }
    else {
      // update the complete output with the complete input
      ATH_CHECK(this->updateContainer(completeBook,tmpCompleteBook));
      // remove the tmp container
      const SG::IConstAuxStore* tmpCompleteBookAux = tmpCompleteBook->getConstStore();
      ATH_CHECK(outputMetaStore()->removeDataAndProxy(tmpCompleteBook));
      ATH_CHECK(outputMetaStore()->removeDataAndProxy(tmpCompleteBookAux));
    }
  }

  if (!m_cutflowTaken) {
    if (addCutFlow().isFailure()) {
      ATH_MSG_ERROR("Could not add CutFlow information");
    }
    m_cutflowTaken = true;
  }
  else {
    ATH_MSG_DEBUG("Cutflow information written into container before endInputFile");
  }
    
>>>>>>> release/21.0.127
  return StatusCode::SUCCESS;
}

StatusCode BookkeeperTool::metaDataStop()
{
  // TERMINATING THE JOB (EndRun)
  // Things to do:
  // 1) Create new incomplete CutBookkeepers if relevant
<<<<<<< HEAD
  // 2) Copy cutflow from the service
  // 3) Copy containers to output

  // All files that have not been fully processed are incomplete
  ATH_CHECK(copyInputContainersToOutput(m_incompleteContainers));

  // Copy cutflow from the service
  ATH_CHECK(copyCutflowFromService());

  // Write out the containers
  for (std::size_t i = 0; i < m_completeContainers.size(); ++i) {
    std::string name = m_outputCollName;
    if (i != 0) {
      name.append("_weight_");
      name.append(std::to_string(i));
    }
    std::string incompleteName = "Incomplete" + name;

    // In MP we might already have them written out
    if (outputMetaStore()->contains<xAOD::CutBookkeeperContainer>(name)) {
      xAOD::CutBookkeeperContainer *complete{};
      if (!outputMetaStore()->retrieve(complete, name).isSuccess()) {
        ATH_MSG_ERROR("Could not get " << name << " CutBookkeepers from output MetaDataStore");
        return StatusCode::FAILURE;
      }
      xAOD::CutFlowHelpers::updateContainer(complete, m_completeContainers.at(i));
    } else {
      ATH_CHECK(outputMetaStore()->record(std::move(m_completeContainers.cont[i]), name));
      ATH_CHECK(outputMetaStore()->record(std::move(m_completeContainers.aux[i]), name + "Aux."));
    }

    if (outputMetaStore()->contains<xAOD::CutBookkeeperContainer>(incompleteName)) {    
      xAOD::CutBookkeeperContainer *incomplete{};
      if (!outputMetaStore()->retrieve(incomplete, incompleteName).isSuccess()) {
        ATH_MSG_ERROR("Could not get " << incompleteName << " CutBookkeepers from output MetaDataStore");
        return StatusCode::FAILURE;
      }
      xAOD::CutFlowHelpers::updateContainer(incomplete, m_incompleteContainers.at(i));
    } else {
      // Only write non-empty incomplete containers
      if (i > 0 && !m_incompleteContainers.at(i)->empty()) {
        ATH_CHECK(outputMetaStore()->record(std::move(m_incompleteContainers.cont[i]), incompleteName));
        ATH_CHECK(outputMetaStore()->record(std::move(m_incompleteContainers.aux[i]), incompleteName + "Aux."));
      }
    }
  }

  m_incompleteContainers.clear();
  m_completeContainers.clear();
=======
  // 2) Print cut flow summary
  // 3) Write root file if requested
  //  Make sure incomplete container exists in output
  std::string inc_name = "Incomplete"+m_outputCollName;

  // Get the complete bookkeeper collection of the output meta-data store
  xAOD::CutBookkeeperContainer* incompleteBook(NULL); 
  if( !(outputMetaStore()->retrieve( incompleteBook, inc_name) ).isSuccess() ) {
    ATH_MSG_WARNING( "Could not get incomplete CutBookkeepers from output MetaDataStore" );
  }

  // Get the tmp bookkeeper from the input
  const xAOD::CutBookkeeperContainer* tmpCompleteBook(NULL);
  if ( outputMetaStore()->contains<xAOD::CutBookkeeperContainer>(m_outputCollName+"tmp") ) {
    if( !(outputMetaStore()->retrieve( tmpCompleteBook, m_outputCollName+"tmp") ).isSuccess() ) {
      ATH_MSG_WARNING( "Could not get tmp CutBookkeepers from output MetaDataStore for " << m_outputCollName+"tmp");
    }
    else {
      // update the complete output with the complete input
      ATH_CHECK(this->updateContainer(incompleteBook,tmpCompleteBook));
      // remove the tmp container
      const SG::IConstAuxStore* tmpCompleteBookAux = tmpCompleteBook->getConstStore();
      ATH_CHECK(outputMetaStore()->removeDataAndProxy(tmpCompleteBook));
      ATH_CHECK(outputMetaStore()->removeDataAndProxy(tmpCompleteBookAux));
    }
  }

  if (!m_cutflowTaken) {
    if (addCutFlow().isFailure()) {
      ATH_MSG_ERROR("Could not add CutFlow information");
    }
  }
  else {
    ATH_MSG_DEBUG("Cutflow information written into container before metaDataStop");
  }

  // Reset after metadata stop
  m_cutflowTaken = false;
  
  return StatusCode::SUCCESS;
}
>>>>>>> release/21.0.127

  ATH_MSG_DEBUG("Successfully copied CutBookkeepers to the output MetaDataStore");

  return StatusCode::SUCCESS;
}

<<<<<<< HEAD

StatusCode BookkeeperTool::copyInputContainersToOutput(CutBookkeepersLocalCache &target,
                                                       const SG::SourceID &source)
{
  CutBookkeepersLocalCache::prepareContainers(target, m_numberOfWeightVariations);

  if (!source.empty()) {
    auto it = m_inputContainers.find(source);
    if (it == m_inputContainers.end()) {
      ATH_MSG_DEBUG("No input containers for this file");
      return StatusCode::SUCCESS;
    }

    for (std::size_t i = 0; i < it->second.size(); ++i) {
      xAOD::CutFlowHelpers::updateContainer(target.at(i), it->second.at(i));
    }
    m_inputContainers.erase(it);

    return StatusCode::SUCCESS;
  }

  for (auto &[s, list] : m_inputContainers) {
    for (std::size_t i = 0; i < list.size(); ++i) {
      xAOD::CutFlowHelpers::updateContainer(target.at(i), list.at(i));
    }
  }
  m_inputContainers.clear();

  return StatusCode::SUCCESS;
}

StatusCode BookkeeperTool::copyCutflowFromService()
{
#ifndef XAOD_STANDALONE
  // Get the bookkeeper from the current processing
  const CutBookkeepersLocalCache &cache = m_cutFlowSvcPrivate->getCutBookkeepers();
  if (!cache.empty()) {
    CutBookkeepersLocalCache::prepareContainers(m_completeContainers, std::max(cache.size(), m_numberOfWeightVariations));

    for (std::size_t i = 0; i < cache.size(); ++i) {
      xAOD::CutFlowHelpers::updateContainer(m_completeContainers.at(i), cache.at(i));
    }
  } else {
    ATH_MSG_ERROR("No cutflow container in the CutFlowSvc");
    return StatusCode::FAILURE;
  }
#endif

  return StatusCode::SUCCESS;
}


StatusCode BookkeeperTool::loadXAODMetaData()
{
#ifdef GENERATIONBASE
  return StatusCode::RECOVERABLE;
#else

  // Try to load MC channel number from file metadata
  ATH_MSG_DEBUG("Attempting to retrieve MC channel number...");
  const xAOD::FileMetaData *fileMetaData{};
  uint32_t mcChannelNumber{uint32_t(-1)};
  if (inputMetaStore()->contains<xAOD::FileMetaData>("FileMetaData")) {
    ATH_CHECK(inputMetaStore()->retrieve(fileMetaData, "FileMetaData"));
    float fltChannelNumber{-1};
    if (fileMetaData->value(xAOD::FileMetaData::mcProcID, fltChannelNumber)) {
      mcChannelNumber = static_cast<uint32_t>(mcChannelNumber);
=======
/*
StatusCode
BookkeeperTool::updateContainer( xAOD::CutBookkeeperContainer* contToUpdate,
                             const xAOD::CutBookkeeperContainer* otherCont ) {
  ATH_MSG_DEBUG("calling updateContainer(...)" );
  ATH_MSG_DEBUG("Have container to update with size=" << contToUpdate->size()
                  << ", and other container with size=" << otherCont->size() );

  // Create an vector of indices of all the newly transferred CutBookkeepers
  std::vector< std::size_t > newEBKIndices;
  // Loop through otherCont.
  // If element already in contToUpdate, update event counts, otherwise create new element
  for ( std::size_t i=0; i<otherCont->size(); ++i ) {
    const xAOD::CutBookkeeper* otherEBK = otherCont->at(i);
    ATH_MSG_VERBOSE("Looping through otherCont at index " << i);
    ATH_MSG_VERBOSE("Have otherEBK with: name=" << otherEBK->name()
                    << ", cycle=" << otherEBK->cycle()
                    << ", nAcceptedEvents=" << otherEBK->nAcceptedEvents()
                    << ", inputStream=" << otherEBK->inputStream() );


    // Loop through the container to be updated (contToUpdate) and see if we find a match
    bool foundEBKToUpdate(false);
    for ( std::size_t j=0; j<contToUpdate->size(); ++j ) {
      xAOD::CutBookkeeper* ebkToUpdate = contToUpdate->at(j);
      // Check if they are identical, if so, update; else add otherEBK
      if ( otherEBK->isEqualTo(ebkToUpdate) ) {
        ebkToUpdate->setPayload( ebkToUpdate->payload() + otherEBK->payload() );
        foundEBKToUpdate = true;
        break;
      }
    } // End: Inner loop over contToUpdate
    if (!foundEBKToUpdate) {
      xAOD::CutBookkeeper* newEBK = new xAOD::CutBookkeeper();
      if ( newEBK->usingPrivateStore() ) { newEBK->releasePrivateStore(); }
      newEBK->makePrivateStore(otherEBK);
      contToUpdate->push_back( newEBK );
      std::size_t ebIdx = newEBK->index();
      newEBKIndices.push_back(ebIdx);
>>>>>>> release/21.0.127
    }
  }
  if (mcChannelNumber == uint32_t(-1)) {
    ATH_MSG_WARNING("... MC channel number could not be loaded from FileMetaData");
#ifdef XAOD_STANDALONE
    mcChannelNumber = 0;
#else
    return StatusCode::RECOVERABLE;
#endif
  }

  // Find the correct truth meta data object
  ATH_MSG_DEBUG("Attempting to load weight meta data from xAOD TruthMetaData for channel " << mcChannelNumber);
  const xAOD::TruthMetaDataContainer *metaDataContainer{};
  ATH_CHECK(inputMetaStore()->retrieve(metaDataContainer, "TruthMetaData"));

  // If we only have one metadata item take MC channel from there if needed
  if (mcChannelNumber == 0 && metaDataContainer->size() == 1) {
    mcChannelNumber = metaDataContainer->at(0)->mcChannelNumber();
    ATH_MSG_WARNING("... MC channel number taken from the metadata as " << mcChannelNumber);
  }

  auto itTruthMetaDataPtr = std::find_if(metaDataContainer->begin(), metaDataContainer->end(),
    [mcChannelNumber] (const auto& it) { return it->mcChannelNumber() == mcChannelNumber; }
  );

  // If no such object is found then return
  if (itTruthMetaDataPtr == metaDataContainer->end()) {
#ifdef XAOD_STANDALONE
    m_numberOfWeightVariations = 1;
    ATH_MSG_DEBUG("Could not load weight meta data! Assumming 1 variation.");
    return StatusCode::SUCCESS;
#else
    ATH_MSG_DEBUG("Could not load weight meta data from TruthMetaData!");
    return StatusCode::RECOVERABLE;
#endif
  }

  // Update cached weight data
  const std::vector<std::string> &truthWeightNames = (*itTruthMetaDataPtr)->weightNames();

  m_numberOfWeightVariations = truthWeightNames.size();
  if (m_numberOfWeightVariations == 0) {
    ATH_MSG_DEBUG("No variations present, setting to 1.");
    m_numberOfWeightVariations = 1;
  }

  return StatusCode::SUCCESS;
#endif
}
*/


StatusCode BookkeeperTool::addCutFlow()
{
  // Add the information from the current processing to the complete output
  // --> same paradigm as original CutFlowSvc
  // Get the complete bookkeeper collection of the output meta-data store
  xAOD::CutBookkeeperContainer* completeBook(NULL); 
  if( !(outputMetaStore()->retrieve( completeBook, m_outputCollName) ).isSuccess() ) {
    ATH_MSG_ERROR( "Could not get complete CutBookkeepers from output MetaDataStore" );
    return StatusCode::FAILURE;
  }

  // Get the bookkeeper from the current processing
  const xAOD::CutBookkeeperContainer* fileCompleteBook(NULL);
  if( outputMetaStore()->contains<xAOD::CutBookkeeperContainer>(m_cutflowCollName) ) {
    if( !(outputMetaStore()->retrieve( fileCompleteBook, m_cutflowCollName) ).isSuccess() ) {
      ATH_MSG_WARNING( "Could not get CutFlowSvc CutBookkeepers from output MetaDataStore" );
    }
    else {
      // update the complete output with the complete input
      ATH_CHECK(this->updateContainer(completeBook,fileCompleteBook));
    }
  }

  return StatusCode::SUCCESS;
}


namespace {


xAOD::CutBookkeeper*
resolveLink (const xAOD::CutBookkeeper* old,
             xAOD::CutBookkeeperContainer& contToUpdate,
             const xAOD::CutBookkeeperContainer& otherCont,
             const std::vector<size_t>& otherIndices)
{
  {
    xAOD::CutBookkeeperContainer::iterator matchIter = 
      std::find( contToUpdate.begin(),
                 contToUpdate.end(),
                 old );
    if (matchIter != contToUpdate.end())
      return *matchIter;
  }

  {
    xAOD::CutBookkeeperContainer::const_iterator matchIter = 
      std::find( otherCont.begin(),
                 otherCont.end(),
                 old );
    if (matchIter != contToUpdate.end()) {
      size_t pos = matchIter - otherCont.begin();
      return contToUpdate[otherIndices[pos]];
    }
  }

  // If we didn't find it, we need to add it
  xAOD::CutBookkeeper* newEBK = new xAOD::CutBookkeeper();
  if ( newEBK->usingPrivateStore() ) { newEBK->releasePrivateStore(); }
  newEBK->makePrivateStore(old);
  contToUpdate.push_back( newEBK );
  return newEBK;
}


} // anonymous namespace


StatusCode
BookkeeperTool::updateContainer( xAOD::CutBookkeeperContainer* contToUpdate,
                             const xAOD::CutBookkeeperContainer* otherCont ) 
{
  ATH_MSG_DEBUG("calling updateContainer(...)" );
  ATH_MSG_VERBOSE("Have container to update with size=" << contToUpdate->size()
                  << ", and other container with size=" << otherCont->size() );

  size_t origSize = contToUpdate->size();

  // Vector of indices in contToUpdate of elements in otherCont.
  std::vector< std::size_t > otherIndices (otherCont->size());
  // Loop through otherCont.
  // If element already in contToUpdate, update event counts, otherwise create new element
  for ( std::size_t i=0; i<otherCont->size(); ++i ) {
    const xAOD::CutBookkeeper* otherEBK = otherCont->at(i);
    ATH_MSG_VERBOSE("Looping through otherCont at index " << i);
    ATH_MSG_VERBOSE("Have otherEBK with: name=" << otherEBK->name()
                    << ", cycle=" << otherEBK->cycle()
                    << ", nAcceptedEvents=" << otherEBK->nAcceptedEvents()
                    << ", inputStream=" << otherEBK->inputStream() );


    // Loop through the container to be updated (contToUpdate) and see if we find a match
    bool foundEBKToUpdate(false);
    for ( std::size_t j=0; j<contToUpdate->size(); ++j ) {
      xAOD::CutBookkeeper* ebkToUpdate = contToUpdate->at(j);
      // Check if they are identical, if so, update; else add otherEBK
      if ( otherEBK->isEqualTo(ebkToUpdate) ) {
        ebkToUpdate->setPayload( ebkToUpdate->payload() + otherEBK->payload() );
        otherIndices[i] = j;
        foundEBKToUpdate = true;
        break;
      }
    } // End: Inner loop over contToUpdate
    if (!foundEBKToUpdate) {
      xAOD::CutBookkeeper* newEBK = new xAOD::CutBookkeeper();
      if ( newEBK->usingPrivateStore() ) { newEBK->releasePrivateStore(); }
      newEBK->makePrivateStore(otherEBK);
      contToUpdate->push_back( newEBK );
      std::size_t ebIdx = newEBK->index();
      otherIndices[i] = ebIdx;
    }
  } // End: Outer loop over contToUpdate

  // Now, we still need to fix the cross-referencing of the newly added CutBookkkeepers
  for ( std::size_t i=origSize; i<contToUpdate->size(); ++i ) {
    xAOD::CutBookkeeper* ebkToModify = contToUpdate->at(i);

    // Parent check
    if ( ebkToModify->hasParent() ) {
      const xAOD::CutBookkeeper* oldParent = ebkToModify->parent();
      const xAOD::CutBookkeeper* newParent = resolveLink (oldParent,
                                                          *contToUpdate,
                                                          *otherCont,
                                                          otherIndices);
      ebkToModify->setParent (newParent);
    } // Done fixing parent

    // Children check
    std::vector< xAOD::CutBookkeeper* > newChildren;
    for ( std::size_t oldIdx=0; oldIdx<ebkToModify->nChildren(); ++oldIdx ) {
      const xAOD::CutBookkeeper* oldEBK = ebkToModify->child(oldIdx);
      newChildren.push_back (resolveLink (oldEBK,
                                          *contToUpdate,
                                          *otherCont,
                                          otherIndices));
    } // Done fixing children
    ebkToModify->setChildren (newChildren);

    // Used others check
    std::vector< xAOD::CutBookkeeper* > newOthers;
    for ( std::size_t oldIdx=0; oldIdx<ebkToModify->nUsedOthers(); ++oldIdx ) {
      const xAOD::CutBookkeeper* oldEBK = ebkToModify->usedOther(oldIdx);
      newOthers.push_back (resolveLink (oldEBK,
                                        *contToUpdate,
                                        *otherCont,
                                        otherIndices));
    } // Done fixing used others
    ebkToModify->setUsedOthers (newOthers);

    // Siblings check
    std::vector< xAOD::CutBookkeeper* > newSiblings;
    for ( std::size_t oldIdx=0; oldIdx<ebkToModify->nSiblings(); ++oldIdx ) {
      const xAOD::CutBookkeeper* oldEBK = ebkToModify->sibling(oldIdx);
      newSiblings.push_back (resolveLink (oldEBK,
                                          *contToUpdate,
                                          *otherCont,
                                          otherIndices));
    } // Done fixing siblings
    ebkToModify->setSiblings (newSiblings);
  } // Done fixing all cross references
  return StatusCode::SUCCESS;
}

StatusCode BookkeeperTool::loadPOOLMetaData()
{
  // AnalysisBase can only use the xAOD::TruthMetaDataContainer, so skip this
#ifdef XAOD_STANDALONE
  return StatusCode::SUCCESS;
#else

  const IOVMetaDataContainer *container
    = inputMetaStore()->tryConstRetrieve<IOVMetaDataContainer>("/Generation/Parameters");
  if (container == nullptr) {
    // set variations to 1 and exit quietly...
    m_numberOfWeightVariations = 1;
    return StatusCode::SUCCESS;
  }
  // payload is a collection of CondAttrListCollections
  // only look a the first one, assuming it exists, and within that only look at the first channel;
  if (!(container->payloadContainer()->size() > 0
        && container->payloadContainer()->at(0)->size() > 0)) {
    return StatusCode::FAILURE;
  }

  // we take the first collection of weights
  // TODO: improve this
  CondAttrListCollection::ChanNum chanNum{container->payloadContainer()->at(0)->chanNum(0)};
  const coral::Attribute& attr
    = container->payloadContainer()->at(0)->attributeList(chanNum)["HepMCWeightNames"];

  ATH_MSG_DEBUG("Attempting to load weight meta data from /Generation/Parameters");
  std::map<std::string, int> truthWeightMap;
  ATH_CHECK(Gaudi::Parsers::parse(truthWeightMap, attr.data<std::string>()));

  m_numberOfWeightVariations = truthWeightMap.size();

  return StatusCode::SUCCESS;
#endif // XAOD_STANDALONE
}
