/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "CxxUtils/make_unique.h"
>>>>>>> release/21.0.127
#include "G4CosmicFilter/G4CosmicFilterTool.h"

namespace G4UA
{

  G4CosmicFilterTool::G4CosmicFilterTool(const std::string& type,
                                         const std::string& name,
                                         const IInterface* parent)
<<<<<<< HEAD
    : UserActionToolBase<G4CosmicFilter>(type, name, parent)
=======
    : ActionToolBaseReport<G4CosmicFilter>(type, name, parent),
      m_config()
>>>>>>> release/21.0.127
  {
    declareProperty("CollectionName", m_config.collectionName);
    declareProperty("PDGId", m_config.PDGId);
    declareProperty("PtMin", m_config.ptMin);
    declareProperty("PtMax", m_config.ptMax);
  }

<<<<<<< HEAD
  std::unique_ptr<G4CosmicFilter>
  G4CosmicFilterTool::makeAndFillAction(G4AtlasUserActions& actionList)
  {
    ATH_MSG_DEBUG("Constructing a G4CosmicFilter");
    auto action = std::make_unique<G4CosmicFilter>(m_config);
    actionList.eventActions.push_back( action.get() );
    return action;
=======
  std::unique_ptr<G4CosmicFilter> G4CosmicFilterTool::makeAction()
  {
    ATH_MSG_DEBUG("makeAction");
    auto action = CxxUtils::make_unique<G4CosmicFilter>(m_config);
    return std::move(action);
  }

  StatusCode G4CosmicFilterTool::queryInterface(const InterfaceID& riid, void** ppvIf)
  {
    if(riid == IG4EventActionTool::interfaceID()) {
      *ppvIf = (IG4EventActionTool*) this;
      addRef();
      return StatusCode::SUCCESS;
    } return ActionToolBase<G4CosmicFilter>::queryInterface(riid, ppvIf);
>>>>>>> release/21.0.127
  }

  StatusCode G4CosmicFilterTool::finalize()
  {
<<<<<<< HEAD
    // Accumulate results across threads
    G4CosmicFilter::Report report;
    m_actions.accumulate(report, &G4CosmicFilter::getReport,
                         &G4CosmicFilter::Report::merge);
    ATH_MSG_INFO("processed " << report.ntot << " events, " <<
                 report.npass << " events passed filter");
=======
    mergeReports();

    ATH_MSG_INFO( "processed "<< m_report.ntot <<" events, "<< m_report.npass<<" events passed filter" );
>>>>>>> release/21.0.127

    return StatusCode::SUCCESS;
  }

} // namespace G4UA
