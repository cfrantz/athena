/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "G4CosmicFilter/G4CosmicFilter.h"

#include "MCTruth/TrackHelper.h"
#include "TrackRecord/TrackRecordCollection.h"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "StoreGate/ReadHandle.h"

#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/IMessageSvc.h"

namespace G4UA
{

  G4CosmicFilter::G4CosmicFilter(const Config& config)
    : AthMessaging(Gaudi::svcLocator()->service< IMessageSvc >( "MessageSvc" ), "G4CosmicFilter"),
<<<<<<< HEAD
      m_config(config), m_report()
=======
      m_config(config), m_report(),
      m_evtStore("StoreGateSvc/StoreGateSvc","G4CosmicFilter"),
      m_detStore("StoreGateSvc/DetectorStore","G4CosmicFilter")
>>>>>>> release/21.0.127
  {
  }

  void G4CosmicFilter::EndOfEventAction(const G4Event*)
  {
    int counter(0);

    m_report.ntot++;

    SG::ReadHandle <TrackRecordCollection> coll(m_config.collectionName);
    if (! coll.isValid() )
      {
<<<<<<< HEAD
        ATH_MSG_WARNING( "Cannot retrieve TrackRecordCollection " << m_config.collectionName );
        G4RunManager::GetRunManager()->AbortEvent();
        return;
=======
	ATH_MSG_WARNING( "Cannot retrieve TrackRecordCollection " << m_config.collectionName );
	G4RunManager::GetRunManager()->AbortEvent();
	return;
>>>>>>> release/21.0.127
      }

    counter = coll->size();

    if (m_config.PDGId!=0 || m_config.ptMin>0 || m_config.ptMax>0)
      {
<<<<<<< HEAD
        counter=0;
        for (const auto& a_tr : *coll)
          {
            if (m_config.PDGId!=0 && m_config.PDGId != std::abs(a_tr.GetPDGCode())) continue;
            if (m_config.ptMin>0 && m_config.ptMin > a_tr.GetMomentum().perp() ) continue;
            if (m_config.ptMax>0 && m_config.ptMax < a_tr.GetMomentum().perp() ) continue;
            counter++;
          }
=======
	counter=0;
	for (const auto& a_tr : *coll)
	  {
	    if (m_config.PDGId!=0 && m_config.PDGId != fabs(a_tr.GetPDGCode())) continue;
	    if (m_config.ptMin>0 && m_config.ptMin > a_tr.GetMomentum().perp() ) continue;
	    if (m_config.ptMax>0 && m_config.ptMax < a_tr.GetMomentum().perp() ) continue;
	    counter++;
	  }
>>>>>>> release/21.0.127
      }

    //std::cout << "EndOfEventAction counter is "<<counter<<std::endl;
    if (counter==0)
      {
<<<<<<< HEAD
        ATH_MSG_INFO("aborting event due to failing filter");
        G4RunManager::GetRunManager()->AbortEvent();
        return;
=======
	ATH_MSG_INFO("aborting event due to failing filter");
	G4RunManager::GetRunManager()->AbortEvent();
	return;
>>>>>>> release/21.0.127
      }

    m_report.npass++;
    return;

  }

} // namespace G4UA
