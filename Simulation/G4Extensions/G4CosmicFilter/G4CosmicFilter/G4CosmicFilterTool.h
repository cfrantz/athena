/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef G4COSMICFILTER_G4UA__G4COSMICFILTERTOOL_H
#define G4COSMICFILTER_G4UA__G4COSMICFILTERTOOL_H

<<<<<<< HEAD
// Infrastructure includes
#include "G4AtlasTools/UserActionToolBase.h"

// Local includes
=======
#include "G4AtlasInterfaces/IG4EventActionTool.h"
#include "G4AtlasTools/ActionToolBase.h"
>>>>>>> release/21.0.127
#include "G4CosmicFilter/G4CosmicFilter.h"

namespace G4UA
{

<<<<<<< HEAD
  class G4CosmicFilterTool : public UserActionToolBase<G4CosmicFilter>
=======
  class G4CosmicFilterTool: public ActionToolBaseReport<G4CosmicFilter>,
                            public IG4EventActionTool
>>>>>>> release/21.0.127
  {

    public:

<<<<<<< HEAD
      /// Standard constructor
      G4CosmicFilterTool(const std::string& type, const std::string& name,
                         const IInterface* parent);

=======
      G4CosmicFilterTool(const std::string& type, const std::string& name,const IInterface* parent);

      virtual G4UserEventAction* getEventAction() override final
      { return static_cast<G4UserEventAction*>( getAction() ); }

      virtual StatusCode queryInterface(const InterfaceID& riid, void** ppvInterface) override;
>>>>>>> release/21.0.127
      virtual StatusCode finalize() override;

    protected:

<<<<<<< HEAD
      /// Create action for this thread
      virtual std::unique_ptr<G4CosmicFilter>
      makeAndFillAction(G4AtlasUserActions&) override final;
=======
      virtual std::unique_ptr<G4CosmicFilter> makeAction() override final;
>>>>>>> release/21.0.127

    private:

      G4CosmicFilter::Config m_config;

  }; // class G4CosmicFilterTool

} // namespace G4UA

#endif
