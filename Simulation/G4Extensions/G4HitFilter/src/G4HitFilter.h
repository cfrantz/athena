/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef G4HITFILTER_H
#define G4HITFILTER_H

<<<<<<< HEAD
=======


>>>>>>> release/21.0.127
#include <string>
#include <vector>
#include <map>

<<<<<<< HEAD
=======

>>>>>>> release/21.0.127
#include "G4UserEventAction.hh"
#include "G4UserRunAction.hh"
#include "AthenaBaseComps/AthMessaging.h"

#include "StoreGate/StoreGateSvc.h"
#include "GaudiKernel/ServiceHandle.h"

<<<<<<< HEAD
namespace G4UA
{
=======
namespace G4UA{
>>>>>>> release/21.0.127

  class G4HitFilter:
    public AthMessaging, public G4UserEventAction, public G4UserRunAction
  {

  public:

    struct Config
    {
      std::vector<std::string> volumenames;
    };

<<<<<<< HEAD
    /// Constructor
=======
>>>>>>> release/21.0.127
    G4HitFilter(const Config& config);

    struct Report
    {
<<<<<<< HEAD
      int ntot = 0;
      int npass = 0;
      void merge(const Report& rep){
        ntot += rep.ntot;
        npass += rep.npass;
=======
      int ntot=0;
      int npass=0;
      void merge(const Report& rep){

        ntot+=rep.ntot;
        npass+=rep.npass;

>>>>>>> release/21.0.127
      }
    };

    const Report& getReport() const
    { return m_report; }

    virtual void EndOfEventAction(const G4Event*) override;
    virtual void BeginOfRunAction(const G4Run*) override;
<<<<<<< HEAD

=======
>>>>>>> release/21.0.127
  private:

    enum hitCntainerTypes {
      CALOCALIB,
      CSC,
      LAR,
      LUCID,
      MDT,
      RPC,
      SI,
      TGC,
      TILE,
      TRT };

<<<<<<< HEAD
    Config m_config;
    Report m_report;

    /// A list of (hitContainerTypes, volumeName) pairs,
    /// filled in BeginOfRunAction.
    std::vector< std::pair<int,std::string> > m_hitContainers;

  }; // class G4HitFilter

} // namespace G4UA

=======

    Config m_config;
    Report m_report;

    typedef ServiceHandle<StoreGateSvc> StoreGateSvc_t;
    /// Pointer to StoreGate (event store by default)
    mutable StoreGateSvc_t m_evtStore;
    /// Pointer to StoreGate (detector store by default)
    mutable StoreGateSvc_t m_detStore;
    std::vector<std::pair<int,std::string> > m_hitContainers;
  }; // class G4HitFilter


} // namespace G4UA




>>>>>>> release/21.0.127
#endif
