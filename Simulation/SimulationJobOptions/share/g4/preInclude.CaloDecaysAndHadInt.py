###############################################################################
#
# Fragment to add calorimeter decays and hadronic interactions to the truth tree
#
# Implemented for the B-physics folks who want to estimate fakes well
# Results in a pretty major blow-up of hit file size - use with care!
#
###############################################################################

def add_calo_decay_truth_strategies():
<<<<<<< HEAD
    printfunc ("ERROR add_calo_decay_truth_strategies is obsolete")
    printfunc ("Please request a replacement configuration")
=======
    print "ERROR add_calo_decay_truth_strategies is obsolete"
    print "Please request a replacement configuration"
>>>>>>> release/21.0.127
    import sys
    sys.exit(1)
    ## ## Modifying truth strategies as requested by e/gamma group
    ## from G4AtlasApps import AtlasG4Eng
    ## AtlasG4Eng.G4Eng._ctrl.mctruthMenu.listStrategies()
    ## AtlasG4Eng.G4Eng._ctrl.mctruthMenu.activateStrategy('Decay', 'CALO::CALO', 1)
    ## AtlasG4Eng.G4Eng._ctrl.mctruthMenu.activateStrategy('HadronicInteraction', 'CALO::CALO',1)


## Register the callback
simFlags.InitFunctions.add_function("postInit", add_calo_decay_truth_strategies)
