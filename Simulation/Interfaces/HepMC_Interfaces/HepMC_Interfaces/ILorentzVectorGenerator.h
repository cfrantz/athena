/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// ILorentzVectorGenerator.h, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#ifndef HEPMC_INTERFACES_ILORENTZVECTORGENERATOR_H
#define HEPMC_INTERFACES_ILORENTZVECTORGENERATOR_H 1

// Gaudi
#include "GaudiKernel/IAlgTool.h"

// forward declarations
namespace CLHEP {
  class HepLorentzVector;
}

namespace Simulation {

<<<<<<< HEAD:Simulation/Interfaces/HepMC_Interfaces/HepMC_Interfaces/ILorentzVectorGenerator.h
=======
  static const InterfaceID IID_ILorentzVectorGenerator("ILorentzVectorGenerator", 1, 0);

>>>>>>> release/21.0.127:Simulation/ISF/ISF_HepMC/ISF_HepMC_Interfaces/ISF_HepMC_Interfaces/ILorentzVectorGenerator.h
  /**
   @class ILorentzVectorGenerator

   Interface definition for an AthenaTool creating a HepLorentzVector

   @author Elmar.Ritsch -at- cern.ch
   */

  class ILorentzVectorGenerator : virtual public IAlgTool {
     public:
       /** Virtual destructor */
       virtual ~ILorentzVectorGenerator(){}

<<<<<<< HEAD:Simulation/Interfaces/HepMC_Interfaces/HepMC_Interfaces/ILorentzVectorGenerator.h
       /// Creates the InterfaceID and interfaceID() method
       DeclareInterfaceID(ILorentzVectorGenerator, 1, 0);
=======
       /** AlgTool interface methods */
       static const InterfaceID& interfaceID() { return IID_ILorentzVectorGenerator; }
>>>>>>> release/21.0.127:Simulation/ISF/ISF_HepMC/ISF_HepMC_Interfaces/ISF_HepMC_Interfaces/ILorentzVectorGenerator.h

       /** Returns a HepLorentzVector */
       virtual CLHEP::HepLorentzVector* generate() const = 0;
  };

} // end of namespace

#endif // HEPMC_INTERFACES_ILORENTZVECTORGENERATOR_H
