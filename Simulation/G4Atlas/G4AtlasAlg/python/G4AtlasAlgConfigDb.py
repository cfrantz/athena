# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

<<<<<<< HEAD
from AthenaCommon.CfgGetter import addAlgorithm

=======
from AthenaCommon.CfgGetter import addAlgorithm, addTool


addTool("G4AtlasAlg.G4AtlasAlgConfig.getAthenaTrackingActionTool",
        "G4UA::AthenaTrackingActionTool")
addTool("G4AtlasAlg.G4AtlasAlgConfig.getAthenaStackingActionTool",
        "G4UA::AthenaStackingActionTool")

>>>>>>> release/21.0.127
# Main Algorithm for AtlasG4
addAlgorithm("G4AtlasAlg.G4AtlasAlgConfig.getG4AtlasAlg", "G4AtlasAlg")
