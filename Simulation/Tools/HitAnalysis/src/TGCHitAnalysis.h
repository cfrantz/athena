/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
=======
Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#ifndef TGC_HIT_ANALYSIS_H
#define TGC_HIT_ANALYSIS_H

#include "AthenaBaseComps/AthAlgorithm.h"

#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"

#include <string>
#include <vector>
#include "TH1.h"
#include "TH2.h"
#include "TTree.h"

class TH1;
class TH2;
class TTree;


class TGCHitAnalysis : public AthAlgorithm {

 public:

   TGCHitAnalysis(const std::string& name, ISvcLocator* pSvcLocator);
<<<<<<< HEAD
   virtual ~TGCHitAnalysis(){}

   virtual StatusCode initialize() override;
   virtual StatusCode execute() override;
=======
   ~TGCHitAnalysis(){}

   virtual StatusCode initialize();
   virtual StatusCode execute();
>>>>>>> release/21.0.127

 private:

   /** Some variables**/
<<<<<<< HEAD
   TH1* m_h_hits_x;
   TH1* m_h_hits_y;
   TH1* m_h_hits_z;
   TH1* m_h_hits_r;
   TH2* m_h_xy;
   TH2* m_h_rz;
   TH1* m_h_hits_eta;
   TH1* m_h_hits_phi;
   TH1* m_h_hits_lx;
   TH1* m_h_hits_ly;
   TH1* m_h_hits_lz;
   TH1* m_h_hits_dcx;
   TH1* m_h_hits_dcy;
   TH1* m_h_hits_dcz;
   TH1* m_h_hits_time;
   TH1* m_h_hits_edep;
   TH1* m_h_hits_kine;
   TH1* m_h_hits_step;
=======
   TH1* h_hits_x;
   TH1* h_hits_y;
   TH1* h_hits_z;
   TH1* h_hits_r;
   TH2* h_xy;
   TH2* h_rz;
   TH1* h_hits_eta;
   TH1* h_hits_phi;
   TH1* h_hits_lx;
   TH1* h_hits_ly;
   TH1* h_hits_lz;
   TH1* h_hits_dcx;
   TH1* h_hits_dcy;
   TH1* h_hits_dcz;
   TH1* h_hits_time;
   TH1* h_hits_edep;
   TH1* h_hits_kine;
   TH1* h_hits_step;
>>>>>>> release/21.0.127

   std::vector<float>* m_hits_x;
   std::vector<float>* m_hits_y;
   std::vector<float>* m_hits_z;
   std::vector<float>* m_hits_r;
   std::vector<float>* m_hits_eta;
   std::vector<float>* m_hits_phi;
   std::vector<float>* m_hits_lx;
   std::vector<float>* m_hits_ly;
   std::vector<float>* m_hits_lz;
   std::vector<float>* m_hits_dcx;
   std::vector<float>* m_hits_dcy;
   std::vector<float>* m_hits_dcz;
   std::vector<float>* m_hits_time;
   std::vector<float>* m_hits_edep;
   std::vector<float>* m_hits_kine;
   std::vector<float>* m_hits_step;
   
   TTree * m_tree;
   std::string m_ntupleFileName; 
   std::string m_path;
   ServiceHandle<ITHistSvc>  m_thistSvc;

};

#endif // TGC_HIT_ANALYSIS_H
