/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

//////////////////////////////////////////////////////////////////////////
// Oleg.Fedin@cern.ch, August 2010
//////////////////////////////////////////////////////////////////////////
#include "McEventCollectionFilter.h"
//
<<<<<<< HEAD
#include "AtlasHepMC/GenEvent.h"
#include "AtlasHepMC/GenVertex.h"
#include "AtlasHepMC/GenParticle.h"
#include "AtlasHepMC/Flow.h"
#include "AtlasHepMC/Polarization.h"
//
#include "InDetSimEvent/SiHit.h"
#include "MuonSimEvent/TGCSimHit.h"
#include "MuonSimEvent/CSCSimHit.h"
#include "MuonSimEvent/sTGCSimHit.h"
#include "MuonSimEvent/MMSimHit.h"
=======
#include "HepMC/GenEvent.h"
#include "HepMC/GenVertex.h"
#include "GeneratorObjects/McEventCollection.h"
//
#include "InDetSimEvent/SiHitCollection.h"
#include "InDetSimEvent/SiHit.h"
#include "InDetSimEvent/TRTUncompressedHitCollection.h"
#include "MuonSimEvent/MDTSimHitCollection.h"
#include "MuonSimEvent/RPCSimHitCollection.h"
#include "MuonSimEvent/TGCSimHitCollection.h"
#include "MuonSimEvent/CSCSimHitCollection.h"
#include "MuonSimEvent/TGCSimHit.h"
#include "MuonSimEvent/CSCSimHit.h"
>>>>>>> release/21.0.127
// CLHEP
#include "CLHEP/Vector/LorentzVector.h"
#include "CLHEP/Units/SystemOfUnits.h"
//
#include "CLHEP/Geometry/Point3D.h"
#include "GeoPrimitives/GeoPrimitives.h"
#include <climits>

McEventCollectionFilter::McEventCollectionFilter(const std::string& name, ISvcLocator* pSvcLocator)
  : AthAlgorithm(name, pSvcLocator)
  , m_inputTruthCollection("StoreGateSvc+TruthEventOLD")
  , m_inputBCMHits("StoreGateSvc+BCMHitsOLD")
  , m_inputPixelHits("StoreGateSvc+PixelHitsOLD")
  , m_inputSCTHits("StoreGateSvc+SCT_HitsOLD")
  , m_inputTRTHits("StoreGateSvc+TRTUncompressedHitsOLD")
  , m_inputCSCHits("StoreGateSvc+CSC_HitsOLD")
  , m_inputMDTHits("StoreGateSvc+MDT_HitsOLD")
  , m_inputRPCHits("StoreGateSvc+RPC_HitsOLD")
  , m_inputTGCHits("StoreGateSvc+TGC_HitsOLD")
  , m_inputSTGCHits("StoreGateSvc+sTGCSensitiveDetectorOLD")
  , m_inputMMHits("StoreGateSvc+MicromegasSensitiveDetectorOLD")
  , m_outputTruthCollection("StoreGateSvc+TruthEvent")
  , m_outputBCMHits("StoreGateSvc+BCMHits")
  , m_outputPixelHits("StoreGateSvc+PixelHits")
  , m_outputSCTHits("StoreGateSvc+SCT_Hits")
  , m_outputTRTHits("StoreGateSvc+TRTUncompressedHits")
  , m_outputCSCHits("StoreGateSvc+CSC_Hits")
  , m_outputMDTHits("StoreGateSvc+MDT_Hits")
  , m_outputRPCHits("StoreGateSvc+RPC_Hits")
  , m_outputTGCHits("StoreGateSvc+TGC_Hits")
  , m_outputSTGCHits("StoreGateSvc+sTGCSensitiveDetector")
  , m_outputMMHits("StoreGateSvc+MicromegasSensitiveDetector")
  , m_IsKeepTRTElect(false)
  , m_PileupPartPDGID(999) //Geantino
  , m_UseTRTHits(true)
  , m_UseCSCHits(true) // On unless RUN3 symmetric layout
  , m_UseSTGCHits(false) // Off unless RUN3 layout
  , m_UseMMHits(false) // Off unless RUN3 layout
  , m_UseBCMHits(true) //On unless RUN4 layout
  , m_RefBarcode(0)
{
  declareProperty("TruthInput"        , m_inputTruthCollection);
  declareProperty("TruthOutput"       , m_outputTruthCollection);
  declareProperty("BCMHitsInput"      , m_inputBCMHits);
  declareProperty("BCMHitsOutput"     , m_outputBCMHits);
  declareProperty("PixelHitsInput"    , m_inputPixelHits);
  declareProperty("PixelHitsOutput"   , m_outputPixelHits);
  declareProperty("SCTHitsInput"      , m_inputSCTHits);
  declareProperty("SCTHitsOutput"     , m_outputSCTHits);
  declareProperty("TRTHitsInput"      , m_inputTRTHits);
  declareProperty("TRTHitsOutput"     , m_outputTRTHits);
  declareProperty("CSCHitsInput"      , m_inputCSCHits);
  declareProperty("CSCHitsOutput"     , m_outputCSCHits);
  declareProperty("MDTHitsInput"      , m_inputMDTHits);
  declareProperty("MDTHitsOutput"     , m_outputMDTHits);
  declareProperty("RPCHitsInput"      , m_inputRPCHits);
  declareProperty("RPCHitsOutput"     , m_outputRPCHits);
  declareProperty("TGCHitsInput"      , m_inputTGCHits);
  declareProperty("TGCHitsOutput"     , m_outputTGCHits);
  declareProperty("sTGCHitsInput"     , m_inputSTGCHits);
  declareProperty("sTGCHitsOutput"    , m_outputSTGCHits);
  declareProperty("MMHitsInput"       , m_inputMMHits);
  declareProperty("MMHitsOutput"      , m_outputMMHits);
  declareProperty("IsKeepTRTElect"    , m_IsKeepTRTElect);
  declareProperty("PileupPartPDGID"   , m_PileupPartPDGID);
  declareProperty("UseTRTHits"        , m_UseTRTHits);
  declareProperty("UseCSCHits"        , m_UseCSCHits);
  declareProperty("UseSTGCHits"       , m_UseSTGCHits);
  declareProperty("UseMMHits"         , m_UseMMHits);
  declareProperty("UseBCMHits"        , m_UseBCMHits);

}


//-----------------------------------------------------
McEventCollectionFilter::~McEventCollectionFilter(){
  //----------------------------------------------------
}

//----------------------------------------------------
StatusCode McEventCollectionFilter::initialize(){
  //----------------------------------------------------

  return StatusCode::SUCCESS;

}

//-------------------------------------------------
StatusCode McEventCollectionFilter::execute(){
<<<<<<< HEAD
  //-------------------------------------------------
=======
//-------------------------------------------------
>>>>>>> release/21.0.127

  ATH_MSG_DEBUG( " execute..... " );

  //... to find  electron barcodes linked to TRT hists
  if(m_IsKeepTRTElect&&m_UseTRTHits) {
    ATH_CHECK( FindTRTElectronHits() );
  }

  //.......Reduce McEventCollection
  ATH_CHECK( ReduceMCEventCollection() );

<<<<<<< HEAD
  //.......to relink all Pixel/SCT hits to the new particle
  ATH_CHECK( SiliconHitsTruthRelink() );

  //.......to relink all TRT hits to the new particle
  if(m_UseTRTHits) {
    ATH_CHECK( TRTHitsTruthRelink() );
  }

  //.......to relink all MDT hits to the new particle
  ATH_CHECK( MDTHitsTruthRelink() );

  //.......to relink all CSC hits to the new particle
  if(m_UseCSCHits) {
    ATH_CHECK( CSCHitsTruthRelink() );
  }

  //.......to relink all RPC hits to the new particle
  ATH_CHECK( RPCHitsTruthRelink() );

  //.......to relink all TGC hits to the new particle
  ATH_CHECK( TGCHitsTruthRelink() );

  //.......to relink all sTGC hits to the new particle
  if(m_UseSTGCHits) {
  ATH_CHECK( STGC_HitsTruthRelink() );
  }

  //.......to relink all MM hits to the new particle
  if(m_UseMMHits) {
    ATH_CHECK( MM_HitsTruthRelink() );
  }
  
  //.......to relink all BCM hits to the new particle
  if(m_UseBCMHits) {
    ATH_CHECK( BCMHitsTruthRelink() );
  }

=======
  //.......to relink all Si hits to the new particle
  ATH_CHECK( SiHistsTruthRelink() );

  //.......to relink all TRT hits to the new particle
  if(m_UseTRTHits) {
    ATH_CHECK( TRTHistsTruthRelink() );
  }

  //.......to relink all MDT hits to the new particle
  ATH_CHECK( MDTHistsTruthRelink() );

  //.......to relink all CSC hits to the new particle
  ATH_CHECK( CSCHistsTruthRelink() );

  //.......to relink all RPC hits to the new particle
  ATH_CHECK( RPCHistsTruthRelink() );

  //.......to relink all TGC hits to the new particle
  ATH_CHECK( TGCHistsTruthRelink() );

>>>>>>> release/21.0.127
  ATH_MSG_DEBUG( "succeded McEventCollectionFilter ..... " );

  return StatusCode::SUCCESS;

}

//-------------------------------------------------
StatusCode McEventCollectionFilter::finalize(){
<<<<<<< HEAD
  //-------------------------------------------------
  //
=======
//-------------------------------------------------
//
>>>>>>> release/21.0.127
  ATH_MSG_DEBUG( "McEventCollectionFilter:: finalize completed successfully" );
  return StatusCode::SUCCESS;

}
//----------------------------------------------------------------
StatusCode McEventCollectionFilter::ReduceMCEventCollection(){
<<<<<<< HEAD
  //----------------------------------------------------------------
  //.......to reduce McEventCollection for pileup  particles
  //----------------------------------------------------------------
  //
  if(!m_inputTruthCollection.isValid())
    {
      ATH_MSG_ERROR( "Could not find McEventCollection");
      return StatusCode::FAILURE;
    }
  ATH_MSG_DEBUG( "Found McEventCollection");
=======
//----------------------------------------------------------------
//.......to reduce McEventCollection for pileup  particles
//----------------------------------------------------------------
//
  // ....... Retrieve MC truht collection
  const McEventCollection* pMcEvtColl=0;
  ATH_CHECK( evtStore()->retrieve(pMcEvtColl,m_mcEventCollection) );
>>>>>>> release/21.0.127

  if (!m_outputTruthCollection.isValid()) m_outputTruthCollection = std::make_unique<McEventCollection>();

  //.......Create new particle (geantino) to link  hits from pileup
  HepMC::GenParticlePtr genPart=HepMC::newGenParticlePtr();
  genPart->set_pdg_id(m_PileupPartPDGID); //Geantino
  genPart->set_status(1); //!< set decay status
  HepMC::suggest_barcode(genPart, std::numeric_limits<int32_t>::max() );

  HepMC::GenVertexPtr genVertex = HepMC::newGenVertexPtr();
  genVertex->add_particle_out(genPart);

  const HepMC::GenEvent* genEvt = *(m_inputTruthCollection->begin());

  //......copy GenEvent to the new one and remove all vertex
  HepMC::GenEvent* evt=new HepMC::GenEvent(*genEvt);


  //to set geantino vertex as a truth primary vertex
  HepMC::ConstGenVertexPtr hScatVx = HepMC::barcode_to_vertex(genEvt,-3);
  if(hScatVx!=nullptr) {
    HepMC::FourVector pmvxpos=hScatVx->position();
    genVertex->set_position(pmvxpos);
    //to set geantino kinematic phi=eta=0, E=p=E_hard_scat
#ifdef HEPMC3 
    auto itrp = hScatVx->particles_in().begin();
    if (hScatVx->particles_in().size()==2){
#else
    HepMC::GenVertex::particles_in_const_iterator itrp =hScatVx->particles_in_const_begin();
    if (hScatVx->particles_in_size()==2){
#endif
      HepMC::FourVector mom1=(*itrp)->momentum();
      HepMC::FourVector mom2=(*(++itrp))->momentum();
      HepMC::FourVector vxmom;
      vxmom.setPx(mom1.e()+mom2.e());
      vxmom.setPy(0.);
      vxmom.setPz(0.);
      vxmom.setE(mom1.e()+mom2.e());

      genPart->set_momentum(vxmom);
    }
  }

#ifdef HEPMC3 
  if(!evt->vertices().empty()) for (auto vtx: evt->vertices()) evt->remove_vertex(vtx);
#else
  if(!evt->vertices_empty()){
    HepMC::GenEvent::vertex_iterator itvtx = evt->vertices_begin();
    for (;itvtx != evt ->vertices_end(); ++itvtx ) {
      HepMC::GenVertexPtr vtx = *itvtx++;
      evt->remove_vertex(vtx);
      delete vtx;
    }
  }
#endif

  //--------------------------------------
  if(m_IsKeepTRTElect){
    for(int i=0;i<(int) m_elecBarcode.size();i++){
<<<<<<< HEAD
      HepMC::ConstGenParticlePtr thePart=HepMC::barcode_to_particle(genEvt,m_elecBarcode[i]);
=======
      HepMC::GenParticle* thePart=genEvt->barcode_to_particle(m_elecBarcode[i]);
>>>>>>> release/21.0.127
      if (!thePart){
        ATH_MSG_DEBUG( "Could not find particle for barcode " << m_elecBarcode[i] );
        continue;
      }
<<<<<<< HEAD
      HepMC::ConstGenVertexPtr vx = thePart->production_vertex();
      HepMC::GenParticlePtr thePart_new = HepMC::newGenParticlePtr( thePart->momentum(),thePart->pdg_id(),
                                                                    thePart->status());
      HepMC::suggest_barcode(thePart_new, m_elecBarcode[i]);

      const HepMC::FourVector& pos= vx->position();
      HepMC::GenVertexPtr vx_new = HepMC::newGenVertexPtr(pos);
=======
      const HepMC::GenVertex* vx = thePart->production_vertex();
      HepMC::GenParticle* thePart_new=new HepMC::GenParticle( thePart->momentum(),thePart->pdg_id(),
                                                              thePart->status(),thePart->flow(),
                                                              thePart->polarization() );
      thePart_new->suggest_barcode(m_elecBarcode[i]);

      HepMC::FourVector pos= vx->position();
      HepMC::GenVertex* vx_new=new HepMC::GenVertex(pos);
>>>>>>> release/21.0.127
      vx_new->add_particle_out(thePart_new);
      evt->add_vertex(vx_new);
    }
  }

  //.....add new vertex with geantino
  evt->add_vertex(genVertex);
<<<<<<< HEAD
  m_RefBarcode=HepMC::barcode(genPart);

  m_outputTruthCollection->push_back(evt);
=======
  m_RefBarcode=genPart->barcode();

  pMcEvtCollNew->push_back(evt);


  //......remove old McEventCollection
  ATH_CHECK( evtStore()->remove(pMcEvtColl) );

  //......write new McEventCollection to SG
  ATH_CHECK( evtStore()->record(pMcEvtCollNew, m_mcEventCollection) );
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;

}
//--------------------------------------------------------
<<<<<<< HEAD
StatusCode McEventCollectionFilter::SiliconHitsTruthRelink(){
  //--------------------------------------------------------
  //.......to relink all Pixel/SCT hits to the new particle
  //--------------------------------------------------------
  //
  if(!m_inputPixelHits.isValid())
    {
      ATH_MSG_ERROR( "Could not find Pixel SiHitCollection");
      return StatusCode::FAILURE;
    }
  ATH_MSG_DEBUG( "Found Pixel SiHitCollection");
=======
StatusCode McEventCollectionFilter::SiHistsTruthRelink(){
//--------------------------------------------------------
//.......to relink all Si hits to the new particle
//--------------------------------------------------------
//
  std::vector <std::string> m_HitContainer;
  m_HitContainer.push_back("PixelHits");
  m_HitContainer.push_back("SCT_Hits");
  m_HitContainer.push_back("BCMHits");
  //??? m_HitContainer.push_back("BLMHits");

  for (unsigned int iHitContainer=0;iHitContainer<m_HitContainer.size();iHitContainer++){

    //.......retrive SiHit collection
    const DataHandle<SiHitCollection> pSiHitColl;
    if(evtStore()->contains<SiHitCollection>(m_HitContainer[iHitContainer])) {
      ATH_CHECK( evtStore()->retrieve(pSiHitColl,m_HitContainer[iHitContainer] ) );
    } else {
      ATH_MSG_ERROR( "Could not find SiHitCollection containing " <<  m_HitContainer[iHitContainer] );
      return StatusCode::FAILURE;
    }

    SiHitCollection * pSiHitC = const_cast<SiHitCollection *> (&*pSiHitColl);

    //.......create new SiHit collection and copy all hits with the new HepMcParticleLink
    SiHitCollection *pSiHitCollNew = new SiHitCollection();

    for (SiHitCollection::const_iterator i = pSiHitC->begin(); i != pSiHitC->end(); ++i) {
      const HepMcParticleLink McLink = (*i).particleLink();


      HepGeom::Point3D<double>   lP1  = (*i).localStartPosition();
      HepGeom::Point3D<double>   lP2  = (*i).localEndPosition();
      double       edep = (*i).energyLoss();
      double       mt   = (*i).meanTime();
      unsigned int id   = (*i).identify();

      int CurBarcode=0;
      if(McLink.barcode()!=0)  CurBarcode=m_RefBarcode;
>>>>>>> release/21.0.127

  if (!m_outputPixelHits.isValid()) m_outputPixelHits = std::make_unique<SiHitCollection>();

  ATH_CHECK(this->SiHitsTruthRelink(m_inputPixelHits,m_outputPixelHits));

<<<<<<< HEAD
  if(!m_inputSCTHits.isValid())
    {
      ATH_MSG_ERROR( "Could not find SCT SiHitCollection");
      return StatusCode::FAILURE;
    }
  ATH_MSG_DEBUG( "Found SCT SiHitCollection");

  if (!m_outputSCTHits.isValid()) m_outputSCTHits = std::make_unique<SiHitCollection>();
=======
    //......remove old SiHitCollection
    ATH_CHECK( evtStore()->remove(pSiHitC) );

    //......write new SiHitCollection
    ATH_CHECK( evtStore()->record(pSiHitCollNew,m_HitContainer[iHitContainer]) );
>>>>>>> release/21.0.127

  ATH_CHECK(this->SiHitsTruthRelink(m_inputSCTHits,m_outputSCTHits));

  return StatusCode::SUCCESS;
}

//--------------------------------------------------------
<<<<<<< HEAD
StatusCode McEventCollectionFilter::BCMHitsTruthRelink(){
  //--------------------------------------------------------
  //.......to relink BCM hits to the new particle
  //--------------------------------------------------------
  //

  if(!m_inputBCMHits.isValid())
    {
      ATH_MSG_ERROR( "Could not find BCM SiHitCollection");
      return StatusCode::FAILURE;
    }
  ATH_MSG_DEBUG( "Found BCM SiHitCollection");
=======
StatusCode McEventCollectionFilter::TRTHistsTruthRelink(){
//--------------------------------------------------------
//.......to relink all TRT hits to the new particle
//--------------------------------------------------------
//
  //.......retrive TRTUncompressedHitCollection collection
  m_HitName  = "TRTUncompressedHits";
  const DataHandle<TRTUncompressedHitCollection> pTRTHitColl;

  if(evtStore()->contains<TRTUncompressedHitCollection>(m_HitName)) {
    ATH_CHECK( evtStore()->retrieve(pTRTHitColl, m_HitName) );
  } else {
    ATH_MSG_ERROR( "Could not find collection containing " << m_HitName );
    return StatusCode::FAILURE;
  }

  ATH_MSG_DEBUG( "Found collection containing " << m_HitName );

  TRTUncompressedHitCollection*  pTRTHitC= const_cast<TRTUncompressedHitCollection*> (&*pTRTHitColl);
>>>>>>> release/21.0.127

  if (!m_outputBCMHits.isValid()) m_outputBCMHits = std::make_unique<SiHitCollection>();

<<<<<<< HEAD
  ATH_CHECK(this->SiHitsTruthRelink(m_inputBCMHits,m_outputBCMHits));

  return StatusCode::SUCCESS;
}

StatusCode McEventCollectionFilter::SiHitsTruthRelink(SG::ReadHandle<SiHitCollection>& inputHits, SG::WriteHandle<SiHitCollection>& outputHits){
  for (SiHitCollection::const_iterator i = inputHits->begin(); i != inputHits->end(); ++i) {
    const HepMcParticleLink oldLink = (*i).particleLink();


    HepGeom::Point3D<double>   lP1  = (*i).localStartPosition();
    HepGeom::Point3D<double>   lP2  = (*i).localEndPosition();
    double       edep = (*i).energyLoss();
    double       mt   = (*i).meanTime();
    unsigned int id   = (*i).identify();

    int curBarcode=0;
    if(oldLink.barcode()!=0)  curBarcode=m_RefBarcode;
    HepMcParticleLink partLink(curBarcode, oldLink.eventIndex(), oldLink.getEventCollection());
    outputHits->Emplace(lP1,lP2, edep, mt,curBarcode , id);
  }
=======
    TRTUncompressedHit newTRTHit(id,CurBarcode,pdgID,kinEnergy,eneDeposit,preX,preY,preZ,postX,postY,postZ,time);
    pTRTHitCollNew->Insert(newTRTHit);

  }

  //.......remove old TRTUncompressedHitCollection
  ATH_CHECK( evtStore()->remove(pTRTHitC) );

  //.......write new  TRTUncompressedHitCollection
  ATH_CHECK( evtStore()->record(pTRTHitCollNew,m_HitName) );
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}
<<<<<<< HEAD

//--------------------------------------------------------
StatusCode McEventCollectionFilter::TRTHitsTruthRelink()
{
  //--------------------------------------------------------
  //.......to relink all TRT hits to the new particle
  //--------------------------------------------------------
  //
  if(!m_inputTRTHits.isValid())
    {
      ATH_MSG_ERROR( "Could not find TRTUncompressedHitsCollection");
      return StatusCode::FAILURE;
    }
  ATH_MSG_DEBUG( "Found TRTUncompressedHitsCollection");

  if (!m_outputTRTHits.isValid()) m_outputTRTHits = std::make_unique<TRTUncompressedHitCollection>();
  for (TRTUncompressedHitCollection::const_iterator i = m_inputTRTHits->begin(); i != m_inputTRTHits->end(); ++i)
    {

      const HepMcParticleLink oldLink = (*i).particleLink();

      int   pdgID = (*i).GetParticleEncoding();
      int curBarcode=oldLink.barcode();
      if(curBarcode!=0)
        {
          if(!(m_IsKeepTRTElect && std::abs(pdgID)==11))
            {
              curBarcode=m_RefBarcode;
            }
        }
      HepMcParticleLink partLink(curBarcode, oldLink.eventIndex(), oldLink.getEventCollection());
      int   id         = (*i).GetHitID();
      float kinEnergy  = (*i).GetKineticEnergy();
      float eneDeposit = (*i).GetEnergyDeposit();
      float preX       = (*i).GetPreStepX();
      float preY       = (*i).GetPreStepY();
      float preZ       = (*i).GetPreStepZ();
      float postX      = (*i).GetPostStepX();
      float postY      = (*i).GetPostStepY() ;
      float postZ      = (*i).GetPostStepZ();
      float time       = (*i).GetGlobalTime();

      m_outputTRTHits->Emplace(id,partLink,pdgID,kinEnergy,eneDeposit,preX,preY,preZ,postX,postY,postZ,time);
    }
=======
//--------------------------------------------------------
StatusCode McEventCollectionFilter::MDTHistsTruthRelink(){
//--------------------------------------------------------
//.......to relink all MDT hits to the new particle
//--------------------------------------------------------

  m_HitName="MDT_Hits";
  const DataHandle<MDTSimHitCollection> pMDTHitColl;

  if(evtStore()->contains<MDTSimHitCollection>(m_HitName)) {
    ATH_CHECK( evtStore()->retrieve(pMDTHitColl, m_HitName) );
  } else {
    ATH_MSG_ERROR( "Could not find MDTSimHitCollection  containing " << m_HitName );
    return StatusCode::FAILURE;
  }


  MDTSimHitCollection* pMDTHitC = const_cast<MDTSimHitCollection*> (&*pMDTHitColl);
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}

//--------------------------------------------------------
StatusCode McEventCollectionFilter::MDTHitsTruthRelink(){
  //--------------------------------------------------------
  //.......to relink all MDT hits to the new particle
  //--------------------------------------------------------
  if(!m_inputMDTHits.isValid())
    {
      ATH_MSG_ERROR( "Could not find MDTSimHitCollection");
      return StatusCode::FAILURE;
    }
  ATH_MSG_DEBUG( "Found MDTSimHitCollection");

  if (!m_outputMDTHits.isValid()) m_outputMDTHits = std::make_unique<MDTSimHitCollection>();
  for(MDTSimHitConstIterator i=m_inputMDTHits->begin();i!=m_inputMDTHits->end();++i){

    const HepMcParticleLink oldLink = (*i).particleLink();
    int curBarcode=0;
    if(oldLink.barcode()!=0)  curBarcode=m_RefBarcode;
    HepMcParticleLink partLink(curBarcode, oldLink.eventIndex(), oldLink.getEventCollection());
    int            id = (*i).MDTid();
    double       time = (*i).globalTime();
    double     radius = (*i).driftRadius();
    Amg::Vector3D  lP = (*i).localPosition();
    //int  trackNumber = (*i).trackNumber();
    double stepLength = (*i).stepLength();
    double eneDeposit = (*i).energyDeposit();
    int         pdgID = (*i).particleEncoding();
    double  kinEnergy = (*i).kineticEnergy();

<<<<<<< HEAD
    m_outputMDTHits->Emplace(id,time,radius,lP,partLink,stepLength,eneDeposit,pdgID,kinEnergy);
  }
=======

    MDTSimHit newMDTHit(id,time,radius,lP,CurBarcode,stepLength,eneDeposit,pdgID,kinEnergy);
    pMDTHitCollNew->Insert(newMDTHit);
  }

  //.......remove old MDTSimHitCollection
  ATH_CHECK( evtStore()->remove(pMDTHitC) );

  //.......write new  MDTSimHitCollection
  ATH_CHECK( evtStore()->record(pMDTHitCollNew,m_HitName) );

  return StatusCode::SUCCESS;
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}

<<<<<<< HEAD
//--------------------------------------------------------
StatusCode McEventCollectionFilter::CSCHitsTruthRelink(){
  //--------------------------------------------------------
  //.......to relink all CSC hits to the new particle
  //--------------------------------------------------------
  if(!m_inputCSCHits.isValid())
    {
      ATH_MSG_ERROR( "Could not find CSCSimHitCollection");
      return StatusCode::FAILURE;
    }
  ATH_MSG_DEBUG( "Found CSCSimHitCollection");
=======
  m_HitName="CSC_Hits";
  const DataHandle<CSCSimHitCollection> pCSCHitColl;

  if(evtStore()->contains<CSCSimHitCollection>(m_HitName)) {
    ATH_CHECK( evtStore()->retrieve(pCSCHitColl, m_HitName) );
  } else {
    ATH_MSG_ERROR( "Could not find CSCSimHitCollection  containing " << m_HitName );
    return StatusCode::FAILURE;
  }


  CSCSimHitCollection* pCSCHitC = const_cast<CSCSimHitCollection*> (&*pCSCHitColl);

  //.......Create new CSCSimHitCollection
  CSCSimHitCollection* pCSCHitCollNew = new CSCSimHitCollection();
>>>>>>> release/21.0.127

  if (!m_outputCSCHits.isValid()) m_outputCSCHits = std::make_unique<CSCSimHitCollection>();
  for(CSCSimHitConstIterator i=m_inputCSCHits->begin();i!=m_inputCSCHits->end();++i){

    const HepMcParticleLink oldLink = (*i).particleLink();
    int curBarcode=0;
    if(oldLink.barcode()!=0)  curBarcode=m_RefBarcode;
    HepMcParticleLink partLink(curBarcode, oldLink.eventIndex(), oldLink.getEventCollection());
    int              id = (*i).CSCid();
    double         time = (*i).globalTime();
    double   eneDeposit = (*i).energyDeposit();
    Amg::Vector3D HitStart = (*i).getHitStart();
    Amg::Vector3D HitEnd = (*i).getHitEnd();
    int          pdgID  = (*i).particleID();
    double    kinEnergy = (*i).kineticEnergy();

<<<<<<< HEAD
    m_outputCSCHits->Emplace(id,time,eneDeposit,HitStart,HitEnd,pdgID,partLink,kinEnergy);
  }
=======
    CSCSimHit newCSCHit(id,time,eneDeposit,HitStart,HitEnd,pdgID,CurBarcode,kinEnergy);
    pCSCHitCollNew->Insert(newCSCHit);
  }

  //.......remove old CSCSimHitCollection
  ATH_CHECK( evtStore()->remove(pCSCHitC) );

  //.......write new  CSCSimHitCollection
  ATH_CHECK( evtStore()->record(pCSCHitCollNew,m_HitName) );
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}

//--------------------------------------------------------
<<<<<<< HEAD
StatusCode McEventCollectionFilter::RPCHitsTruthRelink(){
  //--------------------------------------------------------
  //.......to relink all RPC hits to the new particle
  //--------------------------------------------------------
  if(!m_inputRPCHits.isValid())
    {
      ATH_MSG_ERROR( "Could not find RPCSimHitCollection");
      return StatusCode::FAILURE;
    }
  ATH_MSG_DEBUG( "Found RPCSimHitCollection");
=======
StatusCode McEventCollectionFilter::RPCHistsTruthRelink(){
//--------------------------------------------------------
//.......to relink all RPC hits to the new particle
//--------------------------------------------------------

  m_HitName="RPC_Hits";
  const DataHandle<RPCSimHitCollection> pRPCHitColl;

  if(evtStore()->contains<RPCSimHitCollection>(m_HitName)) {
    ATH_CHECK( evtStore()->retrieve(pRPCHitColl, m_HitName) );
  } else {
    ATH_MSG_ERROR( "Could not find RPCSimHitCollection  containing " << m_HitName );
    return StatusCode::FAILURE;
  }

  RPCSimHitCollection* pRPCHitC = const_cast<RPCSimHitCollection*> (&*pRPCHitColl);
>>>>>>> release/21.0.127

  if (!m_outputRPCHits.isValid()) m_outputRPCHits = std::make_unique<RPCSimHitCollection>();
  for(RPCSimHitConstIterator i=m_inputRPCHits->begin();i!=m_inputRPCHits->end();++i){

    const HepMcParticleLink oldLink = (*i).particleLink();
    int curBarcode=0;
    if(oldLink.barcode()!=0)  curBarcode=m_RefBarcode;
    HepMcParticleLink partLink(curBarcode, oldLink.eventIndex(), oldLink.getEventCollection());
    int            id = (*i).RPCid();
    double       time = (*i).globalTime();
    Amg::Vector3D prepos = (*i).preLocalPosition();
    Amg::Vector3D   ppos = (*i).postLocalPosition();
    double eneDeposit = (*i).energyDeposit();
    int         pdgID = (*i).particleEncoding();
    double kinEnergy  = (*i).kineticEnergy();
    double stepLength = (*i).stepLength();

<<<<<<< HEAD
    m_outputRPCHits->Emplace(id,time,prepos,partLink,ppos,eneDeposit,stepLength,pdgID,kinEnergy);
  }

=======
    RPCSimHit newRPCHit(id,time,prepos,CurBarcode,ppos,eneDeposit,stepLength,pdgID,kinEnergy);
    pRPCHitCollNew->Insert(newRPCHit);

  }

  //.......remove old RPCSimHitCollection
  ATH_CHECK( evtStore()->remove(pRPCHitC) );

  //.......write new  RPCSimHitCollection
  ATH_CHECK( evtStore()->record(pRPCHitCollNew,m_HitName) );

>>>>>>> release/21.0.127
  return StatusCode::SUCCESS;
}

//--------------------------------------------------------
<<<<<<< HEAD
StatusCode McEventCollectionFilter::TGCHitsTruthRelink(){
  //--------------------------------------------------------
  //.......to relink all TGC hits to the new particle
  //--------------------------------------------------------
  if(!m_inputTGCHits.isValid())
    {
      ATH_MSG_ERROR( "Could not find TGCSimHitCollection");
      return StatusCode::FAILURE;
    }
  ATH_MSG_DEBUG( "Found TGCSimHitCollection");
=======
StatusCode McEventCollectionFilter::TGCHistsTruthRelink(){
//--------------------------------------------------------
//.......to relink all TGC hits to the new particle
//--------------------------------------------------------
  m_HitName="TGC_Hits";
  const DataHandle<TGCSimHitCollection> pTGCHitColl;

  if(evtStore()->contains<TGCSimHitCollection>(m_HitName)) {
    ATH_CHECK( evtStore()->retrieve(pTGCHitColl, m_HitName) );
  } else {
    ATH_MSG_ERROR( "Could not find TGCSimHitCollection  containing " << m_HitName );
    return StatusCode::FAILURE;
  }

  TGCSimHitCollection* pTGCHitC = const_cast<TGCSimHitCollection*> (&*pTGCHitColl);
>>>>>>> release/21.0.127

  if (!m_outputTGCHits.isValid()) m_outputTGCHits = std::make_unique<TGCSimHitCollection>();
  for(TGCSimHitConstIterator i=m_inputTGCHits->begin();i!=m_inputTGCHits->end();++i){

    const HepMcParticleLink oldLink = (*i).particleLink();
    int curBarcode=0;
    if(oldLink.barcode()!=0)  curBarcode=m_RefBarcode;
    HepMcParticleLink partLink(curBarcode, oldLink.eventIndex(), oldLink.getEventCollection());
    int             id = (*i).TGCid();
    double        time = (*i).globalTime();
    Amg::Vector3D  pos = (*i).localPosition();
    Amg::Vector3D  dir = (*i).localDireCos();
    double   enDeposit = (*i).energyDeposit();
    double      stpLen = (*i).stepLength();
    int          pdgID = (*i).particleEncoding();
    double  kinEnergy  = (*i).kineticEnergy();

    m_outputTGCHits->Emplace(id,time,pos,dir,partLink,enDeposit,stpLen,pdgID,kinEnergy);
  }

<<<<<<< HEAD
  return StatusCode::SUCCESS;
}

//--------------------------------------------------------
StatusCode McEventCollectionFilter::STGC_HitsTruthRelink(){
  //--------------------------------------------------------
  //.......to relink all sTGC hits to the new particle
  //--------------------------------------------------------
  if(!m_inputSTGCHits.isValid())
    {
      ATH_MSG_ERROR( "Could not find sTGCSimHitCollection");
      return StatusCode::FAILURE;
    }
  ATH_MSG_DEBUG( "Found sTGCSimHitCollection");

  if (!m_outputSTGCHits.isValid()) m_outputSTGCHits = std::make_unique<sTGCSimHitCollection>();
  for(sTGCSimHitConstIterator i=m_inputSTGCHits->begin();i!=m_inputSTGCHits->end();++i){
    const HepMcParticleLink oldLink = (*i).particleLink();
    int curBarcode=0;
    if(oldLink.barcode()!=0)  curBarcode=m_RefBarcode;
    HepMcParticleLink partLink(curBarcode, oldLink.eventIndex(), oldLink.getEventCollection());

    int             id = (*i).sTGCId();
    double        time = (*i).globalTime();
    Amg::Vector3D  pos = (*i).globalPosition();
    int          pdgID = (*i).particleEncoding();
    Amg::Vector3D  dir = (*i).globalDirection();
    double   enDeposit = (*i).depositEnergy();

    m_outputSTGCHits->Emplace(id,time,pos,pdgID,dir,enDeposit,partLink);
  }

=======
  //.......remove old TGCSimHitCollection
  ATH_CHECK( evtStore()->remove(pTGCHitC) );

  //.......write new  TGCSimHitCollection
  ATH_CHECK( evtStore()->record(pTGCHitCollNew,m_HitName) );
>>>>>>> release/21.0.127
  return StatusCode::SUCCESS;
}

//--------------------------------------------------------
<<<<<<< HEAD
StatusCode McEventCollectionFilter::MM_HitsTruthRelink(){
  //--------------------------------------------------------
  //.......to relink all MM hits to the new particle
  //--------------------------------------------------------
  if(!m_inputMMHits.isValid())
    {
      ATH_MSG_ERROR( "Could not find MMSimHitCollection");
      return StatusCode::FAILURE;
    }
  ATH_MSG_DEBUG( "Found MMSimHitCollection");

  if (!m_outputMMHits.isValid()) m_outputMMHits = std::make_unique<MMSimHitCollection>();
  for(MMSimHitConstIterator i=m_outputMMHits->begin();i!=m_outputMMHits->end();++i){
    const HepMcParticleLink oldLink = (*i).particleLink();
    int curBarcode=0;
    if(oldLink.barcode()!=0)  curBarcode=m_RefBarcode;
    HepMcParticleLink partLink(curBarcode, oldLink.eventIndex(), oldLink.getEventCollection());

    int             id = (*i).MMId();
    double        time = (*i).globalTime();
    Amg::Vector3D  pos = (*i).globalPosition();
    int          pdgID = (*i).particleEncoding();
    double    kinEnergy = (*i).kineticEnergy();
    Amg::Vector3D  dir = (*i).globalDirection();
    double   enDeposit = (*i).depositEnergy();

    m_outputMMHits->Emplace(id,time,pos,pdgID,kinEnergy,dir,enDeposit,partLink);
  }

  return StatusCode::SUCCESS;
}

//--------------------------------------------------------
StatusCode McEventCollectionFilter::FindTRTElectronHits()
{
  //--------------------------------------------------------
  //.......retrive TRTUncompressedHitCollection collection
  if(!m_inputTRTHits.isValid())
    {
      ATH_MSG_ERROR( "Could not find TRTUncompressedHitsCollection");
      return StatusCode::FAILURE;
    }
  ATH_MSG_DEBUG( "Found TRTUncompressedHitsCollection");
=======
  //.......retrive TRTUncompressedHitCollection collection
  m_HitName  = "TRTUncompressedHits";
  const DataHandle<TRTUncompressedHitCollection> pTRTHitColl;

  if(evtStore()->contains<TRTUncompressedHitCollection>(m_HitName)) {
    ATH_CHECK( evtStore()->retrieve(pTRTHitColl, m_HitName) );
  } else {
    ATH_MSG_ERROR( "Could not find collection containing " << m_HitName );
    return StatusCode::FAILURE;
  }

  ATH_MSG_DEBUG( "Found collection containing " << m_HitName );
>>>>>>> release/21.0.127

  m_elecBarcode.clear();

  std::set<int> barcode_tmp;

  for (TRTUncompressedHitCollection::const_iterator i = m_inputTRTHits->begin(); i != m_inputTRTHits->end(); ++i)
    {
      const HepMcParticleLink McLink = (*i).particleLink();
      int  pdgID = (*i).GetParticleEncoding();
      if(std::abs(pdgID)==11&&McLink.barcode()!=0) barcode_tmp.insert(McLink.barcode());
    }

  m_elecBarcode.assign(barcode_tmp.begin(),barcode_tmp.end());

  return StatusCode::SUCCESS;
}
