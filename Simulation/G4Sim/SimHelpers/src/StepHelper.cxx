/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/


#include "SimHelpers/StepHelper.h"

#include "G4LogicalVolume.hh"
#include "G4VPhysicalVolume.hh"
#include "G4TouchableHistory.hh"
#include "G4VProcess.hh"

<<<<<<< HEAD
namespace G4StepHelper {
G4ThreeVector preStepPosition(const G4Step* theStep)
=======
StepHelper::StepHelper():m_theStep(0)
>>>>>>> release/21.0.127
{
  assert (theStep!=nullptr);
  return theStep->GetPreStepPoint()->GetPosition();
}
<<<<<<< HEAD
G4ThreeVector postStepPosition(const G4Step* theStep)
=======
StepHelper::StepHelper(const G4Step* s):m_theStep(s)
>>>>>>> release/21.0.127
{
  assert (theStep!=nullptr);
  return theStep->GetPostStepPoint()->GetPosition();
}
std::string particleName(const G4Step* theStep)
{
<<<<<<< HEAD
  assert (theStep!=nullptr);
  return theStep->GetTrack()->GetDefinition()->GetParticleName();
=======
	m_theStep=s;
>>>>>>> release/21.0.127
}
int particlePDGCode(const G4Step* theStep)
{
<<<<<<< HEAD
  assert (theStep!=nullptr);
  return theStep->GetTrack()->GetDefinition()->GetPDGEncoding();
=======
	assert (m_theStep!=0);
	return m_theStep->GetPreStepPoint()->GetPosition();
>>>>>>> release/21.0.127
}
double depositedEnergy(const G4Step* theStep)
{
<<<<<<< HEAD
  assert (theStep!=nullptr);
  return theStep->GetTotalEnergyDeposit();
=======
	assert (m_theStep!=0);
	return m_theStep->GetPostStepPoint()->GetPosition();
>>>>>>> release/21.0.127
}
G4LogicalVolume* getPreStepLogicalVolume(const G4Step* theStep, int iLevel)
{
<<<<<<< HEAD
  return getPreStepPhysicalVolume(theStep, iLevel)->GetLogicalVolume();
=======
	assert (m_theStep!=0);
	return m_theStep->GetTrack()->GetDefinition()->GetParticleName();
>>>>>>> release/21.0.127
}
std::string getPreStepLogicalVolumeName(const G4Step* theStep, int iLevel)
{
<<<<<<< HEAD
  return getPreStepLogicalVolume(theStep, iLevel)->GetName();
=======
	assert (m_theStep!=0);
	return m_theStep->GetTrack()->GetDefinition()->GetPDGEncoding();
>>>>>>> release/21.0.127
}
G4VPhysicalVolume* getPreStepPhysicalVolume(const G4Step* theStep, int iLevel)
{
<<<<<<< HEAD
  const G4TouchableHistory *history
    = static_cast<const G4TouchableHistory*>(theStep->GetPreStepPoint()->GetTouchable());
  if (iLevel<=0)
    {
      return history->GetVolume(std::abs(iLevel));
    }
  const int nLev=history->GetHistoryDepth();
  return history->GetVolume(nLev-iLevel);
=======
	assert (m_theStep!=0);
	return m_theStep->GetTotalEnergyDeposit();
>>>>>>> release/21.0.127
}
G4LogicalVolume* getPostStepLogicalVolume(const G4Step* theStep, int iLevel)
{
  return getPostStepPhysicalVolume(theStep, iLevel)->GetLogicalVolume();
}
std::string getPostStepLogicalVolumeName(const G4Step* theStep, int iLevel)
{
  return getPostStepLogicalVolume(theStep, iLevel)->GetName();
}
G4VPhysicalVolume* getPostStepPhysicalVolume(const G4Step* theStep, int iLevel)
{
<<<<<<< HEAD
  const G4TouchableHistory *history
    = static_cast<const G4TouchableHistory*>(theStep->GetPostStepPoint()->GetTouchable());
  if (iLevel<=0)
    {
      return history->GetVolume(std::abs(iLevel));
    }
  const int nLev=history->GetHistoryDepth();
  return history->GetVolume(nLev-iLevel);
=======
	G4TouchableHistory *history=(G4TouchableHistory *)
					m_theStep->GetPreStepPoint()->GetTouchable();
	if (iLevel<=0)
	{
		return history->GetVolume(abs(iLevel));
	}
	else
	{
		int nLev=history->GetHistoryDepth();
		return history->GetVolume(nLev-iLevel);
	}
>>>>>>> release/21.0.127
}
int preStepBranchDepth(const G4Step* theStep)
{
  return static_cast<const G4TouchableHistory*>(theStep->GetPreStepPoint()->
          GetTouchable())->GetHistoryDepth();
}
int postStepBranchDepth(const G4Step* theStep)
{
  return static_cast<const G4TouchableHistory*>(theStep->GetPostStepPoint()->
          GetTouchable())->GetHistoryDepth();
}
const G4VProcess* getProcess(const G4Step* theStep)
{
<<<<<<< HEAD
  return theStep->GetPostStepPoint()->GetProcessDefinedStep();
=======
	G4TouchableHistory *history=(G4TouchableHistory *)
					m_theStep->GetPostStepPoint()->GetTouchable();
	if (iLevel<=0)
	{
		return history->GetVolume(abs(iLevel));
	}
	else
	{
		int nLev=history->GetHistoryDepth();
		return history->GetVolume(nLev-iLevel);
	}
}
int StepHelper::PreStepBranchDepth() const
{
	return ((G4TouchableHistory *)m_theStep->GetPreStepPoint()->
				GetTouchable())->GetHistoryDepth();
}
int StepHelper::PostStepBranchDepth() const
{
	return ((G4TouchableHistory *)m_theStep->GetPostStepPoint()->
				GetTouchable())->GetHistoryDepth();
}
const G4VProcess* StepHelper::GetProcess() const
{
	return m_theStep->GetPostStepPoint()->GetProcessDefinedStep();
>>>>>>> release/21.0.127
}

std::string getProcessName(const G4Step* theStep)
{
  return getProcess(theStep)->GetProcessName();
}

G4int getProcessSubType(const G4Step* theStep)
{
  return getProcess(theStep)->GetProcessSubType();
}
}
