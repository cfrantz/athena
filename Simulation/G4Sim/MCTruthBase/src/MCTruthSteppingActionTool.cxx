/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "MCTruthSteppingActionTool.h"
<<<<<<< HEAD
=======
#include "CxxUtils/make_unique.h"
>>>>>>> release/21.0.127


namespace G4UA
{

  //---------------------------------------------------------------------------
  // Constructor
  //---------------------------------------------------------------------------
  MCTruthSteppingActionTool::
  MCTruthSteppingActionTool(const std::string& type, const std::string& name,
                            const IInterface* parent)
<<<<<<< HEAD
    : UserActionToolBase<MCTruthSteppingAction>(type, name, parent)
  {
=======
    : ActionToolBase<MCTruthSteppingAction>(type, name, parent)
  {
    declareInterface<IG4EventActionTool>(this);
    declareInterface<IG4SteppingActionTool>(this);
>>>>>>> release/21.0.127
    declareProperty("VolumeCollectionMap", m_volumeCollectionMap,
                    "Map of volume name to output collection name");
  }

  //---------------------------------------------------------------------------
  // Initialize the tool
  //---------------------------------------------------------------------------
  StatusCode MCTruthSteppingActionTool::initialize()
  {
    ATH_MSG_DEBUG( "Initializing " << name() );
    return StatusCode::SUCCESS;
  }

  //---------------------------------------------------------------------------
  // Create an MCTruthSteppingAction
  //---------------------------------------------------------------------------
  std::unique_ptr<MCTruthSteppingAction>
<<<<<<< HEAD
  MCTruthSteppingActionTool::makeAndFillAction(G4AtlasUserActions& actionLists)
  {
    ATH_MSG_DEBUG("Constructing an MCTruthSteppingAction");
    auto action = std::make_unique<MCTruthSteppingAction> (
        m_volumeCollectionMap, msgSvc(), msg().level() );
    actionLists.eventActions.push_back( action.get() );
    actionLists.steppingActions.push_back( action.get() );
    return action;
=======
  MCTruthSteppingActionTool::makeAction()
  {
    ATH_MSG_DEBUG("Constructing an MCTruthSteppingAction");
    return
      std::make_unique<MCTruthSteppingAction>
        ( m_volumeCollectionMap, msgSvc(), msg().level() );
>>>>>>> release/21.0.127
  }

} // namespace G4UA
