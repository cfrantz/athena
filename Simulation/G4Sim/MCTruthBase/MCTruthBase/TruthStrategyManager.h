/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef MCTruthBase_TruthStrategyManager_H
#define MCTruthBase_TruthStrategyManager_H

<<<<<<< HEAD
// ISF include
#include "ISF_Interfaces/ITruthSvc.h"
#include "ISF_Interfaces/IGeoIDSvc.h"

=======
#include "AthenaKernel/MsgStreamMember.h"

// ISF include
#include "ISF_Interfaces/ITruthSvc.h"
#include "ISF_Interfaces/IGeoIDSvc.h"

>>>>>>> release/21.0.127
/// Forward declarations
class G4Step;


<<<<<<< HEAD
/// @brief Singleton class for creating truth incidents.
/// This class is gradually being refactored out of existence.

=======
/// @brief Singleton class for some truth stuff (???)
/// NEEDS DOCUMENTATION
///
>>>>>>> release/21.0.127
class TruthStrategyManager
{

public:

  /// Retrieve the singleton instance
  static TruthStrategyManager* GetStrategyManager();

  /// Returns true if any of the truth strategies return true
<<<<<<< HEAD
  bool CreateTruthIncident(const G4Step*, int subDetVolLevel) const;
=======
  bool CreateTruthIncident(const G4Step*);
>>>>>>> release/21.0.127

  /// Define which ISF TruthService to use
  void SetISFTruthSvc(ISF::ITruthSvc *truthSvc);

  /// Define which ISF GeoIDSvc to use
  void SetISFGeoIDSvc(ISF::IGeoIDSvc *geoIDSvc);
<<<<<<< HEAD
=======

  StatusCode InitializeWorldVolume();
>>>>>>> release/21.0.127

private:
  TruthStrategyManager();
  TruthStrategyManager(const TruthStrategyManager&) = delete;
  TruthStrategyManager& operator=(const TruthStrategyManager&) = delete;

<<<<<<< HEAD
  /// ISF Services the TruthStrategyManager talks to
  ISF::ITruthSvc* m_truthSvc;
  ISF::IGeoIDSvc* m_geoIDSvc;
=======
  /// Log a message using the Athena controlled logging system
  MsgStream& msg( MSG::Level lvl ) const { return m_msg << lvl; }
  /// Check whether the logging system is active at the provided verbosity level
  bool msgLvl( MSG::Level lvl ) const { return m_msg.get().level() <= lvl; }
  /// Private message stream member
  mutable Athena::MsgStreamMember m_msg;

  /// ISF Services the TruthStrategyManager talks to
  ISF::ITruthSvc* m_truthSvc;
  ISF::IGeoIDSvc* m_geoIDSvc;

  /// The level in the G4 volume hierarchy at which can we find the sub-detector name
  int m_subDetVolLevel;
>>>>>>> release/21.0.127
};

#endif
