/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

// class header
#include "Geant4SimSvc.h"

<<<<<<< HEAD
=======
// StoreGate
#include "StoreGate/StoreGateSvc.h"
// Remaining ISF include
#include "ISF_Interfaces/ITruthSvc.h"
#include "ISF_Event/ISFParticle.h"

#include "GeoModelInterfaces/IGeoModelSvc.h"
#include "G4AtlasInterfaces/IDetectorGeometrySvc.h"
#include "ISF_Geant4Interfaces/ITransportTool.h"

// Geant4 classes
#include "G4Timer.hh"
#include "G4SDManager.hh"


>>>>>>> release/21.0.127
/** Constructor **/
iGeant4::Geant4SimSvc::Geant4SimSvc(const std::string& name,ISvcLocator* svc) :
  BaseSimulationSvc(name, svc)
{
}

iGeant4::Geant4SimSvc::~Geant4SimSvc()
{}

/** framework methods */
StatusCode iGeant4::Geant4SimSvc::initialize()
{
  ATH_CHECK (m_simulatorTool.retrieve());
  return StatusCode::SUCCESS;
}

/** framework methods */
StatusCode iGeant4::Geant4SimSvc::finalize()
{
  return StatusCode::SUCCESS;
}

StatusCode iGeant4::Geant4SimSvc::setupEvent()
{
<<<<<<< HEAD
  return m_simulatorTool->setupEventST();
=======
  ATH_MSG_DEBUG ( m_screenOutputPrefix << "setup Event" );

  m_nrOfEntries++;
  if (m_doTiming) m_eventTimer->Start();

  // make sure SD collections are properly initialized in every Athena event
  G4SDManager::GetSDMpointer()->PrepareNewEvent();

  return StatusCode::SUCCESS;
>>>>>>> release/21.0.127
}

StatusCode iGeant4::Geant4SimSvc::releaseEvent()
{
  return m_simulatorTool->releaseEventST();
}

/** Simulation Call */
StatusCode iGeant4::Geant4SimSvc::simulate(const ISF::ISFParticle& isp, McEventCollection* mcEventCollection)
{
  ISF::ISFParticleContainer secondaries; // filled, but not used
  ATH_CHECK(m_simulatorTool->simulate(isp, secondaries, mcEventCollection));
  return StatusCode::SUCCESS;
}

/** Simulation Call */
StatusCode iGeant4::Geant4SimSvc::simulateVector(const ISF::ConstISFParticleVector& particles, McEventCollection* mcEventCollection)
{
  ISF::ISFParticleContainer secondaries; // filled, but not used
  ATH_CHECK (m_simulatorTool->simulateVector(particles,secondaries, mcEventCollection));
  return StatusCode::SUCCESS;
}

