/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// InputConverter.h, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

<<<<<<< HEAD
#ifndef ISF_INPUTCONVERTER_H
#define ISF_INPUTCONVERTER_H 1
=======
#ifndef ISF_SERVICES_INPUTCONVERTER_H
#define ISF_SERVICES_INPUTCONVERTER_H 1
>>>>>>> release/21.0.127

// STL includes
#include <string>
// ISF include
#include "ISF_Interfaces/IInputConverter.h"
// FrameWork includes
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"
#include "AthenaBaseComps/AthService.h"

#include "BarcodeEvent/Barcode.h"

// forward declarations
namespace Barcode {
  class IBarcodeSvc;
}
namespace HepPDT {
  class ParticleDataTable;
}
<<<<<<< HEAD
#include "AtlasHepMC/GenEvent_fwd.h"
#include "AtlasHepMC/GenParticle_fwd.h"

=======
namespace HepMC {
  class GenParticle;
  class GenEvent;
}
>>>>>>> release/21.0.127
class IPartPropSvc;
class McEventCollection;
namespace ISFTesting {
  class InputConverter_test;
}
namespace ISF {
  class ISFParticle;
  class IGenParticleFilter;
}

class G4ParticleDefinition;
class G4PrimaryParticle;
class G4VSolid;

namespace ISF {

  /** @class InputConverter

      Convert simulation input collections to ISFParticles for subsequent ISF simulation.

      @author Elmar.Ritsch -at- cern.ch
     */
<<<<<<< HEAD
  class InputConverter final: public extends<AthService, IInputConverter> {
=======
  class InputConverter final: public AthService, public IInputConverter {
>>>>>>> release/21.0.127

    // allow test to access private data
    friend ISFTesting::InputConverter_test;

  public:
    InputConverter(const std::string& name, ISvcLocator* svc);
    virtual ~InputConverter();

    /** Athena algtool Hooks */
    virtual StatusCode  initialize() override final;
    virtual StatusCode  finalize() override final;

<<<<<<< HEAD
=======
    /** ReturnGaudi InterfaceID */
    static const InterfaceID& interfaceID() { return IID_IInputConverter; }

>>>>>>> release/21.0.127
    /** Convert selected particles from the given McEventCollection into ISFParticles
        and push them into the given ISFParticleContainer */
    virtual StatusCode convert(const McEventCollection& inputGenEvents,
                               ISF::ISFParticleContainer& simParticles,
<<<<<<< HEAD
                               EBC_EVCOLL kindOfCollection=EBC_MAINEVCOLL) const override final;
=======
                               bool isPileup=false) const override final;
>>>>>>> release/21.0.127

    /** */
    virtual StatusCode convertHepMCToG4Event(McEventCollection& inputGenEvents,
                                             G4Event*& outputG4Event,
<<<<<<< HEAD
                                             EBC_EVCOLL kindOfCollection=EBC_MAINEVCOLL) const override final;

    /** Converts vector of ISF::ISFParticles to G4Event */
    G4Event* ISF_to_G4Event(const std::vector<const ISF::ISFParticle*>& isp, HepMC::GenEvent *genEvent, bool useHepMC=false) const override final;
=======
                                             bool isPileup) const override final;

    /** Query the interfaces. **/
    StatusCode queryInterface( const InterfaceID& riid, void** ppvInterface );

    /** Converts vector of ISF::ISFParticles to G4Event */
    G4Event* ISF_to_G4Event(const std::vector<const ISF::ISFParticle*>& isp, HepMC::GenEvent *genEvent) const override final;
>>>>>>> release/21.0.127

  private:

    const G4ParticleDefinition* getG4ParticleDefinition(int pdgcode) const;

<<<<<<< HEAD
#ifdef HEPMC3
    G4PrimaryParticle* getG4PrimaryParticle(HepMC::ConstGenParticlePtr gp) const;
#else
    G4PrimaryParticle* getG4PrimaryParticle(const HepMC::GenParticle& gp) const;
#endif

    G4PrimaryParticle* getG4PrimaryParticle(const ISF::ISFParticle& isp, bool useHepMC) const;

    void addG4PrimaryVertex(G4Event* g4evt, const ISF::ISFParticle& isp, bool useHepMC) const;

    /** Tests whether the given ISFParticle is within the Geant4 world volume */
    bool isInsideG4WorldVolume(const ISF::ISFParticle& isp, const G4VSolid* worldSolid) const;

    /** get right GenParticle mass */
#ifdef HEPMC3
    double getParticleMass(HepMC::ConstGenParticlePtr p) const;
#else
    double getParticleMass(const HepMC::GenParticle& p) const;
#endif

    /** get all generator particles which pass filters */
#ifdef HEPMC3
    std::vector<HepMC::ConstGenParticlePtr > getSelectedParticles(const HepMC::GenEvent& evnt, bool legacyOrdering=false) const;
#else
    std::vector<HepMC::GenParticlePtr > getSelectedParticles(const HepMC::GenEvent& evnt, bool legacyOrdering=false) const;
#endif

    /** check if the given particle passes all filters */
#ifdef HEPMC3
    bool passesFilters(HepMC::ConstGenParticlePtr p) const;
#else
    bool passesFilters(const HepMC::GenParticle& p) const;
#endif

    /** convert GenParticle to ISFParticle */
#ifdef HEPMC3
    ISF::ISFParticle* convertParticle(HepMC::ConstGenParticlePtr genPartPtr, EBC_EVCOLL kindOfCollection=EBC_MAINEVCOLL) const;
#else
    ISF::ISFParticle* convertParticle(HepMC::GenParticlePtr genPartPtr, EBC_EVCOLL kindOfCollection=EBC_MAINEVCOLL) const;
#endif
=======
    G4PrimaryParticle* getG4PrimaryParticle(const HepMC::GenParticle& gp) const;

    G4PrimaryParticle* getG4PrimaryParticle(const ISF::ISFParticle& isp) const;

    void addG4PrimaryVertex(G4Event* g4evt, const ISF::ISFParticle& isp) const;

    /** Tests whether the given ISFParticle is within the Geant4 world volume */
    bool isInsideG4WorldVolume(const ISF::ISFParticle& isp) const;

    /** get right GenParticle mass */
    double getParticleMass(const HepMC::GenParticle& p) const;

    /** get all generator particles which pass filters */
    std::vector<HepMC::GenParticle*> getSelectedParticles(const HepMC::GenEvent& evnt, bool legacyOrdering=false) const;

    /** check if the given particle passes all filters */
    bool passesFilters(const HepMC::GenParticle& p) const;

    /** convert GenParticle to ISFParticle */
    ISF::ISFParticle* convertParticle(HepMC::GenParticle* genPartPtr, int bcid) const;
>>>>>>> release/21.0.127

    /** ParticlePropertyService and ParticleDataTable */
    ServiceHandle<IPartPropSvc>           m_particlePropSvc;          //!< particle properties svc to retrieve PDT
    const HepPDT::ParticleDataTable      *m_particleDataTable;        //!< PDT used to look up particle masses

    bool                                  m_useGeneratedParticleMass; //!< use GenParticle::generated_mass() in simulation

    ToolHandleArray<IGenParticleFilter>   m_genParticleFilters;       //!< HepMC::GenParticle filters

<<<<<<< HEAD
=======
    mutable G4VSolid                     *m_worldSolid;               //!< The Geant4 world volume solid <-- FIXME!!

>>>>>>> release/21.0.127
    bool                                  m_quasiStableParticlesIncluded; //<! will quasi-stable particles be included in the simulation

    ServiceHandle<Barcode::IBarcodeSvc>   m_barcodeSvc;                 //!< The ISF Barcode service
    Barcode::ParticleBarcode              m_barcodeGenerationIncrement; //!< to be retrieved from ISF Barcode service

  };

}


<<<<<<< HEAD
#endif //> !ISF_INPUTCONVERTER_H
=======
#endif //> !ISF_SERVICES_INPUTCONVERTER_H
>>>>>>> release/21.0.127
