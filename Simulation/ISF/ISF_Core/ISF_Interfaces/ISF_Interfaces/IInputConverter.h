/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// IInputConverter.h, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#ifndef ISF_INTERFACES_IINPUTCONVERTER_H
#define ISF_INTERFACES_IINPUTCONVERTER_H 1

// Gaudi
#include "GaudiKernel/IInterface.h"

<<<<<<< HEAD
//GeneratorObjects
#include "GeneratorObjects/HepMcParticleLink.h"

=======
>>>>>>> release/21.0.127
// StoreGate
#include "StoreGate/ReadHandle.h"
#include "StoreGate/WriteHandle.h"

// Simulation includes
#include "ISF_Event/ISFParticleContainer.h"

<<<<<<< HEAD
// forward declarations
class McEventCollection;
class G4Event;
#include "AtlasHepMC/GenEvent_fwd.h"
=======
/** Declaration of the interface ID ( interface id, major version, minor version) */
static const InterfaceID IID_IInputConverter("IInputConverter", 1, 0);

// forward declarations
class McEventCollection;
class G4Event;
namespace HepMC {
  class GenEvent;
}
>>>>>>> release/21.0.127

namespace ISF {

  class ISFParticle;

  /**
     @class IInputConverter

     Interface to Athena service that converts an input McEventCollection
     into a container of ISFParticles.

     @author Elmar.Ritsch -at- cern.ch
  */

  class IInputConverter : virtual public IInterface {
  public:

    /** Virtual destructor */
    virtual ~IInputConverter(){}

<<<<<<< HEAD
    /** Tell Gaudi which InterfaceID we have */
    DeclareInterfaceID( ISF::IInputConverter, 1, 0 );
=======
    /** Gaudi InterfaceID */
    static const InterfaceID& interfaceID() { return IID_IInputConverter; }
>>>>>>> release/21.0.127

    /** Convert selected particles from the given McEventCollection into ISFParticles
        and push them into the given ISFParticleContainer */
    virtual StatusCode convert(const McEventCollection& inputGenEvents,
                               ISFParticleContainer& simParticles,
<<<<<<< HEAD
                               EBC_EVCOLL kindOfCollection=EBC_MAINEVCOLL) const = 0;
=======
                               bool isPileup) const = 0;
>>>>>>> release/21.0.127

    /** Convert selected particles from the given McEventCollection into G4PrimaryParticles
        and push them into the given G4Event */
    virtual StatusCode convertHepMCToG4Event(McEventCollection& inputGenEvents,
                                             G4Event*& outputG4Event,
<<<<<<< HEAD
                                             EBC_EVCOLL kindOfCollection=EBC_MAINEVCOLL) const = 0;

    /** Converts vector of ISF::ISFParticles to G4Event */
    virtual G4Event* ISF_to_G4Event(const std::vector<const ISF::ISFParticle*>& isp, HepMC::GenEvent *genEvent, bool useHepMC=false) const = 0;

  };

} // end of ISF namespace
=======
                                             bool isPileup) const = 0;

    /** Converts vector of ISF::ISFParticles to G4Event */
    virtual G4Event* ISF_to_G4Event(const std::vector<const ISF::ISFParticle*>& isp, HepMC::GenEvent *genEvent) const = 0;

  };

} // end of namespace
>>>>>>> release/21.0.127

#endif // ISF_INTERFACES_IINPUTCONVERTER_H
