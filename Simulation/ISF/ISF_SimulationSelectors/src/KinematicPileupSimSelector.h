/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/
///////////////////////////////////////////////////////////////////
// KinematicPileupSimSelector.h, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#ifndef ISF_TOOLS_KINEMATICPILEUPSIMSELECTOR_H
#define ISF_TOOLS_KINEMATICPILEUPSIMSELECTOR_H 1

// ISF includes
#include "ISF_Event/KinematicParticleCuts.h"
<<<<<<< HEAD
#include "BaseSimulationSelector.h"
=======
#include "ISF_Interfaces/ISimulationSelector.h"
>>>>>>> release/21.0.127

// Barcode interpretation
#include "BarcodeServices/BitCalculator.h"

namespace ISF {

  /** @class KinematicPileupSimSelector
<<<<<<< HEAD

      Simplistic pileup filter with cuts on energy and pseudorapidity.

      @author Elmar.Ritsch -at- cern.ch
      @author Artem.Basalaev -at- cern.ch
     */
  class KinematicPileupSimSelector final : public BaseSimulationSelector, public KinematicParticleCuts {

    public:
=======
  
      Simplistic pileup filter with cuts on energy and pseudorapidity.
  
      @author Elmar.Ritsch -at- cern.ch
      @author Artem.Basalaev -at- cern.ch
     */
  class KinematicPileupSimSelector : public ISimulationSelector, public KinematicParticleCuts {
      
    public: 
>>>>>>> release/21.0.127
     /** Constructor with parameters */
     KinematicPileupSimSelector( const std::string& t, const std::string& n, const IInterface* p );

     /** Destructor */
     ~KinematicPileupSimSelector();


    // Athena algtool's Hooks
<<<<<<< HEAD
    StatusCode  initialize() override;
    StatusCode  finalize() override;
=======
    StatusCode  initialize() override final;
    StatusCode  finalize() override final;
>>>>>>> release/21.0.127

    /** check whether given particle passes all cuts -> will be used for routing decision*/
    inline virtual bool passSelectorCuts(const ISFParticle& particle) const override final;

  private:
    mutable Barcode::BitCalculator m_bitcalculator;

    std::vector<int> m_pileupbcid; // vector of BCIDs to select

  };

}

#endif //> !ISF_TOOLS_KINEMATICPILEUPSIMSELECTOR_H
