/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// KinematicSimSelector.h, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#ifndef ISF_TOOLS_KINEMATICSIMSELECTOR_H
#define ISF_TOOLS_KINEMATICSIMSELECTOR_H 1

#include "GaudiKernel/IPartPropSvc.h"


// ISF includes
#include "ISF_Event/KinematicParticleCuts.h"
#include "BaseSimulationSelector.h"

namespace ISF
{

  /** @class KinematicSimSelector

      Simplistic kinematic filter on energy and pseudorapidity.

      @author Elmar.Ritsch -at- cern.ch
  */
<<<<<<< HEAD
  class KinematicSimSelector final : public BaseSimulationSelector, public KinematicParticleCuts
  {

  ServiceHandle<IPartPropSvc> m_partPropSvc; 
  
=======
  class KinematicSimSelector : public ISimulationSelector, public KinematicParticleCuts
  {

>>>>>>> release/21.0.127
  public:
    /** Constructor with parameters */
    KinematicSimSelector( const std::string& t, const std::string& n, const IInterface* p );

    /** Destructor */
    ~KinematicSimSelector();

    // Athena algtool's Hooks
<<<<<<< HEAD
    virtual StatusCode  initialize() override;
    virtual StatusCode  finalize() override;

    /** check whether given particle passes all cuts -> will be used for routing decision*/
    virtual bool passSelectorCuts(const ISFParticle& particle) const override;
=======
    virtual StatusCode  initialize() override final;
    virtual StatusCode  finalize() override final;

    /** check whether given particle passes all cuts -> will be used for routing decision*/
    virtual bool passSelectorCuts(const ISFParticle& particle) const override final;
>>>>>>> release/21.0.127
  };

}

#endif //> !ISF_TOOLS_KINEMATICSIMSELECTOR_H
