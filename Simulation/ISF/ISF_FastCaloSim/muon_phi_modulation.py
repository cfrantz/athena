#!/usr/local/Cellar/python/2.7.14/bin/python2.7 python
'''
cfrantz
2021-3-11
'''
from ROOT import TMatrixD
from ROOT import TVectorD 
from ROOT import TH1D
from ROOT import TH1I
from ROOT import TFile
from ROOT import TROOT
from ROOT import TSystem
from ROOT import TApplication
from ROOT import TTree
from ROOT import TSystem      
from ROOT import TH2D                                                                            
from ROOT import TPrincipal                                              
from ROOT import TMath                        
from ROOT import TBrowser 
from ISF_FastCaloSimEvent import TFCSApplyFirstPCA
from ISF_FastCaloSimEvent import TFCSMakeFirstPCA 
from ROOT import TLorentzVector
from ROOT import TChain 
from ISF_FastCaloSimEvent import TFCSSimulationState                                                  
from ROOT import TProfile
import CaloGeometryFromFile 
import TFCSSampleDiscovery

import ROOT
import libPyROOT
from ROOT import gROOT
import os
from optparse import OptionParser
import math
from ROOT import TChain
from ROOT import TCanvas
from ROOT import TH2
from ROOT import gDirectory
from ROOT import TreeReader
from ROOT import TLorentzVector

# These are quality-of-life optons
ROOT.gROOT.SetBatch(True)
ROOT.gStyle.SetOptStat(False)
ROOT.gStyle.SetOptTitle(False)
ROOT.gStyle.SetLegendBorderSize(0)
gROOT.ProcessLine("gErrorIgnoreLevel = kInfo+1;")

# Add the files

inputTree = TChain("FCS_ParametrizationInput")
for num in range(0,100):
    inputTree.AddFile("/eos/user/c/cfrantz/fcs/muons/830xxx"+str(num)+"/NTUP_FCS.000"+str(num)+".root")
read_inputTree = TreeReader(inputTree)



# get the deltaphi correction for each relevant layer
TruthPx = read_inputTree.GetVariable("TruthPx")
TruthPy = read_inputTree.GetVariable("TruthPy")
TruthPz = read_inputTree.GetVariable("TruthPz")
TruthE  = read_inputTree.GetVariable("TruthE")
tlv     = TLorentzVector(0,0,0,0)
tlv.SetPxPyPzE(TruthPx,TruthPy,TruthPz,TruthE)
 
# get important variables used in correction
m_layer_totE_name = ["Sampling_1","Sampling_2","Sampling_3"]
m_layer_number    = [          1 ,          2 ,          3 ]
m_pid             = read_inputTree.GetVariable("m_pid")
m_geo             = read_inputTree.GetVariable("m_geo")
# correct phi
energy_weighted_phi_correction=0.0
for l in range(0,len(m_layer_totE_name)):
    if(l<len(m_layer_totE_name)-1):
        if(m_layer_number[l]<8):
            #cell     = const(CaloDetDescrElement*)
            find_eta = float(tlv.Eta())
            find_phi = float(tlv.Phi())
            if(m_pid==11):
                find_eta=read_inputTree.GetVariable(Form("newTTC_mid_eta[0][%d]",m_layer_number[l]))
                find_phi=read_inputTree.GetVariable(Form("newTTC_mid_phi[0][%d]",m_layer_number[l]))
    
            cell = m_geo.getDDE(m_layer_number[l], find_eta, find_phi) #(layer, eta, phi)
            if(cell):
                phi_correction[l]=cell.phi()-cell.phi_raw()
            else:
                phi_correction[l]=0.0
            energy_weighted_phi_correction+=input_data[l]*phi_correction[l]
            print " l  "+str(l)+"   "+str(m_layer_number[l])+" -> "+energy_weighted_phi_correction

##########################################################################
#######################         DRAWING         ##########################

# Canvas + Draw
c = TCanvas("MuonPhiModulation_500k_v1" ,"MuonPhiModulation_500k_v1")
c.cd()

# Set up tick marks properly 
ROOT.gPad.SetTickx()
ROOT.gPad.SetTicky()

m_vector_cell_energies  = "Sum$( Sampling_0.m_vector.cell.energy )"
m_vector_cell_energies += "Sum$( Sampling_0.m_vector.cell.energy )"
m_vector_cell_energies += "Sum$( Sampling_0.m_vector.cell.energy )"
m_vector_cell_energies += "Sum$( Sampling_0.m_vector.cell.energy )"

chain.Draw("Sum$(Sampling_1.m_vector.cell.energy)+Sum$(Sampling_2.m_vector.cell.energy)+Sum$(Sampling_3.m_vector.cell.energy):fmod(newTTC_IDCaloBoundary_phi,2*TMath::Pi()/1024)>>h2Emod(32,-2*TMath::Pi()/1024,2*TMath::Pi()/1024,1000,0,2000)","Length$(TruthPDG)==1","colz")

print "AFTER DRAW"
h2Emod = (gDirectory.Get("h2Emod"))
prof   = h2Emod.ProfileX()
prof.SetLineColor(2)
print "AFTER PROF"
############################################################################
############################################################################ 

ROOT.gPad.SetTickx()
ROOT.gPad.SetTicky()
h2Emod.GetXaxis().SetTitle("Phi")
h2Emod.GetYaxis().SetTitle("Muon Energy MeV")
h2Emod.SetTitle("")

#text1  = ""
#text2  = ""
#legend = TLegend(.1,.7,.3,.9,text1)
#legend.AddEntry(h2Emod,"h2Emod","muons")
#legend.AddEntry(prof,"Profile","-")
#legend.Draw("same")

prof.Draw("same")
c.SaveAs(".pdf")

h2Emod.GetYaxis().SetRangeUser(306.0,314.0)
c.SaveAs("MuonPhiModulation_500k_v1_zoom.pdf")

print "done"
