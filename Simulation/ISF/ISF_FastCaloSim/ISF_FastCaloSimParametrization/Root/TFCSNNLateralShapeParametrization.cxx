/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#include "ISF_FastCaloSimParametrization/TFCSNNLateralShapeParametrization.h"
<<<<<<< HEAD
#include "ISF_FastCaloSimEvent/TFCSExtrapolationState.h"
=======
>>>>>>> release/21.0.127
#include "ISF_FastCaloSimEvent/FastCaloSim_CaloCell_ID.h"

//=============================================
//======= TFCSLateralShapeParametrization =========
//=============================================

TFCSNNLateralShapeParametrization::TFCSNNLateralShapeParametrization(const char* name, const char* title):TFCSLateralShapeParametrizationHitBase(name,title)
{
}

FCSReturnCode TFCSNNLateralShapeParametrization::simulate_hit(Hit& hit,TFCSSimulationState& /*simulstate*/,const TFCSTruthState* /*truth*/, const TFCSExtrapolationState* extrapol)
{
  int cs=calosample();
<<<<<<< HEAD
  hit.eta()=0.5*( extrapol->eta(cs, CaloSubPos::SUBPOS_ENT) + extrapol->eta(cs, CaloSubPos::SUBPOS_EXT) );
  hit.phi()=0.5*( extrapol->phi(cs, CaloSubPos::SUBPOS_ENT) + extrapol->phi(cs, CaloSubPos::SUBPOS_EXT) );
=======
  double hit_eta=0.5*( extrapol->eta(cs, CaloSubPos::SUBPOS_ENT) + extrapol->eta(cs, CaloSubPos::SUBPOS_EXT) );
  double hit_phi=0.5*( extrapol->phi(cs, CaloSubPos::SUBPOS_ENT) + extrapol->phi(cs, CaloSubPos::SUBPOS_EXT) );
  double hit_weight=1;
>>>>>>> release/21.0.127

  return FCSSuccess;
}
