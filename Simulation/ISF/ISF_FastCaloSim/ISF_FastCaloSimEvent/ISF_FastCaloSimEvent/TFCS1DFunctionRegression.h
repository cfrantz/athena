/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#ifndef ISF_FASTCALOSIMEVENT_TFCS1DFunctionRegression_h
#define ISF_FASTCALOSIMEVENT_TFCS1DFunctionRegression_h

#include "ISF_FastCaloSimEvent/TFCS1DFunction.h"
#include "TTree.h"
#include "TH1.h"
#include <vector>

<<<<<<< HEAD
=======
using namespace std;
>>>>>>> release/21.0.127

class TFCS1DFunctionRegression:public TFCS1DFunction
{
  public:

    TFCS1DFunctionRegression() {};
    ~TFCS1DFunctionRegression() {};

<<<<<<< HEAD
    using TFCS1DFunction::rnd_to_fct;
    virtual double rnd_to_fct(double rnd) const;
    double  regression_value(double uniform) const;
    void    set_weights(std::vector<std::vector<double> > fWeightMatrix0to1, std::vector<std::vector<double> > fWeightMatrix1to2);
    double  sigmoid(double) const;

  private:

    std::vector<std::vector<double> > m_fWeightMatrix0to1;
    std::vector<std::vector<double> > m_fWeightMatrix1to2;
=======
    virtual double rnd_to_fct(double rnd);
    double  regression_value(double uniform);
    void    set_weights(vector<vector<double> > fWeightMatrix0to1, vector<vector<double> > fWeightMatrix1to2);
    double  sigmoid(double);

  private:

    vector<vector<double> > m_fWeightMatrix0to1;
    vector<vector<double> > m_fWeightMatrix1to2;
>>>>>>> release/21.0.127


  ClassDef(TFCS1DFunctionRegression,1)  //TFCS1DFunctionRegression

};

#endif
