/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

/**
 * @author John Chapman
 * @date June, 2016
 * @brief Tests for BeamEffectsAlg.
 */

#undef NDEBUG

// Framework
#include "TestTools/initGaudi.h"

<<<<<<< HEAD
// Google Test
#include "gtest/gtest.h"

// HepMC includes
#include "AtlasHepMC/GenEvent.h"
#include "AtlasHepMC/GenVertex.h"
#include "AtlasHepMC/GenParticle.h"
#include "AtlasHepMC/Operators.h"
=======
// ATLAS C++
#include "CxxUtils/make_unique.h"

// Google Test
#include "gtest/gtest.h"
// Google Mock
// #include "gmock/gmock.h"

// HepMC includes
#include "HepMC/GenEvent.h"
>>>>>>> release/21.0.127

// CLHEP includes
#include "CLHEP/Vector/LorentzVector.h"
#include "CLHEP/Units/SystemOfUnits.h"

// Athena headers
<<<<<<< HEAD
=======
#include "CxxUtils/make_unique.h"
>>>>>>> release/21.0.127
#include "GeneratorObjects/McEventCollection.h"

// Tested AthAlgorithm
#include "../src/BeamEffectsAlg.h"


namespace SimTesting {

  // needed every time an AthAlgorithm, AthAlgTool or AthService is instantiated
  ISvcLocator* g_svcLoc = nullptr;

  // global test environment takes care of setting up Gaudi
  class GaudiEnvironment : public ::testing::Environment {
  protected:
    virtual void SetUp() override {
      Athena_test::initGaudi(SimTesting::g_svcLoc);
    }
  };

  class BeamEffectsAlg_test : public ::testing::Test {

  protected:
    virtual void SetUp() override {
      m_alg = new Simulation::BeamEffectsAlg{"BeamEffectsAlg", g_svcLoc};
<<<<<<< HEAD
      ASSERT_TRUE( g_svcLoc->service("StoreGateSvc", m_sg) );
=======
      ASSERT_TRUE( m_alg->setProperties().isSuccess() );
>>>>>>> release/21.0.127
    }

    virtual void TearDown() override {
      ASSERT_TRUE( m_alg->finalize().isSuccess() );
      delete m_alg;
    }

    //
    // accessors for private methods
    // NB: This works because BeamEffectsAlg_test is a friend of the tested
    //     BeamEffectsAlg AthAlgorithm
    //
    template<typename... Args>
    StatusCode patchSignalProcessVertex(Args&&... args) const {
      return m_alg->patchSignalProcessVertex(std::forward<Args>(args)...);
    }

<<<<<<< HEAD
    Simulation::BeamEffectsAlg* m_alg{};
    StoreGateSvc* m_sg{};
=======
    // template<typename... Args>
    // StatusCode setupReadHandleKeyVector(Args&&... args) const {
    //   return m_alg->setupReadHandleKeyVector(std::forward<Args>(args)...);
    // }

    // template<typename... Args>
    // void mergeCollections(Args&&... args) const {
    //   m_alg->mergeCollections(std::forward<Args>(args)...);
    // }
    Simulation::BeamEffectsAlg* m_alg;
>>>>>>> release/21.0.127
  };   // BeamEffectsAlg_test fixture


  TEST_F(BeamEffectsAlg_test, empty_alg_execute) {
<<<<<<< HEAD
    ASSERT_TRUE( m_alg->initialize().isSuccess() );
    EventContext ctx(0,0);
    ctx.setExtension( Atlas::ExtendedEventContext( m_sg, 0 ) );
    // expected to fail as input collection doesn't exist
    ASSERT_TRUE( m_alg->execute(ctx).isFailure() );
=======
    // expected to fail as input collection doesn't exist
    ASSERT_TRUE( m_alg->execute().isFailure() );
>>>>>>> release/21.0.127
  }

  TEST_F(BeamEffectsAlg_test, set_properties) {
    // ordering A, C, B is on purpose to test for unintended alphabetic ordering
    std::string  inputPropertyValue = "'TestGEN_EVENT'";
    std::string outputPropertyValue = "'TestBeamTruthEvent'";
    ASSERT_TRUE( m_alg->setProperty( "InputMcEventCollection",   inputPropertyValue).isSuccess() );
    ASSERT_TRUE( m_alg->setProperty( "OutputMcEventCollection", outputPropertyValue).isSuccess() );
    ASSERT_TRUE( m_alg->setProperty( "ISFRun", true).isSuccess()  );
  }

<<<<<<< HEAD
  TEST_F(BeamEffectsAlg_test, patchSignalProcessVertex_empty_GenEvent) {
    HepMC::GenEvent ge;
    ASSERT_TRUE( patchSignalProcessVertex(ge).isSuccess() );
    ASSERT_TRUE( HepMC::signal_process_vertex(&ge)==nullptr );
  }

  TEST_F(BeamEffectsAlg_test, signal_process_vertex_exists) {
    HepMC::GenEvent ge;
    CLHEP::HepLorentzVector myPos( 1.0, 1.0, 1.0, 1.0);
    HepMC::GenVertexPtr  myVertex = HepMC::newGenVertexPtr( HepMC::FourVector(myPos.x(),myPos.y(),myPos.z(),myPos.t()), -1 );
    HepMC::set_signal_process_vertex(&ge, myVertex );
    ASSERT_TRUE( patchSignalProcessVertex(ge).isSuccess() );
    ASSERT_TRUE( HepMC::signal_process_vertex(&ge)==myVertex );
=======
  TEST_F(BeamEffectsAlg_test, signal_process_vertex_exists) {
    HepMC::GenEvent ge;
    CLHEP::HepLorentzVector myPos( 1.0, 1.0, 1.0, 1.0);
    HepMC::GenVertex *myVertex = new HepMC::GenVertex( myPos, -1 );
    ge.set_signal_process_vertex( myVertex );
    ASSERT_TRUE( patchSignalProcessVertex(ge).isSuccess() );
    ASSERT_TRUE( ge.signal_process_vertex()==myVertex );
>>>>>>> release/21.0.127
  }

  TEST_F(BeamEffectsAlg_test, add_signal_process_vertex_atlasG4) {
    HepMC::GenEvent ge;
    CLHEP::HepLorentzVector myPos( 1.0, 1.0, 1.0, 1.0);
<<<<<<< HEAD
    HepMC::GenVertexPtr  myVertex = HepMC::newGenVertexPtr( HepMC::FourVector(myPos.x(),myPos.y(),myPos.z(),myPos.t()), -1 );
    ge.add_vertex( myVertex );
    ASSERT_TRUE( HepMC::signal_process_vertex(&ge)==nullptr );
    ASSERT_TRUE( m_alg->setProperty( "ISFRun", false).isSuccess()  );
    ASSERT_TRUE( patchSignalProcessVertex(ge).isSuccess() );
    ASSERT_TRUE( HepMC::signal_process_vertex(&ge)==myVertex );
#ifdef HEPMC3
//Not needed for HepMC3
#else
    ASSERT_EQ( *HepMC::signal_process_vertex(&ge), *myVertex );
#endif
=======
    HepMC::GenVertex *myVertex = new HepMC::GenVertex( myPos, -1 );
    ge.add_vertex( myVertex );
    ASSERT_TRUE( ge.signal_process_vertex()==nullptr );
    ASSERT_TRUE( m_alg->setProperty( "ISFRun", false).isSuccess()  );
    ASSERT_TRUE( patchSignalProcessVertex(ge).isSuccess() );
    ASSERT_TRUE( ge.signal_process_vertex()==myVertex );
    ASSERT_EQ( *ge.signal_process_vertex(), *myVertex );
>>>>>>> release/21.0.127
  }

  TEST_F(BeamEffectsAlg_test, add_signal_process_vertex_isfG4) {
    HepMC::GenEvent ge;
    CLHEP::HepLorentzVector myPos( 1.0, 1.0, 1.0, 1.0);
<<<<<<< HEAD
    HepMC::GenVertexPtr  myVertex = HepMC::newGenVertexPtr( HepMC::FourVector(myPos.x(),myPos.y(),myPos.z(),myPos.t()), -1 );
    HepMC::GenVertexPtr  dummyVertex = HepMC::newGenVertexPtr();
    ge.add_vertex( myVertex );
    ASSERT_TRUE( HepMC::signal_process_vertex(&ge)==nullptr );
    ASSERT_TRUE( m_alg->setProperty( "ISFRun", true).isSuccess()  );
    ASSERT_TRUE( patchSignalProcessVertex(ge).isSuccess() );
    ASSERT_TRUE( HepMC::signal_process_vertex(&ge)!=myVertex );
#ifdef HEPMC3
//Not needed for HepMC3
#else
    ASSERT_EQ( *HepMC::signal_process_vertex(&ge), *dummyVertex );
#endif
=======
    HepMC::GenVertex *myVertex = new HepMC::GenVertex( myPos, -1 );
    HepMC::GenVertex *dummyVertex = new HepMC::GenVertex();
    ge.add_vertex( myVertex );
    ASSERT_TRUE( ge.signal_process_vertex()==nullptr );
    ASSERT_TRUE( m_alg->setProperty( "ISFRun", true).isSuccess()  );
    ASSERT_TRUE( patchSignalProcessVertex(ge).isSuccess() );
    ASSERT_TRUE( ge.signal_process_vertex()!=myVertex );
    ASSERT_EQ( *ge.signal_process_vertex(), *dummyVertex );
>>>>>>> release/21.0.127
  }

  TEST_F(BeamEffectsAlg_test, execute_pass_through) {
    // create dummy input McEventCollection containing a dummy GenEvent
<<<<<<< HEAD
    EventContext ctx(0,0);
    ctx.setExtension( Atlas::ExtendedEventContext( m_sg, 0 ) );
    SG::WriteHandleKey<McEventCollection> inputTestDataKey{"GEN_EVENT"};
    ASSERT_TRUE( inputTestDataKey.initialize().isSuccess() );
    SG::WriteHandle<McEventCollection> inputTestDataHandle{inputTestDataKey, ctx};
    inputTestDataHandle = std::make_unique<McEventCollection>();
    inputTestDataHandle->push_back(new HepMC::GenEvent());
    HepMC::GenEvent& ge = *(inputTestDataHandle->at(0));
    CLHEP::HepLorentzVector myPos( 0.0, 0.0, 0.0, 0.0);
    HepMC::GenVertexPtr  myVertex = HepMC::newGenVertexPtr( HepMC::FourVector(myPos.x(),myPos.y(),myPos.z(),myPos.t()), -1 );
    HepMC::FourVector fourMomentum1( 0.0, 0.0, 1.0, 1.0*CLHEP::TeV);
    HepMC::GenParticlePtr  inParticle1 = HepMC::newGenParticlePtr(fourMomentum1, 2, 10);
    myVertex->add_particle_in(inParticle1);
    HepMC::FourVector fourMomentum2( 0.0, 0.0, -1.0, 1.0*CLHEP::TeV);
    HepMC::GenParticlePtr  inParticle2 = HepMC::newGenParticlePtr(fourMomentum2, -2, 10);
    myVertex->add_particle_in(inParticle2);
    HepMC::FourVector fourMomentum3( 0.0, 1.0, 0.0, 1.0*CLHEP::TeV);
    HepMC::GenParticlePtr  inParticle3 = HepMC::newGenParticlePtr(fourMomentum3, 2, 10);
    myVertex->add_particle_out(inParticle3);
    HepMC::FourVector fourMomentum4( 0.0, -1.0, 0.0, 1.0*CLHEP::TeV);
    HepMC::GenParticlePtr  inParticle4 = HepMC::newGenParticlePtr(fourMomentum4, -2, 10);
    myVertex->add_particle_out(inParticle4);
    ge.add_vertex( myVertex );
    HepMC::set_signal_process_vertex(&ge, myVertex );
    ge.set_beam_particles(inParticle1,inParticle2);
    //
    ASSERT_TRUE( m_alg->initialize().isSuccess() );
    ASSERT_TRUE( m_alg->execute(ctx).isSuccess() );
    SG::ReadHandleKey<McEventCollection>     outputTestDataKey{"BeamTruthEvent"};
    ASSERT_TRUE( outputTestDataKey.initialize().isSuccess() );
    SG::ReadHandle<McEventCollection>     outputTestDataHandle{outputTestDataKey,ctx};
    ASSERT_TRUE( outputTestDataHandle.isValid() );
#ifdef HEPMC3
//This should compare the content 
    ASSERT_EQ(*(HepMC::signal_process_vertex(outputTestDataHandle->at(0))), *(HepMC::signal_process_vertex((const HepMC::GenEvent*)inputTestDataHandle->at(0))));
    ASSERT_EQ(**(outputTestDataHandle->at(0)->vertices().begin()), **(((const HepMC::GenEvent*)inputTestDataHandle->at(0))->vertices().begin()));
    ASSERT_EQ(*(outputTestDataHandle->at(0)->beams().at(0)),*(inputTestDataHandle->at(0)->beams().at(0)));
    ASSERT_EQ(*(outputTestDataHandle->at(0)->beams().at(1)),*(inputTestDataHandle->at(0)->beams().at(1)));
#else
=======
    SG::WriteHandle<McEventCollection> inputTestDataHandle{"GEN_EVENT"};
    inputTestDataHandle = CxxUtils::make_unique<McEventCollection>();
    inputTestDataHandle->push_back(new HepMC::GenEvent());
    HepMC::GenEvent& ge = *(inputTestDataHandle->at(0));
    CLHEP::HepLorentzVector myPos( 0.0, 0.0, 0.0, 0.0);
    HepMC::GenVertex *myVertex = new HepMC::GenVertex( myPos, -1 );
    HepMC::FourVector fourMomentum1( 0.0, 0.0, 1.0, 1.0*CLHEP::TeV);
    HepMC::GenParticle* inParticle1 = new HepMC::GenParticle(fourMomentum1, 2, 10);
    myVertex->add_particle_in(inParticle1);
    HepMC::FourVector fourMomentum2( 0.0, 0.0, -1.0, 1.0*CLHEP::TeV);
    HepMC::GenParticle* inParticle2 = new HepMC::GenParticle(fourMomentum2, -2, 10);
    myVertex->add_particle_in(inParticle2);
    HepMC::FourVector fourMomentum3( 0.0, 1.0, 0.0, 1.0*CLHEP::TeV);
    HepMC::GenParticle* inParticle3 = new HepMC::GenParticle(fourMomentum3, 2, 10);
    myVertex->add_particle_out(inParticle3);
    HepMC::FourVector fourMomentum4( 0.0, -1.0, 0.0, 1.0*CLHEP::TeV);
    HepMC::GenParticle* inParticle4 = new HepMC::GenParticle(fourMomentum4, -2, 10);
    myVertex->add_particle_out(inParticle4);
    ge.add_vertex( myVertex );
    ge.set_signal_process_vertex( myVertex );
    ge.set_beam_particles(inParticle1,inParticle2);
    //
    ASSERT_TRUE( m_alg->execute().isSuccess() );
    SG::ReadHandle<McEventCollection>     outputTestDataHandle{"BeamTruthEvent"};
    ASSERT_TRUE( outputTestDataHandle.isValid() );
>>>>>>> release/21.0.127
    ASSERT_EQ(*(outputTestDataHandle->at(0)->signal_process_vertex()), *(inputTestDataHandle->at(0)->signal_process_vertex()));
    ASSERT_EQ(**(outputTestDataHandle->at(0)->vertices_begin()), **(inputTestDataHandle->at(0)->vertices_begin()));
    ASSERT_EQ(*(outputTestDataHandle->at(0)->beam_particles().first), *(inputTestDataHandle->at(0)->beam_particles().first));
    ASSERT_EQ(*(outputTestDataHandle->at(0)->beam_particles().second), *(inputTestDataHandle->at(0)->beam_particles().second));
<<<<<<< HEAD
#endif
=======
>>>>>>> release/21.0.127
  }

} // <-- namespace SimTesting


int main(int argc, char *argv[])
{
  ::testing::InitGoogleTest( &argc, argv );
  ::testing::AddGlobalTestEnvironment( new SimTesting::GaudiEnvironment );
  return RUN_ALL_TESTS();
}
