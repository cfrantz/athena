// Dear emacs, this is -*- C++ -*-

/*
<<<<<<< HEAD
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// GenEventBeamEffectBooster.h, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

>>>>>>> release/21.0.127
#ifndef ISF_HEPMC_GENEVENTBEAMEFFECTBOOSTER_H
#define ISF_HEPMC_GENEVENTBEAMEFFECTBOOSTER_H 1

// Base class headers
#include "AthenaBaseComps/AthAlgTool.h"
#include "HepMC_Interfaces/IGenEventManipulator.h"
// Athena headers
<<<<<<< HEAD
#include "AthenaKernel/IAthRNGSvc.h"
=======
#include "AthenaKernel/IAtRndmGenSvc.h"
#include "InDetBeamSpotService/IBeamCondSvc.h"
>>>>>>> release/21.0.127
// Gaudi headers
#include "GaudiKernel/ServiceHandle.h"
// CLHEP headers
#include "CLHEP/Geometry/Transform3D.h"
#include "CLHEP/Vector/LorentzRotation.h"

<<<<<<< HEAD
namespace ATHRNG {
  class RNGWrapper;
}

#include "AtlasHepMC/GenParticle_fwd.h"
=======
namespace CLHEP {
  class HepRandomEngine;
}

namespace HepMC {
  class GenParticle;
}
>>>>>>> release/21.0.127

namespace Simulation {

  /** @class GenEventBeamEffectBooster

      This tool takes a HepMC::GenEvent and applies boosts due to beam
      tilt, etc.

      based on:
      https://svnweb.cern.ch/trac/atlasoff/browser/Simulation/G4Atlas/G4AtlasUtilities/trunk/src/BeamEffectTransformation.cxx
  */
<<<<<<< HEAD
  class GenEventBeamEffectBooster : public extends<AthAlgTool, IGenEventManipulator>
=======
  class GenEventBeamEffectBooster : public AthAlgTool, virtual public IGenEventManipulator
>>>>>>> release/21.0.127
  {

  public:
    /** Constructor with parameters */
    GenEventBeamEffectBooster( const std::string& t, const std::string& n, const IInterface* p );

    /** Athena algtool's Hooks */
    StatusCode initialize() override final;
    StatusCode finalize() override final;

    StatusCode initializeAthenaEvent();// override final;

    /** modifies the given GenEvent */
    StatusCode manipulate(HepMC::GenEvent& ge) const override final;
  private:
    /** calculate the transformations that we want to apply to the particles in the current GenEvent */
    StatusCode initializeGenEvent(CLHEP::HepLorentzRotation& transform) const;
    /** apply boost to individual GenParticles */
<<<<<<< HEAD
    void boostParticle(HepMC::GenParticlePtr p, const CLHEP::HepLorentzRotation& transform) const;

    ServiceHandle<IAthRNGSvc>       m_rndGenSvc{this, "RandomSvc", "AthRNGSvc"};
    ATHRNG::RNGWrapper*             m_randomEngine{};             //!< Slot-local RNG
    Gaudi::Property<std::string>     m_randomEngineName{this, "RandomStream", "BEAM"};         //!< Name of the random number stream
    Gaudi::Property<bool>              m_applyBoost{this, "ApplyBoost", true};
    Gaudi::Property<bool>              m_applyDivergence{this, "ApplyDivergence", true};

    //BACKUP properties needed until IBeamCondSvc is updated
    Gaudi::Property<double> m_sigma_px_b1{this, "Sigma_px_b1", 20.e-6, "angular divergence in x of beam 1 [rad]"};
    Gaudi::Property<double> m_sigma_px_b2{this, "Sigma_px_b2", 21.e-6, "angular divergence in x of beam 2 [rad]"};
    Gaudi::Property<double> m_sigma_py_b1{this, "Sigma_py_b1", 22.e-6, "angular divergence in y of beam 1 [rad]"};
    Gaudi::Property<double> m_sigma_py_b2{this, "Sigma_py_b2", 23.e-6, "angular divergence in y of beam 2 [rad]"};
    // Crossing angles, note the convention here is taken from online https://atlas-dcs.cern.ch/index.php?page=LHC_INS::INS_BPM
    Gaudi::Property<double> m_xing_x_b1{this, "HalfXing_x_b1", 19.e-6, "half-crossing in x for beam 1 at IP1, i.e. angle between beam 1 and z-axis"};
    Gaudi::Property<double> m_xing_x_b2{this, "HalfXing_x_b2", 1.e-6, "half-crossing in x for beam 2 at IP1, i.e. angle between beam 2 and z-axis"};
    Gaudi::Property<double> m_xing_y_b1{this, "HalfXing_y_b1", 150.e-6, "half-crossing in y for beam 1 at IP1, i.e. angle between beam 1 and z-axis"};
    Gaudi::Property<double> m_xing_y_b2{this, "HalfXing_y_b2", -152.e-6, "half-crossing in y for beam 2 at IP1, i.e. angle between beam 2 and z-axis"};
    Gaudi::Property<double> m_dE{this,  "deltaEOverE", 1.1E-4, "Delta_E/E theoretical value"}; // Delta_E/E theoretical value
    Gaudi::Property<double> m_pbeam1{this, "pbeam1",        3500.0E3,    "Beam 1 Momentum / MeV"}; /// @todo Get from a service
    Gaudi::Property<double> m_pbeam2{this, "pbeam2",        3500.0E3,    "Beam 2 Momentum / MeV"}; /// @todo Get from a service
    Gaudi::Property<double> m_beam1ParticleMass{this, "Beam1ParticleMass", CLHEP::proton_mass_c2, "Mass of particle used in beam 1"};
    Gaudi::Property<double> m_beam2ParticleMass{this, "Beam2ParticleMass", CLHEP::proton_mass_c2, "Mass of particle used in beam 2"};
=======
    void boostParticle(HepMC::GenParticle* p, const CLHEP::HepLorentzRotation& transform) const;

    ServiceHandle<IBeamCondSvc>     m_beamCondSvc;
    ServiceHandle<IAtRndmGenSvc>    m_rndGenSvc;
    CLHEP::HepRandomEngine*         m_randomEngine;             /// TODO this will need to be per thread in the future
    std::string                     m_randomEngineName;         //!< Name of the random number stream
    bool                            m_applyBoost;
    bool                            m_applyDivergence;

    //BACKUP properties needed until IBeamCondSvc is updated
    double m_sigma_px_b1; // angular divergence in x of beam 1 [rad]
    double m_sigma_px_b2; // angular divergence in x of beam 2 [rad]
    double m_sigma_py_b1; // angular divergence in y of beam 1 [rad]
    double m_sigma_py_b2; // angular divergence in y of beam 2 [rad]
    // Crossing angles, note the convention here is taken from online https://atlas-dcs.cern.ch/index.php?page=LHC_INS::INS_BPM
    double m_xing_x_b1;   // half-crossing in x for beam 1 at IP1, i.e. angle between beam 1 and z-axis
    double m_xing_x_b2;    // half-crossing in x for beam 2 at IP1, i.e. angle between beam 2 and z-axis
    double m_xing_y_b1;  // half-crossing in y for beam 1 at IP1, i.e. angle between beam 1 and z-axis
    double m_xing_y_b2; // half-crossing in y for beam 2 at IP1, i.e. angle between beam 2 and z-axis
    double m_dE; // Delta_E/E theoretical value
    double m_pbeam1; /// @todo Get from a service
    double m_pbeam2; /// @todo Get from a service
    double m_beam1ParticleMass;
    double m_beam2ParticleMass;
>>>>>>> release/21.0.127
  };

}

#endif //> !ISF_HEPMC_GENEVENTBEAMEFFECTBOOSTER_H
