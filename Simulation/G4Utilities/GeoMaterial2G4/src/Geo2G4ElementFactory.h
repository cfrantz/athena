/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef GEOMATERIAL2G4_ElementFactory_H
#define GEOMATERIAL2G4_ElementFactory_H

class G4Element;
class GeoElement;

#include <map>
#include <string>
typedef std::map<std::string, G4Element*, std::less<std::string> > elList;

class Geo2G4ElementFactory {
public:
  Geo2G4ElementFactory();
  G4Element* Build(const GeoElement*);
private:
<<<<<<< HEAD:Simulation/G4Utilities/GeoMaterial2G4/src/Geo2G4ElementFactory.h
  elList m_definedElements;
=======
  elList definedElements;
>>>>>>> release/21.0.127:Simulation/G4Utilities/Geo2G4/src/Geo2G4ElementFactory.h
};

#endif
