## Add RadLengthIntegrator to the UserActions, and produce a histogram
from G4AtlasApps.SimFlags import simFlags
<<<<<<< HEAD
simFlags.OptionalUserActionList.addAction('G4UA::LengthIntegratorTool')
=======
simFlags.OptionalUserActionList.addAction('G4UA::LengthIntegratorTool',['Event','Step'])
>>>>>>> release/21.0.127


from AthenaCommon.AppMgr import ServiceMgr
from GaudiSvc.GaudiSvcConf import THistSvc
ServiceMgr += THistSvc("THistSvc")
ServiceMgr.THistSvc.Output  = ["lengths DATAFILE='rad_intLength.histo.root' OPT='RECREATE'"]
