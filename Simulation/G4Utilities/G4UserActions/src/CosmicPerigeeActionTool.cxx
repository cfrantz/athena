/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "CosmicPerigeeActionTool.h"
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "CxxUtils/make_unique.h"
#include "G4UserActions/CosmicPerigeeActionTool.h"
>>>>>>> release/21.0.127

namespace G4UA
{

  //---------------------------------------------------------------------------
  // Constructor
  //---------------------------------------------------------------------------
  CosmicPerigeeActionTool::CosmicPerigeeActionTool(const std::string& type,
                                                   const std::string& name,
                                                   const IInterface* parent)
<<<<<<< HEAD
    : UserActionToolBase<CosmicPerigeeAction>(type, name, parent)
  {
=======
    : ActionToolBase<CosmicPerigeeAction>(type, name, parent)
  {
    declareInterface<IG4SteppingActionTool>(this);
    declareInterface<IG4EventActionTool>(this);
    declareInterface<IG4TrackingActionTool>(this);
>>>>>>> release/21.0.127
    declareProperty("pMinPrimary", m_config.pMinPrimary);
  }

  //---------------------------------------------------------------------------
  // Create the action on request
  //---------------------------------------------------------------------------
<<<<<<< HEAD
  std::unique_ptr<CosmicPerigeeAction>
  CosmicPerigeeActionTool::makeAndFillAction(G4AtlasUserActions& actionList)
  {
    ATH_MSG_DEBUG("Creating CosmicPerigeeAction");
    auto action = std::make_unique<CosmicPerigeeAction>(m_config);
    actionList.eventActions.push_back( action.get() );
    actionList.trackingActions.push_back( action.get() );
    actionList.steppingActions.push_back( action.get() );
    return action;
=======
  std::unique_ptr<CosmicPerigeeAction> CosmicPerigeeActionTool::makeAction()
  {
    ATH_MSG_DEBUG("makeAction");
    auto action = CxxUtils::make_unique<CosmicPerigeeAction>(m_config);
    return std::move(action);
>>>>>>> release/21.0.127
  }

}
