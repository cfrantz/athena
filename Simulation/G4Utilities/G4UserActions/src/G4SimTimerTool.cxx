/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "G4SimTimerTool.h"
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "G4UserActions/G4SimTimerTool.h"
#include "CxxUtils/make_unique.h"
>>>>>>> release/21.0.127

namespace G4UA
{

  //---------------------------------------------------------------------------
  // Constructor
  //---------------------------------------------------------------------------
  G4SimTimerTool::G4SimTimerTool(const std::string& type,
                                 const std::string& name,
                                 const IInterface* parent)
<<<<<<< HEAD
    : UserActionToolBase<G4SimTimer>(type, name, parent)
  {
=======
    : ActionToolBaseReport<G4SimTimer>(type, name, parent)
  {
    declareInterface<IG4EventActionTool>(this);
>>>>>>> release/21.0.127
  }

  //---------------------------------------------------------------------------
  // Initialize - temporarily here for debugging
  //---------------------------------------------------------------------------
  StatusCode G4SimTimerTool::initialize()
  {
    ATH_MSG_DEBUG( "Initializing " << name() );
    return StatusCode::SUCCESS;
  }

  //---------------------------------------------------------------------------
  // Merge results from all threads
  //---------------------------------------------------------------------------
  StatusCode G4SimTimerTool::finalize()
  {
    ATH_MSG_DEBUG( "Finalizing " << name() );

<<<<<<< HEAD
    // Accumulate results across threads
    G4SimTimer::Report report;
    m_actions.accumulate(report, &G4SimTimer::getReport,
                         &G4SimTimer::Report::merge);

    // Report the results
    auto meanSigma = report.meanAndSigma();
    ATH_MSG_INFO("Finalized timing results for " << report.nEvent <<
=======
    mergeReports();

    // Report the results
    auto meanSigma = m_report.meanAndSigma();
    ATH_MSG_INFO("Finalized timing results for " << m_report.nEvent <<
>>>>>>> release/21.0.127
                 " events (not all events used)");
    ATH_MSG_INFO("Average time per event was " <<
                 std::setprecision(4) << meanSigma.first << " +- " <<
                 std::setprecision(4) << meanSigma.second);
    return StatusCode::SUCCESS;
  }

  //---------------------------------------------------------------------------
  // Create the action on request
  //---------------------------------------------------------------------------
  std::unique_ptr<G4SimTimer>
<<<<<<< HEAD
  G4SimTimerTool::makeAndFillAction(G4AtlasUserActions& actionList)
  {
    ATH_MSG_DEBUG("Making a G4SimTimer action");
    auto action = std::make_unique<G4SimTimer>();
    actionList.eventActions.push_back( action.get() );
    return action;
=======
  G4SimTimerTool::makeAction()
  {
    ATH_MSG_DEBUG("Making a G4SimTimer action");
    return CxxUtils::make_unique<G4SimTimer>();
>>>>>>> release/21.0.127
  }

}
