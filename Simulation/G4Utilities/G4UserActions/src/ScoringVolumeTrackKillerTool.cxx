/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "ScoringVolumeTrackKillerTool.h"
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "CxxUtils/make_unique.h"
#include "G4UserActions/ScoringVolumeTrackKillerTool.h"
>>>>>>> release/21.0.127

namespace G4UA
{

  //---------------------------------------------------------------------------
  ScoringVolumeTrackKillerTool::ScoringVolumeTrackKillerTool(const std::string& type,
                                                             const std::string& name,
                                                             const IInterface* parent)
<<<<<<< HEAD
    : UserActionToolBase<ScoringVolumeTrackKiller>(type, name, parent)
  {
=======
    : ActionToolBase<ScoringVolumeTrackKiller>(type, name, parent)
  {
    declareInterface<IG4EventActionTool>(this);
    declareInterface<IG4SteppingActionTool>(this);
>>>>>>> release/21.0.127
  }

  //---------------------------------------------------------------------------
  std::unique_ptr<ScoringVolumeTrackKiller>
<<<<<<< HEAD
  ScoringVolumeTrackKillerTool::makeAndFillAction(G4AtlasUserActions& actionList)
  {
    ATH_MSG_DEBUG("Making a ScoringVolumeTrackKiller action");
    auto action = std::make_unique<ScoringVolumeTrackKiller>();
    actionList.eventActions.push_back( action.get() );
    actionList.steppingActions.push_back( action.get() );
    return action;
=======
  ScoringVolumeTrackKillerTool::makeAction()
  {
    ATH_MSG_DEBUG("makeAction");
    return CxxUtils::make_unique<ScoringVolumeTrackKiller>();
>>>>>>> release/21.0.127
  }

} // namespace G4UA
