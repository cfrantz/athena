/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "HIPKillerTool.h"
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "CxxUtils/make_unique.h"
#include "G4UserActions/HIPKillerTool.h"
>>>>>>> release/21.0.127

namespace G4UA
{

  //---------------------------------------------------------------------------
  HIPKillerTool::HIPKillerTool(const std::string& type, const std::string& name,
                               const IInterface* parent)
<<<<<<< HEAD
    : UserActionToolBase<HIPKiller>(type, name, parent)
  {
  }

  //---------------------------------------------------------------------------
  std::unique_ptr<HIPKiller>
  HIPKillerTool::makeAndFillAction(G4AtlasUserActions& actionList)
  {
    ATH_MSG_DEBUG("Making a HIPKiller action");
    auto action = std::make_unique<HIPKiller>();
    actionList.steppingActions.push_back( action.get() );
    return action;
=======
    : ActionToolBase<HIPKiller>(type, name, parent)
  {
    declareInterface<IG4SteppingActionTool>(this);
  }

  //---------------------------------------------------------------------------
  std::unique_ptr<HIPKiller>  HIPKillerTool::makeAction()
  {
    ATH_MSG_DEBUG("makeAction");
    auto action = CxxUtils::make_unique<HIPKiller>();
    return std::move(action);
>>>>>>> release/21.0.127
  }

} // namespace G4UA
