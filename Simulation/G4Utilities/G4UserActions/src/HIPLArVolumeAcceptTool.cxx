/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "HIPLArVolumeAcceptTool.h"
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "CxxUtils/make_unique.h"
#include "G4UserActions/HIPLArVolumeAcceptTool.h"
>>>>>>> release/21.0.127

namespace G4UA
{

  //---------------------------------------------------------------------------
  HIPLArVolumeAcceptTool::HIPLArVolumeAcceptTool(const std::string& type,
                                                 const std::string& name,
                                                 const IInterface* parent)
<<<<<<< HEAD
    : UserActionToolBase<HIPLArVolumeAccept>(type, name, parent)
  {
  }

  //---------------------------------------------------------------------------
  std::unique_ptr<HIPLArVolumeAccept>
  HIPLArVolumeAcceptTool::makeAndFillAction(G4AtlasUserActions& actionList)
  {
    ATH_MSG_DEBUG("Making a HIPLArVolumeAccept action");
    auto action = std::make_unique<HIPLArVolumeAccept>();
    actionList.eventActions.push_back( action.get() );
    actionList.steppingActions.push_back( action.get() );
    return action;
=======
    : ActionToolBaseReport<HIPLArVolumeAccept>(type, name, parent)
  {
    declareInterface<IG4SteppingActionTool>(this);
    declareInterface<IG4EventActionTool>(this);
  }

  //---------------------------------------------------------------------------
  std::unique_ptr<HIPLArVolumeAccept> HIPLArVolumeAcceptTool::makeAction()
  {
    ATH_MSG_DEBUG("makeAction");
    return CxxUtils::make_unique<HIPLArVolumeAccept>();
>>>>>>> release/21.0.127
  }

  //---------------------------------------------------------------------------
  StatusCode HIPLArVolumeAcceptTool::finalize()
  {
<<<<<<< HEAD
    // Accumulate the results across threads
    HIPLArVolumeAccept::Report report;
    m_actions.accumulate(report, &HIPLArVolumeAccept::getReport,
                         &HIPLArVolumeAccept::Report::merge);
=======
    mergeReports();
>>>>>>> release/21.0.127

    ATH_MSG_INFO("#########################################");
    ATH_MSG_INFO("##                                     ##");
    ATH_MSG_INFO( "##    HIPLArVolumeAccept - EndOfRun    ##");
    ATH_MSG_INFO( "##                                     ##");
    ATH_MSG_INFO( "#########################################");
<<<<<<< HEAD
    ATH_MSG_INFO(report.HIPevts       <<" events were processed by HIPLArVolumeAccept");
    ATH_MSG_INFO(report.HIPevts_failed<<" events were killed because they had no HIP in EMB or EMEC");
    double HIPfraction=1.*(report.HIPevts-report.HIPevts_failed)/report.HIPevts;
=======
    ATH_MSG_INFO(m_report.HIPevts       <<" events were processed by HIPLArVolumeAccept");
    ATH_MSG_INFO(m_report.HIPevts_failed<<" events were killed because they had no HIP in EMB or EMEC");
    double HIPfraction=1.*(m_report.HIPevts-m_report.HIPevts_failed)/m_report.HIPevts;
>>>>>>> release/21.0.127
    ATH_MSG_INFO("HIP Acceptance: "<<HIPfraction);

    return StatusCode::SUCCESS;
  }

} // namespace G4UA
