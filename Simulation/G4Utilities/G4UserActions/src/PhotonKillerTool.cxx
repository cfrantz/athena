/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "PhotonKillerTool.h"
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "G4UserActions/PhotonKillerTool.h"
#include "CxxUtils/make_unique.h"
>>>>>>> release/21.0.127

namespace G4UA
{

  //---------------------------------------------------------------------------
  PhotonKillerTool::PhotonKillerTool(const std::string& type,
                                     const std::string& name,
                                     const IInterface* parent)
<<<<<<< HEAD
    : UserActionToolBase<PhotonKiller>(type, name, parent)
  {
  }

  //---------------------------------------------------------------------------
  std::unique_ptr<PhotonKiller>
  PhotonKillerTool::makeAndFillAction(G4AtlasUserActions& actionList)
  {
    ATH_MSG_DEBUG("Making a PhotonKiller action");
    auto action = std::make_unique<PhotonKiller>();
    actionList.trackingActions.push_back( action.get() );
    actionList.steppingActions.push_back( action.get() );
    return action;
=======
    : ActionToolBase<PhotonKiller>(type, name, parent)
  {
    declareInterface<IG4SteppingActionTool>(this);
    declareInterface<IG4TrackingActionTool>(this);
  }

  //---------------------------------------------------------------------------
  std::unique_ptr<PhotonKiller> PhotonKillerTool::makeAction(){
    ATH_MSG_DEBUG("Making a PhotonKiller action");
    return CxxUtils::make_unique<PhotonKiller>();
>>>>>>> release/21.0.127
  }

} // namespace G4UA
