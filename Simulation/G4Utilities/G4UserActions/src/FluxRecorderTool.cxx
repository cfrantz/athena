/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "FluxRecorderTool.h"
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "CxxUtils/make_unique.h"
#include "G4UserActions/FluxRecorderTool.h"
>>>>>>> release/21.0.127

namespace G4UA
{

  FluxRecorderTool::FluxRecorderTool(const std::string& type,
                                     const std::string& name,
                                     const IInterface* parent)
<<<<<<< HEAD
    : UserActionToolBase<FluxRecorder>(type, name, parent)
  {
  }

  std::unique_ptr<FluxRecorder>
  FluxRecorderTool::makeAndFillAction(G4AtlasUserActions& actionList)
  {
    ATH_MSG_DEBUG("Constructing a FluxRecorder action");
    auto action = std::make_unique<FluxRecorder>();
    actionList.runActions.push_back( action.get() );
    actionList.eventActions.push_back( action.get() );
    actionList.steppingActions.push_back( action.get() );
    return action;
=======
    : ActionToolBase<FluxRecorder>(type, name, parent)
  {
    declareInterface<IG4RunActionTool>(this);
    declareInterface<IG4EventActionTool>(this);
    declareInterface<IG4SteppingActionTool>(this);
  }

  std::unique_ptr<FluxRecorder> FluxRecorderTool::makeAction()
  {
    ATH_MSG_DEBUG("makeAction");
    return CxxUtils::make_unique<FluxRecorder>();
>>>>>>> release/21.0.127
  }

} // namespace G4UA
