/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "StoppedParticleActionTool.h"
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "CxxUtils/make_unique.h"
#include "G4UserActions/StoppedParticleActionTool.h"
>>>>>>> release/21.0.127

namespace G4UA
{

  //---------------------------------------------------------------------------
  StoppedParticleActionTool::StoppedParticleActionTool(const std::string& type,
                                                       const std::string& name,
                                                       const IInterface* parent)
<<<<<<< HEAD
    : UserActionToolBase<StoppedParticleAction>(type, name, parent)
  {
  }

  //---------------------------------------------------------------------------
  std::unique_ptr<StoppedParticleAction>
  StoppedParticleActionTool::makeAndFillAction(G4AtlasUserActions& actionList)
  {
    ATH_MSG_DEBUG("Making a StoppedParticleAction");
    auto action = std::make_unique<StoppedParticleAction>();
    actionList.steppingActions.push_back( action.get() );
    return action;
=======
    : ActionToolBase<StoppedParticleAction>(type, name, parent)
  {
    declareInterface<IG4SteppingActionTool>(this);
  }

  //---------------------------------------------------------------------------
  std::unique_ptr<StoppedParticleAction> StoppedParticleActionTool::makeAction()
  {
    ATH_MSG_DEBUG("makeAction");
    return CxxUtils::make_unique<StoppedParticleAction>();
>>>>>>> release/21.0.127
  }

} // namespace G4UA
