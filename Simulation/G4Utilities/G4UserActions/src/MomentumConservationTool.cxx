/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "MomentumConservationTool.h"
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "CxxUtils/make_unique.h"
#include "G4UserActions/MomentumConservationTool.h"
>>>>>>> release/21.0.127

namespace G4UA
{

  //---------------------------------------------------------------------------
  MomentumConservationTool::MomentumConservationTool(const std::string& type,
                                                     const std::string& name,
                                                     const IInterface* parent)
<<<<<<< HEAD
    : UserActionToolBase<MomentumConservation>(type, name, parent)
  {
  }

  //---------------------------------------------------------------------------
  std::unique_ptr<MomentumConservation>
  MomentumConservationTool::makeAndFillAction(G4AtlasUserActions& actionList)
  {
    ATH_MSG_DEBUG("Making a MomentumConservation action");
    auto action = std::make_unique<MomentumConservation>();
    actionList.eventActions.push_back( action.get() );
    actionList.steppingActions.push_back( action.get() );
    return action;
=======
    : ActionToolBase<MomentumConservation>(type, name, parent)
  {
    declareInterface<IG4EventActionTool>(this);
    declareInterface<IG4SteppingActionTool>(this);
  }

  //---------------------------------------------------------------------------
  std::unique_ptr<MomentumConservation>  MomentumConservationTool::makeAction(){
    ATH_MSG_DEBUG("makeAction");
    auto action = CxxUtils::make_unique<MomentumConservation>();
    return std::move(action);
>>>>>>> release/21.0.127
  }

} // namespace G4UA
