/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "ScoringPlaneTool.h"
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "CxxUtils/make_unique.h"
#include "G4UserActions/ScoringPlaneTool.h"
>>>>>>> release/21.0.127


namespace G4UA
{

  ScoringPlaneTool::ScoringPlaneTool(const std::string& type,
                                     const std::string& name,
                                     const IInterface* parent)
<<<<<<< HEAD
    : UserActionToolBase<ScoringPlane>(type, name, parent)
  {
=======
    : ActionToolBase<ScoringPlane>(type, name, parent)
  {
    declareInterface<IG4RunActionTool>(this);
    declareInterface<IG4SteppingActionTool>(this);
    declareInterface<IG4EventActionTool>(this);
>>>>>>> release/21.0.127
    declareProperty("Plane", m_config.plane);
    declareProperty("PKill", m_config.pkill);
    declareProperty("FName", m_config.fname);
  }

<<<<<<< HEAD
  std::unique_ptr<ScoringPlane>
  ScoringPlaneTool::makeAndFillAction(G4AtlasUserActions& actionList)
  {
    ATH_MSG_DEBUG("Making a ScoringPlane action");
    auto action = std::make_unique<ScoringPlane>(m_config);
    actionList.runActions.push_back( action.get() );
    actionList.eventActions.push_back( action.get() );
    actionList.steppingActions.push_back( action.get() );
    return action;
=======
  std::unique_ptr<ScoringPlane> ScoringPlaneTool::makeAction()
  {
    ATH_MSG_DEBUG("makeAction");
    return CxxUtils::make_unique<ScoringPlane>(m_config);
>>>>>>> release/21.0.127
  }

} // namespace G4UA
