#include "../G4SimTimerTool.h"
#include "../G4TrackCounterTool.h"
#include "../LooperKillerTool.h"
// This is a weird one... Geant4 declares 'times' as 'otimes' in some of its
// headers. That can have a catastrophic interference with how Boost uses the
// `times` variable itself. This next line just gets rid of one such
// catastrophic interference. Though this is super fragile...
#undef times
#include "../CosmicPerigeeActionTool.h"
#include "../MomentumConservationTool.h"
#include "../FastIDKillerTool.h"
#include "../HIPKillerTool.h"
#include "../LengthIntegratorTool.h"
#include "../HIPLArVolumeAcceptTool.h"
#include "../HitWrapperTool.h"
#include "../PhotonKillerTool.h"
#include "../ScoringVolumeTrackKillerTool.h"
#include "../StoppedParticleActionTool.h"
#include "../FluxRecorderTool.h"
#include "../ScoringPlaneTool.h"
#include "../RadiationMapsMakerTool.h"
#include "../RadLengthActionTool.h"
#include "../LooperThresholdSetTool.h"
#include "../VolumeDumperTool.h"
#include "../TestActionTool.h"
#include "../AthenaStackingActionTool.h"
#include "../AthenaTrackingActionTool.h"

<<<<<<< HEAD
DECLARE_COMPONENT( G4UA::G4SimTimerTool )
DECLARE_COMPONENT( G4UA::G4TrackCounterTool )
DECLARE_COMPONENT( G4UA::LooperKillerTool )
DECLARE_COMPONENT( G4UA::CosmicPerigeeActionTool )
DECLARE_COMPONENT( G4UA::MomentumConservationTool )
DECLARE_COMPONENT( G4UA::FastIDKillerTool )
DECLARE_COMPONENT( G4UA::HIPKillerTool )
DECLARE_COMPONENT( G4UA::LengthIntegratorTool )
DECLARE_COMPONENT( G4UA::HIPLArVolumeAcceptTool )
DECLARE_COMPONENT( G4UA::HitWrapperTool )
DECLARE_COMPONENT( G4UA::PhotonKillerTool )
DECLARE_COMPONENT( G4UA::ScoringVolumeTrackKillerTool )
DECLARE_COMPONENT( G4UA::StoppedParticleActionTool )
DECLARE_COMPONENT( G4UA::FluxRecorderTool )
DECLARE_COMPONENT( G4UA::ScoringPlaneTool )
DECLARE_COMPONENT( G4UA::RadiationMapsMakerTool )
DECLARE_COMPONENT( G4UA::RadLengthActionTool )
DECLARE_COMPONENT( G4UA::LooperThresholdSetTool )
DECLARE_COMPONENT( G4UA::TestActionTool )
DECLARE_COMPONENT( G4UA::VolumeDumperTool )
DECLARE_COMPONENT( G4UA::AthenaStackingActionTool )
DECLARE_COMPONENT( G4UA::AthenaTrackingActionTool )
=======
#include "G4UserActions/G4SimTimerTool.h"
#include "G4UserActions/G4TrackCounterTool.h"
#include "G4UserActions/LooperKillerTool.h"
#include "G4UserActions/CosmicPerigeeActionTool.h"
#include "G4UserActions/MomentumConservationTool.h"
#include "G4UserActions/FastIDKillerTool.h"
#include "G4UserActions/HIPKillerTool.h"
#include "G4UserActions/LengthIntegratorTool.h"
#include "G4UserActions/HIPLArVolumeAcceptTool.h"
#include "G4UserActions/HitWrapperTool.h"
#include "G4UserActions/PhotonKillerTool.h"
#include "G4UserActions/ScoringVolumeTrackKillerTool.h"
#include "G4UserActions/StoppedParticleActionTool.h"
#include "G4UserActions/FluxRecorderTool.h"
#include "G4UserActions/ScoringPlaneTool.h"

DECLARE_TOOL_FACTORY( G4UA::G4SimTimerTool )
DECLARE_TOOL_FACTORY( G4UA::G4TrackCounterTool )
DECLARE_TOOL_FACTORY( G4UA::LooperKillerTool )
DECLARE_TOOL_FACTORY( G4UA::CosmicPerigeeActionTool )
DECLARE_TOOL_FACTORY( G4UA::MomentumConservationTool )
DECLARE_TOOL_FACTORY( G4UA::FastIDKillerTool )
DECLARE_TOOL_FACTORY( G4UA::HIPKillerTool )
DECLARE_TOOL_FACTORY( G4UA::LengthIntegratorTool )
DECLARE_TOOL_FACTORY( G4UA::HIPLArVolumeAcceptTool )
DECLARE_TOOL_FACTORY( G4UA::HitWrapperTool )
DECLARE_TOOL_FACTORY( G4UA::PhotonKillerTool )
DECLARE_TOOL_FACTORY( G4UA::ScoringVolumeTrackKillerTool )
DECLARE_TOOL_FACTORY( G4UA::StoppedParticleActionTool )
DECLARE_TOOL_FACTORY( G4UA::FluxRecorderTool )
DECLARE_TOOL_FACTORY( G4UA::ScoringPlaneTool )


DECLARE_FACTORY_ENTRIES( G4UserActions ) {
  DECLARE_TOOL( G4UA::G4SimTimerTool )
  DECLARE_TOOL( G4UA::G4TrackCounterTool )
  DECLARE_TOOL( G4UA::LooperKillerTool )
  DECLARE_TOOL( G4UA::CosmicPerigeeActionTool )
  DECLARE_TOOL( G4UA::MomentumConservationTool )
  DECLARE_TOOL( G4UA::FastIDKillerTool )
  DECLARE_TOOL( G4UA::HIPKillerTool )
  DECLARE_TOOL( G4UA::LengthIntegratorTool )
  DECLARE_TOOL( G4UA::HIPLArVolumeAcceptTool )
  DECLARE_TOOL( G4UA::HitWrapperTool )
  DECLARE_TOOL( G4UA::PhotonKillerTool )
  DECLARE_TOOL( G4UA::ScoringVolumeTrackKillerTool )
  DECLARE_TOOL( G4UA::StoppedParticleActionTool )
  DECLARE_TOOL( G4UA::FluxRecorderTool )
  DECLARE_TOOL( G4UA::ScoringPlaneTool )

}
>>>>>>> release/21.0.127
