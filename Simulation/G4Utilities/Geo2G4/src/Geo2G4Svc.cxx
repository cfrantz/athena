/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "Geo2G4Svc.h"
<<<<<<< HEAD
#include "Geo2G4AssemblyVolume.h"
#include "ExtParameterisedVolumeBuilder.h"

// separate function not part of this class (why?)
BuilderMap InitializeBuilders(Geo2G4AssemblyFactory*);

Geo2G4Svc::Geo2G4Svc(const std::string& name, ISvcLocator* svcLocator)
  : base_class(name,svcLocator)
  , m_defaultBuilder()
  , m_getTopTransform(true)
  , m_G4AssemblyFactory(nullptr)
=======
#include "VolumeBuilder.h"

void InitializeBuilders();

Geo2G4Svc::Geo2G4Svc(const std::string& name, ISvcLocator* svcLocator)
  : AthService(name,svcLocator)
  , m_defaultBuilder(nullptr)
  , m_getTopTransform(true)
>>>>>>> release/21.0.127
{
  ATH_MSG_VERBOSE ("Creating the Geo2G4Svc.");
  declareProperty("GetTopTransform", m_getTopTransform);
}
Geo2G4Svc::~Geo2G4Svc()
{;}

StatusCode Geo2G4Svc::initialize()
{
  static int initialized=0;
  if (initialized)
    {
      ATH_MSG_VERBOSE (" Geo2G4Svc already initialized.");
      return StatusCode::SUCCESS;
<<<<<<< HEAD
    }
  ATH_MSG_VERBOSE ("Initializing the Geo2G4Svc.");
  ATH_MSG_VERBOSE ("Creating all builders available.");
  m_G4AssemblyFactory = std::make_unique<Geo2G4AssemblyFactory>();
  m_builders = InitializeBuilders(m_G4AssemblyFactory.get());
  
  const std::string nameBuilder = "Extended_Parameterised_Volume_Builder"; //TODO Configurable property??
  this->SetDefaultBuilder(nameBuilder);
  ATH_MSG_VERBOSE (nameBuilder << " --> set as default builder" );
  ATH_MSG_VERBOSE (nameBuilder << " --> ParamOn flag = " << GetDefaultBuilder()->GetParam());
  initialized=1;
  if(msgLvl(MSG::VERBOSE))
    {
      this->ListVolumeBuilders();
    }
=======
    }
  ATH_MSG_VERBOSE ("Initializing the Geo2G4Svc.");
  ATH_MSG_VERBOSE ("Creating all builders available.");
  InitializeBuilders(); // separate function not part of this class

  const std::string nameBuilder = "Extended_Parameterised_Volume_Builder"; //TODO Configurable property??
  this->SetDefaultBuilder(nameBuilder);
  ATH_MSG_VERBOSE (nameBuilder << " --> set as default builder" );
  ATH_MSG_VERBOSE (nameBuilder << " --> ParamOn flag = " << m_defaultBuilder->GetParam());
  initialized=1;
  if(msgLvl(MSG::VERBOSE))
    {
      this->ListVolumeBuilders();
    }
>>>>>>> release/21.0.127
  return StatusCode::SUCCESS;
}

StatusCode Geo2G4Svc::finalize()
{
  ATH_MSG_VERBOSE ("Finalizing the Geo2G4Svc.");
  return StatusCode::SUCCESS;
}

<<<<<<< HEAD
=======
StatusCode Geo2G4Svc::queryInterface(const InterfaceID& riid, void**ppvInt)
{
  const InterfaceID& iid=IGeo2G4Svc::interfaceID();
  if (riid==iid)
    {
      *ppvInt=dynamic_cast<IGeo2G4Svc*>(this);
      return StatusCode::SUCCESS;
    }
  return AthService::queryInterface(riid,ppvInt);
}

>>>>>>> release/21.0.127
void Geo2G4Svc::handle(const Incident& )
{
}

void Geo2G4Svc::ListVolumeBuilders() const
{
  ATH_MSG_INFO("---- List of all Volume Builders registered with Geo2G4Svc ----");
  ATH_MSG_INFO("---------------------------------------------------------------");
  for (const auto& builder : m_builders)
    {
      ATH_MSG_INFO(" Volume Builder: "<<builder.second->GetKey());
    }
  ATH_MSG_INFO("---------------------------------------------------------------");
  ATH_MSG_INFO(" default builder is "<<GetDefaultBuilder()->GetKey());
}

<<<<<<< HEAD
=======
void Geo2G4Svc::ListVolumeBuilders() const
{
  ATH_MSG_INFO("---- List of all Volume Builders registered with Geo2G4Svc ----");
  ATH_MSG_INFO("---------------------------------------------------------------");
  for (const auto& builder : m_builders)
    {
      ATH_MSG_INFO(" Volume Builder: "<<builder.second->GetKey());
    }
  ATH_MSG_INFO("---------------------------------------------------------------");
  ATH_MSG_INFO(" default builder is "<<m_defaultBuilder->GetKey());
}

void Geo2G4Svc::UnregisterVolumeBuilder(VolumeBuilder* vb)
{
  const std::string key(vb->GetKey());
  if (m_builders.find(key)!=m_builders.end())
    {
      ATH_MSG_DEBUG ("Removing builder "<<key<<" from the list");
      m_builders.erase(key);
    }
  else
    {
      ATH_MSG_ERROR ("Trying to remove a not-existing builder "<<key);
      ATH_MSG_ERROR ("\t request ignored, nothing done ");
    }
}

>>>>>>> release/21.0.127
VolumeBuilder* Geo2G4Svc::GetVolumeBuilder(std::string s) const
{
  const auto builderItr(m_builders.find(s));
  if (builderItr!=m_builders.end())
    {
<<<<<<< HEAD
      return builderItr->second.get();
=======
      return builderItr->second;
>>>>>>> release/21.0.127
    }
  else
    {
      ATH_MSG_ERROR ("Trying to retrieve a not existing builder "<<s);
      ATH_MSG_ERROR ("\treturning Default Builder");
    }
<<<<<<< HEAD
  return GetDefaultBuilder();
=======
  return m_defaultBuilder;
>>>>>>> release/21.0.127
}
