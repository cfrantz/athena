//Dear emacs, this is -*- C++ -*-.

/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/


#ifndef CALOTPCNV_CALOCLUSTERCELLLINKCNTCNV_P1
#define CALOTPCNV_CALOCLUSTERCELLLINKCNTCNV_P1

<<<<<<< HEAD
=======
#include "AthenaKernel/ITPCnvBase.h"
>>>>>>> release/21.0.127
#include "CaloEvent/CaloClusterCellLinkContainer.h"
#include "CaloTPCnv/CaloClusterCellLinkContainer_p1.h"
#include "AthenaPoolCnvSvc/T_AthenaPoolTPConverter.h"
#include "DataModelAthenaPool/DataLinkCnv_p2.h"

/**
 * @brief T/P conversions for CaloClusterCellLinkContainerCnv_p1
 */
class CaloClusterCellLinkContainerCnv_p1
<<<<<<< HEAD
  : public T_AthenaPoolTPCnvWithKeyBase<CaloClusterCellLinkContainer, CaloClusterCellLinkContainer_p1>
{
public:
  using base_class::transToPersWithKey;
  using base_class::persToTransWithKey;


=======
  : public ITPCnvBase
{
public:
>>>>>>> release/21.0.127
  /**
   * @brief Convert from persistent to transient object.
   * @param pers The persistent object to convert.
   * @param trans The transient object to which to convert.
<<<<<<< HEAD
   * @param key SG key of the object being read.
   * @param log Error logging stream.
   */
  virtual
  void persToTransWithKey (const CaloClusterCellLinkContainer_p1* pers,
                           CaloClusterCellLinkContainer* trans,
                           const std::string& key,
                           MsgStream& log) const override;
=======
   * @param log Error logging stream.
   */
  virtual void persToTrans(const CaloClusterCellLinkContainer_p1* pers,
			   CaloClusterCellLinkContainer* trans,
			   MsgStream& log);
>>>>>>> release/21.0.127


  /**
   * @brief Convert from transient to persistent object.
   * @param trans The transient object to convert.
   * @param pers The persistent object to which to convert.
<<<<<<< HEAD
   * @param key SG key of the object being written.
   * @param log Error logging stream.
   */
  virtual
  void transToPersWithKey (const CaloClusterCellLinkContainer* trans,
                           CaloClusterCellLinkContainer_p1* pers,
                           const std::string& key,
                           MsgStream& log) const override;
=======
   * @param log Error logging stream.
   */
  virtual void transToPers(const CaloClusterCellLinkContainer* trans,
			   CaloClusterCellLinkContainer_p1* pers,
			   MsgStream &log);



  // Methods for invoking conversions on objects given by generic pointers.
  virtual void persToTransUntyped(const void* pers,
                                  void* trans,
                                  MsgStream& log);
  virtual void transToPersUntyped(const void* trans,
                                  void* pers,
                                  MsgStream& log);


  virtual const std::type_info& transientTInfo() const;

  /** return C++ type id of the persistent class this converter is for
      @return std::type_info&
  */
  virtual const std::type_info& persistentTInfo() const;


>>>>>>> release/21.0.127


private:
  DataLinkCnv_p2<DataLink<CaloCellContainer> > m_linkCnv;

};

<<<<<<< HEAD

=======
>>>>>>> release/21.0.127
#endif
