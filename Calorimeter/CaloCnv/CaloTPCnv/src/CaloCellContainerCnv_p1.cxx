/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "CaloTPCnv/CaloCellContainerCnv_p1.h"
#include "CaloEvent/CaloCellContainer.h"
#include "CaloEvent/CaloCompactCellContainer.h"
#include "CaloCompactCellTool.h"
#include "AthenaKernel/getThinningCache.h"

<<<<<<< HEAD
=======
CaloCellContainerCnv_p1::CaloCellContainerCnv_p1 () {
  m_compactCellTool = nullptr;
  init();
}

void CaloCellContainerCnv_p1::init(){
>>>>>>> release/21.0.127

CaloCellContainerCnv_p1::CaloCellContainerCnv_p1 () {}

<<<<<<< HEAD
=======
		sc = toolSvc->retrieveTool("CaloCompactCellTool", m_compactCellTool);
		if (sc.isFailure()) {
			std::cout << "CaloCellContainerCnv_p1: Could not get CaloCompactCellTool." << std::endl;
			m_compactCellTool = nullptr;
			return;
		}
	}
return;
}
>>>>>>> release/21.0.127

void CaloCellContainerCnv_p1::persToTransWithKey(const CaloCompactCellContainer* pers,
                                                 CaloCellContainer* trans,
                                                 const std::string& /*key*/,
                                                 MsgStream& log) const
{
  trans->clear();
  CaloCompactCellTool compactCellTool;
  if (compactCellTool.getTransient(*pers, trans).isFailure()) {
    log << MSG::ERROR << " CaloCellContainerCnv_p1: Could not get transient" << endmsg;
  }
}


void CaloCellContainerCnv_p1::transToPersWithKey(const CaloCellContainer* trans,
                                                 CaloCompactCellContainer* pers,
                                                 const std::string& key,
                                                 MsgStream& log) const
{
  CaloCompactCellTool compactCellTool;

  if (compactCellTool.getPersistent (*trans,
                                     pers,
                                     SG::getThinningDecision (key),
                                     CaloCompactCellTool::VERSION_LATEST).isFailure()) {
    log << MSG::ERROR << " CaloCellContainerCnv_p1: Could not get persistent" << endmsg;
  }
}
