<<<<<<< HEAD
#include "../CaloCellNoiseAlg.h"
#include "../CaloNoise2Ntuple.h"
#include "../CaloRescaleNoise.h"
#include "../CaloFillCellPositionShift.h"
#include "../CaloCellPosition2Ntuple.h"
#include "../CaloAddCellPedShift.h"
#include "../FCAL_HV_Energy_Rescale.h"
#include "../CaloCellCalcEnergyCorr.h"
#include "../CaloCellEnergyCorr2Ntuple.h"
#include "../LArMinBiasAlg.h"

DECLARE_COMPONENT( CaloCellNoiseAlg )
DECLARE_COMPONENT( CaloNoise2Ntuple )
DECLARE_COMPONENT( CaloFillCellPositionShift )
DECLARE_COMPONENT( CaloCellPosition2Ntuple )
DECLARE_COMPONENT( CaloRescaleNoise )
DECLARE_COMPONENT( CaloAddCellPedShift )
DECLARE_COMPONENT( FCAL_HV_Energy_Rescale )
DECLARE_COMPONENT( CaloCellCalcEnergyCorr )
DECLARE_COMPONENT( CaloCellEnergyCorr2Ntuple )
DECLARE_COMPONENT( LArMinBiasAlg )
  
=======
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "CaloCondPhysAlgs/CaloCellNoiseAlg.h"
#include "CaloCondPhysAlgs/CaloNoise2Ntuple.h"
#include "CaloCondPhysAlgs/CaloRescaleNoise.h"
#include "CaloCondPhysAlgs/CaloFillCellPositionShift.h"
#include "CaloCondPhysAlgs/CaloCellPosition2Ntuple.h"
#include "CaloCondPhysAlgs/CaloAddCellPedShift.h"
#include "CaloCondPhysAlgs/FCAL_HV_Energy_Rescale.h"
#include "CaloCondPhysAlgs/CaloCellCalcEnergyCorr.h"
#include "CaloCondPhysAlgs/CaloCellEnergyCorr2Ntuple.h"
#include "CaloCondPhysAlgs/LArMinBiasAlg.h"
#include "CaloCondPhysAlgs/LArHVMapTool.h"

DECLARE_ALGORITHM_FACTORY( CaloCellNoiseAlg )
DECLARE_ALGORITHM_FACTORY( CaloNoise2Ntuple )
DECLARE_ALGORITHM_FACTORY( CaloFillCellPositionShift )
DECLARE_ALGORITHM_FACTORY( CaloCellPosition2Ntuple )
DECLARE_ALGORITHM_FACTORY( CaloRescaleNoise )
DECLARE_ALGORITHM_FACTORY( CaloAddCellPedShift )
DECLARE_ALGORITHM_FACTORY( FCAL_HV_Energy_Rescale )
DECLARE_ALGORITHM_FACTORY( CaloCellCalcEnergyCorr )
DECLARE_ALGORITHM_FACTORY( CaloCellEnergyCorr2Ntuple )
DECLARE_ALGORITHM_FACTORY( LArMinBiasAlg)
DECLARE_TOOL_FACTORY(LArHVMapTool)
  
DECLARE_FACTORY_ENTRIES(CaloCondPhysAlgs) {
  DECLARE_ALGORITHM( CaloCellNoiseAlg )
  DECLARE_ALGORITHM( CaloNoise2Ntuple )
  DECLARE_ALGORITHM( CaloRescaleNoise )
  DECLARE_ALGORITHM( CaloFillCellPositionShift )
  DECLARE_ALGORITHM( CaloCellPosition2Ntuple )
  DECLARE_ALGORITHM( CaloAddCellPedShift )
  DECLARE_ALGORITHM( FCAL_HV_Energy_Rescale )
  DECLARE_ALGORITHM( CaloCellCalcEnergyCorr )
  DECLARE_ALGORITHM( CaloCellEnergyCorr2Ntuple )
  DECLARE_ALGORITHM( LArMinBiasAlg)
  DECLARE_ALGTOOL( LArHVMapTool)
}
>>>>>>> release/21.0.127

