/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

<<<<<<< HEAD
#include "CaloG4Sim/CalibrationDefaultProcessingTool.h"

namespace G4UA
{

  namespace CaloG4
  {

    CalibrationDefaultProcessingTool::
    CalibrationDefaultProcessingTool(const std::string& type,
                                     const std::string& name,
                                     const IInterface* parent)
      : UserActionToolBase<CalibrationDefaultProcessing>(type, name, parent)
    {
      declareProperty("SDName", m_config.SDName);
    }

    std::unique_ptr<CalibrationDefaultProcessing>
    CalibrationDefaultProcessingTool::makeAndFillAction(G4AtlasUserActions& actionList)
    {
      ATH_MSG_DEBUG("Constructing a CalibrationDefaultProcessing action");
      auto action = std::make_unique<CalibrationDefaultProcessing>(m_config);
      actionList.eventActions.push_back( action.get() );
      actionList.steppingActions.push_back( action.get() );
      return action;
    }

  } // namespace CaloG4

} // namespace G4UA
=======
#include "CxxUtils/make_unique.h"
#include "CaloG4Sim/CalibrationDefaultProcessingTool.h"

namespace G4UA{ 
  
  namespace CaloG4{
    
    CalibrationDefaultProcessingTool::CalibrationDefaultProcessingTool(const std::string& type, const std::string& name,const IInterface* parent):
      
      ActionToolBase<CalibrationDefaultProcessing>(type, name, parent), m_config(){
      
      declareProperty("SDName", m_config.SDName);
      
    }
    
    std::unique_ptr<CalibrationDefaultProcessing>  CalibrationDefaultProcessingTool::makeAction(){
      ATH_MSG_DEBUG("makeAction");
      auto action = CxxUtils::make_unique<CalibrationDefaultProcessing>(m_config);
      return std::move(action);
    }
    
    StatusCode CalibrationDefaultProcessingTool::queryInterface(const InterfaceID& riid, void** ppvIf){
      
      if(riid == IG4EventActionTool::interfaceID()) {
	*ppvIf = (IG4EventActionTool*) this;
	addRef();
	return StatusCode::SUCCESS;
      }
      if(riid == IG4SteppingActionTool::interfaceID()) {
	*ppvIf = (IG4SteppingActionTool*) this;
	addRef();
	return StatusCode::SUCCESS;
      }
      return ActionToolBase<CalibrationDefaultProcessing>::queryInterface(riid, ppvIf);
    }
    
  } // namespace CaloG4

} // namespace G4UA 
>>>>>>> release/21.0.127
