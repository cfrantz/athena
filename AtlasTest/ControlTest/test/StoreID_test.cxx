/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/***************************************************************************
 test the store ID setting
 -------------------------------------------
 ATLAS Collaboration
 ***************************************************************************/

<<<<<<< HEAD
// $Id: StoreID_test.cxx 770224 2016-08-26 01:59:31Z ssnyder $
=======
// $Id: StoreID_test.cxx 753911 2016-06-09 13:40:32Z calaf $
>>>>>>> release/21.0.127

#include <iostream>

#undef NDEBUG

#include "StoreGate/StoreGateSvc.h"
#include "StoreGate/SGtests.h"


#ifndef NOGAUDI
#include "TestTools/initGaudi.h"
using namespace Athena_test;
using namespace std;

int main() {
  cout << "*** StoreID_test BEGINS ***" <<endl;
  ISvcLocator* pSvcLoc;
  if (!initGaudi("StoreGateTestCommon.txt", pSvcLoc)) {
    cerr << "This test can not be run" << endl;
    return 0;
  }  
  assert(pSvcLoc);

<<<<<<< HEAD
  StoreGateSvc* pStore(nullptr);
=======
  StoreGateSvc* pStore(0);
>>>>>>> release/21.0.127
  static const bool CREATE(true);
  assert((pSvcLoc->service("StoreGateSvc", pStore, CREATE)).isSuccess());
  assert(pStore);
  
  assert(pStore->storeID() == StoreID::EVENT_STORE);
<<<<<<< HEAD
=======
  pStore->setStoreID(StoreID::SPARE_STORE);
  assert(pStore->storeID() == StoreID::SPARE_STORE);
>>>>>>> release/21.0.127

  assert((pSvcLoc->service("DetectorStore", pStore)).isSuccess());
  assert(pStore);
  assert(pStore->storeID() == StoreID::DETECTOR_STORE);
  
<<<<<<< HEAD
  assert((pSvcLoc->service("ConditionStore", pStore, CREATE)).isSuccess());
=======
  assert((pSvcLoc->service("ConditionsStore", pStore, CREATE)).isSuccess());
>>>>>>> release/21.0.127
  assert(pStore);
  assert(pStore->storeID() == StoreID::CONDITION_STORE);
  
  cout << "*** StoreID_test OK ***" <<endl;
  return 0;
}
#endif /*NOGAUDI*/













