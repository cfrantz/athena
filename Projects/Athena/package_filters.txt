<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
>>>>>>> release/21.0.127
#
# Package filtering rules for the Athena project build.
#

<<<<<<< HEAD
# Technical package(s) that should not show up in packages.txt:
- Build
- .devcontainer
- .vscode

# Ignore the Projects directory:
- Projects/.*

# Remove most HLT packages
# We currently build the HLT as part of Athena. Uncomment the following three lines to revert:
#+ HLT/Trigger/TrigTransforms/TrigTransform
#- HLT/.*
#- Trigger/TrigValidation/TrigP1Test

# Temporary VP1 compilation issues
- graphics/VP1/VP1Systems/VP1TriggerSystems
- graphics/VP1/VP1Systems/VP1MCSystems
- graphics/VP1/VP1Systems/VP1TriggerDecisionSystems
+ graphics/VP1/.*

# Temporarily disable Rivet_i until we have LCG_97_ATLAS_2
- Generators/Rivet_i

# Some analysis packages that are not part of Athena
- Control/AthLinksSA
- PhysicsAnalysis/Algorithms/StandaloneAnalysisAlgorithms
- PhysicsAnalysis/D3PDTools/EventLoop.*
- PhysicsAnalysis/D3PDTools/MultiDraw
- PhysicsAnalysis/D3PDTools/SampleHandler
=======
# Don't pick up anything from the Projects directory:
- Projects/.*

# Remove most HLT packages
+ HLT/Trigger/TrigTransforms/TrigTransform
- HLT/.*
- Trigger/TrigValidation/TrigP1Test

# Offload service has build problems
- Offloading/.*

# Temporary VP1 compilation issues
- graphics/VP1/.*

# Some analysis packages that are not part of Athena
- AsgExternal/Asg_Test
- PhysicsAnalysis/D3PDTools/EventLoop
- PhysicsAnalysis/AnalysisCommon/CPAnalysisExamples
- PhysicsAnalysis/AnalysisCommon/PMGTools
- PhysicsAnalysis/D3PDTools/EventLoop
- PhysicsAnalysis/D3PDTools/EventLoopAlgs
- PhysicsAnalysis/D3PDTools/EventLoopGrid
- PhysicsAnalysis/D3PDTools/MultiDraw
- PhysicsAnalysis/D3PDTools/SampleHandler
- PhysicsAnalysis/ElectronPhotonID/PhotonEfficiencyCorrection
- PhysicsAnalysis/ElectronPhotonID/PhotonVertexSelection
>>>>>>> release/21.0.127
- PhysicsAnalysis/HiggsPhys/Run2/HZZ/Tools/ZMassConstraint
- PhysicsAnalysis/JetPhys/SemileptonicCorr
- PhysicsAnalysis/SUSYPhys/SUSYTools
- PhysicsAnalysis/TauID/DiTauMassTools
<<<<<<< HEAD
- PhysicsAnalysis/TopPhys/TopPhysUtils/.*
- PhysicsAnalysis/TopPhys/xAOD/.*
- Reconstruction/Jet/JetAnalysisTools/JetTileCorrection
- Reconstruction/Jet/JetReclustering
- Trigger/TrigAnalysis/TrigTauAnalysis/TrigTauMatching

# Data quality packages that would generally go to AthDataQuality, but should be built in Athena
# until we have Run 3 Tier-0 ops
#- DataQuality/DataQualityConfigurations
#- DataQuality/DCSCalculator2

# Don't build PerfMonVTune which has external Intel tool dependency
- Control/PerformanceMonitoring/PerfMonVTune

=======
- PhysicsAnalysis/TauID/TauCorrUncert
- PhysicsAnalysis/TopPhys/QuickAna
- PhysicsAnalysis/TrackingID/InDetTrackSystematicsTools
- Reconstruction/Jet/JetAnalysisTools/JetTileCorrection
- Reconstruction/Jet/JetJvtEfficiency
- Reconstruction/Jet/JetReclustering
- Reconstruction/Jet/JetAnalysisTools/JetTileCorrection
- Trigger/TrigAnalysis/TrigMuonEfficiency
- Trigger/TrigAnalysis/TrigTauAnalysis/TrigTauMatching
>>>>>>> release/21.0.127
