/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/
#include "HltROBDataProviderSvc.h"
#include "TrigKernel/HltExceptions.h"
#include "TrigTimeAlgs/TrigTimeStamp.h"

<<<<<<< HEAD
// Gaudi
#include "Gaudi/Interfaces/IOptionsSvc.h"

// hltinterface / data collector
=======
// Include files.
#include "TrigServices/HltROBDataProviderSvc.h"
#include "TrigMonitorBase/TrigLockedHist.h"
#include "StoreGate/StoreGateSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/ThreadGaudi.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/IJobOptionsSvc.h"
#include "GaudiKernel/Property.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IAlgContextSvc.h"
#include "GaudiKernel/IAlgorithm.h"
>>>>>>> release/21.0.127
#include "hltinterface/DataCollector.h"

<<<<<<< HEAD
// eformat
#include "eformat/Status.h"
#include "eformat/write/FullEventFragment.h" // max number of possible ROB fragments
=======
// Constructor.
HltROBDataProviderSvc::HltROBDataProviderSvc(const std::string& name, ISvcLocator* svcloc)
:ROBDataProviderSvc(name,svcloc),
 m_storeGateSvc( "StoreGateSvc", name ),
 m_algContextSvc(0),
 m_onlineRunning(false),
 m_removeEmptyROB(false),
 m_isEventComplete(false),
 m_callerName("UNKNOWN"),
 m_histProp_requestedROBsPerCall(Gaudi::Histo1DDef("RequestedROBsPerCall" ,0,300,50)),
 m_histProp_receivedROBsPerCall(Gaudi::Histo1DDef("ReceivedROBsPerCall" ,0,300,50)),
 m_histProp_timeROBretrieval(Gaudi::Histo1DDef("TimeForROBretrieval" ,0.,500.,50)),
 m_hist_requestedROBsPerCall(0),
 m_hist_receivedROBsPerCall(0),
 m_hist_timeROBretrieval(0),
 m_hist_genericStatusForROB(0),
 m_hist_specificStatusForROB(0)
{
  declareProperty("ignoreROB", m_ignoreROB,"List of ROBs to ignore for retrieval");
  declareProperty("enabledROBs", m_enabledROBs,"List of enabled detector ROBs");
  declareProperty("LArMetROBs", m_enabledLArMetROBs,"List of enabled LAr MET ROBs");
  declareProperty("TileMetROBs", m_enabledTileMetROBs,"List of enabled Tile MET ROBs");
  declareProperty("readROBfromOKS", m_readROBfromOKS=true,"Read enabled ROBs from OKS");
  declareProperty("doMonitoring", m_doMonitoring=false,"Enable histograms");
  declareProperty("doDetailedROBMonitoring", m_doDetailedROBMonitoring=false,"Produce ROB cost data");
  declareProperty("ROBDataMonitorCollectionSGName", m_ROBDataMonitorCollection_SG_Name="ROBDataMonitorCollection","Name of cost monitoring collection in SG");
  declareProperty("HistRequestedROBsPerCall", m_histProp_requestedROBsPerCall,"Number of ROBs requested");
  declareProperty("HistReceivedROBsPerCall", m_histProp_receivedROBsPerCall,"Number of ROBs received");
  declareProperty("HistTimeROBretrieval", m_histProp_timeROBretrieval,"Timing for ROB retrieval");
  declareProperty("ModuleIDGenericLArMetROB", m_genericLArMetModuleID=0xffff,"Generic module id for LAr MET ROB retrieval");  
  declareProperty("ModuleIDGenericTileMetROB", m_genericTileMetModuleID=0xffff,"Generic module id for Tile MET ROB retrieval");
  declareProperty("SeparateMETandDetROBRetrieval", m_separateMETandDetROBRetrieval=true,"Separate retrieval of MET and detector ROBs");

  // fill map with generic status codes
  m_map_GenericStatus[eformat::UNCLASSIFIED]      = "UNCLASSIFIED"; 
  m_map_GenericStatus[eformat::BCID_CHECK_FAIL]   = "BCID_CHECK_FAIL"; 
  m_map_GenericStatus[eformat::LVL1ID_CHECK_FAIL] = "LVL1ID_CHECK_FAIL"; 
  m_map_GenericStatus[eformat::TIMEOUT]           = "TIMEOUT"; 
  m_map_GenericStatus[eformat::DATA_CORRUPTION]   = "DATA_CORRUPTION"; 
  m_map_GenericStatus[eformat::INTERNAL_OVERFLOW] = "INTERNAL_OVERFLOW"; 
  m_map_GenericStatus[eformat::DUMMY_FRAGMENT]    = "DUMMY_FRAGMENT"; 

  // fill vector with specific status codes
  m_vec_SpecificStatus.reserve(16);
  m_vec_SpecificStatus.push_back("TRIGGER_TYPE_SYNC_ERROR/DATAFLOW_DUMMY"); 
  m_vec_SpecificStatus.push_back("FRAGMENT_SIZE_ERROR"); 
  m_vec_SpecificStatus.push_back("DATABLOCK_ERROR"); 
  m_vec_SpecificStatus.push_back("CTRL_WORD_ERROR"); 
  m_vec_SpecificStatus.push_back("MISSING_BOF"); 
  m_vec_SpecificStatus.push_back("MISSING_EOF"); 
  m_vec_SpecificStatus.push_back("INVALID_HEADER_MARKER"); 
  m_vec_SpecificStatus.push_back("FORMAT_ERROR"); 
  m_vec_SpecificStatus.push_back("DUPLICATE_EVENT"); 
  m_vec_SpecificStatus.push_back("SEQUENCE_ERROR"); 
  m_vec_SpecificStatus.push_back("TRANSMISSION_ERROR"); 
  m_vec_SpecificStatus.push_back("TRUNCATION"); 
  m_vec_SpecificStatus.push_back("SHORT_FRAGMENT"); 
  m_vec_SpecificStatus.push_back("FRAGMENT_LOST"); 
  m_vec_SpecificStatus.push_back("FRAGMENT_PENDING"); 
  m_vec_SpecificStatus.push_back("ROBIN_DISCARD_MODE"); 
}
>>>>>>> release/21.0.127

// Athena

// STL includes
#include <algorithm>    // std::find

HltROBDataProviderSvc::HltROBDataProviderSvc(const std::string& name, ISvcLocator* pSvcLocator) :
  base_class(name, pSvcLocator)
{
}

StatusCode HltROBDataProviderSvc::initialize()
{
<<<<<<< HEAD
  ATH_MSG_INFO("HltROBDataProviderSvc::" << __FUNCTION__ << ": name = " << name());
//===================================================================  
//      The filtering of ROBs can be configured with job options as:
//
//      for individual ROBs as :
//      ------------------------
//      ROBDataProviderSvc.filterRobWithStatus = [ (ROB SourceId, StatusCode to remove),
//                                                 (ROB SourceId 2, StatusCode to remove 2), ... ]
//      and:
//      ROBDataProviderSvc.filterRobWithStatus += [ (ROB SourceId n, StatusCode to remove n) ]
//
//      Example:
//      ROBDataProviderSvc.filterRobWithStatus  = [ (0x42002a,0x0000000f), (0x42002e,0x00000008) ]
//      ROBDataProviderSvc.filterRobWithStatus += [ (0x42002b,0x00000000) ]
//
//      for all ROBs of a given sub detector as :
//      -----------------------------------------
//      ROBDataProviderSvc.filterSubDetWithStatus = [ (Sub Det Id, StatusCode to remove),
//                                                    (Sub Det Id 2, StatusCode to remove 2), ... ]
//      and:
//      ROBDataProviderSvc.filterSubDetWithStatus += [ (Sub Det Id n, StatusCode to remove n) ]
//
//      Example:
//      ROBDataProviderSvc.filterSubDetWithStatus  = [ (0x41,0x00000000), (0x42,0x00000000) ]
//      ROBDataProviderSvc.filterSubDetWithStatus += [ (0x41,0xcb0002) ]
//
//      For valid ROB Source Ids, Sub Det Ids and ROB Status elements see the event format
//      document ATL-D-ES-0019 (EDMS)
//===================================================================   
  // get list of ROBs to filter out by status code
  for (unsigned int i = 0; i < m_filterRobWithStatus.value().size(); i++) {
    eformat::helper::SourceIdentifier tmpsrc(m_filterRobWithStatus.value()[i].first);
    if (tmpsrc.human_detector() != "UNKNOWN") {
      m_filterRobMap[tmpsrc.code()].push_back(m_filterRobWithStatus.value()[i].second);
    }
  }
   
  // get list of subdetectors to filter out by status code
  for (unsigned int i = 0; i < m_filterSubDetWithStatus.value().size(); i++) {
    eformat::helper::SourceIdentifier tmpsrc((eformat::SubDetector)m_filterSubDetWithStatus.value()[i].first, 0);
    if (tmpsrc.human_detector() != "UNKNOWN") {
      m_filterSubDetMap[tmpsrc.subdetector_id()].push_back(m_filterSubDetWithStatus.value()[i].second);
    }
  }
  ATH_MSG_INFO(" ---> Filter out empty ROB fragments                               = " << m_filterEmptyROB);

  // print list of ROBs to filter out by status code
  ATH_MSG_INFO(" ---> Filter out specific ROBs by Status Code: # ROBs              = " << m_filterRobMap.size());
  for (auto it : m_filterRobMap) {
    eformat::helper::SourceIdentifier tmpsrc(it.first);
    ATH_MSG_INFO("      RobId=0x" << MSG::hex << it.first << " -> in Sub Det = " << tmpsrc.human_detector());

    for (auto it_status: it.second) { 
      eformat::helper::Status tmpstatus(it_status);
      ATH_MSG_INFO("         Status Code=0x"
		   << MSG::hex << std::setfill( '0' ) << std::setw(8) << tmpstatus.code()
		   << " Generic Part=0x" << std::setw(4) << tmpstatus.generic()
		   << " Specific Part=0x" << std::setw(4) << tmpstatus.specific() << MSG::dec);
    }
  }

  // print list of subdetectors to filter out by status code
  ATH_MSG_INFO(" ---> Filter out Sub Detector ROBs by Status Code: # Sub Detectors = " << m_filterSubDetMap.size());
  for (auto it : m_filterSubDetMap) {
    eformat::helper::SourceIdentifier tmpsrc(it.first, 0);
    ATH_MSG_INFO("      SubDetId=0x" << MSG::hex << it.first << " -> " << tmpsrc.human_detector());
    for (auto it_status : it.second) {
      eformat::helper::Status tmpstatus(it_status);
      ATH_MSG_INFO("         Status Code=0x"
		   << MSG::hex << std::setfill( '0' ) << std::setw(8) << tmpstatus.code()
		   << " Generic Part=0x" << std::setw(4) << tmpstatus.generic()
		   << " Specific Part=0x" << std::setw(4) << tmpstatus.specific() << MSG::dec);
=======
  StatusCode sc = ROBDataProviderSvc::initialize();

  ATH_MSG_INFO(" ---> HltROBDataProviderSvc = " << name() << " initialize "
               << " - package version " << PACKAGE_VERSION);

  // get Property filterEmptyROB from base class
  if ( !sc.isSuccess() ) {
    ATH_MSG_ERROR(" ROBDataProviderSvc::initialize() failed.");
    return sc;
  } else {
    BooleanProperty filterEmptyROB;
    filterEmptyROB.setName("filterEmptyROB");
    if (filterEmptyROB.assign(getProperty("filterEmptyROB"))) {
      m_removeEmptyROB = filterEmptyROB.value() ;
      ATH_MSG_INFO(" ---> getProperty('filterEmptyROB')       = " << filterEmptyROB);
    } else {
      ATH_MSG_WARNING(" ROBDataProviderSvc::getProperty('filterEmptyROB') failed.");
>>>>>>> release/21.0.127
    }
  }

  // get the list of enabled ROBs from OKS
  bool robOKSconfigFound = false;

  if ( m_readROBfromOKS.value() ) {
<<<<<<< HEAD
    ServiceHandle<Gaudi::Interfaces::IOptionsSvc> jobOptionsSvc("JobOptionsSvc", name());
    if ((jobOptionsSvc.retrieve()).isFailure()) {
      ATH_MSG_ERROR("Could not find JobOptionsSvc");
    } else {
      if (jobOptionsSvc->has("DataFlowConfig.DF_Enabled_ROB_IDs") &&
          m_enabledROBs.fromString(jobOptionsSvc->get("DataFlowConfig.DF_Enabled_ROB_IDs")).isSuccess()) {
        robOKSconfigFound = true;
        ATH_MSG_INFO(" ---> Read from OKS                                                = "
                     << MSG::dec << m_enabledROBs.value().size() << " enabled ROB IDs.");
      } else {
        ATH_MSG_WARNING("Could not set Property 'enabledROBs' from OKS.");
=======
    ServiceHandle<IJobOptionsSvc> p_jobOptionsSvc("JobOptionsSvc", name());
    if ((p_jobOptionsSvc.retrieve()).isFailure()) {
      ATH_MSG_ERROR("Could not find JobOptionsSvc");
    } else {
      const std::vector<const Property*>* dataFlowProps = p_jobOptionsSvc->getProperties("DataFlowConfig");
      if(!dataFlowProps)
        ATH_MSG_ERROR("Could not find DataFlowConfig properties");
      else
      {
        for ( const Property* cur : *dataFlowProps ) {
          // the enabled ROB list is found
          if ( cur->name() == "DF_Enabled_ROB_IDs" ) {
            if (m_enabledROBs.assign(*cur)) {
              robOKSconfigFound = true;
              ATH_MSG_INFO(" ---> Read from OKS                       = " << m_enabledROBs.value().size() << " enabled ROB IDs.");
            } else {
              ATH_MSG_WARNING(" Could not set Property 'enabledROBs' from OKS.");
            }
          }

          // the LAr MET ROB list is found
          if ( cur->name() == "DF_LAr_MET_ROB_IDs" ) {
            if (m_enabledLArMetROBs.assign(*cur)) {
              robLArMetOKSconfigFound = true;
              ATH_MSG_INFO(" ---> Read from OKS                       = " << m_enabledLArMetROBs.value().size() << " LAr MET ROB IDs.");
            } else {
              ATH_MSG_WARNING(" Could not set Property 'LArMetROBs' from OKS.");
            }
          }

          // the Tile MET ROB list is found
          if ( cur->name() == "DF_Tile_MET_ROB_IDs" ) {
            if (m_enabledTileMetROBs.assign(*cur)) {
              robTileMetOKSconfigFound = true;
              ATH_MSG_INFO(" ---> Read from OKS                       = " << m_enabledTileMetROBs.value().size() << " Tile MET ROB IDs.");
            } else {
              ATH_MSG_WARNING(" Could not set Property 'TileMetROBs' from OKS.");
            }
          }
        }
>>>>>>> release/21.0.127
      }
    }
  }

<<<<<<< HEAD
  // print list of enabled ROBs, read from OKS
  ATH_MSG_INFO(" ---> Read list of enabled ROBs from OKS                           = " << m_readROBfromOKS);
  if (m_enabledROBs.value().size() == 0) {
    ATH_MSG_INFO(" ---> The list of enabled ROBs has size                            = 0. No check will be performed ");
  } else {
    if (m_readROBfromOKS.value() && robOKSconfigFound) {
      ATH_MSG_INFO(" ---> The list of enabled ROBs has size                            = " << MSG::dec << m_enabledROBs.value().size() 
                   << ". It was read from the partition database." );
    } else {
      ATH_MSG_INFO(" ---> The list of enabled ROBs has size                            = " << MSG::dec << m_enabledROBs.value().size() 
=======
  ATH_MSG_INFO(" ---> HltROBDataProviderSvc               = " << name() << " special properties <---");
  ATH_MSG_INFO(" ---> Filter out empty ROB fragments      = " << m_removeEmptyROB);
  ATH_MSG_INFO(" ---> Fill monitoring histograms          = " << m_doMonitoring);
  ATH_MSG_INFO("        Hist:RequestedROBsPerCall         = " << m_histProp_requestedROBsPerCall);
  ATH_MSG_INFO("        Hist:ReceivedROBsPerCall          = " << m_histProp_receivedROBsPerCall);
  ATH_MSG_INFO("        Hist:TimeROBretrieval             = " << m_histProp_timeROBretrieval);
  ATH_MSG_INFO(" ---> Do detailed ROB monitoring          = " << m_doDetailedROBMonitoring);
  ATH_MSG_INFO(" ---> SG name for ROB monitoring collect. = " << m_ROBDataMonitorCollection_SG_Name);
  ATH_MSG_INFO(" ---> Read list of enabled ROBs from OKS  = " << m_readROBfromOKS);
  if (m_enabledROBs.value().size() == 0) {
    ATH_MSG_INFO(" ---> The list of enabled ROBs has size   = 0. No check will be performed ");
  } else {
    if (m_readROBfromOKS.value() && robOKSconfigFound) {
      ATH_MSG_INFO(" ---> The list of enabled ROBs has size   = " << m_enabledROBs.value().size() 
                   << ". It was read from the partition database." );
    } else {
      ATH_MSG_INFO(" ---> The list of enabled ROBs has size   = " << m_enabledROBs.value().size() 
                   << ". It was read from job options." );
    }
  }

  ATH_MSG_INFO(" ---> Generic Module ID for LAr MET ROB   = " << m_genericLArMetModuleID);
  if (m_enabledLArMetROBs.value().size() == 0) {
    ATH_MSG_INFO(" ---> The list of LAr MET ROBs has size   = 0. No LAr MET ROB access will be done.");
  } else {
    if (m_readROBfromOKS.value() && robLArMetOKSconfigFound) {
      ATH_MSG_INFO(" ---> The list of LAr MET ROBs has size   = " << m_enabledLArMetROBs.value().size() 
                   << ". It was read from the partition database." );
    } else {
      ATH_MSG_INFO(" ---> The list of LAr MET ROBs has size   = " << m_enabledLArMetROBs.value().size() 
>>>>>>> release/21.0.127
                   << ". It was read from job options." );
    }
  }

<<<<<<< HEAD
  // prefetch all ROBs in a ROS on a first retrieval of ROBs from this ROS
  ATH_MSG_INFO(" ---> Prefetch all ROBs in a ROS on first retrieval                = " << m_prefetchAllROBsfromROS);

  // Setup the slot specific cache
  m_eventsCache = SG::SlotSpecificObj<EventCache>( SG::getNSlots() );

  // Retrieve the monitoring tool
  if (!m_monTool.empty()) ATH_CHECK(m_monTool.retrieve());

  if (m_doCostMonitoring) ATH_CHECK(m_trigCostSvcHandle.retrieve());

  return(StatusCode::SUCCESS);
=======
  ATH_MSG_INFO(" ---> Generic Module ID for Tile MET ROB  = " << m_genericTileMetModuleID);
  if (m_enabledTileMetROBs.value().size() == 0) {
    ATH_MSG_INFO(" ---> The list of Tile MET ROBs has size  = 0. No Tile MET ROB access will be done.");
  } else {
    if (m_readROBfromOKS.value() && robTileMetOKSconfigFound) {
      ATH_MSG_INFO(" ---> The list of Tile MET ROBs has size  = " << m_enabledTileMetROBs.value().size() 
                   << ". It was read from the partition database." );
    } else {
      ATH_MSG_INFO(" ---> The list of Tile MET ROBs has size  = " << m_enabledTileMetROBs.value().size() 
                   << ". It was read from job options." );
    }
  }

  ATH_MSG_INFO(" ---> Separate MET and Det ROB Retrieval  = " << m_separateMETandDetROBRetrieval);

  if (m_ignoreROB.value().size() == 0) {
    ATH_MSG_INFO(" ---> The list of ROBs to ignore has size = 0. No check will be performed ");
  } else {
    ATH_MSG_INFO(" ---> The list of ROBs to ignore has size = " << m_ignoreROB.value().size());
  }

  for (unsigned int i=0; i<m_ignoreROB.value().size(); i++) {
    ATH_MSG_INFO(" ---> do not retrieve ROB[" << i << "]: hex(id)=0x"
                 << MSG::hex << m_ignoreROB.value()[i]<<MSG::dec
                 << " dec(id)="<< m_ignoreROB.value()[i]);
  }

  // register incident handler for begin run
  ServiceHandle<IIncidentSvc> incidentSvc("IncidentSvc", name());
  ATH_CHECK(incidentSvc.retrieve());

  long int pri=100;
  incidentSvc->addListener(this,"BeginRun",pri);
  incidentSvc.release().ignore();

  // Setup the StoreGateSvc
  ATH_CHECK(m_storeGateSvc.retrieve());

  return sc;
>>>>>>> release/21.0.127
}

StatusCode HltROBDataProviderSvc::finalize()
{
<<<<<<< HEAD
  ATH_CHECK(m_monTool.release());
  return StatusCode::SUCCESS;
}

/// --- Implementation of IROBDataProviderSvc interface ---
/// --- Legacy interface (deprecated) ---

/// Signal ROB fragments which should be considered for prefetching in online running
void HltROBDataProviderSvc::addROBData(const std::vector<uint32_t>& robIds, 
					 const std::string_view callerName)
{
  const EventContext context{ Gaudi::Hive::currentContext() };
  return addROBData( context, robIds, callerName );
}
=======
  StatusCode sc = ROBDataProviderSvc::finalize();
  if ( !sc.isSuccess() ) {
    ATH_MSG_ERROR(" ROBDataProviderSvc::finalize() failed.");
  }

  // release the AlgContextSvc if used
  if ( m_algContextSvc ) m_algContextSvc->release();
>>>>>>> release/21.0.127

/// Start a new event with a set of ROB fragments, e.g. from LVL1 result, in online and add the fragments to the ROB cache
void HltROBDataProviderSvc::setNextEvent(const std::vector<ROBF>& result)
{
  const EventContext context{ Gaudi::Hive::currentContext() };
  return setNextEvent( context, result );
}

/// Start a new event with a full event fragment and add all ROB fragments in to the ROB cache
void HltROBDataProviderSvc::setNextEvent(const RawEvent* re)
{
  const EventContext context{ Gaudi::Hive::currentContext() };
  return setNextEvent( context, re );
}

/// Retrieve ROB fragments for given ROB ids from the ROB cache
void HltROBDataProviderSvc::getROBData(const std::vector<uint32_t>& robIds, std::vector<const ROBF*>& robFragments, 
				       const std::string_view callerName)
{
  const EventContext context{ Gaudi::Hive::currentContext() };
  return getROBData( context, robIds, robFragments, callerName );
}

/// Retrieve the full event fragment
const RawEvent* HltROBDataProviderSvc::getEvent() 
{
  const EventContext context{ Gaudi::Hive::currentContext() };
  return getEvent( context );
}

<<<<<<< HEAD
/// Store the status for the event.
void HltROBDataProviderSvc::setEventStatus(uint32_t status)
{
  const EventContext context{ Gaudi::Hive::currentContext() };
  setEventStatus( context, status );
}
=======
  //-------------------
  //--- offline running
  //-------------------
  if (!m_onlineRunning) {

    // for offline running all requested ROBs should be found in cache
    // if not issue error
    ROBDataProviderSvc::addROBData(robIdsUnique);
    //------------------
    //--- online running
    //------------------
  } else {
    // detailed ROB monitoring
    //------------------------
    // Create a ROB monitoring collection and register it to StoreGate
    ROBDataMonitorCollection* p_robMonCollection(0); 
    if ( m_doDetailedROBMonitoring.value() ) {
      if ( !(m_storeGateSvc->transientContains<ROBDataMonitorCollection>(m_ROBDataMonitorCollection_SG_Name.value())) ) {
        p_robMonCollection = new ROBDataMonitorCollection;
        if ( p_robMonCollection ) {
          p_robMonCollection->reserve( HltROBDataProviderConstants::Number_of_Rob_Monitor_Structs ) ;
          if ( (m_storeGateSvc->record(p_robMonCollection, m_ROBDataMonitorCollection_SG_Name.value(), true)).isFailure() ) {
            ATH_MSG_WARNING(" Registering ROB Monitoring collection in StoreGate failed.");
            delete p_robMonCollection;
            p_robMonCollection = 0;
          }
        }
      } else {
        if ( m_storeGateSvc->retrieve(p_robMonCollection).isFailure() ) {
          ATH_MSG_WARNING(" Retrieval of ROB Monitoring collection from StoreGate failed.");
          p_robMonCollection = 0;
        }
      }
    }
>>>>>>> release/21.0.127

/// Retrieve the status for the event.
uint32_t HltROBDataProviderSvc::getEventStatus() 
{
  const EventContext context{ Gaudi::Hive::currentContext() };
  return getEventStatus( context );
}

/// --- Implementation of IROBDataProviderSvc interface ---
/// --- Context aware interface for MT ---

/// Signal ROB fragments which should be considered for prefetching in online running
void HltROBDataProviderSvc::addROBData(const EventContext& context, const std::vector<uint32_t>& robIds, 
				       const std::string_view callerName)
{
  ATH_MSG_VERBOSE("start of " << __FUNCTION__ << ": Number of ROB Ids to add = " << robIds.size() 
		  << " caller name = " << callerName);
  EventCache* cache = m_eventsCache.get( context );

  // allocate vector of missing ROB Ids
  std::vector<uint32_t> robIds_missing ;

  // allocate vector with existing ROB fragments in cache
  std::vector<const ROBF*> robFragments_inCache ;

  // check input ROB list against cache
  eventCache_checkRobListToCache(cache,robIds, robFragments_inCache, robIds_missing ) ;

  // call data collector
  if (robIds_missing.size() > 0) {
    ATH_MSG_DEBUG( __FUNCTION__ << ": Number of ROB Ids to reserve with DCM = " << robIds_missing.size()); 
    // reserve the ROBs in the DCM
    try {  
      auto mon_robres_t = Monitored::Timer("TIME_ROBReserveData");
      hltinterface::DataCollector::instance()->reserveROBData(cache->globalEventNumber, robIds_missing);
      mon_robres_t.stop();
      // Fill monitoring histograms
      auto mon_robres_nROBs = Monitored::Scalar("NUMBER_ROBReserveData",robIds_missing.size());
      auto mon = Monitored::Group(m_monTool, mon_robres_t, mon_robres_nROBs);
    } catch (const std::exception& ex) {
      ATH_MSG_ERROR( __FUNCTION__ << ":" << __LINE__ 
		     << "Failed to reserve ROB data, caught an unexpected exception: " << ex.what());
    } catch (...) {
      ATH_MSG_ERROR( __FUNCTION__ << ":" << __LINE__ 
		     << "Failed to reserve ROB data, caught an unexpected exception."); 
    }
  }
}

<<<<<<< HEAD
/// Start a new event with a set of ROB fragments, e.g. from LVL1 result, in online and add the fragments to the ROB cache
void HltROBDataProviderSvc::setNextEvent(const EventContext& context, const std::vector<ROBF>& result)
{
  ATH_MSG_FATAL("Obsolete method HltROBDataProviderSvc::setNextEvent(const EventContext& context, const std::vector<ROBF>& result) called "
		<< "\n context = " << context << " number of ROB fragments = " << result.size() );
}

/// Start a new event with a full event fragment and add all ROB fragments in to the ROB cache
void HltROBDataProviderSvc::setNextEvent(const EventContext& context, const RawEvent* re)
{
  ATH_MSG_VERBOSE("start of " << __FUNCTION__);
  EventCache* cache = m_eventsCache.get( context );
  
  // clear the event cache of the previous event
  eventCache_clear( cache );

  //----------------------------------------------+
  // Fill the event cache with the new event data |
  //----------------------------------------------+

  // store the pointer to the event
  cache->event = re;
  // set the LVL1 id
  cache->currentLvl1ID = re->lvl1_id();
  // set the global event number
  cache->globalEventNumber = re->global_id();
  // set flag for masking L2/EF module ID, this is only necessary for the separate L2 and EF systems from Run 1 
  m_maskL2EFModuleID = (re->nlvl2_trigger_info() != 0);

  //--------------------+
  // Fill the ROB cache |
  //--------------------+

  // get all the ROBFragments
  OFFLINE_FRAGMENTS_NAMESPACE::PointerType robF[eformat::write::MAX_UNCHECKED_FRAGMENTS];
  size_t number_robs = re->children(robF,eformat::write::MAX_UNCHECKED_FRAGMENTS);
  if (number_robs == eformat::write::MAX_UNCHECKED_FRAGMENTS) {
    ATH_MSG_ERROR("ROB buffer overflow: ROBs found = " << number_robs 
		  << " Max. number of ROBs allowed = " << eformat::write::MAX_UNCHECKED_FRAGMENTS);
  }
  std::vector<ROBF> rob_fragments;
  rob_fragments.reserve(number_robs);
  // loop over all ROBs
  for (size_t irob = 0; irob < number_robs; irob++) {
    rob_fragments.push_back(ROBF(robF[irob]));
  }
  // add the ROBs to the cache/rob map
  eventCache_addRobData(cache, std::move(rob_fragments)) ;

  ATH_MSG_DEBUG(" ---> setNextEvent for                " << name() );
  ATH_MSG_DEBUG("      current [global id, LVL1 id] = [" << cache->globalEventNumber << "," << cache->currentLvl1ID << "]" );
  ATH_MSG_DEBUG("      number of received ROBs      =  " << rob_fragments.size() );
  ATH_MSG_DEBUG("      size of ROB cache            =  " << cache->robmap.size() );

  //------------------------------+
  // Initiate whole ROS retrieval |
  //------------------------------+
  if ( m_prefetchAllROBsfromROS.value() && m_enabledROBs.value().size() != 0 ) {
    addROBData( context, m_enabledROBs.value(), "prefetch_HLTROBDataProviderSvc" );
    ATH_MSG_DEBUG("      ROS prefetch init. size      =  " << m_enabledROBs.value().size() );
  }
=======
    // for online running the requested ROBs should be not found in cache
    // ------------------------------------------------------------------
    // vector with missing ROB ids for DataCollector
    std::vector<uint32_t> vRobIds;
    vRobIds.reserve( robIdsUnique.size() ) ;

    // find missing ROB ids which should be retrieved  
    ATH_MSG_DEBUG(" ---> addROBData: Number of ROB Ids requested : " << robIdsUnique.size());

    for (uint32_t id : robIdsUnique) {

      // check if ROB is already in cache
      ONLINE_ROBMAP::iterator map_it = m_online_robmap.find(id) ;
      if(map_it != m_online_robmap.end()) {
        ATH_MSG_DEBUG(" ---> addROBData: Found   ROB Id : 0x" << MSG::hex << (*map_it).second.source_id());
        
        continue;
      } 

      // check if ROB should be ignored
      if (m_ignoreROB.value().size() != 0) {
        std::vector<uint32_t>::const_iterator rob_ignore_it =
            std::find(m_ignoreROB.value().begin(), m_ignoreROB.value().end(),id);
        if(rob_ignore_it != m_ignoreROB.value().end()) {
          ATH_MSG_DEBUG(" ---> addROBData: ROB Id : 0x" << MSG::hex << id << MSG::dec << " will be not retrieved, since it is on the veto list.");
          
          
          continue;
        }
      }

      // check if ROB is actually enabled for readout
      // do not perform this check for MET ROBs
      if ( (m_enabledROBs.value().size() != 0) && 
          (eformat::helper::SourceIdentifier(id).subdetector_id() != eformat::TDAQ_LAR_MET) &&
          (eformat::helper::SourceIdentifier(id).subdetector_id() != eformat::TDAQ_TILE_MET) ){
        std::vector<uint32_t>::const_iterator rob_enabled_it =
            std::find(m_enabledROBs.value().begin(), m_enabledROBs.value().end(),id);
        if(rob_enabled_it == m_enabledROBs.value().end()) {
          ATH_MSG_DEBUG(" ---> addROBData: ROB Id : 0x" << MSG::hex << id << MSG::dec
                        << " will be not retrieved, since it is not on the list of enabled ROBs.");
          
          
          continue;
        }
      }
      // Only Monitor SCHEDULED ROBs
      if ( p_robMonStruct ) {
      		
          (p_robMonStruct->requested_ROBs)[id].rob_history = robmonitor::SCHEDULED;
        }

      // the ROB should be retrieved from the ROS
      ATH_MSG_DEBUG(" ---> addROBData: Request ROB Id : 0x" << MSG::hex << id <<" from ROS ");
      vRobIds.push_back( id ) ;
    } // end loop over requested input ROBs

    if (vRobIds.size() == 0) {
      ATH_MSG_DEBUG(" ---> addROBData: either all requested ROBs are found in cache for running mode ONLINE, \n"
                    << "      or input ROB Id list was empty, \n"
                    << "      or all requested ROBs were not retrieved due to the veto list.\n"
                    << "      Number of requested ROB Ids = " << robIdsUnique.size() << "\n"
                    << "      Lvl1 id                     = " << m_currentLvl1ID);
      // Set ROB request time also in the case when no DataCollector request is necessary 
      // to allow correlation with RoI request times
      // start and stop times will be equal
      if ( p_robMonStruct ) {
        struct timeval time_start;
        struct timeval time_stop;

        gettimeofday(&time_start, 0);
        gettimeofday(&time_stop, 0);

        p_robMonStruct->start_time_of_ROB_request    = time_start;
        p_robMonStruct->end_time_of_ROB_request      = time_stop;
      }
    } else {
      //
      // Tell the DCM that these ROBs may be needed
      // ---------------------------------------------------------------
      //
      if (msgLvl(MSG::DEBUG)) {
        std::ostringstream ost;
        ost << "      Number of scheduled ROB Ids = " << vRobIds.size() << "\n" ;
        unsigned int rob_counter = 1;
        for (uint32_t rob : vRobIds) {
          ost << "       # = "<< std::setw(5) << rob_counter++ << " ROB id = 0x" << std::hex << rob << std::dec << "\n";
        }
        ATH_MSG_DEBUG(" ---> addROBData: The following ROB Ids are scheduled for retrieval and are reserved in the DCM: \n"
                      << "      Lvl1 id                     = " << m_currentLvl1ID << "\n"
                      << ost.str());                      
      }
      // reserve the ROBs in the DCM
      hltinterface::DataCollector::instance()->reserveROBData(m_currentLvl1ID, vRobIds);
    }
>>>>>>> release/21.0.127

  return;
}

/// Retrieve ROB fragments for given ROB ids from the ROB cache
void HltROBDataProviderSvc::getROBData(const EventContext& context, 
				       const std::vector<uint32_t>& robIds, std::vector<const ROBF*>& robFragments, 
				       const std::string_view callerName)
{
  TrigTimeStamp rosStartTime;

  ATH_MSG_VERBOSE("start of " << __FUNCTION__ << ": Number of ROB Ids to get = " << robIds.size() 
		  << " caller name = " << callerName);
  EventCache* cache = m_eventsCache.get( context );

  // allocate vector of missing ROB Ids
  std::vector<uint32_t> robIds_missing ;

  // check input ROB list against cache
  eventCache_checkRobListToCache(cache, robIds, robFragments, robIds_missing) ;

<<<<<<< HEAD
  // no missing ROB fragments, return the found ROB fragments 
  if (robIds_missing.size() == 0) {
    ATH_MSG_DEBUG( __FUNCTION__ << ": All requested ROB Ids were found in the cache. "); 
    return;
  }

  // There were missing ROB fragments retrieve them from the DCM and add them to the cache
  ATH_MSG_DEBUG( __FUNCTION__ << ": Number of ROB Ids to retrieve with DCM = " << robIds_missing.size()); 

  typedef std::vector<hltinterface::DCM_ROBInfo> ROBInfoVec;
  ROBInfoVec vRobInfos;
=======
  if ( result.size() == 0 ) {
    ATH_MSG_ERROR(" ---> setNextEvent online for "<< name() 
                  <<" failed: Size of received vector of ROB fragments = " << result.size());
    return;
  }

  // set the LVL1 id
  m_currentLvl1ID = result[0].rod_lvl1_id();

  // add fragments to map
  std::vector<ROBF>::const_iterator it_robf     = result.begin();
  std::vector<ROBF>::const_iterator it_robf_end = result.end();
  for(; it_robf!=it_robf_end; ++it_robf) {
    uint32_t id = it_robf->source_id() ;
    // check current L1 ID against CTP fragment when possible
    if ( (eformat::helper::SourceIdentifier(id).subdetector_id() == eformat::TDAQ_CTP) &&
         (it_robf->rod_lvl1_id() != m_currentLvl1ID) ) {
      ATH_MSG_ERROR(" ---> Lvl1 ID mismatch for CTP fragment with SourceId = 0x" << MSG::hex << id << MSG::dec
                    << " and L1 Id = " << it_robf->rod_lvl1_id()
                    << " to currently used L1 Id = " << m_currentLvl1ID
                    << " -> Use CTP version from now on.");
      m_currentLvl1ID = it_robf->rod_lvl1_id() ;
    }
    // remove empty ROB fragments or ones with bad status, if requested
    if ((it_robf->rod_ndata() == 0) && (m_removeEmptyROB)) { 
      ATH_MSG_DEBUG(" ---> Empty ROB Id = 0x" << MSG::hex << id << MSG::dec
                    << " removed for L1 Id = " << m_currentLvl1ID);
    } else if ( ROBDataProviderSvc::filterRobWithStatus(&*it_robf) ) {
      if (msgLvl(MSG::DEBUG) && (it_robf->nstatus() > 0)) {
        const uint32_t* it_status;
        it_robf->status(it_status);
        eformat::helper::Status tmpstatus( (*it_status) ) ;
        ATH_MSG_DEBUG(" ---> ROB Id = 0x" << MSG::hex << id
                      << std::setfill( '0' )
                      << " with Generic Status Code = 0x" << std::setw(4) << tmpstatus.generic()
                      << " and Specific Status Code = 0x" << std::setw(4) << tmpstatus.specific()
                      << MSG::dec
                      << " removed for L1 Id = " << m_currentLvl1ID);
      }
    } else {
      m_online_robmap[id]= (*it_robf) ;
    }
>>>>>>> release/21.0.127

  // Get ROB Fragments with DataCollector
  vRobInfos.reserve( robIds_missing.size() ) ;
  try {
    auto mon_rob_t = Monitored::Timer("TIME_ROBRequest");
    hltinterface::DataCollector::instance()->collect(vRobInfos, cache->globalEventNumber, robIds_missing);
    mon_rob_t.stop();
    // Fill monitoring histograms
    auto mon_rob_nROBs = Monitored::Scalar("NUMBER_ROBRequest",vRobInfos.size());
    auto mon = Monitored::Group(m_monTool, mon_rob_t, mon_rob_nROBs);
  } catch (const std::exception& ex) {
    ATH_MSG_ERROR( __FUNCTION__ << ":" << __LINE__ 
		   << "Failed to collect ROBs, caught an unexpected exception: " << ex.what()
		   << ". Throwing hltonl::Exception::EventSourceCorrupted" );
    throw hltonl::Exception::EventSourceCorrupted();
  } catch (...) {
    ATH_MSG_ERROR( __FUNCTION__ << ":" << __LINE__ 
		   << "Failed to collect ROBs, caught an unexpected exception. "
		   << "Throwing hltonl::Exception::EventSourceCorrupted" );
    throw hltonl::Exception::EventSourceCorrupted();
  }

  // Store retrieved ROB data in the cache 
  std::vector<ROBF> robFragments_missing;
  robFragments_missing.reserve( vRobInfos.size() );
  for(ROBInfoVec::const_iterator it=vRobInfos.begin(); it!=vRobInfos.end(); ++it) {
    ATH_MSG_DEBUG(__FUNCTION__ << " ROB Id = 0x" << MSG::hex << it->robFragment.source_id() << MSG::dec
		    << " retrieved from DCM for (global Id, L1 Id) = (" << cache->globalEventNumber << "," << cache->currentLvl1ID <<")" );
    robFragments_missing.push_back( it->robFragment );
  }

  // Check if event should be monitored and monitor ROB data
  auto monitorData = robmonitor::ROBDataMonitorStruct(cache->currentLvl1ID, std::string(callerName));
  if (m_doCostMonitoring && m_trigCostSvcHandle->isMonitoredEvent(context, /*includeMultiSlot =*/ false)) {
    // Monitor HLT cached ROBs
    for (const ROBF* robFrag : robFragments) {
      monitorData.requested_ROBs[robFrag->source_id()] = robmap_getRobData(*robFrag, robmonitor::HLT_CACHED);
    }

    // Add the ROBs to the cache/rob map and collect ignored robs
    std::set<uint32_t> robIds_ignored;
    eventCache_addRobData(cache, std::move(robFragments_missing), robIds_ignored);

    // Monitor DCM ROBs
    for (const hltinterface::DCM_ROBInfo& robInfo : vRobInfos) {
      robmonitor::ROBHistory status;

      // Check ROB history
      if (robIds_ignored.find(robInfo.robFragment.source_id()) != robIds_ignored.end()) {
        status = robmonitor::IGNORED;
      }
      else {
        status = robInfo.robIsCached ? robmonitor::DCM_CACHED : robmonitor::RETRIEVED;
      }

      monitorData.requested_ROBs[robInfo.robFragment.source_id()] = robmap_getRobData(robInfo.robFragment, status);
    }

    // Return all the requested ROB fragments from the cache and collect disabled ROBs
    std::set<uint32_t> robIds_disabled;
    eventCache_checkRobListToCache(cache, robIds, robFragments, robIds_missing, robIds_disabled);

    // Fill undefined (not enabled) ROBs
    for (uint32_t robId : robIds_disabled) {
        monitorData.requested_ROBs[robId] = robmonitor::ROBDataStruct(robId);
        monitorData.requested_ROBs[robId].rob_history = robmonitor::UNDEFINED;
    }
  }
  else {
    // add the ROBs to the cache/rob map
    eventCache_addRobData(cache, std::move(robFragments_missing)) ;

<<<<<<< HEAD
    // return all the requested ROB fragments from the cache
    eventCache_checkRobListToCache(cache, robIds, robFragments, robIds_missing) ;
=======
  if(msgLvl(MSG::DEBUG)) {
    msg() << MSG::DEBUG << " ---> setNextEvent online for "<< name() << endmsg; 
    msg() << MSG::DEBUG << "      online running    = " << m_onlineRunning << endmsg;
    msg() << MSG::DEBUG << "      current LVL1 id   = " << m_currentLvl1ID << endmsg;
    msg() << MSG::DEBUG << "      # LVL1 ROBs       = " << result.size() << endmsg;
    msg() << MSG::DEBUG << "      size of ROB cache = " << m_online_robmap.size() << endmsg;
    msg() << MSG::DEBUG << dumpROBcache() << endmsg; 
>>>>>>> release/21.0.127
  }

  // Save ROS processing time and pass ROS data to CostMonitor
  if (m_doCostMonitoring && m_trigCostSvcHandle->isMonitoredEvent(context, /*includeMultiSlot =*/ false)) {
    TrigTimeStamp rosEndTime;

    monitorData.start_time = rosStartTime.microsecondsSinceEpoch();
    monitorData.end_time = rosEndTime.microsecondsSinceEpoch();

    // Check if ROBMonitorDataStruct is move-constructible
    static_assert(std::is_nothrow_move_constructible<robmonitor::ROBDataMonitorStruct>::value);
    if (m_trigCostSvcHandle->monitorROS(context, std::move(monitorData)).isFailure()) {
      ATH_MSG_WARNING("TrigCost ROS monitoring failed!");
    }  
  }
}

robmonitor::ROBDataStruct HltROBDataProviderSvc::robmap_getRobData(const ROBF& robFrag, robmonitor::ROBHistory robStatus)
{
<<<<<<< HEAD
  auto robData = robmonitor::ROBDataStruct(robFrag.source_id());
  robData.rob_size = robFrag.fragment_size_word();
  robData.rob_status_word = robFrag.nstatus() ? robFrag.status()[0] : 0;
  robData.rob_history = robStatus;
=======
  //--------------------
  // set the caller name
  //--------------------
  if (callerName != "UNKNOWN") m_callerName = callerName;

  //-------------------
  //--- offline running
  //-------------------
  if (!m_onlineRunning) {
    ROBDataProviderSvc::getROBData(robIds,robFragments);
    //------------------
    //--- online running
    //------------------
  } else {
    //--------------------
    // make unique ROB IDs
    //--------------------
    std::vector<uint32_t>::iterator remove_duplicate;
    std::vector<uint32_t> robIdsUnique(robIds);

    sort(robIdsUnique.begin(), robIdsUnique.end()); 
    remove_duplicate = unique(robIdsUnique.begin(), robIdsUnique.end()); 
    robIdsUnique.erase(remove_duplicate, robIdsUnique.end());

    //------------------------------------------------------------------
    // Replace the generic MET ROB ID with the full list of all MET ROBs
    //------------------------------------------------------------------
    // LAr MET ROBs
    uint32_t generic_LAr_MET_id = eformat::helper::SourceIdentifier(eformat::TDAQ_LAR_MET,m_genericLArMetModuleID.value()).code();
    std::vector<uint32_t>::iterator rob_LAr_Met_it = std::find(robIdsUnique.begin(), robIdsUnique.end(), generic_LAr_MET_id);
    if (rob_LAr_Met_it != robIdsUnique.end()) {
      robIdsUnique.erase(rob_LAr_Met_it);
      if (m_enabledLArMetROBs.value().size() != 0) robIdsUnique.insert(robIdsUnique.end(),m_enabledLArMetROBs.value().begin(),m_enabledLArMetROBs.value().end());
    }

    // Tile MET ROBs
    uint32_t generic_Tile_MET_id = eformat::helper::SourceIdentifier(eformat::TDAQ_TILE_MET,m_genericTileMetModuleID.value()).code();
    std::vector<uint32_t>::iterator rob_Tile_Met_it = std::find(robIdsUnique.begin(), robIdsUnique.end(), generic_Tile_MET_id);
    if (rob_Tile_Met_it != robIdsUnique.end()) {
      robIdsUnique.erase(rob_Tile_Met_it);
      if (m_enabledTileMetROBs.value().size() != 0) robIdsUnique.insert(robIdsUnique.end(),m_enabledTileMetROBs.value().begin(),m_enabledTileMetROBs.value().end());
    }

    // detailed ROB monitoring
    //------------------------
    // Create a ROB monitoring collection and register it to StoreGate
    ROBDataMonitorCollection* p_robMonCollection(0); 
    if ((m_doDetailedROBMonitoring.value()) && (robIdsUnique.size() != 0)) {
      if ( !(m_storeGateSvc->transientContains<ROBDataMonitorCollection>(m_ROBDataMonitorCollection_SG_Name.value())) ) {
	p_robMonCollection = new ROBDataMonitorCollection;
	if ( p_robMonCollection ) {
	  p_robMonCollection->reserve( HltROBDataProviderConstants::Number_of_Rob_Monitor_Structs ) ;
	  if ( (m_storeGateSvc->record(p_robMonCollection, m_ROBDataMonitorCollection_SG_Name.value(), true)).isFailure() ) {
	    ATH_MSG_WARNING(" getROBData: Registering ROB Monitoring collection in StoreGate failed.");
	    delete p_robMonCollection;
	    p_robMonCollection = 0;
	  }
	}
      } else {
	if ( m_storeGateSvc->retrieve(p_robMonCollection).isFailure() ) {
	  ATH_MSG_WARNING(" getROBData: Retrieval of ROB Monitoring collection from StoreGate failed.");
	  p_robMonCollection = 0;
	}
      }
    }

    // create a new ROBDataMonitorStruct and fill it
    robmonitor::ROBDataMonitorStruct* p_robMonStruct(0);
    if ( p_robMonCollection ) {
      // caller name
      std::string caller_name("UNKNOWN");
      if (callerName != "UNKNOWN") {
	caller_name = callerName;
      } else if ((callerName == "UNKNOWN") && (m_callerName != "UNKNOWN")) {
	caller_name = m_callerName;
      } else {
	IAlgorithm* alg(0);
	if ( m_algContextSvc ) {
	  alg = m_algContextSvc->currentAlg();
	  caller_name = (alg ? alg->name() : "<NONE>");
	}
      }

      // initialize new ROBDataMonitorStruct
      
      p_robMonStruct = new robmonitor::ROBDataMonitorStruct(m_currentLvl1ID, robIdsUnique, caller_name);
    }

    //--------------------------
    // update internal ROB cache
    //--------------------------
    addROBDataToCache(robIdsUnique, p_robMonStruct);

    // add the ROB monitoring structure to the collection
    if ( p_robMonCollection && p_robMonStruct ) p_robMonCollection->push_back( p_robMonStruct );
>>>>>>> release/21.0.127

  return robData;
}

<<<<<<< HEAD
/// Retrieve the full event fragment
const RawEvent* HltROBDataProviderSvc::getEvent(const EventContext& context)
{
  ATH_MSG_VERBOSE("start of " << __FUNCTION__);
  return m_eventsCache.get( context )->event;
}
=======
    //------------------------------------------------------------------
    // Return requested ROBs from internal cache
    //------------------------------------------------------------------
    for(std::vector<uint32_t>::const_iterator it = robIdsUnique.begin(); it != robIdsUnique.end(); ++it){
      uint32_t id = (*it); 
      ONLINE_ROBMAP::iterator map_it = m_online_robmap.find(id) ; 
      if(map_it != m_online_robmap.end()) {      
        robFragments.push_back( &((*map_it).second) );
      } else {
        ATH_MSG_DEBUG(" ---> getROBData: Failed to find ROB for id 0x"
                      << MSG::hex << id << MSG::dec);
        ATH_MSG_VERBOSE(dumpROBcache());
      }
    }
  }
>>>>>>> release/21.0.127

/// Store the status for the event.
void HltROBDataProviderSvc::setEventStatus(const EventContext& context, uint32_t status)
{
  ATH_MSG_VERBOSE("start of " << __FUNCTION__);
  m_eventsCache.get(context)->eventStatus = status;
}

/// Retrieve the status for the event.
uint32_t HltROBDataProviderSvc::getEventStatus(const EventContext& context)
{
  ATH_MSG_VERBOSE("start of " << __FUNCTION__);
  return m_eventsCache.get( context )->eventStatus;
}

/// Apply a function to all ROBs in the cache
void HltROBDataProviderSvc::processCachedROBs(const EventContext& context, const std::function< void(const ROBF* )>& fn) const
{
  ATH_MSG_VERBOSE("start of " << __FUNCTION__);
  for ( const auto& el : m_eventsCache.get( context )->robmap )
    {
      fn( &el.second );
    }
}

/// Flag to check if all event data have been retrieved
bool HltROBDataProviderSvc::isEventComplete(const EventContext& context) const
{
  ATH_MSG_VERBOSE("start of " << __FUNCTION__);
  return m_eventsCache.get( context )->isEventComplete;
}

/// retrieve in online running all ROBs for the event from the readout system. Only those ROBs are retrieved which are not already in the cache
int HltROBDataProviderSvc::collectCompleteEventData(const EventContext& context, const std::string_view callerName)
{
  ATH_MSG_VERBOSE("start of " << __FUNCTION__  << " caller name = " << callerName);

  EventCache* cache = m_eventsCache.get( context );

  // return if event is already complete 
  if (cache->isEventComplete) return 0;

  typedef std::vector<hltinterface::DCM_ROBInfo> ROBInfoVec;
  ROBInfoVec vRobInfos ;
  if (m_enabledROBs.value().size() != 0) {
    vRobInfos.reserve( m_enabledROBs.value().size() ) ;
  } else {
    vRobInfos.reserve( eformat::write::MAX_UNCHECKED_FRAGMENTS ) ;
  }

  // Get ROB Fragments for complete event with DataCollector
<<<<<<< HEAD
  try {
    auto mon_col_t = Monitored::Timer("TIME_CollectAllROBs");
    hltinterface::DataCollector::instance()->collect(vRobInfos, cache->globalEventNumber);
    mon_col_t.stop();
    ATH_MSG_DEBUG( __FUNCTION__ << ": Number of received ROB Ids = " << vRobInfos.size() );
    // Fill monitoring histograms
    auto mon_col_nROBs = Monitored::Scalar("NUMBER_CollectAllROBs",vRobInfos.size());
    auto mon = Monitored::Group(m_monTool, mon_col_t, mon_col_nROBs);
  } catch (const std::exception& ex) {
    ATH_MSG_ERROR( __FUNCTION__ << ":" << __LINE__ 
		   << "Failed to collect complete event, caught an unexpected exception: " << ex.what()
		   << ". Throwing hltonl::Exception::EventSourceCorrupted" );
    throw hltonl::Exception::EventSourceCorrupted();
  } catch (...) {
    ATH_MSG_ERROR( __FUNCTION__ << ":" << __LINE__ 
		   << "Failed to collect complete event, caught an unexpected exception. "
		   << "Throwing hltonl::Exception::EventSourceCorrupted" );
    throw hltonl::Exception::EventSourceCorrupted();
  }

  // Store retrieved ROB data in the cache 
  std::vector<ROBF> robFragments_missing;
  robFragments_missing.reserve( vRobInfos.size() );
  for(ROBInfoVec::const_iterator it=vRobInfos.begin(); it!=vRobInfos.end(); ++it) {
    ATH_MSG_VERBOSE(__FUNCTION__ << " ROB Id = 0x" << MSG::hex << it->robFragment.source_id() << MSG::dec
		    << " retrieved from DCM for (global Id, L1 Id) = (" << cache->globalEventNumber << "," << cache->currentLvl1ID <<")" );
    robFragments_missing.push_back( it->robFragment );
  }
  // add the ROBs to the cache/rob map
  eventCache_addRobData(cache, std::move(robFragments_missing)) ;
=======
  hltinterface::DataCollector::instance()->collect(vRobInfos, m_currentLvl1ID);

  if ( m_doMonitoring || m_doDetailedROBMonitoring.value() ) gettimeofday(&time_stop, 0);

  if (msgLvl(MSG::DEBUG)) {
    std::ostringstream ost;
    unsigned int rob_counter = 1;
    for (const auto& rob : vRobInfos) {
      ost << "       # = "<< std::setw(5) << rob_counter++ << " ROB id = 0x" 
          << std::hex << rob.robFragment.source_id() << std::dec << "\n" ; 
    }
    ATH_MSG_DEBUG(" ---> collectCompleteEventData: The following ROB Ids were received from DataCollector : \n"
                  << "      Lvl1 id                                             = " << m_currentLvl1ID << "\n"
                  << "      Number of actually received ROB Ids                 = " << vRobInfos.size() << "\n"
                  << ost.str());
  }
    
    
  // detailed ROB monitoring
  //------------------------
  // Create a ROB monitoring collection and register it to StoreGate
  ROBDataMonitorCollection* p_robMonCollection(0); 
  if ( m_doDetailedROBMonitoring.value() ) {
    if ( !(m_storeGateSvc->transientContains<ROBDataMonitorCollection>(m_ROBDataMonitorCollection_SG_Name.value())) ) {
      p_robMonCollection = new ROBDataMonitorCollection;
      if ( p_robMonCollection ) {
        p_robMonCollection->reserve( HltROBDataProviderConstants::Number_of_Rob_Monitor_Structs ) ;
        if ( (m_storeGateSvc->record(p_robMonCollection, m_ROBDataMonitorCollection_SG_Name.value(), true)).isFailure() ) {
          ATH_MSG_WARNING(" Registering ROB Monitoring collection in StoreGate failed.");
          delete p_robMonCollection;
          p_robMonCollection = 0;
        }
      }
    } else {
      if ( m_storeGateSvc->retrieve(p_robMonCollection).isFailure() ) {
        ATH_MSG_WARNING(" Retrieval of ROB Monitoring collection from StoreGate failed.");
        p_robMonCollection = 0;
      }
    }
  }
  
  // create a new ROBDataMonitorStruct and fill it
  robmonitor::ROBDataMonitorStruct* p_robMonStruct(0);
  if ( p_robMonCollection ) {
    // caller name
    std::string caller_name("UNKNOWN");
    if (callerName != "UNKNOWN") {
      caller_name = callerName;
    } else if ((callerName == "UNKNOWN") && (m_callerName != "UNKNOWN")) {
      caller_name = m_callerName;
    } else {
      IAlgorithm* alg(0);
      if ( m_algContextSvc ) {
        alg = m_algContextSvc->currentAlg();
        caller_name = (alg ? alg->name() : "<NONE>");
      }
    }

    // get ROB Ids
    std::vector<uint32_t> robIds;
    robIds.reserve(vRobInfos.size());
    for(ROBInfoVec::const_iterator it=vRobInfos.begin(); it!=vRobInfos.end(); ++it) {
      robIds.push_back( it->robFragment.source_id() ) ;
    }

    // initialize new ROBDataMonitorStruct
    p_robMonStruct = new robmonitor::ROBDataMonitorStruct(m_currentLvl1ID, robIds, caller_name);
  }

  if ( m_doMonitoring || p_robMonStruct ) {
    int secs = 0 ;
    if (time_stop.tv_sec >= time_start.tv_sec)
      secs = time_stop.tv_sec - time_start.tv_sec;

    int usecs = time_stop.tv_usec - time_start.tv_usec;
    float mtime = static_cast<float>(secs)*1000 + static_cast<float>(usecs)/1000;

    //* timing histogram
    if (m_hist_timeROBretrieval) {
      scoped_lock_histogram lock;
      m_hist_timeROBretrieval->Fill(mtime);
      m_hist_timeROBretrieval->LabelsDeflate("X");
    }
    //* number of received ROBs
    if ( m_hist_receivedROBsPerCall ) {
      scoped_lock_histogram lock;
      m_hist_receivedROBsPerCall->Fill(vRobInfos.size());
      m_hist_receivedROBsPerCall->LabelsDeflate("X");
    }

    //* detailed monitoring
    if ( p_robMonStruct ) {
      p_robMonStruct->start_time_of_ROB_request    = time_start;
      p_robMonStruct->end_time_of_ROB_request      = time_stop;
    }
  }

  // add ROBs to cache
  updateROBDataCache(vRobInfos,p_robMonStruct);

  // add the ROB monitoring structure to the collection
  if ( p_robMonCollection && p_robMonStruct ) p_robMonCollection->push_back( p_robMonStruct );
>>>>>>> release/21.0.127

  // update event complete flag
  cache->isEventComplete = true;

  return vRobInfos.size();
}

<<<<<<< HEAD
/// method to filter ROBs with given Status code
bool HltROBDataProviderSvc::robmap_filterRobWithStatus(const ROBF* rob)
{
  // No filter criteria defined
  if ((m_filterRobMap.size() == 0) && (m_filterSubDetMap.size() == 0)) {
    return(false);
  }

  // There should be at least one status element if there was an error
  // in case there are 0 status elements then there was no known error
  // (see event format document ATL-D-ES-0019 (EDMS))
  const uint32_t* rob_it_status;
  const uint32_t null_status(0);
  // The ROB has no status elements
  if (rob->nstatus() == 0) {
    rob_it_status = &null_status;
  } else {
  // The ROB has at least one status element, access it via an iterator
    rob->status(rob_it_status);
  }  

  // Build the full ROB Sourceidentifier
  eformat::helper::SourceIdentifier tmpsrc(rob->rob_source_id());
  
  // Check if there is a ROB specific filter rule defined for this ROB Id and match the status code
  FilterRobMap::iterator map_it_rob = m_filterRobMap.find(tmpsrc.code());
  if (map_it_rob != m_filterRobMap.end()) {
    for (auto it_status: (*map_it_rob).second) {
      if (*rob_it_status == it_status) {
	ATH_MSG_VERBOSE(__FUNCTION__ << " ROB Id = 0x" << MSG::hex << tmpsrc.code() 
			<< " with status = 0x" << *rob_it_status  << MSG::dec
			<< " removed due to ROB filter rule.");
	return(true);
      }
    }
  }

  // Check if there is a sub detector specific filter rule defined for this ROB Id and match the status code
  FilterSubDetMap::iterator map_it_subdet = m_filterSubDetMap.find(tmpsrc.subdetector_id());
  if (map_it_subdet != m_filterSubDetMap.end()) {
    for (auto it_status: (*map_it_subdet).second) {
      if (*rob_it_status == it_status) {
	ATH_MSG_VERBOSE(__FUNCTION__ << " ROB Id = 0x" << MSG::hex << tmpsrc.code() 
			<< " with status = 0x" << *rob_it_status  << MSG::dec
			<< " removed due to SubDet filter rule.");
	return(true);
      }
=======
// handler for BeginRun
void HltROBDataProviderSvc::handle(const Incident& incident) {
  if (incident.type()!="BeginRun") return;
  ATH_MSG_DEBUG("In BeginRun incident.");

  // if detailed ROB monitoring is requested, check if the AlgContextSvc is running, 
  // if yes use it to obtain the calling algorithm name
  if ( m_doDetailedROBMonitoring.value() ) {
    if ( service("AlgContextSvc", m_algContextSvc, /*createIf=*/ false).isFailure() ) {
      ATH_MSG_ERROR("Error retrieving AlgContextSvc."  
                    << "Calling algorithm name not available in detailed ROB monitoring");
      m_algContextSvc=0;
    }
  }

  // define histograms if monitoring is requested
  if ( !m_doMonitoring.value() ) return;

  // find histogramming service
  ServiceHandle<ITHistSvc> rootHistSvc("THistSvc", name());
  if ((rootHistSvc.retrieve()).isFailure()) {
    ATH_MSG_ERROR("Unable to locate THistSvc");
    rootHistSvc.release().ignore();
    return;
  }

  // *-- booking path
  std::string path = std::string("/EXPERT/")+getGaudiThreadGenericName(name())+"/";

  // *-- number of bins for sub detector plots (55 SubDet max.)
  uint32_t n_bins_partEBSubDet = eformat::helper::SubDetectorDictionary.size();

  // *-- number of requested ROBs per call
  m_hist_requestedROBsPerCall = new TH1F (m_histProp_requestedROBsPerCall.value().title().c_str(),
      (m_histProp_requestedROBsPerCall.value().title()+";number of ROBs").c_str(),
      m_histProp_requestedROBsPerCall.value().bins(),
      m_histProp_requestedROBsPerCall.value().lowEdge(),
      m_histProp_requestedROBsPerCall.value().highEdge());
  if (m_hist_requestedROBsPerCall) {
    CAN_REBIN(m_hist_requestedROBsPerCall);
    if( rootHistSvc->regHist(path + m_hist_requestedROBsPerCall->GetName(), m_hist_requestedROBsPerCall).isFailure() ) {
      ATH_MSG_WARNING("Can not register monitoring histogram: " << m_hist_requestedROBsPerCall->GetName());
    }
  }

  // *-- number of received ROBs per call
  m_hist_receivedROBsPerCall  = new TH1F (m_histProp_receivedROBsPerCall.value().title().c_str(),
      (m_histProp_receivedROBsPerCall.value().title()+";number of ROBs").c_str(),
      m_histProp_receivedROBsPerCall.value().bins(),
      m_histProp_receivedROBsPerCall.value().lowEdge(),
      m_histProp_receivedROBsPerCall.value().highEdge());
  if (m_hist_receivedROBsPerCall) {
    CAN_REBIN(m_hist_receivedROBsPerCall);
    if( rootHistSvc->regHist(path + m_hist_receivedROBsPerCall->GetName(), m_hist_receivedROBsPerCall).isFailure() ) {
      ATH_MSG_WARNING("Can not register monitoring histogram: " << m_hist_receivedROBsPerCall->GetName());
    }
  }

  // *-- timing of ROB retrieval
  m_hist_timeROBretrieval     = new TH1F (m_histProp_timeROBretrieval.value().title().c_str(),
      (m_histProp_timeROBretrieval.value().title()+";time [ms]").c_str(),
      m_histProp_timeROBretrieval.value().bins(),
      m_histProp_timeROBretrieval.value().lowEdge(),
      m_histProp_timeROBretrieval.value().highEdge());
  if (m_hist_timeROBretrieval) {
    CAN_REBIN(m_hist_timeROBretrieval);
    if( rootHistSvc->regHist(path + m_hist_timeROBretrieval->GetName(), m_hist_timeROBretrieval).isFailure() ) {
      ATH_MSG_WARNING("Can not register monitoring histogram: " << m_hist_timeROBretrieval->GetName());
    }
  }

  // *-- Generic Status for ROBs per sub detector
  m_hist_genericStatusForROB = new TH2F ("GenericStatusForROBsFromSubDetectors",
      "GenericStatusForROBsFromSubDetectors;;",
      n_bins_partEBSubDet,0.,(float) n_bins_partEBSubDet,
      m_map_GenericStatus.size(),0., (float) m_map_GenericStatus.size());
  if (m_hist_genericStatusForROB) {
    uint32_t n_tmp_bin = 1;
    for (eformat::helper::EnumClass<eformat::SubDetector>::const_iterator it_sub=eformat::helper::SubDetectorDictionary.begin();
        it_sub != eformat::helper::SubDetectorDictionary.end(); ++it_sub ) {
      m_hist_genericStatusForROB->GetXaxis()->SetBinLabel( n_tmp_bin, (it_sub->second).c_str() );
      n_tmp_bin++;
    }

    n_tmp_bin = 1;
    for (std::map<eformat::GenericStatus, std::string>::const_iterator it = m_map_GenericStatus.begin();it != m_map_GenericStatus.end();++it) {
      m_hist_genericStatusForROB->GetYaxis()->SetBinLabel( n_tmp_bin, (*it).second.c_str() );
      n_tmp_bin++;
    }
    if( rootHistSvc->regHist(path + m_hist_genericStatusForROB->GetName(), m_hist_genericStatusForROB).isFailure() ) {
      ATH_MSG_WARNING("Can not register monitoring histogram: " << m_hist_genericStatusForROB->GetName());
    }
  }

  // *-- Specific Status Bits for ROBs per sub detector
  m_hist_specificStatusForROB = new TH2F ("SpecificStatusBitsForROBsFromSubDetectors",
      "SpecificStatusBitsForROBsFromSubDetectors;;",
      n_bins_partEBSubDet,0.,(float) n_bins_partEBSubDet,
      m_vec_SpecificStatus.size(),0., (float) m_vec_SpecificStatus.size());
  if (m_hist_specificStatusForROB) {
    uint32_t n_tmp_bin = 1;
    for (eformat::helper::EnumClass<eformat::SubDetector>::const_iterator it_sub=eformat::helper::SubDetectorDictionary.begin();
        it_sub != eformat::helper::SubDetectorDictionary.end(); ++it_sub ) {
      m_hist_specificStatusForROB->GetXaxis()->SetBinLabel( n_tmp_bin, (it_sub->second).c_str() );
      n_tmp_bin++;
    }

    n_tmp_bin = 1;
    for (std::vector<std::string>::const_iterator it = m_vec_SpecificStatus.begin();it != m_vec_SpecificStatus.end();++it) {
      m_hist_specificStatusForROB->GetYaxis()->SetBinLabel( n_tmp_bin, (*it).c_str() );
      n_tmp_bin++;
    }
    if( rootHistSvc->regHist(path + m_hist_specificStatusForROB->GetName(), m_hist_specificStatusForROB).isFailure() ) {
      ATH_MSG_WARNING("Can not register monitoring histogram: " << m_hist_specificStatusForROB->GetName());
>>>>>>> release/21.0.127
    }
  }
  return(false);
}

void HltROBDataProviderSvc::eventCache_clear(EventCache* cache)
{
  ATH_MSG_VERBOSE("start of " << __FUNCTION__);
  cache->event             = nullptr;
  cache->currentLvl1ID     = 0; 
  cache->globalEventNumber = 0;
  cache->eventStatus       = 0;    
  cache->isEventComplete   = false;    
  { cache->robmap.clear(); }
}

<<<<<<< HEAD
void HltROBDataProviderSvc::eventCache_checkRobListToCache(EventCache* cache, const std::vector<uint32_t>& robIds_toCheck, 
							     std::vector<const ROBF*>& robFragments_inCache, 
							     std::vector<uint32_t>& robIds_missing,
                   std::optional<std::reference_wrapper<std::set<uint32_t>>> robIds_disabled )
{
  ATH_MSG_VERBOSE("start of " << __FUNCTION__ << " number of ROB Ids to check = " << robIds_toCheck.size());
=======
  ATH_MSG_DEBUG(" ---> addROBDataToCache: Number of ROB Ids requested for retrieval : " 
                << robIdsForRetrieval.size() << ", Lvl1 id = " << m_currentLvl1ID);
>>>>>>> release/21.0.127

  // clear output arrays
  robFragments_inCache.clear();
  robIds_missing.clear();

<<<<<<< HEAD
  // allocate sufficient space for output arrays
  robFragments_inCache.reserve( robIds_toCheck.size() );
  robIds_missing.reserve( robIds_toCheck.size() );

  // check input ROB ids
  for (uint32_t id : robIds_toCheck) {

    // check for duplicate IDs on the list of missing ROBs
    std::vector<uint32_t>::iterator missing_it = std::find(robIds_missing.begin(), robIds_missing.end(), id);
    if (missing_it != robIds_missing.end()) {
      ATH_MSG_VERBOSE(__FUNCTION__ << " ROB Id : 0x" << MSG::hex << id << MSG::dec <<" is already on the list of missing IDs.");
=======
  
  std::vector<uint32_t> vRobIds, vMETRobIds;
  vRobIds.reserve( robIdsForRetrieval.size() );
  vMETRobIds.reserve( robIdsForRetrieval.size() );

  // Check requested ROBs
  for (std::vector<uint32_t>::const_iterator rob_it=robIdsForRetrieval.begin(); rob_it!=robIdsForRetrieval.end(); ++rob_it) {
    uint32_t id =  (*rob_it);
    
    // check first if ROB is already in cache
    ONLINE_ROBMAP::iterator map_it = m_online_robmap.find(*rob_it) ;
    if(map_it != m_online_robmap.end()) {
      ATH_MSG_DEBUG(" ---> addROBDataToCache: Found   ROB Id : 0x" << MSG::hex << (*map_it).second.source_id() 
                    << MSG::dec <<" in cache ");
      if ( p_robMonStruct ) {
      	
        (p_robMonStruct->requested_ROBs)[id].rob_history = robmonitor::CACHED;
        (p_robMonStruct->requested_ROBs)[id].rob_size    = ((*map_it).second).fragment_size_word();
        if ( (*map_it).second.nstatus() != 0 ) {
          const uint32_t* it_status;
          (*map_it).second.status(it_status);
          for (uint32_t k=0; k < (*map_it).second.nstatus(); k++) {
            (p_robMonStruct->requested_ROBs)[id].rob_status_words.push_back( *(it_status+k) );
          }
        }
      }
>>>>>>> release/21.0.127
      continue;
    }

<<<<<<< HEAD
    // check if ROB is already in cache
    { ROBMAP::const_iterator map_it = cache->robmap.find(id);
      if (map_it != cache->robmap.end()) {
	ATH_MSG_VERBOSE(__FUNCTION__ << " ROB Id 0x" << MSG::hex << id << MSG::dec
			<< " found for (global Id, L1 Id) = (" << cache->globalEventNumber << "," << cache->currentLvl1ID <<")" );
	robFragments_inCache.push_back( &(map_it->second) );
=======
    // check if ROB is actually enabled for readout
    // do not perform this check for MET ROBs
    if (m_enabledROBs.value().size() != 0) { 
	 if ( (eformat::helper::SourceIdentifier(*rob_it).subdetector_id() != eformat::TDAQ_LAR_MET) &&
	 (eformat::helper::SourceIdentifier(*rob_it).subdetector_id() != eformat::TDAQ_TILE_MET) ){
      std::vector<uint32_t>::const_iterator rob_enabled_it =
	std::find(m_enabledROBs.value().begin(), m_enabledROBs.value().end(),(*rob_it));
      if(rob_enabled_it == m_enabledROBs.value().end()) {
        ATH_MSG_DEBUG(" ---> addROBDataToCache: ROB Id : 0x" << MSG::hex << (*rob_it) << MSG::dec
                      << " will be not retrieved, since it is not on the list of enabled ROBs.");
        if ( p_robMonStruct ) {
        	
        (p_robMonStruct->requested_ROBs)[id].rob_history = robmonitor::DISABLED;
        
      }
>>>>>>> release/21.0.127
	continue;
      }
    }
    
  }

    if (m_ignoreROB.value().size() != 0) {
    std::vector<uint32_t>::const_iterator rob_ignore_it =
    std::find(m_ignoreROB.value().begin(), m_ignoreROB.value().end(),id);
    if(rob_ignore_it != m_ignoreROB.value().end()) {
      ATH_MSG_DEBUG(" ---> addROBDataToCache: ROB Id : 0x" << MSG::hex << id << MSG::dec
      << " will be not retrieved, since it is on the veto list.");
      if ( p_robMonStruct ) {
      	
        (p_robMonStruct->requested_ROBs)[id].rob_history = robmonitor::IGNORED;
       }
       continue;
     }
   }

    // check if ROB is actually enabled for readout
    if (m_enabledROBs.value().size() != 0) {
      std::vector<uint32_t>::const_iterator rob_enabled_it = 
        std::find(m_enabledROBs.value().begin(), m_enabledROBs.value().end(),id);
      if(rob_enabled_it == m_enabledROBs.value().end()) {
        ATH_MSG_VERBOSE(__FUNCTION__ << " ROB Id : 0x" << MSG::hex << id << MSG::dec
                << " will be not added, since it is not on the list of enabled ROBs.");
        if (robIds_disabled) {
          robIds_disabled->get().insert(id);
        }
        continue;
      }
    }
<<<<<<< HEAD

    // the ROB is not in the cache and should be eventually added
    ATH_MSG_VERBOSE(__FUNCTION__ << " ROB Id : 0x" << MSG::hex << id << MSG::dec <<" is missing ");
    robIds_missing.push_back( id ) ;
  } // end loop over input ROB Ids to check
}

void HltROBDataProviderSvc::eventCache_addRobData(EventCache* cache, std::vector<ROBF>&& robFragments,
              std::optional<std::reference_wrapper<std::set<uint32_t>>> robIds_ignored)
{
  ATH_MSG_VERBOSE("start of " << __FUNCTION__ << " number of ROB fragments to add = " << robFragments.size());
=======
  }
 


  typedef std::vector<hltinterface::DCM_ROBInfo> ROBInfoVec;
  ROBInfoVec vRobInfos ;

  // Get ROB Fragments with DataCollector
  if ( m_doMonitoring || p_robMonStruct ) gettimeofday(&time_start, 0);
  if ( vRobIds.size() != 0 ) {
    vRobInfos.reserve( vRobIds.size() ) ;
    hltinterface::DataCollector::instance()->collect(vRobInfos, m_currentLvl1ID, vRobIds);
  }

  // Do a separate data collect call for MET ROBs if required
  if ( (m_separateMETandDetROBRetrieval.value()) && (vMETRobIds.size() != 0) ) {
    ROBInfoVec vMETRobInfos;
    vMETRobInfos.reserve( vMETRobIds.size() ) ;
    // retrieve MET ROBs
    hltinterface::DataCollector::instance()->collect(vMETRobInfos, m_currentLvl1ID, vMETRobIds);

    // add MET ROBs to Det ROBs
    vRobInfos.insert( vRobInfos.end(), vMETRobInfos.begin(), vMETRobInfos.end() );

    
  }



  

  if(msgLvl(MSG::DEBUG) && ((vRobIds.size()!=0) || (vMETRobIds.size()!=0))) {
    std::ostringstream ost;
    unsigned int rob_counter = 1;
    for (const auto& rob : vRobInfos) {
      ost << "       # = "<< std::setw(5) << rob_counter++ << " ROB id = 0x" << std::hex 
          << rob.robFragment.source_id() << std::dec << "\n" ; 
    }
    ATH_MSG_DEBUG(" ---> addROBDataToCache: The following ROB Ids were received from DataCollector : \n"
                  << "      Lvl1 id                                             = " << m_currentLvl1ID << "\n"
                  << "      Number of detector ROB Ids requested for retrieval  = " << vRobIds.size() << "\n"
                  << "      Number of MET ROB Ids requested for retrieval       = " << vMETRobIds.size() << "\n"
                  << "      Number of actually received ROB Ids                 = " << vRobInfos.size() << "\n"
                  << ost.str());
  }
>>>>>>> release/21.0.127

  for (const ROBF& rob : robFragments) {

    // Source ID
    uint32_t id = rob.source_id();
    ATH_MSG_VERBOSE(__FUNCTION__ << " Id = 0x" << std::hex << id << std::dec );

    // mask off the module ID for L2 and EF result for Run 1 data
    if ( (eformat::helper::SourceIdentifier(id).module_id() != 0) &&
	 (eformat::helper::SourceIdentifier(id).subdetector_id() == eformat::TDAQ_LVL2) ) {
      id = eformat::helper::SourceIdentifier(eformat::helper::SourceIdentifier(id).subdetector_id(),0).code();
      if (!m_maskL2EFModuleID) {
	ATH_MSG_ERROR(__FUNCTION__ << " Inconsistent flag for masking L2/EF module IDs");
	m_maskL2EFModuleID=true;
      }
    } else if ( (eformat::helper::SourceIdentifier(id).module_id() != 0) && 
		(eformat::helper::SourceIdentifier(id).subdetector_id() == eformat::TDAQ_EVENT_FILTER) &&
		(m_maskL2EFModuleID) ) {
      id = eformat::helper::SourceIdentifier(eformat::helper::SourceIdentifier(id).subdetector_id(),0).code();
    }

    // check if ROB is already in cache
    { ROBMAP::const_iterator it = cache->robmap.find(id);
      if (it != cache->robmap.end()) {
	ATH_MSG_VERBOSE(__FUNCTION__ << " Duplicate ROB Id 0x" << MSG::hex << id << MSG::dec
			<< " found for (global Id, L1 Id) = (" << cache->globalEventNumber << "," << cache->currentLvl1ID <<")" );
	continue;
      }
    }

    // check for ROBs with no data 
    if ((rob.rod_ndata() == 0) && (m_filterEmptyROB)) {
      ATH_MSG_VERBOSE(__FUNCTION__ << " Empty ROB Id = 0x" << MSG::hex << id << MSG::dec
		      << " removed for (global Id, L1 Id) = (" << cache->globalEventNumber << "," << cache->currentLvl1ID <<")" );
      continue;
    } 

<<<<<<< HEAD
    // filter ROBs with external criteria 
    if (robmap_filterRobWithStatus(&rob)) {
      if (rob.nstatus() > 0) {
	const uint32_t* it_status;
	rob.status(it_status);
	eformat::helper::Status tmpstatus(*it_status);
	ATH_MSG_VERBOSE(__FUNCTION__ << " ROB Id = 0x" << MSG::hex << id << std::setfill('0')
			<< " with Generic Status Code = 0x" << std::setw(4) << tmpstatus.generic()
			<< " and Specific Status Code = 0x" << std::setw(4) << tmpstatus.specific() << MSG::dec
			<< " removed for (global Id, L1 Id) = (" << cache->globalEventNumber << "," << cache->currentLvl1ID <<")" );
      }
      if (robIds_ignored) {
        robIds_ignored->get().insert(id);
=======
// helper function to put retrieved ROB fragments into the local cache and update the monitoring records 
void HltROBDataProviderSvc::updateROBDataCache(std::vector<hltinterface::DCM_ROBInfo>& vRobInfo,
					      robmonitor::ROBDataMonitorStruct* p_robMonStruct) {

  ATH_MSG_DEBUG(" ---> updateROBDataCache: Number of ROB Info records for cache update : " 
                << vRobInfo.size() << ", Lvl1 id = " << m_currentLvl1ID);

  // return if no ROB Info records are available
  if (vRobInfo.size() == 0) return;
  
  
  // add ROBs to cache
  typedef std::vector<hltinterface::DCM_ROBInfo> ROBInfoVec;
  for(ROBInfoVec::const_iterator it=vRobInfo.begin(); it!=vRobInfo.end(); ++it) {
    uint32_t id = it->robFragment.source_id() ;
    
    if ((it->robFragment.rod_ndata() == 0) && (m_removeEmptyROB)) {
      ATH_MSG_DEBUG(" ---> addROBDataToCache: Empty ROB Id = 0x" << MSG::hex << id << MSG::dec
                    << " removed for L1 Id = " << m_currentLvl1ID);
      if ( p_robMonStruct ) {
      	
          (p_robMonStruct->requested_ROBs)[id].rob_history = robmonitor::IGNORED;
        }
    } else if ( ROBDataProviderSvc::filterRobWithStatus(&it->robFragment)) {
      if (msgLvl(MSG::DEBUG) && (it->robFragment.nstatus() > 0)) {
        const uint32_t* it_status;
        it->robFragment.status(it_status);
        eformat::helper::Status tmpstatus( (*it_status) ) ;
        ATH_MSG_DEBUG(" ---> addROBDataToCache: ROB Id = 0x" << MSG::hex << id
                      << std::setfill( '0' )
                      << " with Generic Status Code = 0x" << std::setw(4) << tmpstatus.generic()
                      << " and Specific Status Code = 0x" << std::setw(4) << tmpstatus.specific()
                      << MSG::dec
                      << " removed for L1 Id = " << m_currentLvl1ID);
      }
      if ( p_robMonStruct ) {
      	 (p_robMonStruct->requested_ROBs)[id].rob_history = robmonitor::IGNORED;
        }
    } else {
      m_online_robmap[id]= (it->robFragment);

      //* detailed monitoring
    if ( p_robMonStruct ) {
    	
      (p_robMonStruct->requested_ROBs)[id].rob_history = robmonitor::RETRIEVED;
      (p_robMonStruct->requested_ROBs)[id].rob_size    = it->robFragment.fragment_size_word();
      if ( it->robFragment.nstatus() != 0 ) {
        const uint32_t* it_status;
        it->robFragment.status(it_status);
        for (uint32_t k=0; k < it->robFragment.nstatus(); k++) {
          (p_robMonStruct->requested_ROBs)[id].rob_status_words.push_back( *(it_status+k) );
        }
      }
    } // end detailed monitoring
    }

    //* fill monitoring histogram for ROB generic status
    if ( ( m_hist_genericStatusForROB ) && ( it->robFragment.nstatus() != 0 ) ) {
      const uint32_t* it_status;
      it->robFragment.status(it_status);
      if ((*it_status) != 0) {
        scoped_lock_histogram lock;
        m_hist_genericStatusForROB->Fill(eformat::helper::SourceIdentifier(it->robFragment.source_id()).human_detector().c_str(),
            m_map_GenericStatus[eformat::helper::Status(*it_status).generic()].c_str(),1.);
>>>>>>> release/21.0.127
      }
      continue;
    }

<<<<<<< HEAD
    // add ROB to map
    { cache->robmap.insert(std::make_pair(id,std::move(rob))); }
  }
}

HltROBDataProviderSvc::EventCache::~EventCache()
{
  //  delete event;
  { robmap.clear(); }
}
=======
    //* fill monitoring histogram for ROB specific status
    if ( ( m_hist_specificStatusForROB ) && ( it->robFragment.nstatus() != 0 ) ) {
      const uint32_t* it_status;
      it->robFragment.status(it_status);
      if ((*it_status) != 0) {
        scoped_lock_histogram lock;
        std::bitset<16> specificBits(eformat::helper::Status(*it_status).specific());
        for (unsigned int index=0; index < 16; ++index) {
          if (specificBits[index]) m_hist_specificStatusForROB->Fill(eformat::helper::SourceIdentifier(it->robFragment.source_id()).human_detector().c_str(),
              m_vec_SpecificStatus[index].c_str(),1.);
        }
      }
    }
    
    
  }   // end loop over ROBInfo records
  return;
} // end void updateROBDataCache(...)
>>>>>>> release/21.0.127
