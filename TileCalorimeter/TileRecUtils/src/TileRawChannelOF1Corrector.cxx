/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/
/*
 */

// Tile includes
#include "TileRecUtils/TileRawChannelOF1Corrector.h"
<<<<<<< HEAD
#include "TileEvent/TileMutableRawChannelContainer.h"
=======
>>>>>>> release/21.0.127
#include "TileEvent/TileRawChannelContainer.h"
#include "TileEvent/TileDigitsContainer.h"
#include "TileCalibBlobObjs/TileCalibUtils.h"
#include "TileIdentifier/TileRawChannelUnit.h"
#include "TileIdentifier/TileHWID.h"
<<<<<<< HEAD
=======
#include "TileConditions/TileCondToolNoiseSample.h"
#include "TileConditions/TileCondToolOfc.h"
#include "TileConditions/TileCondToolTiming.h"
#include "TileConditions/TileCondToolEmscale.h"
>>>>>>> release/21.0.127
#include "TileConditions/TileCondToolDspThreshold.h"

// Atlas includes
#include "StoreGate/ReadHandle.h"
#include "AthenaKernel/errorcheck.h"

//========================================================
// constructor
TileRawChannelOF1Corrector::TileRawChannelOF1Corrector(const std::string& type,
    const std::string& name, const IInterface* parent)
    : base_class(type, name, parent)
    , m_tileHWID(0)
<<<<<<< HEAD
{
=======
    , m_tileToolNoiseSample("TileCondToolNoiseSample")
    , m_tileCondToolOfc("TileCondToolOfcCool/TileCondToolOfcCoolOF1")
    , m_tileToolTiming("TileCondToolTiming/TileCondToolOnlineTiming")
    , m_tileToolEms("TileCondToolEmscale")
    , m_tileDspThreshold("TileCondToolDspThreshold")
{
  declareInterface<ITileRawChannelTool>(this);
  declareInterface<TileRawChannelOF1Corrector>(this);

  declareProperty("TileCondToolNoiseSample", m_tileToolNoiseSample);
  declareProperty("TileCondToolOfc", m_tileCondToolOfc);
  declareProperty("TileCondToolTiming", m_tileToolTiming);
  declareProperty("TileCondToolEmscale", m_tileToolEms);
  declareProperty("TileCondToolDspThreshold", m_tileDspThreshold);

  declareProperty("TileDigitsContainer", m_digitsContainerName = "TileDigitsCnt");
>>>>>>> release/21.0.127
  declareProperty("ZeroAmplitudeWithoutDigits", m_zeroAmplitudeWithoutDigits = true);
  declareProperty("CorrectPedestalDifference", m_correctPedestalDifference = true);
}

//========================================================
// Initialize
StatusCode TileRawChannelOF1Corrector::initialize() {

  ATH_MSG_INFO("Initializing...");
<<<<<<< HEAD
=======
  

  const IGeoModelSvc* geoModel = 0;
  CHECK( service("GeoModelSvc", geoModel) );
  
  // dummy parameters for the callback:
  int dummyInt = 0;
  std::list<std::string> dummyList;
  
  if (geoModel->geoInitialized()) {
    return geoInit(dummyInt, dummyList);
  } else {
    CHECK( detStore()->regFcn(&IGeoModelSvc::geoInit, geoModel,
                              &TileRawChannelOF1Corrector::geoInit, this) );
  }
  
  return StatusCode::SUCCESS;
}
>>>>>>> release/21.0.127

  ATH_CHECK( detStore()->retrieve(m_tileHWID) );


<<<<<<< HEAD
  if (m_correctPedestalDifference) {
    //=== get TileCondToolOfc
    ATH_CHECK( m_tileCondToolOfc.retrieve() );

    //=== get TileCondToolNoiseSample
    ATH_CHECK( m_tileToolNoiseSample.retrieve() );

    //=== get TileToolTiming
    ATH_CHECK( m_tileToolTiming.retrieve() );
  } else {
    m_tileCondToolOfc.disable();
    m_tileToolNoiseSample.disable();
    m_tileToolTiming.disable();
  }

  //=== TileCondToolEmscale
  if (m_zeroAmplitudeWithoutDigits) {
    ATH_CHECK( m_tileToolEms.retrieve() );

    //=== get TileToolTiming
    ATH_CHECK( m_tileDspThreshold.retrieve() );
  } else {
    m_tileToolEms.disable();
    m_tileDspThreshold.disable();
  }

  if (m_zeroAmplitudeWithoutDigits) {
    ATH_CHECK( m_digitsContainerKey.initialize() );
  } else {
    m_digitsContainerKey = "";
=======

  if (m_correctPedestalDifference) {
    //=== get TileCondToolOfc
    CHECK( m_tileCondToolOfc.retrieve() );

    //=== get TileCondToolNoiseSample
    CHECK( m_tileToolNoiseSample.retrieve() );

    //=== get TileToolTiming
    CHECK( m_tileToolTiming.retrieve() );
  }

  //=== TileCondToolEmscale
  if (m_zeroAmplitudeWithoutDigits) {
    CHECK( m_tileToolEms.retrieve() );

    //=== get TileToolTiming
    CHECK( m_tileDspThreshold.retrieve() );
>>>>>>> release/21.0.127
  }

  return StatusCode::SUCCESS;
}



// ============================================================================
// process container
StatusCode
TileRawChannelOF1Corrector::process (TileMutableRawChannelContainer& rchCont, const EventContext &ctx) const
{
  ATH_MSG_DEBUG("in TileRawChannelOF1Corrector::process()");

  TileFragHash::TYPE rawChannelType = rchCont.get_type();
  uint32_t bsFlags = rchCont.get_bsflags();

  if ((rawChannelType == TileFragHash::OptFilterDsp 
       || rawChannelType == TileFragHash::OptFilterDspCompressed) // DSP container
      && (bsFlags & (1U << 26U)) == 0) { // OF1 method
<<<<<<< HEAD

    TileRawChannelUnit::UNIT rawChannelUnit = rchCont.get_unit();
    ATH_MSG_VERBOSE( "Units in container is " << rawChannelUnit );

    const TileDigitsContainer* digitsContainer(nullptr);

    if (m_zeroAmplitudeWithoutDigits) {
      SG::ReadHandle<TileDigitsContainer> allDigits(m_digitsContainerKey, ctx);
      digitsContainer = allDigits.cptr();
    }

    for (IdentifierHash hash : rchCont.GetAllCurrentHashes()) {
      TileRawChannelCollection* rawChannelCollection = rchCont.indexFindPtr (hash);
=======
    
    TileRawChannelUnit::UNIT rawChannelUnit = rawChannelContainer->get_unit();
    ATH_MSG_VERBOSE( "Units in container is " << rawChannelUnit );

    const TileDigitsContainer* digitsContainer(nullptr);
    if (m_zeroAmplitudeWithoutDigits) CHECK( evtStore()->retrieve(digitsContainer, m_digitsContainerName) );

    for (const TileRawChannelCollection* rawChannelCollection : *rawChannelContainer) {
>>>>>>> release/21.0.127

      int fragId = rawChannelCollection->identify();
      unsigned int drawer = (fragId & 0x3F);
      unsigned int ros = fragId >> 8;
      unsigned int drawerIdx = TileCalibUtils::getDrawerIdx(ros, drawer);

      bool checkDigits(false);
      std::vector<bool> noDigits(TileCalibUtils::MAX_CHAN, true);
      if (digitsContainer) {
        IdentifierHash fragHash = (digitsContainer->hashFunc())(fragId);
<<<<<<< HEAD
        const TileDigitsCollection* digitsCollection = digitsContainer->indexFindPtr(fragHash);
=======
        const TileDigitsCollection* digitsCollection = *(digitsContainer->indexFind(fragHash));
>>>>>>> release/21.0.127
        if (digitsCollection->getFragBCID() != 0xDEAD) {
          ATH_MSG_VERBOSE(TileCalibUtils::getDrawerString(ros, drawer) << ": digits in bytestream => check digits");
          checkDigits = true;
        } else {
          ATH_MSG_VERBOSE(TileCalibUtils::getDrawerString(ros, drawer) << ": no digits in bytestream => do not check digits");
        }

        for (const TileDigits* tile_digits : *digitsCollection) {
          int channel = m_tileHWID->channel(tile_digits->adc_HWID());
          noDigits[channel] = false;
        }
      }

      for (TileRawChannel* rawChannel : *rawChannelCollection) {
        HWIdentifier adcId = rawChannel->adc_HWID();
        int channel = m_tileHWID->channel(adcId);
        int gain = m_tileHWID->adc(adcId);

        if (m_correctPedestalDifference) {

<<<<<<< HEAD
          float onlinePedestalDifference = m_tileToolNoiseSample->getOnlinePedestalDifference(drawerIdx, channel, gain, rawChannelUnit, ctx);
          float phase = -m_tileToolTiming->getSignalPhase(drawerIdx, channel, gain);
          TileOfcWeightsStruct weights;
          ATH_CHECK( m_tileCondToolOfc->getOfcWeights(drawerIdx, channel, gain, phase, false, weights, ctx) );
          float weightsSum(0.0);
          for (int i = 0; i < weights.n_samples; ++i) weightsSum += weights.w_a[i];
=======
          float onlinePedestalDifference = m_tileToolNoiseSample->getOnlinePedestalDifference(drawerIdx, channel, gain, rawChannelUnit);
          float phase = -m_tileToolTiming->getSignalPhase(drawerIdx, channel, gain);
          const TileOfcWeightsStruct* weights = m_tileCondToolOfc->getOfcWeights(drawerIdx, channel, gain, phase, false);
          float weightsSum(0.0);
          for (int i = 0; i < weights->n_samples; ++i) weightsSum += weights->w_a[i];
>>>>>>> release/21.0.127
          float energyCorrection = onlinePedestalDifference * weightsSum;
          ATH_MSG_VERBOSE( TileCalibUtils::getDrawerString(ros, drawer) 
                           << " ch" << channel << (gain ? " HG: " : " LG: ")
                           << "online pedestal difference: " << onlinePedestalDifference
                           << "; OFC weights (a) sum: " << weightsSum
                           << " => energy correction: " << energyCorrection);
          
          rawChannel->setAmplitude(rawChannel->amplitude() + energyCorrection);
<<<<<<< HEAD

        }

        if (checkDigits && noDigits[channel]) {

          float amplitude = m_tileToolEms->undoOnlCalib(drawerIdx, channel, gain, rawChannel->amplitude(), rawChannelUnit);
          float minimuAmplitudeThreshold(-99999.0);
          float maximumAmplitudeThreshold(99999.0);
          m_tileDspThreshold->getAmplitudeThresholds(drawerIdx, channel, gain, minimuAmplitudeThreshold, maximumAmplitudeThreshold);
          if (amplitude < minimuAmplitudeThreshold
               || amplitude > maximumAmplitudeThreshold) 
            {

            ATH_MSG_VERBOSE( TileCalibUtils::getDrawerString(ros, drawer) 
                             << " ch" << channel << (gain ? " HG:" : " LG:")
                             << " amplitude: " << amplitude << " [ADC]"
                             << " outside range: " << minimuAmplitudeThreshold << ".." << maximumAmplitudeThreshold
                             << " without digits => set apmlitude/time/quality: 0.0/0.0/15.0");

=======

        }

        if (checkDigits && noDigits[channel]) {

          float amplitude = m_tileToolEms->undoOnlCalib(drawerIdx, channel, gain, rawChannel->amplitude(), rawChannelUnit);
          float minimuAmplitudeThreshold(-99999.0);
          float maximumAmplitudeThreshold(99999.0);
          m_tileDspThreshold->getAmplitudeThresholds(drawerIdx, channel, gain, minimuAmplitudeThreshold, maximumAmplitudeThreshold);
          if (amplitude < minimuAmplitudeThreshold
               || amplitude > maximumAmplitudeThreshold) 
            {

            ATH_MSG_VERBOSE( TileCalibUtils::getDrawerString(ros, drawer) 
                             << " ch" << channel << (gain ? " HG:" : " LG:")
                             << " amplitude: " << amplitude << " [ADC]"
                             << " outside range: " << minimuAmplitudeThreshold << ".." << maximumAmplitudeThreshold
                             << " without digits => set apmlitude/time/quality: 0.0/0.0/15.0");

>>>>>>> release/21.0.127
            rawChannel->insert(0.0F, 0.0F, 15.0F);

          }

        }

      }
    }

  }

  return StatusCode::SUCCESS;
}

// ============================================================================
// finalize
StatusCode TileRawChannelOF1Corrector::finalize() {
  return StatusCode::SUCCESS;
}

