/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

//************************************************************
//
// Class TileGeoG4SDTool
// AthTool class for holding the Tile sensitive detector
//
//************************************************************

#ifndef TILEGEOG4SD_TILEGEOG4SDTOOL_H
#define TILEGEOG4SD_TILEGEOG4SDTOOL_H

// Base class header
#include "G4AtlasTools/SensitiveDetectorBase.h"

// STL headers
#include <string>

class G4VSensitiveDetector;
class ITileCalculator;

class TileGeoG4SDTool : public SensitiveDetectorBase
{
public:
  /// Constructor
  TileGeoG4SDTool (const std::string& type, const std::string& name, const IInterface *parent);
  /// Default destructor is just fine
<<<<<<< HEAD

  ///
  virtual StatusCode initialize() override final;

=======

  ///
  virtual StatusCode initialize() override final;

>>>>>>> release/21.0.127
  /// End of an athena event
  virtual StatusCode Gather() override final; //FIXME would be good to be able to avoid this.

protected:
  /// Make me an SD!
<<<<<<< HEAD
  G4VSensitiveDetector* makeSD() const override final;
=======
  G4VSensitiveDetector* makeSD() override final;
>>>>>>> release/21.0.127

private:
  /// Calculator Service
  ServiceHandle<ITileCalculator> m_tileCalculator;
<<<<<<< HEAD
=======
  /// Options for the SD configuration
  TileSDOptions m_options;
>>>>>>> release/21.0.127

};

#endif
