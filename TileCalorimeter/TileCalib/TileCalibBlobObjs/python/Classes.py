# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

<<<<<<< HEAD
import cppyy
=======
try:
    import PyCintex
except:
    import cppyy as PyCintex
>>>>>>> release/21.0.127

TileCalibUtils = cppyy.gbl.TileCalibUtils
TileCalibDrawerCmt = cppyy.gbl.TileCalibDrawerCmt
TileCalibDrawerFlt = cppyy.gbl.TileCalibDrawerFlt
TileCalibDrawerBch = cppyy.gbl.TileCalibDrawerBch
TileCalibDrawerOfc = cppyy.gbl.TileCalibDrawerOfc
TileCalibDrawerInt = cppyy.gbl.TileCalibDrawerInt
TileCalibType      = cppyy.gbl.TileCalibType

TileBchDecoder = cppyy.gbl.TileBchDecoder
TileBchWords   = cppyy.gbl.TileBchWords
TileBchPrbs    = cppyy.gbl.TileBchPrbs
TileBchStatus  = cppyy.gbl.TileBchStatus
