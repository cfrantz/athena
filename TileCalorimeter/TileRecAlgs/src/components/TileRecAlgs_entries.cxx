#include "TileConditions/TileCondToolEmscale.h"
#include "TileConditions/TileCondToolNoiseSample.h"
#include "TileRecUtils/TileCellMaskingTool.h"

<<<<<<< HEAD
#include "../TileCellCorrection.h"
#include "../TileCellSelector.h"
#include "../TileCellIDCToCell.h"
#include "../TileCellVerify.h"
#include "../TileBeamElemToCell.h"
#include "../TileRawChannelToTTL1.h"
#include "../TileDigitsToTTL1.h"
#include "../TileCellToTTL1.h"
#include "../TileRawChannelToHit.h"
#include "../TileDigitsFilter.h"
#include "../TileDigitsThresholdFilter.h"
#include "../MBTSTimeDiffEventInfoAlg.h"
#include "../TileMuonReceiverReadCnt.h"

DECLARE_COMPONENT( TileCellCorrection )
DECLARE_COMPONENT( TileCellSelector )
DECLARE_COMPONENT( TileCellIDCToCell )
DECLARE_COMPONENT( TileCellVerify )
DECLARE_COMPONENT( TileBeamElemToCell )
DECLARE_COMPONENT( TileRawChannelToTTL1 )
DECLARE_COMPONENT( TileDigitsToTTL1 )
DECLARE_COMPONENT( TileRawChannelToHit )
DECLARE_COMPONENT( TileDigitsFilter )
DECLARE_COMPONENT( TileDigitsThresholdFilter )
DECLARE_COMPONENT( TileCellToTTL1 )
DECLARE_COMPONENT( MBTSTimeDiffEventInfoAlg )
DECLARE_COMPONENT( TileMuonReceiverReadCnt )
=======
#include "TileRecAlgs/TileCellCorrection.h"
#include "TileRecAlgs/TileCellSelector.h"
#include "TileRecAlgs/TileCellIDCToCell.h"
#include "TileRecAlgs/TileCellVerify.h"
#include "TileRecAlgs/TileBeamElemToCell.h"
#include "TileRecAlgs/TileRawChannelToTTL1.h"
#include "TileRecAlgs/TileDigitsToTTL1.h"
#include "TileRecAlgs/TileCellToTTL1.h"
#include "TileRecAlgs/TileRawChannelToHit.h"
#include "TileRecAlgs/TileDigitsFilter.h"
#include "TileRecAlgs/TileDigitsThresholdFilter.h"
#include "TileRecAlgs/MBTSTimeDiffEventInfoAlg.h"
#include "TileRecAlgs/TileMuonReceiverReadCnt.h"

DECLARE_SERVICE_FACTORY( TileCellCorrection )
DECLARE_ALGORITHM_FACTORY( TileCellSelector )
DECLARE_ALGORITHM_FACTORY( TileCellIDCToCell )
DECLARE_ALGORITHM_FACTORY( TileCellVerify )
DECLARE_ALGORITHM_FACTORY( TileBeamElemToCell )
DECLARE_ALGORITHM_FACTORY( TileRawChannelToTTL1 )
DECLARE_ALGORITHM_FACTORY( TileDigitsToTTL1 )
DECLARE_ALGORITHM_FACTORY( TileRawChannelToHit )
DECLARE_ALGORITHM_FACTORY( TileDigitsFilter )
DECLARE_ALGORITHM_FACTORY( TileDigitsThresholdFilter )
DECLARE_ALGORITHM_FACTORY( TileCellToTTL1 )
DECLARE_ALGORITHM_FACTORY( MBTSTimeDiffEventInfoAlg )
DECLARE_ALGORITHM_FACTORY( TileMuonReceiverReadCnt )
>>>>>>> release/21.0.127

