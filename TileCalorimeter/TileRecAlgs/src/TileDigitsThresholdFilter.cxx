/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

//*****************************************************************************
//  Filename : TileDigitsThresholdFilter.cxx
//
//  DESCRIPTION
// 
//  Copy TileDigits and TileRawChannel from input container to output container
//
//  HISTORY:
//
//  BUGS:
//
//*****************************************************************************

// Tile includes
<<<<<<< HEAD
#include "TileDigitsThresholdFilter.h"

=======
#include "TileRecAlgs/TileDigitsThresholdFilter.h"
#include "TileEvent/TileDigitsContainer.h"
>>>>>>> release/21.0.127
#include "TileEvent/TileRawChannelContainer.h"
#include "TileConditions/TileCondToolDspThreshold.h"
#include "TileCalibBlobObjs/TileCalibUtils.h"
#include "TileIdentifier/TileHWID.h"

// Atlas includes
<<<<<<< HEAD
#include "AthContainers/ConstDataVector.h"
#include "StoreGate/ReadHandle.h"
#include "StoreGate/WriteHandle.h"
#include "AthenaKernel/errorcheck.h"


=======
#include "AthenaKernel/errorcheck.h"



>>>>>>> release/21.0.127
//C++ STL includes
#include <vector>
#include <algorithm>


//
// Constructor
//
TileDigitsThresholdFilter::TileDigitsThresholdFilter(std::string name, ISvcLocator* pSvcLocator)
  : AthAlgorithm(name, pSvcLocator)
  , m_tileHWID(nullptr)
  , m_tileDspThreshold("TileCondToolDspThreshold")
{
<<<<<<< HEAD
=======
  declareProperty("InputDigitsContainer", m_inputContainer = "TileDigitsCnt");
  declareProperty("OutputDigitsContainer", m_outputContainer = "TileDigitsFiltered");
>>>>>>> release/21.0.127
  declareProperty("TileCondToolDspThreshold", m_tileDspThreshold);
}

TileDigitsThresholdFilter::~TileDigitsThresholdFilter() {
}

//
// Alg standard initialize function
//
StatusCode TileDigitsThresholdFilter::initialize() {

  CHECK( detStore()->retrieve(m_tileHWID) );

  CHECK( m_tileDspThreshold.retrieve() );

<<<<<<< HEAD
  ATH_MSG_INFO( "Input digits container: '" << m_inputDigitsContainerKey.key()
                << "'  output container: '" << m_outputDigitsContainerKey.key() << "'" );

  ATH_CHECK( m_inputDigitsContainerKey.initialize() );
  ATH_CHECK( m_outputDigitsContainerKey.initialize() );
=======
  ATH_MSG_INFO( "Input digits container: '" << m_inputContainer
                << "'  output container: '" << m_outputContainer << "'" );

>>>>>>> release/21.0.127

  ATH_MSG_INFO( "initialization completed" );

  return StatusCode::SUCCESS;
}


/*==========================================================================*/
//
// Begin Execution Phase.
//
StatusCode TileDigitsThresholdFilter::execute() {

  // Create new container for filtered digits
<<<<<<< HEAD
  SG::WriteHandle<TileDigitsContainer> outputDigitsContainer(m_outputDigitsContainerKey);
  ATH_CHECK( outputDigitsContainer.record(std::make_unique<TileDigitsContainer>(false, SG::VIEW_ELEMENTS)) );
  
  SG::ReadHandle<TileDigitsContainer> inputDigitsContainer(m_inputDigitsContainerKey);
  ATH_CHECK( inputDigitsContainer.isValid() );

  outputDigitsContainer->set_unit(inputDigitsContainer->get_unit());
  outputDigitsContainer->set_type(inputDigitsContainer->get_type());
  outputDigitsContainer->set_bsflags(inputDigitsContainer->get_bsflags());

  TileDigitsContainer::const_iterator collItr = inputDigitsContainer->begin();
  TileDigitsContainer::const_iterator collEnd = inputDigitsContainer->end();

  for (; collItr != collEnd; ++collItr) {
    const TileDigitsCollection* digitsCollection = *collItr; 

    auto outputDigitsCollection = std::make_unique<ConstDataVector<TileDigitsCollection> >
      (SG::VIEW_ELEMENTS, digitsCollection->identify());
=======
  TileDigitsContainer* outputContainer = new TileDigitsContainer(true, SG::VIEW_ELEMENTS);
  

  const TileDigitsContainer* inputContainer(nullptr);
  CHECK( evtStore()->retrieve(inputContainer, m_inputContainer) );

  outputContainer->set_unit(inputContainer->get_unit());
  outputContainer->set_type(inputContainer->get_type());
  outputContainer->set_bsflags(inputContainer->get_bsflags());

  for (const TileDigitsCollection* digitsCollection : *inputContainer) {
>>>>>>> release/21.0.127

    int fragId = digitsCollection->identify();
    unsigned int drawerIdx = TileCalibUtils::getDrawerIdxFromFragId(fragId);

<<<<<<< HEAD
    for (const TileDigits* tile_digits : *digitsCollection) {
=======
    for (TileDigits* tile_digits : *digitsCollection) {

>>>>>>> release/21.0.127
      HWIdentifier adcId = tile_digits->adc_HWID();
      int channel = m_tileHWID->channel(adcId);
      int gain = m_tileHWID->adc(adcId);

      float dspThreshold = m_tileDspThreshold->getDspThreshold(drawerIdx, channel, gain);

<<<<<<< HEAD
      const std::vector<float>& digits = tile_digits->samples();
=======
      const std::vector<float> digits = tile_digits->samples();
>>>>>>> release/21.0.127
      auto minMaxDigits = std::minmax_element(digits.begin(), digits.end());
      float minDigit = *minMaxDigits.first;
      float maxDigit = *minMaxDigits.second;

      if (maxDigit - minDigit > dspThreshold) {
<<<<<<< HEAD
        outputDigitsCollection->push_back(tile_digits);
      }

    }
    ATH_CHECK( outputDigitsContainer->addCollection (outputDigitsCollection.release()->asDataVector(), 
                                                     collItr.hashId()) );
    
  }

=======
        outputContainer->push_back(tile_digits);
      }

    }
  }

  CHECK( evtStore()->record(outputContainer, m_outputContainer, false) );
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}


StatusCode TileDigitsThresholdFilter::finalize() {

  ATH_MSG_DEBUG( "in finalize()" );

  return StatusCode::SUCCESS;
}

