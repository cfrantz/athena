/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#ifndef TILECONDITIONS_TILECONDTOOLTMDB_H
#define TILECONDITIONS_TILECONDTOOLTMDB_H

// Tile includes
#include "TileConditions/ITileCondToolTMDB.h"
<<<<<<< HEAD
#include "TileConditions/TileCalibData.h"

// Athena includes
#include "AthenaBaseComps/AthAlgTool.h"
#include "StoreGate/ReadCondHandleKey.h"
=======
#include "TileConditions/ITileCondProxy.h"

// Athena includes
#include "AthenaBaseComps/AthAlgTool.h"

// Gaudi includes
#include "GaudiKernel/ToolHandle.h"

class TileCalibDrawerFlt;
>>>>>>> release/21.0.127

class TileCondToolTMDB: public AthAlgTool, virtual public ITileCondToolTMDB {

  public:

    static const InterfaceID& interfaceID() {
<<<<<<< HEAD
      static const InterfaceID IID_TileCondToolTMDB("TileCondToolTMDB", 1, 0);
=======
      static const InterfaceID IID_TileCondToolTMDB("TileCondToolTMDB", 1, 0);    
>>>>>>> release/21.0.127
      return IID_TileCondToolTMDB;
    };

    TileCondToolTMDB(const std::string& type, const std::string& name, const IInterface* parent);
    virtual ~TileCondToolTMDB();

    virtual StatusCode initialize() override;
    virtual StatusCode finalize() override;

<<<<<<< HEAD
    virtual float getThreshold(unsigned int drawerIdx, unsigned int threshold) const override;
    virtual float getDelay(unsigned int drawerIdx, unsigned int channel) const override;

    virtual void getCalib(unsigned int drawerIdx, unsigned int channel, float& a, float& b) const override;
    virtual unsigned int getWeights(unsigned int drawerIdx, unsigned int channel, TMDB::Weights& weights) const override;

    virtual float channelCalib(unsigned int drawerIdx, unsigned int channel, const std::vector<float>& samples) const override;
    virtual float channelCalib(unsigned int drawerIdx, unsigned int channel, float amplitude) const override;

  private:

    SG::ReadCondHandleKey<TileCalibDataFlt> m_calibThresholdKey{this,
        "TileTMDBThreshold", "TileTMDBThreshold", "Input Tile TMDB threshold calibration constants"};

    SG::ReadCondHandleKey<TileCalibDataFlt> m_calibDelayKey{this,
        "TileTMDBDelay", "TileTMDBDelay", "Input Tile TMDB delay calibration constants"};

    SG::ReadCondHandleKey<TileCalibDataFlt> m_calibTmfKey{this,
        "TileTMDBTMF", "TileTMDBTMF", "Input Tile TMDB TMF calibration constants"};

    SG::ReadCondHandleKey<TileCalibDataFlt> m_calibDataKey{this,
        "TileTMDBCalib", "TileTMDBCalib", "Input Tile TMDB calibration constants"};


=======
    virtual float getThreshold(unsigned int drawerIdx, TMDB::THRESHOLD threshold) const override;
    virtual float getDelay(unsigned int drawerIdx, TMDB::CHANNEL channel) const override;

    virtual void getCalib(unsigned int drawerIdx, TMDB::CHANNEL channel, float& a, float& b) const override;
    virtual unsigned int getWeights(unsigned int drawerIdx, TMDB::CHANNEL channel, TMDB::Weights& weights) const override;
    
    virtual float channelCalib(unsigned int drawerIdx, TMDB::CHANNEL channel, const std::vector<float>& samples) const override;
    virtual float channelCalib(unsigned int drawerIdx, TMDB::CHANNEL channel, float amplitude) const override;

  private:

    //=== TileCondProxies
    ToolHandle<ITileCondProxy<TileCalibDrawerFlt> > m_pryThreshold;
    ToolHandle<ITileCondProxy<TileCalibDrawerFlt> > m_pryDelay;
    ToolHandle<ITileCondProxy<TileCalibDrawerFlt> > m_pryTMF;
    ToolHandle<ITileCondProxy<TileCalibDrawerFlt> > m_pryCalib;
>>>>>>> release/21.0.127

};

#endif
