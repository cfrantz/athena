/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

// Tile includes
#include "TileConditions/TileCondToolOfcCool.h"
#include "TileCalibBlobObjs/TileCalibDrawerOfc.h"


// Athena includes
#include "AthenaKernel/errorcheck.h"
#include "StoreGate/ReadCondHandle.h"

//____________________________________________________________________
TileCondToolOfcCool::TileCondToolOfcCool(const std::string& type, const std::string& name,
    const IInterface* parent)
    : base_class(type, name, parent)
{
}

//
//____________________________________________________________________
TileCondToolOfcCool::~TileCondToolOfcCool() {

}

//
//____________________________________________________________________
StatusCode TileCondToolOfcCool::initialize() {
  ATH_MSG_DEBUG( "In initialize()" );

  ATH_CHECK( m_calibOfcKey.initialize() );

  return StatusCode::SUCCESS;
}

//
//____________________________________________________________________
StatusCode TileCondToolOfcCool::finalize() {
  ATH_MSG_DEBUG( "finalize called" );

  return StatusCode::SUCCESS;
}

//
//____________________________________________________________________
<<<<<<< HEAD
StatusCode
TileCondToolOfcCool::getOfcWeights(unsigned int drawerIdx,
                                   unsigned int channel,
                                   unsigned int adc,
                                   float& phase,
                                   bool /*of2*/,
                                   TileOfcWeightsStruct& weights,
                                   const EventContext& ctx) const
{
  std::fill (std::begin(weights.g), std::end(weights.g), 0);
  std::fill (std::begin(weights.dg), std::end(weights.dg), 0);
  std::fill (std::begin(weights.w_a), std::end(weights.w_a), 0);
  std::fill (std::begin(weights.w_b), std::end(weights.w_b), 0);
  std::fill (std::begin(weights.w_c), std::end(weights.w_c), 0);
=======
const TileOfcWeightsStruct* TileCondToolOfcCool::getOfcWeights(unsigned int drawerIdx, 
                                                               unsigned int channel, 
                                                               unsigned int adc,
                                                               float& phase, 
                                                               bool /* of2 */) {

  if (drawerIdx >= TileCalibUtils::MAX_DRAWERIDX) {
    throw TileCalib::IndexOutOfRange("TileCondToolOfcCool::getOfcWeights", drawerIdx, TileCalibUtils::MAX_DRAWERIDX);
  }

  if (m_first) {
    m_first = false;
    m_NPhases = m_pryOfcCool->getCalibDrawer(drawerIdx)->getNPhases();
    m_NFields = m_pryOfcCool->getCalibDrawer(drawerIdx)->getNFields();
    m_NSamples = m_pryOfcCool->getCalibDrawer(drawerIdx)->getNSamples();

    ATH_MSG_DEBUG( "OFC Blob Type " << m_pryOfcCool->getCalibDrawer(drawerIdx)->getType()
                   << " NPhases " << m_NPhases
                   << " NFields " << m_NFields );
>>>>>>> release/21.0.127

  SG::ReadCondHandle<TileCalibData<TileCalibDrawerOfc>> calibOFC(m_calibOfcKey, ctx);

<<<<<<< HEAD
  calibOFC->getCalibDrawer(drawerIdx)->fillOfc(channel, adc, phase, weights.w_a, weights.w_b,
                                               weights.w_c, weights.g, weights.dg);
=======
  m_pryOfcCool->getCalibDrawer(drawerIdx)->fillOfc(channel, adc, phase, m_weights->w_a, m_weights->w_b, 
                                                   m_weights->w_c, m_weights->g, m_weights->dg);
>>>>>>> release/21.0.127

  weights.n_samples = calibOFC->getCalibDrawer(drawerIdx)->getNSamples();

  return StatusCode::SUCCESS;
}

//
//____________________________________________________________________
int TileCondToolOfcCool::getOfcWeights(unsigned int drawerIdx,
                                       unsigned int channel,
                                       unsigned int adc,
                                       float& phase, 
<<<<<<< HEAD
                                       float *a, float *b, float *c, float *g, float *dg,
                                       const EventContext& ctx) {
=======
                                       float *a, float *b, float *c, float *g, float *dg) {

  if (drawerIdx >= TileCalibUtils::MAX_DRAWERIDX) {
    throw TileCalib::IndexOutOfRange("TileCondToolOfcCool::getOfcWeights", drawerIdx, TileCalibUtils::MAX_DRAWERIDX);
  }

  if (m_first) {
    m_first = false;
    m_NPhases = m_pryOfcCool->getCalibDrawer(drawerIdx)->getNPhases();
    m_NFields = m_pryOfcCool->getCalibDrawer(drawerIdx)->getNFields();
    m_NSamples = m_pryOfcCool->getCalibDrawer(drawerIdx)->getNSamples();

    ATH_MSG_DEBUG( "OFC Blob Type " << m_pryOfcCool->getCalibDrawer(drawerIdx)->getType()
                  << " NPhases " << m_NPhases
                  << " NFields " << m_NFields );
>>>>>>> release/21.0.127

  SG::ReadCondHandle<TileCalibData<TileCalibDrawerOfc>> calibOFC(m_calibOfcKey, ctx);

<<<<<<< HEAD
  calibOFC->getCalibDrawer(drawerIdx)->fillOfc(channel, adc, phase, a, b, c, g, dg);
=======
  m_pryOfcCool->getCalibDrawer(drawerIdx)->fillOfc(channel, adc, phase, a, b, c, g, dg);
>>>>>>> release/21.0.127

  return calibOFC->getCalibDrawer(drawerIdx)->getNSamples();
}

//
//____________________________________________________________________
void TileCondToolOfcCool::getOfcParams(unsigned int drawerIdx
                                       , int &NPhases
                                       , int &NFields
                                       , int &Phamin
                                       , int &Phamax
                                       , int &NSamples
                                       , const EventContext& ctx) {

  SG::ReadCondHandle<TileCalibData<TileCalibDrawerOfc>> calibOFC(m_calibOfcKey, ctx);

  NPhases = calibOFC->getCalibDrawer(drawerIdx)->getNPhases();
  NFields = calibOFC->getCalibDrawer(drawerIdx)->getNFields();
  Phamin = round(calibOFC->getCalibDrawer(drawerIdx)->getPhase(0, 0, 0) * (1 / PHASE_PRECISION));
  Phamax = round(calibOFC->getCalibDrawer(drawerIdx)->getPhase(0, 0, abs(NPhases) - 1) * (1 / PHASE_PRECISION));
  NSamples = calibOFC->getCalibDrawer(drawerIdx)->getNSamples();
}

//
//____________________________________________________________________

