/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

<<<<<<< HEAD
// Tile includes
#include "TileConditions/TileCondToolDspThreshold.h"
#include "TileCalibBlobObjs/TileCalibDrawerFlt.h"

// Athena includes
#include "AthenaKernel/errorcheck.h"
#include "StoreGate/ReadCondHandle.h"
=======

#include "TileConditions/TileCondToolDspThreshold.h"

// Tile includes
#include "TileCalibBlobObjs/TileCalibDrawerFlt.h"
#include "TileCalibBlobObjs/TileCalibUtils.h"
#include "TileCalibBlobObjs/Exception.h"

// Athena includes
#include "AthenaKernel/errorcheck.h"

>>>>>>> release/21.0.127


//
//____________________________________________________________________
TileCondToolDspThreshold::TileCondToolDspThreshold(const std::string& type, const std::string& name, const IInterface* parent)
  : AthAlgTool(type, name, parent)
<<<<<<< HEAD
{
  declareInterface<ITileCondToolDspThreshold>(this);
  declareInterface<TileCondToolDspThreshold>(this);
=======
  , m_pryDspThreshold("TileCondProxyFile_TileCalibDrawerFlt_/TileCondProxyDefault_Threshold", this)
{
  declareInterface<ITileCondToolDspThreshold>(this);
  declareInterface<TileCondToolDspThreshold>(this);
  declareProperty("ProxyDspThreshold", m_pryDspThreshold);
>>>>>>> release/21.0.127
}

//
//____________________________________________________________________
TileCondToolDspThreshold::~TileCondToolDspThreshold() {
}

//
//____________________________________________________________________
StatusCode TileCondToolDspThreshold::initialize() {

  ATH_MSG_DEBUG( "In initialize()" );

<<<<<<< HEAD
  //=== Initialize conditions data key with DSP thresholds
  ATH_CHECK( m_calibDspThresholdKey.initialize() );
=======
  //=== retrieve proxy
  CHECK( m_pryDspThreshold.retrieve() );
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}

//
//____________________________________________________________________
StatusCode TileCondToolDspThreshold::finalize() {
  ATH_MSG_DEBUG( "finalize called" );

  return StatusCode::SUCCESS;
}


//
//____________________________________________________________________
float TileCondToolDspThreshold::getMinimumAmplitudeThreshold(unsigned int drawerIdx, unsigned int channel, unsigned int adc) const {
<<<<<<< HEAD

  SG::ReadCondHandle<TileCalibData<TileCalibDrawerFlt>> calibDspThreshold(m_calibDspThresholdKey);
  return calibDspThreshold->getCalibDrawer(drawerIdx)->getData(channel, adc, 0);

=======
  if (drawerIdx >= TileCalibUtils::MAX_DRAWERIDX) {
    throw TileCalib::IndexOutOfRange("TileCondToolDspThreshold::getThreshold", drawerIdx, TileCalibUtils::MAX_DRAWERIDX);
  }

  return m_pryDspThreshold->getCalibDrawer(drawerIdx)->getData(channel, adc, 0);
>>>>>>> release/21.0.127
}


//
//____________________________________________________________________
float TileCondToolDspThreshold::getMaximumAmplitudeThreshold(unsigned int drawerIdx, unsigned int channel, unsigned int adc) const {
<<<<<<< HEAD

  SG::ReadCondHandle<TileCalibData<TileCalibDrawerFlt>> calibDspThreshold(m_calibDspThresholdKey);
  return calibDspThreshold->getCalibDrawer(drawerIdx)->getData(channel, adc, 1);

=======
  if (drawerIdx >= TileCalibUtils::MAX_DRAWERIDX) {
    throw TileCalib::IndexOutOfRange("TileCondToolDspThreshold::getThreshold", drawerIdx, TileCalibUtils::MAX_DRAWERIDX);
  }

  return m_pryDspThreshold->getCalibDrawer(drawerIdx)->getData(channel, adc, 1);
>>>>>>> release/21.0.127
}


//
//____________________________________________________________________
void TileCondToolDspThreshold::getAmplitudeThresholds(unsigned int drawerIdx, unsigned int channel, unsigned int adc, 
                                                       float& minimumThreshold, float& maximumThreshold) const {
<<<<<<< HEAD

  SG::ReadCondHandle<TileCalibData<TileCalibDrawerFlt>> calibDspThreshold(m_calibDspThresholdKey);
  minimumThreshold = calibDspThreshold->getCalibDrawer(drawerIdx)->getData(channel, adc, 0);
  maximumThreshold = calibDspThreshold->getCalibDrawer(drawerIdx)->getData(channel, adc, 1);

=======
  if (drawerIdx >= TileCalibUtils::MAX_DRAWERIDX) {
    throw TileCalib::IndexOutOfRange("TileCondToolDspThreshold::getThreshold", drawerIdx, TileCalibUtils::MAX_DRAWERIDX);
  }

  minimumThreshold = m_pryDspThreshold->getCalibDrawer(drawerIdx)->getData(channel, adc, 0);
  maximumThreshold = m_pryDspThreshold->getCalibDrawer(drawerIdx)->getData(channel, adc, 1);
>>>>>>> release/21.0.127
}


//
//____________________________________________________________________
float TileCondToolDspThreshold::getDspThreshold(unsigned int drawerIdx, unsigned int channel, unsigned int adc) const {
<<<<<<< HEAD

  SG::ReadCondHandle<TileCalibData<TileCalibDrawerFlt>> calibDspThreshold(m_calibDspThresholdKey);
  return calibDspThreshold->getCalibDrawer(drawerIdx)->getData(channel, adc, 2);

=======
  if (drawerIdx >= TileCalibUtils::MAX_DRAWERIDX) {
    throw TileCalib::IndexOutOfRange("TileCondToolDspThreshold::getThreshold", drawerIdx, TileCalibUtils::MAX_DRAWERIDX);
  }

  return m_pryDspThreshold->getCalibDrawer(drawerIdx)->getData(channel, adc, 2);
>>>>>>> release/21.0.127
}

