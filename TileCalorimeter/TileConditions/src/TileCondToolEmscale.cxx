/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// Tile includes
#include "TileConditions/TileCondToolEmscale.h"
#include "TileConditions/TileCablingService.h"
#include "TileConditions/TileCablingSvc.h"

#include "TileCalibBlobObjs/TileCalibUtils.h"

// Athena includes
#include "AthenaKernel/errorcheck.h"
#include "StoreGate/ReadCondHandle.h"


// Athena includes
#include "AthenaKernel/errorcheck.h"


//
//____________________________________________________________________
static const InterfaceID IID_TileCondToolEmscale("TileCondToolEmscale", 1, 0);

const InterfaceID& TileCondToolEmscale::interfaceID() {
  return IID_TileCondToolEmscale;
}

//
//____________________________________________________________________
TileCondToolEmscale::TileCondToolEmscale(const std::string& type, const std::string& name,
    const IInterface* parent)
    : AthAlgTool(type, name, parent)
<<<<<<< HEAD
=======
    , m_onlCacheUnit( TileRawChannelUnit::Invalid)
    , m_onlCache(std::vector<float>(0))
    , m_OflLasLinUsed(true)
    , m_OflLasNlnUsed(true)
    //=== Offline folders are by default read from ASCII file
    , m_pryOflCisLin("TileCondProxyFile_TileCalibDrawerFlt_/TileCondProxyDefault_OflCisLin", this)
    , m_pryOflCisNln("TileCondProxyFile_TileCalibDrawerFlt_/TileCondProxyDefault_OflCisNln", this)
    , m_pryOflLasLin("TileCondProxyFile_TileCalibDrawerFlt_/TileCondProxyDefault_OflLasLin", this)
    , m_pryOflLasNln("TileCondProxyFile_TileCalibDrawerFlt_/TileCondProxyDefault_OflLasNln", this)
    , m_pryOflLasFib("TileCondProxyFile_TileCalibDrawerFlt_/TileCondProxyDefault_OflLasFib", this)
    , m_pryOflCes("TileCondProxyFile_TileCalibDrawerFlt_/TileCondProxyDefault_OflCes", this)
    , m_pryOflEms("TileCondProxyFile_TileCalibDrawerFlt_/TileCondProxyDefault_OflEms", this)
    //=== Online folders can only be read from the COOL DB
    , m_pryOnlCis("TileCondProxyCool_TileCalibDrawerFlt_/TileCondProxyDefault_OnlCis", this)
    , m_pryOnlLas("TileCondProxyCool_TileCalibDrawerFlt_/TileCondProxyDefault_OnlLas", this)
    , m_pryOnlCes("TileCondProxyCool_TileCalibDrawerFlt_/TileCondProxyDefault_OnlCes", this)
    , m_pryOnlEms("TileCondProxyCool_TileCalibDrawerFlt_/TileCondProxyDefault_OnlEms", this)
    , m_useOflLasFib(false)
    , m_maxChannels(0)
    , m_maxGains(0)
    , m_drawerCacheSize(0)
>>>>>>> release/21.0.127
{
  declareInterface<TileCondToolEmscale>(this);

}

//
//____________________________________________________________________
TileCondToolEmscale::~TileCondToolEmscale() {
}

//
//____________________________________________________________________
StatusCode TileCondToolEmscale::initialize() {

  ATH_MSG_DEBUG( "In initialize()" );
<<<<<<< HEAD
  ATH_CHECK( m_emScaleKey.initialize() );
=======

  //=== Configure online cache unit
  if (m_onlCacheUnitStr == "Invalid") {
    m_onlCacheUnit = TileRawChannelUnit::Invalid;
  } else if (m_onlCacheUnitStr == "OnlineADCcounts") {
    m_onlCacheUnit = TileRawChannelUnit::OnlineADCcounts;
  } else if (m_onlCacheUnitStr == "OnlinePicoCoulombs") {
    m_onlCacheUnit = TileRawChannelUnit::OnlinePicoCoulombs;
  } else if (m_onlCacheUnitStr == "OnlineCesiumPicoCoulombs") {
    m_onlCacheUnit = TileRawChannelUnit::OnlineCesiumPicoCoulombs;
  } else if (m_onlCacheUnitStr == "OnlineMegaElectronVolts") {
    m_onlCacheUnit = TileRawChannelUnit::OnlineMegaElectronVolts;
  } else {
    ATH_MSG_WARNING( "Unrecoginzed jobOption OnlCacheUnit=" << m_onlCacheUnitStr << ". "
                     << "Setting it to \"Invalid\"!" );
    m_onlCacheUnit = TileRawChannelUnit::Invalid;
  }

  //=== Retrieve offline proxies
  CHECK( m_pryOflCisLin.retrieve() );
  CHECK( m_pryOflCisNln.retrieve() );
  CHECK( m_pryOflLasLin.retrieve() );
  CHECK( m_pryOflLasNln.retrieve() );
  CHECK( m_pryOflCes.retrieve() );
  CHECK( m_pryOflEms.retrieve() );

  m_useOflLasFib = !(m_pryOflLasFib.empty());
  if (m_useOflLasFib) {
    //=== retrieve offline proxy
    CHECK( m_pryOflLasFib.retrieve() );
    ATH_MSG_INFO("ProxyOflLasFib is set up and can be used");
  } else {
    ATH_MSG_INFO("ProxyOflLasFib is not set up and cannot be used");
  }

  //=== Attempt to load online folders only if online cache unit is valid
  if (m_onlCacheUnit != TileRawChannelUnit::Invalid) {

    //=== Retrieve offline proxies
    CHECK( m_pryOnlCis.retrieve() );
    CHECK( m_pryOnlLas.retrieve() );
    CHECK( m_pryOnlCes.retrieve() );
    CHECK( m_pryOnlEms.retrieve() );

    //=== Undoing online corrections works only with connection to COOL DB
    //=== i.e. ensure that we have only TileCondProxyCool tools
    const TileCondProxyCoolFlt* ptrPryOnlCis = dynamic_cast<const TileCondProxyCoolFlt*>(&(*m_pryOnlCis));
    const TileCondProxyCoolFlt* ptrPryOnlLas = dynamic_cast<const TileCondProxyCoolFlt*>(&(*m_pryOnlLas));
    const TileCondProxyCoolFlt* ptrPryOnlCes = dynamic_cast<const TileCondProxyCoolFlt*>(&(*m_pryOnlCes));
    const TileCondProxyCoolFlt* ptrPryOnlEms = dynamic_cast<const TileCondProxyCoolFlt*>(&(*m_pryOnlEms));

    if (!ptrPryOnlCis || !ptrPryOnlLas || !ptrPryOnlCes || !ptrPryOnlEms) {
      ATH_MSG_ERROR( "Requested valid OnlCacheUnit (" << m_onlCacheUnitStr << ") "
                    << "but at least one ProxyOnl{Cis|Las|Ces|Ems} is not connected to a COOL folder!" );
      return StatusCode::FAILURE;
    }

    // 
    const TileCondProxyCoolFlt* ptrPryOflLasLin = dynamic_cast<const TileCondProxyCoolFlt*>(&(*m_pryOflLasLin));
    const TileCondProxyCoolFlt* ptrPryOflLasNln = dynamic_cast<const TileCondProxyCoolFlt*>(&(*m_pryOflLasNln));
    if (!ptrPryOflLasLin || !ptrPryOflLasNln) {
      ATH_MSG_ERROR( "At least one ProxyOfl{LasLin|LasNln} is not connected to a COOL folder!" );
      return StatusCode::FAILURE;
    }

    //=== Register resetCache callback function for online folders
    //---
    //--- This below is a bit tricky:
    //--- The regFcn needs a pointer to the function actually called, a pointer to a callback function in a base class 
    //--- (for example a purely virtual function in ITileCondProxy) does NOT work. 
    //--- Therefore we need to provide a pointer to the callback function of the derived class. Hence we ensured
    //--- with above code that we really have a TileCondProxyCool connected to the ToolHandle.
    //---
    CHECK( detStore()->regFcn(&TileCondProxyCoolFlt::callback, ptrPryOnlCis, &TileCondToolEmscale::resetOnlCache, this, true) );
    CHECK( detStore()->regFcn(&TileCondProxyCoolFlt::callback, ptrPryOnlLas, &TileCondToolEmscale::resetOnlCache, this, true) );
    CHECK( detStore()->regFcn(&TileCondProxyCoolFlt::callback, ptrPryOnlCes, &TileCondToolEmscale::resetOnlCache, this, true) );
    CHECK( detStore()->regFcn(&TileCondProxyCoolFlt::callback, ptrPryOnlEms, &TileCondToolEmscale::resetOnlCache, this, true) );

    //=== Register checkIfOflLasLinCalibUsed callback function for offline linear laser folder
    CHECK( detStore()->regFcn(&TileCondProxyCoolFlt::callback, ptrPryOflLasLin, &TileCondToolEmscale::checkIfOflLasLinCalibUsed, this, true) );

    //=== Register checkIfOflLasNlnCalibUsed callback function for offline nonlinear laser folder
    CHECK( detStore()->regFcn(&TileCondProxyCoolFlt::callback, ptrPryOflLasNln, &TileCondToolEmscale::checkIfOflLasNlnCalibUsed, this, true) );

    //=== Resize onlCache to desired size

    ServiceHandle<TileCablingSvc> cablingSvc("TileCablingSvc", name());
    CHECK( cablingSvc.retrieve());
    
    const TileCablingService* cabling = cablingSvc->cablingService();
    if (!cabling) {
      ATH_MSG_ERROR( "Unable to retrieve TileCablingService" );
      return StatusCode::FAILURE;
    }
    
    m_maxChannels = cabling->getMaxChannels();
    m_maxGains = cabling->getMaxGains();
    m_drawerCacheSize = m_maxChannels * m_maxGains;
    
    m_onlCache.resize(m_drawerCacheSize * TileCalibUtils::MAX_DRAWERIDX);
    
  } else {
    ATH_MSG_INFO( "Loading of online calibration folders not requested, "
                  << "since OnlCacheUnit=" << m_onlCacheUnitStr  );
  }

>>>>>>> release/21.0.127
  return StatusCode::SUCCESS;
}

//
//____________________________________________________________________
StatusCode TileCondToolEmscale::finalize() {

  ATH_MSG_DEBUG( "finalize called" );
  return StatusCode::SUCCESS;

<<<<<<< HEAD
=======
//
//____________________________________________________________________
StatusCode TileCondToolEmscale::resetOnlCache( IOVSVC_CALLBACK_ARGS_K(keys)) {
  msg(MSG::INFO) << "TileCondToolEmscale::resetOnlCache() has been triggered by: ";
  std::list<std::string>::const_iterator itr;
  for (itr = keys.begin(); itr != keys.end(); ++itr) {
    msg(MSG::INFO) << "\'" << *itr << "' ";
  }
  msg(MSG::INFO) << endmsg;

  //=== recalculate online cache
  for (unsigned int drawerIdx = 0; drawerIdx < TileCalibUtils::MAX_DRAWERIDX; ++ drawerIdx) {
    for (unsigned int channel = 0; channel < m_maxChannels; ++channel) {
      for (unsigned int adc = 0; adc < m_maxGains; ++adc) {
        m_onlCache[cacheIndex(drawerIdx, channel, adc)] = getOnlCalib(drawerIdx, channel, adc, m_onlCacheUnit);      
      }
    }
  }

  return StatusCode::SUCCESS;
>>>>>>> release/21.0.127
}


//
//____________________________________________________________________
float TileCondToolEmscale::undoOnlCalib(unsigned int drawerIdx, unsigned int channel, unsigned int adc
                                        , float amplitude, TileRawChannelUnit::UNIT onlUnit) const {

<<<<<<< HEAD
  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->undoOnlineChannelCalibration(drawerIdx, channel, adc, amplitude, onlUnit);

=======
  //=== Check if online folders are available
  if (m_onlCacheUnit == TileRawChannelUnit::Invalid) {
    ATH_MSG_FATAL( "Trying to undo online calibration, but COOL folders were not loaded" );
    std::abort();
  }

  //=== Check for valid unit request
  if (onlUnit < TileRawChannelUnit::OnlineADCcounts
      || onlUnit > TileRawChannelUnit::OnlineMegaElectronVolts) {
    throw TileCalib::InvalidRawChanUnit("TileCondToolEmscale::channelCalib(onl)", onlUnit);
  }

  float val(0.);
  //=== Look up total calib constant in cache if possible ...
  if (onlUnit == m_onlCacheUnit) {
    unsigned int idx = cacheIndex(drawerIdx, channel, adc);
    if (idx >= m_onlCache.size()) {
      throw TileCalib::IndexOutOfRange("TileCondToolEmscale::undoOnlineCalib", idx, m_onlCache.size());
    }

    val = m_onlCache[idx];
  }
  //=== ... otherwise compute on the fly  
  else {
    val = getOnlCalib(drawerIdx, channel, adc, onlUnit);
  }

  //=== Sanity check
  if (val == 0.) {
    throw TileCalib::InvalidValue("TileCondToolEmscale::undoOnlCalib", val);
  }

  return amplitude / val;
>>>>>>> release/21.0.127
}

//
//____________________________________________________________________
float TileCondToolEmscale::channelCalib(unsigned int drawerIdx, unsigned int channel, unsigned int adc
                                        , float amplitude, TileRawChannelUnit::UNIT rawDataUnitIn
                                        , TileRawChannelUnit::UNIT rawDataUnitOut) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->calibrateChannel(drawerIdx, channel, adc, amplitude, rawDataUnitIn, rawDataUnitOut);

}

//
//____________________________________________________________________
float TileCondToolEmscale::doCalibEms(unsigned int drawerIdx, unsigned int channel, float amplitude) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->applyEMScaleCalibration(drawerIdx, channel, amplitude);

}

//
//____________________________________________________________________
float TileCondToolEmscale::doCalibCes(unsigned int drawerIdx, unsigned int channel, float amplitude, bool applyLasCorr) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->applyCesiumCalibration(drawerIdx, channel, amplitude, applyLasCorr);

}

//
//____________________________________________________________________
float TileCondToolEmscale::doCalibLas(unsigned int drawerIdx, unsigned int channel, float amplitude) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->applyLaserCalibration(drawerIdx, channel, amplitude);

}

//
//____________________________________________________________________
float TileCondToolEmscale::doCalibCis(unsigned int drawerIdx, unsigned int channel, unsigned int adc, float amplitude) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->applyChargeCalibration(drawerIdx, channel, adc, amplitude);

}

//
//____________________________________________________________________
float TileCondToolEmscale::channelCalibOnl(unsigned int drawerIdx, unsigned int channel, unsigned int adc
                                           , float amplitude, TileRawChannelUnit::UNIT onlUnit) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->calibrateOnlineChannel(drawerIdx, channel, adc, amplitude, onlUnit);

}

//
//____________________________________________________________________
float TileCondToolEmscale::doCalibEmsOnl(unsigned int drawerIdx, unsigned int channel, float amplitude) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->applyOnlineEMScaleCalibration(drawerIdx, channel, amplitude);

}

//
//____________________________________________________________________
float TileCondToolEmscale::doCalibCesOnl(unsigned int drawerIdx, unsigned int channel, float amplitude, bool applyLasCorr) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->applyOnlineCesiumCalibration(drawerIdx, channel, amplitude, applyLasCorr);

}

//
//____________________________________________________________________
float TileCondToolEmscale::doCalibLasOnl(unsigned int drawerIdx, unsigned int channel, float amplitude) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->applyOnlineLaserCalibration(drawerIdx, channel, amplitude);

}

//
//____________________________________________________________________
float TileCondToolEmscale::doCalibCisOnl(unsigned int drawerIdx, unsigned int channel, unsigned int adc, float amplitude) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->applyOnlineChargeCalibration(drawerIdx, channel, adc, amplitude);

}

//
//____________________________________________________________________
float TileCondToolEmscale::getCesRefLas(unsigned int drawerIdx, unsigned int channel, unsigned int adc) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->getCesiumReferenceLaserGain(drawerIdx, channel, adc);

}

//
//____________________________________________________________________
float TileCondToolEmscale::getCesRefHv(unsigned int drawerIdx, unsigned int channel) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->getCesiumReferenceHV(drawerIdx, channel);

}

//
//____________________________________________________________________
float TileCondToolEmscale::getCesRefTemp(unsigned int drawerIdx, unsigned int channel) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->getCesiumReferenceTemperature(drawerIdx, channel);

}

//
//____________________________________________________________________
float TileCondToolEmscale::getLasFiber(unsigned int drawerIdx, unsigned int channel) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->getLaserFiberVariation(drawerIdx, channel);

}

//
//____________________________________________________________________
float TileCondToolEmscale::getLasPartition(unsigned int drawerIdx) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->getLaserPartitionVariation(drawerIdx);

}

//
//____________________________________________________________________
float TileCondToolEmscale::getLasRefHv(unsigned int drawerIdx, unsigned int channel) const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->getLaserReferenceHV(drawerIdx, channel);

}


//
//____________________________________________________________________
TileRawChannelUnit::UNIT TileCondToolEmscale::getOnlCacheUnit() const {

  SG::ReadCondHandle<TileEMScale> emScale(m_emScaleKey);
  return emScale->getOnlineCacheUnit();

}
