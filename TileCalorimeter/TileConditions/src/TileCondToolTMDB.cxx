/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/


// Tile includes
#include "TileConditions/TileCondToolTMDB.h"
#include "TileCalibBlobObjs/TileCalibDrawerFlt.h"
#include "TileCalibBlobObjs/TileCalibUtils.h"

// Athena includes
#include "AthenaKernel/errorcheck.h"
#include "StoreGate/ReadCondHandle.h"
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/


#include "TileConditions/TileCondToolTMDB.h"

// Tile includes
#include "TileCalibBlobObjs/TileCalibDrawerFlt.h"
#include "TileCalibBlobObjs/TileCalibUtils.h"
#include "TileCalibBlobObjs/Exception.h"

// Athena includes
#include "AthenaKernel/errorcheck.h"

>>>>>>> release/21.0.127


//
//____________________________________________________________________
TileCondToolTMDB::TileCondToolTMDB(const std::string& type, const std::string& name, const IInterface* parent)
  : AthAlgTool(type, name, parent)
<<<<<<< HEAD
{
  declareInterface<ITileCondToolTMDB>(this);
  declareInterface<TileCondToolTMDB>(this);
=======
  , m_pryThreshold("TileCondProxyFile_TileCalibDrawerFlt_/TileCondProxyDefault_Threshold", this)
  , m_pryDelay("TileCondProxyFile_TileCalibDrawerFlt_/TileCondProxyDefault_Delay", this)
  , m_pryTMF("TileCondProxyFile_TileCalibDrawerFlt_/TileCondProxyDefault_TMF", this)
  , m_pryCalib("TileCondProxyFile_TileCalibDrawerFlt_/TileCondProxyDefault_Calib", this)
{
  declareInterface<ITileCondToolTMDB>(this);
  declareInterface<TileCondToolTMDB>(this);
  declareProperty("ProxyThreshold", m_pryThreshold);
  declareProperty("ProxyDelay", m_pryDelay);
  declareProperty("ProxyTMF", m_pryTMF);
  declareProperty("ProxyCalib", m_pryCalib);
>>>>>>> release/21.0.127
}

//
//____________________________________________________________________
TileCondToolTMDB::~TileCondToolTMDB() {
}

//
//____________________________________________________________________
StatusCode TileCondToolTMDB::initialize() {

  ATH_MSG_DEBUG( "In initialize()" );

  //=== retrieve proxy
<<<<<<< HEAD
  ATH_CHECK( m_calibThresholdKey.initialize() );
  ATH_CHECK( m_calibDelayKey.initialize() );
  ATH_CHECK( m_calibTmfKey.initialize() );
  ATH_CHECK( m_calibDataKey.initialize() );
=======
  CHECK( m_pryThreshold.retrieve() );
  CHECK( m_pryDelay.retrieve() );
  CHECK( m_pryTMF.retrieve() );
  CHECK( m_pryCalib.retrieve() );
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}

//
//____________________________________________________________________
StatusCode TileCondToolTMDB::finalize() {
<<<<<<< HEAD

=======
>>>>>>> release/21.0.127
  ATH_MSG_DEBUG( "finalize called" );

  return StatusCode::SUCCESS;
}


//
//____________________________________________________________________
<<<<<<< HEAD
float TileCondToolTMDB::getThreshold(unsigned int drawerIdx, unsigned int threshold) const {

  SG::ReadCondHandle<TileCalibData<TileCalibDrawerFlt>> calibThreshold(m_calibThresholdKey);
  return calibThreshold->getCalibDrawer(drawerIdx)->getData(threshold, 0, 0);

=======
float TileCondToolTMDB::getThreshold(unsigned int drawerIdx, TMDB::THRESHOLD threshold) const {

  if (drawerIdx >= TileCalibUtils::MAX_DRAWERIDX) {
    throw TileCalib::IndexOutOfRange("TileCondToolTMDB::getThreshold", drawerIdx, TileCalibUtils::MAX_DRAWERIDX);
  }

  return m_pryThreshold->getCalibDrawer(drawerIdx)->getData(threshold, 0, 0);
>>>>>>> release/21.0.127
}


//
//____________________________________________________________________
<<<<<<< HEAD
float TileCondToolTMDB::getDelay(unsigned int drawerIdx, unsigned int channel) const {

  SG::ReadCondHandle<TileCalibData<TileCalibDrawerFlt>> calibDelay(m_calibDelayKey);
  return calibDelay->getCalibDrawer(drawerIdx)->getData(channel, 0, 0);

=======
float TileCondToolTMDB::getDelay(unsigned int drawerIdx, TMDB::CHANNEL channel) const {

  if (drawerIdx >= TileCalibUtils::MAX_DRAWERIDX) {
    throw TileCalib::IndexOutOfRange("TileCondToolTMDB::getDelay", drawerIdx, TileCalibUtils::MAX_DRAWERIDX);
  }

  return m_pryDelay->getCalibDrawer(drawerIdx)->getData(channel, 0, 0);
>>>>>>> release/21.0.127
}

//
//____________________________________________________________________
<<<<<<< HEAD
void TileCondToolTMDB::getCalib(unsigned int drawerIdx, unsigned int channel, float& a, float& b) const {

  SG::ReadCondHandle<TileCalibData<TileCalibDrawerFlt>> calibData(m_calibDataKey);
  a = calibData->getCalibDrawer(drawerIdx)->getData(channel, 0, 0);
  b = calibData->getCalibDrawer(drawerIdx)->getData(channel, 0, 1);

=======
void TileCondToolTMDB::getCalib(unsigned int drawerIdx, TMDB::CHANNEL channel, float& a, float& b) const {

  if (drawerIdx >= TileCalibUtils::MAX_DRAWERIDX) {
    throw TileCalib::IndexOutOfRange("TileCondToolTMDB::getCalib", drawerIdx, TileCalibUtils::MAX_DRAWERIDX);
  }

  a = m_pryCalib->getCalibDrawer(drawerIdx)->getData(channel, 0, 0);
  b = m_pryCalib->getCalibDrawer(drawerIdx)->getData(channel, 0, 1);
>>>>>>> release/21.0.127
}


//
//____________________________________________________________________
<<<<<<< HEAD
unsigned int TileCondToolTMDB::getWeights(unsigned int drawerIdx, unsigned int channel, TMDB::Weights& weights) const {

  SG::ReadCondHandle<TileCalibData<TileCalibDrawerFlt>> calibTMF(m_calibTmfKey);
  unsigned int nWeights = calibTMF->getCalibDrawer(drawerIdx)->getObjSizeUint32();

  if (weights.size() < nWeights ) {
    ATH_MSG_ERROR("Not enough space in output array to put all weights from DB (nothing to be done!): "
=======
unsigned int TileCondToolTMDB::getWeights(unsigned int drawerIdx, TMDB::CHANNEL channel, TMDB::Weights& weights) const {

  if (drawerIdx >= TileCalibUtils::MAX_DRAWERIDX) {
    throw TileCalib::IndexOutOfRange("TileCondToolTMDB::getWeights", drawerIdx, TileCalibUtils::MAX_DRAWERIDX);
  }

  unsigned int nWeights = m_pryTMF->getCalibDrawer(drawerIdx)->getObjSizeUint32();

  if (weights.size() < nWeights ) {
    ATH_MSG_ERROR("Not enough space in output array to put all weights from DB (nothing to be done!): " 
>>>>>>> release/21.0.127
                  << weights.size() << " < " << nWeights);
    return 0;
  }

  for (unsigned int iWeight = 0; iWeight < nWeights; ++iWeight) {
<<<<<<< HEAD
    weights[iWeight] = calibTMF->getCalibDrawer(drawerIdx)->getData(channel, 0, iWeight);
  }
=======
    weights[iWeight] = m_pryTMF->getCalibDrawer(drawerIdx)->getData(channel, 0, iWeight);
  }
  
>>>>>>> release/21.0.127

  return nWeights;
}




//
//____________________________________________________________________
<<<<<<< HEAD
float TileCondToolTMDB::channelCalib(unsigned int drawerIdx, unsigned int channel, float amplitude) const {

  SG::ReadCondHandle<TileCalibData<TileCalibDrawerFlt>> calibData(m_calibDataKey);
  return amplitude * calibData->getCalibDrawer(drawerIdx)->getData(channel, 0, 0)
    + calibData->getCalibDrawer(drawerIdx)->getData(channel, 0, 1);

=======
float TileCondToolTMDB::channelCalib(unsigned int drawerIdx, TMDB::CHANNEL channel, float amplitude) const {

  if (drawerIdx >= TileCalibUtils::MAX_DRAWERIDX) {
    throw TileCalib::IndexOutOfRange("TileCondToolTMDB::channelCalib", drawerIdx, TileCalibUtils::MAX_DRAWERIDX);
  }

  return m_pryCalib->getCalibDrawer(drawerIdx)->getData(channel, 0, 0) 
    + amplitude * m_pryCalib->getCalibDrawer(drawerIdx)->getData(channel, 0, 1);
>>>>>>> release/21.0.127
}


//
//____________________________________________________________________
<<<<<<< HEAD
float TileCondToolTMDB::channelCalib(unsigned int drawerIdx, unsigned int channel, const std::vector<float>& samples) const {

  SG::ReadCondHandle<TileCalibData<TileCalibDrawerFlt>> calibTMF(m_calibTmfKey);

  if (samples.size() != calibTMF->getCalibDrawer(drawerIdx)->getObjSizeUint32()) {
    ATH_MSG_ERROR("Number of samples and weights in DB are not compatible (nothing to be done!): "
                  << samples.size() << " =! " << calibTMF->getCalibDrawer(drawerIdx)->getObjSizeUint32());
=======
float TileCondToolTMDB::channelCalib(unsigned int drawerIdx, TMDB::CHANNEL channel, const std::vector<float>& samples) const {

  if (drawerIdx >= TileCalibUtils::MAX_DRAWERIDX) {
    throw TileCalib::IndexOutOfRange("TileCondToolTMDB::channelCalib", drawerIdx, TileCalibUtils::MAX_DRAWERIDX);
  }

  if (samples.size() != m_pryTMF->getCalibDrawer(drawerIdx)->getObjSizeUint32()) {
    ATH_MSG_ERROR("Number of samples and weights in DB are not compatible (nothing to be done!): " 
                  << samples.size() << " =! " << m_pryTMF->getCalibDrawer(drawerIdx)->getObjSizeUint32());
>>>>>>> release/21.0.127
    return 0;
  }

  float amplitude(0.0F);
  unsigned int iWeight(0);
  for (float sample : samples) {
<<<<<<< HEAD
    amplitude += sample * calibTMF->getCalibDrawer(drawerIdx)->getData(channel, 0, iWeight);
=======
    amplitude += sample * m_pryTMF->getCalibDrawer(drawerIdx)->getData(channel, 0, iWeight);
>>>>>>> release/21.0.127
    ++iWeight;
  }

  return channelCalib(drawerIdx, channel, amplitude);
}
<<<<<<< HEAD
=======



>>>>>>> release/21.0.127
