# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

import logging; log = logging.getLogger("DCSCalculator2.lib")

from DQUtils.iov_arrangement import flatten_channels
#from DQUtils.sugar import RunLumi, IOVSet

from .subdetector import DCSC_Subdetector, DCSC_DefectTranslate_Subdetector, DCSC_Subdetector_DefectsOnly

from .variable import (DCSC_Variable, DCSC_Variable_With_Mapping,
                       DCSC_Global_Variable, DCSC_Defect_Global_Variable)

from .subdetector import GoodIOV, OUT_OF_CONFIG

from .libcore import (make_multi_mapping,
                      map_channels, connect_adjacent_iovs_defect)

<<<<<<< HEAD
__all__ = ['flatten_channels',
           'DCSC_Subdetector', 'DCSC_DefectTranslate_Subdetector',
           'DCSC_Subdetector_DefectsOnly', 'DCSC_Variable',
           'DCSC_Variable_With_Mapping', 'DCSC_Global_Variable',
           'DCSC_Defect_Global_Variable',
           'GoodIOV', 'OUT_OF_CONFIG',
           'make_multi_mapping', 'map_channels',
           'connect_adjacent_iovs_defect']
=======
    return IOVSet(iovs)

def connect_adjacent_iovs_defect(generator):
    previous = None
    for iov in generator:
        if (previous and previous.connected_to(iov) and
            previous.comment==iov.comment and previous.channel==iov.channel and
            previous.present==iov.present):
            previous = previous._replace(until=iov.until)
        else:
            if previous:
                yield previous
            previous = iov
    if previous:
        yield previous
    
from subdetector import DCSC_Subdetector, DCSC_DefectTranslate_Subdetector, DCSC_Subdetector_DefectsOnly

from variable import (DCSC_Variable, DCSC_Variable_With_Mapping,
                      DCSC_Global_Variable, DCSC_Defect_Global_Variable)

from subdetector import GoodIOV, OUT_OF_CONFIG
>>>>>>> release/21.0.127
