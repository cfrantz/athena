/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/**
 * @class DQTBackgroundMon
 * @author C. D. Burton <burton@utexas.edu>
 * @date 2019/07/31
 * @brief Monitor background processes in the detector.
 */

#ifndef DQTBACKGROUNDMON_H
#define DQTBACKGROUNDMON_H

#include "AthenaMonitoring/AthMonitorAlgorithm.h"

#include "TagEvent/RawInfoSummaryForTag.h"
#include "LArRecEvent/LArCollisionTime.h"
#include "TileEvent/MBTSCollisionTime.h"
#include "TileEvent/TileContainer.h"
#include "LUCID_RawEvent/LUCID_RawDataContainer.h"
#include "RecBackgroundEvent/BeamBackgroundData.h"

class DQTBackgroundMon : public AthMonitorAlgorithm {
public:
    DQTBackgroundMon(const std::string& name, ISvcLocator* pSvcLocator);
    virtual ~DQTBackgroundMon();
    virtual StatusCode initialize() override;
    virtual StatusCode fillHistograms( const EventContext& ctx ) const override;
private:
<<<<<<< HEAD
    template <class T> using RH = SG::ReadHandle<T>;
    template <class T> using RHK = SG::ReadHandleKey<T>;
    template <class T> using GP = Gaudi::Property<T>;

	// Activates trigger and muon parts of the algorithm
    GP<bool> m_doMuons {this, "Muons", true};

    // StoreGate keys
    RHK<RawInfoSummaryForTag> m_RawInfoSummaryForTagKey{this, "RawInfoSummaryForTagKey", "RawInfoSummaryForTag", ""};
    RHK<LArCollisionTime> m_LArCollisionTimeKey {this, "LArCollisionTimeKey", "LArCollisionTime", ""};
    RHK<MBTSCollisionTime> m_MBTSCollisionTimeKey {this, "MBTSCollisionTimeKey", "MBTSCollisionTime", ""};
    RHK<TileCellContainer> m_TileCellContainerKey {this, "MBTSContainerName", "MBTSContainer", ""};
    RHK<LUCID_RawDataContainer> m_LUCID_RawDataContainerKey {this, "Lucid_RawDataKey","Lucid_RawData", ""};
    RHK<Trk::SegmentCollection> m_SegmentCollectionKey {this, "MuonSegmentsKey", "MuonSegments", ""};
    RHK<BeamBackgroundData> m_BeamBackgroundDataKey {this, "BeamBackgroundDataKey", "BeamBackgroundData", ""};
    RHK<xAOD::VertexContainer> m_VertexContainerKey {this, "PrimaryVerticesKey", "PrimaryVertices", ""};
    SG::ReadDecorHandleKey<xAOD::EventInfo> m_eventInfoDecorKey{this,"eventInfoDecorKey",
                                                               "EventInfo.backgroundWord",
                                                               "Key to enforce scheduling"};
    // we dp not need a decorhandle key for the MBTS background because we already depend on MBTSCollisionTime


    // For parsing the int returned by TrigDecTool's getBGCode()
    GP<int> m_filledBG {this, "FilledBGIndex", 1};
    GP<int> m_emptyBG {this, "EmptyBGIndex", 3};
    GP<int> m_unpairIsoBG {this, "UnpairIsoBGIndex", 4};
    GP<int> m_unpairNonIsoBG {this, "UnpairNonIsoBGIndex", 5};

    // Various cuts used in fillHistograms()
    GP<int> m_upPixSP {this, "UpPixSP", 5000};
    GP<int> m_upSctSP {this, "UpSctSP", 10000};
    GP<int> m_LArECSideCut {this, "LArECSideCut", 1};
    GP<int> m_MBTSSideCut {this, "MBTSSideCut", 2};
    GP<float> m_MBTSTimeCut {this, "MBTSTimeCut", 15};
    GP<float> m_MBTSThresholdCut {this, "MBTSThresholdCut", 40.0/222.0};
    GP<int> m_MBTSMask {this, "MBTSMask", TileCell::MASK_BADCH | TileCell::MASK_OVER | TileCell::MASK_TIME };
    GP<int> m_MBTSPattern {this, "MBTSPattern", TileCell::MASK_TIME };
    GP<int> m_nBkgWords {this, "NBkgWords", 32};
=======

  ToolHandle<Trig::TrigDecisionTool> m_trigDec;
  ToolHandle<Muon::MuonEDMHelperTool> m_helperTool;
  ToolHandle<Muon::MuonIdHelperTool>  m_idHelperTool;
  ToolHandle<MuonCalib::IIdToFixedIdTool> m_idToFixedIdTool;

  bool m_doMuons;
  bool m_doTrigger;
  int m_FilledBG;
  int m_EmptyBG;
  int m_UnpairIsoBG;
  int m_UnpairNonIsoBG;

  unsigned int m_bg;
  bool m_filled;
  bool m_empty;
  bool m_unpairiso;
  bool m_unpairnoniso;

  // All BG hists
  TH1F_LW *m_HistBitSet;
  TH1F_LW *m_HistBitSet_Filled;
  TH1F_LW *m_HistBitSet_Empty;
  TH1F_LW *m_HistBitSet_UnpairIso;
  TH1F_LW *m_HistBitSet_UnpairNonIso;

  TH1F_LW *m_HistPixSP;
  TH1F_LW *m_HistPixSP_UnpairIso;
  TH1F_LW *m_HistPixSP_UnpairNonIso;
  TH1F_LW *m_HistPixSPHuge;
  TH1F_LW *m_HistPixSPHuge_UnpairIso;
  TH1F_LW *m_HistPixSPHuge_UnpairNonIso;

  TH1F_LW *m_HistSctSP;
  TH1F_LW *m_HistSctSP_UnpairIso;
  TH1F_LW *m_HistSctSP_UnpairNonIso;
  TH1F_LW *m_HistSctSPHuge;
  TH1F_LW *m_HistSctSPHuge_UnpairIso;
  TH1F_LW *m_HistSctSPHuge_UnpairNonIso;

  TH1F_LW *m_HistLArTimeDiff;

  TH1F_LW *m_HistMBTSTimeDiff;
  TH1F_LW *m_HistMBTSVetoHits;

  TH1F_LW *m_HistLucidHits;

  TH1F_LW *m_HistThetaCsc;
  TH1F_LW *m_HistThetaCscA;
  TH1F_LW *m_HistThetaCscC;
  TH1F_LW *m_HistThetaMdt;
  TH1F_LW *m_HistTimeSeg0Csc;
  TH1F_LW *m_HistTimeSeg0Mdt;
  TH2F_LW *m_HistXYSeg0;
  TH1F_LW *m_HistClusterEnergy;
  TH2F_LW *m_HistClusterEtaPhi;
  TH2F_LW *m_HistClusterEtaTime;
  TH1F_LW *m_HistFakeJetIndex;
  TH1F_LW *m_HistFakeJet1Pt;
  TH1F_LW *m_HistFakeJet1Eta;
  TH1F_LW *m_HistFakeJet1Phi;
  TH1F_LW *m_HistFakeJet1Time;
  TH1F_LW *m_HistFakeJet1Chf;
  TH2F_LW *m_HistFakeJet1EtaTime;
  TH2F_LW *m_HistFakeJet1EmfChf;
  TH1F_LW *m_HistRateBcidACTwoSided;
  TH1F_LW *m_HistRateBcidCATwoSided;
  TH1F_LW *m_HistRateBcidACTwoSidedOneSided;
  TH1F_LW *m_HistRateBcidCATwoSidedOneSided;

  TH1F_LW *m_HistBadLooserJet1Pt;
  TH1F_LW *m_HistBadLooserJet1Eta;
  TH1F_LW *m_HistBadLooserJet1Phi;
  TH1F_LW *m_HistBadLooserJet1Time;
  TH1F_LW *m_HistBadLooserJet1Chf;
  TH2F_LW *m_HistBadLooserJet1EtaTime;
  TH2F_LW *m_HistBadLooserJet1EmfChf;

  TH1F_LW *m_HistNumVertex;
  TH1F_LW *m_HistNumVertex_UnpairIso;
  TH1F_LW *m_HistNumVertex_UnpairNonIso;
  TH1F_LW *m_HistMetLocHadTopo;

  //cut variables
  int m_LArEC_SideCut;
  int m_MBTS_SideCut;
  int m_MBTS_TimeCut;
  float m_MBTS_ThresholdCut;
  std::string m_mbtsContainerName;
  const uint8_t m_MBTS_mask, m_MBTS_pattern;

  //hist config
  int m_nBins_PixSP;
  int m_nBins_SctSP;
  int m_nBins_SPHuge;
  int m_nBins_TimeDiff;

  float m_Up_PixSP;
  float m_Up_SctSP;
  float m_Up_SPHuge;
  float m_Low_TimeDiff;
  float m_Up_TimeDiff;
  int m_nBkgWords;

>>>>>>> release/21.0.127
};
#endif
