//
<<<<<<< HEAD
//   @file    hanconfig.cxx         
=======
//   @file    hcg.cxx
>>>>>>> release/21.0.127
//            navigates through the directory structure of a monitoring 
//            root files and write out some han config boiler plate
//            
//   @author M.Sutton
// 
<<<<<<< HEAD
//      
//
//   $Id: hanconfig.cxx, v0.0   Thu  12 March 2015 14:13:47 CEST sutt $
=======
//   Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
//   Copyright (C) 2013 M.Sutton (sutt@cern.ch)    
//
//   $Id: hcg.cxx  Sat 13 May 2017 15:07:09 CEST sutt$
>>>>>>> release/21.0.127


#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <set>
<<<<<<< HEAD
=======
#include <regex>
>>>>>>> release/21.0.127

#include <cstdlib>
#include <cstdio>

#include <fstream>

#include <sys/stat.h>

#include "simpletimer.h"
#include "node.h"
#include "addnode.h"
#include "spacer.h"

#include "TStyle.h"
#include "TCanvas.h"
#include "TKey.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TFile.h"
#include "TClass.h"
#include "TDirectory.h"

#include "TPad.h"
#include "TStyle.h"



/// file names and file pointers
std::vector<std::string> files;
std::vector<TFile*>      fptr;

std::vector<std::string> savedhistos;
std::vector<std::string> mapped;


<<<<<<< HEAD
/// sadly, includes a return at the end 
std::string date() { 
  time_t t;
  time(&t);
  std::string a = ctime(&t);
=======
/// get the date *without* the return
std::string date() { 
  time_t _t;
  time(&_t);
  std::string a = ctime(&_t);
>>>>>>> release/21.0.127
  std::string b = "";
  for ( unsigned i=0 ; i<a.size()-1 ; i++ ) b+=a[i];
  return b;
}


bool file_exists( const std::string& file ) { 
  struct stat buff;
  return stat(file.c_str(),&buff)==0;   
}

bool verbose = false; 

/// send output to here ...
std::ostream* outp = &std::cout;


bool allhists = true;

std::string base     = "HLT";

std::string outref = "";

std::string algorithm = "HLT_Histogram_Not_Empty&GatherData";

/// glabal timer - how long have I taken so far?
//  struct timeval global_timer;


std::string description = "https://twiki.cern.ch/twiki/bin/view/Atlas/HltTrackingDataQualityMonitoring#Tier0";


/// list of directories to be explicitly remapped
std::map<std::string, std::string> remap;

/// list of directories to be excluded
std::set<std::string> exclude; 

/// list of directories to be explicitly included, together with 
/// corresponding depths of subdirectories
std::map<std::string, int> dirs;


std::vector<std::string> tags;

template<typename T>
std::ostream& operator<<( std::ostream& s, const std::vector<T>& v ) {
  if ( v.empty() ) return s;
  for ( size_t i=0 ; i<v.size() ; i++ ) s << v[i] << "\n";
  return s;
}

bool contains( const std::string& s, const std::string& regx ) { return s.find(regx)!=std::string::npos; } 



template<typename T>
std::ostream& operator<<( std::ostream& s, const std::vector<T*>& v ) {
  if ( v.empty() ) return s;
  for ( int i=0 ; i<v.size() ; i++ ) s << *v[i] << "\n";
  return s;
}



/// get a TObject* from a TKey* 
/// (why can't a TObject be a TKey?)
template<class T>
T* get( TKey* tobj ) { 
  TObject* a = tobj->ReadObj()->Clone();
  ((TH1*)a)->SetDirectory(0);
  return (T*)a;
}


/// return a remapped string
std::string find( const std::string& s ) { 
  std::map<std::string, std::string>::const_iterator itr = remap.find(s);
  if ( itr!=remap.end() ) return itr->second;
  else                    return s;
} 


/// count how many occurances of a regx are in a string 
int count( std::string s, const std::string& regx ) {
  size_t p = s.find( regx );
  int i=0;
  while ( p!=std::string::npos ) {
    i++;
    s.erase( 0, p+1 );
    p = s.find( regx );
  } 
  return i;
}




// chop tokens off the front of a string
std::string chop(std::string& s1, const std::string& s2)
{
  std::string::size_type pos = s1.find(s2);
  std::string s3;
  if ( pos == std::string::npos ) {
    s3 = s1;
    s1.erase(0, s1.size());
  }
  else {
    s3 = s1.substr(0, pos); 
    s1.erase(0, pos+s2.size());
  }
  return s3;
} 


std::vector<std::string> split( const std::string& s, const std::string& t=":"  ) {
    
<<<<<<< HEAD
    std::string s2 = s;
    size_t pos = s2.find(t);
=======
    std::string _s = s;
    size_t pos = _s.find(t);
>>>>>>> release/21.0.127
    
    std::vector<std::string> tags;
    
    while ( pos!=std::string::npos ) { 
<<<<<<< HEAD
      tags.push_back( chop(s2,t) );
      pos = s2.find(t);
    }
    
    tags.push_back(s2);
=======
      tags.push_back( chop(_s,t) );
      pos = _s.find(t);
    }
    
    tags.push_back(_s);
>>>>>>> release/21.0.127
    
    return tags;
}


// chop tokens off the front of a string but not including 
// chop character
std::string chopex(std::string& s1, const std::string& s2)
{
  std::string::size_type pos = s1.find(s2);
  std::string s3;
  if ( pos == std::string::npos ) {
    s3 = s1;
    s1.erase(0, s1.size());
  }
  else {
    s3 = s1.substr(0, pos); 
    s1.erase(0, pos+s2.size());
  }
  return s3;
} 


// chomp them off the end
std::string chomp(std::string& s1, const std::string& s2)
{
  std::string::size_type pos = s1.find(s2);
  std::string s3;
  if ( pos == std::string::npos ) {
    s3 = s1;
    s1.erase(0,s1.size());    
  }
  else {
    s3 = s1.substr(pos+s2.size(),s1.size());
    s1.erase(pos,s1.size()); 
  } 
  return s3;
} 



// chop tokens off the end of a string, leave string unchanged
// return choped string
std::string choptoken(std::string& s1, const std::string& s2)
{
  std::string s3 = "";
  std::string::size_type pos = s1.find(s2);
  if ( pos != std::string::npos ) {
    s3 = s1.substr(0, pos+s2.size()); 
    s1.erase(0, pos+s2.size());
  }
  return s3;
} 


// chop tokens off the end of a string, leave string unchanged
// return choped string
std::string chomptoken(std::string& s1, const std::string& s2)
{
  std::string s3 = "";
  std::string::size_type pos = s1.find(s2);
  if ( pos != std::string::npos ) {
    s3 = s1.substr(pos, s1.size());
    s1.erase(pos, s1.size()); 
  }
  return s3;
} 


// chop tokens off the front of a string
std::string chopfirst(std::string& s1, const std::string& s2)
{
  std::string s3;
  std::string::size_type pos = s1.find_first_not_of(s2);
  if ( pos != std::string::npos ) {
    s3 = s1.substr(0, pos); 
    s1.erase(0, pos);
  }
  else {
    s3 = s1;
    s1 = "";
  } 
  return s3;
} 


std::string choplast(std::string& s1, const std::string& s2)
{
  std::string s3 = "";
  std::string::size_type pos = s1.find_last_not_of(s2);
  if ( pos != std::string::npos ) {
    s3 = s1.substr(pos+1, s1.size());
    s1.erase(pos+1, s1.size());
  }
  return s3;
} 



// chop tokens off the front and end of a string
std::string chopends(std::string& s1, const std::string& s2)
{
  chopfirst(s1, s2);
  choplast(s1, s2);
  return s1;
} 



// remove strings from a string
void removespace(std::string& s, const std::string& s2) 
{
  std::string::size_type pos;
  while ( (pos = s.find(s2))!=std::string::npos ) {
    s.erase(pos, s2.size());
  }
} 


// replace from a string
std::string replace( std::string s, const std::string& s2, const std::string& s3) {
  std::string::size_type pos;
  //  while ( (pos = s.find(" "))!=std::string::npos ) {
  //    s.replace(pos, 1, "-");
  while ( (pos = s.find(s2))!=std::string::npos ) {
    s.replace(pos, s2.size(), s3);
    if ( contains(s3,s2) ) break;
  }
  return s;
} 


// remove regx from a string
<<<<<<< HEAD
void depunctuate(std::string& s, const std::string& regex=":") 
{
  std::string::size_type pos;
  while ( (pos = s.find(regex))!=std::string::npos ) {
    s.erase(pos, regex.size());
=======
void depunctuate(std::string& s, const std::string& regx=":") 
{
  std::string::size_type pos;
  while ( (pos = s.find(regx))!=std::string::npos ) {
    s.erase(pos, regx.size());
>>>>>>> release/21.0.127
  }
} 
  


<<<<<<< HEAD
=======

std::string chopto( std::string& s, const std::string& pattern ) { 
  std::string tag;
  std::string::size_type pos = s.find_first_of( pattern );
  if ( pos==std::string::npos ) { 
    tag = s;
    s = "";
  }
  else { 
    tag = s.substr(0,pos);
    s.erase( 0, pos ); 
  }
  return tag;
}


// remove strings from a string
bool remove( std::string& s, const std::string& s2 ) 
{
  std::string::size_type ssize = s.size();
  std::string::size_type pos;
  while ( (pos=s.find(s2))==0 ) s.erase(pos, s2.size());
  if ( ssize!=s.size() ) return true;
  else                   return false;
} 


class histogram : public std::string { 
  
public:

  histogram( std::string s="" ) : std::string(s) { construct( s ); }
  histogram( const char* s    ) : std::string(s) { construct( std::string(s) ); }
  
  std::vector<std::string>&       dirs()       { return mdirs; }
  const std::vector<std::string>& dirs() const { return mdirs; }

private:
  
  void construct( std::string s ) { 
    std::string::size_type pos = s.find("/");
    while ( pos!=std::string::npos ) { 
      std::string s0 = chop( s, "/" );
      mdirs.push_back(s0);
      pos = s.find("/");
    } 
    mdirs.push_back(s);
  } 

protected:

  std::vector<std::string> mdirs;

};



/// simple error reporting class - should probably throw an exception, 
/// only this is simpler
void error( int i, std::ostream& s ) {  s << std::endl; std::exit(i);  }


/// map instances to allow setting of the algorithms 
/// and descriptions

class hmap_t : public std::map<histogram,std::string> { 

public:

  /// sadly, when matching regular expressions, we need to iterate 
  /// through the map and cannot use the standard map::find() methods
  /// which sadly, slows the searching down

  virtual std::map<histogram,std::string>::iterator find( const std::string& s ) {
    return std::map<histogram,std::string>::find( s );
  }

  virtual std::map<histogram,std::string>::const_iterator find( const std::string& s ) const { 
    return std::map<histogram,std::string>::find( s );
  }

  virtual std::map<histogram,std::string>::iterator match( const std::string& s ) {
    std::map<histogram,std::string>::iterator  itr = begin();
    while( itr!=end() ) { 
      if ( std::regex_match( s, std::regex(itr->first) ) ) return itr;
      itr++;
    }
    return itr;
  }

  virtual std::map<histogram,std::string>::const_iterator match( const std::string& s ) const { 
    std::map<histogram,std::string>::const_iterator  itr = begin();
    while( itr!=end() ) { 
      if ( std::regex_match( s, std::regex(itr->first) ) ) return itr;
      itr++;
    }
    return itr;
  }
  
};


/// store any user histogram to algorithm mapping
hmap_t algorithms;

/// store any user histogram to description mapping
hmap_t descriptions;

/// store any user histogram to display mapping
hmap_t displays;


/// store the list of any directories to be wildcarded and 
/// the algorithms to use for them - use "hist=NULL"; if no 
/// algorithm is required
hmap_t  wildcards;



/// look in a histogram name map and return the regex mapped property name
/// goes up the tree as far as to 2 subdirectory names of additional 
/// specialisation for the histogram names  
/// NB: take care, this is a recursive algorithm !!!
 
std::string match( const hmap_t& m, const node* n, const std::string& obj, const std::string& property ) {    
  /// don't do anything is the map is empty
  if ( m.size() && n )  { 
    hmap_t::const_iterator itr = m.match( obj );
    if ( itr!=m.end() ) return itr->second;
    else if ( n->parent() ) return match( m, n->parent(), n->parent()->name()+"/"+obj, property );
  }
  return property;
}



bool find( const std::string& s, const std::vector<std::string>& regexv ) { 
  for ( unsigned i=regexv.size() ; i-- ; ) if ( std::regex_match( s, std::regex(regexv[i]) ) ) return true;
  return false;
} 




/// look in a histogram name map and return the mapped property name
/// goes up the tree as far as to 2 subdirectory names of additional 
/// specialisation for the histogram names  
/// NB: take care, this is a recursive algorithm !!!

std::string find_internal( const hmap_t& m, const node* n, const std::string& obj, const std::string& property ) {  
  /// don't do anything if the map is empty
  if ( m.size() && n ) { 
    hmap_t::const_iterator itr = m.find( obj );
    if ( itr!=m.end() ) return itr->second;
    else if ( n->parent() )  return find_internal( m, n->parent(), n->parent()->name()+"/", property ); 
  }
  return property;
}



/// look for an exact match first, if you don't find one, search for a regular 
/// expression match  

std::string find( const hmap_t& m, const node* n, const std::string& obj, const std::string& property ) {  
  std::string _property = find_internal( m, n, obj, "FAIL" );
  if ( _property != "FAIL" ) return _property;
  return match( m, n, obj, property );
}



/// parse and individual line - must have the syntax: tag = "value";  

bool parse( const std::string _line, histogram& tag, std::string& val, bool requirequotes=true ) {
  
  std::string line = _line;

  tag = "";
  val = "";

  remove( line, " " ); 
  if ( line.size()==0 ) return false;
  tag = chopto( line, " =" );
  remove( line, " " );  
  if ( !remove( line, "=" ) ) error( 1, std::cerr << "error : tag incorrectly specified\n\t" << _line ); 
  remove( line, " " ); 
  if ( requirequotes ) { 
    if ( !( line.size()>1 && (val += line[0])=="\"" ) ) error( 1, std::cerr << "error : incorrect value syntax - no opening quote\n\t" << _line );
    remove( line, "\"" );
    val += chopto( line, "\"" )+"\"";
    if ( !( line.size()>0 && line[0]=='"' ) )  error( 1, std::cerr << "error : incorrect value syntax - no closing quote\n\t" << _line ); 
    remove( line, "\"" );
  }
  else { 
    val += chopto( line, ";" );
  }
  remove( line, " " );
  if ( line.size()<1 || line[0]!=';' )  error( 1, std::cerr << "error : incorrect value syntax - line not correctly terminated\n\t" << _line );

  return true;
}




std::vector<std::string> read_lines( const std::string& filename ) { 

  std::fstream file( filename );
  
  std::vector<std::string> lines;
  
  std::string buffer;
  

  /// add some padding at the beginning and end, extraxt file 
  /// contents to a more easily managed string 
  char c;
  buffer = " ";
  while ( file.get(c) ) buffer += c;
  buffer += " ";

  //  std::cout << "buffer: " << buffer << std::endl;

  /// remove all comments and line breaks

  int quotecount = 0;

  bool terminated = true;

  for ( unsigned i=1 ; i<buffer.size() ; i++ ) {

    /// check we are not still inside a quote 
    if ( buffer[i]=='"' ) quotecount++;

    /// remove comments
    if ( quotecount%2==0 && buffer[i]=='/' && buffer[i+1]=='/' ) {
      for ( unsigned j=i+2; j<buffer.size() ; j++, i++ ) if ( buffer[j]==10 ) { i++; break; }
    }
    //    else if ( quotecount%2==1 || buffer[i]>31 ) { 
    else if ( buffer[i]>31 ) { 
      /// check whether we have an end of line or not
      if ( quotecount%2!=0 || buffer[i]!=';' ) { 
	if ( terminated ) lines.push_back("");
	terminated = false;
	lines.back() += buffer[i];
      }
      else { 
	lines.back() += ";";
	terminated = true;
      }
    }
  }

  return lines;
}


std::vector<std::string> parse_wc( const std::string& filename ) { 
  
  std::vector<std::string> _lines = read_lines( filename ); 

  std::vector<std::string> out;
  
  for ( unsigned i=0 ; i<_lines.size() ; i++ ) {
    std::string line = _lines[i];
    remove( line, " " );  
    std::string val = chopto( line, " ;" );
    if ( val.size() ) out.push_back( val );
  }
    
  return out;
}

hmap_t  parse( const std::string& filename, bool requirequotes=true ) {

  hmap_t       lookup;

  std::vector<std::string> lines = read_lines( filename );  

  /// now parse each line 
    
  for ( unsigned i=0 ; i<lines.size() ; i++ ) { 
    histogram     tag = "";
    std::string value = "";
    if ( parse( lines[i], tag, value, requirequotes ) ) lookup.insert( hmap_t::value_type( tag, value ) );
  }

  return lookup;
}




>>>>>>> release/21.0.127
std::vector<std::string> maphist( const std::vector<std::string>& v ) {   
  mapped.clear();
  for ( unsigned i=0 ; i<v.size() ; i++ ) { 
    if ( contains( v[i], "Expert" ) ){ 
      std::string tmp = v[i];
      std::string path = chop( tmp, "Expert/" );
      path += "Chains/";
      //      std::cerr << " " << v[i] << "\np:" << path << "\t-> " << tmp << std::endl;
      tmp = replace( tmp, "/", "__" );
      path += tmp;
      mapped.push_back( path );
      //     std::cerr << i << "\t" << mapped.back() << std::endl;
    }
    else { 
      mapped.push_back( v[i] );
    }
  }

  return mapped;
}





/// match the individual directories of two strings 
bool match( std::string s1, std::string s2 ) { 
  
  int i1 = count( s1, "/" );
  int i2 = count( s2, "/" );

  int i = ( i1<i2 ? i1 : i2 ); 

  //  std::cerr << "match s1 " << s1 << " " << s2 << std::endl;

  for ( i++ ; i-- ; ) { 
    size_t p1 = s1.find("/");
    size_t p2 = s2.find("/");

    std::string ss1 = s1.substr( 0, p1 );
    std::string ss2 = s2.substr( 0, p2 );
    
    s1.erase( 0, p1+1 );
    s2.erase( 0, p2+1 );

    //    std::cerr << i << "\tmatch :" << ss1 << ":" << ss2 << "::\t " << s1 << ":" << s2 << "\tresult " << (ss1!=ss2 ) << std::endl;
    
    if ( ss1!=ss2 ) return false;

  } 
  
  return true;

}


/// see whether this directory matches any of the directories we are 
/// explicitly being asked to match
 
bool matchdir( const std::string& s ) { 
  bool matched = false;
  // int idepth = count( s, "/" );
  std::map<std::string,int>::const_iterator itr = dirs.begin();
  while ( itr!=dirs.end() ) { 
    if ( match( s, itr->first) ) matched = true;
    //    std::cerr << "\tmatchdir :" << s << "::" << itr->first << ": " << itr->second << std::endl;
    if ( matched ) return true;
    itr++;
  }
  return false;
}

bool matchcwd( const std::string& s ) { 
  if ( dirs.size()==0 ) return true; 
  std::map<std::string,int>::const_iterator itr = dirs.begin();
  while ( itr!=dirs.end() ) { 
    if ( s.find(itr->first)!=std::string::npos ) return true;
    itr++;
  }
  return false;
}



std::string matchcwdstr( const std::string& s ) { 
  if ( dirs.size()==0 ) return ""; 
  std::map<std::string,int>::const_iterator itr = dirs.begin();
  while ( itr!=dirs.end() ) { 
    if ( s.find(itr->first)!=std::string::npos ) return itr->first;
    itr++;
  }
  return "";
}


std::map<std::string,int>::const_iterator matchcwditr( const std::string& s ) { 
  if ( dirs.size()==0 ) return dirs.end(); 
  std::map<std::string,int>::const_iterator itr = dirs.begin();
  while ( itr!=dirs.end() ) { 
    if ( s.find(itr->first)!=std::string::npos ) return itr;
    itr++;
  }
  return dirs.end();
}



class reference { 

public:

  reference( const std::string& n, const std::string& f ) : 
<<<<<<< HEAD
    m_name(n), m_file(f) { 
=======
    mname(n), mfile(f) { 
>>>>>>> release/21.0.127
    
    /// oh dear, find the run number from the specified file 

    std::cerr << "opening file " << f << std::endl; 

    TFile* r = TFile::Open(f.c_str());
    if ( r==0 ) { 
      std::cerr << "cannot open root file " << f << std::endl;
      std::exit(-1);
    }
    
    r->cd();

    TList* tl  = gDirectory->GetListOfKeys();
    
    /// go through sub directories
    
    for ( int i=0 ; i<tl->GetSize() ; i++ ) { 
      
      TKey* tobj = (TKey*)tl->At(i);
      
      if ( std::string(tobj->GetClassName()).find("TDirectory")!=std::string::npos ) { 
      //      (*outp) << ns << "Directory " << tobj->GetName() << std::endl;
      
	TDirectory* tnd = (TDirectory*)tobj->ReadObj();
	
	std::string dir = tnd->GetName();

	if ( contains( dir, "run_" ) ) {
	  dir.erase( 0, std::string( "run_").size() ); 
<<<<<<< HEAD
	  m_run = std::atoi( dir.c_str() );
=======
	  mrun = std::atoi( dir.c_str() );
>>>>>>> release/21.0.127
	  
	  break;
	}
       
      }
    }

    r->Close();
  } 


<<<<<<< HEAD
  reference( const reference& r ) : m_name(r.m_name), m_file(r.m_file), m_run(r.m_run) { } 

  
  std::string name() const { return m_name; }
  std::string file() const { return m_file; }

  int run() const { return m_run; }

private:

  std::string m_name;
  std::string m_file;

  int         m_run;
=======
  reference( const reference& r ) : mname(r.mname), mfile(r.mfile), mrun(r.mrun) { } 

  
  std::string name() const { return mname; }
  std::string file() const { return mfile; }

  int run() const { return mrun; }

private:

  std::string mname;
  std::string mfile;

  int         mrun;
>>>>>>> release/21.0.127

};



std::ostream& operator<<( std::ostream& s, const reference& r ) { 
  static bool first = true;
  if ( first ) { 
    s << "##########################\n";
    s << "# References\n";
    s << "##########################\n\n";
    first = false;
  }
  s << "reference "      << r.name() << " {\n";
  s << "\t\tfile = "     << r.file() << "\n";
  s << "\t\tpath = run_" << r.run()  << "\n";
  s << "\t\tname = same_name"        << "\n";
  s << "}\n\n";
  return s;
}



std::vector<reference> references;



class header { 

public:

<<<<<<< HEAD
  header( ) { 
=======
  header( const std::string& configname="", const std::string& slicename="" ) { 
>>>>>>> release/21.0.127

    std::string user = std::getenv("USER");

    (*outp) << "######################################################################\n";
<<<<<<< HEAD
    (*outp) << "# $Id: collisions_run.config " << date() << " " << user << " $\n";
=======
    if ( configname=="" )  (*outp) << "# file  collisions_run.config " << date() << " " << user << "\n";
    else                   (*outp) << "# file  " << configname << "  " << date() << " " << user << "\n";
>>>>>>> release/21.0.127
    (*outp) << "######################################################################\n";
    
    (*outp) << "\n";
    (*outp) << "#######################\n";
<<<<<<< HEAD
    (*outp) << "# HLTidtrk\n";
=======
    if ( slicename=="" ) (*outp) << "# HLTidtrk\n";
    else                 (*outp) << "# " << slicename << "\n";
>>>>>>> release/21.0.127
    (*outp) << "#######################\n";

    (*outp) << "\n\n";
  }

};


/// make the sidebar many part of the config

class menu { 

public: 
  
  menu( node& n ) { 

    (*outp) << "\n\n";
    (*outp) << "#######################\n";
    (*outp) << "# Output\n";
    (*outp) << "#######################\n\n\n";

    makemenu( n ); 

  }
 

  void makemenu( node& n, const std::string& space="", std::string path="", std::string rawpath="", bool found=false ) {
    
    bool print = false;
    
    if ( n.name()==base ) found = true;

    if ( n.type()==node::DIRECTORY ) print = found;
    if ( n.name()=="top_level" )     print = true;
    
    if ( n.size() ) { 
      
      /// always try to remap the name

      //      bool exclude_dir = false;

      if ( exclude.find(n.name())!=exclude.end() )  { 
	print = false;
	//	exclude_dir = true;
	return;
      }

      //      if ( found && ( dirs.size() && dirs.find(n.name())==dirs.end() ) ) print = false;
   
      std::string newspacer = space;
      
      if ( print ) newspacer += spacer;
      
      std::string output_name = find(n.name());
      
      
      if ( print && n.type()==node::DIRECTORY ) { 
	if ( path=="" ) path += output_name;
	else            path += "/" + output_name;
	if ( rawpath=="" ) rawpath += n.name();
	else               rawpath += "/" + n.name();
      }

      //      std::cerr << "path " << path << "\tmatchdir " << matchdir( path ) << std::endl;

      if ( found && ( dirs.size() && (!matchdir( path ) && !matchdir( rawpath ) ) ) ) return;

      if ( print ) (*outp) << space << "output " << output_name << " {" << "\n"; // " \t\t(" << path << " size " << n.size() << ")\n";

      for ( unsigned i=0 ; i<n.size() ; i++ ) { 
	if ( n[i]->type()!=node::HISTOGRAM ) makemenu( *n[i], newspacer, path, rawpath, found ) ;
      }

      if ( print ) (*outp) << space << "}\n";
    }    
    
  }

};




/// make the histogram assessment part of the config

class ass { 

public: 
  
<<<<<<< HEAD
  ass( node& n, bool ah=true ) : m_allhists(ah) { 
=======
  ass( node& n, bool ah=true ) : mallhists(ah) { 
>>>>>>> release/21.0.127
    (*outp) << "\n\n";
    (*outp) << "#######################\n";
    (*outp) << "# Histogram Assessments\n";
    (*outp) << "#######################\n\n";
    makeass( n ); 
  }
 

  void makeass( node& n, const std::string& space="", std::string path="", std::string rawpath="", bool found=false ) {
    
    static std::string savedir = "";

    bool print = false;
    
    if ( n.name()==base ) found = true;

    if ( n.type()==node::DIRECTORY ) print = found;
    ///   if ( n.name()=="top_level" )     print = true;
    
    if ( n.size() ) { 
      
      /// always try to remap the name
      
      std::string newspacer = space;
      
      if ( exclude.find(n.name())!=exclude.end() )  { 
	print = false;
	return;
      }

      //     if ( found && dirs.size() && dirs.find(n.name())==dirs.end() ) print = false;

      if ( print ) newspacer += spacer;
      
      std::string output_name = find(n.name());
            
      if ( print && n.type()==node::DIRECTORY ) { 
	if ( path=="" ) path += output_name;
	else            path += "/" + output_name;
	if ( rawpath=="" ) rawpath += n.name();
	else               rawpath += "/" + n.name();
      }

      if ( found && ( dirs.size() &&  !matchdir( path ) && !matchdir( rawpath ) ) ) return;

      if ( print ) (*outp) << space << "dir " << n.name() << " {" << "\n"; // " \t\t(" << path << ")\n";

      bool first_hists = true;

      for ( unsigned i=0 ; i<n.size() ; i++ ) { 
	if       ( n[i]->type()!=node::HISTOGRAM ) makeass( *n[i], newspacer, path, rawpath, found ) ;
	else if  ( n[i]->type()==node::HISTOGRAM ) { 
<<<<<<< HEAD
	  if ( !m_allhists ) { 
	    if ( first_hists ) {
	      (*outp) << space << "\t"   << "hist .* {\n";
	      (*outp) << space << "\t\t" << "regex       \t= 1\n";
	      (*outp) << space << "\t\t" << "algorithm   \t= " << algorithm << "\n";
=======

	  std::string allhists = "";
	  if ( n[i]->parent() ) allhists = find( wildcards, n[i]->parent(), n[i]->parent()->name(), "" ); 

	  //	  std::cerr << "allhists: " << n[i]->parent()->name() << "\tall: " << allhists << ":" << std::endl;

	  std::string _algorithm = algorithm;

	  if ( !mallhists || allhists!="" ) { 
	    if ( first_hists ) {
	      if ( allhists!="" ) _algorithm = allhists; 
	      (*outp) << space << "\t"   << "hist   all_in_dir {\n";
 	      if ( _algorithm!="NULL" && _algorithm!="0" ) (*outp) << space << "\t\t" << "algorithm   \t= " << _algorithm << "\n";
>>>>>>> release/21.0.127
	      (*outp) << space << "\t\t" << "description \t= " << description << "\n";
	      (*outp) << space << "\t\t" << "output      \t= " << path << "\n";
	      (*outp) << space << "\t\t" << "display     \t= StatBox\n";
	      /// extra user specified tags
	      for ( unsigned it=0 ; it<tags.size() ; it++ ) (*outp) << space << "\t\t" << replace(tags[it],"=","\t=") << "\n";
	      (*outp) << space << "\t"   << "}\n";
	    }
	    first_hists = false;
	  }
	  else { 
<<<<<<< HEAD
	    (*outp) << space << "\t"   << "hist " << n[i]->name() << " {\n";
	    (*outp) << space << "\t\t" << "algorithm   \t= " << algorithm << "\n";
	    (*outp) << space << "\t\t" << "description \t= " << description << "\n";
	    (*outp) << space << "\t\t" << "output      \t= " << path << "\n";
	    (*outp) << space << "\t\t" << "display     \t= StatBox\n";
=======
	    
	    std::string _algorithm   = find( algorithms,   n[i], n[i]->name(), algorithm );
	    std::string _description = find( descriptions, n[i], n[i]->name(), description );
	    std::string _display     = find( displays,     n[i], n[i]->name(), "StatBox" );

	    (*outp) << space << "\t"   << "hist " << n[i]->name() << " {\n";
	    (*outp) << space << "\t\t" << "algorithm   \t= " << _algorithm << "\n";
	    (*outp) << space << "\t\t" << "description \t= " << _description << "\n";
	    (*outp) << space << "\t\t" << "output      \t= " << path << "\n";
	    (*outp) << space << "\t\t" << "display     \t= " << _display << "\n";
>>>>>>> release/21.0.127
	    /// extra user specified tags
	    for ( unsigned it=0 ; it<tags.size() ; it++ ) (*outp) << space << "\t\t" << replace(tags[it],"=","\t=") << "\n";
	    (*outp) << space << "\t"   << "}\n";

	    //      TH1* ase = (TH1*)(n[i]->object());
	    //	    if ( ase ) std::cerr << space << "\t\t" << "entries     = " << ase->GetEntries() << "\n";
	    //	    if ( ase ) std::cerr << space << "\t\t" << "entries     = \"" << ase->GetTitle()   << "\"\n";

	  }
	}
      }
      
      if ( print ) (*outp) << space << "}\n"; ///  \t\t##" << n.name() << "\n";
    }    
    
  }
  
private:

<<<<<<< HEAD
  bool m_allhists;
=======
  bool mallhists;
>>>>>>> release/21.0.127

};






bool found_dir = false;

std::string currentfile = "";


/// recursive directory search for TH1 and TH2 and TProfiles

void search( TDirectory* td, const std::string& s, std::string cwd, node* n ) { 

  /// not a directory 
  if ( td==0 ) return;

  if ( std::string(td->GetName()).find("_LB")!=std::string::npos ) return;
  if ( std::string(td->GetName()).find("lb_")!=std::string::npos ) return;

  //  std::cout << "search() in  " << s << td->GetName() << ":    :" << cwd << ":" << std::endl;

  static int ir = 0;

  ir++;

  /// don't go more than 10 directories deep

  if ( ir>10 ) { 
    std::cerr << "search() WARNING too many levels of directories (max 10) !!!\n";
    return;
  }


  TDirectory* here = gDirectory;

  //  gDirectory->pwd();

  td->cd();
  
  if ( cwd!="" ) cwd += "/";
  cwd += td->GetName();

  node* np = n;

  std::string fulldir = td->GetName();


  bool first_found = false;
  if ( !found_dir && matchcwd( cwd ) ) {

    std::string ase = matchcwdstr( cwd );

    if ( (cwd+"/").find( ase+"/" )!=std::string::npos ) { 
    
      found_dir   = true;
      first_found = true;
      
      std::cerr << "matched dirs " << cwd << " " << matchdir( cwd ) << std::endl;
      
      
      std::map<std::string,int>::const_iterator fitr = matchcwditr( cwd );
      
      if ( fitr!=dirs.end() ) { 
	
	if ( fitr->second>0 ) { 
	  
	  std::vector<std::string> subpath;
	  
	  std::string sp = fitr->first; 
	  
	  while( sp.size() ) subpath.push_back( chop(sp,"/") ); 
	  
	  for ( unsigned ip=0 ; ip<subpath.size()-1 ; ip++ ) { 
	    //	    std::cerr << "subpath " << ip << " " << subpath[ip] << std::endl;
	    
	    node* np_ = addnode( np, subpath[ip] );
	    
	    np = np_;
	    
	  }
	}
      }
      
    }
  }
  

  if ( found_dir ) { 
    node* np_ = addnode( np, fulldir, td );
    np = np_;
  }

  if ( found_dir && verbose ) std::cerr << s << cwd << std::endl;


  TList* tl  = gDirectory->GetListOfKeys();
  
  //  struct timeval tv = simpletimer_start();
  
  //  (*outp) << "\ttl " << tl << std::endl;
  
  /// go through sub directories

  for ( int i=0 ; i<tl->GetSize() ; i++ ) { 
    
    TKey* tobj = (TKey*)tl->At(i);
    
    if ( tobj==0 ) continue;
    
    //    (*outp) << "tobj " << tobj;
    //    if ( tobj ) (*outp) << " : \t" << tobj->GetName();
    //    (*outp) << std::endl; 
    
    if ( std::string(tobj->GetClassName()).find("TDirectory")!=std::string::npos ) { 
      //      (*outp) << ns << "Directory " << tobj->GetName() << std::endl;
      
      TDirectory* tnd = (TDirectory*)tobj->ReadObj();
      

      /// descend into the subdirectory ... 
      if ( tnd )  search( tnd, s+spacer, cwd, np );
	
    }
    else { 

      /// if this is a directory we want, print (*outp) the histograms 

      if ( found_dir ) { 
	if ( std::string(tobj->GetClassName()).find("TH1")!=std::string::npos ||
	     std::string(tobj->GetClassName()).find("TH2")!=std::string::npos ||
	     std::string(tobj->GetClassName()).find("TProfile")!=std::string::npos ) {
	  
	  //	  TH1* th = (TH1*)tobj->ReadObj();

	  //	  node* h = addnode( np, "", get<TObject>(tobj), node::HISTOGRAM );
	  // node* h = 
	  addnode( np, tobj->GetName(), get<TObject>(tobj), node::HISTOGRAM );
	  

	  /// get the full path to this object path relative to the file	  
	  std::string subdir = cwd;
	  chop( subdir, currentfile+"/" );

	  /// save the histograms in case we need to save the, later ...
	  savedhistos.push_back( subdir+"/"+tobj->GetName() );

	  /// keep the max number of entries updated
	  if ( std::string(tobj->GetName())=="Chain" ) { 
<<<<<<< HEAD
	    double N = ((TH1*)get<TObject>(tobj))->GetEntries();
=======
	    double N = ((TH1*)get<TObject>(tobj))->GetBinContent(1);
>>>>>>> release/21.0.127

	    //	    std::cout << "entries " << np->name() << " " << " " << np->parent()->name() << " " << N << std::endl;
	    //    std::cout << "\tentries " << np->parent()->name() << "/" << np->name() << "\t" << N << std::endl;
	    std::cout << "\t" << subdir << "\t" << N << std::endl;

	    node* p  = np->parent();
	    if ( p && p->name()!="Shifter" ) { 

	      p->addrate( p->name(), N );

	      node* p2 = p->parent();
	      if ( p2 ) p2->addrate( p->name(), N ); 
	    }
	  }

	}
	
	//      if ( !status ) std::cerr << "bad status" << std::endl;
      }
    }
    
  }
  

  //  double _t = simpletimer_stop(tv);
  
  //  double global_time = simpletimer_stop(global_timer);
    
  ir--;
    
  here->cd();
  
  if ( first_found ) found_dir = false;

}










int cost( std::vector<std::string>& files, node& n, const std::string& directory="", bool deleteref=false, bool relocate=false  ) { 

  std::cerr << "processing ..." << std::endl;

  //  std::cerr << "opening root files" << std::endl;

  fptr.resize(files.size());


  for ( unsigned i=0 ; i<files.size() ; i++ ) { 

    currentfile = files[i];

    std::cerr << "opening " << currentfile << std::endl;

    if ( !contains( files[i], "root://eosatlas") && !file_exists( files[i] ) ){ 
      std::cerr << "file " << files[i] << " does not exist" << std::endl;
      return -1;	
    }
    
    /// open the output file
    fptr[i] = TFile::Open( files[i].c_str() );
    
    if ( fptr[i]==0 || fptr[i]->IsZombie() ) { 
      std::cerr << "file " << files[i] << " cannot be opened" << std::endl;
      return -1;
    }
  
    fptr[i]->cd();

    if ( directory!="" ) fptr[i]->cd(directory.c_str());    

    TDirectory* here = gDirectory;
    
    //    global_timer = simpletimer_start();

    //    int tcount = 0;

    /// navigate the directory structure to 
    /// extracting all the info

    search( gDirectory, "", "", &n );

    here->cd();
    

    /// if asked to delete the unused referenece then do so
    /// if however asked to relocate the histgrams, simply 
    /// modify the list of histogram destinations first
    if ( deleteref || relocate ) { 

      std::cerr << "remapping" << std::endl;

      if ( relocate ) mapped = maphist( savedhistos );
      
      if ( relocate && !deleteref ) std::cerr << "saving histograms to file .newhist.root ... " << std::endl;

      if ( outref=="" ) outref = ".newhist.root";
      TFile* fnew = new TFile( outref.c_str(), "recreate" );
      fnew->cd();

      TDirectory*  base = gDirectory;

      if ( mapped.size() != savedhistos.size() ) mapped = savedhistos;

      for ( unsigned ih=0 ; ih<savedhistos.size() ; ih++ ) { 
	
	std::vector<std::string> dirs = split( mapped[ih], "/" );

	for ( unsigned jh=0 ; jh<dirs.size()-1 ; jh++ ) { 
	  /// std::cerr << "\t" << dirs[jh] << std::endl;
	  TDirectory* renedir = gDirectory->GetDirectory( dirs[jh].c_str() );
	  if ( renedir==0 ) gDirectory->mkdir( dirs[jh].c_str() );
	  gDirectory->cd( dirs[jh].c_str() );
	}
	
	TH1* href  = (TH1*)fptr[i]->Get( savedhistos[ih].c_str() );
	if ( href ) {
	  //	  std::cerr << ih << " " << savedhistos[ih] << " 0x" << href << std::endl;
	  href->Write( dirs.back().c_str() );
	}

	base->cd();
      }


    }

    std::cerr << "closing files" << std::endl; 
        
    fptr[i]->Close();

    delete fptr[i];

    if ( deleteref ) { 
      if ( outref==".newhist.root" ) { 
	std::cerr << "replacing histogram file" << std::endl;
	std::string cmd = std::string("mv ") + files[i] + " " + files[i] + ".bak";
	std::system( cmd.c_str() );
	cmd = std::string("mv .newhist.root ") + files[i];
	std::system( cmd.c_str() );
      }
    }
    
  }

  return 0;
}





int usage(std::ostream& s, int , char** argv, int status=-1) { 
  s << "Usage: " << argv[0] << " [OPTIONS] input1.root ... inputN.root\n\n";
<<<<<<< HEAD
  s << "    -o             FILENAME  \tname of output (filename required)\n";
  s << "    -b,   --base   DIR       \tuse directory DIR as the base for the han config\n";
  s << "    -d,   --dir    DIR       \tonly directories below DIR where DIR is a structure such as HLT/TRIDT etc\n";
  s << "    -x,            DIR       \texclude directory DIR\n";
  s << "    -r             SRC DST   \tremap directory SRC to directory DST\n"; 
  s << "    -ds,  --desc   DESCRIP   \tuse DESCRIP as the description\n"; 
  s << "    -t,   --tag    VALUE     \tadd the VALUE to the list of command per histogram\n";
  s << "    -a,   --algorithm VALUE  \tuse VALUE as the execution algorithm for each histogram\n";
  s << "    -wc,  --wildcard         \tprint use hist * rather than a separate entry for each histogram\n";
  s << "    -dr,  --deleteref        \tdelete unselected histograms\n";
  s << "    -or,  --outref FILENAME  \tdelete file to write reduced output to (overwrites input otherwise) \n";
  s << "    -rh,  --relocate         \trelocate selected histograms\n";
  s << "    -ref, --reference TAG FILE \tadd FILE as a reference file with tag TAG\n";
  s << "    -rc,  --refconf       FILE \tadd FILE to the config as a reference block\n";
  s << "    -v,   --verbose          \tprint verbose output\n";
  s << "    -h,   --help             \tdisplay this help\n";
=======
  s << "    -o                FILENAME  \tname of output (filename required)\n";
  s << "    -b,   --base      DIR       \tuse directory DIR as the base for the han config\n";
  s << "    -d,   --dir       DIR       \tonly directories below DIR where DIR is a structure such as HLT/TRIDT etc\n";
  s << "    -x,   --exclude   DIR       \texclude directory DIR\n";
  s << "    -s,   --slice     SLICE     \ttrigger signature name (for comments)\n"; 
  s << "    -r,   --remap     SRC DST   \tremap directory SRC to directory DST\n"; 
  s << "    -a,   --algorithm VALUE     \tuse VALUE as the execution algorithm for each histogram\n";
  s << "    -af,  --algfile   FILENAME  \tread algorithm information from FILENAME\n";
  s << "    -ds,  --desc      DESCRIP   \tuse DESCRIP as the description\n"; 
  s << "    -df,  --descfile  FILENAME  \tread descriptions from FILENAME\n"; 
  s << "    -dp,  --dispfile  FILENAME  \tread display information from FILENAME\n"; 
  s << "    -t,   --tag       VALUE     \tadd the VALUE to the list of command per histogram\n";
  s << "    -wc,  --wildcard            \tprint use hist * rather than a separate entry for each histogram\n";
  s << "    -wf,  --wcfile    FILENAME  \tread list of directories to wildcard from a file\n";
  s << "    -wd,  --wcdir     DIR=ALG   \tadd wildcard for DIRECTORY using ALG for the algorithm\n";
  s << "    -dr,  --deleteref           \tdelete unselected histograms\n";
  s << "    -or,  --outref   FILENAME   \tfile to write reduced output to (overwrites input otherwise) \n";
  s << "    -rh,  --relocate            \trelocate selected histograms\n";
  s << "    -ref, --reference TAG FILE  \tadd FILE as a reference file with tag TAG\n";
  s << "    -rc,  --refconf       FILE  \tadd FILE to the config as a reference block\n";
  s << "    -v,   --verbose             \tprint verbose output\n";
  s << "    -h,   --help                \tdisplay this help\n";
>>>>>>> release/21.0.127
  s << std::endl;
  return status;
}


std::vector<std::string> refblock;

void referenceblock( const std::string& file ) { 
  std::ifstream strm(file.c_str());
  for ( std::string line ; getline( strm, line ); ) refblock.push_back( line );
} 


<<<<<<< HEAD
int main(int argc, char** argv) { 

  //  std::string cock = "HLT_j150_bperf_split/InDetTrigTrackingxAODCnv_BjetPrmVtx_FTF_SuperRoi/Chain"; 

  //  replace 

  //  std::cout << replace 
=======

int main(int argc, char** argv) { 
>>>>>>> release/21.0.127

  gStyle->SetPadRightMargin(0.05);
  gStyle->SetPadTopMargin(0.075);

<<<<<<< HEAD
  //  TCanvas* tg = new TCanvas("tg", "tg", 650, 900 );
=======

>>>>>>> release/21.0.127
  TCanvas* tg = new TCanvas("tg", "tg", 700, 600 );
  tg->cd();

  gStyle->SetStatY(0.4);                
  // Set y-position (fraction of pad size)
  gStyle->SetStatX(0.89);                
  // Set x-position (fraction of pad size)
  gStyle->SetStatW(0.25);                
  // Set width of stat-box (fraction of pad size)
  gStyle->SetStatH(0.16);      


<<<<<<< HEAD
  //  if ( argc<3 ) usage( std::cerr << "not enough command options", argc, argv );
  if ( argc<2 ) return usage( std::cerr, argc, argv );


  for ( int i=1 ; i<argc ; i++ ) { 
    if ( std::string(argv[i])=="-h" || std::string(argv[i])=="--help" )  return usage( *outp, argc, argv, 0 ); 
    //    if ( std::string(argv[i])=="-v" || std::string(argv[i])=="--version" ) {
    //      (*outp) << argv[0] << " APPLgrid version " << PACKAGE_VERSION << std::endl; 
    //  return 0;
  }
=======
  if ( argc<2 ) return usage( std::cerr, argc, argv );

  /// handle the help message option before dealing with
  /// any other arguments 

  for ( int i=1 ; i<argc ; i++ ) { 
    if ( std::string(argv[i])=="-h" || std::string(argv[i])=="--help" )  return usage( *outp, argc, argv, 0 ); 
  }

  /// now properly parse cmdline options and configure
>>>>>>> release/21.0.127
  
  std::string dir = "";

  std::vector<std::string> subdirs;

<<<<<<< HEAD
=======
  std::string              wildcardfile;

>>>>>>> release/21.0.127

  bool deleteref = false;
  bool relocate  = false;

  std::string outfile = "";
<<<<<<< HEAD
=======
  std::string   slice = "";

  std::string descriptionfile = "";
  std::string displayfile     = "";
  std::string algfile         = "";

>>>>>>> release/21.0.127

  int offset = 1;


  for ( int i=1 ; i<argc ; i++ ) { 
<<<<<<< HEAD
    if      ( std::string(argv[i])=="-v" || std::string(argv[i])=="--verbose" ) verbose = true;
    else if ( std::string(argv[i])=="-o" ) {
      ++i;
      if ( i<argc-offset ) outfile = argv[i];
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( std::string(argv[i])=="-or" || std::string(argv[i])=="--outrefr" ) {
      ++i;
      if ( i<argc-offset ) outref = argv[i];
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( std::string(argv[i])=="-ref" || std::string(argv[i])=="--reference" ) {
      std::string reftag;
      std::string reffile;
      ++i;
      if ( i<argc-offset ) reftag = argv[i];
      else  return usage( std::cerr, argc, argv );
      ++i;
      if ( i<argc-offset ) reffile = argv[i];
=======

    std::string argvi = std::string(argv[i]);

    if      ( argvi=="-v" || argvi=="--verbose" ) verbose = true;
    else if ( argvi=="-o" ) {
      if ( ++i<argc-offset ) outfile = argv[i];
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( argvi=="-or" || argvi=="--outref" ) {
      if ( ++i<argc-offset ) outref = argv[i];
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( argvi=="-ref" || argvi=="--reference" ) {
      std::string reftag;
      std::string reffile;
      if ( ++i<argc-offset ) reftag = argv[i];
      else  return usage( std::cerr, argc, argv );
      if ( ++i<argc-offset ) reffile = argv[i];
>>>>>>> release/21.0.127
      else  return usage( std::cerr, argc, argv );
      references.push_back( reference( reftag, reffile ) ); 
      //      std::cerr << references.back() << std::endl;
    } 
<<<<<<< HEAD
    else if ( std::string(argv[i])=="-rc" || std::string(argv[i])=="-refconf" ) {
      ++i;
      if ( i<argc-offset ) referenceblock( argv[i] );
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( std::string(argv[i])=="-dr"  || std::string(argv[i])=="--deleteref" ) deleteref = true;
    else if ( std::string(argv[i])=="-rh"  || std::string(argv[i])=="--relocate" )  relocate  = true;
    else if ( std::string(argv[i])=="-wc"  || std::string(argv[i])=="--wildcard"  )  allhists = false;
    else if ( std::string(argv[i])=="-d"   || std::string(argv[i])=="--dir"       ) {
      ++i;
      
      if ( i<argc-offset ) { 
	  dirs.insert( std::map<std::string,int>::value_type( argv[i], count( argv[i], "/" ) ) );
	  
	  std::string tdir = argv[i];
	  
	  //	  std::cerr << "dirs " << argv[i] << std::endl;
	  
	  do { 
	    subdirs.push_back( chop( tdir, "/" ) );
	    //   std::cerr << "chop  " << subdirs.back() << std::endl;
	  }
=======
    else if ( argvi=="-rc" || argvi=="--refconf" ) {
      if ( ++i<argc-offset ) referenceblock( argv[i] );
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( argvi=="-s" || argvi=="--slice" ) {
      if ( ++i<argc-offset ) slice = argv[i];
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( argvi=="-dr"  || argvi=="--deleteref" ) deleteref = true;
    else if ( argvi=="-rh"  || argvi=="--relocate" )  relocate  = true;
    else if ( argvi=="-wc"  || argvi=="--wildcard"  )  allhists = false;
    else if ( argvi=="-d"   || argvi=="--dir"       ) {
      if ( ++i<argc-offset ) { 
	  dirs.insert( std::map<std::string,int>::value_type( argv[i], count( argv[i], "/" ) ) );
	  std::string tdir = argv[i];
	  do { 
	    subdirs.push_back( chop( tdir, "/" ) );
	  } 
>>>>>>> release/21.0.127
	  while ( tdir.size() ); 
      }
      else  return usage( std::cerr, argc, argv );
    } 
<<<<<<< HEAD
    else if ( std::string(argv[i])=="-x" ) {
      ++i;
      if ( i<argc-offset ) exclude.insert( argv[i] );
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( std::string(argv[i])=="-ds" || std::string(argv[i]).find("--desc")==0 ) {
      ++i;
      if ( i<argc-offset ) description = argv[i];
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( std::string(argv[i])=="-b" || std::string(argv[i])=="--base" ) {
      ++i;
      if ( i<argc-offset ) base = argv[i] ;
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( std::string(argv[i])=="-a" || std::string(argv[i])=="--algorithm" ) {
      ++i;
      if ( i<argc-offset ) algorithm = argv[i] ;
      else  return usage( std::cerr, argc, argv );
    } 
    //    else if ( std::string(argv[i])=="-o" ) { 
    //    ++i;
    //    if ( i<argc ) output_file = argv[i];
    //   else  return usage( std::cerr, argc, argv );
    //   }
    else if ( std::string(argv[i])=="-t" || std::string(argv[i])=="--tag" ) {
      ++i;
      if ( i<argc-offset ) tags.push_back( argv[i] );
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( std::string(argv[i])=="-r" ) { 
=======
    else if ( argvi=="-x" || argvi=="--exclude" ) {
      if ( ++i<argc-offset ) exclude.insert( argv[i] );
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( argvi=="-ds" || argvi.find("--desc")==0 ) {
      if ( ++i<argc-offset ) description = argv[i];
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( argvi=="-df" || argvi.find("--descfile")==0 ) {
      if ( ++i<argc-offset ) descriptionfile = argv[i];
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( argvi=="-af" || argvi.find("--algfile")==0 ) {
      if ( ++i<argc-offset ) algfile = argv[i];
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( argvi=="-dp" || argvi.find("--dispfile")==0 ) {
      if ( ++i<argc-offset ) displayfile = argv[i];
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( argvi=="-wd" || argvi.find("--wcdir")==0 ) {
      if ( ++i<argc-offset ) { 
	histogram     tag="";
	std::string value="";
	/// parse the input string - add a terminating ";" just in case 
	if ( parse( std::string(argv[i])+";", tag, value, false ) ) wildcards.insert( hmap_t::value_type( tag, value ) );
	else  return usage( std::cerr << "Could not parse wildcard directory", argc, argv );
      }
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( argvi=="-wf" || argvi.find("--wcfile")==0 ) {
      if ( ++i<argc-offset ) wildcardfile = argv[i];
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( argvi=="-dp" || argvi.find("--dispfile")==0 ) {
      if ( ++i<argc-offset ) displayfile = argv[i];
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( argvi=="-b" || argvi=="--base" ) {
      if ( ++i<argc-offset ) base = argv[i] ;
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( argvi=="-a" || argvi=="--algorithm" ) {
      if ( ++i<argc-offset ) algorithm = argv[i] ;
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( argvi=="-t" || argvi=="--tag" ) {
      if ( ++i<argc-offset ) tags.push_back( argv[i] );
      else  return usage( std::cerr, argc, argv );
    } 
    else if ( argvi=="-r" || argvi=="--remap" ) { 
>>>>>>> release/21.0.127
      std::string src;
      std::string dest;
      if ( i<argc+2-offset ) { 
	src  = argv[++i];
	dest = argv[++i];
	remap.insert( std::map<std::string,std::string>::value_type( src, dest ) );
      } else return usage( std::cerr, argc, argv );
    }
    else { 
      offset = 0;
      files.push_back(argv[i]);
    }
  }

<<<<<<< HEAD
=======

>>>>>>> release/21.0.127
  //  std::cout << "tags " << tags.size() << " " << tags << std::endl;

  if ( base == "" ) base = dir;

<<<<<<< HEAD
  /// if output file is not defined
  //  if ( output_file == "" ) return usage( std::cerr, argc, argv );
  
  //  dataset data("test_EF");
  //  files = data.datafiles();
  
=======
>>>>>>> release/21.0.127
  /// check some input files

  if ( files.size()<1 ) return usage( std::cerr, argc, argv );
  

<<<<<<< HEAD
=======
  /// get algorithm information from a file if required 

  if ( algfile!="" ) { 
    std::cerr << "reading algorithm information from : \t" << algfile << std::endl; 
    if ( !file_exists( algfile ) ) error( 1, std::cerr << "algorithm file " << algfile << " does not esist" ); 
    algorithms = parse( algfile, false );
  }


  /// get descriptions from a file if required

  if ( descriptionfile!="" ) { 
    std::cerr << "reading decriptions from : \t\t" << descriptionfile << std::endl; 
    if ( !file_exists( descriptionfile ) ) error( 1, std::cerr << "decription file " << descriptionfile << " does not esist" ); 
    descriptions = parse( descriptionfile );
  }


  /// get display information from file if required

  if ( displayfile!="" ) { 
    std::cerr << "reading display information from : \t" << displayfile << std::endl; 
    if ( !file_exists( displayfile ) ) error( 1, std::cerr << "display file " << displayfile << " does not esist" ); 
    displays = parse( displayfile, false );
  }


  /// parse the wildcard directories 

  if ( wildcardfile!="" ) { 
    std::cerr << "reading wildcard information from : \t" << wildcardfile << std::endl; 
    if ( !file_exists( wildcardfile ) ) error( 1, std::cerr << "wildcard file " << wildcardfile << " does not esist" ); 
    hmap_t _wildcards = parse( wildcardfile, false );
    wildcards.insert(  _wildcards.begin(), _wildcards.end() );
  }

  //  for ( hmap_t::const_iterator itr = wildcards.begin() ; itr!=wildcards.end() ; itr++ ) {
  //     std::cout << "wildcard: " << itr->first << " " << itr->second << std::endl;
  // }


>>>>>>> release/21.0.127
  //  time the actual running of the cost() routine
  
  if ( verbose ) std::cerr << "timing" << std::endl;

  struct timeval tv = simpletimer_start();

  /// create the structure ...

  node n(0, "" );
  n.name( "top_level" );

  int status = cost( files, n, "", deleteref, relocate );

  //  std::cerr << "\n\nnodes " << n << std::endl;

  if ( status ) return status;

  if ( outfile!="" ) outp = new std::ofstream(outfile.c_str()); 

<<<<<<< HEAD
  header h;
=======
  header h( outfile, slice );
>>>>>>> release/21.0.127

  for ( unsigned ir=0 ; ir<references.size() ; ir++ ) (*outp) << references[ir] << std::endl; 

  if ( refblock.size() ) (*outp) << refblock << std::endl;

  /// create the side bar menu part 
  menu m( n );

  /// create the histogram assessment part
  ass( n, allhists );

  /// ensure that buffer is properly flushed
  (*outp) << std::endl;

  double t = simpletimer_stop(tv);
  
  if ( t>1000 ) std::cerr << "total time " << t*0.001 << " s"  << std::endl;
  else          std::cerr << "total time " << t       << " ms" << std::endl;

  return 0;
}
