<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
# $Id: CMakeLists.txt 757192 2016-06-23 09:39:00Z krasznaa $
################################################################################
# Package: TBCnv
################################################################################
>>>>>>> release/21.0.127

# Declare the package name:
atlas_subdir( TBCnv )

<<<<<<< HEAD
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Calorimeter/CaloIdentifier
   Control/AthenaBaseComps
   Control/StoreGate
   Event/ByteStreamCnvSvc
   Event/ByteStreamCnvSvcBase
   Event/ByteStreamData
   Event/EventInfo
   GaudiKernel
   LArCalorimeter/LArIdentifier
   LArCalorimeter/LArTools
   TestBeam/TBEvent
   PRIVATE
   DetectorDescription/Identifier
   LArCalorimeter/LArRawEvent )

>>>>>>> release/21.0.127
# External dependencies:
find_package( tdaq-common COMPONENTS eformat eformat_write )

# Component(s) in the package:
atlas_add_library( TBCnvLib
   TBCnv/*.h TBCnv/*.icc src/*.cxx
   PUBLIC_HEADERS TBCnv
   INCLUDE_DIRS ${TDAQ-COMMON_INCLUDE_DIRS}
   LINK_LIBRARIES ${TDAQ-COMMON_LIBRARIES} CaloIdentifier AthenaBaseComps
<<<<<<< HEAD
   ByteStreamData EventInfo GaudiKernel LArIdentifier TBEvent
   ByteStreamCnvSvcLib ByteStreamCnvSvcBaseLib LArCablingLib
   PRIVATE_LINK_LIBRARIES Identifier LArRawEvent StoreGateLib )

atlas_add_component( TBCnv
   src/components/*.cxx
   LINK_LIBRARIES TBEvent TBCnvLib )
=======
   ByteStreamData EventInfo GaudiKernel LArIdentifier TBEvent StoreGateLib
   ByteStreamCnvSvcLib ByteStreamCnvSvcBaseLib LArToolsLib
   PRIVATE_LINK_LIBRARIES Identifier LArRawEvent )

atlas_add_component( TBCnv
   src/components/*.cxx
   LINK_LIBRARIES TBCnvLib )
>>>>>>> release/21.0.127

# Install files from the package:
atlas_install_joboptions( share/*.py )
