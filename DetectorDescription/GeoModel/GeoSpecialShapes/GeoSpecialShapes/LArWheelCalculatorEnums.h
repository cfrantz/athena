/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef GEOSPECIALSHAPES_LARWHEELCALCULATORENUMS_H
#define GEOSPECIALSHAPES_LARWHEELCALCULATORENUMS_H

namespace LArG4 {

<<<<<<< HEAD
  enum LArWheelCalculator_t {
    InnerAbsorberWheel,  OuterAbsorberWheel,
    InnerElectrodWheel,  OuterElectrodWheel,
    InnerAbsorberModule, OuterAbsorberModule,
    InnerElectrodModule, OuterElectrodModule,
    BackInnerBarretteWheel,       BackOuterBarretteWheel,
    BackInnerBarretteWheelCalib,  BackOuterBarretteWheelCalib,
    BackInnerBarretteModule,      BackOuterBarretteModule,
    BackInnerBarretteModuleCalib, BackOuterBarretteModuleCalib,
    InnerGlueWheel, OuterGlueWheel,
    InnerLeadWheel, OuterLeadWheel
  };

  struct ROOT6_NamespaceAutoloadHook_WheelCalc{};
=======
   enum LArWheelCalculator_t {
		InnerAbsorberWheel,  OuterAbsorberWheel,
		InnerElectrodWheel,  OuterElectrodWheel,
		InnerAbsorberModule, OuterAbsorberModule,
		InnerElectrodModule, OuterElectrodModule,
		BackInnerBarretteWheel,       BackOuterBarretteWheel,
		BackInnerBarretteWheelCalib,  BackOuterBarretteWheelCalib,
		BackInnerBarretteModule,      BackOuterBarretteModule,
		BackInnerBarretteModuleCalib, BackOuterBarretteModuleCalib,
		InnerGlueWheel, OuterGlueWheel,
		InnerLeadWheel, OuterLeadWheel
	};

   struct ROOT6_NamespaceAutoloadHook_WheelCalc{};
>>>>>>> release/21.0.127
}
#endif
