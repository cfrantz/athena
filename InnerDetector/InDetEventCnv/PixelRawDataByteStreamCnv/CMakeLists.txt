# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( PixelRawDataByteStreamCnv )

<<<<<<< HEAD
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Event/ByteStreamData
   GaudiKernel
   InnerDetector/InDetRawEvent/InDetRawData
   PRIVATE
   Control/AthenaBaseComps
   Control/StoreGate
   Event/ByteStreamCnvSvcBase
   InnerDetector/InDetConditions/InDetConditionsSummaryService
   InnerDetector/InDetConditions/PixelConditionsServices
   InnerDetector/InDetDetDescr/InDetIdentifier
   InnerDetector/InDetDetDescr/InDetReadoutGeometry
   InnerDetector/InDetDetDescr/PixelCabling
   Event/xAOD/xAODEventInfo )

>>>>>>> release/21.0.127
# External dependencies:
find_package( tdaq-common COMPONENTS eformat )

# Component(s) in the package:
atlas_add_library( PixelRawDataByteStreamCnvLib
   PixelRawDataByteStreamCnv/*.h
   INTERFACE
   PUBLIC_HEADERS PixelRawDataByteStreamCnv
<<<<<<< HEAD
   LINK_LIBRARIES GaudiKernel ByteStreamData InDetByteStreamErrors InDetRawData )
=======
   LINK_LIBRARIES GaudiKernel ByteStreamData InDetRawData )
>>>>>>> release/21.0.127

atlas_add_component( PixelRawDataByteStreamCnv
   src/*.h src/*.cxx src/components/*.cxx
   INCLUDE_DIRS ${TDAQ-COMMON_INCLUDE_DIRS}
<<<<<<< HEAD
   LINK_LIBRARIES ${TDAQ-COMMON_LIBRARIES} ByteStreamData AthenaKernel EventContainers
   GaudiKernel InDetRawData AthenaBaseComps AthContainers CxxUtils StoreGateLib
   ByteStreamCnvSvcBaseLib InDetIdentifier PixelReadoutGeometry IRegionSelector
   xAODEventInfo TrigSteeringEvent InDetByteStreamErrors PixelConditionsData PixelRawDataByteStreamCnvLib PixelCablingLib ByteStreamCnvSvcLib )
=======
   LINK_LIBRARIES ${TDAQ-COMMON_LIBRARIES} ByteStreamData
   GaudiKernel InDetRawData AthenaBaseComps StoreGateLib
   ByteStreamCnvSvcBaseLib InDetIdentifier InDetReadoutGeometry
   xAODEventInfo PixelCablingLib PixelRawDataByteStreamCnvLib )
>>>>>>> release/21.0.127
