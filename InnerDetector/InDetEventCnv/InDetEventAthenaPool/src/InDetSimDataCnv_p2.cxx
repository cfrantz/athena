/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#include "InDetSimData/InDetSimData.h"
#include "MsgUtil.h"

// Persistent class and converter header file
#include "InDetEventAthenaPool/InDetSimData_p2.h"
#include "InDetSimDataCnv_p2.h"
<<<<<<< HEAD
#include "AthenaBaseComps/AthMessaging.h"

=======
#include "GeneratorObjectsTPCnv/HepMcParticleLinkCnv_p2.h"
#include "SGTools/IProxyDictWithPool.h"
>>>>>>> release/21.0.127

typedef std::vector<InDetSimData::Deposit>::const_iterator depositIterator;

InDetSimDataCnv_p2::InDetSimDataCnv_p2()
  : m_sg(nullptr)
{
}


void
InDetSimDataCnv_p2::persToTrans(const InDetSimData_p2* persObj, InDetSimData* transObj, MsgStream &log)
{
  MSG_VERBOSE(log,"InDetSimDataCnv_p2::persToTrans called ");
<<<<<<< HEAD
  std::vector<InDetSimData::Deposit> deposits;
  const unsigned int ndeposits = persObj->m_enDeposits.size();
  deposits.reserve( ndeposits );
  for (unsigned int icount=0; icount < ndeposits; icount++) {
    HepMcParticleLink mcLink (m_sg);
=======
  HepMcParticleLinkCnv_p2 HepMcPLCnv;
  std::vector<InDetSimData::Deposit> deposits;
  deposits.reserve( persObj->m_enDeposits.size() );
  for (unsigned int icount=0; icount < persObj->m_enDeposits.size(); icount++) {
    HepMcParticleLink mcLink;
>>>>>>> release/21.0.127
    HepMcPLCnv.persToTrans(&(persObj->m_links[icount]),&mcLink, log);
    deposits.emplace_back (mcLink, persObj->m_enDeposits[icount]);
  }

  *transObj = InDetSimData (std::move(deposits),
                            persObj->m_word);
}

void
<<<<<<< HEAD
InDetSimDataCnv_p2::transToPers(const InDetSimData* transObj, InDetSimData_p2* persObj, MsgStream &log)
{
  MSG_VERBOSE(log,"InDetSimDataCnv_p2::transToPers called ");
  HepMcParticleLinkCnv_p2 HepMcPLCnv;

  persObj->m_word = transObj->word();
  const std::vector<InDetSimData::Deposit> &dep(transObj->getdeposits());
  const unsigned int ndeposits = dep.size();
  persObj->m_links.resize(ndeposits);
  persObj->m_enDeposits.resize(ndeposits);
  for (unsigned int icount(0); icount < ndeposits; ++icount) {
    HepMcPLCnv.transToPers(&(dep[icount].first), &(persObj->m_links[icount]), log);
    persObj->m_enDeposits[icount] = dep[icount].second;
  }
}

void InDetSimDataCnv_p2::setCurrentStore (IProxyDict* store)
=======
InDetSimDataCnv_p2::transToPers(const InDetSimData*, InDetSimData_p2*, MsgStream &/*log*/)
{
  throw std::runtime_error("InDetSimDataCnv_p2::transToPers is not supported in this release!");}

void InDetSimDataCnv_p2::setCurrentStore (IProxyDictWithPool* store)
>>>>>>> release/21.0.127
{
  m_sg = store;
}
