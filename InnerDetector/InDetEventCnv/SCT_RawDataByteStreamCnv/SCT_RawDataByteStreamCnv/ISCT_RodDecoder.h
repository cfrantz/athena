/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

/**
<<<<<<< HEAD
 * @file SCT_RawDataByteStreamCnv/ISCT_RodDecoder.h
 * @author Kondo.Gnanvo@cern.ch, Maria.Jose.Costa@cern.ch
 *
 * AlgTool class to decode ROB bytestream data into RDO
=======
 *      @file header for SCT_RodDecoder Class
 *      AlgTool class to decode ROB bytestream data into RDO
 *      @author: Kondo.Gnanvo@cern.ch, Maria.Jose.Costa@cern.ch
 *      @current developer: Kondo Gnanvo, QMUL (London), august 2005
>>>>>>> release/21.0.127
 */

#ifndef INDETRAWDATABYTESTREAM_ISCT_RODDECODER_H 
#define INDETRAWDATABYTESTREAM_ISCT_RODDECODER_H

#include "GaudiKernel/IAlgTool.h"

#include "InDetRawData/SCT_RDO_Container.h"
#include "ByteStreamData/RawEvent.h"
#include "InDetByteStreamErrors/IDCInDetBSErrContainer.h"

#include <vector>

class StatusCode;
class IdentifierHash;

<<<<<<< HEAD
class ISCT_RodDecoder : virtual public IAlgTool 
{
=======
class ISCT_RodDecoder : virtual public IAlgTool {
>>>>>>> release/21.0.127
 public: 

  /** Creates the InterfaceID and interfaceID() method */
  DeclareInterfaceID(ISCT_RodDecoder, 1, 0);

  /** Destructor */
  virtual ~ISCT_RodDecoder() = default;

<<<<<<< HEAD
  /** Fill Collection method */
  virtual StatusCode fillCollection(const OFFLINE_FRAGMENTS_NAMESPACE::ROBFragment&,
                                    SCT_RDO_Container&,
                                    IDCInDetBSErrContainer& errs,
                                    const EventContext& ctx,
                                    const std::vector<IdentifierHash>* vecHash = nullptr) const = 0;
=======
  /** @brief Decode the rob data fragment and fill the collection SCT_RDO_Collection 
   *  with the RDO built by the makeRDO(..) method
   **/
  virtual StatusCode fillCollection(const OFFLINE_FRAGMENTS_NAMESPACE::ROBFragment*,SCT_RDO_Container*,std::vector<IdentifierHash>* vec=0) = 0;
>>>>>>> release/21.0.127
};

#endif //SCT_RAWDATABYTESTREAM_ISCT_RODDECODER_H
