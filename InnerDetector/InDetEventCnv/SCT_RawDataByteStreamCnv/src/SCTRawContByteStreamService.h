// -*- C++ -*-

/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

<<<<<<< HEAD:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamTool.h
#ifndef SCT_RAWDATABYTESTREAMCNV_SCTRAWCONTBYTESTREAMTOOL_H
#define SCT_RAWDATABYTESTREAMCNV_SCTRAWCONTBYTESTREAMTOOL_H

#include "AthenaBaseComps/AthAlgTool.h"
#include "SCT_RawDataByteStreamCnv/ISCTRawContByteStreamTool.h"

#include "ByteStreamCnvSvcBase/FullEventAssembler.h"
#include "ByteStreamCnvSvc/ByteStreamCnvSvc.h"

=======
#ifndef SCT_RAWDATABYTESTREAMCNV_SCTRAWCONTRAWEVENTSERVICE_H
#define SCT_RAWDATABYTESTREAMCNV_SCTRAWCONTRAWEVENTSERVICE_H
///STL
#include <stdint.h>
///Base classes 
#include "AthenaBaseComps/AthService.h"
#include "SCT_RawDataByteStreamCnv/ISCTRawContByteStreamService.h"
///Gaudi
>>>>>>> release/21.0.127:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamService.h
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"


class ISCT_RodEncoder;
class ISCT_CablingTool;
class SCT_ID;
<<<<<<< HEAD:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamTool.h
class SrcIdMap;

/** 
 * @class SCTRawContByteStreamTool
 * 
 * @brief Athena Algorithm Tool to provide conversion from SCT RDO container to ByteStream.
 *
 * Conversion from SCT RDO container to ByteStream, and fill it in RawEvent.
 *
 * The class inherits from AthAlgTool and ISCTRawContByteStreamTool.
 *
 * Contains convert method that maps ROD ID's to vectors of RDOs in those RODs, then
 * loops through the map, using RodEncoder to fill data for each ROD in turn.
 */
class SCTRawContByteStreamTool : public extends<AthAlgTool, ISCTRawContByteStreamTool> 
{
 public:

  /** Constructor */
  SCTRawContByteStreamTool(const std::string& type, const std::string& name, const IInterface* parent);
  
  /** Destructor */
  virtual ~SCTRawContByteStreamTool() = default;

  /** Initialize */
  virtual StatusCode initialize() override;

  /** Finalize */
  virtual StatusCode finalize() override;

  /** 
   * @brief Main Convert method 
   *
   * Maps ROD ID's to vectors of RDOs in those RODs, then loops through the map, 
   * using RodEncoder to fill data for each ROD in turn.
   *
   * @param sctRDOCont SCT RDO Container of Raw Data Collections.
   * */
  virtual StatusCode convert(const SCT_RDO_Container* sctRDOCont) const override;
  
 private:
  ServiceHandle<ByteStreamCnvSvc> m_byteStreamCnvSvc
  { this, "ByteStreamCnvSvc", "ByteStreamCnvSvc" };

  /** Algorithm Tool to decode ROB bytestream data into RDO. */ 
  ToolHandle<ISCT_RodEncoder> m_encoder{this, "Encoder", "SCT_RodEncoder", "SCT ROD Encoder for RDO to BS conversion"};
=======
namespace InDetDD {
  class SCT_DetectorManager;
}
class StoreGateSvc;

#include <string>


/** An AthService class to provide conversion from SCT RDO container
 *  to ByteStream, and fill it in RawEvent
 *  created:  Oct 25, 2002, by Hong Ma 
 *  requirements:   typedef for CONTAINER class method 
 *   StatusCode convert(CONTAINER* cont, RawEvent* re, MsgStream& log ); 
 */

class SCTRawContByteStreamService: virtual public ISCTRawContByteStreamService, virtual public AthService {
 public:

  // RawData type
  typedef SCT_RDORawData          RDO ;
  // Collection type 
  typedef InDetRawDataCollection< SCT_RDORawData > SCTRawCollection; 
  // Container type
  typedef SCT_RDO_Container       SCTRawContainer; 


  //! constructor
  SCTRawContByteStreamService( const std::string& name, ISvcLocator* svcloc) ;
  
  //! destructor 
  virtual ~SCTRawContByteStreamService() ;

  virtual StatusCode queryInterface( const InterfaceID& riid, void** ppvIf );
>>>>>>> release/21.0.127:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamService.h

  /** Providing mappings of online and offline identifiers and also serial numbers. */ 
  ToolHandle<ISCT_CablingTool> m_cabling{this, "SCT_CablingTool", "SCT_CablingTool", "Tool to retrieve SCT Cabling"};

<<<<<<< HEAD:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamTool.h
  /** Identifier helper class for the SCT subdetector that creates compact Identifier objects and 
      IdentifierHash or hash IDs. Also allows decoding of these IDs. */ 
  const SCT_ID* m_sctIDHelper{nullptr};
=======
  //! New convert method which makes use of the encoder class (as done for other detectors)
  StatusCode convert(SCT_RDO_Container* cont, RawEventWrite* re, MsgStream& log); 
  
 private: 
  
  ToolHandle<ISCT_RodEncoder> m_encoder;
  ServiceHandle<ISCT_CablingSvc> m_cabling;
  ServiceHandle<StoreGateSvc> m_detStore;
  const InDetDD::SCT_DetectorManager* m_sct_mgr;
  const SCT_ID*                m_sct_idHelper;
  unsigned short               m_RodBlockVersion;
  FullEventAssembler<SrcIdMap> m_fea; 
>>>>>>> release/21.0.127:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamService.h

  UnsignedShortProperty m_rodBlockVersion{this, "RodBlockVersion", 0};
};

<<<<<<< HEAD:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamTool.h
#endif // SCT_RAWDATABYTESTREAMCNV_SCTRAWCONTBYTESTREAMTOOL_H
=======
#endif
>>>>>>> release/21.0.127:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamService.h
