// -*- C++ -*-

/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

<<<<<<< HEAD
#ifndef SCT_RAWDATABYTESTREAMCNV_SCTRAWCONTBYTESTREAMCNV_H
#define SCT_RAWDATABYTESTREAMCNV_SCTRAWCONTBYTESTREAMCNV_H
=======
#ifndef SCT_RAWDATABYTESTREAMCNV_SCTRAWCONTRAWEVENTCNV_H
#define SCT_RAWDATABYTESTREAMCNV_SCTRAWCONTRAWEVENTCNV_H

// Gaudi
#include "GaudiKernel/Converter.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/MsgStream.h"

// C++ Standard Library
#include <string>
>>>>>>> release/21.0.127

#include "ByteStreamCnvSvcBase/IByteStreamEventAccess.h"
#include "ByteStreamCnvSvcBase/ByteStreamAddress.h"
#include "InDetRawData/InDetRawDataCLASS_DEF.h"
#include "AthenaBaseComps/AthConstConverter.h"

#include "GaudiKernel/ServiceHandle.h"

class DataObject;
<<<<<<< HEAD
class ISCTRawContByteStreamTool;
=======
class IByteStreamEventAccess;
class ISCTRawContByteStreamService;
>>>>>>> release/21.0.127

/**
 * @class SCTRawContByteStreamCnv
 *
 * @brief Converter for writing ByteStream from SCT Raw Data
 *
 * This will do the conversion on demand, triggered by the ByteStreamAddressProviderSvc. 
 * Since it is not possible to configure a Converter with Python Configurables, 
 * we use a tool (SCTRawContByteStreamTool) which in turn uses the lightweight 
 * SCT_RodEncoder class, to do the actual converting. 
 */
class SCTRawContByteStreamCnv : public AthConstConverter 
{
 public:

<<<<<<< HEAD
  /** Constructor */
  SCTRawContByteStreamCnv(ISvcLocator* svcLoc);
=======
/** the converter for writing BS from SCT Raw Data 
 * This will do the conversion on demand, triggered by the 
 * ByteStreamAddressProviderSvc. 
 * Since it is not possible to configure a Converter with
 * Python Configurables, we use a service (SCTRawContByteStreamService)
 * which in turn uses the  lightweight SCT_RodEncoder class, 
 * to do the actual converting. */
>>>>>>> release/21.0.127

  /** Destructor */
  virtual ~SCTRawContByteStreamCnv() = default;
  
  /** Initialize */
  virtual StatusCode initialize() override;

  /** Retrieve the class type of the data store the converter uses. */
  virtual long repSvcType() const override { return i_repSvcType(); }
  /** Storage type */
  static long storageType() { return ByteStreamAddress::storageType(); }
  /** Class ID */
  static const CLID& classID() { return ClassID_traits<SCT_RDO_Container>::ID(); }
  
  /** createObj method (not used!) */
  virtual StatusCode createObjConst(IOpaqueAddress*, DataObject*&) const override { return StatusCode::FAILURE; }

<<<<<<< HEAD
  /** 
   * @brief Method to convert SCT Raw Data into ByteStream
   *
   * Gets pointer to RawEvent, get ID container of SCT RDO.
   * Sets up opaque address for Storegate.
   *
   * Uses SCT RawContByteStreamTool to convert Raw Data to ByteStream.
   *
   * @param pDataObject Pointer to data object.
   * @param pOpaqueAddress Opaque address to object.
   */
  virtual StatusCode createRepConst(DataObject* pDataObject, IOpaqueAddress*& pOpaqueAddress) const override;

 private: 
=======
  /** Storage type and class ID */
  virtual long repSvcType() const { return ByteStream_StorageType;}
  static long storageType() { return ByteStream_StorageType; } 
  static const CLID& classID()    { return ClassID_traits<SCTRawContainer>::ID(); }
  
  /** initialize */
  virtual StatusCode initialize();
  
  /** create Obj is not used ! */
  virtual StatusCode createObj(IOpaqueAddress*, DataObject*&)
  { return StatusCode::FAILURE;}
>>>>>>> release/21.0.127

  /** Tool to do coversion from SCT RDO container to ByteStream */
  ToolHandle<ISCTRawContByteStreamTool> m_rawContByteStreamTool;

<<<<<<< HEAD
  /** Interface for accessing raw data */
=======
 private: 
  /** for BS infrastructure */
  ServiceHandle<ISCTRawContByteStreamService> m_service;
>>>>>>> release/21.0.127
  ServiceHandle<IByteStreamEventAccess> m_byteStreamEventAccess; 
};

#endif // SCT_RAWDATABYTESTREAMCNV_SCTRAWCONTBYTESTREAMCNV_H
