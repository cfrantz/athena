/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

<<<<<<< HEAD:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamTool.cxx
#include "SCTRawContByteStreamTool.h"
=======
/** header file */
#include "SCTRawContByteStreamService.h"
>>>>>>> release/21.0.127:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamService.cxx

#include "ByteStreamData/RawEvent.h" 
#include "ByteStreamCnvSvcBase/SrcIdMap.h" 
#include "eformat/SourceIdentifier.h"
#include "InDetIdentifier/SCT_ID.h"
#include "InDetReadoutGeometry/SiDetectorElement.h"
<<<<<<< HEAD:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamTool.cxx
#include "SCT_RawDataByteStreamCnv/ISCT_RodEncoder.h"
#include "SCT_Cabling/ISCT_CablingTool.h"
#include "StoreGate/ReadCondHandle.h"
=======
#include "InDetReadoutGeometry/SCT_DetectorManager.h"

/// ------------------------------------------------------------------------
/// contructor 

SCTRawContByteStreamService::SCTRawContByteStreamService
( const std::string& name, ISvcLocator* svcloc) :
     AthService(name, svcloc),
     m_encoder   ("SCT_RodEncoder", this),
     m_cabling   ("SCT_CablingSvc",name),
     m_detStore  ("StoreGateSvc/DetectorStore", name),
     m_sct_mgr(0),
     m_sct_idHelper(0)
{
  declareProperty("Encoder",m_encoder);
  declareProperty("RodBlockVersion",m_RodBlockVersion=0);
  declareProperty("CablingSvc",m_cabling);
  return;
}
>>>>>>> release/21.0.127:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamService.cxx

// Constructor 

<<<<<<< HEAD:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamTool.cxx
SCTRawContByteStreamTool::SCTRawContByteStreamTool(const std::string& type, const std::string& name,
                                                   const IInterface* parent) :
  base_class(type, name, parent)
=======
SCTRawContByteStreamService::~SCTRawContByteStreamService()
>>>>>>> release/21.0.127:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamService.cxx
{
}

<<<<<<< HEAD:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamTool.cxx
// Initialize

StatusCode SCTRawContByteStreamTool::initialize() 
{
  ATH_CHECK( m_byteStreamCnvSvc.retrieve() );
=======
StatusCode
SCTRawContByteStreamService::queryInterface( const InterfaceID& riid, void** ppvIf ) {
  if ( ISCTRawContByteStreamService::interfaceID().versionMatch(riid) ) {
    *ppvIf = dynamic_cast<ISCTRawContByteStreamService*>(this);
  } else {
    // Interface is not directly available : try out a base class
    return AthService::queryInterface(riid, ppvIf);
  }
  addRef();
  return StatusCode::SUCCESS;
}

/// ------------------------------------------------------------------------
/// initialize the tool

StatusCode
SCTRawContByteStreamService::initialize() {
  StatusCode sc = AthService::initialize(); 
  /** Retrieve id mapping  */
  if (m_cabling.retrieve().isFailure()) {
    msg(MSG::FATAL) << "Failed to retrieve service " << m_cabling << endmsg;
    return StatusCode::FAILURE;
  } else {
    msg(MSG::INFO) << "Retrieved service " << m_cabling << endmsg;
  }
  /** Retrieve detector manager */
  sc = m_detStore->retrieve(m_sct_mgr,"SCT"); 
  if (sc.isFailure()) {
    msg(MSG::FATAL) << "Cannot retrieve SCT_DetectorManager!" << endmsg;
    return StatusCode::FAILURE;
  }

  /** Get the SCT Helper */
  sc = m_detStore->retrieve(m_sct_idHelper,"SCT_ID"); 
  if (sc.isFailure()) {
    msg(MSG::FATAL) << "Cannot retrieve ID helper!" << endmsg;
    return StatusCode::FAILURE;
  }
>>>>>>> release/21.0.127:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamService.cxx

  // Retrieve ID mapping
  ATH_CHECK(m_cabling.retrieve());
  ATH_MSG_INFO("Retrieved service " << m_cabling);

<<<<<<< HEAD:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamTool.cxx
  // Get the SCT Helper
  ATH_CHECK(detStore()->retrieve(m_sctIDHelper, "SCT_ID"));

  return StatusCode::SUCCESS;
=======
  sc = m_encoder.retrieve();
  if (sc.isFailure())
    msg(MSG::FATAL) <<" Failed to retrieve tool "<< m_encoder <<endmsg;
  
  return sc;
>>>>>>> release/21.0.127:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamService.cxx
}

// Finalize

<<<<<<< HEAD:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamTool.cxx
StatusCode SCTRawContByteStreamTool::finalize() 
{
  return StatusCode::SUCCESS;
=======
StatusCode
SCTRawContByteStreamService::finalize() {
  m_fea.clear();   
  StatusCode sc = AthService::finalize(); 
  return sc;
>>>>>>> release/21.0.127:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamService.cxx
}

// Convert method

<<<<<<< HEAD:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamTool.cxx
StatusCode SCTRawContByteStreamTool::convert(const SCT_RDO_Container* sctRDOCont) const
{
  FullEventAssembler<SrcIdMap>* fullEventAssembler = nullptr;
  ATH_CHECK( m_byteStreamCnvSvc->getFullEventAssembler (fullEventAssembler,
                                                        "SCTRawCont") );
  FullEventAssembler<SrcIdMap>::RODDATA* rod;
  
  // Set ROD Minor version
  fullEventAssembler->setRodMinorVersion(m_rodBlockVersion);
  ATH_MSG_DEBUG(" Setting Minor Version Number to " << m_rodBlockVersion);
=======
StatusCode
SCTRawContByteStreamService::convert(SCT_RDO_Container* cont, RawEventWrite* re, MsgStream& log) {
  m_fea.clear();   
  FullEventAssembler<SrcIdMap>::RODDATA*  theROD ; 
  
  /** set ROD Minor version */
  m_fea.setRodMinorVersion(m_RodBlockVersion);
  if (msgLvl(MSG::DEBUG)) msg(MSG::DEBUG)<<" Setting Minor Version Number to "<<m_RodBlockVersion<<endmsg;
>>>>>>> release/21.0.127:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamService.cxx
  
  // Mapping between ROD IDs and the hits in that ROD
  std::map<uint32_t, std::vector<const SCT_RDORawData*>> rdoMap;

  // The following few lines are to make sure there is an entry in the rdoMap for 
  // every ROD, even if there are no hits in it for a particular event 
  // (as there might be ByteStream errors e.g. TimeOut errors).
  std::vector<std::uint32_t> listOfAllRODs;
  m_cabling->getAllRods(listOfAllRODs);
  for (std::uint32_t rod : listOfAllRODs) {
    rdoMap[rod].clear();
  }

<<<<<<< HEAD:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamTool.cxx
  // Loop over the collections in the SCT RDO container
  for (const InDetRawDataCollection<SCT_RDORawData>* sctRawColl : *sctRDOCont) {
    if (sctRawColl == nullptr) {
      ATH_MSG_WARNING("Null pointer to SCT RDO collection.");
=======
  /**loop over the collections in the SCT RDO container */
  SCTRawContainer::const_iterator it_coll     = cont->begin(); 
  SCTRawContainer::const_iterator it_coll_end = cont->end();
  
  for( ; it_coll!=it_coll_end;++it_coll) {
    const SCTRawCollection* coll = (*it_coll) ;
    if (coll == 0) {
      msg(MSG::WARNING) <<"Null pointer to SCT RDO collection."<<endmsg;
>>>>>>> release/21.0.127:InnerDetector/InDetEventCnv/SCT_RawDataByteStreamCnv/src/SCTRawContByteStreamService.cxx
      continue;
    } 
    else {
      // Collection ID
      Identifier idColl{sctRawColl->identify()};
      IdentifierHash idCollHash{m_sctIDHelper->wafer_hash(idColl)};
      uint32_t robid{m_cabling->getRobIdFromHash(idCollHash)};
      
      if (robid == 0) continue;
      
      // Building the ROD ID
      eformat::helper::SourceIdentifier srcIDROB{robid};
      eformat::helper::SourceIdentifier srcIDROD{srcIDROB.subdetector_id(), srcIDROB.module_id()};
      uint32_t rodid{srcIDROD.code()};
      
      // Loop over RDOs in the collection
      for (const SCT_RDORawData* rdo : *sctRawColl) {
        // Fill the ROD/RDO map
        rdoMap[rodid].push_back(rdo);
      }
    }
  }  // End loop over collections

  // Now encode data for each ROD in turn
  for (auto rodToRDOs : rdoMap) {
    rod = fullEventAssembler->getRodData(rodToRDOs.first); // Get ROD data address
    m_encoder->fillROD(*rod, rodToRDOs.first, rodToRDOs.second); // Encode ROD data
  }
  
  return StatusCode::SUCCESS;
}
