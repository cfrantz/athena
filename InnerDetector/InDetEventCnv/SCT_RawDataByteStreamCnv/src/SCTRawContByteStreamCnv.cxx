/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "SCTRawContByteStreamCnv.h"

#include "SCT_RawDataByteStreamCnv/ISCTRawContByteStreamTool.h"
#include "ByteStreamCnvSvcBase/ByteStreamAddress.h" 
#include "ByteStreamData/RawEvent.h" 
#include "ByteStreamCnvSvcBase/IByteStreamEventAccess.h"
<<<<<<< HEAD
=======
#include "SCT_RawDataByteStreamCnv/ISCTRawContByteStreamService.h"
>>>>>>> release/21.0.127

#include "AthenaBaseComps/AthCheckMacros.h"
#include "StoreGate/StoreGateSvc.h"

<<<<<<< HEAD
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/MsgStream.h"
=======
SCTRawContByteStreamCnv::SCTRawContByteStreamCnv(ISvcLocator* svcloc) :
  Converter(ByteStream_StorageType, classID(),svcloc),
  m_service              ("SCTRawContByteStreamService", "SCTRawContByteStreamCnv"),
  m_byteStreamEventAccess("ByteStreamCnvSvc","SCTRawContByteStreamCnv"),
  m_log                   (msgSvc(), "SCTRawContByteStreamCnv")
{}
 
/// ------------------------------------------------------
/// destructor
>>>>>>> release/21.0.127

// Constructor

SCTRawContByteStreamCnv::SCTRawContByteStreamCnv(ISvcLocator* svcLoc) :
  AthConstConverter(storageType(), classID(), svcLoc, "SCTRawContByteStreamCnv"),
  m_rawContByteStreamTool{"SCTRawContByteStreamTool"},
  m_byteStreamEventAccess{"ByteStreamCnvSvc", "SCTRawContByteStreamCnv"}
{
}

// Initialize

StatusCode SCTRawContByteStreamCnv::initialize()
{
<<<<<<< HEAD
  ATH_CHECK(AthConstConverter::initialize());
  ATH_MSG_DEBUG( " initialize " );

  // Retrieve ByteStreamCnvSvc
  ATH_CHECK(m_byteStreamEventAccess.retrieve());
  ATH_MSG_INFO( "Retrieved service " << m_byteStreamEventAccess );

  // Retrieve byte stream tool
  ATH_CHECK(m_rawContByteStreamTool.retrieve());
  ATH_MSG_INFO( "Retrieved tool " << m_rawContByteStreamTool );

  return StatusCode::SUCCESS;
=======
  StatusCode sc = Converter::initialize(); 
  if(StatusCode::SUCCESS!=sc) return sc; 

  m_log << MSG::DEBUG<< " initialize " <<endmsg; 

  /** Retrieve ByteStreamCnvSvc */
  if (m_byteStreamEventAccess.retrieve().isFailure()) {
    m_log << MSG::FATAL << "Failed to retrieve service " << m_byteStreamEventAccess << endmsg;
    return StatusCode::FAILURE;
  } else 
    m_log << MSG::INFO << "Retrieved service " << m_byteStreamEventAccess << endmsg;

  /** Retrieve byte stream Service */
  if (m_service.retrieve().isFailure()) {
    m_log << MSG::FATAL << "Failed to retrieve service " << m_service << endmsg;
    return StatusCode::FAILURE;
  } else 
    m_log << MSG::INFO << "Retrieved service " << m_service << endmsg;
   
  return StatusCode::SUCCESS; 
   
>>>>>>> release/21.0.127
}

// Method to create RawEvent fragments

StatusCode SCTRawContByteStreamCnv::createRepConst(DataObject* pDataObject, IOpaqueAddress*& pOpaqueAddress) const
{
<<<<<<< HEAD
  // Get IDC for SCT Raw Data
  SCT_RDO_Container* sctRDOCont{nullptr};
  StoreGateSvc::fromStorable(pDataObject, sctRDOCont);
  if (sctRDOCont == nullptr) {
    ATH_MSG_ERROR( " Can not cast to SCTRawContainer " );
=======
  
  /** get RawEvent pointer */
  RawEventWrite* re = m_byteStreamEventAccess->getRawEvent(); 
  
  /** get IDC for SCT Raw Data */
  SCT_RDO_Container* cont(0); 
  StoreGateSvc::fromStorable(pObj, cont); 
  if(!cont){
    m_log << MSG::ERROR << " Can not cast to SCTRawContainer " << endmsg ; 
    return StatusCode::FAILURE;    
  } 
  
  /** set up the IOpaqueAddress for Storegate */
  std::string nm = pObj->registry()->name(); 
  pAddr = new ByteStreamAddress(classID(),nm,""); 

  /** now use the service to do the conversion */
  StatusCode sc = m_service->convert(cont, re, m_log) ;
  if(sc.isFailure()){
    m_log << MSG::ERROR
	  << " Could not convert rdo with SCTRawContByteStreamSvc " << endmsg;
>>>>>>> release/21.0.127
    return StatusCode::FAILURE;
  }

  // Set up the IOpaqueAddress for Storegate
  std::string dataObjectName{pDataObject->registry()->name()};
  pOpaqueAddress = new ByteStreamAddress(classID(), dataObjectName, "");

  // Use the tool to do the conversion
  ATH_CHECK(m_rawContByteStreamTool->convert(sctRDOCont) );

  return StatusCode::SUCCESS;
}
