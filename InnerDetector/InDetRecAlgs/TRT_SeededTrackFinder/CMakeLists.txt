<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
################################################################################
# Package: TRT_SeededTrackFinder
################################################################################
>>>>>>> release/21.0.127

# Declare the package name:
atlas_subdir( TRT_SeededTrackFinder )

<<<<<<< HEAD
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/AthenaBaseComps
                          GaudiKernel
                          InnerDetector/InDetConditions/InDetBeamSpotService
                          InnerDetector/InDetRecTools/InDetRecToolInterfaces
                          Tracking/TrkEvent/TrkSegment
                          Tracking/TrkEvent/TrkTrack
                          Tracking/TrkExtrapolation/TrkExInterfaces
                          Tracking/TrkFitter/TrkFitterInterfaces
                          PRIVATE
                          Control/CxxUtils
                          InnerDetector/InDetRecEvent/InDetRIO_OnTrack
                          Tracking/TrkEvent/TrkEventPrimitives
                          Tracking/TrkEvent/TrkPseudoMeasurementOnTrack )

>>>>>>> release/21.0.127
# Component(s) in the package:
atlas_add_component( TRT_SeededTrackFinder
                     src/*.cxx
                     src/components/*.cxx
<<<<<<< HEAD
                     LINK_LIBRARIES AthenaBaseComps BeamSpotConditionsData CxxUtils GaudiKernel IRegionSelector InDetRIO_OnTrack InDetRecToolInterfaces RoiDescriptor SiSPSeededTrackFinderData TrkCaloClusterROI TrkEventPrimitives TrkEventUtils TrkExInterfaces TrkFitterInterfaces TrkPseudoMeasurementOnTrack TrkSegment TrkToolInterfaces TrkTrack )
=======
                     LINK_LIBRARIES AthenaBaseComps GaudiKernel InDetRecToolInterfaces TrkSegment TrkTrack TrkExInterfaces TrkFitterInterfaces CxxUtils InDetRIO_OnTrack TrkEventPrimitives TrkPseudoMeasurementOnTrack )

# Install files from the package:
atlas_install_headers( TRT_SeededTrackFinder )

>>>>>>> release/21.0.127
