/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/


#include "PixelCablingSvc.h"

#include "GaudiKernel/ServiceHandle.h"
#include "StoreGate/StoreGateSvc.h"

#include "InDetReadoutGeometry/SiDetectorElement.h"
#include "PixelReadoutGeometry/PixelModuleDesign.h"
#include "PixelReadoutGeometry/PixelDetectorManager.h"

//#define PIXEL_DEBUG

<<<<<<< HEAD
PixelCablingSvc::PixelCablingSvc(const std::string& name, ISvcLocator*svc):
  AthService(name,svc),
  m_detStore("DetectorStore", name),
  m_detManager(0),
  m_idHelper(0)
=======

//using namespace std;
//using eformat::helper::SourceIdentifier;

static unsigned int defaultColumnsPerFE_pix     = 18;   // number of columns per FE
static unsigned int defaultRowsPerFE_pix        = 164;  // number of rows per FE
static unsigned int defaultFEsPerHalfModule_pix = 8;    // number of FEs in a row
//static unsigned int defaultColumnsPerFE_IBL     = 80;
//static unsigned int defaultRowsPerFE_IBL        = 336;
//static unsigned int defaultFEsPerHalfModule_IBL = 2;

static const unsigned int defaultNumLayers = 4;      // number of barrel layers, incl IBL
static const unsigned int defaultNumDisks = 3;       // number of disk layers

////////////////////////
// constructor
////////////////////////

PixelCablingSvc::PixelCablingSvc(const std::string& name, ISvcLocator*svc) :
    AthService(name,svc),
    m_cablingTool("PixelFillCablingData"),
    m_detStore("DetectorStore", name),
    m_IBLParameterSvc("IBLParameterSvc",name),
    m_idHelper(0),
    m_cabling(new PixelCablingData),
    m_callback_calls(0),
    m_dataString(""),
    m_key("/PIXEL/ReadoutSpeed"),
    m_keyFEI4("/PIXEL/HitDiscCnfg"),
    m_keyCabling("/PIXEL/CablingMap"),
    m_dump_map_to_file(false),
    m_useIBLParameterSvc(true),
    m_IBLpresent(false),
    m_isHybrid(false),
    m_DBMpresent(false),
    m_dbm_columnsPerFE(0),
    m_dbm_rowsPerFE(0),
    m_dbm_FEsPerHalfModule(0),
    m_eta_module_offset(0)
{
    // "Final": use text file, "COOL": read from COOL
    declareProperty("MappingType", m_mappingType = "COOL");
    // Name of mapping file to use, if mappingType == Final
// STSTST    declareProperty("MappingFile", m_final_mapping_file = "Pixels_Atlas_IdMapping_2016.dat");
    declareProperty("MappingFile", m_final_mapping_file = "PixelCabling/Pixels_Atlas_IdMapping_2016.dat");
    // NOT USED
    declareProperty("Bandwidth", m_bandwidth = 0);
    // NOT USED
    declareProperty("Coral_Connectionstring", m_coraldbconnstring = "oracle://ATLAS_COOLPROD/ATLAS_COOLONL_PIXEL"); // used to configure CORAL based tool
    // NOT USED
    declareProperty("DictionaryTag", m_dictTag="PIXEL");
    // NOT USED
    declareProperty("ConnectivityTag", m_connTag="PIT-ALL-V39");
    // Folder name for FE-I4 hit discriminator configuration
    declareProperty("KeyFEI4", m_keyFEI4 = "/PIXEL/HitDiscCnfg");
    // Folder name for readout speed
    declareProperty("Key", m_key, "Key=/PIXEL/ReadoutSpeed");
    // Folder name for cabling map
    declareProperty("KeyCabling", m_keyCabling, "Key=/PIXEL/CablingMap");
    // Write out the cabling map to a text file
    declareProperty("DumpMapToFile", m_dump_map_to_file = false);
}

/*
////////////////////////
// Copy constructor
////////////////////////
PixelCablingSvc::PixelCablingSvc(const PixelCablingSvc &other, const std::string& name, ISvcLocator*svc) :
    AthService(name,svc),
    m_cablingTool("PixelFillCablingData"),
    m_detStore("DetectorStore", name),
    m_IBLParameterSvc("IBLParameterSvc",name),
    m_idHelper(0),
    m_cabling(0),
    m_callback_calls(0),
    m_dataString(""),
    m_key("/PIXEL/ReadoutSpeed"),
    m_keyFEI4("/PIXEL/HitDiscCnfg"),
    m_keyCabling("/PIXEL/CablingMap"),
    m_dump_map_to_file(false),
    m_useIBLParameterSvc(true),
    m_IBLpresent(false),
    m_isHybrid(false),
    m_DBMpresent(false),
    m_dbm_columnsPerFE(0),
    m_dbm_rowsPerFE(0),
    m_dbm_FEsPerHalfModule(0),
    m_eta_module_offset(0)
{
    // Copy everyzing
    m_mappingType = other.m_mappingType;
    m_final_mapping_file = other.m_final_mapping_file;
    m_bandwidth = other.m_bandwidth;
    m_coraldbconnstring = other.m_coraldbconnstring;
    m_dictTag = other.m_dictTag;
    m_connTag = other.m_connTag;
    m_key = other.m_key;
    m_keyFEI4 = other.m_keyFEI4;
    m_keyCabling = other.m_keyCabling;
    m_dump_map_to_file = other.m_dump_map_to_file;
}

////////////////////////
// Assignment operator
////////////////////////
PixelCablingSvc& PixelCablingSvc::operator= (const PixelCablingSvc &other) {

    if (&other != this) {
        m_mappingType = other.m_mappingType;
        m_final_mapping_file = other.m_final_mapping_file;
        m_bandwidth = other.m_bandwidth;
        m_coraldbconnstring = other.m_coraldbconnstring;
        m_dictTag = other.m_dictTag;
        m_connTag = other.m_connTag;
        m_key = other.m_key;
        m_keyFEI4 = other.m_keyFEI4;
        m_keyCabling = other.m_keyCabling;
        m_dump_map_to_file = other.m_dump_map_to_file;
    }
    return *this;
}
*/

////////////////////////
// destructor
////////////////////////
PixelCablingSvc::~PixelCablingSvc()
{
}


////////////////////////
// initialize
////////////////////////
StatusCode PixelCablingSvc::initialize( )
{
    StatusCode sc;
    msg(MSG::INFO) << "PixelCablingSvc::initialize" << endmsg;

    // Get an Identifier helper object
    sc = m_detStore.retrieve();
    if(!sc.isSuccess()){
        ATH_MSG_FATAL("Unable to retrieve detector store");
        return StatusCode::FAILURE;
    }
    else {
        msg(MSG::DEBUG) << "DetectorStore service found" << endmsg;
    }


    // Get the PixelID Helper
    if (m_detStore->retrieve(m_idHelper, "PixelID").isFailure()) {
        msg(MSG::FATAL) << "Could not get Pixel ID helper" << endmsg;
        return StatusCode::FAILURE;
    }

    // Get ToolSvc
    IToolSvc* toolSvc;
    if(StatusCode::SUCCESS != service("ToolSvc", toolSvc)) {
        msg(MSG::ERROR) << "Cannot get ToolSvc!" << endmsg;
        return StatusCode::FAILURE;
    }


    // Get IBLParameterSvc
    if (!m_useIBLParameterSvc) {
        msg(MSG::INFO) << "IBLParameterSvc not used" << endmsg;
    }
    else if (m_IBLParameterSvc.retrieve().isFailure()) {
        msg(MSG::FATAL) << "Could not retrieve IBLParameterSvc" << endmsg;
        return StatusCode::FAILURE;
    }


    // Get IBL and DBM properties from IBLParameterSvc

    // Set layout
    m_IBLpresent = m_IBLParameterSvc->containsIBL();
    m_isHybrid = m_IBLParameterSvc->contains3D();
    m_DBMpresent = m_IBLParameterSvc->containsDBM();

    // Get the values
    m_IBLParameterSvc->setCablingParameters(m_layer_columnsPerFE, m_layer_rowsPerFE, m_layer_FEsPerHalfModule,
                                            &m_dbm_columnsPerFE, &m_dbm_rowsPerFE, &m_dbm_FEsPerHalfModule);


    // Fill the columns/rows/FEsPerHalfModule vectors with remaining pixel values
    // m_layer_columns(rows)PerFE should be [IBL, PixLayer1, PixLayer2, PixLayer3]

    unsigned int numLayers = m_IBLpresent ? defaultNumLayers : (defaultNumLayers - 1);

    while (numLayers > m_layer_columnsPerFE.size()) {
        m_layer_columnsPerFE.push_back(defaultColumnsPerFE_pix);
    }
    while (numLayers > m_layer_rowsPerFE.size()) {
        m_layer_rowsPerFE.push_back(defaultRowsPerFE_pix);
    }
    std::vector<int> numFEs;
    numFEs.push_back(defaultFEsPerHalfModule_pix);
    while (numLayers > m_layer_FEsPerHalfModule.size()) {
        m_layer_FEsPerHalfModule.push_back(numFEs);
    }

    // Do the same for disks
    while (defaultNumDisks > m_disk_columnsPerFE.size()) {
        m_disk_columnsPerFE.push_back(defaultColumnsPerFE_pix);
    }
    while (defaultNumDisks > m_disk_rowsPerFE.size()) {
        m_disk_rowsPerFE.push_back(defaultRowsPerFE_pix);
    }
    while (defaultNumDisks > m_disk_FEsPerHalfModule.size()) {
        m_disk_FEsPerHalfModule.push_back(defaultFEsPerHalfModule_pix);
    }


    // The Eta_module value returned by PixelID::eta_module is a signed integer,
    // symmetric around z = 0. We need to offset is so that it starts at 0 (is unsigned)
    m_eta_module_offset = m_layer_FEsPerHalfModule[0].size()/2;


    // Print out all values
#ifdef PIXEL_DEBUG
    msg(MSG::DEBUG) << "-- PixelCablingSvc ------------------------------" << endmsg;
    msg(MSG::DEBUG) << "m_IBLpresent = " << m_IBLpresent << endmsg;
    msg(MSG::DEBUG) << "m_DBMpresent = " << m_DBMpresent << endmsg;
    msg(MSG::DEBUG) << "m_isHybrid = " << m_isHybrid << endmsg;
    msg(MSG::DEBUG) << "m_layer_columnsPerFE = " << m_layer_columnsPerFE << endmsg;
    msg(MSG::DEBUG) << "m_layer_rowsPerFE = " << m_layer_rowsPerFE << endmsg;
    msg(MSG::DEBUG) << "m_layer_FEsPerHalfModule = " << m_layer_FEsPerHalfModule << endmsg;
    msg(MSG::DEBUG) << "m_disk_columnsPerFE = " << m_disk_columnsPerFE << endmsg;
    msg(MSG::DEBUG) << "m_disk_rowsPerFE = " << m_disk_rowsPerFE << endmsg;
    msg(MSG::DEBUG) << "m_disk_FEsPerHalfModule = " << m_disk_FEsPerHalfModule << endmsg;
    msg(MSG::DEBUG) << "m_dbm_columnsPerFE = " << m_dbm_columnsPerFE << endmsg;
    msg(MSG::DEBUG) << "m_dbm_rowsPerFE = " << m_dbm_rowsPerFE << endmsg;
    msg(MSG::DEBUG) << "m_dbm_FEsPerHalfModule = " << m_dbm_FEsPerHalfModule << endmsg;
    msg(MSG::DEBUG) << "m_final_mapping_file = " << m_final_mapping_file << endmsg;
    msg(MSG::DEBUG) << "useMapFromOptions = " << useMapFromOptions << endmsg;
    msg(MSG::DEBUG) << "-------------------------------------------------" << endmsg;
#endif


    // Get the cabling tool
    if (m_cablingTool.retrieve().isFailure()) {
        ATH_MSG_ERROR("Cannot get PixelFillCablingData tool");
        return StatusCode::FAILURE;
    }
    else ATH_MSG_INFO("PixelFillCablingData tool retrieved");


    if ((m_mappingType != "COOL") && (m_mappingType != "Final")) {
        ATH_MSG_FATAL("Unknown PixelCablingSvc configuration: " << m_mappingType);
        return StatusCode::FAILURE;
    }

    if (m_mappingType != "COOL") 
      if (!m_cablingTool->fillMapFromFile(m_final_mapping_file,m_cabling.get())) {
        ATH_MSG_ERROR("Filling pixel cabling from file \"" << m_final_mapping_file << "\" failed");
        return StatusCode::FAILURE;
    }

    m_context = m_idHelper->wafer_context();


    // Register readout speed callback
    const DataHandle<AthenaAttributeList> attrlist;
    if (m_detStore->contains<AthenaAttributeList>(m_key)) {

        sc = m_detStore->regFcn(&IPixelCablingSvc::IOVCallBack,
                                dynamic_cast<IPixelCablingSvc*>(this),
                                attrlist, m_key);
        if (!sc.isSuccess()) {
            ATH_MSG_FATAL("Unable to register readoutspeed callback");
            return StatusCode::FAILURE;
        }
    }
    else {
        ATH_MSG_WARNING("Folder " << m_key << " not found, using default readoutspeed"
                        << " values (all modules at SINGLE_40)");
    }

    // Register cabling map callback
    if (m_mappingType == "COOL") {
        const DataHandle<AthenaAttributeList> attrlist_cabling;
        if (m_detStore->contains<AthenaAttributeList>(m_keyCabling)) {

            sc = m_detStore->regFcn(&IPixelCablingSvc::IOVCallBack,
                                    dynamic_cast<IPixelCablingSvc*>(this),
                                    attrlist_cabling, m_keyCabling);

            if (!sc.isSuccess()) {
                ATH_MSG_FATAL("Unable to register CablingMap callback");
                return StatusCode::FAILURE;
            }
        }
        else {
            ATH_MSG_WARNING("Folder " << m_keyCabling << " not found, exiting");
            return StatusCode::FAILURE;
        }
    }

    // Register hitdisccnfg callback
    if (m_IBLpresent) {
        const DataHandle<AthenaAttributeList> attrlist_hdc;
        if (m_detStore->contains<AthenaAttributeList>(m_keyFEI4)) {

            sc = m_detStore->regFcn(&IPixelCablingSvc::IOVCallBack,
                                    dynamic_cast<IPixelCablingSvc*>(this),
                                    attrlist_hdc, m_keyFEI4);

            // If regFcn fails even when folder is present -> abort
            if (!sc.isSuccess()) {
                ATH_MSG_FATAL("Unable to register HitDiscCnfg callback");
                return StatusCode::FAILURE;
            }
        }
        else {
            ATH_MSG_WARNING("Folder " << m_keyFEI4 << " not found, using default HitDiscCnfg"
                            << " values (all FEs at HitDiscCnfg = 3)");
        }
    }

    // Testing
    //ATH_MSG_INFO("DetStore: " << m_detStore->dump());


    return sc;
}

////////////////////////
// finalize
////////////////////////
StatusCode PixelCablingSvc::finalize()
>>>>>>> release/21.0.127
{
}

PixelCablingSvc::~PixelCablingSvc() { }

StatusCode PixelCablingSvc::initialize() {
  ATH_MSG_INFO("PixelCablingSvc::initialize()");

  ATH_CHECK(m_detStore.retrieve());

  ATH_CHECK(m_detStore->retrieve(m_detManager,"Pixel"));

  ATH_CHECK(m_detStore->retrieve(m_idHelper,"PixelID"));

  return StatusCode::SUCCESS;
}

StatusCode PixelCablingSvc::finalize() {
  ATH_MSG_INFO("PixelCablingSvc::finalize()");
  return StatusCode::SUCCESS;
}

StatusCode PixelCablingSvc::queryInterface(const InterfaceID &riid, void** ppvInterface) {
  if (IPixelCablingSvc::interfaceID().versionMatch(riid)) {
    *ppvInterface = dynamic_cast<IPixelCablingSvc*>(this);
  }
  else {
    // Interface is not directly available : try out a base class
    return AthService::queryInterface(riid, ppvInterface);
  }
  addRef();
  return StatusCode::SUCCESS;
}

Identifier PixelCablingSvc::getPixelIdfromHash(IdentifierHash offlineIdHash, uint32_t FE, uint32_t row, uint32_t column) {
  return getPixelId(m_idHelper->wafer_id(offlineIdHash), FE, row, column);
}

Identifier PixelCablingSvc::getPixelId(Identifier offlineId, uint32_t FE, uint32_t row, uint32_t column) {

  const InDetDD::SiDetectorElement *element = m_detManager->getDetectorElement(offlineId);
  const InDetDD::PixelModuleDesign *p_design = static_cast<const InDetDD::PixelModuleDesign*>(&element->design());

  unsigned int columnsPerFE     = p_design->columnsPerCircuit();
  unsigned int FEsPerHalfModule = p_design->numberOfCircuits();
  unsigned int rowsPerFE = 0;
  int column_row_offset = 0;
  if (p_design->getReadoutTechnology()==InDetDD::PixelModuleDesign::FEI4) {
    rowsPerFE    = p_design->rowsPerCircuit();
    column_row_offset = -1;
  }
  else if (p_design->getReadoutTechnology()==InDetDD::PixelModuleDesign::FEI3) {
    rowsPerFE    = p_design->rowsPerCircuit()/2+4;  // normal + ganged
  }

  // ---------------------
  // Check input sanity
  // ---------------------
  // Identify the module type
  moduletype thisModule = getModuleType(offlineId);

<<<<<<< HEAD
  // Correct row, column
  row = row + column_row_offset;
  column = column + column_row_offset;

  if (row>=rowsPerFE || column>=columnsPerFE || FE>=((thisModule==IBL_PLANAR || thisModule==IBL_3D || thisModule==DBM) ? FEsPerHalfModule:2*FEsPerHalfModule)) {
    ATH_MSG_DEBUG("Illegal pixel requested OfflineID: " << std::hex << offlineId << std::dec << " FE: " << FE << " row: " << row << " column: " << column);
    ATH_MSG_DEBUG("Limits are: FE < " << ((thisModule==IBL_PLANAR || thisModule==IBL_3D || thisModule == DBM) ? FEsPerHalfModule : 2*FEsPerHalfModule) << ", row < " << rowsPerFE << ", column < " << columnsPerFE);
    return Identifier(); // illegal Identifier, standardized for PixelRodDecoder
  }
=======
}

////////////////////////
// getPixelIdfromHash - get the pixelId from the offlineIdHash, FE, row and column
////////////////////////
Identifier PixelCablingSvc::getPixelIdfromHash(IdentifierHash offlineIdHash, uint32_t FE, uint32_t row, uint32_t column)
{
    return PixelCablingSvc::getPixelId(m_idHelper->wafer_id(offlineIdHash), FE, row, column);
}

////////////////////////
// getPixelId - get the pixelId from the offlineId, FE, row and column
////////////////////////
Identifier PixelCablingSvc::getPixelId(Identifier offlineId, uint32_t FE, uint32_t row, uint32_t column)
{
    // Identify the module type
    moduletype thisModule = getModuleType(offlineId);

    unsigned int columnsPerFE, rowsPerFE, FEsPerHalfModule;
    unsigned int phi_index, eta_index;

    unsigned int layer_disk = abs(m_idHelper->layer_disk(offlineId));
    unsigned int eta_module = 0;    // Only IBL modules need this

    // Declare row/column offset for FE-I4 vs FE-I3
    int column_row_offset = 0;


    // ---------------------
    // Set module properties
    // ---------------------

    switch (thisModule) {

    case NONE:
        msg(MSG::ERROR) << "Module type not identified -- cannot build pixelId" << endmsg;
        return Identifier();

    case DBM:
        columnsPerFE = m_dbm_columnsPerFE;
        rowsPerFE = m_dbm_rowsPerFE;
        FEsPerHalfModule = m_dbm_FEsPerHalfModule;
        column_row_offset = -1;
        break;

    case PIX_ENDCAP:
        FEsPerHalfModule = m_disk_FEsPerHalfModule[layer_disk];
        columnsPerFE =  m_disk_columnsPerFE[layer_disk];
        rowsPerFE = m_disk_rowsPerFE[layer_disk];
        break;

    case IBL:
        column_row_offset = -1;
        eta_module = m_idHelper->eta_module(offlineId) + m_eta_module_offset;
        if (eta_module >= m_layer_FEsPerHalfModule[0].size()) {
            msg(MSG::WARNING) << "getPixelId: Eta_module out of range (eta_module = " << eta_module
                              << ", expected number of modules per stave = " << m_layer_FEsPerHalfModule[0].size() << ")" << endmsg;
            return Identifier();
        }
        // fall through

    default:        // PIX_BARREL + IBL
        columnsPerFE = m_layer_columnsPerFE[layer_disk];
        rowsPerFE = m_layer_rowsPerFE[layer_disk];
        FEsPerHalfModule = m_layer_FEsPerHalfModule[layer_disk][eta_module];
        break;
    }


    // ---------------------
    // Check input sanity
    // ---------------------

    // Correct row, column
    row = row + column_row_offset;
    column = column + column_row_offset;

    if (row >= rowsPerFE || column >= columnsPerFE ||
            FE >= ((thisModule == IBL || thisModule == DBM) ? FEsPerHalfModule : 2*FEsPerHalfModule)) {
        msg(MSG::DEBUG) << "Illegal pixel requested OfflineID: " << std::hex << offlineId << std::dec
                        << " FE: " << FE << " row: " << row << " column: " << column << endmsg;
        msg(MSG::DEBUG) << "Limits are: FE < " << ((thisModule == IBL || thisModule == DBM) ? FEsPerHalfModule : 2*FEsPerHalfModule)
                       << ", row < " << rowsPerFE << ", column < " << columnsPerFE << endmsg;
        return Identifier(); // illegal Identifier, standardized for PixelRodDecoder
    }

    // ---------------------
    // Convert row/column to eta/phi indices
    // ---------------------

    switch (thisModule) {
>>>>>>> release/21.0.127

  // ---------------------
  // Convert row/column to eta/phi indices
  // ---------------------
  unsigned int phi_index, eta_index;
  switch (thisModule) {
    case DBM:
      eta_index = rowsPerFE-1-row;
      if (m_idHelper->barrel_ec(offlineId)>0) {     // A side (pos. eta)
        phi_index = column;
      } 
      else {                                        // C side
        phi_index = columnsPerFE-1-column;
      }
      break;

    case IBL_PLANAR:
      phi_index = rowsPerFE-1-row;
      eta_index = FE*columnsPerFE+column;
      break;

    case IBL_3D:
      phi_index = rowsPerFE-1-row;
      eta_index = FE*columnsPerFE+column;
      break;

    default:    // pixels
<<<<<<< HEAD
      if (FE<FEsPerHalfModule) {
        phi_index = ((2*rowsPerFE)-1)-row;
        eta_index = ((columnsPerFE*FEsPerHalfModule)-1)-(column+(FE*columnsPerFE));
      } 
      else {
        phi_index = row;
        eta_index = ((FE-FEsPerHalfModule)*columnsPerFE)+column;
      }
      if (thisModule==PIX_ENDCAP) {
        // Swap phi_index for even endcap modules
        if ((m_idHelper->phi_module(offlineId))%2==0) {
          phi_index = 2*rowsPerFE-phi_index-1;
          ATH_MSG_DEBUG("Even disk module found, phi module: " << m_idHelper->phi_module(offlineId) << " swapped phi index to : " << phi_index);
=======
        if (FE < FEsPerHalfModule) {
            phi_index = ((2*rowsPerFE)-1)-row;
            eta_index = ((columnsPerFE*FEsPerHalfModule)-1)-(column+(FE*columnsPerFE));
        } else {
            phi_index = row;
            eta_index = ((FE-FEsPerHalfModule)*columnsPerFE)+column;
        }
        if (thisModule == PIX_ENDCAP) {
            // Swap phi_index for even endcap modules
            if ((m_idHelper->phi_module(offlineId))%2 == 0) {
                phi_index = 2*rowsPerFE-phi_index-1;
                if (msgLvl(MSG::DEBUG)) msg(MSG::DEBUG) << "Even disk module found, phi module: " << m_idHelper->phi_module(offlineId)
                                << " swapped phi index to : " << phi_index << endmsg;
            }
>>>>>>> release/21.0.127
        }
      }
      break;
  }

  Identifier pixelId = m_idHelper->pixel_id(offlineId,phi_index,eta_index);

#ifdef PIXEL_DEBUG
<<<<<<< HEAD
  unsigned int eta_index_max = m_idHelper->eta_index_max(offlineId);
  unsigned int phi_index_max = m_idHelper->phi_index_max(offlineId);
  if (eta_index>eta_index_max) {
    ATH_MSG_DEBUG("Error! eta_index: " << eta_index << " > eta_index_max: " << eta_index_max);
  }
  if (phi_index>phi_index_max) {
    ATH_MSG_DEBUG("Error! phi_index: " << phi_index << " > phi_index_max: " << phi_index_max);
  }
  //consistency check - to be removed to speed up
  uint32_t check_FE = getFE(&pixelId,offlineId);
  uint32_t check_row = getRow(&pixelId,offlineId) + column_row_offset;
  uint32_t check_column = getColumn(&pixelId,offlineId) + column_row_offset;
  if (check_FE!=FE || check_row!=row || check_column!=column) {
    ATH_MSG_WARNING("identify OfflineID: 0x" << std::hex << offlineId << std::dec << " FE: " << FE << " row: " << row << " column: " << column << " unequal to:");
    ATH_MSG_WARNING("identify PixelID: 0x" << std::hex << pixelId << std::dec << " FE: " << check_FE << " row: " << check_row << " column: " << check_column);
  }
=======
    unsigned int eta_index_max =  m_idHelper->eta_index_max(offlineId);
    unsigned int phi_index_max =  m_idHelper->phi_index_max(offlineId);
    if (eta_index > eta_index_max) msg(MSG::WARNING) << "Error! eta_index: " << eta_index << " > eta_index_max: " << eta_index_max << endmsg;
    if (phi_index > phi_index_max) msg(MSG::WARNING) << "Error! phi_index: " << phi_index << " > phi_index_max: " << phi_index_max << endmsg;
    //consistency check - to be removed to speed up
    uint32_t check_FE = getFE(&pixelId,offlineId);
    uint32_t check_row = getRow(&pixelId,offlineId) + column_row_offset;
    uint32_t check_column = getColumn(&pixelId,offlineId) + column_row_offset;
    if (check_FE != FE || check_row != row || check_column != column) {
        msg(MSG::WARNING) << "identify OfflineID: 0x" << std::hex << offlineId << std::dec << " FE: " << FE << " row: " << row << " column: " << column << " unequal to:" << endmsg;
        msg(MSG::WARNING) << "identify PixelID: 0x" << std::hex << pixelId << std::dec << " FE: " << check_FE << " row: " << check_row << " column: " << check_column << endmsg;
    }
>>>>>>> release/21.0.127
#endif

  return pixelId;
}

uint32_t PixelCablingSvc::getFE(Identifier *pixelId, Identifier offlineId) {

  const InDetDD::SiDetectorElement *element = m_detManager->getDetectorElement(offlineId);
  const InDetDD::PixelModuleDesign *p_design = static_cast<const InDetDD::PixelModuleDesign*>(&element->design());

  unsigned int columnsPerFE     = p_design->columnsPerCircuit();
  unsigned int FEsPerHalfModule = p_design->numberOfCircuits();
  unsigned int rowsPerFE = 0;
  if (p_design->getReadoutTechnology()==InDetDD::PixelModuleDesign::FEI4) {
    rowsPerFE    = p_design->rowsPerCircuit();
  }
  else if (p_design->getReadoutTechnology()==InDetDD::PixelModuleDesign::FEI3) {
    rowsPerFE    = p_design->rowsPerCircuit()/2+4;  // normal + ganged
  }

  // ---------------------
  // Set module properties
  // ---------------------
  moduletype thisModule = getModuleType(offlineId);
  unsigned int FE;
  unsigned int phi_index = m_idHelper->phi_index(*pixelId);
  int eta_index = m_idHelper->eta_index(*pixelId);

  switch (thisModule) {

    case DBM:
      FE = 0;     // simple as that
      return FE;
      break;

    case PIX_ENDCAP:
<<<<<<< HEAD
      // Swap phi_index for even endcap modules
      if ((m_idHelper->phi_module(offlineId))%2==0) {
        phi_index = 2*rowsPerFE-phi_index-1;
      }
      break;
=======
        FEsPerHalfModule = m_disk_FEsPerHalfModule[layer_disk];
        columnsPerFE =  m_disk_columnsPerFE[layer_disk];
        rowsPerFE = m_disk_rowsPerFE[layer_disk];

        // Swap phi_index for even endcap modules
        if ((m_idHelper->phi_module(offlineId))%2 == 0) {
            phi_index = 2*rowsPerFE-phi_index-1;
        }

        break;

    case IBL:
        eta_module = m_idHelper->eta_module(offlineId) + m_eta_module_offset; //offset by 10 to start counting from 0
        if (eta_module >= m_layer_FEsPerHalfModule[0].size()) {
            msg(MSG::WARNING) << "getFE: Eta_module out of range (eta_module = " << eta_module
                              << ", expected number of modules per stave = " << m_layer_FEsPerHalfModule[0].size() << ")" << endmsg;
            return 0xffffffff;
        }
        // fall through
>>>>>>> release/21.0.127

    default:        // PIX_BARREL + IBL
      break;
  }


  // ---------------------
  // Compute FE number
  // ---------------------
  if (phi_index>=rowsPerFE) {
    FE = (int)((FEsPerHalfModule-1)-(eta_index/columnsPerFE));
  } 
  else {
    FE = (int)(eta_index/columnsPerFE)+FEsPerHalfModule;
  }

  // For IBL, the above returns FE number in range [2,3] (planar), or [1] (3D sensors).
  // Change that to be [0,1] (planar) and [0] (3D) for consistency
  if (thisModule==IBL_PLANAR || thisModule==IBL_3D) { FE = FE - FEsPerHalfModule; }

  return FE;
}

uint32_t PixelCablingSvc::getColumn(Identifier *pixelId, Identifier offlineId) {

  const InDetDD::SiDetectorElement *element = m_detManager->getDetectorElement(offlineId);
  const InDetDD::PixelModuleDesign *p_design = static_cast<const InDetDD::PixelModuleDesign*>(&element->design());

  unsigned int columnsPerFE     = p_design->columnsPerCircuit();
  unsigned int rowsPerFE = 0;
  int column_offset = 0;
  if (p_design->getReadoutTechnology()==InDetDD::PixelModuleDesign::FEI4) {
    rowsPerFE    = p_design->rowsPerCircuit();
    column_offset = 1;
  }
  else if (p_design->getReadoutTechnology()==InDetDD::PixelModuleDesign::FEI3) {
    rowsPerFE    = p_design->rowsPerCircuit()/2+4;  // normal + ganged
  }

  moduletype thisModule = getModuleType(offlineId);

  unsigned int phi_index = m_idHelper->phi_index(*pixelId);
  int eta_index = m_idHelper->eta_index(*pixelId);

  // ---------------------
  // Set module properties
  // ---------------------
  switch (thisModule) {

    case DBM:
      break;

    case PIX_ENDCAP:
<<<<<<< HEAD
      // Swap phi_index for even endcap modules
      if ((m_idHelper->phi_module(offlineId))%2==0) {
        phi_index = 2*rowsPerFE-phi_index-1;
      }
      break;
=======
        columnsPerFE =  m_disk_columnsPerFE[layer_disk];
        rowsPerFE = m_disk_rowsPerFE[layer_disk];

        // Swap phi_index for even endcap modules
        if ((m_idHelper->phi_module(offlineId))%2 == 0) {
            phi_index = 2*rowsPerFE-phi_index-1;
        }

        break;

    case IBL:
        column_offset = 1;
        eta_module = m_idHelper->eta_module(offlineId) + m_eta_module_offset; //offset by 10 to start counting from 0
        if (eta_module >= m_layer_FEsPerHalfModule[0].size()) {
            msg(MSG::WARNING) << "getColumn: Eta_module out of range (eta_module = " << eta_module
                              << ", expected number of modules per stave = " << m_layer_FEsPerHalfModule[0].size() << ")" << endmsg;
            return 0xffffffff;
        }
        // fall through
>>>>>>> release/21.0.127

    default:        // PIX_BARREL + IBL
      break;
  }

  // ---------------------
  // Convert eta index to column number
  // ---------------------
  int column;

  // DBM (column <-> phi_index)
  if (thisModule==DBM) {
    if (m_idHelper->barrel_ec(offlineId)>0) {
      column = m_idHelper->phi_index(*pixelId);     // A side
    }
    else {
<<<<<<< HEAD
      column = columnsPerFE-m_idHelper->phi_index(*pixelId)-1;  // C side
    }
  }
  // Pixels, IBL
  else {
    if ((phi_index>=rowsPerFE)) {
      column = (columnsPerFE-1)-(eta_index%columnsPerFE);
    } 
    else {
      column = eta_index%columnsPerFE;
=======
        if ((phi_index >= rowsPerFE)) {
            column = (columnsPerFE-1)-(eta_index%columnsPerFE);
        } else {
            column = eta_index%columnsPerFE;
        }
    }


    // ---------------------
    // Check output sanity
    // ---------------------
    if (column >= (int)columnsPerFE) {
        msg(MSG::WARNING) << "Computed column number exceeds maximum value: col = " << column + column_offset
                          << " (max = " << columnsPerFE << ")" <<  endmsg;
        return 0xffffffff;
>>>>>>> release/21.0.127
    }
  }

  // ---------------------
  // Check output sanity
  // ---------------------
  if (column>=(int)columnsPerFE) {
    ATH_MSG_ERROR("Computed column number exceeds maximum value: col = " << column + column_offset << " (max = " << columnsPerFE << ")");
    return 0xffffffff;
  }

  return column + column_offset;
}

uint32_t PixelCablingSvc::getRow(Identifier *pixelId, Identifier offlineId) {

  const InDetDD::SiDetectorElement *element = m_detManager->getDetectorElement(offlineId);
  const InDetDD::PixelModuleDesign *p_design = static_cast<const InDetDD::PixelModuleDesign*>(&element->design());

  unsigned int rowsPerFE = 0;
  int row_offset = 0;
  if (p_design->getReadoutTechnology()==InDetDD::PixelModuleDesign::FEI4) {
    rowsPerFE    = p_design->rowsPerCircuit();
    row_offset = 1;
  }
  else if (p_design->getReadoutTechnology()==InDetDD::PixelModuleDesign::FEI3) {
    rowsPerFE    = p_design->rowsPerCircuit()/2+4;  // normal + ganged
  }

  moduletype thisModule = getModuleType(offlineId);

  unsigned int phi_index = m_idHelper->phi_index(*pixelId);

  // ---------------------
  // Set module properties
  // ---------------------
  switch (thisModule) {

    case DBM:
      break;

    case PIX_ENDCAP:
<<<<<<< HEAD
      // Swap phi_index for even endcap modules
      if ((m_idHelper->phi_module(offlineId))%2==0) {
        phi_index = 2*rowsPerFE-phi_index-1;
      }
      break;
=======
        rowsPerFE = m_disk_rowsPerFE[layer_disk];

        // Swap phi_index for even endcap modules
        if ((m_idHelper->phi_module(offlineId))%2 == 0) {
            phi_index = 2*rowsPerFE-phi_index-1;
        }

        break;

    case IBL:
        row_offset = 1;
        eta_module = m_idHelper->eta_module(offlineId) + m_eta_module_offset; //offset by 10 to start counting from 0
        if (eta_module >= m_layer_FEsPerHalfModule[0].size()) {
            msg(MSG::WARNING) << "getRow: Eta_module out of range (eta_module = " << eta_module
                              << ", expected number of modules per stave = " << m_layer_FEsPerHalfModule[0].size() << ")" << endmsg;
            return 0xffffffff;
        }
        // fall through
>>>>>>> release/21.0.127

    default:        // PIX_BARREL + IBL
      break;
  }

  // ---------------------
  // Convert phi index to row number
  // ---------------------
  int row;

  switch (thisModule) {

    case DBM:
<<<<<<< HEAD
      // Row <-> eta_index
      row = rowsPerFE-m_idHelper->eta_index(*pixelId)-1;
      break;
=======
        // Row <-> eta_index
        row = rowsPerFE - m_idHelper->eta_index(*pixelId) - 1;
        break;

    case IBL:
        row = rowsPerFE - 1 - phi_index;
        break;

    default:    // Pixels
        if (phi_index >= rowsPerFE) {
            row = ((2*rowsPerFE)-1)-phi_index;
        } else {
            row = phi_index;
        }
        break;
    }

    // ---------------------
    // Check output sanity
    // ---------------------
    if (row >= (int)rowsPerFE) {
        msg(MSG::WARNING) << "Computed row number exceeds maximum value: row = " << row + row_offset
                          << "(max = " << rowsPerFE << ")" << endmsg;
        return 0xffffffff;
    }
>>>>>>> release/21.0.127

    case IBL_PLANAR:
      row = rowsPerFE-1-phi_index;
      break;

    case IBL_3D:
      row = rowsPerFE-1-phi_index;
      break;

    default:    // Pixels
      if (phi_index>=rowsPerFE) {
        row = ((2*rowsPerFE)-1)-phi_index;
      } 
      else {
        row = phi_index;
      }
      break;
  }

  // ---------------------
  // Check output sanity
  // ---------------------
  if (row >= (int)rowsPerFE) {
    ATH_MSG_ERROR("Computed row number exceeds maximum value: row = " << row + row_offset << "(max = " << rowsPerFE << ")");
    return 0xffffffff;
  }
  return row + row_offset;
}

unsigned int PixelCablingSvc::getLocalFEI4(const uint32_t fe, const uint64_t onlineId) {
  unsigned int linknum40 = (onlineId>>24) & 0xFF;
  unsigned int linknum80 = (onlineId>>32) & 0xFF;

<<<<<<< HEAD
  if (fe==linknum40) { 
    return 0;
  }
  else if (fe==linknum80) {
    return 1;
  }
  else {
    ATH_MSG_ERROR("Error in retrieving local FE-I4 number: linknumber " << fe << " not found in onlineID " << std::hex << onlineId);
  }
  return 0xF;
=======
////////////////////////
// getFEwrtSlink
// Function to get the number of an FE-I4 within an Slink,
// i.e. pos. value in the range [0,7], corresponding to
// the 'nnn' bits of a fragment header. To get the number
// of an FE within a module, use getFE.
////////////////////////
uint32_t PixelCablingSvc::getFEwrtSlink(Identifier *pixelId) {

    unsigned int nnn = 99;
    Identifier offlineId = m_idHelper->wafer_id(*pixelId);
    uint64_t onlineId = getOnlineId(offlineId);
    uint32_t linkNum = (onlineId >> 24) & 0xFFFF;
    unsigned int localFE = getFE(pixelId, offlineId);   // FE number within module, [0,1]. Increases with increasing eta_index

    nnn = (linkNum >> (localFE * 8)) & 0xF;

    // Check for errors
    if (nnn > 7) msg(MSG::WARNING) << "Error in the identification of the FE-I4 w.r.t. Slink" << endmsg;

    return nnn;
}


////////////////////////
// getAllRods - get all Rods from the mapping
////////////////////////

std::vector<uint32_t>& PixelCablingSvc::getAllRods()
{
    return m_cabling->get_allRods();
}

////////////////////////
// getAllRobs - get all ROBs from the mapping
////////////////////////
std::vector<uint32_t>& PixelCablingSvc::getAllRobs()
{
    return m_cabling->get_allRobs();
>>>>>>> release/21.0.127
}

PixelCablingSvc::moduletype PixelCablingSvc::getModuleType(const Identifier& id) {
  moduletype isType = NONE;

  const Identifier wafer_id = m_idHelper->wafer_id(id);
  const InDetDD::SiDetectorElement *element = m_detManager->getDetectorElement(wafer_id);
  const InDetDD::PixelModuleDesign *p_design = static_cast<const InDetDD::PixelModuleDesign*>(&element->design());

  if (p_design->getReadoutTechnology()==InDetDD::PixelModuleDesign::FEI4) {
    if (p_design->numberOfCircuits()==2) { isType = IBL_PLANAR; }
    else                                 { isType = IBL_3D; }

    if (m_idHelper->is_dbm(id)) { isType = DBM; }
  }
  else if (p_design->getReadoutTechnology()==InDetDD::PixelModuleDesign::FEI3) {
    if      (abs(m_idHelper->barrel_ec(id))==0) { isType = PIX_BARREL; }
    else if (abs(m_idHelper->barrel_ec(id))==2) { isType = PIX_ENDCAP; }
    else                                        { isType = NONE; }
  }
  else {
    isType = NONE;
  }
  return isType;
}

PixelCablingSvc::pixeltype PixelCablingSvc::getPixelType(const Identifier& id) {
  pixeltype isType = NORMAL;

  const Identifier wafer_id = m_idHelper->wafer_id(id);
  Identifier pixelId = id;
  Identifier offlineId = m_idHelper->wafer_id(pixelId);
  int col = getColumn(&pixelId, offlineId);
  int row = getRow(&pixelId, offlineId);

  const InDetDD::SiDetectorElement *element = m_detManager->getDetectorElement(wafer_id);
  const InDetDD::PixelModuleDesign *p_design = static_cast<const InDetDD::PixelModuleDesign*>(&element->design());

  if (p_design->getReadoutTechnology()==InDetDD::PixelModuleDesign::FEI4) {
    if (p_design->numberOfCircuits()==2) {       // IBL planar
      if (col==0 || col==p_design->columnsPerCircuit()-1) {   // column edge =0,79
        isType = LONG;
      }
      else {
        isType = NORMAL;
      }
    }
    else {
      isType = NORMAL;
    }
  }
  else {
    if (col>0 && col<p_design->columnsPerCircuit()-1) {
      if (row>=p_design->rowsPerCircuit()/2-1-6-1 && row<=p_design->rowsPerCircuit()/2-1) {
        if ((row-(p_design->rowsPerCircuit()/2-1-6)+1)%2+1==1) {
          isType = LONG;
        }
        else if ((row-(p_design->rowsPerCircuit()/2-1-6)+1)%2+1==2) {
          isType = GANGED;
        }
        else {
          isType = NORMAL;
        }
      }
      else {
        isType = NORMAL;
      }
    } 
    else if (col==0 || col==p_design->columnsPerCircuit()-1) {
      if (row>=p_design->rowsPerCircuit()/2-1-6-1) {
        isType = GANGED;
      }
      else {
        isType = LONG;
      }
    }
<<<<<<< HEAD
=======


    return StatusCode::SUCCESS;
}




////////////////////////
// getOnlineIdFromRobId
// Wrapper function to PixelCablingData::getOnlineIdFromRobId
////////////////////////
uint64_t PixelCablingSvc::getOnlineIdFromRobId(const uint32_t robId, const uint32_t link_module) {
    return m_cabling->getOnlineIdFromRobId(robId, link_module);
}


////////////////////////
// getLocalFEI4
// returns local FE number on IBL / DBM module
////////////////////////
unsigned int PixelCablingSvc::getLocalFEI4(const uint32_t fe, const uint64_t onlineId) {

    unsigned int linknum40 = (onlineId >> 24) & 0xFF;
    unsigned int linknum80 = (onlineId >> 32) & 0xFF;

    if (fe == linknum40) return 0;
    else if (fe == linknum80) return 1;
    else msg(MSG::WARNING) << "Error in retrieving local FE-I4 number: linknumber " << fe
                           << " not found in onlineID " << std::hex << onlineId << endmsg;
    return 0xF;
}

////////////////////////
// isIBL - determine if a given robId or pixelId corresponds to an IBL module or not
////////////////////////
bool PixelCablingSvc::isIBL(const uint32_t robId) {
    // IBL subdetector ID is 0x14
    if (((robId >> 16) & 0xFF) == 0x14) return true;
    else return false;
}

bool PixelCablingSvc::isIBL(const Identifier& id) {
    // If IBL is present, an IBL pixelId will have barrel_ec = layer_disk = 0
    if (m_IBLpresent && m_idHelper->barrel_ec(id) == 0 && m_idHelper->layer_disk(id) == 0) return true;
    else return false;
}

////////////////////////
// isDBM - determine if a given robId or pixelId corresponds to a DBM module or not
////////////////////////
bool PixelCablingSvc::isDBM(const uint32_t robId) {
    // DBM subdetector ID is 0x15
    if (((robId >> 16) & 0xFF) == 0x15) return true;
    else return false;
}

bool PixelCablingSvc::isDBM(const Identifier& id) {
    return m_idHelper->is_dbm(id);
}

////////////////////////
// getModuleType
////////////////////////
PixelCablingSvc::moduletype PixelCablingSvc::getModuleType(const Identifier& id) {

    moduletype isType = NONE;

    if (isIBL(id)) isType = IBL;
    else if (isDBM(id)) isType = DBM;
>>>>>>> release/21.0.127
    else {
      ATH_MSG_WARNING("Pixel Type : the col number should be 0-" << p_design->columnsPerCircuit() << ", not " <<col);
      isType = NORMAL;
    }
  }
  return isType;
}

