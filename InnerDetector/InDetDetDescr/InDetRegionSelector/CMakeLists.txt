# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( InDetRegionSelector )

# Component(s) in the package:
atlas_add_component( InDetRegionSelector
                     src/*.cxx
                     src/components/*.cxx
<<<<<<< HEAD
                     LINK_LIBRARIES AthenaBaseComps RegSelLUT GaudiKernel PixelConditionsData SCT_CablingLib Identifier InDetIdentifier InDetReadoutGeometry IRegionSelector StoreGateLib TRT_ReadoutGeometry TRT_CablingLib )
=======
                     INCLUDE_DIRS ${TDAQ-COMMON_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
                     LINK_LIBRARIES ${TDAQ-COMMON_LIBRARIES} ${CLHEP_LIBRARIES} AthenaBaseComps RegSelLUT GaudiKernel SCT_CablingLib Identifier InDetIdentifier InDetReadoutGeometry PixelCablingLib )
>>>>>>> release/21.0.127

# Install files from the package:
atlas_install_joboptions( share/*.py )
