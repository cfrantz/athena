<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
################################################################################
# Package: SiCombinatorialTrackFinderTool_xk
################################################################################
>>>>>>> release/21.0.127

# Declare the package name:
atlas_subdir( SiCombinatorialTrackFinderTool_xk )

<<<<<<< HEAD
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/AthenaBaseComps
                          GaudiKernel
                          InnerDetector/InDetConditions/InDetConditionsSummaryService
                          InnerDetector/InDetDetDescr/InDetReadoutGeometry
                          InnerDetector/InDetRecEvent/InDetPrepRawData
                          InnerDetector/InDetRecTools/InDetRecToolInterfaces
                          MagneticField/MagFieldInterfaces
                          Tracking/TrkDetDescr/TrkGeometry
                          Tracking/TrkEvent/TrkEventPrimitives
                          Tracking/TrkEvent/TrkPatternParameters
                          Tracking/TrkEvent/TrkTrack
                          Tracking/TrkExtrapolation/TrkExInterfaces
                          Tracking/TrkTools/TrkToolInterfaces
                          PRIVATE
                          Event/EventInfo
                          InnerDetector/InDetRecEvent/InDetRIO_OnTrack
                          Tracking/TrkDetDescr/TrkSurfaces
                          Tracking/TrkEvent/TrkMaterialOnTrack
                          Tracking/TrkEvent/TrkMeasurementBase
                          Tracking/TrkEvent/TrkRIO_OnTrack )

# External dependencies:
find_package( Boost COMPONENTS filesystem thread system )

>>>>>>> release/21.0.127
# Component(s) in the package:
atlas_add_component( SiCombinatorialTrackFinderTool_xk
                     src/*.cxx
                     src/components/*.cxx
<<<<<<< HEAD
                     LINK_LIBRARIES AthenaBaseComps GaudiKernel InDetPrepRawData InDetReadoutGeometry InDetRecToolInterfaces MagFieldConditions SiSPSeededTrackFinderData StoreGateLib TrkExInterfaces TrkGeometry TrkMeasurementBase TrkToolInterfaces TrkTrack )
=======
                     INCLUDE_DIRS ${Boost_INCLUDE_DIRS}
                     LINK_LIBRARIES ${Boost_LIBRARIES} AthenaBaseComps GaudiKernel InDetReadoutGeometry InDetPrepRawData InDetRecToolInterfaces MagFieldInterfaces TrkGeometry TrkEventPrimitives TrkPatternParameters TrkTrack TrkExInterfaces TrkToolInterfaces EventInfo InDetRIO_OnTrack TrkSurfaces TrkMaterialOnTrack TrkMeasurementBase TrkRIO_OnTrack )

# Install files from the package:
atlas_install_headers( SiCombinatorialTrackFinderTool_xk )

>>>>>>> release/21.0.127
