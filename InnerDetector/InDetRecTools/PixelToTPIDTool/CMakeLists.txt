# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( PixelToTPIDTool )

<<<<<<< HEAD
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Control/AthenaBaseComps
   Control/AthenaKernel
   Control/StoreGate
   GaudiKernel
   Tracking/TrkEvent/TrkEventPrimitives
   PRIVATE
   Database/AthenaPOOL/AthenaPoolUtilities
   Database/RegistrationServices
   DetectorDescription/Identifier
   Event/EventInfo
   InnerDetector/InDetConditions/PixelConditionsServices
   InnerDetector/InDetDetDescr/InDetIdentifier
   InnerDetector/InDetDetDescr/PixelGeoModel
   InnerDetector/InDetRecEvent/InDetRIO_OnTrack
   Reconstruction/Particle
   Tools/PathResolver
   Tracking/TrkDetDescr/TrkSurfaces
   Tracking/TrkEvent/TrkMeasurementBase
   Tracking/TrkEvent/TrkParameters
   Tracking/TrkEvent/TrkRIO_OnTrack
   Tracking/TrkEvent/TrkTrack
   Tracking/TrkEvent/TrkTrackSummary
   Tracking/TrkTools/TrkToolInterfaces )

>>>>>>> release/21.0.127
# External dependencies:
find_package( CLHEP )

# Component(s) in the package:
atlas_add_component( PixelToTPIDTool
  PixelToTPIDTool/*.h  src/*.cxx src/components/*.cxx
<<<<<<< HEAD
  INCLUDE_DIRS ${CLHEP_INCLUDE_DIRS}
  LINK_LIBRARIES ${CLHEP_LIBRARIES}
  AthenaBaseComps StoreGateLib GaudiKernel TrkEventPrimitives
  AthenaPoolUtilities Identifier InDetIdentifier InDetRIO_OnTrack
  PathResolver TrkSurfaces TrkMeasurementBase TrkParameters
  TrkRIO_OnTrack TrkTrack TrkToolInterfaces PixelGeoModelLib PixelConditionsData )
=======
  INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${CORAL_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
  LINK_LIBRARIES ${ROOT_LIBRARIES} ${CORAL_LIBRARIES} ${CLHEP_LIBRARIES}
  AthenaBaseComps AthenaKernel StoreGateLib GaudiKernel TrkEventPrimitives
  AthenaPoolUtilities Identifier EventInfo InDetIdentifier InDetRIO_OnTrack
  Particle PathResolver TrkSurfaces TrkMeasurementBase TrkParameters
  TrkRIO_OnTrack TrkTrack TrkTrackSummary TrkToolInterfaces PixelGeoModelLib )
>>>>>>> release/21.0.127

# Install files from the package:
atlas_install_runtime( share/*.txt )
