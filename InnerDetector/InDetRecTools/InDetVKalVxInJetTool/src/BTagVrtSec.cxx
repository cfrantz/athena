/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

// Header include
#include "InDetVKalVxInJetTool/InDetVKalVxInJetTool.h"
#include "GeoPrimitives/GeoPrimitivesHelpers.h"
//-------------------------------------------------
// Other stuff
#include  "AnalysisUtils/AnalysisMisc.h"
#include  "TrkToolInterfaces/ITrackSummaryTool.h"
#include  "TrkVKalVrtFitter/TrkVKalVrtFitter.h"

#include "TProfile.h"
#include "TH2D.h"
#include  "TMath.h"

///----------------------------------------------------------------------------------------
///  getVrtSec returns the vector results with the following
///   1) Vertex mass
///   2) Vertex/jet energy fraction
///   3) Number of initially selected 2tr vertices
///   4) Number of selected for vertexing tracks in jet 
///   5) Number of track in secondary vertex
///   6) 3D SV-PV significance with sign
///   7) Jet energy used in (2) calculation 
///   8) Minimal distance between vertex and any material layer
///   9) Transverse vertex/jet energy fraction. Jet pT independent.
///   10) "Product" variable
///   11) "Boost" variable
///
///  Author:  Vadim Kostyukhin (vadim.kostyukhin@cern.ch)
///---------------------------------------------------------------------------------------- 
namespace InDet{

<<<<<<< HEAD
  float median(std::vector<float> &Vec){
    int N=Vec.size();
    if(N==1) return Vec[0];
    if(N==2) return (Vec[0]+Vec[1])/2.;
    if(N==3) return Vec[1];
    if(N>3){
      std::vector<float> tmp(Vec);
      int N05m=(N-1)/2, N05p=N/2;
      //std::sort(tmp.begin(),tmp.end());  //can use nth_element instead of completely sorting, it's quicker
      if(N05m==N05p){ 
         std::nth_element(tmp.begin(),tmp.begin()+N05m,tmp.end());
         return tmp[N05m];
      } else { 
         std::nth_element(tmp.begin(),tmp.begin()+N05m,tmp.end());
         std::nth_element(tmp.begin(),tmp.begin()+N05p,tmp.end());
         return (tmp[N05m]+tmp[N05p])/2.;
=======
  //inline double cutNDepNorm(int N, double Prb){return 1.41421356237*TMath::ErfInverse(1.-sqrt(2.*Prb/(N*N-N)));}
  inline double cutNDepNorm(int N, double Prb){return TMath::ChisquareQuantile(1.-sqrt(2.*Prb/(N*N-N)),2.);}

  xAOD::Vertex* InDetVKalVxInJetTool::GetVrtSec(const std::vector<const Rec::TrackParticle*>& InpTrk,
                                                const xAOD::Vertex                          & PrimVrt,
	                                        const TLorentzVector                        & JetDir,
                                                std::vector<double>                         & Results,
                                                std::vector<const Rec::TrackParticle*> & ListSecondTracks,
                                                std::vector<const Rec::TrackParticle*> & TrkFromV0) 
  const
  {

      if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG) << "GetVrtSec() called with Rec::TrackParticle=" <<InpTrk.size()<< endmsg;
   
      std::vector<const Rec::TrackParticle*> SelectedTracks,SelectedTracksRelax;
      SelectedTracks.clear(); SelectedTracksRelax.clear();
      ListSecondTracks.clear();
      Results.clear();        

      m_NRefTrk=0;
      if( InpTrk.size() < 2 ) { return 0;} // 0,1 track => nothing to do!


      int NPVParticle = SelGoodTrkParticle( InpTrk, PrimVrt, JetDir, SelectedTracks);

      long int NTracks = (int) (SelectedTracks.size());
      if(m_FillHist){m_hb_ntrkjet->Fill( (double) NTracks, m_w_1); }
      if( NTracks < 2 ) { return 0;} // 0,1 selected track => nothing to do!

      if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG) << "Number of selected tracks inside jet= " <<NTracks << endmsg;
      
      SelGoodTrkParticleRelax( InpTrk, PrimVrt, JetDir, SelectedTracksRelax);
      TLorentzVector MomentumJet = TotalMom(GetPerigeeVector(SelectedTracksRelax));
      if(m_FillHist){m_hb_jmom->Fill( MomentumJet.E(), m_w_1); }


//--------------------------------------------------------------------------------------------	 
//                    Initial Rec::TrackParticle list ready


      std::vector<const Rec::TrackParticle*>  TracksForFit;
      //std::vector<double> InpMass; for(int i=0; i<NTracks; i++){InpMass.push_back(m_massPi);}
      std::vector<double> InpMass(NTracks,m_massPi);

      xAOD::Vertex xaodPrimVrt; xaodPrimVrt.setPosition(PrimVrt.position());
                                xaodPrimVrt.setCovariancePosition(PrimVrt.covariancePosition());
      Select2TrVrt(SelectedTracks, TracksForFit, xaodPrimVrt, JetDir, InpMass, 
	                           TrkFromV0, ListSecondTracks);
      m_WorkArray->m_Incomp.clear();  // Not needed for single vertex version
//
//--- Cleaning
// 
      if( TrkFromV0.size() > 1) {
        RemoveDoubleEntries(TrkFromV0);
        AnalysisUtils::Sort::pT (&TrkFromV0);
      }
//---
      std::vector<const Rec::TrackParticle*> saveSecondTracks(ListSecondTracks);
      double Vrt2TrackNumber = (double) ListSecondTracks.size()/2.;
      RemoveDoubleEntries(ListSecondTracks);
      AnalysisUtils::Sort::pT (&ListSecondTracks);
//--Number of 2tr vertices where each track is used
      std::vector<int> combCount(ListSecondTracks.size());
      for(int tk=0;tk<(int)ListSecondTracks.size(); tk++){
        combCount[tk]=std::count(saveSecondTracks.begin(),saveSecondTracks.end(),ListSecondTracks[tk]);
      }
//---
      if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<" Found different tracks in pairs="<< ListSecondTracks.size()<<endmsg;
      if(ListSecondTracks.size() < 2 ) { return 0;} // Less than 2 tracks left

//
//-----------------------------------------------------------------------------------------------------
//            Secondary track list is ready
//            Now common vertex fit
//
      std::vector<const Rec::TrackParticle*>::const_iterator   i_ntrk, i_found;
      Amg::Vector3D          FitVertex;
      std::vector<double> ErrorMatrix;
      std::vector< std::vector<double> > TrkAtVrt; 
      TLorentzVector    Momentum;
      double Signif3D, Signif3DP=0, Signif3DS=0;
      std::vector<double> Impact,ImpactError;

      double Chi2 =  FitCommonVrt( ListSecondTracks,combCount, xaodPrimVrt, JetDir, InpMass, FitVertex, ErrorMatrix, Momentum, TrkAtVrt);
//
      if( Chi2 < 0) { return 0; }      // Vertex not reconstructed
//
// Check jet tracks not in secondary vertex
      std::vector<const Rec::TrackParticle*> AdditionalTracks;
      VrtVrtDist(PrimVrt, FitVertex, ErrorMatrix, Signif3D);
      if(Signif3D>8.){
       for (i_ntrk = SelectedTracks.begin(); i_ntrk < SelectedTracks.end(); ++i_ntrk) {
         i_found = find( ListSecondTracks.begin(), ListSecondTracks.end(), (*i_ntrk));
	 if( i_found != ListSecondTracks.end() ) continue;
         Signif3DS = m_fitSvc->VKalGetImpact((*i_ntrk), FitVertex         , 1, Impact, ImpactError);
         if( Signif3DS > 10.) continue;
         Signif3DP = m_fitSvc->VKalGetImpact((*i_ntrk), PrimVrt.position(), 1, Impact, ImpactError);
         if(m_FillHist){ m_hb_diffPS->Fill( Signif3DP-Signif3DS, m_w_1); }
	 if(Signif3DP-Signif3DS>1.0) AdditionalTracks.push_back((*i_ntrk));
       }
      }
//
// Add found tracks and refit
      if( AdditionalTracks.size() > 0){
        for (i_ntrk = AdditionalTracks.begin(); i_ntrk < AdditionalTracks.end(); ++i_ntrk)
                      ListSecondTracks.push_back((*i_ntrk));
        std::vector<int> tmpCount(ListSecondTracks.size(),1);
        Chi2 =  FitCommonVrt( ListSecondTracks, tmpCount, xaodPrimVrt, JetDir, InpMass, FitVertex, ErrorMatrix, Momentum, TrkAtVrt);
        if( Chi2 < 0) { return 0; }      // Vertex not reconstructed
      }
//
//  Saving of results
//
// VK 15.10.2007 Wrong idea. Kills high Pt jets 
// VK 12.02.2008 Idea correct, implementation was wrong!!! Tracks must have BL hits only if vertex is inside BL!! 
      if( ListSecondTracks.size()==2 ){         // If there are 2 only tracks
        int Charge=0, tCnt=0;
	int BLshared=0;
	int PXshared=0;
        std::vector<const Rec::TrackParticle*>::const_iterator   i_ntrk;
        for (i_ntrk = ListSecondTracks.begin(); i_ntrk < ListSecondTracks.end(); ++i_ntrk) {
            Charge +=  (int) (*i_ntrk)->charge();
            const Trk::TrackSummary* testSum = (*i_ntrk)->trackSummary();
            if(testSum){  BLshared   += testSum->get(Trk::numberOfBLayerSharedHits);
                          PXshared   += testSum->get(Trk::numberOfPixelSharedHits); }
            tCnt++;
        }
        if(m_useVertexCleaning){
          if(!Check2TrVertexInPixel(ListSecondTracks[0],ListSecondTracks[1],FitVertex,Signif3D)) return 0;
          double xDif=FitVertex.x()-m_XlayerB, yDif=FitVertex.y()-m_YlayerB ; 
          double Dist2D=sqrt(xDif*xDif+yDif*yDif);
          if     (Dist2D < m_RlayerB-m_SVResolutionR) {       // Inside B-layer
            if(m_FillHist){ if(Charge){m_hb_totmass2T1->Fill(Momentum.M(),m_w_1);}else{m_hb_totmass2T0->Fill(Momentum.M(),m_w_1);} }
            if(m_FillHist){ m_hb_blshared->Fill((float)BLshared,m_w_1); }
            //if(BLshared>m_CutSharedHits) return 0;
          } else if(Dist2D > m_RlayerB+m_SVResolutionR) {       //Outside b-layer
            if(m_FillHist){ m_hb_pxshared->Fill((float)PXshared,m_w_1); }
            //if(PXshared>m_CutSharedHits) return 0;
          }
        }   // end of 2tr vertex cleaning code
//
        if( !Charge && fabs(Momentum.M()-m_massK0)<15. ) {       // Final rejection of K0
	  TrkFromV0.push_back(ListSecondTracks[0]);
	  TrkFromV0.push_back(ListSecondTracks[1]);
          if( TrkFromV0.size() > 1) {
	     RemoveDoubleEntries(TrkFromV0);
             AnalysisUtils::Sort::pT (&TrkFromV0);
          }
          return 0;
        }
//-- Protection against fake vertices far from interaction point 
	if(NPVParticle<1)NPVParticle=1;
        double vvdist3D=VrtVrtDist(PrimVrt, FitVertex, ErrorMatrix, Signif3D);
        double t3Dimp1= m_fitSvc->VKalGetImpact(ListSecondTracks[0], PrimVrt.position(), 1, Impact, ImpactError)/fabs(TrkAtVrt[0][2]);
        double t3Dimp2= m_fitSvc->VKalGetImpact(ListSecondTracks[1], PrimVrt.position(), 1, Impact, ImpactError)/fabs(TrkAtVrt[1][2]);
	double selVar=(t3Dimp1<t3Dimp2 ? t3Dimp1 : t3Dimp2)/sqrt((double)NPVParticle)/vvdist3D/500.;
        if(m_FillHist){ m_hb_tr2SelVar->Fill( selVar , m_w_1); }
	if(selVar<m_AntiFake2trVrtCut)return 0;
        if(m_FillHist){ m_hb_totmass2T2->Fill(Momentum.M(),m_w_1); }
      }


      double JetVrtDir =
             JetDir.Px()*(FitVertex.x()-PrimVrt.x())
           + JetDir.Py()*(FitVertex.y()-PrimVrt.y())
 	   + JetDir.Pz()*(FitVertex.z()-PrimVrt.z());
      if(  m_getNegativeTag )
         { if( JetVrtDir>0. )   return 0; }
      else if( m_getNegativeTail )
         { ; }
      else 
         { if( JetVrtDir<0. ) return 0; } 

 
      double xvt=FitVertex.x(); double yvt=FitVertex.y();
      double Dist2DBP=sqrt( (xvt-m_Xbeampipe)*(xvt-m_Xbeampipe) + (yvt-m_Ybeampipe)*(yvt-m_Ybeampipe) ); 
      double Dist2DBL=sqrt( (xvt-m_XlayerB)*(xvt-m_XlayerB) + (yvt-m_YlayerB)*(yvt-m_YlayerB) ); 
      double Dist2DL1=sqrt( (xvt-m_Xlayer1)*(xvt-m_Xlayer1) + (yvt-m_Ylayer1)*(yvt-m_Ylayer1) );
      double Dist2DL2=sqrt( (xvt-m_Xlayer2)*(xvt-m_Xlayer2) + (yvt-m_Ylayer2)*(yvt-m_Ylayer2) );
      double minDstMat=39.9;
      minDstMat=TMath::Min(minDstMat,fabs(Dist2DBP-m_Rbeampipe));
      minDstMat=TMath::Min(minDstMat,fabs(Dist2DBL-m_RlayerB));
      minDstMat=TMath::Min(minDstMat,fabs(Dist2DL1-m_Rlayer1));
      minDstMat=TMath::Min(minDstMat,fabs(Dist2DL2-m_Rlayer2));
      if(m_existIBL) minDstMat=TMath::Min(minDstMat,fabs(Dist2DL2-m_Rlayer3));  // 4-layer pixel detector

      VrtVrtDist(PrimVrt, FitVertex, ErrorMatrix, Signif3D);
      if(JetVrtDir < 0) Signif3D = -Signif3D;

      Amg::Vector3D DirForPt( FitVertex.x()-PrimVrt.x(),
                              FitVertex.y()-PrimVrt.y(),
		              FitVertex.z()-PrimVrt.z());
      if( m_MassType == 3 ) Results.push_back( TotalTVMom(DirForPt, GetPerigeeVector(ListSecondTracks))); 
      if( m_MassType == 2 ) Results.push_back(TotalTMom(GetPerigeeVector(ListSecondTracks))*1.15); 
      if( m_MassType == 1 ) Results.push_back(Momentum.M());        //1st

      double eRatio = Momentum.E()/MomentumJet.E(); 
      Results.push_back(  eRatio<0.99999 ? eRatio : 0.99999);      //2nd
      Results.push_back(Vrt2TrackNumber);                          //3rd
      Results.push_back((double)NTracks);                          //4th
      Results.push_back((double)ListSecondTracks.size());          //5th
      Results.push_back(Signif3D);                                 //6th
      Results.push_back(MomentumJet.E());                          //7th
      Results.push_back(minDstMat);                                //8th
      double nRatio = Momentum.Et(JetDir.Vect())/sqrt(MomentumJet.Perp());   nRatio /= (nRatio+4.); 
      Results.push_back( nRatio );                                 //9th   New transverse energy ration
      Results.push_back((Momentum.M()-2.*m_massPi)*eRatio/m_massB);           //10th   "Product" variable
      Results.push_back((Momentum.Pt()/Momentum.M())*(m_massB/JetDir.Pt()) ); //11th   "Boost" variable

      if(m_FillHist){
          m_hb_r2dc->Fill( FitVertex.perp(), m_w_1);
          m_hb_mom->Fill( MomentumJet.E(), m_w_1);     
          m_hb_ratio->Fill( Results[1], m_w_1);
          m_hb_totmass->Fill( Results[0], m_w_1);
          m_hb_nvrt2->Fill( Results[2], m_w_1);
          m_hb_sig3DTot->Fill( Signif3D, m_w_1);
          // Find highest track Pt with respect to jet direction
          double trackPt, trackPtMax=0.;
          for (int tr=0; tr<(int)ListSecondTracks.size(); tr++) {
            trackPt=pTvsDir(Amg::Vector3D(JetDir.Vect().X(),JetDir.Vect().Y(),JetDir.Vect().Z()) , TrkAtVrt[tr]);
	    if(trackPt>trackPtMax)trackPtMax=trackPt;
          }
          m_hb_trkPtMax->Fill( trackPtMax, m_w_1);
          m_pr_effVrt->Fill((float)m_NRefTrk,1.);              
	  m_pr_effVrtEta->Fill( JetDir.Eta(),1.);
>>>>>>> release/21.0.127
      }
    }
    return 0.;
  }
  //
  // Probability-like 2-track B-vertex score function based on track scores.
  // It is distributed approximetely flat in [0,1]
  inline float getVrtScore(int i, int j, std::vector<std::vector<float>> & trkScore){
     if(i==j)return 0.;
     float res = trkScore.at(i)[0] *  trkScore.at(j)[0];
     res  *=    (1.-trkScore[i][1])*(1.-trkScore[j][1]);
     res  *=    (1.-trkScore[i][2])*(1.-trkScore[j][2]);
     return res;
  }

  const double c_vrtBCMassLimit=5500.;  // Mass limit to consider a vertex not coming from B,C-decays


<<<<<<< HEAD
=======
  xAOD::Vertex* InDetVKalVxInJetTool::tryPseudoVertex(const std::vector<const xAOD::TrackParticle*>& SelectedTracks,
                                                      const xAOD::Vertex                           & PrimVrt,
                                                      const TLorentzVector                         & JetDir,
                                                      const TLorentzVector                         & TrkJet,
						      const int                                    & nTrkLead,
	                                              std::vector<double>                          & Results)
  const
  {
//---------------First try jet axis+track
    if((int)SelectedTracks.size()<nTrkLead)return 0;
    //-----------------------------------------
    TLorentzVector sumSelTrk(0.,0.,0.,0.);  //Tracks coming from any SV

    Amg::Vector3D                           FitVertex;
    std::vector<double>                     ErrorMatrix;
    std::vector< std::vector<double> >      TrkAtVrt; 
    TLorentzVector                          Momentum;
    std::vector<double>                     Chi2PerTrk;
    long int              tChrg=0;
    double                Chi2=0.;
    std::vector<const xAOD::TrackParticle*> tTrkForFit(2,0);
    std::vector<float> tmpCov(15,0.); tmpCov[0]=1e-4; tmpCov[2]=4.e-4; tmpCov[5]=1.e-4; tmpCov[9]=1.e-4; tmpCov[14]=1.e-12; 
    StatusCode scode; scode.setChecked();  
    std::vector<const xAOD::TrackParticle*> reducedTrkSet(SelectedTracks.begin(),SelectedTracks.begin()+nTrkLead);
    double maxImp=RemoveNegImpact(reducedTrkSet,PrimVrt,JetDir,m_pseudoSigCut);
    if(reducedTrkSet.size()==0) return 0; 
    if(reducedTrkSet.size()==1 && maxImp<m_pseudoSigCut+1.)return 0;
    if(m_FillHist)m_hb_rawVrtN->Fill(reducedTrkSet.size(),1.);
//
//------ 
    int sel1T=-1; double selDST=0.;
    if(reducedTrkSet.size()>0){ 
       m_fitSvc->setApproximateVertex(PrimVrt.x(), PrimVrt.y(), PrimVrt.z()); 
       xAOD::TrackParticle *tmpBTP=new xAOD::TrackParticle(); tmpBTP->makePrivateStore();
       tmpBTP->setDefiningParameters(0., 0., JetDir.Phi(), JetDir.Theta(), sin(JetDir.Theta())/2.e5);   // Pt=200GeV track
       tmpBTP->setParametersOrigin(PrimVrt.x(),PrimVrt.y(),PrimVrt.z());
       tmpBTP->setDefiningParametersCovMatrixVec(tmpCov);
       tTrkForFit[1]=tmpBTP; 
       TLorentzVector sumB(0.,0.,0.,0.);
       int nvFitted=0;
       for(int it=0; it<(int)reducedTrkSet.size(); it++){
          tTrkForFit[0]=reducedTrkSet[it];
	  if(tTrkForFit[0]->pt()<2000.)continue;
	  scode=VKalVrtFitBase( tTrkForFit, FitVertex, Momentum, tChrg, ErrorMatrix, Chi2PerTrk, TrkAtVrt, Chi2);  nvFitted++;
	  if(scode.isFailure() || Chi2>6.)continue;
          if(FitVertex.perp()>reducedTrkSet[it]->radiusOfFirstHit())continue; 
          //double dSVPV=ProjPosT(FitVertex-PrimVrt.position(),JetDir);
	  double Signif3D; VrtVrtDist(PrimVrt, FitVertex, ErrorMatrix, Signif3D);
          if(FitVertex.perp()>m_Rbeampipe && Signif3D<2.)  continue;  // Cleaning of material interactions
   	  if(m_FillHist)m_hb_DST_JetTrkSV->Fill(Signif3D,1.);
          if(tTrkForFit[0]->pt()<5.e4){
            if(!Check2TrVertexInPixel(tTrkForFit[0],tTrkForFit[0],FitVertex,Signif3D)) continue;
	  }
          sumSelTrk += MomAtVrt(TrkAtVrt[0]) ; sumB += MomAtVrt(TrkAtVrt[1]) ;
	  if(Signif3D>selDST){ sel1T=it; selDST=Signif3D;} 
       }
       if(sumSelTrk.Pt()>0. && sel1T>=0 ){
	  Results.resize(4);
	  double pt1=sumSelTrk.Pt(sumB.Vect()); double E1=sqrt(pt1*pt1+sumSelTrk.M2()); 
          Results[0]=pt1+E1;                      //Invariant mass
          Results[1]=sumSelTrk.Pt()/TrkJet.Pt();  //Ratio
          Results[2]=0.;                          //Should be
          Results[3]=reducedTrkSet.size();        //Found leading tracks with high positive impact
          tTrkForFit[0]=reducedTrkSet[sel1T]; //------------------------- Refit selected vertex for xAOD::Vertex
          if(nvFitted>1){  //If only one fit attempt made - don't need to refit
	    scode=VKalVrtFitBase( tTrkForFit, FitVertex, Momentum, tChrg, ErrorMatrix, Chi2PerTrk, TrkAtVrt, Chi2);
	    if(scode.isFailure()){sumSelTrk.SetXYZT(0.,0.,0.,0.); Results.clear();}
          }
       }
       delete tmpBTP;
     }
// 
//------ Plane-Plane crossing doesn't provide good vertices for the moment
//     int sel2TI=-1,sel2TJ=-1;
//     if(reducedTrkSet.size()>1 && sel1T<0){ 
//       int nPRT=reducedTrkSet.size();
//       for(int it=0; it<nPRT-1; it++){  for(int jt=it+1; jt<nPRT; jt++){
//          TLorentzVector  pseudoB=GetBDir( reducedTrkSet[it],reducedTrkSet[jt],PrimVrt);
//          if(pseudoB.DeltaR(JetDir)>0.3)continue;
//	  sel2TI=it; sel2TJ=jt;
//       } }
//       if(sel1T<0 && sel2TI>=0){
//          tTrkForFit[0]=reducedTrkSet[sel2TI]; tTrkForFit[1]=reducedTrkSet[sel2TJ];
//	  scode=VKalVrtFitBase( tTrkForFit, FitVertex, Momentum, tChrg, ErrorMatrix, Chi2PerTrk, TrkAtVrt, Chi2);
//	  if(scode.isSuccess() && ProjPosT(FitVertex-PrimVrt.position(),JetDir) > 0 ){
//            sumSelTrk += MomAtVrt(TrkAtVrt[0]) ; sumSelTrk += MomAtVrt(TrkAtVrt[1]) ;
//	    Results.resize(4);
//            Results[0]=sumSelTrk.M();               //Invariant mass
//            Results[1]=sumSelTrk.Pt()/TrkJet.Pt();  //Ratio
//            Results[2]=0.;                          //Should be
//            Results[3]=nPRT;        //Found leading tracks with high positive impact
//       } }
//     }
//
//------ 
     if(sumSelTrk.Pt()==0)return 0;    //---------- Nothing found. Damn it! Else - return xAOD::Vertex
     Results.resize(7);
     Results[4]=0.; Results[5]=0.;
     Results[6]=TrkJet.E();
     xAOD::Vertex * tmpVertex=new xAOD::Vertex();
     tmpVertex->makePrivateStore();
     tmpVertex->setPosition(FitVertex);
     std::vector<float> floatErrMtx; floatErrMtx.resize(ErrorMatrix.size());
     for(int i=0; i<(int)ErrorMatrix.size(); i++) floatErrMtx[i]=ErrorMatrix[i];
     tmpVertex->setCovariance(floatErrMtx);
     tmpVertex->setFitQuality(Chi2, (float)(tTrkForFit.size()*2.-3.));
     std::vector<Trk::VxTrackAtVertex> & tmpVTAV=tmpVertex->vxTrackAtVertex();    tmpVTAV.clear();
       AmgSymMatrix(5) *CovMtxP=new AmgSymMatrix(5);   (*CovMtxP).setIdentity(); 
       Trk::Perigee * tmpMeasPer  =  new Trk::Perigee( 0.,0., TrkAtVrt[0][0], TrkAtVrt[0][1], TrkAtVrt[0][2],
                                                              Trk::PerigeeSurface(FitVertex), CovMtxP );
       tmpVTAV.push_back( Trk::VxTrackAtVertex( 1., tmpMeasPer) );
       ElementLink<xAOD::TrackParticleContainer> TEL;  TEL.setElement( tTrkForFit[0] );
       const xAOD::TrackParticleContainer* cont = (const xAOD::TrackParticleContainer* ) (tTrkForFit[0]->container() );
       TEL.setStorableObject(*cont);
       tmpVertex->addTrackAtVertex(TEL,1.);
     if(m_FillHist){m_hb_massJetTrkSV ->Fill(Results[0],1.);
                    m_hb_ratioJetTrkSV->Fill(Results[1],1.); 
                    m_hb_NImpJetTrkSV ->Fill(Results[3],1.);}
     return tmpVertex;
  }
>>>>>>> release/21.0.127




  xAOD::Vertex* InDetVKalVxInJetTool::getVrtSec(const std::vector<const xAOD::TrackParticle*>& inpTrk,
                                                const xAOD::Vertex                           & primVrt,
                                                const TLorentzVector                         & jetDir,
	                                        std::vector<double>                          & results,
                                                std::vector<const xAOD::TrackParticle*>      & listSecondTracks,
				                std::vector<const xAOD::TrackParticle*>      & trkFromV0,
						int & nRefPVTrk) 
  const
  {

<<<<<<< HEAD
      ATH_MSG_DEBUG("getVrtSec() called with xAOD::TrackParticle=" <<inpTrk.size());
=======
      if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG) << "GetVrtSec() called with xAOD::TrackParticle=" <<InpTrk.size()<< endmsg;
   
      std::vector<const xAOD::TrackParticle*> SelectedTracks(0),SelectedTracksRelax(0);
      Results.clear();        
      ListSecondTracks.clear();
>>>>>>> release/21.0.127

      std::vector<double> errorMatrix,Impact,ImpactError;
      Amg::Vector3D          fitVertex;
      std::vector< std::vector<double> > TrkAtVrt; 
      TLorentzVector    Momentum;
      double Signif3D=0., Chi2=0.;

      std::vector<const xAOD::TrackParticle*> selectedTracks(0);
      results.clear();        
      listSecondTracks.clear();
      nRefPVTrk=0;

<<<<<<< HEAD
      if( inpTrk.size() < 2 ) { return 0;} // 0,1 track => nothing to do!
      nRefPVTrk = selGoodTrkParticle( inpTrk, primVrt, jetDir, selectedTracks);
      if(m_fillHist){m_hb_ntrkjet->Fill( (double)selectedTracks.size(), m_w_1);
                     m_pr_NSelTrkMean->Fill(jetDir.Pt(),(double)selectedTracks.size()); }
      long int NTracks = (int) (selectedTracks.size());
=======
      long int NTracks = (int) (SelectedTracks.size());
      if(m_FillHist){m_hb_ntrkjet->Fill( (double) NTracks, m_w_1); }
>>>>>>> release/21.0.127
      if( NTracks < 2 ) { return 0;} // 0,1 selected track => nothing to do!

      if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG) << "Number of selected tracks inside jet= " <<NTracks << endmsg;
      
<<<<<<< HEAD
      TLorentzVector MomentumJet = totalMom(selectedTracks);
      if(m_fillHist){m_hb_jmom->Fill( MomentumJet.E(), m_w_1); }
=======
      SelGoodTrkParticleRelax( InpTrk, PrimVrt, JetDir, SelectedTracksRelax);
      TLorentzVector MomentumJet = TotalMom(SelectedTracksRelax);
      if(m_FillHist){m_hb_jmom->Fill( MomentumJet.E(), m_w_1); }
>>>>>>> release/21.0.127


//--------------------------------------------------------------------------------------------	 
//                    Initial xAOD::TrackParticle list ready
      float Vrt2TrackNumber =0;
<<<<<<< HEAD
=======
      std::vector<const xAOD::TrackParticle*>::const_iterator   i_ntrk;
      Amg::Vector3D          FitVertex;
      std::vector<double> ErrorMatrix,Impact,ImpactError;
      std::vector< std::vector<double> > TrkAtVrt; 
      TLorentzVector    Momentum;
      double Signif3D=0., Chi2=0.;
      const int nTrkLead=5;

>>>>>>> release/21.0.127

      std::vector<const xAOD::TrackParticle*>  tracksForFit;
      std::vector<double> inpMass(NTracks,m_massPi);
      select2TrVrt(selectedTracks, tracksForFit, primVrt, jetDir, inpMass, nRefPVTrk, 
                   trkFromV0,listSecondTracks);
//
//--- Cleaning
// 
      if( trkFromV0.size() > 1) {
        removeDoubleEntries(trkFromV0);
        AnalysisUtils::Sort::pT (&trkFromV0); 
      }
//---
<<<<<<< HEAD
      Vrt2TrackNumber = (double) listSecondTracks.size()/2.;
      removeDoubleEntries(listSecondTracks);
      AnalysisUtils::Sort::pT (&listSecondTracks);
      for(auto iv0 : trkFromV0){ auto itf=std::find(selectedTracks.begin(),selectedTracks.end(),iv0);
                                 if(itf!=selectedTracks.end())  selectedTracks.erase(itf);}
//---
      if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<" Found different xAOD tracks in pairs="<< listSecondTracks.size()<<endmsg;
      if(listSecondTracks.size() < 2 ) return 0;

//--Ranking of selected tracks
      std::vector<float> trkRank(0);
      for(auto tk : listSecondTracks) trkRank.push_back( m_trackClassificator->trkTypeWgts(tk, primVrt, jetDir)[0] );
      while( median(trkRank)<0.3 && trkRank.size()>3 ) {
        int Smallest= std::min_element(trkRank.begin(),trkRank.end()) - trkRank.begin();
        removeEntryInList(listSecondTracks,trkRank,Smallest);
=======
      Vrt2TrackNumber = (double) ListSecondTracks.size()/2.;
      std::vector<const xAOD::TrackParticle*> saveSecondTracks(ListSecondTracks);
      RemoveDoubleEntries(ListSecondTracks);
      for(auto iv0 : TrkFromV0){ auto itf=std::find(SelectedTracks.begin(),SelectedTracks.end(),iv0);
                                 if(itf!=SelectedTracks.end())  SelectedTracks.erase(itf);}
//---
      if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<" Found different xAOD tracks in pairs="<< ListSecondTracks.size()<<endmsg;
      if(ListSecondTracks.size() < 2 ) return tryPseudoVertex( SelectedTracks, PrimVrt, JetDir, MomentumJet, nTrkLead, Results);

//--- At least 2 secondary tracks are found.

      AnalysisUtils::Sort::pT (&ListSecondTracks);
//--Number of 2tr vertices where each track is used
      std::vector<int> combCount(ListSecondTracks.size());
      for(int tk=0;tk<(int)ListSecondTracks.size(); tk++){
        combCount[tk]=std::count(saveSecondTracks.begin(),saveSecondTracks.end(),ListSecondTracks[tk]);
>>>>>>> release/21.0.127
      }
//
//-----------------------------------------------------------------------------------------------------
//            Secondary track list is ready
//            Now common vertex fit
//
      double Signif3DP=0, Signif3DS=0;

      Chi2 =  fitCommonVrt( listSecondTracks, trkRank, primVrt, jetDir, inpMass, fitVertex, errorMatrix, Momentum, TrkAtVrt);
      if( Chi2 < 0 && listSecondTracks.size()>2 ) { // Vertex not reconstructed. Try to remove one track with biggest pt.
        double tpmax=0; int ipmax=-1;
        for(int it=0; it<(int)listSecondTracks.size(); it++) if(tpmax<listSecondTracks[it]->pt()){tpmax=listSecondTracks[it]->pt(); ipmax=it;}
        if(ipmax>=0)removeEntryInList(listSecondTracks,trkRank,ipmax);
        Chi2 =  fitCommonVrt( listSecondTracks, trkRank, primVrt, jetDir, inpMass, fitVertex, errorMatrix, Momentum, TrkAtVrt);
        if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<" Second fitCommonVrt try="<< Chi2<<" Ntrk="<<listSecondTracks.size()<<endmsg;
      }
<<<<<<< HEAD
      if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<" fitCommonVrt result="<< Chi2<<" Ntrk="<<listSecondTracks.size()<<endmsg;
=======
      if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<" FitCommonVrt result="<< Chi2<<endmsg;
>>>>>>> release/21.0.127
//
      if( Chi2 < 0) return 0;
//
// Check jet tracks not in secondary vertex
      std::map<double,const xAOD::TrackParticle*> AdditionalTracks;
      vrtVrtDist(primVrt, fitVertex, errorMatrix, Signif3D);
      if(Signif3D>8.){
<<<<<<< HEAD
	int hitL1=0, nLays=0, hitIBL=0, hitBL=0; 
        for (auto i_ntrk : selectedTracks) {
          if(  find( listSecondTracks.begin(), listSecondTracks.end(), i_ntrk) != listSecondTracks.end() ) continue; // Track is used already
          std::vector<float> trkScore=m_trackClassificator->trkTypeWgts(i_ntrk, primVrt, jetDir);
	  if( trkScore[0] < 0.1) continue; //Remove very low track HF score
          Signif3DS = m_fitSvc->VKalGetImpact(i_ntrk, fitVertex         , 1, Impact, ImpactError);
          if( Signif3DS > 10.) continue;
          getPixelLayers(i_ntrk , hitIBL , hitBL, hitL1, nLays );
          if( hitIBL<=0 && hitBL<=0 ) continue;                  // No IBL and BL pixel hits => non-precise track
          Signif3DP = m_fitSvc->VKalGetImpact(i_ntrk, primVrt.position(), 1, Impact, ImpactError);
          if(Signif3DP<1.)continue;
          if(m_fillHist){ m_hb_diffPS->Fill( Signif3DP-Signif3DS, m_w_1); }
	  if(Signif3DP-Signif3DS>4.0) AdditionalTracks[Signif3DP-Signif3DS]=i_ntrk;
        }
=======
       for (i_ntrk = SelectedTracks.begin(); i_ntrk < SelectedTracks.end(); ++i_ntrk) {
         i_found = find( ListSecondTracks.begin(), ListSecondTracks.end(), (*i_ntrk));
	 if( i_found != ListSecondTracks.end() ) continue;
         Signif3DS = m_fitSvc->VKalGetImpact((*i_ntrk), FitVertex         , 1, Impact, ImpactError);
         if( Signif3DS > 10.) continue;
         Signif3DP = m_fitSvc->VKalGetImpact((*i_ntrk), PrimVrt.position(), 1, Impact, ImpactError);
         if(m_FillHist){ m_hb_diffPS->Fill( Signif3DP-Signif3DS, m_w_1); }
	 if(Signif3DP-Signif3DS>1.0) AdditionalTracks.push_back((*i_ntrk));
       }
>>>>>>> release/21.0.127
      }
//
// Add found tracks and refit
//
      if( AdditionalTracks.size() > 0){
<<<<<<< HEAD
        while (AdditionalTracks.size()>3) AdditionalTracks.erase(AdditionalTracks.begin());//Tracks are in increasing DIFF order.
        for (auto atrk : AdditionalTracks) listSecondTracks.push_back(atrk.second);        //3tracks with max DIFF are selected
        trkRank.clear();
        for(auto tk : listSecondTracks) trkRank.push_back( m_trackClassificator->trkTypeWgts(tk, primVrt, jetDir)[0] );
        Chi2 =  fitCommonVrt( listSecondTracks, trkRank, primVrt, jetDir, inpMass, fitVertex, errorMatrix, Momentum, TrkAtVrt);
        if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<" Added track fitCommonVrt output="<< Chi2<<endmsg;
        if( Chi2 < 0) return 0;
=======
        for (i_ntrk = AdditionalTracks.begin(); i_ntrk < AdditionalTracks.end(); ++i_ntrk)
                      ListSecondTracks.push_back((*i_ntrk));
        std::vector<int> tmpCount(ListSecondTracks.size(),1);
        Chi2 =  FitCommonVrt( ListSecondTracks, tmpCount, PrimVrt, JetDir, InpMass, FitVertex, ErrorMatrix, Momentum, TrkAtVrt);
        if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<" Added track FitCommonVrt output="<< Chi2<<endmsg;
        if( Chi2 < 0) return tryPseudoVertex( SelectedTracks, PrimVrt, JetDir, MomentumJet, nTrkLead, Results);
>>>>>>> release/21.0.127
      }
//
//  Saving of results
//
//
      if( listSecondTracks.size()==2 ){         // If there are 2 only tracks
        if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<" Start Ntr=2 vertex check"<<endmsg;
        int Charge=0;
        for (auto i_ntrk : listSecondTracks) { Charge +=  (int) i_ntrk->charge();}
	vrtVrtDist(primVrt, fitVertex, errorMatrix, Signif3D);
// Check track pixel hit patterns vs vertex position.
        if(m_useVertexCleaningPix){
          if(!check2TrVertexInPixel(listSecondTracks[0],listSecondTracks[1],fitVertex,errorMatrix)) return 0;
        }
<<<<<<< HEAD
// Check track first measured points vs vertex position.
        if(m_useVertexCleaningFMP){
          float hitR1  = listSecondTracks[0]->radiusOfFirstHit();
          float hitR2  = listSecondTracks[1]->radiusOfFirstHit();
          float vrErr  = vrtRadiusError(fitVertex, errorMatrix);
          if(std::abs(hitR1-hitR2)>25.) return 0;                                 // Hits in different pixel layers
          if( fitVertex.perp()-std::min(hitR1,hitR2) > 2.*vrErr) return 0; // Vertex is behind hit in pixel 
        }
//--------
=======

        double vvdist3D=VrtVrtDist(PrimVrt, FitVertex, ErrorMatrix, Signif3D);
        if(m_useVertexCleaning){
          if(!Check2TrVertexInPixel(ListSecondTracks[0],ListSecondTracks[1],FitVertex,Signif3D)) return 0;
          double xDif=FitVertex.x()-m_XlayerB, yDif=FitVertex.y()-m_YlayerB ; 
          double Dist2D=sqrt(xDif*xDif+yDif*yDif);
          if     (Dist2D < m_RlayerB-m_SVResolutionR) {       // Inside B-layer
            if(m_FillHist){ if(Charge){m_hb_totmass2T1->Fill(Momentum.M(),m_w_1);}else{m_hb_totmass2T0->Fill(Momentum.M(),m_w_1);} }
            if(m_FillHist){ m_hb_blshared->Fill((float)BLshared,m_w_1); }
            //if(BLshared>m_CutSharedHits) return 0;          //VK Kills more b-jets events
          }else if(Dist2D > m_RlayerB+m_SVResolutionR) {      //Outside b-layer
            if(m_FillHist){ m_hb_pxshared->Fill((float)PXshared,m_w_1); }
            //if(PXshared>m_CutSharedHits) return 0;
          }
        } //end 2tr vertex cleaning code
>>>>>>> release/21.0.127
//
        if(m_fillHist){ if(Charge){m_hb_totmass2T1->Fill(Momentum.M(),m_w_1);}else{m_hb_totmass2T0->Fill(Momentum.M(),m_w_1);} }
        if( !Charge && std::abs(Momentum.M()-m_massK0)<15. ) {       // Final rejection of K0
	  trkFromV0.push_back(listSecondTracks[0]);
	  trkFromV0.push_back(listSecondTracks[1]);
          if( trkFromV0.size() > 1) {
             removeDoubleEntries(trkFromV0);
             AnalysisUtils::Sort::pT (&trkFromV0);
          }
          return 0;
        }
<<<<<<< HEAD
        if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<" Ntr=2 vertex check passed"<<endmsg;
=======
//-- Protection against fake vertices far from interaction point 
	if(NPVParticle<1)NPVParticle=1;
        double t3Dimp1= m_fitSvc->VKalGetImpact(ListSecondTracks[0], PrimVrt.position(), 1, Impact, ImpactError)/fabs(TrkAtVrt[0][2]);
        double t3Dimp2= m_fitSvc->VKalGetImpact(ListSecondTracks[1], PrimVrt.position(), 1, Impact, ImpactError)/fabs(TrkAtVrt[1][2]);
	double selVar=(t3Dimp1<t3Dimp2 ? t3Dimp1 : t3Dimp2)/sqrt((double)NPVParticle)/vvdist3D/500.;
        if(m_FillHist){ m_hb_tr2SelVar->Fill( selVar , m_w_1); }
	if(selVar<m_AntiFake2trVrtCut)return 0;
        if(m_FillHist){ m_hb_totmass2T2->Fill(Momentum.M(),m_w_1); }
>>>>>>> release/21.0.127
      }

	    
      double jetVrtDir = projSV_PV(fitVertex,primVrt,jetDir);
      if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<"Combined SV neg.dir="<<jetVrtDir<<endmsg;
      if(  m_getNegativeTag )
         { if( jetVrtDir>0. )   return 0; }
      else if( m_getNegativeTail )
         { ; }
      else 
         { if( jetVrtDir<0. ) return 0; } 

      double xvt=fitVertex.x(); double yvt=fitVertex.y();
      double Dist2DBP=std::hypot( xvt-m_beampipeX, yvt-m_beampipeY); 
      double Dist2DBL=std::hypot( xvt-m_xLayerB, yvt-m_yLayerB); 
      double Dist2DL1=std::hypot( xvt-m_xLayer1, yvt-m_yLayer1);
      double Dist2DL2=std::hypot( xvt-m_xLayer2, yvt-m_yLayer2);
      double minDstMat=39.9;
      minDstMat=TMath::Min(minDstMat,std::abs(Dist2DBP-m_beampipeR));
      minDstMat=TMath::Min(minDstMat,std::abs(Dist2DBL-m_rLayerB));
      minDstMat=TMath::Min(minDstMat,std::abs(Dist2DL1-m_rLayer1));
      minDstMat=TMath::Min(minDstMat,std::abs(Dist2DL2-m_rLayer2));
      if(m_existIBL) minDstMat=std::min(minDstMat,std::abs(Dist2DL2-m_rLayer3));  // 4-layer pixel detector
 
 
      vrtVrtDist(primVrt, fitVertex, errorMatrix, Signif3D);
      if(jetVrtDir < 0) Signif3D = -Signif3D;

<<<<<<< HEAD
=======
      if(FitVertex.perp()>m_Rbeampipe && Signif3D<20.)  return 0;  // Final cleaning of material interactions

      Amg::Vector3D DirForPt( FitVertex.x()-PrimVrt.x(),
                              FitVertex.y()-PrimVrt.y(),
		              FitVertex.z()-PrimVrt.z());
      //if( m_MassType == 3 ) Results.push_back( TotalTVMom(DirForPt, GetPerigeeVector(ListSecondTracks))); 
      //if( m_MassType == 2 ) Results.push_back(TotalTMom(GetPerigeeVector(ListSecondTracks))*1.15); 
      if( m_MassType == 1 ) Results.push_back(Momentum.M());       //1st
>>>>>>> release/21.0.127

      results.push_back(Momentum.M());                             //1st
      double eRatio = Momentum.E()/MomentumJet.E();
      results.push_back(  eRatio<0.99999 ? eRatio : 0.99999);      //2nd
      results.push_back(Vrt2TrackNumber);                          //3rd
      results.push_back((double)NTracks);                          //4th
      results.push_back((double)listSecondTracks.size());          //5th
      results.push_back(Signif3D);                                 //6th
      results.push_back(MomentumJet.E());                          //7th
      results.push_back(minDstMat);                                //8th
      double nRatio = Momentum.Et(jetDir.Vect())/std::sqrt(MomentumJet.Perp());   nRatio /= (nRatio+4.); 
      results.push_back( nRatio );                                 //9th   New transverse energy ration
      results.push_back((Momentum.M()-2.*m_massPi)*eRatio/m_massB);           //10th   "Product" variable
      results.push_back((Momentum.Pt()/Momentum.M())*(m_massB/jetDir.Pt()) ); //11th   "Boost" variable

      if(m_fillHist){
          // Find highest track Pt with respect to jet direction
          double trackPt, trackPtMax=0.;
          for (int tr=0; tr<(int)listSecondTracks.size(); tr++) {
	    if(listSecondTracks[tr]->pt()/jetDir.Pt()>0.5)continue;
            trackPt=pTvsDir(Amg::Vector3D(jetDir.X(),jetDir.Y(),jetDir.Z()) , TrkAtVrt[tr]);
	    if(trackPt>trackPtMax)trackPtMax=trackPt;
          }
<<<<<<< HEAD
	  m_hb_rNdc->Fill( fitVertex.perp(), m_w_1);
          m_hb_trkPtMax->Fill( trackPtMax, m_w_1);
          m_pr_effVrt->Fill((float)nRefPVTrk,1.);              
	  m_pr_effVrtEta->Fill( jetDir.Eta(),1.);
          m_hb_mom->Fill( MomentumJet.E(), m_w_1);
          m_hb_ratio->Fill( results[1], m_w_1); 
          m_hb_totmass->Fill( results[0], m_w_1); 
          m_hb_nvrt2->Fill( results[2], m_w_1);
          m_hb_sig3DTot->Fill( Signif3D, m_w_1);
          m_hb_dstToMat->Fill( minDstMat, m_w_1);
          float R=jetDir.DeltaR(TLorentzVector(fitVertex.x()-primVrt.x(),fitVertex.y()-primVrt.y(),
	                                       fitVertex.z()-primVrt.z(), 1.e4));
          m_hb_deltaRSVPV->Fill( R, m_w_1);
          if(m_curTup)m_curTup->TotM=Momentum.M();
       }
=======
          m_hb_r2dc->Fill( FitVertex.perp(), m_w_1);    
          m_hb_trkPtMax->Fill( trackPtMax, m_w_1);
          m_pr_effVrt->Fill((float)m_NRefTrk,1.);              
	  m_pr_effVrtEta->Fill( JetDir.Eta(),1.);
          m_hb_mom->Fill( MomentumJet.E(), m_w_1);
          m_hb_ratio->Fill( Results[1], m_w_1); 
          m_hb_totmass->Fill( Results[0], m_w_1); 
          m_hb_nvrt2->Fill( Results[2], m_w_1);
          m_hb_sig3DTot->Fill( Signif3D, m_w_1);
          m_hb_dstToMat->Fill( minDstMat, m_w_1);
      }
>>>>>>> release/21.0.127

//-------------------------------------------------------------------------------------
//Return xAOD::Vertex
       xAOD::Vertex * tmpVertex=new (std::nothrow) xAOD::Vertex();
       if(!tmpVertex)return nullptr;
       tmpVertex->makePrivateStore();
       tmpVertex->setPosition(fitVertex);
       std::vector<float> floatErrMtx; floatErrMtx.resize(errorMatrix.size());
       for(int i=0; i<(int)errorMatrix.size(); i++) floatErrMtx[i]=errorMatrix[i];
       tmpVertex->setCovariance(floatErrMtx);
       tmpVertex->setFitQuality(Chi2, (float)(listSecondTracks.size()*2.-3.));

       std::vector<Trk::VxTrackAtVertex> & tmpVTAV=tmpVertex->vxTrackAtVertex();    tmpVTAV.clear();
       for(int ii=0; ii<(int)listSecondTracks.size(); ii++) {
         AmgSymMatrix(5) *CovMtxP=new (std::nothrow) AmgSymMatrix(5);   if(CovMtxP) (*CovMtxP).setIdentity(); 
         Trk::Perigee * tmpMeasPer  =  new (std::nothrow) Trk::Perigee( 0.,0., TrkAtVrt[ii][0], TrkAtVrt[ii][1], TrkAtVrt[ii][2],
                                                                Trk::PerigeeSurface(fitVertex), CovMtxP );
         tmpVTAV.push_back( Trk::VxTrackAtVertex( 1., tmpMeasPer) );
         ElementLink<xAOD::TrackParticleContainer> TEL;  TEL.setElement( listSecondTracks[ii] );
         const xAOD::TrackParticleContainer* cont = (const xAOD::TrackParticleContainer* ) (listSecondTracks[ii]->container() );
	 TEL.setStorableObject(*cont);
         tmpVertex->addTrackAtVertex(TEL,1.);
       }
       return tmpVertex;


  }




//
//--------------------------------------------------------
//   Template routine for global secondary vertex fitting
//

  template <class Track>
  double InDetVKalVxInJetTool::fitCommonVrt(std::vector<const Track*>& listSecondTracks,
 				  std::vector<float>        & trkRank,
                                  const xAOD::Vertex        & primVrt,
 	                          const TLorentzVector      & jetDir,
                                  std::vector<double>       & inpMass, 
	                          Amg::Vector3D             & fitVertex,
                                  std::vector<double>       & errorMatrix,
	                          TLorentzVector            & Momentum,
		        std::vector< std::vector<double> >  & TrkAtVrt)
  const
 {
<<<<<<< HEAD
      if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG) << "fitCommonVrt() called " <<listSecondTracks.size()<< endmsg;
=======
      if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG) << "FitCommonVrt() called " <<ListSecondTracks.size()<< endmsg;
>>>>>>> release/21.0.127
//preparation
      StatusCode sc;
      std::vector<double> Chi2PerTrk;
      long int           Charge;
      double             Chi2 = 0.;
      Amg::Vector3D      tmpVertex;

      int Outlier=1, i=0;
//
// Start of fit
//
      std::unique_ptr<Trk::IVKalState> state = m_fitSvc->makeState();
      m_fitSvc->setMassInputParticles( inpMass, *state );            // Use pions masses
      sc=VKalVrtFitFastBase(listSecondTracks,fitVertex,*state);          /* Fast crude estimation */
      if(sc.isFailure() || fitVertex.perp() > m_rLayer2*2. ) {    /* No initial estimation */ 
         m_fitSvc->setApproximateVertex(primVrt.x(), primVrt.y(), primVrt.z(),*state); /* Use as starting point */
      } else {
         m_fitSvc->setApproximateVertex(fitVertex.x(),fitVertex.y(),fitVertex.z(),*state); /*Use as starting point*/
      }
//fit itself
      int NTracksVrt = listSecondTracks.size(); double FitProb=0.;
      std::vector<double> trkFitWgt(0);
      for (i=0; i < NTracksVrt-1; i++) {
         if(m_RobustFit)m_fitSvc->setRobustness(m_RobustFit, *state);
         else m_fitSvc->setRobustness(0, *state);
         sc=VKalVrtFitBase(listSecondTracks,fitVertex,Momentum,Charge,
                           errorMatrix,Chi2PerTrk,TrkAtVrt,Chi2,
                           *state, true);
         if(sc.isFailure() ||  Chi2 > 1000000. ) { return -10000.;}    // No fit
         sc=GetTrkFitWeights(trkFitWgt, *state);
         if(sc.isFailure()){ return -10000.;}    // No weights
	 Outlier=std::min_element(trkFitWgt.begin(),trkFitWgt.end())-trkFitWgt.begin();
         //////Outlier = findMax( Chi2PerTrk, trkRank ); 
	 FitProb=TMath::Prob( Chi2, 2*listSecondTracks.size()-3);
	 if(listSecondTracks.size() == 2 )              break;         // Only 2 tracks left
//////////////////////////////
         double signif3Dproj=vrtVrtDist( primVrt, fitVertex, errorMatrix, jetDir);
         if(signif3Dproj<0 && (!m_getNegativeTail) && (!m_getNegativeTag)){
	   double maxDst=-1.e12; int maxT=-1; double minChi2=1.e12;
	   for(int it=0; it<(int)listSecondTracks.size(); it++){
              std::vector<const Track*> tmpList(listSecondTracks);
              tmpList.erase(tmpList.begin()+it);
              sc=VKalVrtFitBase(tmpList,tmpVertex,Momentum,Charge,errorMatrix,Chi2PerTrk,TrkAtVrt,Chi2,*state,true);
              if(sc.isFailure())continue;
              signif3Dproj=vrtVrtDist( primVrt, tmpVertex, errorMatrix, jetDir);
	      if(signif3Dproj>maxDst  && maxDst<10. ){maxDst=signif3Dproj; maxT=it; minChi2=Chi2;}
	      else if(signif3Dproj>0. && maxDst>10. && Chi2<minChi2) {minChi2=Chi2; maxT=it;}
	   }
	   if(maxT>=0){ Outlier=maxT;   removeEntryInList(listSecondTracks,trkRank,Outlier);
                        m_fitSvc->setApproximateVertex(fitVertex.x(),fitVertex.y(),fitVertex.z(),*state);
                        if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<" Remove negative outlier="<< maxT<<" from "
                            <<listSecondTracks.size()+1<<" tracks"<<endmsg;
			continue;}
	 }
////////////////////////////////////////
//	 if( Momentum.m() < c_vrtBCMassLimit) {
//	   if( Chi2PerTrk[Outlier] < m_secTrkChi2Cut && FitProb > 0.001)  break;  // Solution found
//	 }
	 if( FitProb > 0.001) {
	   if( Momentum.M() <c_vrtBCMassLimit) {
	     if( Chi2PerTrk[Outlier] < m_secTrkChi2Cut*m_chiScale[listSecondTracks.size()<10?listSecondTracks.size():10])  break;  // Solution found
           } else {
	     double minM=1.e12; int minT=-1; double minChi2=1.e12;
	     for(int it=0; it<(int)listSecondTracks.size(); it++){
                std::vector<const Track*> tmpList(listSecondTracks);
                tmpList.erase(tmpList.begin()+it);
                sc=VKalVrtFitBase(tmpList,tmpVertex,Momentum,Charge,errorMatrix,Chi2PerTrk,TrkAtVrt,Chi2,*state,true);
                if(sc.isFailure())continue;
		if(projSV_PV(tmpVertex,primVrt,jetDir)<0.)continue; // Drop negative direction 
	        Chi2 += trkRank[it];                                // Remove preferably non-HF-tracks
		if(Momentum.M()<minM  && minM>c_vrtBCMassLimit){minM=Momentum.M(); minT=it; minChi2=Chi2;}
		else if(Momentum.M()<c_vrtBCMassLimit && minM<c_vrtBCMassLimit && Chi2<minChi2){minChi2=Chi2; minT=it;}
	     }
             if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<" Big mass. Remove trk="<<minT<<" New mass="<<minM<<" New Chi2="<<minChi2<<endmsg;
	     if(minT>=0)Outlier=minT;
	   }
	 }
         if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<"SecVrt remove trk="<<Outlier<<" from "<< listSecondTracks.size()<<" tracks"<<endmsg;
         removeEntryInList(listSecondTracks,trkRank,Outlier);
         m_fitSvc->setApproximateVertex(fitVertex.x(),fitVertex.y(),fitVertex.z(),*state); /*Use as starting point*/
      }
//--
<<<<<<< HEAD
      if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<" SecVrt fit converged. Ntr="<< listSecondTracks.size()<<" Chi2="<<Chi2
         <<" Chi2_trk="<<Chi2PerTrk[Outlier]<<" Prob="<<FitProb<<" M="<<Momentum.M()<<" Dir="<<projSV_PV(fitVertex,primVrt,jetDir)<<endmsg;
=======
      if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<" SecVrt fit converged="<< ListSecondTracks.size()<<", "
          <<Chi2<<", "<<Chi2PerTrk[Outlier]<<" Mass="<<Momentum.M()<<endmsg;
>>>>>>> release/21.0.127
//--
      if( listSecondTracks.size()==2 ){
	 if( Momentum.M() > c_vrtBCMassLimit || FitProb < 0.001 || Chi2PerTrk[Outlier] > m_secTrkChi2Cut) return -10000.;  
      } 
//
//-- To kill remnants of conversion
      double Dist2D=std::sqrt(fitVertex.x()*fitVertex.x()+fitVertex.y()*fitVertex.y());
      if( listSecondTracks.size()==2  && (Dist2D > 20.) && Charge==0 ) {
        double mass_EE   =  massV0( TrkAtVrt,m_massE,m_massE);
<<<<<<< HEAD
        if(m_fillHist){m_hb_totmassEE->Fill( mass_EE, m_w_1); }
=======
        if(m_FillHist){m_hb_totmassEE->Fill( mass_EE, m_w_1); }
>>>>>>> release/21.0.127
        if( mass_EE < 40. ) return -40.;
      }
//-- Test creation of Trk::Track
//      if( listSecondTracks.size()==2 && Charge==0 ) {      
//        std::vector<double> VKPerigee,CovPerigee;
//        sc=m_fitSvc->VKalVrtCvtTool(fitVertex,Momentum,errorMatrix,0,VKPerigee,CovPerigee,*state);
//        if(sc.isSuccess())const Trk::Track* TT = m_fitSvc->CreateTrkTrack(VKPerigee,CovPerigee,*state);
//      }
//      if( listSecondTracks.size()==2  && (Dist2D > 20.) ) {    // Protection against fake vertices    
//         std::vector<double> Impact,ImpactError;double ImpactSignif;
//         ImpactSignif = m_fitSvc->VKalGetImpact(listSecondTracks[0], primVrt.position(), 1, Impact, ImpactError);
//         if( ImpactSignif<4. && ImpactSignif>-3. ) return -41.;
//         ImpactSignif = m_fitSvc->VKalGetImpact(listSecondTracks[1], primVrt.position(), 1, Impact, ImpactError);
//         if( ImpactSignif<4. && ImpactSignif>-3. ) return -41.;
//      }
      return Chi2;
}







//
//
//--------------------------------------------------------
//   Template routine for 2track secondary vertices selection
//

    template <class Track>
    void InDetVKalVxInJetTool::select2TrVrt(std::vector<const Track*>  & selectedTracks,
                                  std::vector<const Track*>            & tracksForFit,
                                  const xAOD::Vertex                   & primVrt,
	                          const TLorentzVector                 & jetDir,
                                  std::vector<double>                  & inpMass, 
				  int                                  & nRefPVTrk,
	                          std::vector<const Track*>   & trkFromV0,
	                          std::vector<const Track*>   & listSecondTracks)
    const
    {
      std::vector<double> Chi2PerTrk,VKPerigee,CovPerigee;
      std::vector<double> Impact,ImpactError;
      double             Signif3D, Dist2D, jetVrtDir;
      long int           Charge;
      int i,j;
      StatusCode sc; sc.setChecked();
      Vrt2Tr tmpVrt;
      std::vector<Vrt2Tr> all2TrVrt(0);
      int NTracks = (int) (selectedTracks.size());

//
//  Impact parameters with sign calculations
//
      std::vector<float> covPV=primVrt.covariance(); 
      double SignifR=0.,SignifZ=0.;
      std::vector<int> hitIBL(NTracks,0), hitBL(NTracks,0);
      std::vector<double> TrkSig3D(NTracks);
      std::vector< std::vector<float> > trkScore(NTracks);
      AmgVector(5) tmpPerigee; tmpPerigee.setZero();
      for (i=0; i<NTracks; i++) {
<<<<<<< HEAD
         TrkSig3D[i] = m_fitSvc->VKalGetImpact(selectedTracks[i], primVrt.position(), 1, Impact, ImpactError);
         tmpPerigee = getPerigee(selectedTracks[i])->parameters(); 
         if( sin(tmpPerigee[2]-jetDir.Phi())*Impact[0] < 0 ){ Impact[0] = -std::abs(Impact[0]);}
	                                                else{ Impact[0] =  std::abs(Impact[0]);}
         if(  (tmpPerigee[3]-jetDir.Theta())*Impact[1] < 0 ){ Impact[1] = -std::abs(Impact[1]);}
	                                                else{ Impact[1] =  std::abs(Impact[1]);}
	 SignifR = Impact[0]/ std::sqrt(ImpactError[0]);
	 SignifZ = Impact[1]/ std::sqrt(ImpactError[2]);
	 int hL1=0, nLays=0; getPixelLayers(selectedTracks[i] , hitIBL[i] , hitBL[i], hL1, nLays );
         //----
         trkScore[i]=m_trackClassificator->trkTypeWgts(selectedTracks[i], primVrt, jetDir);
         if(m_fillHist){
	    m_hb_impactR->Fill( SignifR, m_w_1); 
            m_hb_impactZ->Fill( SignifZ, m_w_1); 
            m_hb_impactRZ->Fill(SignifR, SignifZ, m_w_1); 
	    m_hb_impact->Fill( TrkSig3D[i], m_w_1);
	    if(i<DevTuple::maxNTrk && m_curTup){
                 m_curTup->etatrk[i]=selectedTracks[i]->eta();
                 m_curTup->p_prob[i]=rankBTrk(selectedTracks[i]->pt(),jetDir.Pt(),0.);
                 m_curTup->s_prob[i]=rankBTrk(0.,0.,TrkSig3D[i]); 
                 m_curTup->SigR[i]=SignifR; m_curTup->SigZ[i]=SignifZ; 
                 m_curTup->d0[i]=Impact[0]; m_curTup->Z0[i]=Impact[1];
	         m_curTup->idMC[i]=getG4Inter(selectedTracks[i]); 
                 if(getIdHF(selectedTracks[i]))m_curTup->idMC[i]=2;
	         if(getMCPileup(selectedTracks[i]))m_curTup->idMC[i]=3;
		 m_curTup->wgtB[i]=trkScore[i][0]; m_curTup->wgtL[i]=trkScore[i][1]; m_curTup->wgtG[i]=trkScore[i][2]; 
		 m_curTup->sig3D[i]=TrkSig3D[i];
		 m_curTup->chg[i]=tmpPerigee[4]<0. ? 1: -1;
                 m_curTup->ibl[i]=hitIBL[i];
		 m_curTup->bl[i]=hitBL[i];
                 m_curTup->fhitR[i]=selectedTracks[i]->radiusOfFirstHit();
		 TLorentzVector TLV=selectedTracks[i]->p4();
		 m_curTup->pTvsJet[i]=TLV.Perp(jetDir.Vect());
		 TLorentzVector normJ;  normJ.SetPtEtaPhiM(1.,jetDir.Eta(),jetDir.Phi(),0.);
		 m_curTup->prodTJ[i]=std::sqrt(TLV.Dot(normJ));
		 m_curTup->nVrtT[i]=0;
            }
	 }
=======
         ImpactSignif = m_fitSvc->VKalGetImpact(SelectedTracks[i], PrimVrt.position(), 1, Impact, ImpactError);
         TrackSignifBase[i]=ImpactSignif;
         tmpPerigee = GetPerigee(SelectedTracks[i])->parameters(); 
         if( sin(tmpPerigee[2]-JetDir.Phi())*Impact[0] < 0 ){ Impact[0] = -fabs(Impact[0]);}
	                                                else{ Impact[0] =  fabs(Impact[0]);}
         if(  (tmpPerigee[3]-JetDir.Theta())*Impact[1] < 0 ){ Impact[1] = -fabs(Impact[1]);}
	                                                else{ Impact[1] =  fabs(Impact[1]);}
	 SignifR = Impact[0]/ sqrt(ImpactError[0]);
	 SignifZ = Impact[1]/ sqrt(ImpactError[2]);
         if(m_FillHist){
	    m_hb_impactR->Fill( SignifR, m_w_1); 
            m_hb_impactZ->Fill( SignifZ, m_w_1); 
            m_hb_impactRZ->Fill(SignifR, SignifZ, m_w_1); 
         }
         TrackPt[i] = sin(tmpPerigee[3])/fabs(tmpPerigee[4]);
	 if(ImpactSignif < 3.) { NPrimTrk += 1;}
         else{NSecTrk += 1;}
         if( SignifR<0 || SignifZ<0 ) m_NRefTrk++;
         if(m_getNegativeTail){
  	    ImpactSignif = sqrt( SignifR*SignifR + SignifZ*SignifZ);
  	 }else if(m_getNegativeTag){
  	    ImpactSignif = sqrt(  (SignifR-0.6)*(SignifR-0.6)
  	                        + (SignifZ-0.6)*(SignifZ-0.6) );
  	 }else{
  	    ImpactSignif = sqrt(  (SignifR+0.6)*(SignifR+0.6)
  	                        + (SignifZ+0.6)*(SignifZ+0.6) );
  	 }
         if(fabs(SignifR) < m_AntiPileupSigRCut) {   // cut against tracks from pileup vertices  
           if(SignifZ > 1.+m_AntiPileupSigZCut ) ImpactSignif=0.;  
           if(SignifZ < 1.-m_AntiPileupSigZCut ) ImpactSignif=0.;  
         }
      
         TrackSignif[i]=ImpactSignif;
         if(m_FillHist){m_hb_impact->Fill( ImpactSignif, m_w_1);}
>>>>>>> release/21.0.127
      }
      if(m_fillHist){  m_curTup->ptjet=jetDir.Perp();  m_curTup->etajet=jetDir.Eta(); m_curTup->phijet=jetDir.Phi();
                       m_curTup->nTrkInJet=std::min(NTracks,DevTuple::maxNTrk); };

      listSecondTracks.reserve(2*NTracks);                 // Reserve memory for single vertex


      Amg::Vector3D iniVrt(0.,0.,0.);
      for (i=0; i<NTracks-1; i++) {
         for (j=i+1; j<NTracks; j++) {
	     if(m_multiWithPrimary) {  // For multi-vertex with primary one search
                if(trkScore[i][2] > 0.75)continue;  //---- Use classificator to remove Pileup+Interactions only
                if(trkScore[j][2] > 0.75)continue;
             }else{
                if(trkScore[i][0]==0.)continue;  //Explicitly remove non-classified tracks
                if(trkScore[j][0]==0.)continue;
                if(getVrtScore(i,j,trkScore) < m_cutBVrtScore) continue;
	     }
	     int badTracks = 0;                                       //Bad tracks identification 
             tracksForFit.resize(2);
             std::unique_ptr<Trk::IVKalState> state = m_fitSvc->makeState();
             m_fitSvc->setMassInputParticles( inpMass, *state );     // Use pion masses for fit
             tracksForFit[0]=selectedTracks[i];
             tracksForFit[1]=selectedTracks[j];
             sc=VKalVrtFitFastBase(tracksForFit,tmpVrt.fitVertex,*state);              /* Fast crude estimation*/
             if( sc.isFailure() || tmpVrt.fitVertex.perp() > m_rLayer2*2. ) {   /* No initial estimation */ 
                iniVrt=primVrt.position();
                if( m_multiWithPrimary ) iniVrt.setZero(); 
 	     } else {
                jetVrtDir = projSV_PV(tmpVrt.fitVertex,primVrt,jetDir);
                if( m_multiWithPrimary ) jetVrtDir=std::abs(jetVrtDir); /* Always positive when primary vertex is seeked for*/ 
                if( jetVrtDir>0. ) iniVrt=tmpVrt.fitVertex;                /* Good initial estimation */ 
                else               iniVrt=primVrt.position();
             }
             m_fitSvc->setApproximateVertex(iniVrt.x(), iniVrt.y(), iniVrt.z(),*state);
             tmpVrt.i=i; tmpVrt.j=j;
             m_fitSvc->setRobustness(6, *state);
             sc=VKalVrtFitBase(tracksForFit,tmpVrt.fitVertex, tmpVrt.momentum, Charge,
                               tmpVrt.errorMatrix, tmpVrt.chi2PerTrk, tmpVrt.trkAtVrt, tmpVrt.chi2,
                               *state, true);
             if(sc.isFailure())                       continue;          /* No fit */ 
             if(tmpVrt.chi2 > m_sel2VrtChi2Cut)       continue;          /* Bad Chi2 */
	     if(std::abs(tmpVrt.fitVertex.z())> 650.)     continue;  // definitely outside of Pixel detector
             Dist2D=tmpVrt.fitVertex.perp(); 
	     if(Dist2D    > 180. )             continue;  // can't be from B decay

	     double vrErr  = vrtRadiusError(tmpVrt.fitVertex, tmpVrt.errorMatrix);
             if(vrErr>1.5&&getVrtScore(i,j,trkScore) < 4.*m_cutBVrtScore) continue;

             double mass_PiPi =  tmpVrt.momentum.M();  
	     if(mass_PiPi > m_Vrt2TrMassLimit)      continue;  // can't be from B decay
             vrtVrtDist(primVrt, tmpVrt.fitVertex, tmpVrt.errorMatrix, Signif3D);
	     tmpVrt.signif3D=Signif3D;
             vrtVrtDist2D(primVrt, tmpVrt.fitVertex, tmpVrt.errorMatrix, tmpVrt.signif2D);
//---
             TVector3 SVmPV(tmpVrt.fitVertex.x()-primVrt.x(),tmpVrt.fitVertex.y()-primVrt.y(),tmpVrt.fitVertex.z()-primVrt.z());
             tmpVrt.dRSVPV=jetDir.DeltaR(TLorentzVector(SVmPV, 1.)); //DeltaR SV-PV vs jet
             if(tmpVrt.dRSVPV > m_coneForTag ) continue;  // SV is outside of the jet cone
//---
             jetVrtDir = SVmPV.Dot(jetDir.Vect());
 	     double vPos=SVmPV.Dot(tmpVrt.momentum.Vect())/tmpVrt.momentum.Rho();
             if((!m_multiWithPrimary) &&(!m_getNegativeTail) && (!m_getNegativeTag) &&  jetVrtDir<0. )  continue; /* secondary vertex behind primary*/
	     if(vPos<-100.) continue;                                              /* Secondary vertex is too far behind primary*/
//
// Check track pixel hit patterns vs vertex position.
             if(m_useVertexCleaningPix && !check2TrVertexInPixel(selectedTracks[i],selectedTracks[j],tmpVrt.fitVertex,tmpVrt.errorMatrix)) continue;
// Check track first measured points vs vertex position.
             if(m_useVertexCleaningFMP){
               float ihitR  = selectedTracks[i]->radiusOfFirstHit();
               float jhitR  = selectedTracks[j]->radiusOfFirstHit();
               if(std::abs(ihitR-jhitR)>25.) continue;                            // Hits in different pixel layers
               if( tmpVrt.fitVertex.perp()-std::min(ihitR,jhitR) > 2.*vrErr) continue; // Vertex is behind hit in pixel 
             }
//--------
//
<<<<<<< HEAD
             double signif3Dproj=vrtVrtDist( primVrt, tmpVrt.fitVertex, tmpVrt.errorMatrix, jetDir);
	     tmpVrt.signif3DProj=signif3Dproj;
	     if(m_fillHist){ 
	        double Signif3DSign=Signif3D; if(jetVrtDir<0) Signif3DSign=-Signif3D;
	        m_hb_signif3D->Fill( Signif3DSign, m_w_1);
	        m_hb_sig3DNtr->Fill(signif3Dproj,  m_w_1);
	     }

             //if( m_multiWithPrimary || m_multiVertex) { // For multivertex
             //  add_edge(i,j,*m_compatibilityGraph);
             //} 
  	     if( m_multiWithPrimary )   continue;   /* Multivertex with primary one. All below is not needed */
=======
             double Signif3Dproj=VrtVrtDist( PrimVrt, FitVertex, ErrorMatrix, JetDir);
             double Signif3DSign=Signif3D;
  	     if(  JetVrtDir < 0) Signif3DSign=-Signif3D;
  	     if(m_FillHist)m_hb_signif3D->Fill( Signif3DSign, m_w_1);
	     if(m_FillHist)m_hb_massPiPi2->Fill( mass_PiPi, m_w_1);
             if(Signif3DSign<12. && Chi2>m_Sel2VrtChi2Cut)       continue;          /* Bad Chi2 */
>>>>>>> release/21.0.127
//
//  Check if V0 or material interaction on Pixel layer is present
//
	     if( Charge == 0 && Signif3D>8. && mass_PiPi<900.) {
<<<<<<< HEAD
               double mass_PPi  =  massV0( tmpVrt.trkAtVrt,m_massP,m_massPi);
               double mass_EE   =  massV0( tmpVrt.trkAtVrt,m_massE,m_massE);
               if(m_fillHist && !m_multiVertex){m_hb_massEE->Fill( mass_EE, m_w_1);} 
=======
               double mass_PPi  =  massV0( TrkAtVrt,m_massP,m_massPi);
               double mass_EE   =  massV0( TrkAtVrt,m_massE,m_massE);
               if(m_FillHist){m_hb_massEE->Fill( mass_EE, m_w_1);} 
>>>>>>> release/21.0.127
	       if(       mass_EE <  40.)  { 
	         badTracks = 3;
	       }else{
<<<<<<< HEAD
                 if(m_fillHist && !m_multiVertex){m_hb_massPiPi->Fill( mass_PiPi, m_w_1);} /* Total mass with input particles masses*/
                 if(m_fillHist && !m_multiVertex){m_hb_massPPi->Fill( mass_PPi, m_w_1);} 
	         if( std::abs(mass_PiPi-m_massK0) < 22. )  badTracks = 1;
	         if( std::abs(mass_PPi-m_massLam) <  8. )  badTracks = 2;
=======
                 if(m_FillHist){m_hb_massPiPi->Fill( mass_PiPi, m_w_1);}     /* Total mass with input particles masses*/
                 if(m_FillHist){m_hb_massPPi->Fill( mass_PPi, m_w_1);} 
	         if( fabs(mass_PiPi-m_massK0) < 22. )  BadTracks = 1;
	         if( fabs(mass_PPi-m_massLam) <  8. )  BadTracks = 2;
	         //double TransMass=TotalTMom(GetPerigeeVector(TracksForFit));
	         //if( TransMass<400. && m_FillHist)m_hb_massPiPi1->Fill( mass_PiPi, m_w_1);
>>>>>>> release/21.0.127
	       }
//
//  Creation of V0 tracks
//
	       if(badTracks){
	          std::vector<double> inpMassV0;
                  //Reset VKalVrt settings
                  state = m_fitSvc->makeState();
		                                              //matrix are calculated
		  if( badTracks == 1 ) {  // K0 case
		    inpMassV0.push_back(m_massPi);inpMassV0.push_back(m_massPi);
                    m_fitSvc->setMassInputParticles( inpMassV0, *state );
                    m_fitSvc->setMassForConstraint(m_massK0, *state);
                    m_fitSvc->setCnstType(1, *state);       // Set mass  constraint
                  }
		  if( badTracks == 2 ) {  // Lambda case
	            if( std::abs(1./tmpVrt.trkAtVrt[0][2]) > std::abs(1./tmpVrt.trkAtVrt[1][2]) ) {
		            inpMassV0.push_back(m_massP);inpMassV0.push_back(m_massPi);
	            }else{  inpMassV0.push_back(m_massPi);inpMassV0.push_back(m_massP); }
                    m_fitSvc->setMassInputParticles( inpMassV0, *state );
                    m_fitSvc->setMassForConstraint(m_massLam, *state);
                    m_fitSvc->setCnstType(1, *state);       // Set mass  constraint
                  }
		  if( badTracks == 3 ) {  // Gamma case
		    inpMassV0.push_back(m_massE);inpMassV0.push_back(m_massE);
                    m_fitSvc->setMassInputParticles( inpMassV0, *state );
                    m_fitSvc->setCnstType(12, *state);       // Set 3d angular constraint
                  }
                  m_fitSvc->setApproximateVertex(tmpVrt.fitVertex.x(),tmpVrt.fitVertex.y(),tmpVrt.fitVertex.z(),*state);
                  TLorentzVector MomentumV0;
                  Amg::Vector3D  fitVertexV0;
                  std::vector< std::vector<double> > TrkAtVrtV0; 
                  std::vector<double> errorMatrixV0;
		  double Chi2V0;
                  sc=VKalVrtFitBase(tracksForFit, fitVertexV0, MomentumV0, Charge,
                                    errorMatrixV0,Chi2PerTrk,TrkAtVrtV0,Chi2V0,
                                    *state, true);
                  if(sc.isSuccess()) {
                    sc=m_fitSvc->VKalVrtCvtTool(fitVertexV0,MomentumV0,errorMatrixV0,0,VKPerigee,CovPerigee,*state);
                    if(sc.isSuccess()) {
<<<<<<< HEAD
                      const Trk::Track* TT = m_fitSvc->CreateTrkTrack(VKPerigee,CovPerigee,*state); 
                      double ImpactSignifV0=m_fitSvc->VKalGetImpact(TT, primVrt.position(), 0, Impact, ImpactError, *state);
                      if(m_fillHist){m_hb_impV0->Fill( ImpactSignifV0, m_w_1);}
	              if(ImpactSignifV0>3.0 ) badTracks=0;
=======
                      const Trk::Track* TT = m_fitSvc->CreateTrkTrack(VKPerigee,CovPerigee); 
                      ImpactSignif=m_fitSvc->VKalGetImpact(TT, PrimVrt.position(), 0, Impact, ImpactError);
                      if(m_FillHist){m_hb_impV0->Fill( ImpactSignif, m_w_1); }
	              if(ImpactSignif>3.5) BadTracks=0;
>>>>>>> release/21.0.127
		      delete TT;
	            } else { badTracks=0;}
	         }  // else { badTracks=0;}
               }
             }
//
//  Check interactions on material layers
//
<<<<<<< HEAD
            float minWgtI = std::min(trkScore[i][2],trkScore[j][2]);
            if( minWgtI >0.50 && Dist2D > m_beampipeR-vrtRadiusError(tmpVrt.fitVertex, tmpVrt.errorMatrix) ) badTracks = 4;
            //if( (trkScore[i][2]>0.4 || trkScore[j][2]>0.4) 
            //   && insideMatLayer(tmpVrt.fitVertex.x(),tmpVrt.fitVertex.y()) ) badTracks=5;
//
//-----------------------------------------------
	     tmpVrt.badVrt=badTracks;          //
	     all2TrVrt.push_back(tmpVrt);      //
//-----------------------------------------------
             if(m_fillHist){  m_hb_r2d->Fill( tmpVrt.fitVertex.perp(), m_w_1); }
=======
	     float xvt=FitVertex.x(); float yvt=FitVertex.y();
             if(m_FillHist){m_hb_r2d->Fill( Dist2D, m_w_1);}
             if(m_useMaterialRejection){
              //if(m_materialMap){
              //  if(m_materialMap->inMaterial(FitVertex)) BadTracks=4;
              //  if(msgLvl(MSG::DEBUG))msg(MSG::DEBUG)<<" MaterialMap test="<< BadTracks<<endreq;
	      //}else{   
               float Dist2DBP=sqrt( (xvt-m_Xbeampipe)*(xvt-m_Xbeampipe) + (yvt-m_Ybeampipe)*(yvt-m_Ybeampipe) ); 
               float Dist2DBL=sqrt( (xvt-m_XlayerB)*(xvt-m_XlayerB) + (yvt-m_YlayerB)*(yvt-m_YlayerB) ); 
               float Dist2DL1=sqrt( (xvt-m_Xlayer1)*(xvt-m_Xlayer1) + (yvt-m_Ylayer1)*(yvt-m_Ylayer1) );
               float Dist2DL2=sqrt( (xvt-m_Xlayer2)*(xvt-m_Xlayer2) + (yvt-m_Ylayer2)*(yvt-m_Ylayer2) );
               if(m_existIBL){              // 4-layer pixel detector
                 if( fabs(Dist2DBP-m_Rbeampipe)< 1.0)  BadTracks = 4;           // Beam Pipe removal  
                 if( fabs(Dist2DBL-m_RlayerB)  < 2.5)  BadTracks = 4;
                 if( fabs(Dist2DL1-m_Rlayer1)  < 3.0)  BadTracks = 4;
                 if( fabs(Dist2DL2-m_Rlayer2)  < 3.0)  BadTracks = 4;
                 if( fabs(Dist2DL2-m_Rlayer3)  < 3.0)  BadTracks = 4;
               }else{                       // 3-layer pixel detector
                 if( fabs(Dist2DBP-m_Rbeampipe)< 1.5)  BadTracks = 4;           // Beam Pipe removal  
                 if( fabs(Dist2DBL-m_RlayerB)  < 3.5)  BadTracks = 4;
                 if( fabs(Dist2DL1-m_Rlayer1)  < 4.0)  BadTracks = 4;
                 if( fabs(Dist2DL2-m_Rlayer2)  < 5.0)  BadTracks = 4;
               }  //}
             }
>>>>>>> release/21.0.127
//
//  Creation of tracks from V0 list
//	     
	     if( badTracks ){
	        trkFromV0.push_back(selectedTracks[i]);
	        trkFromV0.push_back(selectedTracks[j]);
	     }else{
	        if( (Signif3D>m_sel2VrtSigCut) ) {
                  if(std::abs(signif3Dproj)<m_sel2VrtSigCut)continue;
	          listSecondTracks.push_back(selectedTracks[i]);
	          listSecondTracks.push_back(selectedTracks[j]);
                }
	     }
         }
      } 
//      if(m_fillHist)  fillVrtNTup(all2TrVrt);

//
//-- Start vertex analysis
/*
      std::vector<int> trkTypeSV(NTracks,-1);   // Track identification: -1 -unknown, 2-fragmentation, 1-Interaction, 0-HF
      std::vector<int> nFrVrtT(NTracks,0);      // Fragmentation vertex per track counter
      int foundHF=0;
      for( auto vv : all2TrVrt){
        if( 1.-1./11.*vv.signif2D > std::min(trkScore[vv.i][0],trkScore[vv.j][0]) ){
           if( std::min(trkScore[vv.i][1],trkScore[vv.j][1]) > 0.5 ){ nFrVrtT[vv.i]++;  nFrVrtT[vv.j]++; }
           continue;
        }
	if( trkScore[vv.i][0]+trkScore[vv.j][0] < trkScore[vv.i][1]+trkScore[vv.j][1]) continue;
	if( trkScore[vv.i][0]+trkScore[vv.j][0] < trkScore[vv.i][2]+trkScore[vv.j][2]) continue;
        trkTypeSV[vv.i]=0; trkTypeSV[vv.j]=0; foundHF++;
        //if( trkScore[vv.i][0]>trkScore[vv.i][1] && trkScore[vv.j][0]>trkScore[vv.j][1] 
        // && trkScore[vv.i][0]>trkScore[vv.i][2] && trkScore[vv.j][0]>trkScore[vv.j][2] ){ trkTypeSV[vv.i]=0; trkTypeSV[vv.j]=0; foundHF++; } 
      }
      for( auto vv : all2TrVrt){                                                 //Now interactions+V0s
	if( foundHF==1 && (trkTypeSV[vv.i]==0 || trkTypeSV[vv.j]==0 )) continue; //preserve single HF-vertex
        if( vv.badVrt ){ trkTypeSV[vv.i]=1; trkTypeSV[vv.j]=1;}
      }
      for( auto vv : all2TrVrt){                                                              //Now fragmentation
        if( trkTypeSV[vv.i]>=0 || trkTypeSV[vv.j]>=0 ) continue;                              //Skip identified tracks
        if( trkScore[vv.i][1]>trkScore[vv.i][0] && trkScore[vv.j][1]>trkScore[vv.j][0] 
         && trkScore[vv.i][1]>trkScore[vv.i][2] && trkScore[vv.j][1]>trkScore[vv.j][2] ){ trkTypeSV[vv.i]=2; trkTypeSV[vv.j]=2;} 
      }
      for (i=0; i<NTracks; i++) if( trkTypeSV[i]==0 && nFrVrtT[i]>0 && trkScore[i][1]>0.5 ) trkTypeSV[i]=2;
//      
//-- Remove FF, IF(some) and II vertices
//      iv=0; while ( iv < (int)all2TrVrt.size() )
//             { if( trkTypeSV[all2TrVrt[iv].i]>0 && trkTypeSV[all2TrVrt[iv].j]>0) all2TrVrt.erase(all2TrVrt.begin()+iv); else iv++; }
//
<<<<<<< HEAD
*/
//-- Cleaning. Remove small wgtB tracks attached to one vertex only. 
//      std::vector<int> inVrt(NTracks,0);
//      for( auto vv : all2TrVrt){ inVrt[vv.i]++; inVrt[vv.j]++; }
//      ////std::map<float,int> trkInOneV; for( int kt=0; kt<NTracks; kt++) if(inVrt[kt]==1) trkInOneV[trkScore[kt][0]]=kt;
//      for(int kk=0; kk<NTracks; kk++){
//        if( inVrt[kk]==1 && trkScore[kk][0]<2.5*m_cutHFClass ) {
//           int iv=0;   while (iv<(int)all2TrVrt.size()){ if( all2TrVrt[iv].i==kk || all2TrVrt[iv].j==kk ) { all2TrVrt.erase(all2TrVrt.begin()+iv); break;} else iv++; }
//      } }
//============================================================================
//-- Save results
      listSecondTracks.clear();
      std::map<float,int> trkHF;
      for( auto &vv : all2TrVrt){ 
        if( m_multiWithPrimary || m_multiVertex) add_edge(vv.i,vv.j,*m_compatibilityGraph);
        trkHF[trkScore[vv.i][0]]=vv.i; trkHF[trkScore[vv.j][0]]=vv.j;
      }
      for( auto it : trkHF) { listSecondTracks.push_back(selectedTracks[it.second]); }
//-Debug
      if( m_fillHist && m_curTup ){ 
         for( auto &it : trkHF) { m_curTup->itHF[m_curTup->NTHF++]=it.second; }
         for( auto &vv : all2TrVrt){ m_curTup->nVrtT[vv.i]++; m_curTup->nVrtT[vv.j]++; }
         fillVrtNTup(all2TrVrt);
      }
//
//--------------------------------------------------------------------
//-- Post-selection checks 
//--------------------------------------------------------------------
      if(listSecondTracks.size()>0 ){ 
        if(m_fillHist){ m_pr_effVrt2tr->Fill((float)nRefPVTrk,1.);
                        m_pr_effVrt2trEta->Fill( jetDir.Eta(),1.);}
      } else if(listSecondTracks.size()==0) { if(m_fillHist){m_pr_effVrt2tr->Fill((float)nRefPVTrk,0.);
                                                             m_pr_effVrt2trEta->Fill(jetDir.Eta(),0.); }}
=======
      if(ListSecondTracks.size()==1 && closeVrtSig[0]<9.){    //Turned off for the moment (ListSecondTracks.size() can't be 1!)
        auto it0=std::find(SelectedTracks.begin(),SelectedTracks.end(),ListSecondTracks[0]);
        auto it1=std::find(SelectedTracks.begin(),SelectedTracks.end(),ListSecondTracks[1]);
	int eGood=1; double cutNTrkDep=cutNDepNorm(m_NRefTrk,0.05);                        // NTrk dependence
        if(it0!=SelectedTracks.end() && pow(TrackSignifBase[it0-SelectedTracks.begin()],2.) < cutNTrkDep )  eGood=0;
        if(it1!=SelectedTracks.end() && pow(TrackSignifBase[it1-SelectedTracks.begin()],2.) < cutNTrkDep )  eGood=0;
	if(!eGood)ListSecondTracks.clear();
      }
      //--------------------------------------------------------------------
      if(ListSecondTracks.size()>0 ){ 
        if(m_FillHist){ m_pr_effVrt2tr->Fill((float)m_NRefTrk,1.);
                      //m_pr_effVrt2trEta->Fill( JetDir.Eta(),1.);}
                        m_pr_effVrt2trEta->Fill( JetDir.Eta(),(double)ListSecondTracks.size()/2.);}
	//----- Only vertices with unique tracks are considered as bad
	if(TrkFromV0.size()){
	  std::vector<const Track*> tmpVec(0);
          for(int tk=0;tk<(int)TrkFromV0.size(); tk+=2){
            int nFound1=std::count(ListSecondTracks.begin(),ListSecondTracks.end(),TrkFromV0[tk]);
            int nFound2=std::count(ListSecondTracks.begin(),ListSecondTracks.end(),TrkFromV0[tk+1]);
	    if(nFound1+nFound2){
	       ListSecondTracks.push_back(TrkFromV0[tk]);
	       ListSecondTracks.push_back(TrkFromV0[tk+1]);
	    }else{
	       tmpVec.push_back(TrkFromV0[tk]);
	       tmpVec.push_back(TrkFromV0[tk+1]);
            }
          }
          TrkFromV0.swap(tmpVec);
	}
      } else if(ListSecondTracks.size()==0) { if(m_FillHist){m_pr_effVrt2tr->Fill((float)m_NRefTrk,0.);
                                                             m_pr_effVrt2trEta->Fill(JetDir.Eta(),0.); }}
/////////// Attempt to find iasolated tracks with high impact parameter. They are RARE!!! No worth to use them!
//        TLorentzVector psum,tmp; int nprt=0;
//        for (i=0; i<NTracks; i++) {
//          if( TrackSignif[i]<2.*SelLim || signTrk[i]<0) continue;
//     auto it0=std::find(ListSecondTracks.begin(),ListSecondTracks.end(),SelectedTracks[i]); if( it0!=ListSecondTracks.end() )continue;
//          it0=std::find(TrkFromV0.begin(),TrkFromV0.end(),SelectedTracks[i]);               if( it0!=TrkFromV0.end() )continue;
//          it0=std::find(ListCloseTracks.begin(),ListCloseTracks.end(),SelectedTracks[i]);   if( it0!=ListCloseTracks.end() )continue;
//          int ibl,bl,l1,nlay; getPixelLayers(SelectedTracks[i], ibl, bl, l1, nlay); if(ibl+bl<2)continue; 
//          nprt++; tmp.SetPtEtaPhiM(SelectedTracks[i]->pt(),SelectedTracks[i]->eta(),SelectedTracks[i]->phi(),m_massPi); psum += tmp;
//        } if(nprt<1)return;
//        if(nprt==1) { tmp.SetPtEtaPhiM(psum.Pt(),JetDir.Eta(),JetDir.Phi(),m_massPi); psum += tmp; }
//        if(m_FillHist)m_hb_massPiPi1->Fill( psum.M(), m_w_1);
/////////////////////////////////////
>>>>>>> release/21.0.127
      return;
   }




   template <class Track>
   bool InDetVKalVxInJetTool::check2TrVertexInPixel( const Track* p1, const Track* p2,
                                              Amg::Vector3D &fitVertex, std::vector<double> & vrtErr)
   const
   {
	int blTrk[2]={0,0};
	int blP[2]={0,0};
	int l1Trk[2]={0,0};
	int l1P[2]={0,0};
	int l2Trk[2]={0,0};
	int nLays[2]={0,0};
        getPixelLayers( p1, blTrk[0] , l1Trk[0], l2Trk[0], nLays[0] );
        getPixelLayers( p2, blTrk[1] , l1Trk[1], l2Trk[1], nLays[1] );    // Very close to PV. Both b-layer hits are mandatory.
<<<<<<< HEAD
        getPixelProblems(p1, blP[0], l1P[0] );
        getPixelProblems(p2, blP[1], l1P[1] );
        double xvt=fitVertex.x(),  yvt=fitVertex.y(); 
        double radiusError=vrtRadiusError(fitVertex, vrtErr);
        double Dist2DBL=std::hypot(xvt-m_xLayerB, yvt-m_yLayerB);
        if      (Dist2DBL < m_rLayerB-radiusError) {       //----------------------------------------- Inside B-layer
          if( blTrk[0]==0 && blTrk[1]==0) return false;  // No b-layer hits at all, but all expected
	  if( blTrk[0]<1  && l1Trk[0]<1 ) return false;
	  if( blTrk[1]<1  && l1Trk[1]<1 ) return false;
          if(  nLays[0]           <2 )    return false;  // Less than 2 layers on track 0
          if(  nLays[1]           <2 )    return false;  // Less than 2 layers on track 1
=======
        if( Signif3D<15. && FitVertex.perp()<15. ){
	   if( blTrk[0]<1  && l1Trk[0]<1  )  return false;
	   if( blTrk[1]<1  && l1Trk[1]<1  )  return false;
	   if( blTrk[0]==0 && blTrk[1]==0 )  return false;
        }
        double xDif=FitVertex.x()-m_XlayerB, yDif=FitVertex.y()-m_YlayerB ; 
        double Dist2DBL=sqrt(xDif*xDif+yDif*yDif);
        if      (Dist2DBL < m_RlayerB-m_SVResolutionR){       //----------------------------------------- Inside B-layer
          if(blTrk[0]==0 && blTrk[1]==0) return false;  // No b-layer hits at all
          if(  nLays[0]           <2 )   return false;  // Less than 2 layers on track 0
          if(  nLays[1]           <2 )   return false;  // Less than 2 layers on track 1
>>>>>>> release/21.0.127
	  return true;
        }else if(Dist2DBL > m_rLayerB+radiusError){      //----------------------------------------- Outside b-layer
          if( blTrk[0]>0 && blP[0]==0 && blTrk[1]>0 && blP[1]==0 ) return false;  // Good hit in b-layer is present
        }
// 
// L1 and L2 are considered only if vertex is in acceptance
//
	if(std::abs(fitVertex.z())<400.){
          double Dist2DL1=std::hypot(xvt-m_xLayer1, yvt-m_yLayer1);
          double Dist2DL2=std::hypot(xvt-m_xLayer2, yvt-m_yLayer2);
          if      (Dist2DL1 < m_rLayer1-radiusError) {   //------------------------------------------ Inside 1st-layer
	     if( l1Trk[0]==0 && l1Trk[1]==0 )     return false;  // No L1 hits at all
             if( l1Trk[0]<1  && l2Trk[0]<1  )     return false;  // Less than 1 hits on track 0
             if( l1Trk[1]<1  && l2Trk[1]<1  )     return false;  // Less than 1 hits on track 1
             return true;
<<<<<<< HEAD
          }else if(Dist2DL1 > m_rLayer1+radiusError) {  //------------------------------------------- Outside 1st-layer
	     if( l1Trk[0]>0 && l1P[0]==0 && l1Trk[1]>0 && l1P[1]==0 )       return false;  //  Good L1 hit is present
=======
          }else if(Dist2DL1 > m_Rlayer1+m_SVResolutionR) {  //------------------------------------------- Outside 1st-layer
	     if( l1Trk[0]>0 || l1Trk[1]>0 )       return false;  //  L1 hits are present
>>>>>>> release/21.0.127
          }
          
          if      (Dist2DL2 < m_rLayer2-radiusError) {  //------------------------------------------- Inside 2nd-layer
	     if( (l2Trk[0]+l2Trk[1])==0 )  return false;           // At least one L2 hit must be present
<<<<<<< HEAD
          }else if(Dist2DL2 > m_rLayer2+radiusError) {  
	  //   if( (l2Trk[0]+l2Trk[1])>0  )  return false;           // L2 hits are present
	  }     
=======
          }else if(Dist2DL2 > m_Rlayer2+m_SVResolutionR) {  
	  //   if( (l2Trk[0]+l2Trk[1])>0  )  return false;           // L2 hits are present
	  }           
>>>>>>> release/21.0.127
        } else {
	  int d0Trk[2]={0,0}; 
	  int d1Trk[2]={0,0}; 
	  int d2Trk[2]={0,0}; 
          getPixelDiscs( p1, d0Trk[0] , d1Trk[0], d2Trk[0] );
          getPixelDiscs( p2, d0Trk[1] , d1Trk[1], d2Trk[1] );
          if( d0Trk[0]+d1Trk[0]+d2Trk[0] ==0 )return false;
          if( d0Trk[1]+d1Trk[1]+d2Trk[1] ==0 )return false;
        }
        return true;
   }

<<<<<<< HEAD
=======
    


    template <class Track>
    double InDetVKalVxInJetTool::RemoveNegImpact(std::vector<const Track*>  & inTracks,
                                                 const xAOD::Vertex        & PrimVrt,
	                                         const TLorentzVector      & JetDir,
					         double Limit)
    const
    {
      int blTrk=0, l1Trk=0, l2Trk=0, nLays=0;
      int i=0;
      while( i<(int)inTracks.size()){
        getPixelLayers( inTracks[i], blTrk , l1Trk, l2Trk, nLays );
        //if(nLays<1 || inTracks[i]->pt()>JetDir.Pt()) inTracks.erase(inTracks.begin()+i); //bad track: no pixel hit OR trk_pt>jet_pt
        if(nLays<1) inTracks.erase(inTracks.begin()+i); //bad track: no pixel hit
        else        i++;
      }
//----
      int NTracks = inTracks.size();
      std::vector<double> ImpR(NTracks);
      std::vector<double> Impact,ImpactError;
      AmgVector(5) tmpPerigee; tmpPerigee<<0.,0.,0.,0.,0.;
      double maxImp=-1.e10;
      for (i=0; i<NTracks; i++) {
         m_fitSvc->VKalGetImpact(inTracks[i], PrimVrt.position(), 1, Impact, ImpactError);
         tmpPerigee = GetPerigee(inTracks[i])->parameters(); 
         if( sin(tmpPerigee[2]-JetDir.Phi())*Impact[0] < 0 ){ Impact[0] = -fabs(Impact[0]);}
	                                                else{ Impact[0] =  fabs(Impact[0]);}
         if(  (tmpPerigee[3]-JetDir.Theta())*Impact[1] < 0 ){ Impact[1] = -fabs(Impact[1]);}
	                                                else{ Impact[1] =  fabs(Impact[1]);}
	 double SignifR = Impact[0]/ sqrt(ImpactError[0]); ImpR[i]=SignifR;
         if(fabs(SignifR)   < m_AntiPileupSigRCut) {   // cut against tracks from pileup vertices  
           if( fabs(Impact[1])/sqrt(ImpactError[2]) > m_AntiPileupSigZCut ) ImpR[i]=-9999.;  
         }
	 if(ImpR[i]>maxImp)maxImp=ImpR[i];
      }
      if(maxImp<Limit){  inTracks.clear(); return maxImp;}
      double rmin=1.e6;
      do{ rmin=1.e6; int jpm=0; 
          for(i=0; i<(int)ImpR.size(); i++){ if(rmin>ImpR[i]){rmin=ImpR[i]; jpm=i;}}; if(rmin>Limit)continue;
          ImpR.erase(ImpR.begin()+jpm); inTracks.erase(inTracks.begin()+jpm);
      }while(rmin<=Limit);
      return maxImp;
    }


>>>>>>> release/21.0.127
}  //end of namespace
