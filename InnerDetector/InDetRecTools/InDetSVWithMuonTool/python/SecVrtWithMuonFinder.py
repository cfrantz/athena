<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

from AthenaCommon.Logging  import logging
=======
# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

from GaudiKernel.GaudiHandles import *
from GaudiKernel.Proxy.Configurable import *
from AthenaCommon.Include  import Include, IncludeError, include
from AthenaCommon.Logging  import logging
from AthenaCommon.AppMgr   import ToolSvc
from AthenaCommon          import CfgMgr

>>>>>>> release/21.0.127
from InDetSVWithMuonTool.InDetSVWithMuonToolConf import InDet__InDetSVWithMuonTool

# define the class
class SecVrtWithMuonFinder( InDet__InDetSVWithMuonTool ):

    def __init__(self, name = 'SecVrtWithMuonFinder'  ):        

        from __main__ import ToolSvc
        mlog = logging.getLogger( 'SecVrtWithMuonFinder::__init__ ' )
        mlog.info("entering")
        #----------------------
        # VKalVrt vertex fitter
        # 
        from TrkVKalVrtFitter.TrkVKalVrtFitterConf import Trk__TrkVKalVrtFitter
        MuonVertexFitterTool = Trk__TrkVKalVrtFitter(name="MuonVertexFitterTool",
<<<<<<< HEAD
                                                  Extrapolator="Trk::Extrapolator/AtlasExtrapolator"
=======
                                                  Extrapolator="Trk::Extrapolator/AtlasExtrapolator",
                                                  AtlasMagFieldSvc = "AtlasFieldSvc"
                                                  #AtlasMagFieldSvc = "Default",
                                                  #Extrapolator = "DefaultVKalPropagator"
>>>>>>> release/21.0.127
                                                 )
        ToolSvc += MuonVertexFitterTool
        #----------------------
        # Secondary vertex finder itself
        #
        InDet__InDetSVWithMuonTool.__init__( self, name = name,
                                              VertexFitterTool     = MuonVertexFitterTool
                                            )

