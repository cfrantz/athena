/*
<<<<<<< HEAD
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef INDETOVERLAY_INDETSDOOVERLAY_H
#define INDETOVERLAY_INDETSDOOVERLAY_H

#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "InDetSimData/InDetSimDataCollection.h"

class InDetSDOOverlay : public AthReentrantAlgorithm
{
public:
  InDetSDOOverlay(const std::string &name, ISvcLocator *pSvcLocator);

  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext& ctx) const override;

private:
  SG::ReadHandleKey<InDetSimDataCollection> m_bkgInputKey{ this, "BkgInputKey", "", "ReadHandleKey for Background Input InDetSimDataCollection" };
  SG::ReadHandleKey<InDetSimDataCollection> m_signalInputKey{ this, "SignalInputKey", "", "ReadHandleKey for Signal Input InDetSimDataCollection" };
  SG::WriteHandleKey<InDetSimDataCollection> m_outputKey{ this, "OutputKey", "", "WriteHandleKey for Output InDetSimDataCollection" };

};

#endif // INDETOVERLAY_INDETSDOOVERLAY_H
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef INDETSDOOVERLAY_H
#define INDETSDOOVERLAY_H

#include <string>

#include "OverlayAlgBase/OverlayAlgBase.h"

class InDetSDOOverlay : public OverlayAlgBase
{
public:
  InDetSDOOverlay(const std::string &name, ISvcLocator *pSvcLocator);
  
  virtual StatusCode overlayInitialize() override;
  virtual StatusCode overlayExecute() override;
  virtual StatusCode overlayFinalize() override;

private:
  bool m_do_TRT, m_do_TRT_background;
  std::string m_mainInputTRTKey;
  std::string m_overlayInputTRTKey;
  std::string m_outputTRTKey;

  bool m_do_SCT, m_do_SCT_background;
  std::string m_mainInputSCTKey;
  std::string m_overlayInputSCTKey;
  std::string m_outputSCTKey;

  bool m_do_Pixel, m_do_Pixel_background;
  std::string m_mainInputPixelKey;
  std::string m_overlayInputPixelKey;
  std::string m_outputPixelKey;
};

#endif // INDETSDOOVERLAY_H
>>>>>>> release/21.0.127
