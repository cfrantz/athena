# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# Declare the package name:
atlas_subdir( PixelRTT )

<<<<<<< HEAD
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   InnerDetector/InDetCalibAlgs/PixelCalibAlgs
   PRIVATE
   InnerDetector/InDetConditions/PixelConditionsData
   Tools/PathResolver )

>>>>>>> release/21.0.127
# External dependencies:
find_package( ROOT COMPONENTS Core MathCore Hist RIO Gpad )

# Component(s) in the package:
atlas_add_library( PixelValidation
   PixelRTT/*.h src/*.cxx
   PUBLIC_HEADERS PixelRTT
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
<<<<<<< HEAD
   LINK_LIBRARIES ${ROOT_LIBRARIES} CxxUtils PixelCalibAlgsLib
=======
   LINK_LIBRARIES ${ROOT_LIBRARIES} PixelCalibAlgsLib
>>>>>>> release/21.0.127
   PRIVATE_LINK_LIBRARIES PixelConditionsData PathResolver )

atlas_add_executable( doPixelValidation
   Application/doPixelValidation.cxx
<<<<<<< HEAD
   LINK_LIBRARIES CxxUtils PixelValidation )
=======
   LINK_LIBRARIES PixelValidation )
>>>>>>> release/21.0.127

# Install files from the package:
atlas_install_joboptions( share/*.py )
atlas_install_runtime( test/*.* )
