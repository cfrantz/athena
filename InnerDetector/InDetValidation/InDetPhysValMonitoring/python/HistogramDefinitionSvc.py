# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
from InDetRecExample.TrackingCommon import setDefaults


<<<<<<< HEAD
def getHistogramDefinitionSvc(**kwargs):
    import InDetPhysValMonitoring.InDetPhysValMonitoringConf
    kwargs = setDefaults(kwargs,
                         DefinitionSource="InDetPVMPlotDefRun2.xml",
                         DefinitionFormat="text/xml")
    return InDetPhysValMonitoring.InDetPhysValMonitoringConf.HistogramDefinitionSvc(**kwargs)
=======
import InDetPhysValMonitoring.InDetPhysValMonitoringConf
class HistogramDefinitionSvc(object) :
  '''
  Namespace for Histogram definition services
  '''
  def __init__(self) :
     raise('must not be instantiated. Only child classes should be instantiated.')

  class HistogramDefinitionSvc(InDetPhysValMonitoring.InDetPhysValMonitoringConf.HistogramDefinitionSvc) :
      '''
      Default HistogramDefinitionSvc 
      '''
      @injectNameArgument
      def __new__(cls, *args, **kwargs) :
          return InDetPhysValMonitoring.InDetPhysValMonitoringConf.HistogramDefinitionSvc.__new__(cls,*args,**kwargs)

      @checkKWArgs
      def __init__(self, **kwargs) :
          super(HistogramDefinitionSvc.HistogramDefinitionSvc,self)\
                        .__init__(**_args( kwargs,
                                           name = self.__class__.__name__))

          # special parameters of the default HistogramDefinitionSvc
          self.DefinitionSource="InDetPVMPlotDefRun2.xml"
          self.DefinitionFormat="text/xml"

>>>>>>> release/21.0.127
