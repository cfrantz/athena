/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/
#include "InDetPhysValMonitoring/InDetPhysValMonitoringTool.h"
#include "../InDetPhysValTruthDecoratorAlg.h"
#include "../InDetPhysHitDecoratorAlg.h"
#include "../ParameterErrDecoratorAlg.h"
#include "../TruthClassDecoratorAlg.h"
#include "../TrackTruthSelectionTool.h"
#include "../dRMatchingTool.h"
#include "../TrackSelectionTool.h"
#include "InDetPhysValMonitoring/HistogramDefinitionSvc.h"
<<<<<<< HEAD
=======
#include "InDetPhysValMonitoring/AlgTestHistoDefSvc.h"
#include "InDetPhysValMonitoring/ToolTestMonitoringPlots.h"
>>>>>>> release/21.0.127
#include "../AthTruthSelectionTool.h"

#include "../DummyTrackSlimmingTool.h"

<<<<<<< HEAD
DECLARE_COMPONENT( HistogramDefinitionSvc )
DECLARE_COMPONENT( InDetPhysValMonitoringTool )
DECLARE_COMPONENT( InDetPhysValTruthDecoratorAlg )
DECLARE_COMPONENT( InDetPhysHitDecoratorAlg )
DECLARE_COMPONENT( ParameterErrDecoratorAlg )
DECLARE_COMPONENT( TruthClassDecoratorAlg )
DECLARE_COMPONENT( TrackTruthSelectionTool )
DECLARE_COMPONENT( dRMatchingTool )
DECLARE_COMPONENT( TrackSelectionTool )
DECLARE_COMPONENT( DummyTrackSlimmingTool )
DECLARE_COMPONENT( AthTruthSelectionTool )
=======
DECLARE_ALGORITHM_FACTORY(InDetPhysValDecoratorAlg)
DECLARE_ALGORITHM_FACTORY(AlgTestHistoDefSvc)
DECLARE_SERVICE_FACTORY(HistogramDefinitionSvc)
DECLARE_TOOL_FACTORY(InDetPhysValMonitoringTool)
DECLARE_TOOL_FACTORY(InDetPhysValLargeD0Tool)
DECLARE_TOOL_FACTORY(ToolTestMonitoringPlots)
DECLARE_TOOL_FACTORY(InDetPhysValTruthDecoratorTool)
DECLARE_TOOL_FACTORY(InDetPhysHitDecoratorTool)
DECLARE_TOOL_FACTORY(ParameterErrDecoratorTool)
DECLARE_TOOL_FACTORY(TruthClassDecoratorTool)
DECLARE_TOOL_FACTORY(TrackTruthSelectionTool)
DECLARE_TOOL_FACTORY(dRMatchingTool)
DECLARE_TOOL_FACTORY(TrackSelectionTool)
DECLARE_TOOL_FACTORY( DummyTrackSlimmingTool)
DECLARE_TOOL_FACTORY( AthTruthSelectionTool)

DECLARE_FACTORY_ENTRIES(InDetPhysValMonitoring){
    DECLARE_ALGORITHM( InDetPhysValDecoratorAlg )
    DECLARE_ALGORITHM( AlgTestHistoDefSvc )
    DECLARE_SERVICE(HistogramDefinitionSvc)
    DECLARE_TOOL(InDetPhysValMonitoringTool)
    DECLARE_TOOL(InDetPhysValLargeD0Tool)
    DECLARE_TOOL(ToolTestMonitoringPlots)
    DECLARE_TOOL(InDetPhysValTruthDecoratorTool)
    DECLARE_TOOL(InDetPhysHitDecoratorTool)
    DECLARE_TOOL(ParameterErrDecoratorTool)
    DECLARE_TOOL(TruthClassDecoratorTool)
    DECLARE_TOOL(TrackTruthSelectionTool)
    DECLARE_TOOL(dRMatchingTool)
    DECLARE_TOOL(TrackSelectionTool)
    DECLARE_TOOL( DummyTrackSlimmingTool)
    DECLARE_TOOL( AthTruthSelectionTool)

}
>>>>>>> release/21.0.127

