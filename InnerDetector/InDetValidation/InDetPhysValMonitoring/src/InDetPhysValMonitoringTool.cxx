/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

/**
 * @file InDetPhysValMonitoringTool.cxx
 * @author shaun roe
 **/
<<<<<<< HEAD

#include "GaudiKernel/SystemOfUnits.h"

#include "InDetPhysValMonitoring/InDetPhysValMonitoringTool.h"
#include "InDetPhysHitDecoratorAlg.h"
=======

#include "InDetPhysValMonitoring/InDetPhysValMonitoringTool.h"
#include "InDetPhysHitDecoratorTool.h"
>>>>>>> release/21.0.127
#include "InDetRttPlots.h"
#include "InDetPerfPlot_nTracks.h"
#include "AthTruthSelectionTool.h"
#include "CachedGetAssocTruth.h"
<<<<<<< HEAD

#include "safeDecorator.h"
=======
>>>>>>> release/21.0.127
// #include "TrackTruthLookup.h"
//
#include "TrkToolInterfaces/ITrackSelectorTool.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODTracking/TrackStateValidation.h"
<<<<<<< HEAD
=======
#include "xAODTracking/TrackStateValidationContainer.h"
#include "xAODEventInfo/EventInfo.h"
>>>>>>> release/21.0.127
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthPileupEvent.h"
#include "xAODTruth/TruthPileupEventAuxContainer.h"

#include "TrkTrack/TrackCollection.h"
<<<<<<< HEAD
=======
#include "xAODJet/JetContainer.h"
#include "PATCore/TAccept.h"
>>>>>>> release/21.0.127
//
#include <algorithm>
#include <limits>
#include <cmath> // to get std::isnan(), std::abs etc.
// #include <functional> // to get std::plus
#include <utility>
#include <cstdlib> // to getenv
#include <vector>

#include "InDetTruthVertexValidation/InDetVertexTruthMatchUtils.h"

<<<<<<< HEAD
namespace { // utility functions used here

  // get truth/track matching probability
  float
  getMatchingProbability(const xAOD::TrackParticle& trackParticle) {
    float result(std::numeric_limits<float>::quiet_NaN());

    if (trackParticle.isAvailable<float>("truthMatchProbability")) {
      result = trackParticle.auxdata<float>("truthMatchProbability");
    }
    return result;
  }

  template <class T>
  inline float
  safelyGetEta(const T& pTrk, const float safePtThreshold = 0.1) {
    return (pTrk->pt() > safePtThreshold) ? (pTrk->eta()) : std::nan("");
  }

  // general utility function to check value is in range
  template <class T>
  inline bool
  inRange(const T& value, const T& minVal, const T& maxVal) {
    return not ((value < minVal)or(value > maxVal));
  }

  template<class T>
  inline bool
  inRange(const T& value, const T& absoluteMax) {
    return not (std::abs(value) > absoluteMax);
  }

  // Cuts on various objects
  bool
  passJetCuts(const xAOD::Jet& jet) {
    const float absEtaMax = 2.5;
    const float jetPtMin = 100.0;  // in GeV
    const float jetPtMax = 5000.0; // in GeV
    const float jetPt = jet.pt() / Gaudi::Units::GeV; // GeV
    const float jetEta = jet.eta();

    return inRange(jetPt, jetPtMin, jetPtMax) and inRange(jetEta, absEtaMax);
  }

  // general utility function to return bin index given a value and the upper endpoints of each bin
  template <class T>
  unsigned int
  binIndex(const T& val, const std::vector<T>& partitions) {// signature should allow other containers
    unsigned int i(0);
    bool nf = true;

    while (nf and i != partitions.size()) {
      nf = (val > partitions[i++]);
=======

namespace { // utility functions used here
  // sort method for prospects
  bool
  sortProspects(std::pair<float, const xAOD::TrackParticle*> pair1,
                std::pair<float, const xAOD::TrackParticle*> pair2) {
    return(pair1.first < pair2.first);
  }

  // get truth/track matching probability
  float
  getMatchingProbability(const xAOD::TrackParticle& trackParticle) {
    float result(std::numeric_limits<float>::quiet_NaN());

    if (trackParticle.isAvailable<float>("truthMatchProbability")) {
      result = trackParticle.auxdata<float>("truthMatchProbability");
>>>>>>> release/21.0.127
    }
    return nf ? i : i - 1;
  }

  bool
<<<<<<< HEAD
  acceptVertex(const xAOD::Vertex* vtx) {
    return(vtx->vertexType() != xAOD::VxType::NoVtx);
  }

  bool
  acceptTruthVertex(const xAOD::TruthVertex* vtx) {
    const float x(vtx->x()), y(vtx->y()), z(vtx->z());
    const float vrtR2 = (x * x + y * y); // radial distance squared

    return inRange(z, 500.f) and not (vrtR2 > 1); // units?
  }

  const std::vector<float> ETA_PARTITIONS = {
    2.7, 3.5, std::numeric_limits<float>::infinity()
  };

=======
  isInsideOut(const xAOD::TrackParticle& track) {
    std::bitset<xAOD::TrackPatternRecoInfo::NumberOfTrackRecoInfo>  patternInfo = track.patternRecoInfo();
    return patternInfo.test(0);
  }

  template <class T>
  inline float
  safelyGetEta(const T& pTrk, const float safePtThreshold = 0.1) {
    return (pTrk->pt() > safePtThreshold) ? (pTrk->eta()) : std::nan("");
  }

  // Convert to GeV from the default MeV
  constexpr float
  operator "" _GeV (long double energy) {
    return energy * 0.001;
  }

  constexpr float
  operator "" _GeV (unsigned long long energy) {
    return energy * 0.001;
  }

  // general utility function to check value is in range
  template <class T>
  inline bool
  inRange(const T& value, const T& minVal, const T& maxVal) {
    return not ((value < minVal)or(value > maxVal));
  }

  template<class T>
  inline bool
  inRange(const T& value, const T& absoluteMax) {
    return not (std::abs(value) > absoluteMax);
  }

  // Cuts on various objects
  bool
  passJetCuts(const xAOD::Jet& jet) {
    const float absEtaMax = 2.5;
    const float jetPtMin = 100;  // in GeV
    const float jetPtMax = 1000; // in GeV
    const float jetPt = jet.pt() * 1_GeV; // GeV
    const float jetEta = jet.eta();

    return inRange(jetPt, jetPtMin, jetPtMax) and inRange(jetEta, absEtaMax);
  }

  // general utility function to return bin index given a value and the upper endpoints of each bin
  template <class T>
  unsigned int
  binIndex(const T& val, const std::vector<T>& partitions) {// signature should allow other containers
    unsigned int i(0);
    bool nf = true;

    while (nf and i != partitions.size()) {
      nf = (val > partitions[i++]);
    }
    return nf ? i : i - 1;
  }

  bool
  acceptVertex(const xAOD::Vertex* vtx) {
    return(vtx->vertexType() != xAOD::VxType::NoVtx);
  }

  bool
  acceptTruthVertex(const xAOD::TruthVertex* vtx) {
    const float x(vtx->x()), y(vtx->y()), z(vtx->z());
    const float vrtR2 = (x * x + y * y); // radial distance squared

    return inRange(z, 500.f) and not (vrtR2 > 1); // units?
  }

  const std::vector<float> ETA_PARTITIONS = {
    2.7, 3.5, std::numeric_limits<float>::infinity()
  };
>>>>>>> release/21.0.127
}// namespace

///Parametrized constructor
InDetPhysValMonitoringTool::InDetPhysValMonitoringTool(const std::string& type, const std::string& name,
                                                       const IInterface* parent) :
  ManagedMonitorToolBase(type, name, parent),
  m_useTrackSelection(false),
<<<<<<< HEAD
  m_useVertexTruthMatchTool(false),
  m_trackSelectionTool("InDet::InDetTrackSelectionTool/TrackSelectionTool"),
  m_vtxValidTool("InDetVertexTruthMatchTool/VtxTruthMatchTool"),
  m_truthSelectionTool("AthTruthSelectionTool", this),
  m_doTrackInJetPlots(true),
  m_fillTruthToRecoNtuple(false) {
  declareProperty("useTrackSelection", m_useTrackSelection);
  declareProperty("TrackSelectionTool", m_trackSelectionTool);
  declareProperty("VertexTruthMatchTool", m_vtxValidTool);
  declareProperty("useVertexTruthMatchTool", m_useVertexTruthMatchTool);
  declareProperty("TruthSelectionTool", m_truthSelectionTool);
  declareProperty("doTruthOriginPlots", m_doTruthOriginPlots);
  declareProperty("FillTrackInJetPlots", m_doTrackInJetPlots);
  declareProperty("FillTrackInBJetPlots", m_doBjetPlots);
  declareProperty("FillTruthToRecoNtuple", m_fillTruthToRecoNtuple);
  declareProperty("maxTrkJetDR", m_maxTrkJetDR = 0.4);
  declareProperty("DirName", m_dirName = "SquirrelPlots/");
  declareProperty("SubFolder", m_folder);
  declareProperty("PileupSwitch", m_pileupSwitch = "HardScatter");
  declareProperty("LowProb", m_lowProb=0.50);
  declareProperty("HighProb", m_highProb=0.80);
  declareProperty("SkillLevel", m_detailLevel=10);
}

InDetPhysValMonitoringTool::~InDetPhysValMonitoringTool() {

}

StatusCode
InDetPhysValMonitoringTool::initialize() {
  ATH_CHECK(ManagedMonitorToolBase::initialize());
  // Get the track selector tool only if m_useTrackSelection is true;
  // first check for consistency i.e. that there is a trackSelectionTool if you ask
  // for trackSelection
  ATH_CHECK(m_trackSelectionTool.retrieve(EnableTool {m_useTrackSelection} ));
  ATH_CHECK(m_truthSelectionTool.retrieve(EnableTool {not m_truthParticleName.key().empty()} ));
  ATH_CHECK(m_vtxValidTool.retrieve(EnableTool {m_useVertexTruthMatchTool}));
  ATH_CHECK(m_trackTruthOriginTool.retrieve( EnableTool {m_doTruthOriginPlots} ));

  ATH_MSG_DEBUG("m_useVertexTruthMatchTool ====== " <<m_useVertexTruthMatchTool);
  if (m_truthSelectionTool.get() ) {
    m_truthCutFlow = CutFlow(m_truthSelectionTool->nCuts());
  }
  m_monPlots = std::make_unique<InDetRttPlots> (nullptr, m_dirName + m_folder, m_detailLevel); // m_detailLevel := DEBUG, enable expert histograms
  m_monPlots->SetFillJetPlots(m_doTrackInJetPlots,m_doBjetPlots);

  ATH_CHECK( m_trkParticleName.initialize() );
  ATH_CHECK( m_truthParticleName.initialize( (m_pileupSwitch == "HardScatter" or m_pileupSwitch == "All") and not m_truthParticleName.key().empty() ) );
  ATH_CHECK( m_vertexContainerName.initialize() );
  ATH_CHECK( m_truthVertexContainerName.initialize( not m_truthVertexContainerName.key().empty() ) );
  ATH_CHECK( m_eventInfoContainerName.initialize() );

  ATH_CHECK( m_truthEventName.initialize( (m_pileupSwitch == "HardScatter" or m_pileupSwitch == "All") and not m_truthEventName.key().empty() ) );
  ATH_CHECK( m_truthPileUpEventName.initialize( (m_pileupSwitch == "PileUp" or m_pileupSwitch == "All") and not m_truthPileUpEventName.key().empty() ) );
  ATH_CHECK( m_jetContainerName.initialize( not m_jetContainerName.key().empty()) );  

  std::vector<std::string> required_float_track_decorations {"d0","hitResiduals_residualLocX","d0err"};
  std::vector<std::string> required_int_track_decorations {};
  std::vector<std::string> required_float_truth_decorations {"d0"};
  std::vector<std::string> required_int_truth_decorations {};
  std::vector<std::string> required_int_jet_decorations {"HadronConeExclTruthLabelID"};

  std::string empty_prefix;
  IDPVM::addReadDecoratorHandleKeys(*this, m_trkParticleName, empty_prefix, required_float_track_decorations, m_floatTrkDecor);
  IDPVM::addReadDecoratorHandleKeys(*this, m_trkParticleName, empty_prefix, required_int_truth_decorations,   m_intTrkDecor);
  if (!m_truthParticleName.key().empty()) {
    IDPVM::addReadDecoratorHandleKeys(*this, m_truthParticleName, empty_prefix, required_float_truth_decorations, m_floatTruthDecor);
    IDPVM::addReadDecoratorHandleKeys(*this, m_truthParticleName, empty_prefix, required_int_truth_decorations,   m_intTruthDecor);
  }
  if (m_doTrackInJetPlots && m_doBjetPlots){
    IDPVM::addReadDecoratorHandleKeys(*this, m_jetContainerName, empty_prefix, required_int_jet_decorations, m_intJetDecor);
  }
  return StatusCode::SUCCESS;
}

StatusCode
InDetPhysValMonitoringTool::fillHistograms() {
  ATH_MSG_DEBUG("Filling hists " << name() << "...");
  // function object could be used to retrieve truth: IDPVM::CachedGetAssocTruth getTruth;

  // retrieve trackParticle container
  SG::ReadHandle<xAOD::TrackParticleContainer> tracks(m_trkParticleName);
  SG::ReadHandle<xAOD::TruthPileupEventContainer> truthPileupEventContainer;
  if( not m_truthPileUpEventName.key().empty()) {
    truthPileupEventContainer = SG::ReadHandle<xAOD::TruthPileupEventContainer> (m_truthPileUpEventName);
  }

  SG::ReadHandle<xAOD::EventInfo> pie = SG::ReadHandle<xAOD::EventInfo>(m_eventInfoContainerName);
 
  std::vector<const xAOD::TruthParticle*> truthParticlesVec = getTruthParticles();

  // Mark the truth particles in our vector as "selected". 
  // This is needed because we later access the truth matching via xAOD decorations, where we do not 'know' about membership to this vector.
  markSelectedByPileupSwitch(truthParticlesVec);

  IDPVM::CachedGetAssocTruth getAsTruth; // only cache one way, track->truth, not truth->tracks 

  if (not tracks.isValid()) {
    return StatusCode::FAILURE;
  }
  if (not pie.isValid()){
    ATH_MSG_WARNING("Shouldn't happen - EventInfo is buggy, setting mu to 0");
  }

  ATH_MSG_DEBUG("Getting number of pu interactings per event");

  ATH_MSG_DEBUG("Filling vertex plots");
  SG::ReadHandle<xAOD::VertexContainer>  vertices(m_vertexContainerName);
  const xAOD::Vertex* primaryvertex = nullptr;
  const float puEvents = !m_truthPileUpEventName.key().empty() and truthPileupEventContainer.isValid() ?  static_cast<int>( truthPileupEventContainer->size() ) : pie.isValid() ? pie->actualInteractionsPerCrossing() : 0;
  const float nVertices = not vertices->empty() ? vertices->size() : 0;

  if (vertices.isValid() and not vertices->empty()) {
    ATH_MSG_DEBUG("Number of vertices retrieved for this event " << vertices->size());
    const auto& stdVertexContainer = vertices->stdcont();
    //Find the first non-dummy vertex; note *usually* there is only one (or maybe zero)
    auto findVtx = std::find_if(stdVertexContainer.rbegin(), stdVertexContainer.rend(), acceptVertex);
    primaryvertex = (findVtx == stdVertexContainer.rend()) ? nullptr : *findVtx;
    //Filling plots for all reconstructed vertices and the hard-scatter
    ATH_MSG_DEBUG("Filling vertices info monitoring plots");

    // Fill vectors of truth HS and PU vertices
    std::pair<std::vector<const xAOD::TruthVertex*>, std::vector<const xAOD::TruthVertex*>> truthVertices = getTruthVertices();
    std::vector<const xAOD::TruthVertex*> truthHSVertices = truthVertices.first;
    std::vector<const xAOD::TruthVertex*> truthPUVertices = truthVertices.second;

    // Decorate vertices
    if (m_useVertexTruthMatchTool && m_vtxValidTool) {
       ATH_CHECK(m_vtxValidTool->matchVertices(*vertices));
       ATH_MSG_DEBUG("Hard scatter classification type: " << InDetVertexTruthMatchUtils::classifyHardScatter(*vertices) << ", vertex container size = " << vertices->size());
    }
    m_monPlots->fill(*vertices, truthHSVertices, truthPUVertices);

    ATH_MSG_DEBUG("Filling vertex/event info monitoring plots");
    //Filling vertexing plots for the reconstructed hard-scatter as a function of mu
    m_monPlots->fill(*vertices, puEvents);
  } else {
    //FIXME: Does this happen for single particles?
    ATH_MSG_WARNING("Skipping vertexing plots.");
  }


  if( not m_truthVertexContainerName.key().empty()){
    // get truth vertex container name - m_truthVertexContainerName
    SG::ReadHandle<xAOD::TruthVertexContainer> truthVrt = SG::ReadHandle<xAOD::TruthVertexContainer>( m_truthVertexContainerName );

    //
    //Get the HS vertex position from the truthVertexContainer
    //FIXME: Add plots w.r.t truth HS positions (vertexing plots)
    //
    const xAOD::TruthVertex* truthVertex = 0;
    if (truthVrt.isValid()) {
      const auto& stdVertexContainer = truthVrt->stdcont();
      //First truth particle vertex?
      auto findVtx = std::find_if(stdVertexContainer.rbegin(), stdVertexContainer.rend(), acceptTruthVertex);
      truthVertex = (findVtx == stdVertexContainer.rend()) ? nullptr : *findVtx;
    } else {
      ATH_MSG_WARNING("Cannot open " << m_truthVertexContainerName.key() << " truth vertex container");
    }
    if (not truthVertex) ATH_MSG_INFO ("Truth vertex did not pass cuts");
  }
  //
  //Counters for cutflow
  //
  unsigned int nSelectedTruthTracks(0), nSelectedRecoTracks(0), nSelectedMatchedTracks(0), nAssociatedTruth(0), nMissingAssociatedTruth(0), nFakeTracks(0), nTruths(0);

  CutFlow tmp_truth_cutflow( m_truthSelectionTool.get() ?  m_truthSelectionTool->nCuts() : 0 );
  
  //
  //Loop over all reconstructed tracks
  //
  // If writing ntuples, use a truth-to-track(s) cache to handle truth matching.
  // Based on the assumption that multiple tracks can (albeit rarely) share the same truth association.
  std::map<const xAOD::TruthParticle*, std::vector<const xAOD::TrackParticle*>> cacheTruthMatching {};
  //
  std::vector<const xAOD::TrackParticle*> selectedTracks {};
  selectedTracks.reserve(tracks->size());
  unsigned int nTrackBAT = 0, nTrackSTD = 0, nTrackANT = 0, nTrackTOT = 0;
  for (const auto thisTrack: *tracks) {
    //FIXME: Why is this w.r.t the primary vertex?
    const asg::AcceptData& accept = m_trackSelectionTool->accept(*thisTrack, primaryvertex);
    if (m_useTrackSelection and not accept) continue;
    fillTrackCutFlow(accept); //?? Is this equal???

    selectedTracks.push_back(thisTrack);
    //Number of selected reco tracks
    nSelectedRecoTracks++;
    //Fill plots for selected reco tracks, hits / perigee / ???
    std::bitset<xAOD::TrackPatternRecoInfo::NumberOfTrackRecoInfo>  patternInfo = thisTrack->patternRecoInfo();
    bool isBAT = patternInfo.test(xAOD::TrackPatternRecoInfo::TRTSeededTrackFinder);
    bool isANT = patternInfo.test(xAOD::TrackPatternRecoInfo::SiSpacePointsSeedMaker_LargeD0);
    bool isSTD = not isBAT and not isANT;
    if(isBAT) nTrackBAT++;
    if(isSTD) nTrackSTD++;
    if(isANT) nTrackANT++;
    nTrackTOT++;
    m_monPlots->fill(*thisTrack);                                      
    m_monPlots->fill(*thisTrack, puEvents, nVertices);  //fill mu dependent plots
    const xAOD::TruthParticle* associatedTruth = getAsTruth.getTruth(thisTrack);
    float prob = getMatchingProbability(*thisTrack); 

    // This is where the Fake, and Really Fake fillers need to go. Where would the really really fakes go?
    if (associatedTruth) {
      nAssociatedTruth++;

      // if there is associated truth also a truth selection tool was retrieved.
      IAthSelectionTool::CutResult passed( m_truthSelectionTool ?  m_truthSelectionTool->accept(associatedTruth) : IAthSelectionTool::CutResult(0) );
      if (m_truthSelectionTool) {
        //FIXME: What is this for???
        tmp_truth_cutflow.update( passed.missingCuts() );
      }

      if ((not std::isnan(prob)) and (prob > m_lowProb) and passed) {
        nSelectedMatchedTracks++; 
        bool truthIsFromB = false;
        if ( m_doTruthOriginPlots and m_trackTruthOriginTool->isFrom(associatedTruth, 5) ) {
          truthIsFromB = true;
        }
        m_monPlots->fill(*thisTrack, *associatedTruth, truthIsFromB); // Make plots requiring matched truth
      }
    }

    const bool isAssociatedTruth = associatedTruth ? true : false;
    const bool isFake = not std::isnan(prob) ? (prob < m_lowProb) : true;

    if(isFake) nFakeTracks++;
    if(!isAssociatedTruth) nMissingAssociatedTruth++;
    m_monPlots->fillFakeRate(*thisTrack, isFake, isAssociatedTruth, puEvents, nVertices);

    if (m_fillTruthToRecoNtuple) {
      // Decorate track particle with extra flags
      decorateTrackParticle(*thisTrack, accept);

      if (isAssociatedTruth) {
        // Decorate truth particle with extra flags
        decorateTruthParticle(*associatedTruth, m_truthSelectionTool->accept(associatedTruth));

        // Cache truth-to-track associations
        auto cachedAssoc = cacheTruthMatching.find(associatedTruth);
        // Check if truth particle already present in cache
        if (cachedAssoc == cacheTruthMatching.end()) {
          // If not yet present, add truth-to-track association in cache
          cacheTruthMatching[associatedTruth] = {thisTrack};
        }
        else {
          // If already present, cache additional track associations (here multiple track particle can be linked to the same truth particle)
          cachedAssoc->second.push_back(thisTrack);
        }
      }
      else {
        // Fill track only entries with dummy truth values
        m_monPlots->fillNtuple(*thisTrack);
      }
    }
  }
  if (m_fillTruthToRecoNtuple) {
    // Now fill all truth-to-track associations
    // Involves some double-filling of truth particles in cases where multiple tracks share the same truth association,
    // these duplicates can be filtered in the ntuple by selecting only the 'best matched' truth-associated track particles.
    for (auto& cachedAssoc: cacheTruthMatching) {
      const xAOD::TruthParticle* thisTruth = cachedAssoc.first;

      // Decorate that this truth particle is being filled to prevent double counting in truth particle loop
      m_dec_hasTruthFilled(*thisTruth) = true;

      // Sort all associated tracks by truth match probability
      std::sort(cachedAssoc.second.begin(), cachedAssoc.second.end(), 
        [](const xAOD::TrackParticle* t1, const xAOD::TrackParticle* t2) { return getMatchingProbability(*t1) > getMatchingProbability(*t2); }
      );

      // Fill all tracks associated to to this truth particle, also recording 'truth match ranking' as index in probability-sorted vector of matched tracks
      for (int itrack = 0; itrack < (int) cachedAssoc.second.size(); itrack++) {
          const xAOD::TrackParticle* thisTrack = cachedAssoc.second[itrack];
          
          // Fill track entries with truth association
          m_monPlots->fillNtuple(*thisTrack, *thisTruth, itrack);
      }
    }
  }
  m_monPlots->fill(nTrackANT, nTrackSTD, nTrackBAT, puEvents, nVertices);
  m_monPlots->fill(nTrackTOT, puEvents, nVertices);

  //FIXME: I don't get why... this is here
  if (m_truthSelectionTool.get()) {
    ATH_MSG_DEBUG( CutFlow(tmp_truth_cutflow).report(m_truthSelectionTool->names()) );
    std::lock_guard<std::mutex> lock(m_mutex);
    m_truthCutFlow.merge(std::move(tmp_truth_cutflow));
  }
  
  //
  //TruthParticle loop to fill efficiencies
  //
  for (int itruth = 0; itruth < (int) truthParticlesVec.size(); itruth++) {  // Outer loop over all truth particles
    nTruths++;
    const xAOD::TruthParticle* thisTruth = truthParticlesVec[itruth];

    // if the vector of truth particles is not empty also a truthSelectionTool was retrieved
    const IAthSelectionTool::CutResult accept = m_truthSelectionTool->accept(thisTruth);
    if (accept) {
      ++nSelectedTruthTracks; // total number of truth which pass cuts per event
      bool isEfficient(false); // weight for the trackeff histos
      
      m_monPlots->fill(*thisTruth); // This is filling truth-only plots
      //
      //Loop over reco tracks to find the match
      //
      const xAOD::TrackParticle* matchedTrack = nullptr;
      for (const auto& thisTrack: selectedTracks) { // Inner loop over selected track particleis
        const xAOD::TruthParticle* associatedTruth = getAsTruth.getTruth(thisTrack);
        if (associatedTruth && associatedTruth == thisTruth) {
          float prob = getMatchingProbability(*thisTrack);
          if (not std::isnan(prob) && prob > m_lowProb) {
            isEfficient = true;
            matchedTrack = thisTrack;
            break;
          }
        }
      }
      ATH_MSG_DEBUG("Filling efficiency plots info monitoring plots");
      m_monPlots->fillEfficiency(*thisTruth, *matchedTrack, isEfficient, puEvents, nVertices);
    }
    
    if (m_fillTruthToRecoNtuple) {
      // Skip if already filled in track loop
      if (hasTruthFilled(*thisTruth)) continue;

      // Decorate truth particle with extra flags
      decorateTruthParticle(*thisTruth, accept);
      
      // Fill truth only entries with dummy track values
      m_monPlots->fillNtuple(*thisTruth);
    }
  }

  if (nSelectedRecoTracks == nMissingAssociatedTruth) {
    if (not m_truthParticleName.key().empty()) {
      ATH_MSG_DEBUG("NO TRACKS had associated truth.");
    }
  } else {
    ATH_MSG_DEBUG(nAssociatedTruth << " tracks out of " << tracks->size() << " had associated truth.");
  }

  m_monPlots->fillCounter(nSelectedRecoTracks, InDetPerfPlot_nTracks::SELECTEDRECO);
  m_monPlots->fillCounter(tracks->size(), InDetPerfPlot_nTracks::ALLRECO);
  m_monPlots->fillCounter(nSelectedTruthTracks, InDetPerfPlot_nTracks::SELECTEDTRUTH);
  m_monPlots->fillCounter(nTruths, InDetPerfPlot_nTracks::ALLTRUTH);
  m_monPlots->fillCounter(nAssociatedTruth, InDetPerfPlot_nTracks::ALLASSOCIATEDTRUTH);
  m_monPlots->fillCounter(nSelectedMatchedTracks, InDetPerfPlot_nTracks::MATCHEDRECO);
  
  // Tracking In Dense Environment
  if (!m_doTrackInJetPlots) return StatusCode::SUCCESS;

  if (m_jetContainerName.key().empty()) {
    ATH_MSG_WARNING("Not a valid key for the jetContainer, skipping TIDE plots");
    return StatusCode::SUCCESS;
  }

  SG::ReadHandle<xAOD::JetContainer> jets(m_jetContainerName);
  SG::AuxElement::ConstAccessor<std::vector<ElementLink<xAOD::IParticleContainer> > > ghosttruth("GhostTruth");
  SG::AuxElement::ConstAccessor<int> btagLabel("HadronConeExclTruthLabelID");
  
  if (not jets.isValid() or truthParticlesVec.empty()) {
    ATH_MSG_WARNING(
      "Cannot open " << m_jetContainerName <<
        " jet container or TruthParticles truth particle container. Skipping jet plots.");
  } else {
    for (const auto thisJet: *jets) {         // The big jets loop
      if (not passJetCuts(*thisJet)) {
        continue;
      }
      bool isBjet = false; 
      if (!btagLabel.isAvailable(*thisJet)){
           ATH_MSG_WARNING("Failed to extract b-tag truth label from jet");
      }
      else{
        isBjet = (btagLabel(*thisJet) == 5); 
      }
      if(!ghosttruth.isAvailable(*thisJet)) {
           ATH_MSG_WARNING("Failed to extract ghost truth particles from jet");
      } else {
        for(const auto& el : ghosttruth(*thisJet)){ 
          if(el.isValid()) {
            const xAOD::TruthParticle *truth = static_cast<const xAOD::TruthParticle*>(*el);
            if (thisJet->p4().DeltaR(truth->p4()) > m_maxTrkJetDR) {
                continue;
            }

            const IAthSelectionTool::CutResult accept = m_truthSelectionTool->accept(truth);
              
            if(!accept) continue;
            bool isEfficient(false);

            for (auto thisTrack: *tracks) {
              if (m_useTrackSelection and not (m_trackSelectionTool->accept(*thisTrack, primaryvertex))) {
                continue;
              }

              const xAOD::TruthParticle* associatedTruth = getAsTruth.getTruth(thisTrack);
              if (associatedTruth and associatedTruth == truth) {
                float prob = getMatchingProbability(*thisTrack);
                if (not std::isnan(prob) && prob > m_lowProb) {
                  isEfficient = true;
                  break;
                }
              }
            }

            bool truthIsFromB = false;
            if ( m_doTruthOriginPlots and m_trackTruthOriginTool->isFrom(truth, 5) ) {
              truthIsFromB = true;
            }
            m_monPlots->fillEfficiency(*truth, *thisJet, isEfficient, isBjet, truthIsFromB);
          }
        }
      }

      for (auto thisTrack: *tracks) {    // The beginning of the track loop
        if (m_useTrackSelection and not (m_trackSelectionTool->accept(*thisTrack, primaryvertex))) {
          continue;
        }
        if (thisJet->p4().DeltaR(thisTrack->p4()) > m_maxTrkJetDR) {
          continue;
        }
        float prob = getMatchingProbability(*thisTrack);
        if(std::isnan(prob)) prob = 0.0;
      
        const xAOD::TruthParticle* associatedTruth = getAsTruth.getTruth(thisTrack); 
        const bool unlinked = (associatedTruth==nullptr);
        const bool isFake = (associatedTruth && prob < m_lowProb);
        bool truthIsFromB = false;
        if ( m_doTruthOriginPlots and m_trackTruthOriginTool->isFrom(associatedTruth, 5) ) {
          truthIsFromB = true;
        }
        m_monPlots->fill(*thisTrack, *thisJet, isBjet, isFake, unlinked, truthIsFromB);                                   
        if (associatedTruth){
          m_monPlots->fillFakeRate(*thisTrack, *thisJet, isFake, isBjet, truthIsFromB);
       }
      }
    }
  } // loop over jets

  return StatusCode::SUCCESS;
}

void InDetPhysValMonitoringTool::decorateTrackParticle(const xAOD::TrackParticle & track, const asg::AcceptData & passed) const {
  // Decorate outcome of track selection
  m_dec_passedTrackSelection(track) = (bool)(passed);
}

void InDetPhysValMonitoringTool::decorateTruthParticle(const xAOD::TruthParticle & truth, const IAthSelectionTool::CutResult & passed) const {  
  // Decorate outcome of truth selection
  m_dec_passedTruthSelection(truth) = (bool)(passed);

  // Decorate if selected by pileup switch
  m_dec_selectedByPileupSwitch(truth) = isSelectedByPileupSwitch(truth);
}

bool InDetPhysValMonitoringTool::hasTruthFilled(const xAOD::TruthParticle & truth) const{
  if (!m_acc_hasTruthFilled.isAvailable(truth)) {
    ATH_MSG_DEBUG("Truth particle not yet filled in ntuple");
    return false;
  }
  return m_acc_hasTruthFilled(truth);
}

bool InDetPhysValMonitoringTool::isSelectedByPileupSwitch(const xAOD::TruthParticle & truth) const{
  if (!m_acc_selectedByPileupSwitch.isAvailable(truth)) {
      ATH_MSG_DEBUG("Selected by pileup switch decoration requested from a truth particle but not available");
      return false;
  }
  return m_acc_selectedByPileupSwitch(truth);
}

void InDetPhysValMonitoringTool::markSelectedByPileupSwitch(const std::vector<const xAOD::TruthParticle*> & truthParticles) const{
  for (const auto& thisTruth: truthParticles) {
    m_dec_selectedByPileupSwitch(*thisTruth) = true;
  }
=======
  m_onlyInsideOutTracks(false),
  m_TrkSelectPV(false),
  m_trackSelectionTool("InDet::InDetTrackSelectionTool/TrackSelectionTool"),
  m_truthSelectionTool("AthTruthSelectionTool", this),
  m_prospectsMatched(6, 0),
  m_twoMatchedEProb(0),
  m_threeMatchedEProb(0),
  m_fourMatchedEProb(0),
  m_truthCounter(0),
  m_truthCutCounters{},
  m_fillTIDEPlots(false),
  m_fillExtraTIDEPlots(false) {
  declareProperty("TrackParticleContainerName", m_trkParticleName = "InDetTrackParticles"); // Aug 8th: switch
                                                                                            // "InDetTrackParticles"
                                                                                            // with
                                                                                            // "TrackCollection_tlp5_CombinedInDetTracks"
                                                                                            // (v1)...no tracks appeared
  declareProperty("TruthParticleContainerName", m_truthParticleName = "TruthParticles");
  declareProperty("VertexContainerName", m_vertexContainerName = "PrimaryVertices");
  declareProperty("TruthVertexContainerName", m_truthVertexContainerName = "TruthVertices");
  declareProperty("EventInfoContainerName", m_eventInfoContainerName = "EventInfo");
  declareProperty("useTrackSelection", m_useTrackSelection);
  declareProperty("useTrkSelectPV", m_TrkSelectPV);
  declareProperty("onlyInsideOutTracks", m_onlyInsideOutTracks);
  declareProperty("TrackSelectionTool", m_trackSelectionTool);
  declareProperty("TruthSelectionTool", m_truthSelectionTool);
  declareProperty("FillTrackInJetPlots", m_fillTIDEPlots);
  declareProperty("FillExtraTrackInJetPlots", m_fillExtraTIDEPlots);
  declareProperty("jetContainerName", m_jetContainerName = "AntiKt4TruthJets");
  declareProperty("maxTrkJetDR", m_maxTrkJetDR = 0.4);
  declareProperty("DirName", m_dirName = "IDPerformanceMon/");
  declareProperty("SubFolder", m_folder);
  declareProperty("PileupSwitch", m_pileupSwitch = "All");
}

InDetPhysValMonitoringTool::~InDetPhysValMonitoringTool() {
}

StatusCode
InDetPhysValMonitoringTool::initialize() {
  ATH_MSG_DEBUG("Initializing " << name() << "...");
  ATH_CHECK(ManagedMonitorToolBase::initialize());
  // Get the track selector tool only if m_useTrackSelection is true;
  // first check for consistency i.e. that there is a trackSelectionTool if you ask
  // for trackSelection
  if (m_useTrackSelection) {
    ATH_CHECK(m_trackSelectionTool.retrieve());
    if (not m_trackSelectionTool) {
      ATH_MSG_ERROR(
        "\033[1;31mYou have chosen to use track selection, but no track selection tool was configured\033[0m\n");
      return StatusCode::FAILURE;
    }
  }
  ATH_CHECK(m_truthSelectionTool.retrieve());
  m_truthCutCounters = m_truthSelectionTool->counters();
  m_monPlots = std::move(std::unique_ptr<InDetRttPlots> (new InDetRttPlots(0, m_dirName + m_folder)));
  m_monPlots->SetFillExtraTIDEPlots(m_fillExtraTIDEPlots);
  return StatusCode::SUCCESS;
}

StatusCode
InDetPhysValMonitoringTool::fillHistograms() {
  ATH_MSG_DEBUG("Filling hists " << name() << "...");
  // function object could be used to retrieve truth: IDPVM::CachedGetAssocTruth getTruth;
  const char* debugBacktracking = std::getenv("BACKTRACKDEBUG");

  // retrieve trackParticle container
  const auto ptracks = getContainer<xAOD::TrackParticleContainer>(m_trkParticleName);
  if (not ptracks) {
    return StatusCode::FAILURE;
  }
  if(debugBacktracking){
    std::cout<<"Rey: Start of new event \n";
    std::cout<<"Finn: Number of particles in container: "<<ptracks->size()<<"\n";
  }
  //
  // retrieve truthParticle container
  std::vector<const xAOD::TruthParticle*> truthParticlesVec = getTruthParticles();
  // IDPVM::TrackTruthLookup getAsTruth(ptracks, &truthParticlesVec); //caches everything upon construction
  IDPVM::CachedGetAssocTruth getAsTruth; // only cache one way, track->truth, not truth->tracks
  //
  bool incFake = false;
  int nMuEvents = 0;
  const xAOD::TruthPileupEventContainer* truthPileupEventContainer = 0;
  if (incFake) {
    ATH_MSG_VERBOSE("getting TruthPileupEvents container");
    const char* truthPUEventCollName =
      evtStore()->contains<xAOD::TruthPileupEventContainer>("TruthPileupEvents") ? "TruthPileupEvents" :
      "TruthPileupEvent";
    evtStore()->retrieve(truthPileupEventContainer, truthPUEventCollName);
    nMuEvents = (int) truthPileupEventContainer->size();
  }
  ATH_MSG_DEBUG("Filling vertex plots");
  const xAOD::VertexContainer* pvertex = getContainer<xAOD::VertexContainer>(m_vertexContainerName);
  const xAOD::Vertex* pvtx = nullptr;
  if (pvertex and not pvertex->empty()) {
    ATH_MSG_DEBUG("Number of vertices retrieved for this event " << pvertex->size());
    const auto& stdVertexContainer = pvertex->stdcont();
    // find last non-dummy vertex; note *usually* there is only one (or maybe zero)
    auto findVtx = std::find_if(stdVertexContainer.rbegin(), stdVertexContainer.rend(), acceptVertex);
    pvtx = (findVtx == stdVertexContainer.rend()) ? nullptr : *findVtx;
    m_monPlots->fill(*pvertex);
  } else {
    ATH_MSG_WARNING("Skipping vertexing plots.");
  }
  ATH_MSG_DEBUG("Filling vertex/event info monitoring plots");
  const xAOD::EventInfo* pei = getContainer<xAOD::EventInfo>(m_eventInfoContainerName);
  if (pei and pvertex) {
    m_monPlots->fill(*pvertex, *pei);
  } else {
    ATH_MSG_WARNING("Skipping vertexing plots using EventInfo.");
  }

  // get truth vertex container name - m_truthVertexContainerName
  const xAOD::TruthVertexContainer* truthVrt = getContainer<xAOD::TruthVertexContainer>(m_truthVertexContainerName);
  const xAOD::TruthVertex* truthVertex = 0;
  if (truthVrt) {
    if (!m_TrkSelectPV) {
      ATH_MSG_VERBOSE("size of TruthVertex container = " << truthVrt->size());
    }
    const auto& stdVertexContainer = truthVrt->stdcont();
    auto findVtx = std::find_if(stdVertexContainer.rbegin(), stdVertexContainer.rend(), acceptTruthVertex);
    truthVertex = (findVtx == stdVertexContainer.rend()) ? nullptr : *findVtx;
  } else {
    ATH_MSG_WARNING("Cannot open " << m_truthVertexContainerName << " truth vertex container");
  }
  if (not truthVertex) ATH_MSG_INFO ("Truth vertex did not pass cuts");
  unsigned int num_truth_selected(0), nSelectedTracks(0), num_truthmatch_match(0);
  // the truth matching probability must not be <= 0., otherwise the tool will seg fault in case of missing truth (e.g.
  // data):
  const float minProbEffLow(0.50); // if the probability of a match is less than this, we call it a fake
  //    const float minProbEffHigh(0.80); //if the probability of a match is higher than this, it either feeds the NUM
  // or is a duplicate
  // VJ - Mar 14, 2016 - even for effs, use 0.51
  const float minProbEffHigh(0.5); // if the probability of a match is higher than this, it either feeds the NUM or is
                                   // a duplicate

  // check if we are doing track selection relative to PV (or the default, which is the Beam Spot)
  if (!m_TrkSelectPV) {
    pvtx = nullptr;
  }

  bool fillVtx = true; // fill PV plots in fillSpectrum only once

  // Main track loop, filling Track-only, Track 'n' Truth with good matching probability (meas, res, & pull), and Fakes
  std::vector<int> incTrkNum = {
    0, 0, 0
  };
  m_truthSelectionTool->clearCounters();

  // dummy variables
  int base(0), hasTruth(0), hashighprob(0), passtruthsel(0);

  for (const auto& thisTrack: *ptracks) {
    m_monPlots->fillSpectrum(*thisTrack); // This one needs prob anyway, why not rearrange & eliminate
                                          // getMatchingProbability from RttPlots? 5-17-16
    const xAOD::TruthParticle* associatedTruth = getAsTruth.getTruth(thisTrack);
    float prob = getMatchingProbability(*thisTrack);
    if (!m_TrkSelectPV && truthVertex) {
      m_monPlots->fillSpectrum(*thisTrack, *truthVertex);
    }
    if (m_TrkSelectPV && pvtx) {
      m_monPlots->fillSpectrum(*thisTrack, *pvtx, fillVtx);
    }
    fillVtx = false;

    if (m_useTrackSelection) {
      if (!(m_trackSelectionTool->accept(*thisTrack, pvtx))) {
        continue;
      }
    }
    if (m_onlyInsideOutTracks and(not isInsideOut(*thisTrack))) {
      continue; // not an inside-out track
    }
    ++nSelectedTracks;                                                    // increment number of selected tracks
    m_monPlots->fill(*thisTrack);                                         // Make all the plots requiring only
                                                                          // trackParticle
    // const float absTrackEta = (thisTrack->pt() >1e-7) ? std::abs(thisTrack->eta()) : std::nan("");
    const float absTrackEta = std::abs(safelyGetEta(thisTrack));
    const unsigned int idx = binIndex(absTrackEta, ETA_PARTITIONS);
    if (idx < incTrkNum.size()) {
      ++incTrkNum[idx];
    }

    // This is where the BMR, Fake, and Really Fake fillers need to go.
    float BMR_w(0), RF_w(0); // weights for filling the Bad Match & Fake Rate plots
    float Prim_w(0), Sec_w(0), Unlinked_w(0);  // weights for the fake plots

    if (associatedTruth) m_monPlots->track_vs_truth(*thisTrack, *associatedTruth, prob);

    if (prob < minProbEffHigh) {
      BMR_w = 1;
    }
    if (prob < minProbEffLow) {
      RF_w = 1;
    }
    m_monPlots->fillBMR(*thisTrack, BMR_w);
    m_monPlots->fillRF(*thisTrack, RF_w);

    if (!associatedTruth) {
      m_monPlots->fillSpectrumUnlinked2(*thisTrack);
      Unlinked_w = 1; // Unlinked, set weight to 1
    }
    base += 1;
    if (associatedTruth) {
      hasTruth += 1;
      if (prob < minProbEffLow) { // nan will also fail this test
        const bool isFake = (prob < minProbEffLow);
        m_monPlots->fillFakeRate(*thisTrack, isFake);
        if ((associatedTruth->barcode() < 200e3)and(associatedTruth->barcode() != 0)) {
          Prim_w = 1; // Fake Primary, set weight to 1
        }
        if (associatedTruth->barcode() >= 200e3) {
          Sec_w = 1;                                         // Fake Secondary, set weight to 1
        }
      }
      if (prob > minProbEffLow) {
        hashighprob += 1;
      }
      if (m_truthSelectionTool->accept(associatedTruth)) {
        passtruthsel += 1;
      }
      if ((prob > minProbEffLow) and m_truthSelectionTool->accept(associatedTruth)) {
        m_monPlots->fill(*thisTrack, *associatedTruth); // Make all plots requiring both truth & track (meas, res, &
                                                        // pull)
      }
    }

    m_monPlots->fillLinkedandUnlinked(*thisTrack, Prim_w, Sec_w, Unlinked_w);
  }
  ATH_MSG_DEBUG(m_truthSelectionTool->str());
  const auto& tmp = m_truthSelectionTool->counters(); // get array of counters for the cuts

  unsigned idx(0);
  for (auto& i:m_truthCutCounters) {
    i += tmp[idx++]; // i=sum of all the individual counters on each cut.
  }
  int nTruths(0), nInsideOut(0), nOutsideIn(0);
  std::vector<int> incTrkDenom = {
    0, 0, 0
  };

  // This is the beginning of the Nested Loop, built mainly for the Efficiency Plots
  if (debugBacktracking) {
    std::cout << "Start of new nested loop event ------------------------------------------------ \n";
  }
  // preselect tracks to do efficiency calculation
  std::vector<const xAOD::TrackParticle*> selectedTracks {};
  selectedTracks.reserve(ptracks->size());
  for (const auto& thisTrack: *ptracks) {
    if (m_useTrackSelection and not (m_trackSelectionTool->accept(*thisTrack, pvtx))) {
      continue;
    }
    const bool insideOut = isInsideOut(*thisTrack);
    if (insideOut) {
      nInsideOut += 1;
    } else {
      nOutsideIn += 1;
    }
    if (m_onlyInsideOutTracks and(not insideOut)) {
      continue;
    }
    selectedTracks.push_back(thisTrack);
  }

  ATH_MSG_DEBUG("Starting nested loop efficiency calculation");
  for (int itruth = 0; itruth < (int) truthParticlesVec.size(); itruth++) {  // Outer loop over all truth particles
    nTruths += 1;
    const xAOD::TruthParticle* thisTruth = truthParticlesVec[itruth];
    m_monPlots->fillSpectrum(*thisTruth);

    if (thisTruth->pdgId() == 22) {
      if (thisTruth->nParents() == 0) {
        m_monPlots->prim_photon_fill(*thisTruth);
      } else {
        m_monPlots->brem_photon_fill(*thisTruth);
      }
    }

    const bool accept = m_truthSelectionTool->accept(thisTruth);
    if (accept) {
      ++m_truthCounter; // total number of truth tracks which pass cuts
      ++num_truth_selected; // total number of truth which pass cuts per event
      const float absTruthEta = std::abs(safelyGetEta(thisTruth));
      const unsigned int idx = binIndex(absTruthEta, ETA_PARTITIONS);
      if (idx < incTrkNum.size()) {
        ++incTrkNum[idx];
      }

      // LMTODO add this Jain/Swift
      bool addsToEfficiency(true); // weight for the trackeff histos

      if (debugBacktracking) {
        float lepton_w(0);
        std::cout << "Barcode: " << thisTruth->barcode() << "\n";
        std::cout << "PDGId: " << thisTruth->pdgId() << "\n";
        std::cout << "Number of Parents: " << thisTruth->nParents() << "\n";

        if (thisTruth->hasProdVtx()) {
          const xAOD::TruthVertex* vtx = thisTruth->prodVtx();
          double prod_rad = vtx->perp();
          double prod_z = vtx->z();
          std::cout << "Vertex Radial Position: " << prod_rad << "\n";
          std::cout << "Vertex z Position: " << prod_z << "\n";
        }
        double Px = thisTruth->px();
        double Py = thisTruth->py();
        std::cout << "Px: " << Px << "\n";
        std::cout << "Py: " << Py << "\n";
        std::cout << "Pz: " << thisTruth->pz() << "\n";
        constexpr int electronId = 11;
        if (thisTruth->absPdgId() == electronId) {
          double PtSquared = (Px * Px) + (Py * Py);
          if (PtSquared < 9000000) {
            lepton_w = 1;
          }
        }
        m_monPlots->lepton_fill(*thisTruth, lepton_w);
      }// end of debugging backtracking section

      if(debugBacktracking){
	if(thisTruth->hasProdVtx()){
	  const xAOD::TruthVertex* vtx = thisTruth->prodVtx();
	  double prod_rad = vtx->perp();
	  if(prod_rad < 300){
	    double min_dR = 10;
	    float bestmatch = 0;
	    double best_inverse_delta_pt(0);
	    double inverse_delta_pt(0);
	    for(const auto& thisTrack: selectedTracks){
	      float prob(0);
	      if((thisTrack->qOverP()) * (thisTruth->auxdata<float>("qOverP")) > 0){
		prob = getMatchingProbability(*thisTrack);
		double track_theta = thisTrack->theta();
		double truth_theta = thisTruth->auxdata< float >("theta");
		double truth_eta = thisTruth->eta();
		double track_eta = -std::log(std::tan(track_theta/2));
		
		double track_pt = thisTrack->pt() * 0.001;
		double truth_pt = thisTruth->pt() * 0.001;
	      
		if((track_pt != 0) and (truth_pt != 0))  inverse_delta_pt = ((1./track_pt) - (1./truth_pt));

		double delta_eta = track_eta - truth_eta;
		double delta_theta = track_theta - truth_theta;
		double delta_R = sqrt(delta_eta * delta_eta + delta_theta * delta_theta);
		if(min_dR > delta_R) min_dR = delta_R;
	      }
	      if(prob >= bestmatch) best_inverse_delta_pt = inverse_delta_pt;
	      bestmatch = std::max(prob, bestmatch);
	    }
	    m_monPlots->minDR(min_dR, prod_rad, bestmatch, best_inverse_delta_pt);
	  }
	}
      }

      std::vector <std::pair<float, const xAOD::TrackParticle*> > matches; // Vector of pairs:
                                                                           // <truth_matching_probability, track> if
                                                                           // prob > minProbEffLow (0.5)
      float bestMatch = 0;
      for (const auto& thisTrack: selectedTracks) { // Inner loop over selected track particles
        const xAOD::TruthParticle* associatedTruth = getAsTruth.getTruth(thisTrack);
        if (associatedTruth && associatedTruth == thisTruth) {
          float prob = getMatchingProbability(*thisTrack);
          if (not std::isnan(prob)) {
            bestMatch = std::max(prob, bestMatch);
            if (prob > minProbEffLow) {
              matches.push_back(std::make_pair(prob, thisTrack));
            }
            const bool isFake = (prob < minProbEffLow);
            m_monPlots->fillFakeRate(*thisTrack, isFake);
          }
        }
      }

      // count number of prospects and increment entry in vector for this event
      int deg_count = matches.size();
      if (deg_count <= 4) {
        ++m_prospectsMatched[deg_count];
      } else {
        ++m_prospectsMatched[5];
      }

      // special histograms for 1 or 2 matched tracks
      if (deg_count == 1) {
        m_monPlots->fillSingleMatch(*matches.at(0).second);
      } else if (deg_count == 2) {
        m_monPlots->fillTwoMatchDuplicate(matches[1].first, matches[0].first,
                                          *matches[1].second, *matches[0].second,
                                          *thisTruth);
      }
      // determine how many duplicate match probabilities
      // first create vector of probabilities and sort
      if (deg_count > 1) {
        std::vector<float> probs;
        for (int i = 0; i < deg_count; i++) {
          probs.push_back(matches[i].first);
        }
        std::sort(matches.begin(), matches.end(), sortProspects);
        // count duplicates
        float prev = matches[0].first;
        int nduplicates = 0;
        const std::array<int*, 5> matchedEProbs = {
          nullptr, nullptr, &m_twoMatchedEProb, &m_threeMatchedEProb, &m_fourMatchedEProb
        };
        for (int i = 1; i < deg_count; i++) {
          bool duplicate = std::fabs(matches[i].first - prev) < 1.e-9;
          if (duplicate) {
            ++nduplicates;
          }
          if (!duplicate || i == deg_count - 1) {
            if (nduplicates > 1) {
              (*(matchedEProbs[deg_count]))++;
            }
            nduplicates = 0;
            prev = matches[i].first;
          }
        }
      }

      // fill truth-only plots
      if (bestMatch >= minProbEffHigh) {
        ++num_truthmatch_match;
        const xAOD::TruthParticle* associatedTruth = matches.empty() ? nullptr : getAsTruth.getTruth(
          matches.back().second);
        if (!associatedTruth) {
          continue;
        }
        m_monPlots->fill(*associatedTruth); // This is filling truth-only plots:  m_TrackTruthInfoPlots
      } else {
        addsToEfficiency = false;
      }

      m_monPlots->fillEfficiency(*thisTruth, addsToEfficiency);
    } // end of the "if(accept)" loop
  }// End of Big truthParticle loop
  ATH_MSG_DEBUG("End of efficiency calculation");

  if (m_useTrackSelection) {
    for (const auto& thisTrack: *ptracks) { // Inner loop over all track particle
      if (m_useTrackSelection) {
        Root::TAccept trackAccept = m_trackSelectionTool->accept(*thisTrack, pvtx);
        fillTrackCutFlow(trackAccept);
      }
    }
  }

  if (num_truthmatch_match == 0) {
    ATH_MSG_INFO("NO TRACKS had associated truth.");
  } else {
    ATH_MSG_DEBUG(num_truthmatch_match << " tracks out of " << ptracks->size() << " had associated truth.");
  }
  m_monPlots->fillIncTrkRate(nMuEvents, incTrkNum, incTrkDenom);
  m_monPlots->fillCounter(nSelectedTracks, InDetPerfPlot_nTracks::SELECTED);
  m_monPlots->fillCounter(ptracks->size(), InDetPerfPlot_nTracks::ALL);
  m_monPlots->fillCounter(truthParticlesVec.size(), InDetPerfPlot_nTracks::TRUTH);
  m_monPlots->fillCounter(num_truthmatch_match, InDetPerfPlot_nTracks::TRUTH_MATCHED);
  // Tracking In Dense Environment
  if (m_fillTIDEPlots && !m_jetContainerName.empty()) {
    return doJetPlots(ptracks, getAsTruth, pvtx);
  }     // if TIDE
  // ATH_MSG_INFO(getTruth.report());
  return StatusCode::SUCCESS;
>>>>>>> release/21.0.127
}

StatusCode
InDetPhysValMonitoringTool::bookHistograms() {
<<<<<<< HEAD
  ATH_MSG_INFO("Booking hists " << name() << "with detailed level: " << m_detailLevel);
=======
  ATH_MSG_INFO("Booking hists " << name() << "...");
  m_monPlots->setDetailLevel(100); // DEBUG, enable expert histograms
>>>>>>> release/21.0.127
  m_monPlots->initialize();
  std::vector<HistData> hists = m_monPlots->retrieveBookedHistograms();
  for (auto hist : hists) {
    ATH_CHECK(regHist(hist.first, hist.second, all)); // ??
  }
  // do the same for Efficiencies, but there's a twist:
  std::vector<EfficiencyData> effs = m_monPlots->retrieveBookedEfficiencies();
  for (auto& eff : effs) {
    // reg**** in the monitoring baseclass doesnt have a TEff version, but TGraph *
    // pointers just get passed through, so we use that method after an ugly cast
    ATH_CHECK(regGraph(reinterpret_cast<TGraph*>(eff.first), eff.second, all)); // ??
<<<<<<< HEAD
  }
  // register trees for ntuple writing
  if (m_fillTruthToRecoNtuple) {
    std::vector<TreeData> trees = m_monPlots->retrieveBookedTrees();
    for (auto& t : trees) {
      ATH_CHECK(regTree(t.first, t.second, all));
    }
=======
>>>>>>> release/21.0.127
  }
  return StatusCode::SUCCESS;
}

StatusCode
InDetPhysValMonitoringTool::procHistograms() {
  ATH_MSG_INFO("Finalising hists " << name() << "...");
<<<<<<< HEAD
  //TODO: ADD Printouts for Truth??
=======
  ATH_MSG_INFO(" number with one matched reco = " << m_prospectsMatched[1]);
  ATH_MSG_INFO(" number with two matched reco = " << m_prospectsMatched[2]);
  ATH_MSG_INFO(" number with two matched reco where there is a duplicate mc_probability = " << m_twoMatchedEProb);
  ATH_MSG_INFO(" number with three matched reco = " << m_prospectsMatched[3]);
  ATH_MSG_INFO(" number with three matched reco where there is a duplicate mc_probability = " << m_threeMatchedEProb);
  ATH_MSG_INFO(" number with four matched reco = " << m_prospectsMatched[4]);
  ATH_MSG_INFO(" number with four matched reco where there is a duplicate mc_probability = " << m_fourMatchedEProb);
  ATH_MSG_INFO(" number with five+ matched reco = " << m_prospectsMatched[5]);
  ATH_MSG_INFO(" total number of truth particles which pass cuts = " << m_truthCounter);
  ATH_MSG_INFO(
    " total number of truth particles which pass and match a reco track (including duplicates) = " <<
      m_prospectsMatched[1] +
      m_prospectsMatched[2] + m_prospectsMatched[3] + m_prospectsMatched[4] + m_prospectsMatched[5]);
  ATH_MSG_INFO(" number zero matched reco = " << m_prospectsMatched[0]);
  ATH_MSG_INFO(
    " total number of truth tracks, which have more than one matching reco track = " << m_prospectsMatched[2] +
      m_prospectsMatched[3] + m_prospectsMatched[4] + m_prospectsMatched[5]);
  ATH_MSG_INFO(
    " total number of truth tracks, which have more than one matching reco and atleast one duplicate mc_probability = " << m_twoMatchedEProb + m_threeMatchedEProb +
      m_fourMatchedEProb);
>>>>>>> release/21.0.127
  if (m_useTrackSelection) {
    ATH_MSG_INFO("");
    ATH_MSG_INFO("Now Cutflow for track cuts:");
    ATH_MSG_INFO("");
    for (int i = 0; i < (int) m_trackCutflow.size(); ++i) {
      ATH_MSG_INFO("number after " << m_trackCutflowNames[i] << ": " << m_trackCutflow[i]);
    }
<<<<<<< HEAD
  }

  ATH_MSG_INFO("");
  ATH_MSG_INFO("Cutflow for truth tracks:");
  if (m_truthSelectionTool.get()) {
    ATH_MSG_INFO("Truth selection report: " << m_truthCutFlow.report( m_truthSelectionTool->names()) );
  }
=======
  }

  ATH_MSG_INFO("");
  ATH_MSG_INFO("Cutflow for truth tracks:");
  unsigned int idx(0);
  for (const auto& cutName:m_truthSelectionTool->names()) {
    ATH_MSG_INFO("number after " << cutName << ": " << m_truthCutCounters[idx++]);
  }
>>>>>>> release/21.0.127
  if (endOfRunFlag()) {
    m_monPlots->finalize();
  }
  ATH_MSG_INFO("Successfully finalized hists");
  return StatusCode::SUCCESS;
}

const std::vector<const xAOD::TruthParticle*>
<<<<<<< HEAD
InDetPhysValMonitoringTool::getTruthParticles() const {
  // truthParticles.clear();
  std::vector<const xAOD::TruthParticle*> tempVec {};
  if (m_pileupSwitch == "All") {

    if (m_truthParticleName.key().empty()) {
      return tempVec;
    }
    SG::ReadHandle<xAOD::TruthParticleContainer> truthParticleContainer( m_truthParticleName);
    if (not truthParticleContainer.isValid()) {
=======
InDetPhysValMonitoringTool::getTruthParticles() {
  // truthParticles.clear();
  std::vector<const xAOD::TruthParticle*> tempVec {};
  if (m_truthParticleName.empty()) {
    return tempVec;
  }
  if (m_pileupSwitch == "All") {
    const xAOD::TruthParticleContainer* truthParticleContainer =
      (!m_truthParticleName.empty() ? getContainer<xAOD::TruthParticleContainer>(m_truthParticleName) : nullptr);
    if (not truthParticleContainer) {
>>>>>>> release/21.0.127
      return tempVec;
    }
    tempVec.insert(tempVec.begin(), truthParticleContainer->begin(), truthParticleContainer->end());
  } else {
    if (m_pileupSwitch == "HardScatter") {
      // get truthevent container to separate out pileup and hardscatter truth particles
<<<<<<< HEAD
      if (not m_truthEventName.key().empty()) {
      SG::ReadHandle<xAOD::TruthEventContainer> truthEventContainer( m_truthEventName);
      const xAOD::TruthEvent* event = (truthEventContainer.isValid()) ? truthEventContainer->at(0) : nullptr;
=======
      const xAOD::TruthEventContainer* truthEventContainer = nullptr;
      const std::string truthEventCollName =
        evtStore()->contains<xAOD::TruthEventContainer>("TruthEvents") ? "TruthEvents" : "TruthEvent";
      evtStore()->retrieve(truthEventContainer, truthEventCollName);
      const xAOD::TruthEvent* event = (truthEventContainer) ? truthEventContainer->at(0) : nullptr;
>>>>>>> release/21.0.127
      if (not event) {
        return tempVec;
      }
      const auto& links = event->truthParticleLinks();
      tempVec.reserve(event->nTruthParticles());
      for (const auto& link : links) {
<<<<<<< HEAD
        if (link.isValid()){
          tempVec.push_back(*link);
        }
      }
      }
    } else if (m_pileupSwitch == "PileUp") {
      if (not m_truthPileUpEventName.key().empty()) {
      ATH_MSG_VERBOSE("getting TruthPileupEvents container");
      // get truth particles from all pileup events
      SG::ReadHandle<xAOD::TruthPileupEventContainer> truthPileupEventContainer(m_truthPileUpEventName);
      if (truthPileupEventContainer.isValid()) {
=======
        tempVec.push_back(*link);
      }
    } else if (m_pileupSwitch == "PileUp") {
      ATH_MSG_VERBOSE("getting TruthPileupEvents container");
      // get truth particles from all pileup events
      const xAOD::TruthPileupEventContainer* truthPileupEventContainer = nullptr;
      const std::string truthPUEventCollName =
        evtStore()->contains<xAOD::TruthPileupEventContainer>("TruthPileupEvents") ? "TruthPileupEvents" :
        "TruthPileupEvent";
      evtStore()->retrieve(truthPileupEventContainer, truthPUEventCollName);
      if (truthPileupEventContainer) {
>>>>>>> release/21.0.127
        const unsigned int nPileup = truthPileupEventContainer->size();
        tempVec.reserve(nPileup * 200); // quick initial guess, will still save some time
        for (unsigned int i(0); i != nPileup; ++i) {
          auto eventPileup = truthPileupEventContainer->at(i);
          // get truth particles from each pileup event
          int ntruth = eventPileup->nTruthParticles();
          ATH_MSG_VERBOSE("Adding " << ntruth << " truth particles from TruthPileupEvents container");
          const auto& links = eventPileup->truthParticleLinks();
          for (const auto& link : links) {
            tempVec.push_back(*link);
          }
        }
      } else {
        ATH_MSG_ERROR("no entries in TruthPileupEvents container!");
      }
<<<<<<< HEAD
      }
=======
>>>>>>> release/21.0.127
    } else {
      ATH_MSG_ERROR("bad value for PileUpSwitch");
    }
  }
  return tempVec;
}

<<<<<<< HEAD
std::pair<const std::vector<const xAOD::TruthVertex*>, const std::vector<const xAOD::TruthVertex*>>
InDetPhysValMonitoringTool::getTruthVertices() const {

  std::vector<const xAOD::TruthVertex*> truthHSVertices = {};
  truthHSVertices.reserve(5);
  std::vector<const xAOD::TruthVertex*> truthPUVertices = {};
  truthPUVertices.reserve(100);
  const xAOD::TruthVertex* truthVtx = nullptr;

  bool doHS = false;
  bool doPU = false;
  if (m_pileupSwitch == "All") {
    doHS = true;
    doPU = true;
  }
  else if (m_pileupSwitch == "HardScatter") {
    doHS = true;
  }
  else if (m_pileupSwitch == "PileUp") {
    doPU = true;
  }
  else {
    ATH_MSG_ERROR("Bad value for PileUpSwitch: " << m_pileupSwitch);
  }

  if (doHS) {
    if (not m_truthEventName.key().empty()) {
      ATH_MSG_VERBOSE("Getting TruthEvents container.");
      SG::ReadHandle<xAOD::TruthEventContainer> truthEventContainer(m_truthEventName);
      if (truthEventContainer.isValid()) {
        for (const auto evt : *truthEventContainer) {
          truthVtx = evt->truthVertex(0);
          if (truthVtx) {
            truthHSVertices.push_back(truthVtx);
          }
        }
      }
      else {
        ATH_MSG_ERROR("No entries in TruthEvents container!");
      }
    }
  }

  if (doPU) {
    if (not m_truthPileUpEventName.key().empty()) {
      ATH_MSG_VERBOSE("Getting TruthEvents container.");
      SG::ReadHandle<xAOD::TruthPileupEventContainer> truthPileupEventContainer(m_truthPileUpEventName);
      if (truthPileupEventContainer.isValid()) {
        for (const auto evt : *truthPileupEventContainer) {
          truthVtx = evt->truthVertex(0);
          if (truthVtx) {
            truthPUVertices.push_back(truthVtx);
          }
        }
      }
      else {
        ATH_MSG_ERROR("No entries in TruthPileupEvents container!");
      }
    }
  }

  return std::make_pair<const std::vector<const xAOD::TruthVertex*>, const std::vector<const xAOD::TruthVertex*>>((const std::vector<const xAOD::TruthVertex*>)truthHSVertices, (const std::vector<const xAOD::TruthVertex*>)truthPUVertices);

}

void
InDetPhysValMonitoringTool::fillTrackCutFlow(const asg::AcceptData& accept) {
  fillCutFlow(accept, m_trackCutflowNames, m_trackCutflow);
  return;
}

void
InDetPhysValMonitoringTool::fillCutFlow(const asg::AcceptData& accept, std::vector<std::string>& names,
=======
void
InDetPhysValMonitoringTool::fillTrackCutFlow(Root::TAccept& accept) {
  fillCutFlow(accept, m_trackCutflowNames, m_trackCutflow);
  return;
}

void
InDetPhysValMonitoringTool::fillCutFlow(Root::TAccept& accept, std::vector<std::string>& names,
>>>>>>> release/21.0.127
                                        std::vector<int>& cutFlow) {
  // initialise cutflows
  if (cutFlow.empty()) {
    names.push_back("preCut");
    cutFlow.push_back(0);
    for (unsigned int i = 0; i != accept.getNCuts(); ++i) {
      cutFlow.push_back(0);
      names.push_back((std::string) accept.getCutName(i));
    }
  }
  // get cutflow
  cutFlow[0] += 1;
  bool cutPositive = true;
  for (unsigned int i = 0; i != (accept.getNCuts() + 1); ++i) {
    if (!cutPositive) {
      continue;
    }
    if (accept.getCutResult(i)) {
      cutFlow[i + 1] += 1;
    } else {
      cutPositive = false;
    }
  }
  return;
}

StatusCode
InDetPhysValMonitoringTool::doJetPlots(const xAOD::TrackParticleContainer* ptracks,
                                       IDPVM::CachedGetAssocTruth& getAsTruth,
                                       const xAOD::Vertex* primaryVtx) {
  const xAOD::JetContainer* jets = getContainer<xAOD::JetContainer>(m_jetContainerName);
  const xAOD::TruthParticleContainer* truthParticles = getContainer<xAOD::TruthParticleContainer>("TruthParticles");
  const float minProbEffHigh = 0.5;

  if (!jets || !truthParticles) {
    ATH_MSG_WARNING(
      "Cannot open " << m_jetContainerName <<
        " jet container or TruthParticles truth particle container. Skipping jet plots.");
  } else {
    for (const auto& thisJet: *jets) {         // The big jets loop
      if (not passJetCuts(*thisJet)) {
        continue;
      }
      for (auto thisTrack: *ptracks) {    // The beginning of the track loop
        if (m_useTrackSelection and not (m_trackSelectionTool->accept(*thisTrack, primaryVtx))) {
          continue;
        }
        if (m_onlyInsideOutTracks and !(isInsideOut(*thisTrack))) {
          continue; // not an inside out track
        }
        if (thisJet->p4().DeltaR(thisTrack->p4()) > m_maxTrkJetDR) {
          continue;
        }
        //
        const bool safelyFilled = m_monPlots->filltrkInJetPlot(*thisTrack, *thisJet);
        if (safelyFilled) {
          float trkInJet_w(0), trkInJet_BMR(1);
          float prob = getMatchingProbability(*thisTrack);
          if (prob > minProbEffHigh) {
            trkInJet_w = 1;
            trkInJet_BMR = 0;
          }
          m_monPlots->jet_fill(*thisTrack, *thisJet, trkInJet_w);                          // fill trkinjeteff plots
          m_monPlots->jetBMR(*thisTrack, *thisJet, trkInJet_BMR);                          // fin in track in jet bad
          m_monPlots->fillSimpleJetPlots(*thisTrack, prob);                                // Fill all the
                                                                                          
          const xAOD::TruthParticle* associatedTruth = getAsTruth.getTruth(thisTrack);     //
                                                                                         
          if (associatedTruth) {
            m_monPlots->fillJetResPlots(*thisTrack, *associatedTruth, *thisJet);          // Fill jet pull &
                                                                                          // resolution plots
            int barcode = associatedTruth->barcode();
            m_monPlots->fillJetHitsPlots(*thisTrack, prob, barcode);                      // Fill the two extra
                                                                                          // plots
            if (m_truthSelectionTool->accept(associatedTruth)) {                          // Fill the Jet plots with
                                                                                          // "Eff" in the name
              m_monPlots->fillJetEffPlots(*associatedTruth, *thisJet);
            }
          }
        }
      } // end of track loop
        // fill in things like sum jet pT in dR bins - need all tracks in the jet first
      m_monPlots->fillJetPlotCounter(*thisJet);
      for (const auto& thisTruth: *truthParticles) {
        // for primary tracks we want an efficiency as a function of track jet dR
        if ((m_truthSelectionTool->accept(thisTruth) and(thisJet->p4().DeltaR(thisTruth->p4()) < m_maxTrkJetDR))) {
          m_monPlots->fillJetTrkTruth(*thisTruth, *thisJet);
        }
      }
      m_monPlots->fillJetTrkTruthCounter(*thisJet);
    } // loop over jets
  }
  return StatusCode::SUCCESS;
}
