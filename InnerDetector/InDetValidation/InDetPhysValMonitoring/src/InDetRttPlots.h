/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#ifndef INDETPHYSVALMONITORING_INDETRTTPLOTS
#define INDETPHYSVALMONITORING_INDETRTTPLOTS
/**
 * @file InDetRttPlots.h
 * @author shaun roe
 * @date 29/3/2014
 **/


// std includes
#include <string>
<<<<<<< HEAD
#include <memory>

=======
>>>>>>> release/21.0.127
// local includes
#include "InDetPlotBase.h"
#include "InDetBasicPlot.h"

#include "TrkValHistUtils/RecoInfoPlots.h"
#include "TrkValHistUtils/TruthInfoPlots.h"
#include "InDetPerfPlot_nTracks.h"
<<<<<<< HEAD
#include "InDetPerfPlot_FakeRate.h"
#include "InDetPerfPlot_Efficiency.h"
#include "InDetPerfPlot_HitResidual.h"
#include "InDetPerfPlot_HitEfficiency.h"
#include "InDetPerfPlot_TrackParameters.h"
=======
#include "InDetPerfPlot_res.h"
#include "InDetPerfPlot_fakes.h"
#include "InDetPerfPlot_Eff.h"
#include "InDetPerfPlot_hitResidual.h"
#include "InDetPerfPlot_hitEff.h"
#include "InDetPerfPlot_spectrum.h"
#include "InDetPerfPlot_duplicate.h"

#include "InDetDummyPlots.h"
#include "InDet_BadMatchRate.h"

>>>>>>> release/21.0.127
#include "TrkValHistUtils/IDHitPlots.h"
#include "InDetPerfPlot_Hits.h"
#include "InDetPerfPlot_Vertex.h"
#include "InDetPerfPlot_VertexTruthMatching.h"
#include "InDetPerfPlot_VerticesVsMu.h"
#include "InDetPerfPlot_TrkInJet.h"
#include "InDetPerfPlot_TRTExtension.h"
#include "InDetPerfPlot_ANTracking.h"

#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODEventInfo/EventInfo.h"

#include "InDetPerfPlot_Resolution.h"
#include "InDetPerfNtuple_TruthToReco.h" 

#include "InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h"

///class holding all plots for Inner Detector RTT Validation and implementing fill methods
class InDetRttPlots: public InDetPlotBase {
public:
<<<<<<< HEAD
  InDetRttPlots(InDetPlotBase* pParent, const std::string& dirName, const int iDetailLevel = 10);

  void SetFillJetPlots(bool fillJets, bool fillBJets);

  ///fill for things needing truth and track only
  void fill(const xAOD::TrackParticle& particle, const xAOD::TruthParticle& truthParticle, bool truthIsFromB=false);

  ///fill for things needing track only
  void fill(const xAOD::TrackParticle& particle);
  void fill(const xAOD::TrackParticle& particle, const float mu, const unsigned int nVtx); //mu dependent plots
  void fill(const unsigned int nTrkANT, const unsigned int nTrkSTD, const unsigned int nTrkBAT, const float mu, const unsigned int nVtx);
  void fill(const unsigned int nTracks, const unsigned int mu, const unsigned nVtx);
  ///fill for things needing truth only
  void fill(const xAOD::TruthParticle& particle);
  ///Fill for efficiency plots
  void fillEfficiency(const xAOD::TruthParticle& truth, const xAOD::TrackParticle& track, const bool isGood, const float mu, const unsigned int nVtx);

  ///fill for things needing all truth - not just the ones from the reco tracks
  
  ///fill reco-vertex related plots
  void fill(const xAOD::VertexContainer& vertexContainer, const std::vector<const xAOD::TruthVertex*>& truthHSVertices, const std::vector<const xAOD::TruthVertex*>& truthPUVertices);
  ///fill reco-vertex related plots that need EventInfo
  void fill(const xAOD::VertexContainer& vertexContainer, const unsigned int nPU);

  void fill(const xAOD::TrackParticle& track, const xAOD::Jet& jet, bool isBjet=false, bool isFake=false, bool isUnlinked=false, bool truthIsFromB=false);
  void fillEfficiency(const xAOD::TruthParticle& truth, const xAOD::Jet& jet, const bool isGood, bool isBjet=false, bool truthIsFromB=false);
  void fillFakeRate(const xAOD::TrackParticle& track, const xAOD::Jet& jet, const bool isFake, bool isBjet=false, bool truthIsFromB=false);
  
=======
  InDetRttPlots(InDetPlotBase* pParent, const std::string& dirName);
  void
  SetFillExtraTIDEPlots(bool fillthem) {
    m_moreJetPlots = fillthem;
  }

  ///fill for things needing truth and track only
  void fill(const xAOD::TrackParticle& particle, const xAOD::TruthParticle& truthParticle);
  ///fill for things needing track only
  void fill(const xAOD::TrackParticle& particle);
  ///fill for things needing truth only
  void fill(const xAOD::TruthParticle& particle);
  ///Fill for efficiency plots
  void fillEfficiency(const xAOD::TruthParticle& truth, const bool isGood);

  void fillSpectrum(const xAOD::TrackParticle& trackParticle);
  void fillSpectrum(const xAOD::TruthParticle& particle);
  void fillSpectrum(const xAOD::TrackParticle& trkprt, const xAOD::TruthVertex& truthVrt);
  void fillSpectrum(const xAOD::TrackParticle& trkprt, const xAOD::Vertex& vertex);
  void fillSpectrum(const xAOD::TrackParticle& trkprt, const xAOD::Vertex& vertex, bool fill);
  void fillSpectrumLinked(const xAOD::TrackParticle& particle, const xAOD::TruthParticle& truthParticle, float weight);
  void fillLinkedandUnlinked(const xAOD::TrackParticle& particle, float Prim_w, float Sec_w, float Unlinked_w);
  void fillSpectrumUnlinked2(const xAOD::TrackParticle& particle);
  void fillSingleMatch(const xAOD::TrackParticle& trackParticle);
  void fillTwoMatchDuplicate(Float_t prob1, Float_t prob2, const xAOD::TrackParticle& trackParticle,
                             const xAOD::TrackParticle& particle, const xAOD::TruthParticle& tp);
  ///fill for things needing all truth - not just the ones from the reco tracks

  void lepton_fill(const xAOD::TruthParticle& truth, float weight);
  void prim_photon_fill(const xAOD::TruthParticle& truth);
  void brem_photon_fill(const xAOD::TruthParticle& truth);
  void track_vs_truth(const xAOD::TrackParticle& track, const xAOD::TruthParticle& truth, float tmp);
  void minDR(float min_dR, float prod_rad, float bestmatch, double BIDPt);
  void BT_fill(const xAOD::TruthParticle& truth, float weight);

  // fill the fake and bad match rate plots
  void fillBMR(const xAOD::TrackParticle& track, float weight);
  void fillRF(const xAOD::TrackParticle& track, float weight);

  ///fill reco-vertex related plots
  void fill(const xAOD::VertexContainer& vertexContainer);
  ///fill reco-vertex related plots that need EventInfo
  void fill(const xAOD::VertexContainer& vertexContainer, const xAOD::EventInfo& ei);

  // New set has replaced fillJetPlot
  bool filltrkInJetPlot(const xAOD::TrackParticle& particle, const xAOD::Jet& jet);
  void fillSimpleJetPlots(const xAOD::TrackParticle& particle, float prob);
  void fillJetHitsPlots(const xAOD::TrackParticle& particle, float prob, int barcode);
  void fillJetResPlots(const xAOD::TrackParticle& particle, const xAOD::TruthParticle& truth, const xAOD::Jet& jet);
  void fillJetEffPlots(const xAOD::TruthParticle& truth, const xAOD::Jet& jet);

  void jet_fill(const xAOD::TrackParticle& track, const xAOD::Jet& jet, float weight);
  void jetBMR(const xAOD::TrackParticle& track, const xAOD::Jet& jet, float weight);

  void fillJetPlotCounter(const xAOD::Jet& jet);
  void fillJetTrkTruth(const xAOD::TruthParticle& truth, const xAOD::Jet& jet);
  void fillJetTrkTruthCounter(const xAOD::Jet& jet);

>>>>>>> release/21.0.127
  virtual ~InDetRttPlots() {/**nop**/
  };
  ///fill for Counters
  void fillCounter(const unsigned int freq, const InDetPerfPlot_nTracks::CounterCategory counter);
  ///fill for fakes
<<<<<<< HEAD
  void fillFakeRate(const xAOD::TrackParticle& particle, const bool isFake, const bool isAssociatedTruth, const float mu, const unsigned int nVtx);

  // fill IDPVM Ntuple
  void fillNtuple(const xAOD::TrackParticle& track);
  void fillNtuple(const xAOD::TruthParticle& truth);
  void fillNtuple(const xAOD::TrackParticle& track, const xAOD::TruthParticle& truth, const int truthMatchRanking); 
private:
  InDetPerfPlot_TrackParameters m_trackParameters;
  InDetPerfPlot_nTracks m_nTracks;
  InDetPerfPlot_HitResidual m_hitResidualPlot;
  InDetPerfPlot_HitEfficiency m_hitEffPlot;
  InDetPerfPlot_FakeRate m_fakePlots;
  InDetPerfPlot_FakeRate m_missingTruthFakePlots;
  InDetPerfPlot_Resolution m_resolutionPlotPrim;
  InDetPerfPlot_Resolution m_resolutionPlotPrim_truthFromB;
  InDetPerfPlot_Hits m_hitsRecoTracksPlots;
  InDetPerfPlot_Efficiency m_effPlots;
  InDetPerfPlot_VerticesVsMu m_verticesVsMuPlots;
  InDetPerfPlot_Vertex m_vertexPlots;
  InDetPerfPlot_Vertex m_hardScatterVertexPlots;
  InDetPerfPlot_VertexTruthMatching m_hardScatterVertexTruthMatchingPlots;
  InDetPerfPlot_TRTExtension m_trtExtensionPlots;
  InDetPerfPlot_ANTracking m_anTrackingPlots;
  InDetPerfNtuple_TruthToReco m_ntupleTruthToReco;
  std::unique_ptr<InDetPerfPlot_Resolution> m_resolutionPlotSecd;
  std::unique_ptr<InDetPerfPlot_Hits> m_hitsMatchedTracksPlots;
  std::unique_ptr<InDetPerfPlot_Hits> m_hitsFakeTracksPlots{nullptr};
  std::unique_ptr<InDetPerfPlot_Hits> m_hitsUnlinkedTracksPlots{nullptr};
  std::unique_ptr<InDetPerfPlot_VertexTruthMatching> m_vertexTruthMatchingPlots;
  bool m_doTrackInJetPlots;
  bool m_doTrackInBJetPlots;
  std::unique_ptr<InDetPerfPlot_TrkInJet> m_trkInJetPlots;
  std::unique_ptr<InDetPerfPlot_TrkInJet> m_trkInJetPlots_bjets;
  std::unique_ptr<InDetPerfPlot_TrkInJet> m_trkInJetPlots_matched;
  std::unique_ptr<InDetPerfPlot_TrkInJet> m_trkInJetPlots_matched_bjets;
  std::unique_ptr<InDetPerfPlot_TrkInJet> m_trkInJetPlots_fake;
  std::unique_ptr<InDetPerfPlot_TrkInJet> m_trkInJetPlots_fake_bjets;
  std::unique_ptr<InDetPerfPlot_TrkInJet> m_trkInJetPlots_unlinked;
  std::unique_ptr<InDetPerfPlot_TrkInJet> m_trkInJetPlots_unlinked_bjets;
  
  // by track origin
  bool m_doTruthOriginPlots;
  std::unique_ptr<InDetPerfPlot_TrkInJet> m_trkInJetPlots_truthFromB;

  //By track authors
  std::unique_ptr<InDetPerfPlot_Efficiency> m_effSiSPSeededFinderPlots;
  std::unique_ptr<InDetPerfPlot_Efficiency> m_effInDetExtensionProcessorPlots;
  std::unique_ptr<InDetPerfPlot_Efficiency> m_effTRTSeededTrackFinderPlots;
  std::unique_ptr<InDetPerfPlot_Efficiency> m_effTRTStandalonePlots;
  std::unique_ptr<InDetPerfPlot_Efficiency> m_effSiSpacePointsSeedMaker_LargeD0Plots;

  std::unique_ptr<InDetPerfPlot_FakeRate> m_fakeSiSPSeededFinderPlots;
  std::unique_ptr<InDetPerfPlot_FakeRate> m_fakeInDetExtensionProcessorPlots;
  std::unique_ptr<InDetPerfPlot_FakeRate> m_fakeTRTSeededTrackFinderPlots;
  std::unique_ptr<InDetPerfPlot_FakeRate> m_fakeTRTStandalonePlots;
  std::unique_ptr<InDetPerfPlot_FakeRate> m_fakeSiSpacePointsSeedMaker_LargeD0Plots;

  std::unique_ptr<InDetPerfPlot_TrackParameters> m_trkParaSiSPSeededFinderPlots;
  std::unique_ptr<InDetPerfPlot_TrackParameters> m_trkParaInDetExtensionProcessorPlots;
  std::unique_ptr<InDetPerfPlot_TrackParameters> m_trkParaTRTSeededTrackFinderPlots;
  std::unique_ptr<InDetPerfPlot_TrackParameters> m_trkParaTRTStandalonePlots;
  std::unique_ptr<InDetPerfPlot_TrackParameters> m_trkParaSiSpacePointsSeedMaker_LargeD0Plots;

  std::unique_ptr<InDetPerfPlot_Resolution> m_resSiSPSeededFinderPlots;
  std::unique_ptr<InDetPerfPlot_Resolution> m_resInDetExtensionProcessorPlots;
  std::unique_ptr<InDetPerfPlot_Resolution> m_resTRTSeededTrackFinderPlots;
  std::unique_ptr<InDetPerfPlot_Resolution> m_resTRTStandalonePlots;
  std::unique_ptr<InDetPerfPlot_Resolution> m_resSiSpacePointsSeedMaker_LargeD0Plots;



=======
  void fillFakeRate(const xAOD::TrackParticle& particle, const bool match,
                    const InDetPerfPlot_fakes::Category c = InDetPerfPlot_fakes::ALL);
  void fillIncTrkRate(const unsigned int nMuEvents, std::vector<int> incTrkNum, std::vector<int> incTrkDenom);
  void fillFakeRateLinked(const xAOD::TrackParticle& particle, const xAOD::TruthParticle& truthParticle);
  void fillFakeRateUnlinked(const xAOD::TrackParticle& particle);
private:
  InDetPerfPlot_Pt m_ptPlot;
  InDetBasicPlot m_basicPlot;
  Trk::RecoInfoPlots m_TrackRecoInfoPlots;
  Trk::TruthInfoPlots m_TrackTruthInfoPlots;
  InDetPerfPlot_nTracks m_nTracks;
  InDetPerfPlot_res m_resPlots;
  InDetPerfPlot_hitResidual m_hitResidualPlot;
  InDetPerfPlot_hitEff m_hitEffPlot;
  InDetPerfPlot_fakes m_fakePlots; // fakes vs eta etc, as per original RTT code

  // ITk resolutions
  InDetPerfPlot_resITk* m_ITkResolutionPlotPrim;
  InDetPerfPlot_resITk* m_ITkResolutionPlotSecd;

  InDetPerfPlot_HitDetailed m_hitsMatchedTracksPlots;
  InDetPerfPlot_HitDetailed m_hitsDetailedPlots;
  InDetPerfPlot_Eff m_effPlots;
  InDetDummyPlots m_dumPlots;
  InDet_BadMatchRate m_BadMatchRate;

  InDetPerfPlot_VertexContainer m_verticesPlots;
  InDetPerfPlot_Vertex m_vertexPlots;
  InDetPerfPlot_Vertex m_hardScatterVertexPlots;

  InDetPerfPlot_duplicate m_duplicatePlots;

  bool m_moreJetPlots;
  bool m_ITkResPlots;
  InDetPerfPlot_TrkInJet m_trkInJetPlot;
  InDetPerfPlot_TrkInJet m_trkInJetPlot_highPt;
  InDetPerfPlot_Pt m_trkInJetPtPlot;
  InDetBasicPlot m_trkInJetBasicPlot;
  // Trk::ParamPlots m_trkInJetPtEtaPlots;
  // Trk::ImpactPlots m_trkInJetIPPlots;
  Trk::RecoInfoPlots m_trkInJetTrackRecoInfoPlots;
  InDetPerfPlot_HitDetailed m_trkInJetHitsDetailedPlots;
  InDetPerfPlot_fakes m_trkInJetFakePlots; // fakes vs eta etc, as per original RTT code
  InDetPerfPlot_res m_trkInJetResPlots;
  InDetPerfPlot_res* m_trkInJetResPlotsDr0010;
  InDetPerfPlot_res* m_trkInJetResPlotsDr1020;
  InDetPerfPlot_res* m_trkInJetResPlotsDr2030;
  InDetPerfPlot_Eff m_trkInJetEffPlots;
  InDetPerfPlot_res m_trkInJetHighPtResPlots;
  InDetPerfPlot_HitDetailed m_trkInJetHitsFakeTracksPlots;
  InDetPerfPlot_HitDetailed m_trkInJetHitsMatchedTracksPlots;
  Trk::TruthInfoPlots m_trkInJetTrackTruthInfoPlots;
  InDetPerfPlot_spectrum m_specPlots;
>>>>>>> release/21.0.127

  std::string m_trackParticleTruthProbKey;
  float m_truthProbLowThreshold;
};




#endif
