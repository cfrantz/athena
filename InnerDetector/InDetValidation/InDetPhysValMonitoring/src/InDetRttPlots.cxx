/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

/**
 * @file InDetRttPlots.cxx
 * @author shaun roe
 **/

#include "InDetRttPlots.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"
#include <cmath> // std::isnan()
#include <limits>

<<<<<<< HEAD
InDetRttPlots::InDetRttPlots(InDetPlotBase* pParent, const std::string& sDir, const int iDetailLevel) : InDetPlotBase(pParent, sDir),
  m_trackParameters(this, "Tracks/Selected/Parameters"),
  m_nTracks(this, "Tracks/Tracks"),
  m_hitResidualPlot(this, "Tracks/Hits/Residuals"),
  m_hitEffPlot(this, "Tracks/Hits/Efficiency"),
  m_fakePlots(this, "Tracks/FakeRate"),
  m_missingTruthFakePlots(this, "Tracks/Unlinked/FakeRate"),
  m_resolutionPlotPrim(this, "Tracks/Matched/Resolutions/Primary"),
  m_resolutionPlotPrim_truthFromB(this, "Tracks/Matched/Resolutions/TruthFromB"),
  m_hitsRecoTracksPlots(this, "Tracks/Selected/HitsOnTracks"),
  m_effPlots(this, "Tracks/Efficiency"),
  m_verticesVsMuPlots(this, "Vertices/AllPrimaryVertices"),
  m_vertexPlots(this, "Vertices/AllPrimaryVertices"),
  m_hardScatterVertexPlots(this, "Vertices/HardScatteringVertex"),
  m_hardScatterVertexTruthMatchingPlots(this, "Vertices/HardScatteringVertex"),
  m_trtExtensionPlots(this, "Tracks/TRTExtension"),
  m_anTrackingPlots(this, "Tracks/ANT"),
  m_ntupleTruthToReco(this, "Ntuples", "TruthToReco"),
  m_resolutionPlotSecd(nullptr),
  m_doTrackInJetPlots(true),
  m_doTrackInBJetPlots(true),
  m_doTruthOriginPlots(true) // plots are created, but not filled without --doTruthOrigin flag
{
  this->m_iDetailLevel = iDetailLevel;
  m_trackParticleTruthProbKey = "truthMatchProbability";
  m_truthProbLowThreshold = 0.5;
  
  if(m_iDetailLevel >= 200){
    m_resolutionPlotSecd = std::make_unique<InDetPerfPlot_Resolution>(this, "Tracks/Matched/Resolutions/Secondary");
    m_hitsMatchedTracksPlots = std::make_unique<InDetPerfPlot_Hits>(this, "Tracks/Matched/HitsOnTracks");
    m_hitsFakeTracksPlots = std::make_unique<InDetPerfPlot_Hits>(this, "Tracks/Fakes/HitsOnTracks");
    m_hitsUnlinkedTracksPlots = std::make_unique<InDetPerfPlot_Hits>(this, "Tracks/Unlinked/HitsOnTracks");
    m_vertexTruthMatchingPlots = std::make_unique<InDetPerfPlot_VertexTruthMatching>(this, "Vertices/AllPrimaryVertices", m_iDetailLevel);

    //Split by track author
    m_effSiSPSeededFinderPlots = std::make_unique<InDetPerfPlot_Efficiency>(this, "TracksByAuthor/SiSPSeededFinder/Tracks/Efficiency");
    m_effInDetExtensionProcessorPlots = std::make_unique<InDetPerfPlot_Efficiency>(this, "TracksByAuthor/InDetExtensionProcessor/Tracks/Efficiency");
    m_effTRTSeededTrackFinderPlots = std::make_unique<InDetPerfPlot_Efficiency>(this, "TracksByAuthor/TRTSeededTrackFinder/Tracks/Efficiency");
    m_effTRTStandalonePlots = std::make_unique<InDetPerfPlot_Efficiency>(this, "TracksByAuthor/TRTStandalone/Tracks/Efficiency");
    m_effSiSpacePointsSeedMaker_LargeD0Plots = std::make_unique<InDetPerfPlot_Efficiency>(this, "TracksByAuthor/SiSpacePointsSeedMaker_LargeD0/Tracks/Efficiency");

    m_fakeSiSPSeededFinderPlots = std::make_unique<InDetPerfPlot_FakeRate>(this, "TracksByAuthor/SiSPSeededFinder/Tracks/FakeRate");
    m_fakeInDetExtensionProcessorPlots = std::make_unique<InDetPerfPlot_FakeRate>(this, "TracksByAuthor/InDetExtensionProcessor/Tracks/FakeRate");
    m_fakeTRTSeededTrackFinderPlots = std::make_unique<InDetPerfPlot_FakeRate>(this, "TracksByAuthor/TRTSeededTrackFinder/Tracks/FakeRate");
    m_fakeTRTStandalonePlots = std::make_unique<InDetPerfPlot_FakeRate>(this, "TracksByAuthor/TRTStandalone/Tracks/FakeRate");
    m_fakeSiSpacePointsSeedMaker_LargeD0Plots = std::make_unique<InDetPerfPlot_FakeRate>(this, "TracksByAuthor/SiSpacePointsSeedMaker_LargeD0/Tracks/FakeRate");

    m_trkParaSiSPSeededFinderPlots = std::make_unique<InDetPerfPlot_TrackParameters>(this, "TracksByAuthor/SiSPSeededFinder/Tracks/Parameters");
    m_trkParaInDetExtensionProcessorPlots = std::make_unique<InDetPerfPlot_TrackParameters>(this, "TracksByAuthor/InDetExtensionProcessor/Tracks/Parameters");
    m_trkParaTRTSeededTrackFinderPlots = std::make_unique<InDetPerfPlot_TrackParameters>(this, "TracksByAuthor/TRTSeededTrackFinder/Tracks/Parameters");
    m_trkParaTRTStandalonePlots = std::make_unique<InDetPerfPlot_TrackParameters>(this, "TracksByAuthor/TRTStandalone/Tracks/Parameters");
    m_trkParaSiSpacePointsSeedMaker_LargeD0Plots = std::make_unique<InDetPerfPlot_TrackParameters>(this, "TracksByAuthor/SiSpacePointsSeedMaker_LargeD0/Tracks/Parameters");

    m_resSiSPSeededFinderPlots = std::make_unique<InDetPerfPlot_Resolution>(this, "TracksByAuthor/SiSPSeededFinder/Tracks/Resolution");
    m_resInDetExtensionProcessorPlots = std::make_unique<InDetPerfPlot_Resolution>(this, "TracksByAuthor/InDetExtensionProcessor/Tracks/Resolution");
    m_resTRTSeededTrackFinderPlots = std::make_unique<InDetPerfPlot_Resolution>(this, "TracksByAuthor/TRTSeededTrackFinder/Tracks/Resolution");
    m_resTRTStandalonePlots = std::make_unique<InDetPerfPlot_Resolution>(this, "TracksByAuthor/TRTStandalone/Tracks/Resolution");
    m_resSiSpacePointsSeedMaker_LargeD0Plots = std::make_unique<InDetPerfPlot_Resolution>(this, "TracksByAuthor/SiSpacePointsSeedMaker_LargeD0/Tracks/Resolution");

  }

  /// update detail level of all the child tools
  setDetailLevel(m_iDetailLevel);
}


void InDetRttPlots::SetFillJetPlots(bool fillJets, bool fillBJets){

  m_doTrackInJetPlots = fillJets;
  m_doTrackInBJetPlots = fillBJets;

  if(m_doTrackInJetPlots){
    m_trkInJetPlots = std::make_unique<InDetPerfPlot_TrkInJet>(this, "TracksInJets/Tracks");
    if (m_iDetailLevel >= 200){
      m_trkInJetPlots_matched = std::make_unique<InDetPerfPlot_TrkInJet>(this, "TracksInJets/Matched",false);
      m_trkInJetPlots_fake = std::make_unique<InDetPerfPlot_TrkInJet>(this, "TracksInJets/Fakes",false);
      m_trkInJetPlots_unlinked = std::make_unique<InDetPerfPlot_TrkInJet>(this, "TracksInJets/Unlinked",false);
    }
    if(m_doTrackInBJetPlots){
      m_trkInJetPlots_bjets = std::make_unique<InDetPerfPlot_TrkInJet>(this, "TracksInBJets/Tracks");
      if (m_iDetailLevel >= 200){
        m_trkInJetPlots_matched_bjets = std::make_unique<InDetPerfPlot_TrkInJet>(this, "TracksInBJets/Matched",false);
        m_trkInJetPlots_fake_bjets = std::make_unique<InDetPerfPlot_TrkInJet>(this, "TracksInBJets/Fakes",false);
        m_trkInJetPlots_unlinked_bjets = std::make_unique<InDetPerfPlot_TrkInJet>(this, "TracksInBJets/Unlinked",false);
      }
    }
    if(m_doTruthOriginPlots){
      m_trkInJetPlots_truthFromB = std::make_unique<InDetPerfPlot_TrkInJet>(this, "TracksInBJets/TruthFromB");
    }
=======

namespace { // utility functions used in this area - stolen from InDetPhysValMonitoringTool and other places - not so
            // nice as should be in a common header somewhere
  // get truth/track matching probability
  float
  getMatchingProbability(const xAOD::TrackParticle& trackParticle) {
    float result(std::numeric_limits<float>::quiet_NaN());

    if (trackParticle.isAvailable<float>("truthMatchProbability")) {
      result = trackParticle.auxdata<float>("truthMatchProbability");
    }
    return result;
  }
}// namespace


InDetRttPlots::InDetRttPlots(InDetPlotBase* pParent, const std::string& sDir) : InDetPlotBase(pParent, sDir),

  m_ptPlot(this, "Tracks/SelectedGoodTracks"),
  m_basicPlot(this, "Tracks/SelectedGoodTracks"),
  m_TrackRecoInfoPlots(this, "Tracks/SelectedGoodTracks"),
  m_TrackTruthInfoPlots(this, "Truth"),
  m_nTracks(this, "Tracks/SelectedGoodTracks"),
  m_resPlots(this, "Tracks/SelectedGoodTracks"),
  m_hitResidualPlot(this, "Tracks/SelectedGoodTracks"),
  m_hitEffPlot(this, "Tracks/SelectedGoodTracks"),
  m_fakePlots(this, "Tracks/SelectedFakeTracks"),
  m_ITkResolutionPlotPrim(nullptr),
  m_ITkResolutionPlotSecd(nullptr),
  m_hitsMatchedTracksPlots(this, "Tracks/SelectedMatchedTracks"),
  m_hitsDetailedPlots(this, "Tracks/SelectedGoodTracks"),
  m_effPlots(this, "Tracks/SelectedGoodTracks"),
  m_dumPlots(this, "Tracks/SelectedGoodTracks"),
  m_BadMatchRate(this, "Tracks/SelectedBadMatchTracks"),
  m_verticesPlots(this, "Vertices/AllPrimaryVertices"),
  m_vertexPlots(this, "Vertices/AllPrimaryVertices"),
  m_hardScatterVertexPlots(this, "Vertices/HardScatteringVertex"),
  m_duplicatePlots(this, "Tracks/SelectedGoodTracks"),
  m_trkInJetPlot(this, "Tracks/SelectedGoodJetTracks"),
  m_trkInJetPlot_highPt(this, "Tracks/SelectedGoodHighPtJetTracks"),
  m_trkInJetPtPlot(this, "Tracks/SelectedGoodJetTracks"),
  m_trkInJetBasicPlot(this, "Tracks/SelectedGoodJetTracks"),
  m_trkInJetTrackRecoInfoPlots(this, "Tracks/SelectedGoodJetTracks"),
  m_trkInJetHitsDetailedPlots(this, "Tracks/SelectedGoodJetTracks"),
  m_trkInJetFakePlots(this, "Tracks/SelectedFakeJetTracks"),
  m_trkInJetResPlots(this, "Tracks/SelectedGoodJetTracks"),
  m_trkInJetResPlotsDr0010(nullptr),
  m_trkInJetResPlotsDr1020(nullptr),
  m_trkInJetResPlotsDr2030(nullptr),

  m_trkInJetEffPlots(this, "Tracks/SelectedGoodJetTracks"),
  m_trkInJetHighPtResPlots(this, "Tracks/SelectedGoodJetHighPtTracks"),
  m_trkInJetHitsFakeTracksPlots(this, "Tracks/SelectedFakeJetTracks"),
  m_trkInJetHitsMatchedTracksPlots(this, "Tracks/SelectedMatchedJetTracks"),
  m_trkInJetTrackTruthInfoPlots(this, "TruthInJet"),
  m_specPlots(this, "Tracks/PreSelectionSpectrumPlots") {
  m_moreJetPlots = false; // changed with setter function
  m_ITkResPlots = false;
  // These settings are probably all redundant & can be removed from this script
  m_trackParticleTruthProbKey = "truthMatchProbability";
  m_truthProbLowThreshold = 0.5;

  if (m_moreJetPlots) {
    m_trkInJetResPlotsDr0010 = new InDetPerfPlot_res(this, "Tracks/SelectedGoodJetDr0010Tracks");
    m_trkInJetResPlotsDr1020 = new InDetPerfPlot_res(this, "Tracks/SelectedGoodJetDr1020Tracks");
    m_trkInJetResPlotsDr2030 = new InDetPerfPlot_res(this, "Tracks/SelectedGoodJetDr2030Tracks");
  }
  if (m_ITkResPlots) {
    // Resolutions for ITk
    m_ITkResolutionPlotPrim = new InDetPerfPlot_resITk(this, "Tracks/SelectedMatchedTracks/Primary");
    m_ITkResolutionPlotSecd = new InDetPerfPlot_resITk(this, "Tracks/SelectedMatchedTracks/Secondary");
  }
}

void
InDetRttPlots::fill(const xAOD::TrackParticle& particle, const xAOD::TruthParticle& truthParticle) {
  // fill measurement bias, resolution, and pull plots
  m_resPlots.fill(particle, truthParticle);
  m_basicPlot.fill(truthParticle);

  // fill ITK resolutions (bias / resolutions)
  if (m_ITkResPlots) {
    if (particle.isAvailable<float>(m_trackParticleTruthProbKey)) {
      const float prob = particle.auxdata<float>(m_trackParticleTruthProbKey);
      float barcode = truthParticle.barcode();
      if (barcode < 200000 && barcode != 0 && prob > 0.5) {
        m_ITkResolutionPlotPrim->fill(particle, truthParticle);
      } else if (barcode >= 200000 && prob > 0.7) {
        m_ITkResolutionPlotSecd->fill(particle, truthParticle);
      }
    }
  }
  // Not sure that the following hitsMatchedTracksPlots does anything...
  float barcode = truthParticle.barcode();
  if (barcode < 100000 && barcode != 0) { // Not sure why the barcode limit is 100k instead of 200k...
    m_hitsMatchedTracksPlots.fill(particle);
>>>>>>> release/21.0.127
  }

}

<<<<<<< HEAD

//
//Fill plots for matched particles
//

void
InDetRttPlots::fill(const xAOD::TrackParticle& particle, const xAOD::TruthParticle& truthParticle, bool isFromB) {
  // fill measurement bias, resolution, and pull plots

  // fill ITK resolutions (bias / resolutions)
  if (particle.isAvailable<float>(m_trackParticleTruthProbKey)) {
    const float prob = particle.auxdata<float>(m_trackParticleTruthProbKey);
    float barcode = truthParticle.barcode();
    if (barcode < 200000 && barcode != 0 && prob > 0.5) {
        m_resolutionPlotPrim.fill(particle, truthParticle);
    } else if (barcode >= 200000 && prob > 0.7 && m_iDetailLevel >= 200) {
        m_resolutionPlotSecd->fill(particle, truthParticle);
    }
    if ( m_doTruthOriginPlots and isFromB ) {
      m_resolutionPlotPrim_truthFromB.fill(particle, truthParticle);
    }

    if(m_iDetailLevel >= 200 and (barcode < 200000 and barcode != 0 and prob > 0.5)){
      std::bitset<xAOD::TrackPatternRecoInfo::NumberOfTrackRecoInfo>  patternInfo = particle.patternRecoInfo();
    
      bool isSiSpSeededFinder = patternInfo.test(0);
      bool isInDetExtensionProcessor = patternInfo.test(3);
      bool isTRTSeededTrackFinder = patternInfo.test(4);
      bool isTRTStandalone = patternInfo.test(20);
      bool isSiSpacePointsSeedMaker_LargeD0 = patternInfo.test(49);

      if(isSiSpSeededFinder and not isInDetExtensionProcessor) m_resSiSPSeededFinderPlots->fill(particle, truthParticle);
      if(isInDetExtensionProcessor and not (isTRTSeededTrackFinder or isSiSpacePointsSeedMaker_LargeD0)) m_resInDetExtensionProcessorPlots->fill(particle, truthParticle);
      if(isTRTSeededTrackFinder and not isTRTStandalone) m_resTRTSeededTrackFinderPlots->fill(particle, truthParticle);
      if(isTRTStandalone) m_resTRTStandalonePlots->fill(particle, truthParticle);
      if(isSiSpacePointsSeedMaker_LargeD0) m_resSiSpacePointsSeedMaker_LargeD0Plots->fill(particle, truthParticle);

    }

    if (barcode < 200000 && barcode != 0 && prob > 0.5) m_trtExtensionPlots.fill(particle, truthParticle);


  }
 
  if(m_iDetailLevel >= 200){
    float barcode = truthParticle.barcode();
    if (barcode < 200000 && barcode != 0) { 
      m_hitsMatchedTracksPlots->fill(particle);
    }
  }
}

//
//Fill basic track properties for reconstructed tracks 
//

void
InDetRttPlots::fill(const xAOD::TrackParticle& particle) {
  m_hitResidualPlot.fill(particle);
  m_hitEffPlot.fill(particle);
  // fill pt plots
  m_trackParameters.fill(particle);
  m_anTrackingPlots.fill(particle);

  if(m_iDetailLevel >= 200){
    std::bitset<xAOD::TrackPatternRecoInfo::NumberOfTrackRecoInfo>  patternInfo = particle.patternRecoInfo();
    
    bool isSiSpSeededFinder = patternInfo.test(0);
    bool isInDetExtensionProcessor = patternInfo.test(3);
    bool isTRTSeededTrackFinder = patternInfo.test(4);
    bool isTRTStandalone = patternInfo.test(20);
    bool isSiSpacePointsSeedMaker_LargeD0 = patternInfo.test(49);

    if(isSiSpSeededFinder and not isInDetExtensionProcessor) m_trkParaSiSPSeededFinderPlots->fill(particle);
    else if(isInDetExtensionProcessor and not (isTRTSeededTrackFinder or isSiSpacePointsSeedMaker_LargeD0)) m_trkParaInDetExtensionProcessorPlots->fill(particle);
    else if(isTRTSeededTrackFinder and not isTRTStandalone) m_trkParaTRTSeededTrackFinderPlots->fill(particle);
    else if(isTRTStandalone) m_trkParaTRTStandalonePlots->fill(particle);
    else if(isSiSpacePointsSeedMaker_LargeD0) m_trkParaSiSpacePointsSeedMaker_LargeD0Plots->fill(particle);

  }

  m_hitsRecoTracksPlots.fill(particle);
  m_trtExtensionPlots.fill(particle);
}

void
InDetRttPlots::fill(const xAOD::TrackParticle& particle, const float mu, const unsigned int nVtx) {

  m_trtExtensionPlots.fill(particle, mu, nVtx);

}

void
InDetRttPlots::fill(const unsigned int nTrkANT, const unsigned int nTrkSTD, const unsigned int nTrkBAT, const float mu, const unsigned int nVtx) { 

  m_anTrackingPlots.fill(nTrkANT, nTrkSTD, nTrkBAT, mu, nVtx);

}

void
InDetRttPlots::fill(const unsigned int ntracks, const unsigned int mu, const unsigned int nvertices) {

 m_nTracks.fill(ntracks, mu, nvertices);

  
=======
void
InDetRttPlots::fillSpectrum(const xAOD::TrackParticle& trackParticle) {
  float prob = getMatchingProbability(trackParticle);
  m_specPlots.fillSpectrum(trackParticle, prob);
}

void
InDetRttPlots::fillSpectrum(const xAOD::TruthParticle& particle) {
  m_specPlots.fillSpectrum(particle);
}

void
InDetRttPlots::fillSpectrum(const xAOD::TrackParticle& trkprt, const xAOD::TruthVertex& truthVrt) {
  m_specPlots.fillSpectrum(trkprt, truthVrt);
}

void
InDetRttPlots::fillSpectrum(const xAOD::TrackParticle& trkprt, const xAOD::Vertex& vertex, bool fill) {
  m_specPlots.fillSpectrum(trkprt, vertex, fill);
}

void
InDetRttPlots::fillSpectrumLinked(const xAOD::TrackParticle& particle, const xAOD::TruthParticle& truthParticle,
                                  float /*weight*/) {
  double prob = getMatchingProbability(particle);
  m_specPlots.fillSpectrumLinked(particle, truthParticle, prob);
}

void
InDetRttPlots::fillLinkedandUnlinked(const xAOD::TrackParticle& particle, float Prim_w, float Sec_w, float Unlinked_w) {
  m_fakePlots.fillLinkedandUnlinked(particle, Prim_w, Sec_w, Unlinked_w);
}

void
InDetRttPlots::fillSpectrumUnlinked2(const xAOD::TrackParticle& particle) {
  double prob = getMatchingProbability(particle);
  m_specPlots.fillSpectrumUnlinked2(particle, prob);
}

void
InDetRttPlots::fillSingleMatch(const xAOD::TrackParticle& trackParticle) {
  m_duplicatePlots.fillSingleMatch(trackParticle);
}

void
InDetRttPlots::fillTwoMatchDuplicate(Float_t prob1, Float_t prob2, const xAOD::TrackParticle& trackParticle,
                                     const xAOD::TrackParticle& particle, const xAOD::TruthParticle& tp) {
  m_duplicatePlots.fillTwoMatchDuplicate(prob1, prob2, trackParticle, particle, tp);
}

void
InDetRttPlots::fill(const xAOD::TrackParticle& particle) {
  m_hitResidualPlot.fill(particle);
  m_hitEffPlot.fill(particle);
  // fill pt plots
  m_ptPlot.fill(particle); // to maintain correspondence with old InDetPerformanceRTT
  m_basicPlot.fill(particle);
  m_TrackRecoInfoPlots.fill(particle);
  m_hitsDetailedPlots.fill(particle);
}

void
InDetRttPlots::fillEfficiency(const xAOD::TruthParticle& truth, const bool isGood) {
  m_effPlots.fill(truth, isGood);
  m_fakePlots.fillIncFakeDenom(truth);
}

void
InDetRttPlots::lepton_fill(const xAOD::TruthParticle& truth, float weight) {
  m_dumPlots.lepton_fill(truth, weight);
}

void
InDetRttPlots::prim_photon_fill(const xAOD::TruthParticle& truth) {
  m_dumPlots.prim_photon_fill(truth);
}

void
InDetRttPlots::brem_photon_fill(const xAOD::TruthParticle& truth) {
  m_dumPlots.brem_photon_fill(truth);
}

void
InDetRttPlots::track_vs_truth(const xAOD::TrackParticle& track,const xAOD::TruthParticle& truth, float tmp){
  m_dumPlots.track_vs_truth(track, truth, tmp);
}

void
InDetRttPlots::minDR(float min_dR, float prod_rad, float bestmatch, double BIDPt){
  m_dumPlots.minDR(min_dR, prod_rad, bestmatch, BIDPt);
}

void
InDetRttPlots::BT_fill(const xAOD::TruthParticle& truth, float weight) {
  m_effPlots.BT_fill(truth, weight);
}

void
InDetRttPlots::fill(const xAOD::TruthParticle& truthParticle) {
  // fill truth plots
  m_TrackTruthInfoPlots.fill(truthParticle);
}

void
InDetRttPlots::fillBMR(const xAOD::TrackParticle& track, float weight) {
  // fill the plot requiring truth matching probability less than the upper limit (50.1% right now)
  m_BadMatchRate.fillBMR(track, weight);
>>>>>>> release/21.0.127
}
//
//Fill plots for selected truth particle
//

void
<<<<<<< HEAD
InDetRttPlots::fill(const xAOD::TruthParticle& truthParticle) {
  // fill truth plots 
  m_trackParameters.fill(truthParticle);
}

//
//Fill Efficiencies
//

void
InDetRttPlots::fillEfficiency(const xAOD::TruthParticle& truth, const xAOD::TrackParticle& track, const bool isGood, const float mu, const unsigned int nVtx) {
  m_effPlots.fill(truth, isGood);

  m_anTrackingPlots.fillEfficiency(truth, track, isGood, mu, nVtx);
  if(m_iDetailLevel >= 200){
    if(isGood){
      std::bitset<xAOD::TrackPatternRecoInfo::NumberOfTrackRecoInfo>  patternInfo = track.patternRecoInfo();
    
      bool isSiSpSeededFinder = patternInfo.test(0);
      bool isInDetExtensionProcessor = patternInfo.test(3);
      bool isTRTSeededTrackFinder = patternInfo.test(4);
      bool isTRTStandalone = patternInfo.test(20);
      bool isSiSpacePointsSeedMaker_LargeD0 = patternInfo.test(49);

      if(isSiSpSeededFinder and not isInDetExtensionProcessor) m_effSiSPSeededFinderPlots->fill(truth, isGood);
      if(isInDetExtensionProcessor and not (isTRTSeededTrackFinder or isSiSpacePointsSeedMaker_LargeD0)) m_effInDetExtensionProcessorPlots->fill(truth, isGood);
      if(isTRTSeededTrackFinder and not isTRTStandalone) m_effTRTSeededTrackFinderPlots->fill(truth, isGood);
      if(isTRTStandalone) m_effTRTStandalonePlots->fill(truth, isGood);
      if(isSiSpacePointsSeedMaker_LargeD0) m_effSiSpacePointsSeedMaker_LargeD0Plots->fill(truth, isGood);
    } else {
      m_effSiSPSeededFinderPlots->fill(truth, isGood);
      m_effInDetExtensionProcessorPlots->fill(truth, isGood);
      m_effTRTSeededTrackFinderPlots->fill(truth, isGood);
      m_effTRTStandalonePlots->fill(truth, isGood);
      m_effSiSpacePointsSeedMaker_LargeD0Plots->fill(truth, isGood);

    }
    
=======
InDetRttPlots::fillRF(const xAOD::TrackParticle& track, float weight) {
  // fill the plot requiring truth matching probability less than the lower limit (50.0% right now)
  m_BadMatchRate.fillRF(track, weight);
}

void
InDetRttPlots::fill(const xAOD::VertexContainer& vertexContainer) {
  // fill vertex container general properties
  // m_verticesPlots.fill(vertexContainer); //if ever needed
  // fill vertex-specific properties, for all vertices and for hard-scattering vertex
  for (const auto& vtx : vertexContainer.stdcont()) {
    if (vtx->vertexType() == xAOD::VxType::NoVtx) {
      continue; // skip dummy vertex
    }
    m_vertexPlots.fill(*vtx);
    if (vtx->vertexType() == xAOD::VxType::PriVtx) {
      m_hardScatterVertexPlots.fill(*vtx);
    }
>>>>>>> release/21.0.127
  }

<<<<<<< HEAD
}

//
//Fill Fake Rates
//

void
InDetRttPlots::fillFakeRate(const xAOD::TrackParticle& track, const bool isFake, const bool isAssociatedTruth, const float mu, const unsigned int nVtx){

  m_missingTruthFakePlots.fill(track, !isAssociatedTruth);
  m_anTrackingPlots.fillUnlinked(track, !isAssociatedTruth, mu, nVtx);
  if(m_iDetailLevel >= 200){
    if (!isAssociatedTruth) m_hitsUnlinkedTracksPlots->fill(track);
    else m_hitsFakeTracksPlots->fill(track);
  }
  if(isAssociatedTruth) {
    m_fakePlots.fill(track, isFake);
      m_anTrackingPlots.fillFakeRate(track, isFake, mu, nVtx);

    if(m_iDetailLevel >= 200){
      std::bitset<xAOD::TrackPatternRecoInfo::NumberOfTrackRecoInfo>  patternInfo = track.patternRecoInfo();
      
      bool isSiSpSeededFinder = patternInfo.test(0);
      bool isInDetExtensionProcessor = patternInfo.test(3);
      bool isTRTSeededTrackFinder = patternInfo.test(4);
      bool isTRTStandalone = patternInfo.test(20);
      bool isSiSpacePointsSeedMaker_LargeD0 = patternInfo.test(49);

      if(isSiSpSeededFinder and not isInDetExtensionProcessor) m_fakeSiSPSeededFinderPlots->fill(track, isFake); //No extensions 
      if(isInDetExtensionProcessor and not (isTRTSeededTrackFinder or isSiSpacePointsSeedMaker_LargeD0)) m_fakeInDetExtensionProcessorPlots->fill(track, isFake); //Extensions but not Back-tracking
      if(isTRTSeededTrackFinder and not isTRTStandalone) m_fakeTRTSeededTrackFinderPlots->fill(track, isFake); //BackTracking
      if(isTRTStandalone) m_fakeTRTStandalonePlots->fill(track, isFake); //TRT standalone
      if(isSiSpacePointsSeedMaker_LargeD0) m_fakeSiSpacePointsSeedMaker_LargeD0Plots->fill(track, isFake); //ANT
    }
  }

}



//
//Fill Vertexing Plots
//
void
InDetRttPlots::fill(const xAOD::VertexContainer& vertexContainer, const std::vector<const xAOD::TruthVertex*>& truthHSVertices, const std::vector<const xAOD::TruthVertex*>& truthPUVertices) {
  // fill vertex container general properties
  // m_verticesVsMuPlots.fill(vertexContainer); //if ever needed
  // fill vertex-specific properties, for all vertices and for hard-scattering vertex
  for (const auto& vtx : vertexContainer.stdcont()) {
    if (vtx->vertexType() == xAOD::VxType::NoVtx) {
      ATH_MSG_DEBUG("IN InDetRttPlots::fill, found xAOD::VxType::NoVtx");
      continue; // skip dummy vertex
    }
    m_vertexPlots.fill(*vtx);
    ATH_MSG_DEBUG("IN InDetRttPlots::fill, filling for all vertices");
    if (vtx->vertexType() == xAOD::VxType::PriVtx) {
      m_hardScatterVertexPlots.fill(*vtx);
      if(truthHSVertices.size()>0)m_hardScatterVertexTruthMatchingPlots.fill(*vtx,truthHSVertices[0]);
      else m_hardScatterVertexTruthMatchingPlots.fill(*vtx);
      ATH_MSG_DEBUG("IN InDetRttPlots::fill, filling for all HS vertex");
    }
  }
  if(m_iDetailLevel >= 200){
    m_vertexTruthMatchingPlots->fill(vertexContainer, truthHSVertices, truthPUVertices);
  }
=======
void
InDetRttPlots::fill(const xAOD::VertexContainer& vertexContainer, const xAOD::EventInfo& ei) {
  m_verticesPlots.fill(vertexContainer, ei);
}

void
InDetRttPlots::fillCounter(const unsigned int freq, const InDetPerfPlot_nTracks::CounterCategory counter) {
  m_nTracks.fill(freq, counter);
}

void
InDetRttPlots::fillFakeRate(const xAOD::TrackParticle& particle, const bool match,
                            const InDetPerfPlot_fakes::Category c) {
  m_fakePlots.fill(particle, match, c);
}

bool
InDetRttPlots::filltrkInJetPlot(const xAOD::TrackParticle& particle, const xAOD::Jet& jet) {
  ATH_MSG_VERBOSE("Filling trk in jet");
  bool pass = m_trkInJetPlot.fill(particle, jet);
  ATH_MSG_VERBOSE("Filling trk in jet hi pt");
  m_trkInJetPlot_highPt.fill(particle, jet);
  return pass;
>>>>>>> release/21.0.127
}


void
<<<<<<< HEAD
InDetRttPlots::fill(const xAOD::VertexContainer& vertexContainer, unsigned int nPU) {
  m_verticesVsMuPlots.fill(vertexContainer, nPU);
=======
InDetRttPlots::fillSimpleJetPlots(const xAOD::TrackParticle& particle, float prob) {
  // the full suit of track plots
  m_trkInJetPtPlot.fill(particle);
  m_trkInJetBasicPlot.fill(particle);
  m_trkInJetTrackRecoInfoPlots.fill(particle);
  m_trkInJetHitsDetailedPlots.fill(particle);

  if (std::isnan(prob)) {
    return;
  }
  const bool isFake = (prob < m_truthProbLowThreshold);
  m_trkInJetFakePlots.fill(particle, isFake, InDetPerfPlot_fakes::ALL);
>>>>>>> release/21.0.127
}

//
//Fill Counters
//
void
<<<<<<< HEAD
InDetRttPlots::fillCounter(const unsigned int freq, const InDetPerfPlot_nTracks::CounterCategory counter) {
  m_nTracks.fill(freq, counter);
=======
InDetRttPlots::fillJetHitsPlots(const xAOD::TrackParticle& particle, float prob, int barcode) {
  if (prob < m_truthProbLowThreshold) {
    m_trkInJetHitsFakeTracksPlots.fill(particle);
  } else if (barcode < 100000 && barcode != 0) {
    m_trkInJetHitsMatchedTracksPlots.fill(particle);
  }
>>>>>>> release/21.0.127
}

//Track in Jet Plots
void
<<<<<<< HEAD
InDetRttPlots::fill(const xAOD::TrackParticle& track, const xAOD::Jet& jet, bool isBjet, bool isFake, bool isUnlinked, bool truthIsFromB){
  m_trkInJetPlots->fill(track, jet);
  if (m_iDetailLevel >= 200){
    if (isFake){
      m_trkInJetPlots_fake->fill(track,jet); 
    }
    else if (isUnlinked){
      m_trkInJetPlots_unlinked->fill(track,jet); 
    }
    else {
      m_trkInJetPlots_matched->fill(track,jet); 
    }
  }
  if(isBjet && m_doTrackInBJetPlots){
    m_trkInJetPlots_bjets->fill(track, jet);
    if ( truthIsFromB ) { // truth from B decay
      m_trkInJetPlots_truthFromB->fill(track, jet);
    }
     
    if (m_iDetailLevel >= 200){
      if (isFake){
        m_trkInJetPlots_fake_bjets->fill(track,jet); 
      }
      else if (isUnlinked){
        m_trkInJetPlots_unlinked_bjets->fill(track,jet); 
      }
      else {
        m_trkInJetPlots_matched_bjets->fill(track,jet); 
      }
    }
  }
}

void
InDetRttPlots::fillEfficiency(const xAOD::TruthParticle& truth, const xAOD::Jet& jet, bool isEfficient, bool isBjet, bool truthIsFromB) {
  m_trkInJetPlots->fillEfficiency(truth, jet, isEfficient); 
  if(isBjet && m_doTrackInBJetPlots) m_trkInJetPlots_bjets->fillEfficiency(truth, jet, isEfficient);
  
  if ( isBjet and m_doTrackInBJetPlots and truthIsFromB ) { // truth is from B
    m_trkInJetPlots_truthFromB->fillEfficiency(truth, jet, isEfficient);
  }
}

void
InDetRttPlots::fillFakeRate(const xAOD::TrackParticle& track, const xAOD::Jet& jet, bool isFake, bool isBjet, bool truthIsFromB) {
  m_trkInJetPlots->fillFakeRate(track, jet, isFake); 
  if(isBjet && m_doTrackInBJetPlots) m_trkInJetPlots_bjets->fillFakeRate(track, jet, isFake); 

  if ( isBjet and m_doTrackInBJetPlots and truthIsFromB ) { // truth is from B
    m_trkInJetPlots_truthFromB->fillFakeRate(track, jet, isFake);
  }
=======
InDetRttPlots::fillJetResPlots(const xAOD::TrackParticle& particle, const xAOD::TruthParticle& truth,
                               const xAOD::Jet& jet) {
  // fill pull and resolution plots
  m_trkInJetResPlots.fill(particle, truth);
  if (particle.pt() > 10e3) { // 10 GeV
    m_trkInJetHighPtResPlots.fill(particle, truth);
  }
  if (m_moreJetPlots) {
    float dR(jet.p4().DeltaR(particle.p4()));
    if (dR < 0.1) {
      m_trkInJetResPlotsDr0010->fill(particle, truth);
    } else if (dR < 0.2) {
      m_trkInJetResPlotsDr1020->fill(particle, truth);
    } else if (dR < 0.3) {
      m_trkInJetResPlotsDr2030->fill(particle, truth);
    }
  } // m_moreJetPlots
}

void
InDetRttPlots::fillJetEffPlots(const xAOD::TruthParticle& truth, const xAOD::Jet& jet) {
  m_trkInJetPlot.BookEffReco(truth, jet);         // fill hists with truth info!
  m_trkInJetPlot_highPt.BookEffReco(truth, jet);  // fill hists with truth info!
}

void
InDetRttPlots::jet_fill(const xAOD::TrackParticle& track, const xAOD::Jet& jet, float weight) {
  m_trkInJetEffPlots.jet_fill(track, jet, weight);
}

void
InDetRttPlots::jetBMR(const xAOD::TrackParticle& track, const xAOD::Jet& jet, float weight) {
  m_BadMatchRate.jetBMR(track, jet, weight);
>>>>>>> release/21.0.127
}

//IDPVM Ntuple
void
<<<<<<< HEAD
InDetRttPlots::fillNtuple(const xAOD::TrackParticle& track) {
  // Fill track only entries with dummy truth values
  m_ntupleTruthToReco.fillTrack(track);
  m_ntupleTruthToReco.fillTree();
}

void
InDetRttPlots::fillNtuple(const xAOD::TruthParticle& truth) {
  // Fill truth only entries with dummy track values
  m_ntupleTruthToReco.fillTruth(truth);
  m_ntupleTruthToReco.fillTree();
}

void 
InDetRttPlots::fillNtuple(const xAOD::TrackParticle& track, const xAOD::TruthParticle& truth, const int truthMatchRanking) {
  // Fill track and truth entries
  m_ntupleTruthToReco.fillTrack(track, truthMatchRanking);
  m_ntupleTruthToReco.fillTruth(truth);
  m_ntupleTruthToReco.fillTree();
}
=======
InDetRttPlots::fillJetTrkTruth(const xAOD::TruthParticle& truth, const xAOD::Jet& jet) {
  m_trkInJetPlot.BookEffTruth(truth, jet);
  m_trkInJetPlot_highPt.BookEffTruth(truth, jet);
}

void
InDetRttPlots::fillJetPlotCounter(const xAOD::Jet& jet) {
  m_trkInJetPlot.fillCounter(jet);
  m_trkInJetPlot_highPt.fillCounter(jet);
}

void
InDetRttPlots::fillJetTrkTruthCounter(const xAOD::Jet& jet) {
  m_trkInJetPlot.fillEff(jet);
  m_trkInJetPlot_highPt.fillEff(jet);
}

void
InDetRttPlots::fillIncTrkRate(const unsigned int nMuEvents, std::vector<int> incTrkNum, std::vector<int> incTrkDenom) {
  m_fakePlots.fillIncTrkRate(nMuEvents, incTrkNum, incTrkDenom);
}
>>>>>>> release/21.0.127
