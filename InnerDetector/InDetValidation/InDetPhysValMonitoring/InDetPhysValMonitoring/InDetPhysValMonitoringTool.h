/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#ifndef INDETPHYSVALMONITORING_INDETPHYSVALMONITORINGTOOL_H
#define INDETPHYSVALMONITORING_INDETPHYSVALMONITORINGTOOL_H
/**
 * @file InDetPhysValMonitoringTool.h
 * header file for class of same name
 * @author shaun roe
 * @date 21 February 2014
**/
<<<<<<< HEAD

//local include
#include "InDetPhysValMonitoring/IAthSelectionTool.h"
#include "InDetPhysValMonitoring/CutFlow.h"
=======




//local include
#include "InDetPhysValMonitoring/IInDetPhysValDecoratorTool.h"
#include "InDetPhysValMonitoring/IAthSelectionTool.h"
>>>>>>> release/21.0.127

//#include "PATCore/IAsgSelectionTool.h"
#include "AthenaMonitoring/ManagedMonitorToolBase.h"
#include "InDetTrackSelectionTool/IInDetTrackSelectionTool.h"

<<<<<<< HEAD
#include "InDetTrackSystematicsTools/InDetTrackTruthOriginTool.h"
#include "InDetTrackSystematicsTools/InDetTrackTruthOriginDefs.h"

#include "InDetTruthVertexValidation/IInDetVertexTruthMatchTool.h"

=======
>>>>>>> release/21.0.127
//#gaudi includes
#include "GaudiKernel/ToolHandle.h"
//EDM includes
#include "xAODTruth/TruthParticleContainer.h"
<<<<<<< HEAD
#include "xAODTruth/TruthPileupEventContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"

//Athena
#include "AtlasDetDescr/AtlasDetectorID.h"
#include "CxxUtils/checker_macros.h"
#include "InDetIdentifier/PixelID.h"
#include "InDetIdentifier/SCT_ID.h"
#include "InDetIdentifier/TRT_ID.h"
#include "TRT_ReadoutGeometry/TRT_DetectorManager.h"
=======
//Athena
#include "AtlasDetDescr/AtlasDetectorID.h"
#include "InDetIdentifier/PixelID.h"
#include "InDetIdentifier/SCT_ID.h"
#include "InDetIdentifier/TRT_ID.h"
#include "InDetReadoutGeometry/PixelDetectorManager.h"
#include "InDetReadoutGeometry/SCT_DetectorManager.h"
#include "InDetReadoutGeometry/TRT_DetectorManager.h"
>>>>>>> release/21.0.127
//STL includes
#include <string>
#include <vector>


//fwd declaration
class IInDetPhysValDecoratorTool;
class InDetRttPlots;
namespace IDPVM{
    class CachedGetAssocTruth;
}
namespace IDPVM{
  class CachedGetAssocTruth;
}



/**
 * Tool to book and fill inner detector histograms for physics validation
 */
class InDetPhysValMonitoringTool:public ManagedMonitorToolBase{
public:
    ///Constructor with parameters
    InDetPhysValMonitoringTool(const std::string & type, const std::string & name, const IInterface* parent);
    ///Destructor
    virtual ~InDetPhysValMonitoringTool();
    /** \name BaseclassMethods Baseclass methods reimplemented 
    }**/
    //@{
    virtual StatusCode initialize();
    virtual StatusCode bookHistograms();
    virtual StatusCode fillHistograms();
    virtual StatusCode procHistograms();
    //@}
private:
<<<<<<< HEAD
=======
	///prevent default construction
	InDetPhysValMonitoringTool();
  // Private utility methods
  void fillTrackCutFlow(Root::TAccept& accept);
  void fillCutFlow(Root::TAccept& accept, std::vector<std::string> & names, std::vector<int> & cutFlow);
  // Get truth particles into a vector, possibly using the pileup from the event
	const std::vector<const xAOD::TruthParticle *> getTruthParticles();
	//
	const Trk::TrackParameters* getUnbiasedTrackParameters(const Trk::TrackParameters* trkParameters, const Trk::MeasurementBase* measurement );
  // Get a data container; implementation at end of this header file
  template<class T>
	const T* getContainer( const std::string & containerName);
	// Do Jet/TIDE plots (Tracking In Dense Environment)
	StatusCode doJetPlots(const xAOD::TrackParticleContainer * pTracks, 
	                      IDPVM::CachedGetAssocTruth & association,
	                      const  xAOD::Vertex * primaryVtx);
	///TrackParticle container's name
	std::string m_trkParticleName;
	///TruthParticle container's name
	std::string m_truthParticleName;
	///Primary vertex container's name
	std::string m_vertexContainerName;
	///Truth vertex container's name
	std::string m_truthVertexContainerName;
	///EventInfo container name
	std::string m_eventInfoContainerName;

	///Directory name
	std::string m_dirName;

	///histograms
	std::unique_ptr< InDetRttPlots > m_monPlots;
	///Tool for selecting tracks
	bool m_useTrackSelection;
	bool m_onlyInsideOutTracks;
	bool m_TrkSelectPV;   // make track selection relative to PV
	ToolHandle<InDet::IInDetTrackSelectionTool> m_trackSelectionTool;
  ToolHandle<IAthSelectionTool> m_truthSelectionTool;
	std::vector<int> m_prospectsMatched;
	int m_twoMatchedEProb;
	int m_threeMatchedEProb;
	int m_fourMatchedEProb;
	int m_truthCounter;

	std::vector<std::string> m_trackCutflowNames;
	std::vector<int> m_trackCutflow;
	std::vector<unsigned int> m_truthCutCounters;
	std::string m_pileupSwitch; // All, PileUp, or HardScatter
	
	///Jet Things
	std::string m_jetContainerName;
	float m_maxTrkJetDR;
	bool m_fillTIDEPlots;
	bool m_fillExtraTIDEPlots;

	std::string m_folder;
};

template<class T>
  inline 
	const T* InDetPhysValMonitoringTool::getContainer(const std::string & containerName){
		const T * ptr = evtStore()->retrieve< const T >( containerName );
    	if (!ptr) {
        	ATH_MSG_WARNING("Container '"<<containerName<<"' could not be retrieved");
    	}
    	return ptr;
	}
>>>>>>> release/21.0.127

    ///prevent default construction
    InDetPhysValMonitoringTool();
    // Private utility methods
    void fillTrackCutFlow(const asg::AcceptData& accept);
    void fillCutFlow(const asg::AcceptData& accept, std::vector<std::string> & names, std::vector<int> & cutFlow);
    // Get truth particles into a vector, possibly using the pileup from the event
    const std::vector<const xAOD::TruthParticle *> getTruthParticles() const;
    std::pair<const std::vector<const xAOD::TruthVertex*>, const std::vector<const xAOD::TruthVertex*>> getTruthVertices() const;

    //
    const Trk::TrackParameters* getUnbiasedTrackParameters(const Trk::TrackParameters* trkParameters, const Trk::MeasurementBase* measurement );
    // Do Jet/TIDE plots (Tracking In Dense Environment)
    StatusCode fillJetHistograms(const xAOD::TrackParticleContainer * pTracks, 
                        IDPVM::CachedGetAssocTruth & association,
                        const  xAOD::Vertex * primaryVtx,
                        const std::vector<const xAOD::TruthParticle*> &truthParticles);

	// accessors/decorators
    SG::AuxElement::Accessor<bool>  m_acc_hasTruthFilled{"hasTruthFilled"};
    SG::AuxElement::Decorator<bool> m_dec_hasTruthFilled{"hasTruthFilled"};	
    SG::AuxElement::Decorator<bool> m_dec_passedTruthSelection{"passedTruthSelection"};	
    SG::AuxElement::Decorator<bool> m_dec_passedTrackSelection{"passedTrackSelection"};	
    SG::AuxElement::Accessor<bool>  m_acc_selectedByPileupSwitch{"selectedByPileupSwitch"};
    SG::AuxElement::Decorator<bool> m_dec_selectedByPileupSwitch{"selectedByPileupSwitch"};

    // decorate track particle for ntuple writing
    void decorateTrackParticle(const xAOD::TrackParticle & track, const asg::AcceptData & passed) const;

    // decorate truth particle for ntuple writing
    void decorateTruthParticle(const xAOD::TruthParticle & truth, const IAthSelectionTool::CutResult & passed) const;

    // safely check the "hasTruthFilled" decoration on a truth particle
    bool hasTruthFilled(const xAOD::TruthParticle & truth) const;
    
    // safely check the "selectedByPileupSwitch" decoration on a truth particle
    bool isSelectedByPileupSwitch(const xAOD::TruthParticle & truth) const;
   
    // set the "selectedByPileupSwitch" decoration for all particles in the passed vector
    void markSelectedByPileupSwitch(const std::vector<const xAOD::TruthParticle*> & truthParticles) const;

    ///TrackParticle container's name
    SG::ReadHandleKey<xAOD::TrackParticleContainer>  m_trkParticleName
        {this,"TrackParticleContainerName", "InDetTrackParticles"};

    ///TruthParticle container's name
    SG::ReadHandleKey<xAOD::TruthParticleContainer> m_truthParticleName
        {this, "TruthParticleContainerName",  "TruthParticles", ""};

    ///Primary vertex container's name
    SG::ReadHandleKey<xAOD::VertexContainer>  m_vertexContainerName
        {this,"VertexContainerName", "PrimaryVertices", ""};

    ///Truth vertex container's name
    SG::ReadHandleKey<xAOD::TruthVertexContainer> m_truthVertexContainerName
        {this,"TruthVertexContainerName",  "TruthVertices",""};

    ///EventInfo container name
    SG::ReadHandleKey<xAOD::EventInfo> m_eventInfoContainerName
        {this,"EventInfoContainerName", "EventInfo", ""};

    SG::ReadHandleKey<xAOD::TruthEventContainer> m_truthEventName
        {this, "TruthEvents", "TruthEvents","Name of the truth events container probably either TruthEvent or TruthEvents"};

    SG::ReadHandleKey<xAOD::TruthPileupEventContainer> m_truthPileUpEventName
        {this, "TruthPileupEvents", "TruthPileupEvents","Name of the truth pileup events container probably TruthPileupEvent(s)"};

    SG::ReadHandleKey<xAOD::JetContainer> m_jetContainerName
        {this, "JetContainerName", "AntiKt4LCTopoJets" , ""};


    // needed to indicate data dependencies
    std::vector<SG::ReadDecorHandleKey<xAOD::TrackParticleContainer> > m_floatTrkDecor;
    std::vector<SG::ReadDecorHandleKey<xAOD::TrackParticleContainer> > m_intTrkDecor;
    std::vector<SG::ReadDecorHandleKey<xAOD::TruthParticleContainer> > m_floatTruthDecor;
    std::vector<SG::ReadDecorHandleKey<xAOD::TruthParticleContainer> > m_intTruthDecor;
    std::vector<SG::ReadDecorHandleKey<xAOD::JetContainer> > m_intJetDecor;

    ///Directory name
    std::string m_dirName;

    ///histograms
    std::unique_ptr< InDetRttPlots > m_monPlots;
    ///Tool for selecting tracks
    bool m_useTrackSelection;
    bool m_useVertexTruthMatchTool;
    bool m_TrkSelectPV;   // make track selection relative to PV
    bool m_doTruthOriginPlots;
    ToolHandle<InDet::IInDetTrackSelectionTool> m_trackSelectionTool;
    ToolHandle<IInDetVertexTruthMatchTool> m_vtxValidTool;
    ToolHandle<IAthSelectionTool> m_truthSelectionTool;
    ToolHandle<InDet::IInDetTrackTruthOriginTool> m_trackTruthOriginTool{this, "trackTruthOriginTool", "InDet::InDetTrackTruthOriginTool"};

    mutable std::mutex  m_mutex;
    mutable CutFlow     m_truthCutFlow ATLAS_THREAD_SAFE; // Guarded by m_mutex
    std::vector<int> m_prospectsMatched;
    float m_lowProb;
    float m_highProb;
    int m_detailLevel;
    int m_truthCounter;

    std::vector<std::string> m_trackCutflowNames;
    std::vector<int> m_trackCutflow;
    std::string m_pileupSwitch; // All, PileUp, or HardScatter
    ///Jet Things

    float m_maxTrkJetDR;
    bool m_doTrackInJetPlots;
    bool m_doBjetPlots; 
	bool m_fillTruthToRecoNtuple;

    std::string m_folder;
};
#endif
