/*
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
*/

#include "InDetBeamSpotReader.h"
#include "VxVertex/VxCandidate.h"

InDet::InDetBeamSpotReader::InDetBeamSpotReader(const std::string& name, ISvcLocator* pSvcLocator):
  AthReentrantAlgorithm(name, pSvcLocator)
{
}

StatusCode InDet::InDetBeamSpotReader::initialize() {
  ATH_MSG_DEBUG( "in initialize()" );
<<<<<<< HEAD
=======
  
 
  if ( m_toolSvc.retrieve().isFailure() ) {
    ATH_MSG_FATAL( "Failed to retrieve service " << m_toolSvc );
    return StatusCode::FAILURE;
  } else 
    ATH_MSG_INFO( "Retrieved service " << m_toolSvc );
>>>>>>> release/21.0.127

  ATH_CHECK( m_beamSpotKey.initialize() );
  ATH_CHECK( m_vxContainer.initialize(!m_vxContainer.empty()) );

<<<<<<< HEAD
  return StatusCode::SUCCESS;
}

StatusCode InDet::InDetBeamSpotReader::execute(const EventContext& ctx) const {
  ATH_MSG_DEBUG( "in execute()");

  SG::ReadCondHandle<InDet::BeamSpotData> beamSpotHandle { m_beamSpotKey, ctx };
  ATH_MSG_INFO( "In event " << ctx.eventID() );
  ATH_MSG_INFO("BeamSpot Position: "
               << beamSpotHandle->beamPos()[0] << " "
               << beamSpotHandle->beamPos()[1] << " "
               << beamSpotHandle->beamPos()[2]
               <<", Sigma: "
               << beamSpotHandle->beamSigma(0) << " "
               << beamSpotHandle->beamSigma(1) << " "
               << beamSpotHandle->beamSigma(2)
               << ", Tilt: "
               << beamSpotHandle->beamTilt(0) << " "
               << beamSpotHandle->beamTilt(1)
               << ", Status: "
               << beamSpotHandle->beamStatus());

  //get list of PVs
  if (!m_vxContainer.empty()) {
    ATH_MSG_INFO("Beamspot position at PV z-position");
    SG::ReadHandle<VxContainer> importedVxContainer(m_vxContainer, ctx);
    for (const Trk::VxCandidate* vtx : *importedVxContainer) {
      if (static_cast<int>(vtx->vxTrackAtVertex()->size())==0) continue;
      ATH_MSG_INFO("PV position: " << vtx->recVertex().position() );
      double z = vtx->recVertex().position().z();
      ATH_MSG_INFO("\n\t"
                   << beamSpotHandle->beamPos()(0)
                   + (z - beamSpotHandle->beamPos()(2))
                   *beamSpotHandle->beamTilt(0) << "\n\t"
                   << beamSpotHandle->beamPos()(1)
                   + (z - beamSpotHandle->beamPos()(2))
                   *beamSpotHandle->beamTilt(1) );
    }
  }
=======
  if ( m_beamSpotSvc.retrieve().isFailure() ) {
    ATH_MSG_FATAL( "Failed to retrieve service " << m_beamSpotSvc );
    return StatusCode::FAILURE;
  } else 
    ATH_MSG_INFO( "Retrieved service " << m_beamSpotSvc );
  
  
  return StatusCode::SUCCESS;
}

StatusCode InDet::InDetBeamSpotReader::execute(){
 
  ATH_MSG_DEBUG( "in execute()");

  //get the set of 
  const DataHandle<EventInfo> eventInfo;
  if (StatusCode::SUCCESS != evtStore()->retrieve( eventInfo ) ){
    ATH_MSG_ERROR( "Cannot get event info." );
    return StatusCode::FAILURE;
  }
  EventID* eventID = eventInfo->event_ID();
    ATH_MSG_INFO( "In event " << (*eventID) );
    ATH_MSG_INFO("BeamSpot Position: \n "
		   << m_beamSpotSvc->beamPos() );
    ATH_MSG_INFO("BeamSpot Sigma\n\t"
		   << m_beamSpotSvc->beamSigma(0) << "\n\t"
		   << m_beamSpotSvc->beamSigma(1) << "\n\t"
		   << m_beamSpotSvc->beamSigma(2) << "\n\t");
    ATH_MSG_INFO("BeamSpot Tilt\n\t"
		   << m_beamSpotSvc->beamTilt(0) << "\n\t"
		   << m_beamSpotSvc->beamTilt(1) << "\n\t");
    ATH_MSG_INFO("Beamspot position at PV z-position");

  const VxContainer* importedVxContainer =0;
  static const std::string m_containerName = "VxPrimaryCandidate";
  if ( StatusCode::SUCCESS != evtStore()->retrieve(importedVxContainer,m_containerName)){
    ATH_MSG_ERROR( "No " << m_containerName << " found in StoreGate" );
    return StatusCode::FAILURE;
  }
  //get list of PVs
  VxContainer::const_iterator vtxItr;
  for(vtxItr=importedVxContainer->begin(); 
      vtxItr!=importedVxContainer->end(); ++vtxItr) {
    if (static_cast<int>((*vtxItr)->vxTrackAtVertex()->size())==0) continue;
    if (msgLvl(MSG::INFO)) ATH_MSG_INFO("PV position:  " 
				 << (*vtxItr)->recVertex().position() );
    double z = (*vtxItr)->recVertex().position().z();
    if (msgLvl(MSG::INFO)) ATH_MSG_INFO("\n\t"
	  << m_beamSpotSvc->beamPos()(0) 
      + (z - m_beamSpotSvc->beamPos()(2))
      *m_beamSpotSvc->beamTilt(0) << "\n\t"
	  << m_beamSpotSvc->beamPos()(1)
      + (z - m_beamSpotSvc->beamPos()(2))
      *m_beamSpotSvc->beamTilt(1) );
  }
  
  return StatusCode::SUCCESS;
}



StatusCode InDet::InDetBeamSpotReader::finalize() {
  ATH_MSG_DEBUG( "in finalize()" );
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}
