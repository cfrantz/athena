#include "src/IDAlignMonEfficiencies.h"
#include "src/IDAlignMonGenericTracks.h"
#include "src/InDetAlignMonBeamSpot.h"
#include "src/IDAlignMonResiduals.h"
#include "src/IDAlignMonTruthComparison.h"
#include "src/IDAlignMonNtuple.h" 
#include "src/IDAlignMonTrackSegments.h" 
#include "src/IDAlignMonSivsTRT.h" 
#include "src/TrackSelectionTool.h" 
#include "src/TrackSelectionAlg.h" 
#include "src/IDAlignMonPVBiases.h"

<<<<<<< HEAD
DECLARE_COMPONENT( IDAlignMonPVBiases )
DECLARE_COMPONENT( IDAlignMonEfficiencies )
DECLARE_COMPONENT( IDAlignMonGenericTracks )
DECLARE_COMPONENT( InDetAlignMonBeamSpot )
DECLARE_COMPONENT( IDAlignMonResiduals )
DECLARE_COMPONENT( IDAlignMonTruthComparison )
DECLARE_COMPONENT( IDAlignMonNtuple )
DECLARE_COMPONENT( IDAlignMonTrackSegments )
DECLARE_COMPONENT( IDAlignMonSivsTRT )
DECLARE_COMPONENT( InDetAlignMon::TrackSelectionTool )
DECLARE_COMPONENT( TrackSelectionAlg )

=======
#include "GaudiKernel/DeclareFactoryEntries.h"
DECLARE_TOOL_FACTORY(IDAlignMonPVBiases)
DECLARE_TOOL_FACTORY(IDAlignMonEfficiencies)
DECLARE_TOOL_FACTORY(IDAlignMonGenericTracks)
DECLARE_TOOL_FACTORY(InDetAlignMonBeamSpot)
DECLARE_TOOL_FACTORY(IDAlignMonResiduals)
DECLARE_TOOL_FACTORY(IDAlignMonTruthComparison)
DECLARE_TOOL_FACTORY(IDAlignMonNtuple)
DECLARE_TOOL_FACTORY(IDAlignMonTrackSegments)
DECLARE_TOOL_FACTORY(IDAlignMonSivsTRT)
DECLARE_NAMESPACE_TOOL_FACTORY(InDetAlignMon, TrackSelectionTool)
DECLARE_ALGORITHM_FACTORY(TrackSelectionAlg)

DECLARE_FACTORY_ENTRIES(InDetAlignmentMonitoring) {
  DECLARE_NAMESPACE_TOOL(InDetAlignMon, TrackSelectionTool);
}
>>>>>>> release/21.0.127
