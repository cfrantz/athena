/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#ifndef IDAlignMonPVBiases_H
#define IDAlignMonPVBiases_H

// **********************************************************************
// IDAlignMonPVBIases.cxx
// AUTHORS: Ambrosius  Vermeulen, Pierfrancesco Butti
// **********************************************************************

<<<<<<< HEAD
#include "TrkVertexFitterInterfaces/ITrackToVertexIPEstimator.h"
=======
>>>>>>> release/21.0.127
#include <vector>

#include "GaudiKernel/StatusCode.h"
#include "AthenaMonitoring/AthenaMonManager.h"
#include "AthenaMonitoring/ManagedMonitorToolBase.h"
#include "EventPrimitives/EventPrimitives.h"
#include "EventPrimitives/EventPrimitivesHelpers.h"
#include "GaudiKernel/AlgTool.h"
#include "GaudiKernel/ToolHandle.h"
#include "TH3F.h"
#include "TH2F.h"
#include "TFile.h"
<<<<<<< HEAD
#include "StoreGate/ReadHandleKey.h"
#include "xAODEventInfo/EventInfo.h"
=======
>>>>>>> release/21.0.127
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/VertexContainer.h"
#include <map>

namespace Trk  { 
<<<<<<< HEAD
=======
  class ITrackToVertexIPEstimator;
>>>>>>> release/21.0.127
  class VxCandidate;
  class Track;
  class VxTrackAtVertex;
}

namespace InDetAlignMon{
  class TrackSelectionTool;
}

class IDAlignMonPVBiases : public ManagedMonitorToolBase
{

public:

  IDAlignMonPVBiases( const std::string & type, const std::string & name, const IInterface* parent ); 

  virtual ~IDAlignMonPVBiases();
  virtual StatusCode initialize();
  virtual StatusCode fillHistograms();
  virtual StatusCode finalize();
  virtual StatusCode bookHistograms();
  virtual StatusCode procHistograms();

<<<<<<< HEAD
  void InitializeHistograms();

  void RegisterHisto(MonGroup& mon, TH1* histo);
  //void RegisterHisto(MonGroup& mon, TH1F_LW* histo);
  void RegisterHisto(MonGroup& mon, TH2* histo);
  void RegisterHisto(MonGroup& mon, TProfile* histo);
  void RegisterHisto(MonGroup& mon, TProfile2D* histo);

 protected:

	/////////////////////////////////////////////////
    	///////Declare histo's 400MeV until 600MeV///////
    	/////////////////////////////////////////////////
	TH3F* m_trkd0_wrtPV_vs_phi_vs_eta_400MeV_600MeV_positive;
	TH3F* m_trkd0_wrtPV_vs_phi_vs_eta_400MeV_600MeV_negative;

	TH2F* m_trkd0_wrtPV_vs_phi_400MeV_600MeV_positive;
  	TH2F* m_trkd0_wrtPV_vs_phi_400MeV_600MeV_negative;

	TH2F* m_trkd0_wrtPV_vs_eta_400MeV_600MeV_positive;
  	TH2F* m_trkd0_wrtPV_vs_eta_400MeV_600MeV_negative;
 
	/////////////////////////////////////////////////
    	////////Declare histo's 600MeV until 1GeV////////
    	/////////////////////////////////////////////////
	TH3F* m_trkd0_wrtPV_vs_phi_vs_eta_600MeV_1GeV_positive;
	TH3F* m_trkd0_wrtPV_vs_phi_vs_eta_600MeV_1GeV_negative;

	TH2F* m_trkd0_wrtPV_vs_phi_600MeV_1GeV_positive;
  	TH2F* m_trkd0_wrtPV_vs_phi_600MeV_1GeV_negative;

	TH2F* m_trkd0_wrtPV_vs_eta_600MeV_1GeV_positive;
  	TH2F* m_trkd0_wrtPV_vs_eta_600MeV_1GeV_negative;
  
	/////////////////////////////////////////////////
    	/////////Declare histo's 1GeV until 2GeV/////////
    	/////////////////////////////////////////////////
	TH3F* m_trkd0_wrtPV_vs_phi_vs_eta_1GeV_2GeV_positive;
	TH3F* m_trkd0_wrtPV_vs_phi_vs_eta_1GeV_2GeV_negative;

	TH2F* m_trkd0_wrtPV_vs_phi_1GeV_2GeV_positive;
  	TH2F* m_trkd0_wrtPV_vs_phi_1GeV_2GeV_negative;

	TH2F* m_trkd0_wrtPV_vs_eta_1GeV_2GeV_positive;
  	TH2F* m_trkd0_wrtPV_vs_eta_1GeV_2GeV_negative;
  
	/////////////////////////////////////////////////
    	/////////Declare histo's 2GeV until 5GeV/////////
    	/////////////////////////////////////////////////
	TH3F* m_trkd0_wrtPV_vs_phi_vs_eta_2GeV_5GeV_positive;
	TH3F* m_trkd0_wrtPV_vs_phi_vs_eta_2GeV_5GeV_negative;

	TH2F* m_trkd0_wrtPV_vs_phi_2GeV_5GeV_positive;
  	TH2F* m_trkd0_wrtPV_vs_phi_2GeV_5GeV_negative;

	TH2F* m_trkd0_wrtPV_vs_eta_2GeV_5GeV_positive;
  	TH2F* m_trkd0_wrtPV_vs_eta_2GeV_5GeV_negative;
  
	/////////////////////////////////////////////////
    	////////Declare histo's 5GeV until 10GeV/////////
    	/////////////////////////////////////////////////
	TH3F* m_trkd0_wrtPV_vs_phi_vs_eta_5GeV_10GeV_positive;
	TH3F* m_trkd0_wrtPV_vs_phi_vs_eta_5GeV_10GeV_negative;

	TH2F* m_trkd0_wrtPV_vs_phi_5GeV_10GeV_positive;
  	TH2F* m_trkd0_wrtPV_vs_phi_5GeV_10GeV_negative;
  
	TH2F* m_trkd0_wrtPV_vs_eta_5GeV_10GeV_positive;
  	TH2F* m_trkd0_wrtPV_vs_eta_5GeV_10GeV_negative;
  
	/////////////////////////////////////////////////
    	/////////Declare histo's larger than 10GeV///////
    	/////////////////////////////////////////////////
	TH3F* m_trkd0_wrtPV_vs_phi_vs_eta_10GeV_positive;
	TH3F* m_trkd0_wrtPV_vs_phi_vs_eta_10GeV_negative;

	TH2F* m_trkd0_wrtPV_vs_phi_10GeV_positive;
  	TH2F* m_trkd0_wrtPV_vs_phi_10GeV_negative;

	TH2F* m_trkd0_wrtPV_vs_eta_10GeV_positive;
  	TH2F* m_trkd0_wrtPV_vs_eta_10GeV_negative;

private:

  //const AtlasDetectorID*                m_idHelper;
  //const PixelID*                        m_pixelID;
  //const SCT_ID*                         m_sctID; 
  //const TRT_ID*                         m_trtID; 

  int m_checkrate;
  int m_events; 
  int m_histosBooked;
  std::string m_tracksName;
  std::string m_triggerChainName;
  std::string m_VxPrimContainerName;
  PublicToolHandle< Trk::ITrackToVertexIPEstimator >  m_trackToVertexIPEstimator
     {this,"TrackToVertexIPEstimator","Trk::TrackToVertexIPEstimator",""};
  //std::string m_TreeFolder;
  //TTree* m_Tree;
  //std::string m_TreeName;

  unsigned int            m_runNumber;
  unsigned int            m_evtNumber;
  unsigned int            m_lumi_block;
=======
private:
  bool fillVertexInformation() const;
  const xAOD::Vertex* findAssociatedVertexTP(const xAOD::TrackParticle *) const;
  
  mutable std::map<const xAOD::TrackParticle*, const xAOD::Vertex* > m_trackVertexMapTP;

  int m_checkrate;
  int m_events; 
  std::string m_tracksName;
  std::string m_triggerChainName;
  std::string m_VxPrimContainerName;
  ToolHandle< Trk::ITrackToVertexIPEstimator >  m_trackToVertexIPEstimator;
  const xAOD::VertexContainer* m_vertices;
  std::string m_TreeFolder;
  TTree* m_Tree;
  std::string m_TreeName;

  mutable unsigned int            m_runNumber;
  mutable unsigned int            m_evtNumber;
  mutable unsigned int            m_lumi_block;
>>>>>>> release/21.0.127

  double m_charge;
  double m_pt;
  double m_eta;
  double m_phi;
  double m_z0;
  double m_d0;
  double m_z0_err;
  double m_d0_err;
  double m_vertex_x;
  double m_vertex_y;
  double m_vertex_z;
<<<<<<< HEAD
	
  ToolHandle< InDetAlignMon::TrackSelectionTool > m_trackSelection; 

  SG::ReadHandleKey<xAOD::EventInfo> m_eventInfoKey{this, "EventInfoKey", "EventInfo", "SG Key of EventInfo object"};
  SG::ReadHandleKey<xAOD::TrackParticleContainer> m_trackParticleKey{this, "TrackParticleKey", "InDetTrackParticles"};
  SG::ReadHandleKey<xAOD::VertexContainer> m_vertexKey { this, "VertexContainer", "PrimaryVertices", "primary vertex container" };
=======

  //Daiki stuf
  double bsXerror;
  double bsYerror;
  double bsXYerror;
  double bsZerror;
	
  ToolHandle< InDetAlignMon::TrackSelectionTool > m_trackSelection; 
>>>>>>> release/21.0.127
};

#endif
