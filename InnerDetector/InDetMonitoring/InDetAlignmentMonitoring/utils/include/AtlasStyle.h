/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

//
//   @file    AtlasStyle.h         
//   
//            ATLAS Style, based on a style file from BaBar
//
//
//   @author M.Sutton
// 
<<<<<<< HEAD
=======
//   Copyright (C) 2010 Atlas Collaboration
>>>>>>> release/21.0.127
//
//   $Id: AtlasStyle.h, v0.0   Thu 25 Mar 2010 10:34:20 CET $

#ifndef  __ATLASSTYLE_H
#define __ATLASSTYLE_H

#include "TStyle.h"

void SetAtlasStyle();

TStyle* AtlasStyle(); 

#endif // __ATLASSTYLE_H
