// -*- C++ -*-

/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/**
 * @file SCT_NameFormatter.h
 * Contains string formatting utility functions for use in SCT_Monitoring
 * @ author shaun roe
 **/
#ifndef SCT_NameFormatter_H
#define SCT_NameFormatter_H

#include <string>

namespace SCT_Monitoring {
  ///format an element index (e.g. in looping through barrels, this goes from 0->(2*(nbarrels)) - 1 into layer and side
  ///for use in both the histogram title and its name
  class LayerSideFormatter {
<<<<<<< HEAD
  private:
    const unsigned int m_element;
    const std::string m_layerStr;
    const std::string m_sideStr;
    unsigned int m_region;
  public:
  LayerSideFormatter(const unsigned int i) : m_element(i), m_layerStr(std::to_string(i / 2)), m_sideStr(std::to_string(
                                                                                                                       i % 2)),
=======
private:
    const unsigned int m_element;
    const std::string m_layerStr;
    const std::string m_sideStr;
    unsigned int m_region;// 30.11.2014
public:
    LayerSideFormatter(const unsigned int i) : m_element(i), m_layerStr(std::to_string(i / 2)), m_sideStr(std::to_string(
                                                                                                            i % 2)),
>>>>>>> release/21.0.127
      m_region(1) {
      // nop
    }

<<<<<<< HEAD
  LayerSideFormatter(const unsigned int i, const unsigned int m) : m_element(i), m_layerStr(std::to_string(i / 2)),
      m_sideStr(std::to_string(i % 2)), m_region(m) {
=======
    LayerSideFormatter(const unsigned int i, const unsigned int m) : m_element(i), m_layerStr(std::to_string(i / 2)),
      m_sideStr(std::to_string(i % 2)), m_region(m) {// 30.11.2014
>>>>>>> release/21.0.127
      // nop
    }

    std::string
<<<<<<< HEAD
      layer() const {
=======
    layer() const {
>>>>>>> release/21.0.127
      return m_layerStr;
    }

    std::string
<<<<<<< HEAD
      side() const {
=======
    side() const {
>>>>>>> release/21.0.127
      return m_sideStr;
    }

    std::string
<<<<<<< HEAD
      layerPlus1() const {
=======
    layerPlus1() const {
>>>>>>> release/21.0.127
      return std::to_string((m_element / 2) + 1);
    }

    std::string
<<<<<<< HEAD
      title() const {
      if (m_region == 1) {
        return std::string("Layer ") + m_layerStr + std::string(" Side ") + m_sideStr;
      } else {
=======
    title() const {
      if (m_region == 1) {
        return std::string("Layer ") + m_layerStr + std::string(" Side ") + m_sideStr;
      }else {
>>>>>>> release/21.0.127
        return std::string("Disk ") + m_layerStr + std::string(" Side ") + m_sideStr;
      }
    }

    std::string
<<<<<<< HEAD
      dedicated_title() const {
      if (m_region == 1) {
        return std::string("Layer ") + m_layerStr + std::string(" Side ") + std::to_string((m_element % 2 + 1) % 2);
      } else {
=======
    dedicated_title() const {
      if (m_region == 1) {
        return std::string("Layer ") + m_layerStr + std::string(" Side ") + std::to_string((m_element % 2 + 1) % 2);
      }else {
>>>>>>> release/21.0.127
        return std::string("Disk ") + m_layerStr + std::string(" Side ") + std::to_string((m_element % 2 + 1) % 2);
      }
    }

    std::string
<<<<<<< HEAD
      name(const std::string &delimiter = "_") const {
=======
    name(const std::string &delimiter = "_") const {
>>>>>>> release/21.0.127
      return m_layerStr + delimiter + m_sideStr;
    }
  };
}// end of SCT_Monitoring namespace

#endif
