/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef TRTPhysicsTool_H
#define TRTPhysicsTool_H

// Include files
#include "AthenaBaseComps/AthAlgTool.h"

#include "G4AtlasInterfaces/IPhysicsOptionTool.h"

//to handle
#include "GaudiKernel/ToolHandle.h"

#include "G4VPhysicsConstructor.hh"


/** @class TRTPhysicsTool TRTPhysicsTool.h "TRT:TR_Process/TRTPhysicsTool.h"
 *
 *
 *
 *  @author Edoardo Farina
 *  @date  18-05-2015
 */
<<<<<<< HEAD
class TRTPhysicsTool final: public G4VPhysicsConstructor, public extends<AthAlgTool, IPhysicsOptionTool>
=======
class TRTPhysicsTool : public G4VPhysicsConstructor, public AthAlgTool,
                       virtual public IPhysicsOptionTool
>>>>>>> release/21.0.127
{
public:
  /// Standard constructor
  TRTPhysicsTool( const std::string& type , const std::string& name,
                  const IInterface* parent ) ;

  virtual ~TRTPhysicsTool(){}; ///< Destructor

  /// Initialize method
<<<<<<< HEAD
  virtual StatusCode initialize() override;
  virtual void ConstructParticle() override;
  virtual void ConstructProcess() override;

  /// IPhysicsOptionTool method; simply returns self.
  virtual G4VPhysicsConstructor* GetPhysicsOption() override;

=======
  virtual StatusCode initialize() override final;
  virtual void ConstructParticle() override final;
  virtual void ConstructProcess() override final;

  /// IPhysicsOptionTool method; simply returns self.
  virtual G4VPhysicsConstructor* GetPhysicsOption() override final;

>>>>>>> release/21.0.127
protected:

  std::string m_xmlFile;

};

#endif
