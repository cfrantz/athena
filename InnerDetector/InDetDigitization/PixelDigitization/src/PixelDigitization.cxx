/*
<<<<<<< HEAD
   Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
 */
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

////////////////////////////////////////////////////////////////////////////
// PixelDigitization.cxx
//   Implementation file for class PixelDigitization
////////////////////////////////////////////////////////////////////////////
// (c) ATLAS Detector software
////////////////////////////////////////////////////////////////////////////

#include "PixelDigitization.h"
#include "PixelDigitizationTool.h"
>>>>>>> release/21.0.127

#include "PixelDigitization.h"

// Constructor with parameters:
PixelDigitization::PixelDigitization(const std::string& name,
                                     ISvcLocator* pSvcLocator) :
  AthAlgorithm(name, pSvcLocator) {
}

// Initialize method:
StatusCode PixelDigitization::initialize() {
  ATH_MSG_DEBUG("initialize()");
  ATH_CHECK(m_pixelDigitizationTool.retrieve());
  ATH_MSG_DEBUG("Successfully retreived IPixelDigitizaitonTool.");
  return StatusCode::SUCCESS;
}

// Execute method:
StatusCode PixelDigitization::execute() {
  ATH_MSG_DEBUG("execute()");
  return m_pixelDigitizationTool->processAllSubEvents(Gaudi::Hive::currentContext());
}
