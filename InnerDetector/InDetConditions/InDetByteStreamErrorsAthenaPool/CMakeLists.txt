<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
################################################################################
# Package: InDetByteStreamErrorsAthenaPool
################################################################################
>>>>>>> release/21.0.127

# Declare the package name:
atlas_subdir( InDetByteStreamErrorsAthenaPool )

<<<<<<< HEAD
# Component(s) in the package:
atlas_add_library( InDetByteStreamErrorsAthenaPool
   src/*.cxx
   PUBLIC_HEADERS InDetByteStreamErrorsAthenaPool
   LINK_LIBRARIES AthAllocators AthenaPoolCnvSvcLib Identifier InDetByteStreamErrors )

atlas_add_poolcnv_library( InDetByteStreamErrorsAthenaPoolPoolCnv
   FILES InDetByteStreamErrors/InDetBSErrContainer.h
   InDetByteStreamErrors/TRT_BSIdErrContainer.h
   InDetByteStreamErrors/TRT_BSErrContainer.h
   InDetByteStreamErrors/IDCInDetBSErrContainer.h
   LINK_LIBRARIES InDetByteStreamErrorsAthenaPool )
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Database/AthenaPOOL/AthenaPoolUtilities
   DetectorDescription/Identifier
   PRIVATE
   GaudiKernel
   AtlasTest/TestTools
   Control/DataModel
   Database/AthenaPOOL/AthenaPoolCnvSvc
   InnerDetector/InDetConditions/InDetByteStreamErrors )

# Component(s) in the package:
atlas_add_poolcnv_library( InDetByteStreamErrorsAthenaPoolPoolCnv src/*.cxx
   FILES InDetByteStreamErrors/InDetBSErrContainer.h
   InDetByteStreamErrors/TRT_BSIdErrContainer.h
   InDetByteStreamErrors/TRT_BSErrContainer.h
   LINK_LIBRARIES AthenaPoolUtilities Identifier TestTools DataModel
   AthenaPoolCnvSvcLib )
>>>>>>> release/21.0.127

atlas_add_dictionary( InDetByteStreamErrorsAthenaPoolCnvDict
   InDetByteStreamErrorsAthenaPool/InDetByteStreamErrorsAthenaPoolCnvDict.h
   InDetByteStreamErrorsAthenaPool/selection.xml
<<<<<<< HEAD
   LINK_LIBRARIES InDetByteStreamErrorsAthenaPool )
=======
   LINK_LIBRARIES AthenaPoolUtilities Identifier TestTools DataModel
   AthenaPoolCnvSvcLib )
>>>>>>> release/21.0.127

# Tests in the package:
atlas_add_test( InDetBSErrContainerCnv_p1_test
   SOURCES test/InDetBSErrContainerCnv_p1_test.cxx
   src/InDetBSErrContainerCnv_p1.cxx
<<<<<<< HEAD
   LINK_LIBRARIES GaudiKernel AthenaPoolCnvSvcLib Identifier InDetByteStreamErrors InDetByteStreamErrorsAthenaPool )

atlas_add_test( IDCInDetBSErrContainerCnv_p1_test
   SOURCES test/IDCInDetBSErrContainerCnv_p1_test.cxx
   src/IDCInDetBSErrContainerCnv_p1.cxx
   LINK_LIBRARIES GaudiKernel AthenaPoolCnvSvcLib Identifier InDetByteStreamErrors InDetByteStreamErrorsAthenaPool )

#atlas_add_test( TRT_BSErrContainerCnv_p1_test
#   SOURCES test/TRT_BSErrContainerCnv_p1_test.cxx
#   src/TRT_BSErrContainerCnv_p1.cxx
#   LINK_LIBRARIES GaudiKernel AthenaPoolCnvSvcLib Identifier AthAllocators InDetByteStreamErrors InDetByteStreamErrorsAthenaPool )
=======
   LINK_LIBRARIES GaudiKernel AthenaPoolCnvSvcLib Identifier DataModel )

atlas_add_test( TRT_BSErrContainerCnv_p1_test
   SOURCES test/TRT_BSErrContainerCnv_p1_test.cxx
   src/TRT_BSErrContainerCnv_p1.cxx
   LINK_LIBRARIES GaudiKernel AthenaPoolCnvSvcLib Identifier DataModel )
>>>>>>> release/21.0.127

atlas_add_test( TRT_BSIdErrContainerCnv_p1_test
   SOURCES test/TRT_BSIdErrContainerCnv_p1_test.cxx
   src/TRT_BSIdErrContainerCnv_p1.cxx
<<<<<<< HEAD
   LINK_LIBRARIES GaudiKernel AthenaPoolCnvSvcLib Identifier AthAllocators InDetByteStreamErrors InDetByteStreamErrorsAthenaPool )
=======
   LINK_LIBRARIES GaudiKernel AthenaPoolCnvSvcLib Identifier DataModel )

# Install files from the package:
atlas_install_headers( InDetByteStreamErrorsAthenaPool )
>>>>>>> release/21.0.127
