import InDetPhysValMonitoring.InDetPhysValDecoration
decoration = InDetPhysValMonitoring.InDetPhysValDecoration.addDecoratorIfNeeded()

# add ID physics validation monitoring tool

<<<<<<< HEAD
from InDetPhysValMonitoring.InDetPhysValJobProperties import InDetPhysValFlags
import InDetPhysValMonitoring.InDetPhysValMonitoringTool as InDetPhysValMonitoringTool

mons=[ (True                                             , InDetPhysValMonitoringTool.getInDetPhysValMonitoringTool),
       (InDetPhysValFlags.doValidateLargeD0Tracks()      , InDetPhysValMonitoringTool.getInDetLargeD0PhysValMonitoringTool),
       (InDetPhysValFlags.doValidateLooseTracks()        , InDetPhysValMonitoringTool.getInDetPhysValMonitoringToolLoose),
       (InDetPhysValFlags.doValidateTightPrimaryTracks() , InDetPhysValMonitoringTool.getInDetPhysValMonitoringToolTightPrimary),
       (InDetPhysValFlags.doValidateDBMTracks()          , InDetPhysValMonitoringTool.getInDetPhysValMonitoringToolDBM),
       (InDetPhysValFlags.doValidateGSFTracks()          , InDetPhysValMonitoringTool.getInDetPhysValMonitoringToolGSF)
     ]

for enabled, creator in mons :
    if enabled :
        monMan.AthenaMonTools += [ creator() ]

from InDetPhysValMonitoring.InDetPhysValMonitoringTool import getInDetPhysValMonitoringTool
from  InDetPhysValMonitoring.InDetPhysValJobProperties import InDetPhysValFlags
from  InDetPhysValMonitoring.ConfigUtils import extractCollectionPrefix
for col in InDetPhysValFlags.validateExtraTrackCollections() :
    prefix=extractCollectionPrefix(col)
    monMan.AthenaMonTools += [ getInDetPhysValMonitoringTool(name                       = 'InDetPhysValMonitoringTool'+prefix,
                                                             SubFolder                  = prefix+'Tracks/',
                                                             TrackParticleContainerName = prefix+'TrackParticles') ]
=======
from InDetPhysValMonitoring.InDetPhysValMonitoringTool import InDetPhysValMonitoringTool
from InDetPhysValMonitoring.InDetPhysValJobProperties import InDetPhysValFlags

indet_mon_tool = InDetPhysValMonitoringTool.InDetPhysValMonitoringTool()
ToolSvc += [indet_mon_tool] 
monMan.AthenaMonTools += [indet_mon_tool] 

if InDetPhysValFlags.doValidateDBMTracks():
    indet_mon_tool_DBM = InDetPhysValMonitoringTool.InDetPhysValMonitoringToolDBM()
    ToolSvc += [ indet_mon_tool_DBM ]
    monMan.AthenaMonTools += [ indet_mon_tool_DBM ] 

if InDetPhysValFlags.doValidateGSFTracks():
    indet_mon_tool_GSF = InDetPhysValMonitoringTool.InDetPhysValMonitoringToolGSF()
    ToolSvc += [ indet_mon_tool_GSF ]
    monMan.AthenaMonTools += [ indet_mon_tool_GSF ]

if InDetPhysValFlags.doValidateLooseTracks():
    indet_mon_tool_Loose = InDetPhysValMonitoringTool.InDetPhysValMonitoringToolLoose()
    ToolSvc += [ indet_mon_tool_Loose ]
    monMan.AthenaMonTools += [ indet_mon_tool_Loose ] 

if InDetPhysValFlags.doValidateTightPrimaryTracks():
    indet_mon_tool_TightPrimary = InDetPhysValMonitoringTool.InDetPhysValMonitoringToolTightPrimary()
    ToolSvc += [ indet_mon_tool_TightPrimary ]
    monMan.AthenaMonTools += [ indet_mon_tool_TightPrimary ] 
>>>>>>> release/21.0.127
