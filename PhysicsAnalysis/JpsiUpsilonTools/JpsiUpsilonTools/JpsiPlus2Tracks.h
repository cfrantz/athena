/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

// ****************************************************************************
// ----------------------------------------------------------------------------
// JpsiPlus2Tracks header file
//
// James Catmore <James.Catmore@cern.ch>

// ----------------------------------------------------------------------------
// ****************************************************************************
#ifndef JPSIPLUS2TRACKS_H
#define JPSIPLUS2TRACKS_H
#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "TrkVKalVrtFitter/TrkVKalVrtFitter.h"
<<<<<<< HEAD
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"
#include "StoreGate/ReadHandleKey.h"
#include <vector>
#include <string>
#include "xAODMuon/MuonContainer.h"
#include "JpsiUpsilonTools/ICandidateSearch.h"
=======
#include "DataModel/DataVector.h"
#include "HepPDT/ParticleDataTable.hh"
#include "xAODTracking/TrackParticle.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODTracking/Vertex.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/VertexAuxContainer.h"

#include <vector>
#include <cmath>
#include <string>
>>>>>>> release/21.0.127
/////////////////////////////////////////////////////////////////////////////

namespace Trk {
    class IVertexFitter;
    class VxTrackAtVertex;
    class ITrackSelectorTool;
}
namespace InDet { class VertexPointEstimator; }

<<<<<<< HEAD
namespace xAOD{
   class BPhysHelper;
}
=======
>>>>>>> release/21.0.127

namespace Analysis {
    
    static const InterfaceID IID_JpsiPlus2Tracks("JpsiPlus2Tracks", 1, 0);
    
<<<<<<< HEAD
    class JpsiPlus2Tracks:  public Analysis::ICandidateSearch, public AthAlgTool
=======
    class JpsiPlus2Tracks:  virtual public AthAlgTool
>>>>>>> release/21.0.127
    {
    public:
        JpsiPlus2Tracks(const std::string& t, const std::string& n, const IInterface*  p);
        ~JpsiPlus2Tracks();
<<<<<<< HEAD
        virtual StatusCode initialize() override;
=======
        StatusCode initialize();
        StatusCode finalize();
>>>>>>> release/21.0.127
        
        static const InterfaceID& interfaceID() { return IID_JpsiPlus2Tracks;};
        
        //-------------------------------------------------------------------------------------
        //Doing Calculation and inline functions
<<<<<<< HEAD
        virtual StatusCode performSearch(xAOD::VertexContainer*& , xAOD::VertexAuxContainer*& ) const override;

        static double getInvariantMass(const xAOD::TrackParticle*, double, const xAOD::TrackParticle*, double);
        static double getInvariantMass(const std::vector<const xAOD::TrackParticle*> &trk, const std::vector<double>&);
        static bool   oppositeCharges(const xAOD::TrackParticle*, const xAOD::TrackParticle*);

        bool  passCuts(xAOD::BPhysHelper &bHelper, const std::vector<double> &masses, const std::string &str) const;
        bool  vertexCuts(xAOD::BPhysHelper &bHelper) const;
        xAOD::Vertex* fit(const std::vector<const xAOD::TrackParticle*>&,
                          const xAOD::TrackParticleContainer*, const xAOD::Vertex* pv, const xAOD::TrackParticleContainer* GSL) const;
        //-------------------------------------------------------------------------------------
        
    private:
        bool m_pipiMassHyp;
        bool m_kkMassHyp;
        bool m_kpiMassHyp;
        bool m_kpMassHyp;
        bool m_oppChargesOnly;
        bool m_sameChargesOnly;
=======
        StatusCode performSearch(xAOD::VertexContainer*& , xAOD::VertexAuxContainer*& );
        static double getPt(const xAOD::TrackParticle*, const xAOD::TrackParticle*);
        static double getPt(const xAOD::TrackParticle*, const xAOD::TrackParticle*, const xAOD::TrackParticle*, const xAOD::TrackParticle*);
        static double getInvariantMass(const xAOD::TrackParticle*, double, const xAOD::TrackParticle*, double);
        static double getInvariantMass(const xAOD::TrackParticle*, double, const xAOD::TrackParticle*, double,
                                const xAOD::TrackParticle*, double, const xAOD::TrackParticle*, double);
        static bool   oppositeCharges(const xAOD::TrackParticle*, const xAOD::TrackParticle*);
        static bool   isContainedIn(const xAOD::TrackParticle*, std::vector<const xAOD::TrackParticle*>);
        static bool   isContainedIn(const xAOD::TrackParticle*, const xAOD::MuonContainer*);
        xAOD::Vertex* fit(const std::vector<const xAOD::TrackParticle*>&,
                          bool, double, const xAOD::TrackParticleContainer*);
        //-------------------------------------------------------------------------------------
        
    private:
        const HepPDT::ParticleDataTable *m_particleDataTable;
        bool m_pipiMassHyp;
        bool m_kkMassHyp;
        bool m_kpiMassHyp;
>>>>>>> release/21.0.127
        double m_trkThresholdPt;
        double m_trkMaxEta;
        double m_BThresholdPt;
        double m_BMassUpper;
        double m_BMassLower;
<<<<<<< HEAD
        SG::ReadHandleKey<xAOD::VertexContainer> m_jpsiCollectionKey;
        double m_jpsiMassUpper;
        double m_jpsiMassLower;
        SG::ReadHandleKey<xAOD::TrackParticleContainer> m_TrkParticleCollection;
        SG::ReadHandleKey<xAOD::TrackParticleContainer> m_TrkParticleGSFCollection;
        SG::ReadHandleKey<xAOD::MuonContainer> m_MuonsUsedInJpsi;
        bool m_excludeJpsiMuonsOnly; //Add by Matt Klein
        bool m_excludeCrossJpsiTracks; //Added by Matteo Bedognetti
=======
        std::string m_jpsiCollectionKey;
        double m_jpsiMassUpper;
        double m_jpsiMassLower;
        std::string m_TrkParticleCollection;
        std::string m_MuonsUsedInJpsi;
	bool m_excludeCrossJpsiTracks; //Added by Matteo Bedognetti
        std::vector<xAOD::Vertex*> m_vxc;
>>>>>>> release/21.0.127
        ToolHandle < Trk::IVertexFitter > m_iVertexFitter;
        ToolHandle < Trk::ITrackSelectorTool > m_trkSelector;
        ToolHandle < InDet::VertexPointEstimator > m_vertexEstimator;
        Trk::TrkVKalVrtFitter* m_VKVFitter;
        bool m_useMassConst;
        double m_altMassConst;
        double m_diTrackMassUpper;
        double m_diTrackMassLower;
        
        // additional cuts (Daniel Scheirich)
        double m_chi2cut;                 // chi2/Ndof of the final veretx
        double m_diTrackPt;               // pT of the hadronic track pair before fit
        double m_trkQuadrupletMassUpper;  // invariant mass of all the 4 tracks before fit
        double m_trkQuadrupletMassLower;
        double m_trkQuadrupletPt;         // combined pT of all 4 tracks  before fit
        double m_finalDiTrackMassUpper;   // inveriant mass of the hadronic track pair after the fit
        double m_finalDiTrackMassLower;
        double m_finalDiTrackPt;          // pT of the hadronic track after fit
        double m_trkDeltaZ;               // DeltaZ between the JPsi vertex and hadronic tracks Z0
        // (to reduce the pileup contribution before vertexing)
<<<<<<< HEAD
        std::vector<double> m_manualMassHypo;
        int m_requiredNMuons;
        // fit with PV
        bool m_vertexFittingWithPV;
        SG::ReadHandleKey<xAOD::VertexContainer> m_PVerticesCollection;
        std::vector<double> m_altMassMuonTracks;
        std::vector<double>  m_mumukkMasses;
        std::vector<double>  m_mumupipiMasses;
        std::vector<double>  m_mumukpiMasses;
        std::vector<double>  m_mumupikMasses;
        std::vector<double>  m_mumukpMasses;
        std::vector<double>  m_mumupkMasses;
        std::vector<int>     m_useGSFTrackIndices;
        std::bitset<4>       m_useGSFTrack;

=======
        
>>>>>>> release/21.0.127
    };
} // end of namespace
#endif
