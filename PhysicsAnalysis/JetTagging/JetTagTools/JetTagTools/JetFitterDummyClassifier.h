/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#ifndef BTAGTOOL_JETFITTERDUMMYCLASSIFIER_C
#define BTAGTOOL_JETFITTERDUMMYCLASSIFIER_C

/******************************************************
    @class JetFitterDummyClassifier
     Package : JetTagTools
     Created : March 2007

     DESCRIPTION: filler for JetFitter classifier, does nothing

********************************************************/

#include "AthenaBaseComps/AthAlgTool.h"
#include "GaudiKernel/ToolHandle.h"

#include "JetTagTools/IJetFitterClassifierTool.h"

<<<<<<< HEAD
#include "xAODBTagging/ftagfloat_t.h"

namespace Analysis {


  class IJetFitterTagInfo;

  class JetFitterDummyClassifier : public extends<AthAlgTool, IJetFitterClassifierTool>
  {
  public:
    using base_class::base_class;

    virtual StatusCode initialize() override;
    virtual StatusCode finalize() override;

    // IJetFitterClassifier interface
    virtual
=======
namespace Analysis {


static const InterfaceID IID_JetFitterDummyClassifier("Analysis::JetFitterDummyClassifier", 1, 0);

  class IJetFitterTagInfo;

  class JetFitterDummyClassifier : public AthAlgTool, public IJetFitterClassifierTool {

  public:

    /** AlgTool interface methods */
    static const InterfaceID& interfaceID() { return IID_JetFitterDummyClassifier; };

    JetFitterDummyClassifier(const std::string& name,
			const std::string& n, const IInterface* p);
    ~JetFitterDummyClassifier();

    virtual StatusCode initialize();
    virtual StatusCode finalize();

    // IJetFitterClassifier interface
>>>>>>> release/21.0.127
    StatusCode fillLikelihoodValues(xAOD::BTagging* BTag,
                                    const std::string & jetauthor,
                                    const std::string& inputbasename,
                                    const std::string& outputbasename,
<<<<<<< HEAD
                                    ftagfloat_t jetpT,
                                    ftagfloat_t jeteta,
                                    ftagfloat_t IP3dlike=-5000) const override;
=======
                                    double jetpT,
                                    double jeteta,
                                    double IP3dlike=-5000);


  private:
>>>>>>> release/21.0.127
  };

}//end Analysis namespace

#endif
