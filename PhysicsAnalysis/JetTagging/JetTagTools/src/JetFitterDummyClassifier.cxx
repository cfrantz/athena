/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name    : JetFitterDummyClassifier.h
/// Package : BTagTools
/// Author  : Dan Guest
/// Created : Jan 2017
///
/// DESCRIPTION: filler for JetFitter classifier, does nothing
///
///
///////////////////////////////////////////////////////////////////////////////////////////////////////
#include <string>
#include "GaudiKernel/ITHistSvc.h"


#include "JetTagTools/IJetFitterClassifierTool.h"
#include "JetTagTools/JetFitterDummyClassifier.h"

namespace Analysis {


<<<<<<< HEAD
  StatusCode JetFitterDummyClassifier::initialize() {
    ATH_MSG_DEBUG(" Initialization of JetFitterDummyClassifier succesfull");
=======
  JetFitterDummyClassifier::JetFitterDummyClassifier(const std::string& name,
                                                     const std::string& n,
                                                     const IInterface* p):
    AthAlgTool(name, n,p)
  {
    declareInterface<IJetFitterClassifierTool>(this);
  }

/////////////////////////////////////////////////////////////////////////////////////
/// Destructor - check up memory allocation
/// delete any memory allocation on the heap

  JetFitterDummyClassifier::~JetFitterDummyClassifier() {
  }

  StatusCode JetFitterDummyClassifier::initialize() {
    ATH_MSG_INFO(" Initialization of JetFitterDummyClassifier succesfull");
>>>>>>> release/21.0.127
    return StatusCode::SUCCESS;
  }

  StatusCode JetFitterDummyClassifier::finalize() {
<<<<<<< HEAD
    ATH_MSG_DEBUG(" Finalization of JetFitterDummyClassifier succesfull");
=======
    ATH_MSG_INFO(" Finalization of JetFitterDummyClassifier succesfull");
>>>>>>> release/21.0.127
    return StatusCode::SUCCESS;
  }


  StatusCode JetFitterDummyClassifier
  ::fillLikelihoodValues(xAOD::BTagging* /*BTag*/,
                         const std::string & /*jetauthor*/,
                         const std::string& /*inputbasename*/,
                         const std::string& /*outputbasename*/,
<<<<<<< HEAD
                         ftagfloat_t /* jetpT */,
                         ftagfloat_t /* jeteta */,
                         ftagfloat_t /* IP3dlike=-5000 */) const
  {
    return StatusCode::SUCCESS;
  }


=======
                         double /* jetpT */,
                         double /* jeteta */,
                         double /* IP3dlike=-5000 */) {
    return StatusCode::SUCCESS;

  }



>>>>>>> release/21.0.127
}//end Analysis namespace
