########################################
# BTagging_jobOptions.py
# author: andreas.wildauer@cern.ch
#         devivie@lal.in2p3.fr
#         vacavant@in2p3.fr 
#
# Main jobO for b-tagging:
# - load all the necessary tools
# - configure the b-tagging algorithms
########################################

# <================= IMPORTANT ==============================================>
  # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  # NOTE: The inclusion of the LoadTools is no longer needed with the
  # new configuration; the default configuration is automatically set up
  # for any unconfigured jet collection where which setupJetBTaggerTool
  # is called. In fact the only thing the LoadTools does now is just call
  # this default setup on all jet collections in BTaggingFlags.Jets.
  #
  # If you need to modify the default setup permanently can modify
  # BTaggingConfiguration_LoadTools.py in the ./python/ directory.
  #
  # If you want different settings not obtainable via the BTaggingFlags,
  # you need to use the new configuration scheme before any call to
  # setupJetBTaggerTools is made for the jet collection in question.
  # You can start by calling BTaggingConfiguration_LoadTools.py's
  # Initiate() function.
  # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# <================= IMPORTANT ==============================================>

# <================= IMPORTANT ==============================================>
  # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  # NOTE: Consider adding some of the stuff found here to the Initiate()
  # function in BTaggingConfiguration_LoadTools.py. The code there is
  # run exactly once; if B-tagging is enabled.
  #
  # DOING SO WILL MAKE THE CODE COMPATIBLE WITH A FUTURE CHANGE IN JETREC
  # WHERE THEY WILL RETRIEVE OUR BTAGGING FUNCTION IF REQUESTED BY A USER.
  # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# <================= IMPORTANT ==============================================>

import re

if not BTaggingFlags.DoNotSetupBTagging: # Temporary measure so the JetRec people can test setting this all up from their side.
  #
  # ========== Load and configure everything
  #
  
  from BTagging.BTaggingConfiguration import getConfiguration
  ConfInstance = getConfiguration()

  if ConfInstance.checkFlagsUsingBTaggingFlags():

    #Jet collections
    #JetCollectionList = ['AntiKt4LCTopoJets', 'AntiKt4EMTopoJets', 'AntiKt4TrackJets', 'AntiKt4EMPFlowJets', 'AntiKt2TrackJets']
    JetCollectionList = ['AntiKt4EMTopoJets']
<<<<<<< HEAD
=======
    from JetRec.JetRecFlags import jetFlags
    #if jetFlags.useTruth():
    #  JetCollectionList += [ 'AntiKt10TruthWZJets', 'AntiKt4TruthWZJets' ]

    #WOUTER: Moved these into the BTaggingsFlags.py file.
    #BTaggingFlags.CalibrationChannelAliases += [ "AntiKt4EMTopo->AntiKt4TopoEM" ]
    #BTaggingFlags.CalibrationChannelAliases += [ "AntiKt4LCTopo->AntiKt4LCTopo,AntiKt4TopoEM" ]
    #BTaggingFlags.CalibrationChannelAliases += [ "AntiKt10LCTopo->AntiKt6LCTopo,AntiKt6TopoEM,AntiKt4TopoEM" ]
    #BTaggingFlags.CalibrationChannelAliases += [ "AntiKt10Truth->AntiKt6TopoEM,AntiKt4TopoEM" ]
    #BTaggingFlags.CalibrationChannelAliases += [ "AntiKt10TruthWZ->AntiKt6TopoEM,AntiKt4TopoEM" ]
    #BTaggingFlags.CalibrationChannelAliases += [ "AntiKt4Truth->AntiKt4TopoEM" ]
    #BTaggingFlags.CalibrationChannelAliases += [ "AntiKt4TruthWZ->AntiKt4TopoEM" ]
    #BTaggingFlags.CalibrationChannelAliases += [ "AntiKt4Track->AntiKt4TopoEM" ]
    #BTaggingFlags.CalibrationChannelAliases += [ "AntiKt3Track->AntiKt4TopoEM" ]
>>>>>>> release/21.0.127

    BTaggingFlags.Jets = [ name[:-4] for name in JetCollectionList]

    from AthenaCommon.AlgSequence import AlgSequence
    topSequence = AlgSequence()

    for i, jet in enumerate(JetCollectionList):
<<<<<<< HEAD
          btagger = ConfInstance.setupJetBTaggerAlg(ToolSvc, jet) #The [:-4] is not needed here; this function automatically removes trailing 'jets' or 'Jets'.
=======
        # try: 
#          jet = jet.replace("Track", "PV0Track")
#          jetname = getattr(jtm, jet)
  #          btagger = JetBTaggerTool(AuthorSubString[i].lower(), 
  #                                   BTagTool=myBTagTool, 
  #                                   BTagName = AuthorSubString[i], 
  #                                   BTagTrackAssocTool = myBTagTrackAssociation, 
  #                                   BTagSVName = tmpSVname, 
  #                                   BTagJFVtxName = tmpJFVxname, 
  #                                   BTagSecVertexing=myBTagSecVtx)
  #
  #          ToolSvc += btagger
          btagger = ConfInstance.setupJetBTaggerTool(ToolSvc, jet) #The [:-4] is not needed here; this function automatically removes trailing 'jets' or 'Jets'.
>>>>>>> release/21.0.127
          if btagger is None:
            continue
          topSequence += btagger
          #jet = jet.replace("Track", "PV0Track")
          #jetname = getattr(jtm, jet)
          #jetname.unlock()
          #jetname.JetModifiers += [ btagger ]
          #jetname.lock()
          if BTaggingFlags.OutputLevel < 3:
<<<<<<< HEAD
            printfunc (ConfInstance.getJetCollectionTool(jet[:-4]))
=======
            print ConfInstance.getJetCollectionTool(jet[:-4])
        # except AttributeError as error:
        #   print '#BTAG# --> ' + str(error)
        #   NotInJetToolManager.append(AuthorSubString[i])

    if len(NotInJetToolManager) > 0:
        AuthorSubString = list(set(AuthorSubString) - set(NotInJetToolManager))


    # WOUTER: THESE LISTS ARE NOW FILLED DYNAMICALLY AND GOVERNED BY BTAGGINGFLAGS (see ./python/BTaggingsFlags.py and ./python/BTaggingConfiguration.py (function called RegisterOutputContainersForJetCollection)
    # Both standard and aux container must be listed explicitly.
    # For release 19, the container version must be explicit.
    #BaseName = "xAOD::BTaggingContainer_v1#"
    #BaseAuxName = "xAOD::BTaggingAuxContainer_v1#"
    #AOD list
    #BTaggingFlags.btaggingAODList += [ BaseName + author for author in AuthorSubString]
    #BTaggingFlags.btaggingAODList += [ BaseAuxName + author + 'Aux.' for author in AuthorSubString]
    #ESD list
    #BTaggingFlags.btaggingESDList += [ BaseName + author for author in AuthorSubString]
    #BTaggingFlags.btaggingESDList += [ BaseAuxName + author + 'Aux.' for author in AuthorSubString]

    #AOD list SeCVert
    #BaseNameSecVtx = "xAOD::VertexContainer_v1#"
    #BaseAuxNameSecVtx = "xAOD::VertexAuxContainer_v1#"
    #BTaggingFlags.btaggingAODList += [ BaseNameSecVtx + author + tmpSVname for author in AuthorSubString]
    #BTaggingFlags.btaggingAODList += [ BaseAuxNameSecVtx + author + tmpSVname + 'Aux.-vxTrackAtVertex' for author in AuthorSubString]
    #ESD list
    #BTaggingFlags.btaggingESDList += [ BaseNameSecVtx + author + tmpSVname for author in AuthorSubString]
    #BTaggingFlags.btaggingESDList += [ BaseAuxNameSecVtx + author + tmpSVname + 'Aux.-vxTrackAtVertex' for author in AuthorSubString]

    #AOD list JFSeCVert
    #BaseNameJFSecVtx = "xAOD::BTagVertexContainer_v1#"
    #BaseAuxNameJFSecVtx = "xAOD::BTagVertexAuxContainer_v1#"
    #BTaggingFlags.btaggingAODList += [ BaseNameJFSecVtx + author + tmpJFVxname for author in AuthorSubString]
    #BTaggingFlags.btaggingAODList += [ BaseAuxNameJFSecVtx + author + tmpJFVxname + 'Aux.' for author in AuthorSubString]
    #ESD list
    #BTaggingFlags.btaggingESDList += [ BaseNameJFSecVtx + author + tmpJFVxname for author in AuthorSubString]
    #BTaggingFlags.btaggingESDList += [ BaseAuxNameJFSecVtx + author + tmpJFVxname + 'Aux.' for author in AuthorSubString]
>>>>>>> release/21.0.127

