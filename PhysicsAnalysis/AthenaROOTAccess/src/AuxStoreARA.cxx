/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// $Id$
/**
 * @file AthenaROOTAcccess/src/AuxStoreARA.cxx
 * @author marcin, scott
 * @date May, 2014
 * @brief Aux store implementation to manage dynamic aux variables.
 *
 * This is basically the same as the corresponding class in RootStorageSvc.
 * Duplicated here due to the lack of any suitable common packages
 * with the correct dependencies.
 */


#include "AuxStoreARA.h"
<<<<<<< HEAD
#include "AthContainersRoot/getDynamicAuxID.h"
=======
#include "AthContainersRoot/RootAuxVectorFactory.h"
>>>>>>> release/21.0.127
#include "AthenaROOTAccess/branchSeek.h"
#include "AthContainers/tools/error.h"

#include "AthContainers/AuxTypeRegistry.h"
#include "AthContainers/exceptions.h"

#include "TROOT.h"
#include "TBranch.h"
#include "TClass.h"

#include <iostream>


using namespace AthenaROOTAccess;


AuxStoreARA::AuxStoreARA(SG::IAuxBranches &container, long long entry, bool standalone)
  : SG::AuxStoreRoot (container, entry, standalone)
{
  fillAuxIDs();
}


SG::auxid_t AuxStoreARA::resolveAuxID (SG::IAuxBranches& /*container*/,
                                       const std::type_info* ti,
                                       const std::string& name,
                                       const std::string& elem_type_name,
                                       const std::string& branch_type_name)
{
  if (!ti) {
    SG::AuxTypeRegistry& r = SG::AuxTypeRegistry::instance();
    return r.findAuxID(name);
  }
<<<<<<< HEAD
  return SG::getDynamicAuxID (*ti, name,
                              elem_type_name, branch_type_name,
                              standalone());
 }
=======
  return nullptr;
}


} // Anonymous namespace


AuxStoreARA::AuxStoreARA(IAuxBranches &container, long long entry, bool standalone)
  : SG::AuxStoreInternal (standalone),
    m_entry(entry), m_container(container)
{
   SG::AuxTypeRegistry& r = SG::AuxTypeRegistry::instance();
   for( const auto& attr2branch: m_container.auxBranches() ) {
      const string& attr = attr2branch.first;
      std::string elemen_type_name;
      std::string branch_type_name;
      const std::type_info* ti = getElementType(attr2branch.second,
                                                standalone,
                                                elemen_type_name,
                                                branch_type_name);
      SG::auxid_t auxid;
      if (!ti)
        auxid = r.findAuxID(attr);
      else {
        auxid = r.getAuxID(*ti, attr);

        if (auxid == SG::null_auxid) {
          std::string fac_class_name = "SG::AuxTypeVectorFactory<" +
            elemen_type_name;
          if (fac_class_name[fac_class_name.size()-1] == '>')
            fac_class_name += ' ';
          fac_class_name += '>';
          TClass* fac_class = getClassIfDictionaryExists (fac_class_name);
          if (fac_class)
          {
            TClass* base_class = getClassIfDictionaryExists ("SG::IAuxTypeVectorFactory");
            if (base_class) {
              int offs = fac_class->GetBaseClassOffset (base_class);
              if (offs >= 0) {
                void* fac_vp = fac_class->New();
                if (fac_vp) {
                  SG::IAuxTypeVectorFactory* fac = reinterpret_cast<SG::IAuxTypeVectorFactory*> (reinterpret_cast<unsigned long>(fac_vp) + offs);
                  r.addFactory (*ti, fac);
                  auxid = r.getAuxID(*ti, attr);
                }
              }
            }
          }
        }

        if (auxid == SG::null_auxid) {
          std::string vec_name = branch_type_name;
          if (standalone) {
            vec_name = "std::vector<" + branch_type_name;
            if (vec_name[vec_name.size()-1] == '>')
              vec_name += " ";
            vec_name += ">";
          }
          TClass* vec_class = TClass::GetClass (branch_type_name.c_str());

          if (vec_class) {
            SG::IAuxTypeVectorFactory* fac = new SG::RootAuxVectorFactory (vec_class);
            r.addFactory (*ti, fac);
            auxid = r.getAuxID(*ti, attr);
          }
        }

      }
      // add AuxID to the list
      // May still be null if we don't have a dictionary for the branch.
      if (auxid != SG::null_auxid)
        addAuxID (auxid);
   }

   //lock();
}
>>>>>>> release/21.0.127


AuxStoreARA::~AuxStoreARA()
{
  for (TBranch* br : m_branches)
    br->SetAddress(nullptr);
}


void AuxStoreARA::GetEntry (long long entry)
{
  setEntry (entry);
  for (TBranch* br : m_branches)
    branchSeek (br, entry);
}


bool AuxStoreARA::doReadData (SG::IAuxBranches& container,
                              SG::auxid_t /*auxid*/,
                              TBranch& branch,
                              TClass* cl,
                              void* vector,
                              long long entry)
{
   void* data;

   if (standalone() && !cl)
     data = vector;
   else {
     m_ptrs.push_back (vector);
     data = &m_ptrs.back();
   }

   if( container.readAuxBranch(branch, data, entry).isSuccess() ) {
    m_branches.push_back (&branch);
    return true;
  }

  ATHCONTAINERS_ERROR("AuxStoreAPR::doReadData", std::string("Error reading branch ") + branch.GetName() );
  return false;
}
