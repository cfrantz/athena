/*
<<<<<<< HEAD
  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

///////////////////////////////////////////////////////////////////
// DiTauTrackParticleThinning.h, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#ifndef DERIVATIONFRAMEWORK_DITAUTRACKPARTICLETHINNING_H
#define DERIVATIONFRAMEWORK_DITAUTRACKPARTICLETHINNING_H

#include <string>
<<<<<<< HEAD
#include <atomic>
=======
>>>>>>> release/21.0.127

#include "AthenaBaseComps/AthAlgTool.h"
#include "DerivationFrameworkInterfaces/IThinningTool.h"
#include "GaudiKernel/ToolHandle.h"
#include "DerivationFrameworkInDet/TracksInCone.h"
<<<<<<< HEAD
#include "xAODTau/DiTauJetContainer.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODEgamma/EgammaContainer.h"
#include "StoreGate/ThinningHandleKey.h"
#include "StoreGate/ReadHandleKey.h"

#include "ExpressionEvaluation/ExpressionParserUser.h"

namespace DerivationFramework {

  class DiTauTrackParticleThinning : public extends<ExpressionParserUser<AthAlgTool>, IThinningTool> {
    public: 
      DiTauTrackParticleThinning(const std::string& t, const std::string& n, const IInterface* p);
      virtual ~DiTauTrackParticleThinning();
      virtual StatusCode initialize() override;
      virtual StatusCode finalize() override;
      virtual StatusCode doThinning() const override;

    private:
      mutable std::atomic<unsigned int> m_ntot  {};
      mutable std::atomic<unsigned int> m_npass {};
      StringProperty m_streamName
        { this, "StreamName", "", "Name of the stream being thinned" };
      SG::ReadHandleKey<xAOD::DiTauJetContainer>          m_ditauKey
        { this, "DiTauKey","",""};
      SG::ThinningHandleKey<xAOD::TrackParticleContainer> m_inDetSGKey
        { this, "InDetTrackParticlesKey", "InDetTrackParticles", "" };
      Gaudi::Property<std::string>                        m_selectionString
         { this, "SelectionString", "", ""};
=======

namespace ExpressionParsing {
  class ExpressionParser;
}

class IThinningSvc;

namespace DerivationFramework {

  class DiTauTrackParticleThinning : public AthAlgTool, public IThinningTool {
    public: 
      DiTauTrackParticleThinning(const std::string& t, const std::string& n, const IInterface* p);
      ~DiTauTrackParticleThinning();
      StatusCode initialize();
      StatusCode finalize();
      virtual StatusCode doThinning() const;

    private:
      ServiceHandle<IThinningSvc> m_thinningSvc;
      mutable unsigned int m_ntot, m_npass;
      std::string m_ditauSGKey, m_inDetSGKey, m_selectionString;
      bool m_and;
      ExpressionParsing::ExpressionParser *m_parser;
>>>>>>> release/21.0.127
  }; 
}

#endif // DERIVATIONFRAMEWORK_DITAUTRACKPARTICLETHINNING_H
