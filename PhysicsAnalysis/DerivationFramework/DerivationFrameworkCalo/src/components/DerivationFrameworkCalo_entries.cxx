#include "DerivationFrameworkCalo/CaloClusterThinning.h"
#include "DerivationFrameworkCalo/JetCaloClusterThinning.h"
#include "DerivationFrameworkCalo/CellsInConeThinning.h"
#include "DerivationFrameworkCalo/GainDecorator.h"
#include "DerivationFrameworkCalo/MaxCellDecorator.h"
#include "DerivationFrameworkCalo/ClusterEnergyPerLayerDecorator.h"

using namespace DerivationFramework;
 
<<<<<<< HEAD
DECLARE_COMPONENT( CaloClusterThinning )
DECLARE_COMPONENT( JetCaloClusterThinning )
DECLARE_COMPONENT( CellsInConeThinning )
DECLARE_COMPONENT( GainDecorator )
DECLARE_COMPONENT( MaxCellDecorator )
DECLARE_COMPONENT( ClusterEnergyPerLayerDecorator )

=======
DECLARE_TOOL_FACTORY( CaloClusterThinning )
DECLARE_TOOL_FACTORY( JetCaloClusterThinning )
DECLARE_TOOL_FACTORY( CellsInConeThinning )
DECLARE_TOOL_FACTORY( GainDecorator )
DECLARE_TOOL_FACTORY( MaxCellDecorator )
DECLARE_TOOL_FACTORY( ClusterEnergyPerLayerDecorator )

DECLARE_FACTORY_ENTRIES( DerivationFrameworkCalo ) {
   DECLARE_TOOL( CaloClusterThinning )
   DECLARE_TOOL( JetCaloClusterThinning )
   DECLARE_TOOL( CellsInConeThinning )
   DECLARE_TOOL( GainDecorator )
   DECLARE_TOOL( MaxCellDecorator )
   DECLARE_TOOL( ClusterEnergyPerLayerDecorator )   
}
>>>>>>> release/21.0.127
 

