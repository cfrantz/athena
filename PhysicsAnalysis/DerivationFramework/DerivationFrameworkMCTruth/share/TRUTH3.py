#==============================================================================
# Set up common services and job object.
# This should appear in ALL derivation job options
#==============================================================================
from DerivationFrameworkCore.DerivationFrameworkMaster import *

<<<<<<< HEAD
# Add translator from EVGEN input to xAOD-like truth
# Add all the particle derivation tools
# This sets up its own common kernel and adds the common tools to it
from DerivationFrameworkMCTruth.MCTruthCommon import addStandardTruthContents
addStandardTruthContents()
=======
#====================================================================
# JET/MET
#====================================================================
# Set jet flags
from JetRec.JetRecFlags import jetFlags
jetFlags.useTruth = True
jetFlags.useTracks = False
# Add jet algorithms
from JetRec.JetAlgorithm import addJetRecoToAlgSequence
addJetRecoToAlgSequence(DerivationFrameworkJob,eventShapeTools=None)
from JetRec.JetRecStandard import jtm
from JetRec.JetRecConf import JetAlgorithm
#jetFlags.truthFlavorTags = []

jetFlags.truthFlavorTags = ["BHadronsInitial", "BHadronsFinal", "BQuarksFinal",
                            "CHadronsInitial", "CHadronsFinal", "CQuarksFinal",
                            "TausFinal",
                            "Partons",
                            ]

if dfInputIsEVNT:
  # Standard truth jets
  # To recover jet constituents remove the last modifier.
  akt4 = jtm.addJetFinder("AntiKt4TruthJets", "AntiKt", 0.4, "truth", modifiersin=[jtm.truthpartondr, jtm.partontruthlabel, jtm.removeconstit, jtm.jetdrlabeler, jtm.trackjetdrlabeler], ptmin= 20000)
  #akt4 = jtm.addJetFinder("AntiKt4TruthJets", "AntiKt", 0.4, "truth", modifiersin=[jtm.truthpartondr, jtm.partontruthlabel, jtm.removeconstit, jtm.jetdrlabeler, jtm.trackjetdrlabeler], ptmin= 20000)
  akt4alg = JetAlgorithm("jetalgAntiKt4TruthJets", Tools = [akt4] )
  DerivationFrameworkJob += akt4alg

  # WZ Truth Jets
  akt4wz = jtm.addJetFinder("AntiKt4TruthWZJets",  "AntiKt", 0.4,  "truthwz", ptmin= 20000, modifiersin=[jtm.truthpartondr, jtm.partontruthlabel, jtm.removeconstit])
  akt4wzalg = JetAlgorithm("jetalgAntiKt4TruthWZJets", Tools = [akt4wz] )
  DerivationFrameworkJob += akt4wzalg
  #jtm.addJetFinder("AntiKt6TruthWZJets",  "AntiKt", 0.6,  "truthwz", ptmin= 5000)
  # Other jets
  #akt6  = jtm.addJetFinder("AntiKt6TruthJets", "AntiKt", 0.6, "truth", ptmin= 5000)
  #akt10 = jtm.addJetFinder("AntiKt10TruthJets", "AntiKt", 1.0, "truth", ptmin= 5000)
  #akt10trim = jtm.addJetTrimmer("TrimmedAntiKt10TruthJets", rclus=0.3, ptfrac=0.05, input='AntiKt10TruthJets')

  #Large R jets
  akt10 = jtm.addJetFinder("AntiKt10TruthJets", "AntiKt", 1.0, "truth",ptmin= 100000)
  akt10alg = JetAlgorithm("jetalgAntiKt10TruthJets", Tools = [akt10] )
  DerivationFrameworkJob += akt10alg
  akt10trim = jtm.addJetTrimmer("TrimmedAntiKt10TruthJets", rclus=0.2, ptfrac=0.05, input='AntiKt10TruthJets', modifiersin=[jtm.nsubjettiness, jtm.removeconstit])
  akt10trimalg = JetAlgorithm("jetalgTrimmedAntiKt10TruthJets", Tools = [akt10trim] )
  DerivationFrameworkJob += akt10trimalg

# Add truth-based MET algorithm here
import METReconstruction.METConfig_Truth
from METReconstruction.METRecoFlags import metFlags # not sure if you even need this line
from METReconstruction.METRecoConfig import getMETRecoAlg
metAlg = getMETRecoAlg('METReconstruction')
DerivationFrameworkJob += metAlg

#==============================================================================
# Create the derivation kernel algorithm
#==============================================================================

#Photons with a pT cut
TRUTH3PhotonTool = DerivationFramework__TruthCollectionMaker(name                 = "TRUTH3PhotonTool",
                                                             NewCollectionName       = "Truth3Photons",
                                                             ParticleSelectionString = "(abs(TruthParticles.pdgId) == 22) && (TruthParticles.status == 1) && (TruthParticles.pt > 20.0*GeV)")
# ((TruthParticles.classifierParticleOrigin != 42) || (TruthParticles.pt > 20.0*GeV)))")
ToolSvc += TRUTH3PhotonTool

'''
#from DerivationFrameworkMCTruth.DerivationFrameworkMCTruthConf import DerivationFramework__TruthIsolationTool
TRUTH3PhotonIsolationTool1 = DerivationFramework__TruthIsolationTool(name = "TRUTH3PhotonIsolationTool1",
                                                                  isoParticlesKey = "TruthPhotons",
                                                                  allParticlesKey = "TruthParticles",
                                                                  particleIDsToCalculate = [22],
                                                                  IsolationConeSizes = [0.2],
                                                                  IsolationVarNamePrefix = 'etcone',
                                                                  ChargedParticlesOnly = False
                                                                  )
ToolSvc += TRUTH3PhotonIsolationTool1
TRUTH3PhotonIsolationTool2 = DerivationFramework__TruthIsolationTool(name = "TRUTH3PhotonIsolationTool2",
                                                                  isoParticlesKey = "TruthPhotons",
                                                                  allParticlesKey = "TruthParticles",
                                                                  particleIDsToCalculate = [22],
                                                                  IsolationConeSizes = [0.3],
                                                                  IsolationVarNamePrefix = 'ptcone',
                                                                  ChargedParticlesOnly = True
                                                                  )
ToolSvc += TRUTH3PhotonIsolationTool2
'''

#Now, for partons.
TRUTH3TopTool = DerivationFramework__TruthCollectionMaker(name                   = "TRUTH3TopTool",
                                                          NewCollectionName       = "TruthTop",
                                                          ParticleSelectionString = "(abs(TruthParticles.pdgId) == 6)",
                                                          Do_Compress = True,
                                                          )
ToolSvc += TRUTH3TopTool

TRUTH3BosonTool = DerivationFramework__TruthCollectionMaker(name                   = "TRUTH3BosonTool",
                                                            NewCollectionName       = "TruthBoson",
                                                            ParticleSelectionString = "(abs(TruthParticles.pdgId) == 23 || abs(TruthParticles.pdgId) == 24 || abs(TruthParticles.pdgId) == 25)",
                                                            Do_Compress = True,
                                                            Do_Sherpa= True)
ToolSvc += TRUTH3BosonTool

TRUTH3BSMTool = DerivationFramework__TruthCollectionMaker(name                   = "TRUTH3BSMTool",
                                                          NewCollectionName       = "TruthBSM",
                                                          ParticleSelectionString = "( (31<abs(TruthParticles.pdgId) && abs(TruthParticles.pdgId)<38) || abs(TruthParticles.pdgId)==39 || abs(TruthParticles.pdgId)==41 || abs(TruthParticles.pdgId)==42 || abs(TruthParticles.pdgId)== 7 || abs(TruthParticles.pdgId)== 8 || (1000000<abs(TruthParticles.pdgId) && abs(TruthParticles.pdgId)<1000040) || (2000000<abs(TruthParticles.pdgId) && abs(TruthParticles.pdgId)<2000040) )",
                                                          Do_Compress = True)
ToolSvc += TRUTH3BSMTool
#Taken from the code in DerivationFramework::MenuTruthThinning::isBSM

#Let's save the post-shower HT and MET filter values that will make combining filtered samples easier (adds to the EventInfo)
SUSYGenFilt_MCTruthClassifier = MCTruthClassifier(name = "SUSYGenFilt_MCTruthClassifier",
                                                  ParticleCaloExtensionTool="")
ToolSvc += SUSYGenFilt_MCTruthClassifier
GenFilter = CfgMgr.DerivationFramework__SUSYGenFilterTool(
  "MTandHTGenFilt",
  )
ToolSvc += GenFilter

from DerivationFrameworkCore.DerivationFrameworkCoreConf import DerivationFramework__DerivationKernel
DerivationFrameworkJob += CfgMgr.DerivationFramework__DerivationKernel("TRUTH3Kernel",
                                                                       AugmentationTools = [DFCommonTruthClassificationTool,GenFilter,
                                                                                            DFCommonTruthMuonTool,DFCommonTruthElectronTool,TRUTH3PhotonTool,DFCommonTruthNeutrinoTool,
                                                                                            TRUTH3TopTool,TRUTH3BosonTool,TRUTH3BSMTool,
                                                                                            DFCommonTruthElectronDressingTool, DFCommonTruthMuonDressingTool,
                                                                                            DFCommonTruthElectronIsolationTool1, DFCommonTruthElectronIsolationTool2,
                                                                                            DFCommonTruthMuonIsolationTool1, DFCommonTruthMuonIsolationTool2],
                                                                       ThinningTools = [])
#                                                                       ThinningTools = [TRUTH3PhotonIsolationTool1, TRUTH3PhotonIsolationTool2])
>>>>>>> release/21.0.127

#==============================================================================
# Set up stream
#==============================================================================
streamName = derivationFlags.WriteDAOD_TRUTH3Stream.StreamName
fileName = buildFileName( derivationFlags.WriteDAOD_TRUTH3Stream )
TRUTH3Stream = MSMgr.NewPoolRootStream( streamName, fileName )

#==============================================================================
# Set up slimming content list here
#==============================================================================
from DerivationFrameworkCore.SlimmingHelper import SlimmingHelper
TRUTH3SlimmingHelper = SlimmingHelper("TRUTH3SlimmingHelper")
TRUTH3SlimmingHelper.AppendToDictionary = {'TruthEvents':'xAOD::TruthEventContainer','TruthEventsAux':'xAOD::TruthEventAuxContainer',
                                           'MET_Truth':'xAOD::MissingETContainer','MET_TruthAux':'xAOD::MissingETAuxContainer',
                                           'TruthElectrons':'xAOD::TruthParticleContainer','TruthElectronsAux':'xAOD::TruthParticleAuxContainer',
                                           'TruthMuons':'xAOD::TruthParticleContainer','TruthMuonsAux':'xAOD::TruthParticleAuxContainer',
                                           'TruthPhotons':'xAOD::TruthParticleContainer','TruthPhotonsAux':'xAOD::TruthParticleAuxContainer',
                                           'TruthTaus':'xAOD::TruthParticleContainer','TruthTausAux':'xAOD::TruthParticleAuxContainer',
                                           'TruthNeutrinos':'xAOD::TruthParticleContainer','TruthNeutrinosAux':'xAOD::TruthParticleAuxContainer',
                                           'TruthBSM':'xAOD::TruthParticleContainer','TruthBSMAux':'xAOD::TruthParticleAuxContainer',
                                           'TruthBoson':'xAOD::TruthParticleContainer','TruthBosonAux':'xAOD::TruthParticleAuxContainer',
                                           'TruthBottom':'xAOD::TruthParticleContainer','TruthBottomAux':'xAOD::TruthParticleAuxContainer',
                                           'TruthTop':'xAOD::TruthParticleContainer','TruthTopAux':'xAOD::TruthParticleAuxContainer',
                                           'TruthForwardProtons':'xAOD::TruthParticleContainer','TruthForwardProtonsAux':'xAOD::TruthParticleAuxContainer',
                                           'BornLeptons':'xAOD::TruthParticleContainer','BornLeptonsAux':'xAOD::TruthParticleAuxContainer',
                                           'HardScatterParticles':'xAOD::TruthParticleContainer','HardScatterParticlesAux':'xAOD::TruthParticleAuxContainer',
                                           'HardScatterVertices':'xAOD::TruthVertexContainer','HardScatterVerticesAux':'xAOD::TruthVertexAuxContainer',
                                           'TruthBosonsWithDecayParticles':'xAOD::TruthParticleContainer','TruthBosonsWithDecayParticlesAux':'xAOD::TruthParticleAuxContainer',
                                           'TruthBosonsWithDecayVertices':'xAOD::TruthVertexContainer','TruthBosonsWithDecayVerticesAux':'xAOD::TruthVertexAuxContainer',
                                           'TruthBSMWithDecayParticles':'xAOD::TruthParticleContainer','TruthBSMWithDecayParticlesAux':'xAOD::TruthParticleAuxContainer',
                                           'TruthBSMWithDecayVertices':'xAOD::TruthVertexContainer','TruthBSMWithDecayVerticesAux':'xAOD::TruthVertexAuxContainer',
                                           'AntiKt4TruthDressedWZJets':'xAOD::JetContainer','AntiKt4TruthDressedWZJetsAux':'xAOD::JetAuxContainer',
                                           'AntiKt10TruthSoftDropBeta100Zcut10Jets':'xAOD::JetContainer','AntiKt10TruthSoftDropBeta100Zcut10JetsAux':'xAOD::JetAuxContainer'
                                          }
<<<<<<< HEAD

from DerivationFrameworkMCTruth.MCTruthCommon import addTruth3ContentToSlimmerTool
addTruth3ContentToSlimmerTool(TRUTH3SlimmingHelper)
=======
TRUTH3SlimmingHelper.AllVariables = ["MET_Truth","MET_TruthRegions","TruthElectrons","TruthMuons","Truth3Photons","TruthTaus","TruthNeutrinos","TruthBSM","TruthTop","TruthBoson"]
TRUTH3SlimmingHelper.ExtraVariables = ["AntiKt4TruthJets.GhostCHadronsFinalCount.GhostBHadronsFinalCount.pt.HadronConeExclTruthLabelID.ConeTruthLabelID.PartonTruthLabelID.TruthLabelDeltaR_B.TruthLabelDeltaR_C.TruthLabelDeltaR_T",
                                       "TrimmedAntiKt10TruthJets.pt.Tau1_wta.Tau2_wta.Tau3_wta",
                                       "TruthEvents.Q.XF1.XF2.PDGID1.PDGID2.PDFID1.PDFID2.X1.X2.weights.crossSection"]
>>>>>>> release/21.0.127
TRUTH3SlimmingHelper.AppendContentToStream(TRUTH3Stream)
TRUTH3Stream.AddMetaDataItem( [ "xAOD::TruthMetaDataContainer#TruthMetaData", "xAOD::TruthMetaDataAuxContainer#TruthMetaDataAux." ] )

# Keep the metadata of course!
TRUTH3Stream.AddMetaDataItem( [ "xAOD::TruthMetaDataContainer#TruthMetaData", "xAOD::TruthMetaDataAuxContainer#TruthMetaDataAux." ] )

# Remove the old-style event info. It just duplicates information, and it's quite large in the case of multiple event weights.
# Note that in 21.2, this means we can't read the file in athena. A bit too dangerous given truth analyses that might be of interest.
# In master, this is safe and saves us another 0.75kB/event or so
TRUTH3Stream.RemoveItem('EventInfo#*')

# If we don't have a conditions tag set by now, then assume this job isn't going to have one and kill the conditions service
if len(globalflags.ConditionsTag())==0:
    svcMgr.PoolSvc.ReadCatalog = []
