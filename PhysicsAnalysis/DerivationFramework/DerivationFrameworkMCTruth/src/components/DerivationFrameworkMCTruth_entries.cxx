#include "DerivationFrameworkMCTruth/TruthDressingTool.h"
#include "DerivationFrameworkMCTruth/TruthIsolationTool.h"
#include "DerivationFrameworkMCTruth/MenuTruthThinning.h"
#include "DerivationFrameworkMCTruth/GenericTruthThinning.h"
#include "DerivationFrameworkMCTruth/TruthCollectionMaker.h"
#include "DerivationFrameworkMCTruth/TruthCollectionMakerTau.h"
#include "DerivationFrameworkMCTruth/TruthClassificationDecorator.h"
#include "DerivationFrameworkMCTruth/CompactHardTruth.h"
#include "DerivationFrameworkMCTruth/HardTruthThinning.h"
#include "DerivationFrameworkMCTruth/HadronOriginDecorator.h"
#include "DerivationFrameworkMCTruth/HadronOriginClassifier.h"
#include "DerivationFrameworkMCTruth/TruthDecayCollectionMaker.h"
#include "src/TruthNavigationDecorator.h"
#include "DerivationFrameworkMCTruth/TruthD2Decorator.h"
#include "DerivationFrameworkMCTruth/TruthQGDecorationTool.h"
#include "src/TruthBornLeptonCollectionMaker.h"
#include "src/HardScatterCollectionMaker.h"
#include "src/TruthLinkRepointTool.h"
#include "DerivationFrameworkMCTruth/TruthPVCollectionMaker.h"
#include "src/GenFilterTool.h"
#include "src/TruthEDDecorator.h"
#include "src/TruthMetaDataWriter.h"

using namespace DerivationFramework;

<<<<<<< HEAD
DECLARE_COMPONENT( TruthDressingTool )
DECLARE_COMPONENT( TruthIsolationTool )
DECLARE_COMPONENT( MenuTruthThinning )
DECLARE_COMPONENT( GenericTruthThinning )
DECLARE_COMPONENT( TruthCollectionMaker )
DECLARE_COMPONENT( TruthCollectionMakerTau )
DECLARE_COMPONENT( TruthClassificationDecorator )
DECLARE_COMPONENT( CompactHardTruth )
DECLARE_COMPONENT( HardTruthThinning )
DECLARE_COMPONENT( HadronOriginDecorator )
DECLARE_COMPONENT( HadronOriginClassifier )
DECLARE_COMPONENT( TruthDecayCollectionMaker )
DECLARE_COMPONENT( TruthNavigationDecorator )
DECLARE_COMPONENT( TruthD2Decorator )
DECLARE_COMPONENT( TruthQGDecorationTool )
DECLARE_COMPONENT( TruthBornLeptonCollectionMaker )
DECLARE_COMPONENT( HardScatterCollectionMaker )
DECLARE_COMPONENT( TruthLinkRepointTool )
DECLARE_COMPONENT( TruthPVCollectionMaker )
DECLARE_COMPONENT( GenFilterTool )
DECLARE_COMPONENT( TruthEDDecorator )
DECLARE_COMPONENT( TruthMetaDataWriter )
=======
DECLARE_TOOL_FACTORY( TruthDressingTool )
DECLARE_TOOL_FACTORY( TruthIsolationTool )
DECLARE_TOOL_FACTORY( MenuTruthThinning )
DECLARE_TOOL_FACTORY( GenericTruthThinning )
DECLARE_TOOL_FACTORY( TruthCollectionMaker )
DECLARE_TOOL_FACTORY( TruthCollectionMakerTau )
DECLARE_TOOL_FACTORY( TruthClassificationDecorator )
DECLARE_ALGORITHM_FACTORY( CompactHardTruth )
DECLARE_TOOL_FACTORY( HardTruthThinning )

DECLARE_TOOL_FACTORY( HadronOriginDecorator )
DECLARE_TOOL_FACTORY(  HadronOriginClassifier )


DECLARE_FACTORY_ENTRIES( DerivationFrameworkMCTruth) {
   DECLARE_TOOL( TruthDressingTool )
   DECLARE_TOOL( TruthIsolationTool )
   DECLARE_TOOL( MenuTruthThinning )
   DECLARE_TOOL( GenericTruthThinning )
   DECLARE_TOOL( TruthCollectionMaker )
   DECLARE_TOOL( TruthCollectionMakerTau )
   DECLARE_TOOL( TruthClassificationDecorator )
   DECLARE_ALGORITHM( CompactHardTruth )
   DECLARE_TOOL( HardTruthThinning )
   DECLARE_TOOL( HadronOriginDecorator )
   DECLARE_TOOL(  HadronOriginClassifier )


}

>>>>>>> release/21.0.127
