<<<<<<< HEAD
# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
=======
# $Id: CMakeLists.txt 767045 2016-08-09 11:46:34Z emoyse $
################################################################################
# Package: IsolationSelection
################################################################################
>>>>>>> release/21.0.127

# Declare the package name:
atlas_subdir( IsolationSelection )

<<<<<<< HEAD
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs(
  PUBLIC
  Control/AthContainers
  Control/AthToolSupport/AsgTools
  Event/xAOD/xAODBase
  Event/xAOD/xAODEgamma
  Event/xAOD/xAODMuon
  Event/xAOD/xAODPrimitives
  PhysicsAnalysis/AnalysisCommon/PATCore
  PhysicsAnalysis/AnalysisCommon/PATInterfaces
  InnerDetector/InDetRecTools/InDetTrackSelectionTool
  PRIVATE
  Control/AthenaBaseComps
  GaudiKernel
  Tools/PathResolver )

>>>>>>> release/21.0.127
# External dependencies:
find_package( ROOT COMPONENTS Core Hist Tree TMVA )

set(extra_libs)
if ( NOT XAOD_STANDALONE AND NOT XAOD_ANALYSIS )
  set(extra_libs TrkExUtils)
endif()

# Libraries in the package:
atlas_add_library( IsolationSelectionLib
  IsolationSelection/*.h Root/*.cxx
  PUBLIC_HEADERS IsolationSelection
  PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
<<<<<<< HEAD
  LINK_LIBRARIES AthContainers AsgTools xAODBase xAODEgamma xAODMuon xAODEventInfo ${extra_libs}
  xAODPrimitives PATCoreLib PATInterfaces InDetTrackSelectionToolLib xAODTracking
  ${ROOT_LIBRARIES} 
  PRIVATE_LINK_LIBRARIES PathResolver FourMomUtils TrackVertexAssociationToolLib InDetTrackSelectionToolLib )

if( NOT XAOD_STANDALONE )
   atlas_add_component( IsolationSelection
      src/*.h src/*.cxx src/components/*.cxx
      LINK_LIBRARIES AsgTools AthenaBaseComps xAODBase xAODCore xAODEgamma xAODMuon
      GaudiKernel IsolationSelectionLib )
endif()

atlas_add_dictionary( IsolationSelectionDict
   IsolationSelection/IsolationSelectionDict.h
   IsolationSelection/selection.xml
   LINK_LIBRARIES IsolationSelectionLib )

# Executable(s) in the package:
if( XAOD_STANDALONE OR XAOD_ANALYSIS )
   atlas_add_executable( testIsolationCloseByCorrectionTool
      util/testIsolationCloseByCorrectionTool.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} AsgTools xAODBase xAODRootAccess xAODEventInfo
      xAODEgamma xAODMuon xAODCore IsolationSelectionLib )

   atlas_add_executable( testIsolationSelectionTool
      util/testIsolationSelectionTool.cxx
      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
      LINK_LIBRARIES ${ROOT_LIBRARIES} xAODEgamma xAODMuon
      xAODPrimitives IsolationSelectionLib xAODRootAccess)
endif()

# Test(s) in the package:
if( XAOD_STANDALONE OR XAOD_ANALYSIS )
   atlas_add_test( ut_reflex SCRIPT test/ut_reflex.py )
endif()

# Install files from the package:
atlas_install_joboptions( share/*.py )
=======
  LINK_LIBRARIES AthContainers AsgTools xAODBase xAODEgamma xAODMuon
  xAODPrimitives PATCoreLib PATInterfaces InDetTrackSelectionToolLib
  PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} PathResolver )


atlas_add_component( IsolationSelection
  src/*.h src/*.cxx src/components/*.cxx
  LINK_LIBRARIES AsgTools AthenaBaseComps xAODEgamma xAODMuon GaudiKernel
  IsolationSelectionLib )
>>>>>>> release/21.0.127
