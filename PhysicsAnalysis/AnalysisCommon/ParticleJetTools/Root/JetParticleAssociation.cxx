/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

// author: cpollard@cern.ch

#include "ParticleJetTools/JetParticleAssociation.h"
<<<<<<< HEAD
#include "xAODTracking/TrackParticleContainer.h"
=======
>>>>>>> release/21.0.127

using namespace std;
using namespace xAOD;

JetParticleAssociation::JetParticleAssociation(const string& name)
    : AsgTool(name) {

        declareProperty("jetCollectionName", m_jetCollectionName);
        declareProperty("outputCollectionName", m_outputCollectionName);
<<<<<<< HEAD
        declareProperty("inputParticleCollectionName", m_inputParticleCollectionName);
=======
>>>>>>> release/21.0.127

        return;
    }

StatusCode JetParticleAssociation::initialize() {
<<<<<<< HEAD
    m_dec = new SG::AuxElement::Decorator<vector<ElementLink<IParticleContainer> > >(m_outputCollectionName);
=======
    dec = new SG::AuxElement::Decorator<vector<ElementLink<IParticleContainer> > >(m_outputCollectionName);
>>>>>>> release/21.0.127

    return StatusCode::SUCCESS;
}

StatusCode JetParticleAssociation::execute() {

<<<<<<< HEAD
    const JetContainer* jets = nullptr;
=======
    const JetContainer* jets = NULL;
>>>>>>> release/21.0.127
    if ( evtStore()->retrieve( jets, m_jetCollectionName ).isFailure() ) {
        ATH_MSG_FATAL("JetParticleAssociation: "
                "failed to retrieve jet collection \"" +
                m_jetCollectionName + "\"");
        return StatusCode::FAILURE;
    }

<<<<<<< HEAD
    const IParticleContainer* parts = nullptr;
    if ( evtStore()->retrieve( parts, m_inputParticleCollectionName ).isFailure() ) {
        ATH_MSG_FATAL("JetParticleAssociation: "
                "failed to retrieve particle collection \"" +
                m_inputParticleCollectionName + "\"");
        return StatusCode::FAILURE;
    }

#ifndef GENERATIONBASE
    const vector<vector<ElementLink<IParticleContainer> > >* matches = match(*jets, *parts);
=======
    const vector<vector<ElementLink<IParticleContainer> > >* matches = match(*jets);

>>>>>>> release/21.0.127

    SG::AuxElement::ConstAccessor<vector<ElementLink<TrackParticleContainer> > >
        trkacc("BTagTrackToJetAssociator");

    for (unsigned int iJet = 0; iJet < jets->size(); iJet++)
<<<<<<< HEAD
        (*m_dec)(*jets->at(iJet)) = (*matches)[iJet];

    delete matches;
#endif //if not GENERATIONBASE
=======
        (*dec)(*jets->at(iJet)) = (*matches)[iJet];

    delete matches;
>>>>>>> release/21.0.127

    return StatusCode::SUCCESS;
}


StatusCode JetParticleAssociation::finalize() {
<<<<<<< HEAD
    delete m_dec;
=======
    delete dec;
>>>>>>> release/21.0.127
    return StatusCode::SUCCESS;
}
