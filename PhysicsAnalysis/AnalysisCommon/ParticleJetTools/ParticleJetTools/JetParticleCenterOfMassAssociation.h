/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

// author: jie.yu@cern.ch

#ifndef PARTICLEJETTOOLS_JETPARTICLECENTEROFMASSASSOCIATION_H
#define PARTICLEJETTOOLS_JETPARTICLECENTEROFMASSASSOCIATION_H

#include "ParticleJetTools/JetParticleAssociation.h"

#include <vector>
#include <string>

class JetParticleCenterOfMassAssociation : public JetParticleAssociation {
    ASG_TOOL_CLASS(JetParticleCenterOfMassAssociation, JetParticleAssociation)
    public:

        JetParticleCenterOfMassAssociation(const std::string& name);

<<<<<<< HEAD
        virtual const std::vector<std::vector<ElementLink<xAOD::IParticleContainer> > >*
            match(const xAOD::JetContainer&, const xAOD::IParticleContainer&) const override;
=======
        const std::vector<std::vector<ElementLink<xAOD::IParticleContainer> > >*
            match(const xAOD::JetContainer&) const;
>>>>>>> release/21.0.127

        inline double getAngleSize(const double& par_R) const{ 
            double result = acos(1-par_R*0.5);
            return result;
        }


    private:
<<<<<<< HEAD
=======
        std::string m_inputTrackCollectionName;
>>>>>>> release/21.0.127
        double m_partMatchCone;
        double m_parentJetCone;
};

#endif
