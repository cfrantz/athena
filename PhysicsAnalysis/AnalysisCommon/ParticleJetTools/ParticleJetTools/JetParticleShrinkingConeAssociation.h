/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

// author: cpollard@cern.ch

#ifndef PARTICLEJETTOOLS_JETPARTICLESHRINKINGCONEASSOCIATION_H
#define PARTICLEJETTOOLS_JETPARTICLESHRINKINGCONEASSOCIATION_H

#include "ParticleJetTools/JetParticleAssociation.h"

#include <vector>
#include <string>

class JetParticleShrinkingConeAssociation : public JetParticleAssociation {
    ASG_TOOL_CLASS(JetParticleShrinkingConeAssociation, JetParticleAssociation)
    public:

        JetParticleShrinkingConeAssociation(const std::string& name);

<<<<<<< HEAD
        virtual const std::vector<std::vector<ElementLink<xAOD::IParticleContainer> > >*
            match(const xAOD::JetContainer&, const xAOD::IParticleContainer&) const override;
=======
        const std::vector<std::vector<ElementLink<xAOD::IParticleContainer> > >*
            match(const xAOD::JetContainer&) const;
>>>>>>> release/21.0.127

        inline double coneSize(double pt) const {
            return (m_coneSizeFitPar1 + exp(m_coneSizeFitPar2 + m_coneSizeFitPar3*pt));
        }


    private:
<<<<<<< HEAD
=======
        std::string m_inputParticleCollectionName;
>>>>>>> release/21.0.127
        double m_coneSizeFitPar1;
        double m_coneSizeFitPar2;
        double m_coneSizeFitPar3;
};

#endif
