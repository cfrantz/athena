//#include "ParticleJetTools/JetTrackTruthMatching.h"
#include "ParticleJetTools/JetQuarkLabel.h"
#include "ParticleJetTools/JetConeLabeling.h"
//#include "ParticleJetTools/JetQGPartonLabel.h"
#include "ParticleJetTools/ParticleToJetAssociator.h"
//#include "ParticleJetTools/FindLeptonTruth.h"
#include "ParticleJetTools/CopyFlavorLabelTruthParticles.h"
#include "ParticleJetTools/CopyBosonTopLabelTruthParticles.h"
#include "ParticleJetTools/CopyTruthPartons.h"
#include "ParticleJetTools/JetPartonTruthLabel.h"
#include "ParticleJetTools/CopyTruthJetParticles.h"
#include "ParticleJetTools/ParticleJetDeltaRLabelTool.h"
#include "ParticleJetTools/JetParticleAssociationAlg.h"
<<<<<<< HEAD
#include "ParticleJetTools/ParticleJetGhostLabelTool.h"
#include "ParticleJetTools/JetParticleShrinkingConeAssociation.h"
#include "ParticleJetTools/JetParticleCenterOfMassAssociation.h"
#include "ParticleJetTools/JetTruthLabelingTool.h"
=======
#include "ParticleJetTools/JetParticleShrinkingConeAssociation.h"
#include "ParticleJetTools/JetParticleCenterOfMassAssociation.h"
>>>>>>> release/21.0.127

using namespace Analysis;

/* DECLARE_COMPONENT( Analysis::JetTrackTruthMatching ) */
DECLARE_COMPONENT( Analysis::JetQuarkLabel )
DECLARE_COMPONENT( Analysis::JetConeLabeling )
DECLARE_COMPONENT( Analysis::JetPartonTruthLabel )
/* DECLARE_COMPONENT( Analysis::JetQGPartonLabel ) */
DECLARE_COMPONENT( Analysis::ParticleToJetAssociator )
/* DECLARE_COMPONENT( Analysis::FindLeptonTruth ) */
/// @todo Convert to namespace, tool, etc?
<<<<<<< HEAD
DECLARE_COMPONENT( CopyFlavorLabelTruthParticles )
DECLARE_COMPONENT( CopyBosonTopLabelTruthParticles )
DECLARE_COMPONENT( CopyTruthPartons )
DECLARE_COMPONENT( CopyTruthJetParticles )
DECLARE_COMPONENT( ParticleJetDeltaRLabelTool )
DECLARE_COMPONENT( ParticleJetGhostLabelTool )
DECLARE_COMPONENT( JetParticleShrinkingConeAssociation )
DECLARE_COMPONENT( JetParticleCenterOfMassAssociation )
DECLARE_COMPONENT( JetTruthLabelingTool )
DECLARE_COMPONENT( JetParticleAssociationAlg )

=======
DECLARE_TOOL_FACTORY( CopyFlavorLabelTruthParticles )
DECLARE_TOOL_FACTORY( CopyBosonTopLabelTruthParticles )
DECLARE_TOOL_FACTORY( CopyTruthPartons )
DECLARE_TOOL_FACTORY( CopyTruthJetParticles )
DECLARE_TOOL_FACTORY( ParticleJetDeltaRLabelTool )
DECLARE_TOOL_FACTORY( JetParticleShrinkingConeAssociation )
DECLARE_TOOL_FACTORY( JetParticleCenterOfMassAssociation )
DECLARE_ALGORITHM_FACTORY( JetParticleAssociationAlg )

/** factory entries need to have the name of the package */
DECLARE_FACTORY_ENTRIES( ParticleJetTools ) {
/*    DECLARE_NAMESPACE_TOOL( Analysis, JetTrackTruthMatching ) */
    DECLARE_NAMESPACE_TOOL( Analysis, JetQuarkLabel )
    DECLARE_NAMESPACE_TOOL( Analysis, JetConeLabeling )
    DECLARE_NAMESPACE_TOOL( Analysis, JetPartonTruthLabel )
/*    DECLARE_NAMESPACE_TOOL( Analysis, JetQGPartonLabel ) */
    DECLARE_NAMESPACE_TOOL( Analysis, ParticleToJetAssociator )
/*    DECLARE_NAMESPACE_TOOL( Analysis, FindLeptonTruth ) */
    /// @todo Convert to namespace, tool, etc?
    DECLARE_TOOL( CopyFlavorLabelTruthParticles )
    DECLARE_TOOL( CopyBosonTopLabelTruthParticles )
    DECLARE_TOOL( CopyTruthPartons )
    DECLARE_TOOL( CopyTruthJetParticles )
    DECLARE_TOOL( ParticleJetDeltaRLabelTool )
    DECLARE_TOOL( JetParticleShrinkingConeAssociation )
    DECLARE_TOOL( JetParticleCenterOfMassAssociation )
    DECLARE_ALGORITHM( JetParticleAssociationAlg )
}
>>>>>>> release/21.0.127
