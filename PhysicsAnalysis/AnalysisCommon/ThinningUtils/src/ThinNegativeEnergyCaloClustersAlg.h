///////////////////////// -*- C++ -*- /////////////////////////////

/*
<<<<<<< HEAD
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/


#ifndef THINNINGUTILS_ThinNegativeEnergyCaloClustersAlg_H
#define THINNINGUTILS_ThinNegativeEnergyCaloClustersAlg_H 1

/**
 @class ThinNegativeEnergyCaloClustersAlg
*/


// STL includes
#include <string>

<<<<<<< HEAD
#include "xAODCaloEvent/CaloClusterContainer.h"

=======
>>>>>>> release/21.0.127
// FrameWork includes
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"
#include "AthenaBaseComps/AthAlgorithm.h"
<<<<<<< HEAD
#include "StoreGate/ThinningHandleKey.h"
=======
#include "AthenaKernel/IThinningSvc.h"
>>>>>>> release/21.0.127

class ThinNegativeEnergyCaloClustersAlg
: public ::AthAlgorithm
{
public:
    
    /// Constructor with parameters:
    ThinNegativeEnergyCaloClustersAlg( const std::string& name, ISvcLocator* pSvcLocator );
    
    /// Destructor:
    virtual ~ThinNegativeEnergyCaloClustersAlg();
    
    /// Athena algorithm's initalize hook
    virtual StatusCode  initialize();
    
    /// Athena algorithm's execute hook
    virtual StatusCode  execute();
    
    /// Athena algorithm's finalize hook
    virtual StatusCode  finalize();
    
private:
<<<<<<< HEAD
    StringProperty m_streamName
    { this, "StreamName", "", "Name of the stream for which thinning is being done." };
    
    /// Should the thinning run?
    BooleanProperty m_doThinning
    { this, "ThinNegativeEnergyCaloClusters", true, "Should the thinning of negative energy calo clusters be run?" };
   
    /// Names of the containers to thin
    SG::ThinningHandleKey<xAOD::CaloClusterContainer> m_caloClustersKey
    { this, "CaloClustersKey", "CaloCalTopoClusters", "StoreGate key for the CaloClustersContainer to be thinned" };
=======
    /// Pointer to IThinningSvc
    ServiceHandle<IThinningSvc> m_thinningSvc;
    
    /// Should the thinning run?
    bool m_doThinning;
   
    /// Names of the containers to thin
    std::string m_caloClustersKey;
>>>>>>> release/21.0.127
 
    /// Counters
    unsigned long m_nEventsProcessed;
    unsigned long m_nCaloClustersProcessed;
    unsigned long m_nCaloClustersThinned;

};


#endif //> !THINNINGUTILS_ThinNegativeEnergyCaloClustersAlg_H
