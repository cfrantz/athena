<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

from RecExConfig.Configured import Configured
from AthenaCommon.Logging import logging
=======
# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

from RecExConfig.Configured import Configured
from AthenaCommon.Logging import logging
from AthenaCommon.AthenaCommonFlags  import athenaCommonFlags
>>>>>>> release/21.0.127

class ThinTrkTrack(Configured):    
    def configure(self):

        mlog = logging.getLogger ('TrkTrackPostExecStream.py::configure:')
        mlog.info('entering')

        try:
<<<<<<< HEAD
            from RecExConfig.ObjKeyStore import cfgKeyStore
=======
>>>>>>> release/21.0.127
            from ThinningUtils.ThinningUtilsConf import ThinTrkTrackAlg
            theTrkTrackThinner = ThinTrkTrackAlg(
                "ThinTrkTrackAlg",
                doElectrons=True,
                doPhotons=True,
<<<<<<< HEAD
                doMuons=cfgKeyStore.isInInput ('TrackCollection',
                                               'CombinedMuonTracks'),
=======
                doMuons=True,
>>>>>>> release/21.0.127
                MuonsKey="Muons",
                ElectronsKey="Electrons",
                PhotonsKey="Photons",
                CombinedMuonsTrackKey="CombinedMuonTracks",
<<<<<<< HEAD
                GSFTrackKey="GSFTracks",
                StreamName='StreamAOD')
            
            print (theTrkTrackThinner)
=======
                GSFTrackKey="GSFTracks")                
            
            from AthenaCommon.Constants import VERBOSE, DEBUG, INFO, ERROR           
            #theTrkTrackThinner.OutputLevel=DEBUG            
            print theTrkTrackThinner
>>>>>>> release/21.0.127

        except Exception:
            import traceback
            mlog.error("could not get handle to ThinTrkTrackAlg")
<<<<<<< HEAD
            traceback.print_exc()
=======
            print traceback.format_exc()
>>>>>>> release/21.0.127
            return False 

        mlog.info("now adding to topSequence")
        from AthenaCommon.AlgSequence import AlgSequence
        topSequence = AlgSequence()
        topSequence += theTrkTrackThinner
        mlog.info("Done adding to topSequence")

        return True
