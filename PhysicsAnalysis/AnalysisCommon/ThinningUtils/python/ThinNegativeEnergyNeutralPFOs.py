<<<<<<< HEAD
# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

from __future__ import print_function
=======
# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127

from RecExConfig.Configured import Configured
from AthenaCommon.Logging import logging

class ThinNegativeEnergyNeutralPFOs(Configured):
    
    def configure(self):
        mlog = logging.getLogger ('ThinNegativeEnergyNeutralPFOs.py::configure:')
        mlog.info('entering')
        try:
            from ThinningUtils.ThinningUtilsConf import ThinNegativeEnergyNeutralPFOsAlg
            theNegativeEnergyNeutralPFOsThinner = ThinNegativeEnergyNeutralPFOsAlg(
                "ThinNegativeEnergyNeutralPFOsAlg",
<<<<<<< HEAD
                ThinNegativeEnergyNeutralPFOs = True,
                StreamName = 'StreamAOD'
            )
            from RecExConfig.ObjKeyStore import cfgKeyStore
            if cfgKeyStore.isInInput('xAOD::FlowElementContainer', 'JetETMissNeutralFlowElements',):
                theNegativeEnergyNeutralPFOsThinner.NeutralPFOsFEKey = "JetETMissNeutralFlowElements"
            if cfgKeyStore.isInInput('xAOD::FlowElementContainer', 'JetETMissLCNeutralFlowElements',):
                theNegativeEnergyNeutralPFOsThinner.LCNeutralPFOsFEKey = "JetETMissLCNeutralFlowElements"
            print (theNegativeEnergyNeutralPFOsThinner)

            CHSnPFOsThinAlg = None


            from JetRec.JetRecFlags import jetFlags
            if (jetFlags.useTracks or
                cfgKeyStore.isInInput ('xAOD::PFOContainer',
                                       'CHSNeutralParticleFlowObjects')):
                CHSnPFOsThinAlg = ThinNegativeEnergyNeutralPFOsAlg(
                    "ThinNegativeEnergyCHSNeutralPFOsAlg",
                    NeutralPFOsKey="CHSNeutralParticleFlowObjects",
                    ThinNegativeEnergyNeutralPFOs = True,
                    StreamName = 'StreamAOD'
                    )
                print (CHSnPFOsThinAlg)

        except Exception:
            mlog.error("could not get handle to ThinNegativeEnergyNeutralPFOsAlg")
            import traceback
            traceback.print_exc()
=======
                ThinNegativeEnergyNeutralPFOs = True
            )
            print theNegativeEnergyNeutralPFOsThinner
        except Exception:
            mlog.error("could not get handle to ThinNegativeEnergyNeutralPFOsAlg")
            print traceback.format_exc()
>>>>>>> release/21.0.127
            return False 
        mlog.info("now adding to topSequence")
        from AthenaCommon.AlgSequence import AlgSequence
        topSequence = AlgSequence()
        topSequence += theNegativeEnergyNeutralPFOsThinner
<<<<<<< HEAD
        if CHSnPFOsThinAlg:
            topSequence += CHSnPFOsThinAlg
=======
>>>>>>> release/21.0.127
        return True
