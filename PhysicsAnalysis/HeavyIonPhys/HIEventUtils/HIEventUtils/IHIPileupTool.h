/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

// HIPileupTool.h

<<<<<<< HEAD
#ifndef HIEVENTUTILS_IHIPILEUPTOOL_H
#define HIEVENTUTILS_IHIPILEUPTOOL_H

#include "AsgTools/IAsgTool.h"
#include "xAODForward/ZdcModuleContainer.h"
#include "xAODHIEvent/HIEventShapeContainer.h"

//R.Longo - 12/10/2019 - Replacing PATCore/TAccept (inherited from 21.0 HI-equalization)
#include "PATCore/AcceptInfo.h"
#include "PATCore/AcceptData.h"

namespace HI
{

  class IHIPileupTool : virtual public asg::IAsgTool {
    ASG_TOOL_INTERFACE( HI::IHIPileupTool )

  public:

    virtual const asg::AcceptInfo& getAcceptInfo(const xAOD::HIEventShapeContainer& evShCont, const xAOD::ZdcModuleContainer& ZdcCont) const = 0;
    //Added by R.Longo and S.Tapia on 14-10-2019 to bypass an error in including 21.0 missing commits
    virtual bool   is_pileup(const xAOD::HIEventShapeContainer&, const xAOD::ZdcModuleContainer& ) const = 0;

  };
=======
#ifndef IHIPileupTool_H
#define IHIPileupTool_H

#include "AsgTools/IAsgTool.h"
#include "PATCore/TAccept.h"
#include "xAODForward/ZdcModuleContainer.h"
#include "xAODHIEvent/HIEventShapeContainer.h"


namespace HI

{

class IHIPileupTool : virtual public asg::IAsgTool {
  ASG_TOOL_INTERFACE( HI::IHIPileupTool )

public:

  // Display a message.
  //virtual int talk() const =0;
  virtual const Root::TAccept& getTAccept(const xAOD::HIEventShapeContainer& evShCont, const xAOD::ZdcModuleContainer& ZdcCont) const = 0;
  //virtual const Root::TAccept& accept() const = 0;

};
>>>>>>> release/21.0.127
}
#endif
