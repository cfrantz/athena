/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#ifndef HIEVENTUTILS_HITOWERWEIGHTTOOL_H
#define HIEVENTUTILS_HITOWERWEIGHTTOOL_H

#include "HIEventUtils/IHITowerWeightTool.h"
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef HI_HITOWERWEIGHTTOOL_H
#define HI_HITOWERWEIGHTTOOL_H

>>>>>>> release/21.0.127
#include "AsgTools/IAsgTool.h"
#include "AsgTools/AsgTool.h"
#include <map>

class TH3F;

<<<<<<< HEAD
////////////////////////////////////////////////////////////
// Tool that gets the tower weights for by tower by tower
// difference in response during the HI jet reconstruction.
////////////////////////////////////////////////////////////

class HITowerWeightTool : virtual public asg::AsgTool, virtual public IHITowerWeightTool
{
    ASG_TOOL_CLASS(HITowerWeightTool,IHITowerWeightTool)
  public:
    HITowerWeightTool(const std::string& n);

    virtual ~HITowerWeightTool() {};
    virtual StatusCode initialize() override;
    virtual StatusCode configureEvent() override;
    virtual float getEtaPhiResponse(float eta, float phi) const override;
    virtual float getEtaPhiOffset(float eta, float phi) const override;
    virtual float getWeight(float eta, float phi, int sampling) const override;
    virtual float getWeightEta(float eta, float phi, int sampling) const override;
    virtual float getWeightPhi(float eta, float phi, int sampling) const override;
    virtual float getWeightMag(float eta, float phi, int sampling) const override;

  private:
    bool m_applycorrection;
    std::string m_inputFile;
    std::string m_configDir;
    bool m_init;
    TH3F* m_h3W;
    TH3F* m_h3Eta;
    TH3F* m_h3Phi;
    TH3F* m_h3Mag;
    TH3F* m_h3EtaPhiResponse;
    TH3F* m_h3EtaPhiOffset;
    unsigned int m_runNumber;
    int m_runIndex;
    std::map<unsigned int, int> m_runMap;
=======
class IHITowerWeightTool : virtual public asg::IAsgTool {
  ASG_TOOL_INTERFACE(IHITowerWeightTool)
  
  public:

  virtual ~IHITowerWeightTool() {};
  virtual StatusCode initialize() = 0;
  virtual StatusCode configureEvent() = 0;
  virtual float getEtaPhiResponse(float, float) const = 0;
  virtual float getEtaPhiOffset(float, float) const = 0;
  virtual float getWeight(float, float, int) const = 0;
  virtual float getWeightEta(float, float, int) const = 0;
  virtual float getWeightPhi(float, float, int) const = 0;
  virtual float getWeightMag(float, float, int) const = 0;

  
};

class HITowerWeightTool : virtual public asg::AsgTool, virtual public IHITowerWeightTool
{
  ASG_TOOL_CLASS(HITowerWeightTool,IHITowerWeightTool)
  public:
  HITowerWeightTool(const std::string& n);

  virtual ~HITowerWeightTool() {};
  virtual StatusCode initialize();
  virtual StatusCode configureEvent();
  virtual float getEtaPhiResponse(float eta, float phi) const;
  virtual float getEtaPhiOffset(float eta, float phi) const;
  virtual float getWeight(float eta, float phi, int sampling) const;
  virtual float getWeightEta(float eta, float phi, int sampling) const;
  virtual float getWeightPhi(float eta, float phi, int sampling) const;
  virtual float getWeightMag(float eta, float phi, int sampling) const;
  
private:
  std::string m_input_file;
  std::string m_config_dir;
  bool m_init;
  TH3F* m_h3_w;
  TH3F* m_h3_eta;
  TH3F* m_h3_phi;  
  TH3F* m_h3_mag;
  TH3F* m_h3_eta_phi_response;
  TH3F* m_h3_eta_phi_offset;
  unsigned int m_run_number;
  int m_run_index;
  std::map<unsigned int, int> m_run_map;
>>>>>>> release/21.0.127


};


#endif
