/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HIEventUtils/HITowerWeightTool.h"
#include "PathResolver/PathResolver.h"
#include "xAODEventInfo/EventInfo.h"
#include <TH3F.h>
#include <TFile.h>
#include <sstream>
#include <iostream>
#include <iomanip>

HITowerWeightTool::HITowerWeightTool(const std::string& n) : asg::AsgTool(n),
							     m_init(false),
<<<<<<< HEAD
							     m_h3W(nullptr),
							     m_h3Eta(nullptr),
							     m_h3Phi(nullptr),
							     m_h3Mag(nullptr),
							     m_h3EtaPhiResponse(nullptr),
							     m_h3EtaPhiOffset(nullptr),
							     m_runNumber(0),
							     m_runIndex(0)

{
  declareProperty("ApplyCorrection",m_applycorrection=true,"If false unit weigts are applied");
  declareProperty("InputFile",m_inputFile="cluster.geo.HIJING_2018.root","File containing cluster geometric moments.");
  declareProperty("ConfigDir",m_configDir="HIJetCorrection/","Directory containing configuration file.");
=======
							     m_h3_w(nullptr),
							     m_h3_eta(nullptr),
							     m_h3_phi(nullptr),
							     m_h3_mag(nullptr),
							     m_h3_eta_phi_response(nullptr),
							     m_h3_eta_phi_offset(nullptr),
							     m_run_number(0),
							     m_run_index(0)

{
  declareProperty("InputFile",m_input_file="cluster.geo.root","File containing cluster geometric moments.");
  declareProperty("ConfigDir",m_config_dir="HIEventUtils/","Directory containing configuration file.");
  if(initialize().isFailure()) ATH_MSG_WARNING("Initial configuration of tool failed");

>>>>>>> release/21.0.127
}


float HITowerWeightTool::getWeight(float eta, float phi, int sample) const
{
<<<<<<< HEAD
  return m_h3W->GetBinContent(m_h3W->FindBin(eta,phi,sample));
}
float HITowerWeightTool::getWeightEta(float eta, float phi, int sample) const
{
  return m_h3Eta->GetBinContent(m_h3Eta->FindBin(eta,phi,sample));
}
float HITowerWeightTool::getWeightPhi(float eta, float phi, int sample) const
{
  return m_h3Phi->GetBinContent(m_h3Phi->FindBin(eta,phi,sample));
}
float HITowerWeightTool::getWeightMag(float eta, float phi, int sample) const
{
  return m_h3Mag->GetBinContent(m_h3Mag->FindBin(eta,phi,sample));
=======
  return m_h3_w->GetBinContent(m_h3_w->FindBin(eta,phi,sample));
}
float HITowerWeightTool::getWeightEta(float eta, float phi, int sample) const
{
  return m_h3_eta->GetBinContent(m_h3_eta->FindBin(eta,phi,sample));
}
float HITowerWeightTool::getWeightPhi(float eta, float phi, int sample) const
{
  return m_h3_phi->GetBinContent(m_h3_phi->FindBin(eta,phi,sample));
}
float HITowerWeightTool::getWeightMag(float eta, float phi, int sample) const
{
  return m_h3_mag->GetBinContent(m_h3_mag->FindBin(eta,phi,sample));
>>>>>>> release/21.0.127
}

float HITowerWeightTool::getEtaPhiResponse(float eta, float phi) const
{
<<<<<<< HEAD
  if(m_runIndex==0)  return 1;

  int eb=m_h3EtaPhiResponse->GetXaxis()->FindBin(eta);
  int pb=m_h3EtaPhiResponse->GetYaxis()->FindBin(phi);
  float rv=m_h3EtaPhiResponse->GetBinContent(eb,pb,m_runIndex);
=======
  if(m_run_index==0)  return 1;

  int eb=m_h3_eta_phi_response->GetXaxis()->FindBin(eta);
  int pb=m_h3_eta_phi_response->GetYaxis()->FindBin(phi);
  float rv=m_h3_eta_phi_response->GetBinContent(eb,pb,m_run_index);
>>>>>>> release/21.0.127
  return rv;
}

float HITowerWeightTool::getEtaPhiOffset(float eta, float phi) const
{
<<<<<<< HEAD
  if(m_runIndex==0) return 0;
  int eb=m_h3EtaPhiOffset->GetXaxis()->FindBin(eta);
  int pb=m_h3EtaPhiOffset->GetYaxis()->FindBin(phi);
  return m_h3EtaPhiOffset->GetBinContent(eb,pb,m_runIndex)*std::cosh(eta);
=======
  if(m_run_index==0) return 0;
  int eb=m_h3_eta_phi_offset->GetXaxis()->FindBin(eta);
  int pb=m_h3_eta_phi_offset->GetYaxis()->FindBin(phi);
  return m_h3_eta_phi_offset->GetBinContent(eb,pb,m_run_index)*std::cosh(eta);
>>>>>>> release/21.0.127
}

StatusCode HITowerWeightTool::configureEvent()
{
<<<<<<< HEAD
  if (!m_applycorrection){
    m_runIndex=0;
    ATH_MSG_DEBUG("Using unit weights and doing no eta-phi correction.");
    return StatusCode::SUCCESS;
  }

=======
>>>>>>> release/21.0.127
  const xAOD::EventInfo* ei=nullptr;
  if(evtStore()->retrieve(ei,"EventInfo").isFailure())
  {
    ATH_MSG_ERROR("Could not retrieve EventInfo");
    return StatusCode::FAILURE;
  }
  unsigned int run_number=ei->runNumber();
<<<<<<< HEAD

  if(m_runNumber!=run_number)
  {
    auto itr=m_runMap.find(run_number);
    if(itr==m_runMap.end())
    {
			//trying generic run number <=> no run dependence
		        run_number = 226000;
		        auto itrg=m_runMap.find(run_number);
		        if(itrg==m_runMap.end()){
		            m_runIndex=0;
		            ATH_MSG_WARNING("No generic calibration or calibration for " << run_number << " is avaliable. Doing no eta-phi correction.");
		        }
		        else {
		            m_runIndex=itrg->second;
		            ATH_MSG_DEBUG("Using generic calibration for eta-phi correction.");
		        }
    }
    else m_runIndex=itr->second;
    m_runNumber=run_number;
=======
  if(m_run_number!=run_number)
  {
    auto itr=m_run_map.find(run_number);
    if(itr==m_run_map.end())
    {
      m_run_index=0;
      ATH_MSG_WARNING("No calibration avaliable for " << run_number << ". Doing no eta-phi correction.");
    }
    else m_run_index=itr->second;
    m_run_number=run_number;
>>>>>>> release/21.0.127
  }
  return StatusCode::SUCCESS;
}

StatusCode HITowerWeightTool::initialize()
{
  if(m_init) return StatusCode::SUCCESS;
<<<<<<< HEAD
  std::string local_path=m_configDir+m_inputFile;
  std::string full_path=PathResolverFindCalibFile(local_path);
  ATH_MSG_INFO("Reading input file "<< m_inputFile << " from " << full_path);
  TFile* f=TFile::Open(full_path.c_str());
  if(f==nullptr)
  {
    ATH_MSG_FATAL("Cannot read config file " << full_path );
    return StatusCode::FAILURE;
  }

  m_h3W=(TH3F*)f->GetObjectChecked("h3_w","TH3F");
  if(m_h3W==nullptr)
  {
    ATH_MSG_FATAL("Cannot find TH3F m_h3W in config file " << full_path );
    return StatusCode::FAILURE;
  }

  m_h3Eta=(TH3F*)f->GetObjectChecked("h3_eta","TH3F");
  if(m_h3Eta==nullptr)
  {
    ATH_MSG_FATAL("Cannot find TH3F m_h3Eta in config file " << full_path );
    return StatusCode::FAILURE;
  }

  m_h3Phi=(TH3F*)f->GetObjectChecked("h3_phi","TH3F");
  if(m_h3Phi==nullptr)
  {
    ATH_MSG_FATAL("Cannot find TH3F m_h3Phi in config file " << full_path );
    return StatusCode::FAILURE;
  }

  m_h3Mag=(TH3F*)f->GetObjectChecked("h3_R","TH3F");
  if(m_h3Mag==nullptr)
  {
    ATH_MSG_FATAL("Cannot find TH3F m_h3Mag in config file " << full_path );
    return StatusCode::FAILURE;
  }

  m_h3W->SetDirectory(0);
  m_h3Eta->SetDirectory(0);
  m_h3Phi->SetDirectory(0);
  m_h3Mag->SetDirectory(0);

  m_h3EtaPhiResponse=(TH3F*)f->GetObjectChecked("h3_eta_phi_response","TH3F");
  if(m_h3EtaPhiResponse==nullptr)
  {
    ATH_MSG_FATAL("Cannot find TH3F h3_eta_phi_response in config file " << full_path );
    return StatusCode::FAILURE;
  }
  m_h3EtaPhiResponse->SetDirectory(0);
  m_h3EtaPhiOffset=(TH3F*)f->GetObjectChecked("h3_eta_phi_offset","TH3F");
  if(m_h3EtaPhiOffset==nullptr)
  {
    ATH_MSG_FATAL("Cannot find TH3F h3_eta_phi_offset in config file " << full_path );
    return StatusCode::FAILURE;
  }
  m_h3EtaPhiOffset->SetDirectory(0);

  TH1I* h1_run_index=(TH1I*)f->GetObjectChecked("h1_run_index","TH1I");
  if(h1_run_index==nullptr)
  {
    ATH_MSG_FATAL("Cannot find TH3F h1_run_index in config file " << full_path );
    return StatusCode::FAILURE;
  }
  for(int xbin=1; xbin<=h1_run_index->GetNbinsX(); xbin++) {
		m_runMap.emplace_hint(m_runMap.end(),std::make_pair(h1_run_index->GetBinContent(xbin),xbin));
	}
=======
  std::string local_path=m_config_dir+m_input_file;
  std::string full_path=PathResolverFindCalibFile(local_path);
  TFile* f=TFile::Open(full_path.c_str());
  if(f==nullptr) 
  {
    ATH_MSG_FATAL("Cannot read config file " << full_path ); 
    return StatusCode::FAILURE;
  }

  m_h3_w=(TH3F*)f->GetObjectChecked("h3_w","TH3F");
  if(m_h3_w==nullptr) 
  {
    ATH_MSG_FATAL("Cannot find TH3F m_h3_w in config file " << full_path ); 
    return StatusCode::FAILURE;
  }

  m_h3_eta=(TH3F*)f->GetObjectChecked("h3_eta","TH3F");
  if(m_h3_eta==nullptr) 
  {
    ATH_MSG_FATAL("Cannot find TH3F m_h3_eta in config file " << full_path ); 
    return StatusCode::FAILURE;
  }

  m_h3_phi=(TH3F*)f->GetObjectChecked("h3_phi","TH3F");
  if(m_h3_phi==nullptr) 
  {
    ATH_MSG_FATAL("Cannot find TH3F m_h3_phi in config file " << full_path ); 
    return StatusCode::FAILURE;
  }

  m_h3_mag=(TH3F*)f->GetObjectChecked("h3_R","TH3F");
  if(m_h3_mag==nullptr) 
  {
    ATH_MSG_FATAL("Cannot find TH3F m_h3_mag in config file " << full_path ); 
    return StatusCode::FAILURE;
  }

  m_h3_w->SetDirectory(0);
  m_h3_eta->SetDirectory(0);
  m_h3_phi->SetDirectory(0);
  m_h3_mag->SetDirectory(0);

  m_h3_eta_phi_response=(TH3F*)f->GetObjectChecked("h3_eta_phi_response","TH3F");
  if(m_h3_eta_phi_response==nullptr) 
  {
    ATH_MSG_FATAL("Cannot find TH3F h3_eta_phi_response in config file " << full_path ); 
    return StatusCode::FAILURE;
  }
  m_h3_eta_phi_response->SetDirectory(0);
  m_h3_eta_phi_offset=(TH3F*)f->GetObjectChecked("h3_eta_phi_offset","TH3F");
  if(m_h3_eta_phi_offset==nullptr) 
  {
    ATH_MSG_FATAL("Cannot find TH3F h3_eta_phi_offset in config file " << full_path ); 
    return StatusCode::FAILURE;
  }
  m_h3_eta_phi_offset->SetDirectory(0);
  
  TH1I* h1_run_index=(TH1I*)f->GetObjectChecked("h1_run_index","TH1I");
  if(h1_run_index==nullptr) 
  {
    ATH_MSG_FATAL("Cannot find TH3F h1_run_index in config file " << full_path ); 
    return StatusCode::FAILURE;
  }
  for(int xbin=1; xbin<h1_run_index->GetNbinsX(); xbin++) m_run_map.emplace_hint(m_run_map.end(),std::make_pair(h1_run_index->GetBinContent(xbin),xbin));
>>>>>>> release/21.0.127
  f->Close();
  m_init=true;
  return StatusCode::SUCCESS;

}
