/*
<<<<<<< HEAD
 Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
 Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
 */

/*
 * MuonEfficiencyScaleFactors.h
 *
 *  Created on: Apr 9, 2014
 *      Author: goblirsc
>>>>>>> release/21.0.127
 */

#ifndef MUONEFFICIENCYSCALEFACTORS_H_
#define MUONEFFICIENCYSCALEFACTORS_H_

<<<<<<< HEAD
#include "MuonAnalysisInterfaces/IMuonEfficiencyScaleFactors.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyType.h"
#include "MuonEfficiencyCorrections/EfficiencyScaleFactor.h"
#include "MuonEfficiencyCorrections/EffiCollection.h"
=======
#include "MuonEfficiencyCorrections/IMuonEfficiencyScaleFactors.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyType.h"
#include "MuonEfficiencyCorrections/EfficiencyScaleFactor.h"
#include "MuonEfficiencyCorrections/EffiCollection.h"

#include <AsgTools/ToolHandle.h>
#include <AsgTools/AsgTool.h>
>>>>>>> release/21.0.127

#include "AsgTools/ToolHandle.h"
#include "AsgTools/AsgTool.h"
#include "AsgDataHandles/ReadHandleKey.h"
#include "xAODEventInfo/EventInfo.h"

#include <string>
#include <memory>
#include <map>
#include <unordered_map>
namespace CP {
<<<<<<< HEAD
    class MuonEfficiencyScaleFactors: virtual public CP::IMuonEfficiencyScaleFactors, public asg::AsgTool {
=======
    class MuonEfficiencyScaleFactors: public CP::IMuonEfficiencyScaleFactors, public asg::AsgTool {
>>>>>>> release/21.0.127

        public:
            MuonEfficiencyScaleFactors(const std::string& name);

<<<<<<< HEAD
            virtual ~MuonEfficiencyScaleFactors() = default;
            //Proper constructor for Athena
            ASG_TOOL_CLASS3( MuonEfficiencyScaleFactors, CP::IMuonEfficiencyScaleFactors, CP::ISystematicsTool, CP::IReentrantSystematicsTool )
=======
            virtual ~MuonEfficiencyScaleFactors();
            //Proper constructor for Athena
            ASG_TOOL_CLASS2( MuonEfficiencyScaleFactors, CP::IMuonEfficiencyScaleFactors, CP::ISystematicsTool )
>>>>>>> release/21.0.127

            /// initialize the tool once all settings are in place!
            virtual StatusCode initialize();

            /// Retrieve the Scale factor and decorate the muon
            virtual CorrectionCode getEfficiencyScaleFactor(const xAOD::Muon& mu, float& sf, const xAOD::EventInfo* info = 0) const;
            virtual CorrectionCode applyEfficiencyScaleFactor(const xAOD::Muon& mu, const xAOD::EventInfo* info = 0) const;
            /// replica generation
            virtual CorrectionCode getEfficiencyScaleFactorReplicas(const xAOD::Muon& mu, std::vector<float> & sf_err, const xAOD::EventInfo* info = 0) const;
            virtual CorrectionCode applyEfficiencyScaleFactorReplicas(const xAOD::Muon& mu, int nreplicas = 50, const xAOD::EventInfo* info = 0) const;

            /// returns: whether this tool is affected by the given systematis
            virtual bool isAffectedBySystematic(const SystematicVariation& systematic) const;

            /// returns: the list of all systematics this tool can be affected by
            virtual SystematicSet affectingSystematics() const;

            /// returns: the list of all systematics this tool recommends to use
            virtual SystematicSet recommendedSystematics() const;

<<<<<<< HEAD
            virtual StatusCode applySystematicVariation(const SystematicSet& systConfig);
=======
            virtual SystematicCode applySystematicVariation(const SystematicSet& systConfig);
>>>>>>> release/21.0.127

            /// Obtain the muon efficiency measured using the data
            virtual CorrectionCode getDataEfficiency(const xAOD::Muon& mu, float& eff, const xAOD::EventInfo* info = 0) const;
            virtual CorrectionCode applyDataEfficiency(const xAOD::Muon& mu, const xAOD::EventInfo* info = 0) const;

            virtual CorrectionCode getDataEfficiencyReplicas(const xAOD::Muon& mu, std::vector<float> & sf_err, const xAOD::EventInfo* info = 0) const;
            virtual CorrectionCode applyDataEfficiencyReplicas(const xAOD::Muon& mu, int nreplicas = 50, const xAOD::EventInfo* info = 0) const;

            /// Obtain the muon efficiency measured using the MC
            virtual CorrectionCode getMCEfficiency(const xAOD::Muon& mu, float& eff, const xAOD::EventInfo* info = 0) const;
            virtual CorrectionCode applyMCEfficiency(const xAOD::Muon& mu, const xAOD::EventInfo* info = 0) const;

            virtual CorrectionCode getMCEfficiencyReplicas(const xAOD::Muon& mu, std::vector<float> & sf_err, const xAOD::EventInfo* info = 0) const;
            virtual CorrectionCode applyMCEfficiencyReplicas(const xAOD::Muon& mu, int nreplicas = 50, const xAOD::EventInfo* info = 0) const;

<<<<<<< HEAD
            virtual int getUnCorrelatedSystBin(const xAOD::Muon& mu) const;
            virtual std::string getUncorrelatedSysBinName(unsigned int Bin) const;
            virtual std::string getUncorrelatedSysBinName(const SystematicSet& systConfig) const;

=======
            // copy constructor, to make reflex happy...
            MuonEfficiencyScaleFactors(const MuonEfficiencyScaleFactors& tocopy);

            // to make coverity happy
            MuonEfficiencyScaleFactors & operator =(const MuonEfficiencyScaleFactors & tocopy);
>>>>>>> release/21.0.127

        private:
            unsigned int getRandomRunNumber(const xAOD::EventInfo* info) const;
            /// load the SF histos
<<<<<<< HEAD
            StatusCode LoadInputs();

            SG::ReadHandleKey<xAOD::EventInfo> m_eventInfo{this, "EventInfoContName", "EventInfo", "event info key"};
           
            /// Scale-factor files since  Moriond2019 contain the breakdown of systematics into
            /// their individual components. This method loads all systematics and looks their
            /// systematics up and returns them in a map together with a bitmask which files are
            /// affected by the systematic
            std::map<std::string, unsigned int> lookUpSystematics();
        public:
            /// The following methods are meant to propagate information from the central
            /// tool to the subtool managing the individual scale-factor maps to keep their
            /// constructors small in number of arguments. The users do not have to call them.
            
            
            /// Construct the name of the input files from the configuration
            /// Make these methods public such that they can be used by the
            ///  scale-factor managing EffiCollection class without piping
            ///  All arguments to the constructor again
            std::string filename_Central()const;
            /// Reconstruction scale-factors have a dedicated map
            /// for calo-tag muons around |\eta|<0.1. If the scale-factor
            /// is isolation/TTVA then the central file name is returned
            std::string filename_Calo()const;
            /// High-eta reconstruction scale-factors are not obtained by the means of 
            /// are not obtained by the means of tag & probe, but rather by building
            /// the double ratio. The map is delivered in a dedicated file whose path
            /// is returned here
            std::string filename_HighEta()const;
 
            /// Returns the scale-factor maps from a complementary scale-factor
            /// measurement using the J/Psi or Upsilon resonance
            std::string filename_LowPt()const;
            std::string filename_LowPtCalo()const;
            
            /// If the pt of the muon is below that threshold the J/Psi or Upsilon
            /// map is used given that it's available.
            float lowPtTransition() const;
            
            /// Returns the type of the measurement to be carried out... E.g. Reco/TTVA/Iso
            CP::MuonEfficiencyType measurement() const; 
            
            /// The apply<Blah> methods decorate their result directly to the muon. The name of the decorators
            /// can be set by the users themselves using several properties. To avoid that systmatics overwrite
            /// each other and the nominal the final maps are decorating the muon following the logic
            ///     <decoration>__<syst_name>
            
            /// The following methods propagate the basic decoration names to the maps            
            std::string sf_decoration() const;
            std::string data_effi_decoration() const;
            std::string mc_effi_decoration() const;
            
            std::string sf_replica_decoration() const;
            std::string data_effi_replica_decoration() const;
            std::string mc_effi_replica_deocration() const;
            
            /// Returns the position of the collection in the 
            /// syst set vector. If the collection is not part
            /// of this class -1 is returned
            size_t getPosition(const EffiCollection* coll) const;
            
            /// Returns the number of EffiCollections stored in this class
            size_t getNCollections() const;
            /// Returns a boolean whether the uncorrelation of systematics has been switched on
            bool uncorrelate_sys() const;
            
            /// Returns the string telling the tool in which
            /// float AuxElement the information of the separation
            /// to the closest jet is stored
            std::string close_by_jet_decoration() const;
            /// option to set if we want to use 1D or 2D isolation SFs
            bool use_2D_iso_corrections() const;
            
       
    private:
            /// utility method to 'dress' a filename using the path resolver
            std::string resolve_file_location(const std::string &filename)const;
           
           
            /// the working point to operate on
            std::string m_wp;
           
            /// This vector stores all scale-factor maps
            std::vector<std::unique_ptr<EffiCollection>> m_sf_sets;
            
            /// Pointer to the current active map in terms of systematics
            EffiCollection* m_current_sf;
=======
            bool LoadEffiSet(MuonEfficiencySystType sysType);
            bool LoadInputs();

            /// construct the name of the input files from the configuration
            std::string filename_Central();
            std::string filename_Calo();
            std::string filename_HighEta();
            std::string filename_LowPt();
            std::string filename_LowPtCalo();

            // utility method to 'dress' a filename using the path resolver
            std::string resolve_file_location(const std::string &filename);

            //Some util functions
            void CopyInformation(const MuonEfficiencyScaleFactors & tocopy);
            //These are functions needed during initialization
            StatusCode CreateDecorator(SG::AuxElement::Decorator<float>* &Dec, std::string &DecName, const std::string& defaultName);
            StatusCode CreateVecDecorator(SG::AuxElement::Decorator<std::vector<float>>* &Dec, std::string &DecName, const std::string& defaultName);
            StatusCode IsDecoratorNameUnique(std::string &name);
            SystematicSet SetupSystematics(bool doUnfolded = false) const;
            void SetupCheckSystematicSets();
            /// the working point to operate on
            std::string m_wp;
            boost::unordered_map<MuonEfficiencySystType, EffiCollection*> m_sf_sets;

            EffiCollection *m_current_sf;
>>>>>>> release/21.0.127

            std::string m_custom_dir;
            std::string m_custom_file_Combined;
            std::string m_custom_file_Calo;
            std::string m_custom_file_HighEta;
            std::string m_custom_file_LowPt;
            std::string m_custom_file_LowPtCalo;
<<<<<<< HEAD
   
            // Decorate the data-monteCarlo & scale-factors
            // to the muon. Decoration names can be set flexile
            std::string m_efficiency_decoration_name_data;
            std::string m_efficiency_decoration_name_mc;
            std::string m_sf_decoration_name;
      
            /// subfolder to load from the calibration db
            std::string m_calibration_version;
            

            /// threshold below which low-pt SF (i.e. from JPsi) should be used
            float m_lowpt_threshold;
            /// Name of the decoration to catch up the close by jets
            std::string m_iso_jet_dR;
            bool m_use2DIsoCorr;
            CP::SystematicSet m_affectingSys;
            /// It turned out that the code spends a large time in the look up of
            /// the systematics. This map tries to mitigate this issue.
            std::unordered_map<CP::SystematicSet, EffiCollection*> m_filtered_sys_sets;
   
            bool m_init;
            bool m_seperateSystBins;
            bool m_breakDownSyst;
            bool m_applyKineDepSys;
=======

            // info to apply to the muon when in audit mode
            std::string m_version_string;
            std::string m_sys_string;

            std::map<CP::SystematicSet, CP::SystematicSet> m_filtered_sys_sets;

            std::string m_efficiency_decoration_name_data;
            std::string m_efficiency_decoration_name_mc;
            std::string m_sf_decoration_name;
            std::string m_sf_replica_decoration_name;
            std::string m_eff_replica_decoration_name;
            std::string m_mc_eff_replica_decoration_name;

            // subfolder to load from the calibration db
            std::string m_calibration_version;

            // threshold below which low-pt SF (i.e. from JPsi) should be used
            double m_lowpt_threshold;
            // decorators to quickly apply the eff and SF
            SG::AuxElement::Decorator<float>* m_effDec;
            SG::AuxElement::Decorator<float>* m_MCeffDec;
            SG::AuxElement::Decorator<float>* m_sfDec;

            SG::AuxElement::Decorator<std::vector<float> >* m_sfrDec;
            SG::AuxElement::Decorator<std::vector<float> >* m_effrDec;
            SG::AuxElement::Decorator<std::vector<float> >* m_MCeffrDec;

            CP::SystematicSet m_affectingSys;

            // need CP::SystematicSets as members to retrieve MuonEfficiencySystType each event
            CP::SystematicSet* m_Sys1Down;
            CP::SystematicSet* m_Sys1Up;
            CP::SystematicSet* m_Stat1Down;
            CP::SystematicSet* m_Stat1Up;
            CP::SystematicSet* m_LowPtSys1Down;
            CP::SystematicSet* m_LowPtSys1Up;
            CP::SystematicSet* m_LowPtStat1Down;
            CP::SystematicSet* m_LowPtStat1Up;

            bool m_init;
            bool m_seperateSystBins;
>>>>>>> release/21.0.127
            CP::MuonEfficiencyType m_Type;
    };

} /* namespace CP */

#endif /* MUONEFFICIENCYSCALEFACTORS_H_ */
