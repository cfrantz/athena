/*
<<<<<<< HEAD
 Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
 Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
 */

/*
 * EfficiencyScaleFactor.cxx
 *
 *  Created on: Apr 10, 2014
 *      Author: goblirsc
>>>>>>> release/21.0.127
 */

#include <MuonEfficiencyCorrections/EfficiencyScaleFactor.h>
#include <MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h>
#include <MuonEfficiencyCorrections/EffiCollection.h>
#include <TRandom3.h>
#include <TClass.h>
namespace CP {
<<<<<<< HEAD
    unsigned int EfficiencyScaleFactor::m_warningLimit = 10;
    EfficiencyScaleFactor::EfficiencyScaleFactor(std::shared_ptr<EfficiencyScaleFactor> nominal,
                                  const MuonEfficiencyScaleFactors& ref_tool,
                                  const std::string& file,
                                  const std::string& time_unit,
                                  const std::string& syst_name,
                                  int syst_type_bitmap):
            m_measurement(ref_tool.measurement()),
            m_syst_name(syst_name),
            m_is_up(syst_type_bitmap & EffiCollection::UpVariation),
            m_is_lowpt(syst_type_bitmap & EffiCollection::JPsiAnalysis),
            m_respond_to_kineDepSyst(syst_type_bitmap & EffiCollection::PtDependent),
            m_separateBinSyst(syst_type_bitmap & EffiCollection::UnCorrelated),
            m_sf(),
            m_eff(),
            m_mc_eff(),
                        
            m_sf_decor(std::make_unique<FloatDecorator>(ref_tool.sf_decoration()+ (m_syst_name.empty()? "" :"_"+ m_syst_name + (m_is_up? "_1UP": "_1DN")))),
            m_eff_decor(std::make_unique<FloatDecorator>(ref_tool.data_effi_decoration()+ (m_syst_name.empty()? "" :"_"+ m_syst_name + (m_is_up? "_1UP": "_1DN")))),          
            m_mc_eff_decor(std::make_unique<FloatDecorator>(ref_tool.mc_effi_decoration()+ (m_syst_name.empty()? "" :"_"+ m_syst_name + (m_is_up? "_1UP": "_1DN")))),
            m_sf_rep_decor(std::make_unique<FloatVectorDecorator>(ref_tool.sf_replica_decoration()+ (m_syst_name.empty()? "" :"_"+ m_syst_name + (m_is_up? "_1UP": "_1DN")))),
            m_eff_rep_decor(std::make_unique<FloatVectorDecorator>(ref_tool.data_effi_replica_decoration()+ (m_syst_name.empty()? "" :"_"+ m_syst_name + (m_is_up? "_1UP": "_1DN")))),
            m_mc_eff_rep_decor(std::make_unique<FloatVectorDecorator>(ref_tool.mc_effi_replica_deocration()+ (m_syst_name.empty()? "" :"_"+ m_syst_name + (m_is_up? "_1UP": "_1DN")))),
          
            m_sf_KineDepsys(),
            m_sf_replicas(),
            m_eff_replicas(),
            m_mc_eff_replicas(),
            m_NominalFallBack(nominal),
            m_SystematicBin(-1),
            m_default_eff(1.),
            m_default_eff_ttva(1.),
            m_warnsPrinted(0),
            m_firstRun(1),
            m_lastRun(999999){
    
        if (ref_tool.measurement() == CP::MuonEfficiencyType::Iso){
            dRJetAxisHandler::set_close_jet_decorator(ref_tool.close_by_jet_decoration());
            dRJetAxisHandler::set_use_2D_sf(ref_tool.use_2D_iso_corrections());
        }    
        // open the file
        std::unique_ptr<TFile> f (TFile::Open(file.c_str(), "READ"));
        if (!f || !f->IsOpen()) {
            Error("EfficiencyScaleFactor", "Unable to open file %s", file.c_str());
            return;
        }
        // now we can read our three mean histograms Histos
        m_eff = ReadHistFromFile("Eff", f.get(), time_unit);
        m_mc_eff = ReadHistFromFile("MC_Eff", f.get(), time_unit);
        m_sf = ReadHistFromFile("SF", f.get(), time_unit);
        /// Nominal set loaded nothing needs to be done further      
        if (syst_name.empty()) return;
        
        
        if (IsUpVariation()){
            m_default_eff = 2.;
            m_default_eff_ttva = 1. + 1.e-9;
        } else {
            m_default_eff = 0.;
            m_default_eff_ttva = 1. - 1.e-9;            
=======
    EfficiencyScaleFactor::EfficiencyScaleFactor() :
                    m_sf(0),
                    m_eff(0),
                    m_mc_eff(0),
                    m_sf_sys(0),
                    m_eff_sys(0),
                    m_mc_eff_sys(0),
                    m_sf_KineDepsys(0),
                    m_eff_KineDepsys(0),
                    m_sf_replicas(),
                    m_eff_replicas(),
                    m_mc_eff_replicas(),
                    m_etaphi(),
                    m_sysType(),
                    m_is_lowpt(false),
                    m_respond_to_kineDepSyst(false),
                    m_default_eff(1.),
                    m_default_eff_ttva(1.),
                    m_Type(CP::MuonEfficiencyType::Undefined),
                    m_NominalFallBack(NULL),
                    m_SystematicBin(-1) {
    }

    EfficiencyScaleFactor::EfficiencyScaleFactor(const std::string &file, const std::string &time_unit, MuonEfficiencySystType sysType, CP::MuonEfficiencyType effType, bool isLowPt, bool hasPtDepSys) :
                    EfficiencyScaleFactor() {
        m_sysType = sysType;
        m_Type = effType;
        m_is_lowpt = isLowPt;
        m_respond_to_kineDepSyst = hasPtDepSys;
        ReadFromFile(file, time_unit);
        if ((sysname().find("up") != std::string::npos) || (sysname().find("Up") != std::string::npos)) {
            m_default_eff = 2.;
            m_default_eff_ttva = 1. + 1.e-9;
        } else if ((sysname().find("down") != std::string::npos) || (sysname().find("Down") != std::string::npos)) {
            m_default_eff = 0.;
            m_default_eff_ttva = 1. - 1.e-9;
        }
    }
    EfficiencyScaleFactor::EfficiencyScaleFactor(EfficiencyScaleFactor* Nominal, const std::string &file, const std::string &time_unit, MuonEfficiencySystType sysType, CP::MuonEfficiencyType effType, bool is_lowpt, bool hasPtDepSys) :
                    EfficiencyScaleFactor::EfficiencyScaleFactor(file, time_unit, sysType, effType, is_lowpt, hasPtDepSys) {
        m_NominalFallBack = Nominal;

    }

    EfficiencyScaleFactor::EfficiencyScaleFactor(const EfficiencyScaleFactor & other) {
        CopyContent(other);
    }
    EfficiencyScaleFactor & EfficiencyScaleFactor::operator =(const EfficiencyScaleFactor & other) {
        if (this == &other) {
            return *this;
        }
        CopyContent(other);
        return *this;
    }
    void EfficiencyScaleFactor::CopyContent(const EfficiencyScaleFactor &other) {
        Clear();
        m_sysType = other.m_sysType;
        m_is_lowpt = other.m_is_lowpt;
        m_respond_to_kineDepSyst = other.m_respond_to_kineDepSyst;
        m_Type = other.m_Type;
        CopyHistHandler(m_sf, other.m_sf);
        CopyHistHandler(m_eff, other.m_eff);
        CopyHistHandler(m_mc_eff, other.m_mc_eff);

        CopyHistHandler(m_sf_sys, other.m_sf_sys);
        CopyHistHandler(m_eff_sys, other.m_eff_sys);
        CopyHistHandler(m_mc_eff_sys, other.m_mc_eff_sys);

        if (other.m_respond_to_kineDepSyst) {
            if (m_sf_KineDepsys) {
                (*m_sf_KineDepsys) = (*other.m_sf_KineDepsys);
            }
            if (m_eff_KineDepsys) {
                (*m_eff_KineDepsys) = (*other.m_eff_KineDepsys);
            }
        }
        CopyReplicaVec(m_sf_replicas, other.m_sf_replicas);
        CopyReplicaVec(m_eff_replicas, other.m_eff_replicas);
        CopyReplicaVec(m_mc_eff_replicas, other.m_mc_eff_replicas);
        m_NominalFallBack = other.m_NominalFallBack;
        m_SystematicBin = other.m_SystematicBin;
    }
    void EfficiencyScaleFactor::CopyHistHandler(HistHandler* &own, const HistHandler* other) {
        if (!other) {
            own = 0;
            return;
        }
        own = package_histo(dynamic_cast<TH1*>(other->GetHist()->Clone((std::string("EffSFCloneOf") + other->GetHist()->GetName()).c_str())));

    }
    void EfficiencyScaleFactor::CopyReplicaVec(EfficiencyScaleFactor::SFvec &own, const EfficiencyScaleFactor::SFvec &other) {
        for (ciSFvec h = other.begin(); h != other.end(); ++h) {
            HistHandler* copy = 0;
            CopyHistHandler(copy, *h);
            if (copy) own.push_back(copy);
        }
    }
    bool EfficiencyScaleFactor::ReadFromFile(std::string file, std::string time_unit) {
        // open the file
        TDirectory* dir = gDirectory;
        TFile* f = TFile::Open(file.c_str(), "READ");
        if (!f) {
            Error("EfficiencyScaleFactor", "Unable to open file %s", file.c_str());
            return false;
        }
        dir->cd();
        // now we can read our four Histos
        m_eff = ReadHistFromFile("Eff", f, time_unit);
        m_eff_sys = ReadHistFromFile("Eff_sys", f, time_unit);

        m_sf = ReadHistFromFile("SF", f, time_unit);
        m_sf_sys = ReadHistFromFile("SF_sys", f, time_unit);

        m_mc_eff = ReadHistFromFile("MC_Eff", f, time_unit);
        m_mc_eff_sys = ReadHistFromFile("MC_Eff_sys", f, time_unit);

        // for high pt eff, we also load the pt dependent part
        if (m_respond_to_kineDepSyst) {
            if (m_Type != CP::MuonEfficiencyType::BadMuonVeto) {
                m_sf_KineDepsys = new PtDependentSystHandler(ReadHistFromFile("SF_PtDep_sys", f, time_unit));
                m_eff_KineDepsys = new PtDependentSystHandler(ReadHistFromFile("Eff_PtDep_sys", f, time_unit));
            } else {
                TDirectory* SystDir = NULL;
                f->GetObject(("KinematicSystHandler_" + time_unit).c_str(), SystDir);
                m_sf_KineDepsys = new BadMuonVetoSystHandler(SystDir);
                m_eff_KineDepsys = new BadMuonVetoSystHandler(SystDir);
            }

        }
        // and don't forget to close the file again!
        f->Close();
        delete f;
        return true;
    }
    bool EfficiencyScaleFactor::CheckConsistency() const {
        //Check whether  the SFs could be successfully loaded
        if (!m_sf) Error("EfficiencyScaleFactor", ("Could not load the SF for " + EfficiencyTypeName(m_Type) + " and systematic " + EfficiencySystName(m_sysType)).c_str());
        if (!m_sf_sys) Error("EfficiencyScaleFactor", ("Could not load the SF systematic for " + EfficiencyTypeName(m_Type) + " and systematic " + EfficiencySystName(m_sysType)).c_str());
        if (m_respond_to_kineDepSyst && !m_sf_KineDepsys->initialize()) {
            Error("EfficiencyScaleFactor", ("Could not load the SF pt-dependent systematic for " + EfficiencyTypeName(m_Type) + " and systematic " + EfficiencySystName(m_sysType)).c_str());
            return false;
        }
        if (m_NominalFallBack == this) {
            Error("EfficiencyScaleFactor", ("The EfficiencyScaleFactor " + EfficiencyTypeName(m_Type) + " has itself as Nominal Fall back").c_str());
            return false;
        }
        return m_sf_sys != NULL && m_sf != NULL;
    }
    HistHandler* EfficiencyScaleFactor::ReadHistFromFile(std::string name, TFile* f, std::string time_unit) {
        TH1* histHolder = 0;
        f->GetObject((name + std::string("_") + time_unit).c_str(), histHolder);
        if (!histHolder) {
            // if no period weighting, the histo may also not have a time period in the name at all
            if (time_unit == "All") {
                f->GetObject(name.c_str(), histHolder);
            }
            return 0;
        }
        // replace the histos by clones so that we can close the files again
        HistHandler* out = package_histo((TH1*) (histHolder->Clone(Form("%s_%s_%s%s", name.c_str(), f->GetName(), time_unit.c_str(), sysname().c_str()))));
        if (histHolder) delete histHolder;
        return out;
    }
    HistHandler *EfficiencyScaleFactor::package_histo(TH1* h) {
        // make sure that the correct type of histo is used
        if (dynamic_cast<TH1F*>(h)) {
            return new HistHandler_TH1F(dynamic_cast<TH1F*>(h));
        } else if (dynamic_cast<TH2F*>(h)) {
            return new HistHandler_TH2F(dynamic_cast<TH2F*>(h));
        } else if (dynamic_cast<TH3F*>(h)) {
            return new HistHandler_TH3F(dynamic_cast<TH3F*>(h));
        } else if (dynamic_cast<TH2Poly*>(h)) {
            return new HistHandler_TH2Poly(dynamic_cast<TH2Poly*>(h));
        } else {
            Error("EfficiencyScaleFactor", "Unable to package histo %s (%s) in a known HistHandler", h->GetName(), h->IsA()->GetName());
            return 0;
>>>>>>> release/21.0.127
        }
        std::function<void(std::unique_ptr<HistHandler>&, const std::string& )> syst_loader = [this, &f, &time_unit, &syst_type_bitmap] (std::unique_ptr<HistHandler>& nominal, const std::string& hist_type) {
            if(!nominal) return;
            std::unique_ptr<HistHandler> sys = ReadHistFromFile(Form("%s_%s_%s", hist_type.c_str() ,
                                                                                 m_syst_name.c_str(), 
                                                                                 (syst_type_bitmap & EffiCollection::Symmetric ? "SYM" : (m_is_up ? "1UP" : "1DN")) ), 
                                                                f.get(), time_unit);
            if (sys) {
                for (int i = 1; i <= nominal->nBins(); ++i) {
                    double content = nominal->GetBinContent(i);
                    double variation = (IsUpVariation() ? 1. : -1.)*sys->GetBinContent(i);
                    nominal->SetBinContent(i,content + variation);
                }
                return;
            }
            /// Asking for the total systematic... May be the current file
            /// does not support the asymmetric break-down yet. Let's try the good old approach
            /// and load the total sys histogram
            if (m_syst_name == "SYS"){
                std::unique_ptr<HistHandler> old_sys = ReadHistFromFile(hist_type +"_sys", f.get(), time_unit);
                /// Not even the old approach lead to something fruitful... Lets forget it and reset everything
                if (!old_sys){
                    nominal.reset();
                    return;
                }
                
                for (int i = 1; i<= nominal->nBins(); ++i) {
                     nominal->SetBinContent(i, nominal->GetBinContent(i) + (IsUpVariation() ? 1. : -1.)*old_sys->GetBinContent(i));
                }
            } 
            /// Stat error can be retrieved from the nominal histogram itself
            else if (m_syst_name == "STAT") {
                for (int i = 1; i<= nominal->nBins(); ++i) {
                     nominal->SetBinContent(i, nominal->GetBinContent(i) + (IsUpVariation() ? 1. : -1.)*nominal->GetBinError(i));
                }
            } 
            /// This systematic name is reserved for the kinematic histhandler
            else if (m_syst_name == "PTDEPENDENCY"){
                return;
            } 
            /// Some other systematic is asked for... Failure
            else{
                Error("EfficiencyScaleFactor()", "Failed to load sytstematic variation  %s for measurement %s and histo-type %s", sysname(true).c_str(), 
                                                                                                               EfficiencyTypeName(m_measurement).c_str(),
                                                                                                               hist_type.c_str());
                nominal.reset();
            }
        };
        /// Apply the systematic variations
        syst_loader(m_sf,"SF");
        syst_loader(m_eff,"Eff");
        syst_loader(m_mc_eff,"MC_Eff");        
       
        /// Thus far there're no kinematic dependent systematics for low-pt
        if (m_is_lowpt || (file == ref_tool.filename_HighEta() && ref_tool.filename_HighEta() != ref_tool.filename_Central()) ||
        (file == ref_tool.filename_Calo() && ref_tool.filename_Calo() != ref_tool.filename_Central())) m_respond_to_kineDepSyst =false;
        /// As well  as for the high-eta  range.
       
        /// Load the pt_dependent systematics if needed
        if (!m_respond_to_kineDepSyst){
             return;
        }      
        // Load the systematic of the bad veto
        if (m_measurement == CP::MuonEfficiencyType::BadMuonVeto) {
            TDirectory* SystDir = nullptr;
            f->GetObject(("KinematicSystHandler_" + time_unit).c_str(), SystDir);
            /// Old format of the pt-dependent systematic loaded
            if (SystDir) {
                m_sf_KineDepsys = std::make_unique<BadMuonVetoSystHandler>(SystDir);
            } else {
                /// Now it comes to the new format
                f->GetObject("BadMuonVetoSyst_3Stations", SystDir);
                TDirectory* SystDir_2Stat = nullptr;
                f->GetObject("BadMuonVetoSyst_2Stations", SystDir_2Stat);
                m_sf_KineDepsys = std::make_unique<BadMuonVetoSystHandler>(SystDir, SystDir_2Stat);
            }           
            m_sf_KineDepsys->SetSystematicWeight( IsUpVariation() ? 1 : -1);
            if (!m_sf_KineDepsys->initialize()){
                Error("EfficiencyScaleFactor()", "Pt dependent systematic could not be loaded");
                m_sf_KineDepsys.reset();
            }           
            return;
        } else if (m_measurement == CP::MuonEfficiencyType::TTVA){
            m_sf_KineDepsys = std::make_unique<TTVAClosureSysHandler>(ReadHistFromFile("SF_NonClosure_sys",f.get(),time_unit));
            if (!m_sf_KineDepsys->initialize()){
                Error("EfficiencyScaleFactor()", "TTVA non closure systematic could not be loaded.");
                m_sf_KineDepsys.reset();
            }
            m_sf_KineDepsys->SetSystematicWeight( IsUpVariation() ? 1 : -1);
            return;
        
        }
        /// That one needs to be named properly in the future        
        m_sf_KineDepsys = std::make_unique<PtKinematicSystHandler>(ReadHistFromFile(Form("SF_PtFlatness_1%s", m_is_up?"UP" :"DN"), f.get(), time_unit), ReadHistFromFile("SF_PtDep_sys", f.get(), time_unit));
      
        /// Use the approach from the old sacle-factor file
        if(!m_sf_KineDepsys->initialize()){    
            m_sf_KineDepsys = std::make_unique<PrimodialPtSystematic>(ReadHistFromFile("SF_PtDep_sys", f.get(), time_unit));
        }
        m_sf_KineDepsys->SetSystematicWeight( IsUpVariation() ? 1 : -1);            
    }
<<<<<<< HEAD
    EfficiencyScaleFactor::EfficiencyScaleFactor(const MuonEfficiencyScaleFactors& ref_tool,
                                  const std::string &file, 
                                  const std::string &time_unit):
                                  EfficiencyScaleFactor(std::shared_ptr<EfficiencyScaleFactor>(),
                                                        ref_tool,file, time_unit, "",0){}
           
    std::string EfficiencyScaleFactor::sysname(bool with_direction) const {
        return m_syst_name.empty() ? "" : EfficiencyTypeName(m_measurement) +  "_" + m_syst_name +(m_is_lowpt ? "_LOWPT" : "") + (with_direction ? (m_is_up ?"__1UP" : "__1DN") :"");  
    }
    bool EfficiencyScaleFactor::separateBinSyst() const {
        return m_separateBinSyst;
    }
    bool EfficiencyScaleFactor::IsUpVariation() const{
        return m_is_up;
    }
    unsigned int EfficiencyScaleFactor::firstRun() const{
        return m_firstRun;
    }
    unsigned int EfficiencyScaleFactor::lastRun() const{
        return m_lastRun;
    }
    bool EfficiencyScaleFactor::coversRunNumber(unsigned int run) const{
        return m_firstRun <= run && run <= m_lastRun;
    }
    void EfficiencyScaleFactor::setFirstLastRun(unsigned int first, unsigned int last){
        m_firstRun = first;
        m_lastRun = last;
    }
    bool EfficiencyScaleFactor::CheckConsistency()  {
        //Check whether  the SFs could be successfully loaded
        if (!m_sf) {
            Error("EfficiencyScaleFactor()", "Could not load the SF for  measurement %s and systematic %s",EfficiencyTypeName(m_measurement).c_str(), sysname().c_str());
            return false;
        }
        if (m_respond_to_kineDepSyst && !m_sf_KineDepsys->initialize()) {
            Error("EfficiencyScaleFactor()", "Could not load the SF pt-dependent systematic for %s and systematic %s", EfficiencyTypeName(m_measurement).c_str(), sysname().c_str() );
            return false;
        }
        if (m_NominalFallBack.get() == this) {
            Error("EfficiencyScaleFactor()", "The EfficiencyScaleFactor %s has itself as Nominal Fall back.", EfficiencyTypeName(m_measurement).c_str());
            m_NominalFallBack.reset();
            return false;
        }
        if (m_NominalFallBack) {
            if (!m_NominalFallBack->sysname().empty() || sysname().empty()){
                Error("EfficiencyScaleFactor()", "Either nominal is assigned with a fall back (%s) or the nominal fall back is invald(%s).", sysname().c_str(), m_NominalFallBack->sysname().c_str() );
                return false;
            }
        }
        if (firstRun() > lastRun()){
            Error("EfficiencyScaleFactor()", "Invalid run number range. Since the map is ranging from %u to %u.", firstRun(), lastRun());
            return false;
        }
        std::function<bool(const std::unique_ptr<HistHandler>&)> consistent_histo = [](const std::unique_ptr<HistHandler>& histo)->bool{
            /// The histogram is simply not loaded which is fine since only
            /// the scale-factor is required
            if (!histo) return true;
            for ( int i = 1; i <= histo->nBins(); ++i){
                if (std::isnan(histo->GetBinContent(i)) || std::isinf(histo->GetBinContent(i))){
                    Error("EfficiencyScaleFactor()", "The %d-th bin %s has not a number (%f)",i, histo->GetBinName(i).c_str(), histo->GetBinContent(i));
                    return false;
                }
            }
            return true;
        };
        if (!consistent_histo(m_sf)){
            Error("EfficiencyScaleFactor()", "Invalid scalefactor in %s", sysname().c_str());
            return false;
        }
        if (!consistent_histo(m_eff)){
            Error("EfficiencyScaleFactor()", "Invalid data-efficiency in %s", sysname().c_str());
            return false;
        }
        if (!consistent_histo(m_mc_eff)){
            Error("EfficiencyScaleFactor()", "Invalid MC-efficiency in %s", sysname().c_str());
            return false;
        }
        return true;
    }
    std::unique_ptr<HistHandler> EfficiencyScaleFactor::ReadHistFromFile(const std::string& name, TFile* f, const std::string& time_unit) {
        
        TH1* hist_from_file =  nullptr;
        
        f->GetObject((name + std::string("_") + time_unit).c_str(), hist_from_file);
         // if no period weighting, the histo may also not have a time period in the name at all
           
        if (!hist_from_file && time_unit == "All") {
            f->GetObject(name.c_str(), hist_from_file);
        }
        if (!hist_from_file) {
            return std::unique_ptr<HistHandler>();
        }
        return package_histo(hist_from_file);
    }
    std::unique_ptr<HistHandler> EfficiencyScaleFactor::package_histo(TH1* h) {
        // make sure that the correct type of histo is used
        // Dynamic cast for TH2 Poly otherwise we can rely on the GetDimension() ,ethod
        if (!h) return std::unique_ptr<HistHandler>();
        if (dynamic_cast<TH2Poly*>(h)) {
            return std::make_unique<HistHandler_TH2Poly>(dynamic_cast<TH2Poly*>(h));
        }else if (h->GetDimension() == 3) {
            return std::make_unique<HistHandler_TH3>(h);
        }else if (h->GetDimension() == 2) {
            return std::make_unique<HistHandler_TH2>(h);
        } else if (h->GetDimension() == 1) {
            return std::make_unique<HistHandler_TH1>(h);
        } 
        Error("EfficiencyScaleFactor", "Unable to package histo %s (%s) in a known HistHandler", h->GetName(), h->IsA()->GetName());
        return std::unique_ptr<HistHandler>();
    }
    int EfficiencyScaleFactor::nBins() const {
        return m_sf ? m_sf->nBins() : -1;
    }
    int EfficiencyScaleFactor::nOverFlowBins() const{
        return m_sf ? m_sf->nOverFlowBins() : -1;
    }
    bool EfficiencyScaleFactor::isOverFlowBin(int b) const{      
        return m_sf ? m_sf->isOverFlowBin(b) : true;
    }
    CorrectionCode EfficiencyScaleFactor::ScaleFactor(const xAOD::Muon& mu, float & SF) const {
        if (m_separateBinSyst && m_NominalFallBack) {
            int bin = -1;
            CorrectionCode cc = m_sf->FindBin(mu, bin);
            if (cc == CP::CorrectionCode::Error) {
                return cc;
            }
            if (bin != m_SystematicBin) {
                return m_NominalFallBack->ScaleFactor(mu, SF);
            }
        }       
        CorrectionCode cc = GetContentFromHist(m_sf.get(), mu, SF, true);
        if (cc == CorrectionCode::Error) Error("EfficiencyScaleFactor", "Could not apply the scale factor");
        return cc;
    }   

    CorrectionCode EfficiencyScaleFactor::DataEfficiency(const xAOD::Muon& mu, float & Eff) const {
        if (m_separateBinSyst && m_NominalFallBack && m_eff) {
            int bin = -1;
            CorrectionCode cc = m_eff->FindBin(mu, bin);
            if (cc == CP::CorrectionCode::Error) {
                return cc;
            }
            if (bin != m_SystematicBin) {
                return m_NominalFallBack->DataEfficiency(mu, Eff);
            }
        }
        CorrectionCode cc = GetContentFromHist(m_eff.get(), mu, Eff, true);
        if (cc == CorrectionCode::Error) Error("EfficiencyScaleFactor", "Could not apply the data efficiency");
        return cc;
    }
    CorrectionCode EfficiencyScaleFactor::MCEfficiency(const xAOD::Muon& mu, float & Eff) const {
        if (m_separateBinSyst && m_NominalFallBack && m_mc_eff) {
            int bin = -1;
            CorrectionCode cc = m_mc_eff->FindBin(mu, bin);
            if (cc == CP::CorrectionCode::Error) {
                return cc;
            }
            if (bin != m_SystematicBin) {
                return m_NominalFallBack->MCEfficiency(mu, Eff);
            }
        }
        CorrectionCode cc = GetContentFromHist(m_mc_eff.get(), mu, Eff, false);
        if (cc == CorrectionCode::Error) Error("EfficiencyScaleFactor", "Could not apply the Monte Carlo efficiency");
        return cc;
    }

    CorrectionCode EfficiencyScaleFactor::GetContentFromHist(HistHandler* Hist, const xAOD::Muon& mu, float & Eff, bool add_kine_syst) const {
        Eff = m_default_eff;
        if (!Hist) {
            if (m_warnsPrinted < m_warningLimit){
                Warning("EfficiencyScaleFactor", "Could not find histogram for variation %s and muon with pt=%.4f, eta=%.2f and phi=%.2f, returning %.1f", sysname().c_str(), mu.pt(), mu.eta(), mu.phi(), m_default_eff);
                ++m_warnsPrinted;
            }
            return CorrectionCode::OutOfValidityRange;
        }
        if (m_measurement == CP::MuonEfficiencyType::TTVA && std::abs(mu.eta()) > 2.5 && std::abs(mu.eta()) <= 2.7 && mu.muonType() == xAOD::Muon::MuonType::MuonStandAlone) {
            if (m_warnsPrinted < m_warningLimit){
                Info("EfficiencyScaleFactor", "No TTVA sf/efficiency provided for standalone muons with 2.5<|eta|<2.7 for variation %s and muon with pt=%.4f, eta=%.2f and phi=%.2f, returning %.1f", sysname().c_str(), mu.pt(), mu.eta(), mu.phi(), m_default_eff_ttva);
                ++m_warnsPrinted;
            }
            Eff = m_default_eff_ttva;
            return CorrectionCode::Ok;
        }
        int bin = -1;       
        CorrectionCode cc = Hist->FindBin(mu, bin);
        if (cc != CorrectionCode::Ok) return cc;
        else Eff = Hist->GetBinContent(bin);
       
        if (add_kine_syst && m_respond_to_kineDepSyst) {           
            return m_sf_KineDepsys->GetKineDependent(mu, Eff);
        }
        return CorrectionCode::Ok;
    }
    CorrectionCode EfficiencyScaleFactor::ApplyScaleFactor(const xAOD::Muon& mu) const{
        float sf = 0;
        CorrectionCode result = ScaleFactor(mu, sf);
        (*m_sf_decor)(mu) = sf;
        return result;  
    }
    CorrectionCode EfficiencyScaleFactor::ApplyScaleFactorReplicas(const xAOD::Muon& mu, int n_replicas) {
        std::vector<float> replicas(n_replicas);
        CorrectionCode result = ScaleFactorReplicas(mu, replicas);
        (*m_sf_rep_decor)(mu)= replicas;
        return result;
    }
    
    CorrectionCode EfficiencyScaleFactor::ApplyDataEfficiency(const xAOD::Muon& mu) const {
        float effi = 0;
        CorrectionCode result = DataEfficiency(mu, effi);
        (*m_eff_decor)(mu) = effi;
        return result;  
    }
    CorrectionCode EfficiencyScaleFactor::ApplyDataEfficiencyReplicas(const xAOD::Muon& mu, int n_replicas){
        std::vector<float> replicas(n_replicas);
        CorrectionCode result = DataEfficiencyReplicas(mu, replicas);
        (*m_eff_rep_decor)(mu) = replicas;
        return result;
    }
   CorrectionCode EfficiencyScaleFactor::ApplyMCEfficiency(const xAOD::Muon& mu) const {
        float effi = 0;
        CorrectionCode result = MCEfficiency(mu, effi);
        (*m_mc_eff_decor)(mu) = effi;
        return result;  
    }
    CorrectionCode EfficiencyScaleFactor::ApplyMCEfficiencyReplicas(const xAOD::Muon& mu, int n_replicas){
        std::vector<float> replicas(n_replicas);
        CorrectionCode result = MCEfficiencyReplicas(mu, replicas);
        (*m_mc_eff_rep_decor)(mu) = replicas;
        return result;
    } 
    CorrectionCode EfficiencyScaleFactor::ScaleFactorReplicas(const xAOD::Muon& mu, std::vector<float> & SF) {
        return GetContentReplicasFromHist(m_sf_replicas, mu, SF, true);
    }
    CorrectionCode EfficiencyScaleFactor::DataEfficiencyReplicas(const xAOD::Muon& mu, std::vector<float> & eff) {
        return GetContentReplicasFromHist(m_eff_replicas, mu, eff, true);
    }
    CorrectionCode EfficiencyScaleFactor::MCEfficiencyReplicas(const xAOD::Muon& mu, std::vector<float> & eff) {
        return GetContentReplicasFromHist(m_mc_eff_replicas, mu, eff, false);
    }
    CorrectionCode EfficiencyScaleFactor::GetContentReplicasFromHist(EfficiencyScaleFactor::SFReplicaVec &replicas, const xAOD::Muon& mu, std::vector<float> & SF, bool add_kine_syst) {
        if (replicas.size() != SF.size()) GenerateReplicas(SF.size(), 1000. * mu.phi() + mu.eta());
        if (replicas.empty()) return CorrectionCode::OutOfValidityRange;
        if (m_measurement == CP::MuonEfficiencyType::TTVA && std::abs(mu.eta()) > 2.5 && std::abs(mu.eta()) <= 2.7 && mu.muonType() == xAOD::Muon::MuonType::MuonStandAlone) {
            if (m_warnsPrinted < m_warningLimit){
                Info("EfficiencyScaleFactor", "No TTVA sf/efficiency provided for standalone muons with 2.5<|eta|<2.7 for variation %s and muon with pt=%.4f, eta=%.2f and phi=%.2f, returning %.1f", sysname().c_str(), mu.pt(), mu.eta(), mu.phi(), m_default_eff_ttva);
                ++m_warnsPrinted;
            }
            for (auto& r : SF)
                r = m_default_eff_ttva;
            return CorrectionCode::Ok;
        }
        int bin = -1;
        float extra_sys = 1;
        if (add_kine_syst && m_respond_to_kineDepSyst) {
           CorrectionCode cc = m_sf_KineDepsys->GetKineDependent(mu, extra_sys);
            if (cc != CorrectionCode::Ok) {}
        }
        
        CorrectionCode res = (*replicas.begin())->FindBin(mu, bin);
        if (res != CorrectionCode::Ok) return res;
        else {
            for (size_t k = 0; k < SF.size(); k++) {
                SF[k] = replicas.at(k)->GetBinContent(bin) * extra_sys;
            }
        }
        return res;
    }
    void EfficiencyScaleFactor::GenerateReplicas(int nrep, int seed) {
        GenerateReplicasFromHist(m_eff.get(), nrep, seed, m_eff_replicas);
        GenerateReplicasFromHist(m_sf.get(), nrep, seed, m_sf_replicas);
        GenerateReplicasFromHist(m_mc_eff.get(), nrep, seed, m_mc_eff_replicas);
    }
    void EfficiencyScaleFactor::GenerateReplicasFromHist(HistHandler* h, int nrep, int seed, EfficiencyScaleFactor::SFReplicaVec &replicas) {
        if (!h) return;
        TRandom3 Rndm(seed);
        replicas.clear();
        replicas.reserve(nrep);
        int nBins = h->nBins();
        for (int t = 0; t < nrep; t++) {
            replicas.push_back(package_histo(h->GetHist()));
            HistHandler* replica = replicas.back().get();
            for (int bin = 0; bin < nBins; bin++) {
                replica->SetBinContent(bin, Rndm.Gaus(h->GetBinContent(bin), h->GetBinError(bin)));
            }            
        }
    }
    
    bool EfficiencyScaleFactor::SetSystematicBin(int bin) {
        if (m_syst_name.empty()) return true;
        if (!m_NominalFallBack) {
            Error("EfficiencyScaleFactor::SetSystematicBin()", "No fallback has been given for %s", sysname().c_str());
            return false;
        }
        if (!m_separateBinSyst || bin < 1 || bin > nBins()) {
            Error("EfficiencyScaleFactor::SetSystematicBin()", "The current bin %i is out of the maximum range %u ", bin, nBins());
            return false;
        }
        m_SystematicBin = bin;
        return true;
    }
    std::string EfficiencyScaleFactor::GetBinName(int bin) const {
        if (bin < 1 || bin > nBins()) {
            Error("EfficiencyScaleFactor::GetBinName()", "The current bin %i is out of the maximum range %u ", bin, nBins());
            return "Out of range";
        }
        return m_sf->GetBinName(bin);
    }
    
    int EfficiencyScaleFactor::FindBinSF(const xAOD::Muon & mu) const {
        int bin = -1;
        CorrectionCode cc = m_sf->FindBin(mu, bin);
        if (cc == CP::CorrectionCode::Error) {
            return -1;
        }
        return bin;
    }
=======
    int EfficiencyScaleFactor::nBinsSF() const {
        if (m_sf) {
            return m_sf->NBins();
        }
        return -1;
    }
    int EfficiencyScaleFactor::nBinsEfficiency() const {
        if (m_eff) {
            return m_eff->NBins();
        }
        if (m_mc_eff) {
            return m_mc_eff->NBins();
        }
        return -1;
    }
    std::string EfficiencyScaleFactor::sysname() const {
        return EfficiencySystName(m_sysType);
    }
    CorrectionCode EfficiencyScaleFactor::ScaleFactor(const xAOD::Muon& mu, float & SF) const {
        if (m_NominalFallBack) {
            int bin = -1;
            CorrectionCode cc = m_sf->FindBin(mu, bin);
            if (cc == CP::CorrectionCode::Error) {
                return cc;
            }
            if (bin != m_SystematicBin) {
                return m_NominalFallBack->ScaleFactor(mu, SF);
            }
        }
        CorrectionCode cc = GetContentFromHist(m_sf, m_sf_KineDepsys, mu, SF, m_respond_to_kineDepSyst);
        if (cc == CorrectionCode::Error) Error("EfficiencyScaleFactor", "Could not apply the scale factor");
        return cc;
    }

    CorrectionCode EfficiencyScaleFactor::DataEfficiency(const xAOD::Muon& mu, float & Eff) const {
        if (m_NominalFallBack && m_eff) {
            int bin = -1;
            CorrectionCode cc = m_eff->FindBin(mu, bin);
            if (cc == CP::CorrectionCode::Error) {
                return cc;
            }
            if (bin != m_SystematicBin) {
                return m_NominalFallBack->DataEfficiency(mu, Eff);
            }
        }
        CorrectionCode cc = GetContentFromHist(m_eff, m_eff_KineDepsys, mu, Eff, m_respond_to_kineDepSyst);
        if (cc == CorrectionCode::Error) Error("EfficiencyScaleFactor", "Could not apply the data efficiency");
        return cc;
    }
    CorrectionCode EfficiencyScaleFactor::MCEfficiency(const xAOD::Muon& mu, float & Eff) const {
        if (m_NominalFallBack && m_mc_eff) {
            int bin = -1;
            CorrectionCode cc = m_mc_eff->FindBin(mu, bin);
            if (cc == CP::CorrectionCode::Error) {
                return cc;
            }
            if (bin != m_SystematicBin) {
                return m_NominalFallBack->MCEfficiency(mu, Eff);
            }
        }
        CorrectionCode cc = GetContentFromHist(m_mc_eff, NULL, mu, Eff, false);
        if (cc == CorrectionCode::Error) Error("EfficiencyScaleFactor", "Could not apply the Monte Carlo efficiency");
        return cc;
    }

    CorrectionCode EfficiencyScaleFactor::GetContentFromHist(HistHandler* Hist, IKinematicSystHandler* PtDepHist, const xAOD::Muon& mu, float & Eff, bool PtDepHistNeeded) const {
        Eff = m_default_eff;
        if (!Hist) {
            Warning("EfficiencyScaleFactor", Form("Could not find histogram for variation %s and muon with pt=%.4f, eta=%.2f and phi=%.2f, returning %.1f", sysname().c_str(), mu.pt(), mu.eta(), mu.phi(), m_default_eff));
            return CorrectionCode::OutOfValidityRange;
        }
        if (m_Type == CP::MuonEfficiencyType::TTVA && fabs(mu.eta()) > 2.5 && fabs(mu.eta()) <= 2.7 && mu.muonType() == xAOD::Muon::MuonType::MuonStandAlone) {
            static bool Warned = false;
            if (!Warned) Info("EfficiencyScaleFactor", Form("No TTVA sf/efficiency provided for standalone muons with 2.5<|eta|<2.7 for variation %s and muon with pt=%.4f, eta=%.2f and phi=%.2f, returning %.1f", sysname().c_str(), mu.pt(), mu.eta(), mu.phi(), m_default_eff_ttva));
            Warned = true;
            Eff = m_default_eff_ttva;
            return CorrectionCode::Ok;
        }
        int bin = -1;
        CorrectionCode cc = Hist->FindBin(mu, bin);
        if (cc != CorrectionCode::Ok) return cc;
        else Eff = Hist->GetBinContent(bin);
        if (PtDepHistNeeded) {
            return PtDepHist->GetKineDependent(mu, Eff);
        }
        return CorrectionCode::Ok;
    }
    CorrectionCode EfficiencyScaleFactor::ScaleFactorReplicas(const xAOD::Muon& mu, std::vector<float> & SF) {
        return GetContentReplicasFromHist(m_sf_replicas, mu, SF);
    }
    CorrectionCode EfficiencyScaleFactor::DataEfficiencyReplicas(const xAOD::Muon& mu, std::vector<float> & eff) {
        return GetContentReplicasFromHist(m_eff_replicas, mu, eff);
    }
    CorrectionCode EfficiencyScaleFactor::MCEfficiencyReplicas(const xAOD::Muon& mu, std::vector<float> & eff) {
        return GetContentReplicasFromHist(m_mc_eff_replicas, mu, eff);
    }
    CorrectionCode EfficiencyScaleFactor::GetContentReplicasFromHist(EfficiencyScaleFactor::SFvec &replicas, const xAOD::Muon& mu, std::vector<float> & SF) {
        if (replicas.size() != SF.size()) GenerateReplicas(SF.size(), 1000. * mu.phi() + mu.eta());
        if (replicas.empty()) return CorrectionCode::OutOfValidityRange;
        if (m_Type == CP::MuonEfficiencyType::TTVA && fabs(mu.eta()) > 2.5 && fabs(mu.eta()) <= 2.7 && mu.muonType() == xAOD::Muon::MuonType::MuonStandAlone) {
            static bool Warned = false;
            if (!Warned) Info("EfficiencyScaleFactor", Form("No TTVA sf/efficiency provided for standalone muons with 2.5<|eta|<2.7 for variation %s and muon with pt=%.4f, eta=%.2f and phi=%.2f, returning %.1f", sysname().c_str(), mu.pt(), mu.eta(), mu.phi(), m_default_eff_ttva));
            Warned = true;
            for (auto& r : SF)
                r = m_default_eff_ttva;
            return CorrectionCode::Ok;
        }
        int bin = -1;
        CorrectionCode res = (*replicas.begin())->FindBin(mu, bin);
        if (res != CorrectionCode::Ok) return res;
        else {
            for (size_t k = 0; k < SF.size(); k++) {
                SF[k] = replicas.at(k)->GetBinContent(bin);
            }
        }
        return res;
    }
    EfficiencyScaleFactor::~EfficiencyScaleFactor() {
        Clear();
    }
    void EfficiencyScaleFactor::DeleteOldReplicas(EfficiencyScaleFactor::SFvec &Vec, bool ClearVec) {
        for (auto &old : Vec) {
            if (old) delete old;
            old = NULL;
        }
        if (ClearVec) Vec.clear();
    }
    void EfficiencyScaleFactor::Clear() {
        DeleteOldReplicas(m_sf_replicas, true);
        DeleteOldReplicas(m_eff_replicas, true);
        DeleteOldReplicas(m_mc_eff_replicas, true);
        if (m_sf) delete m_sf;
        if (m_eff) delete m_eff;
        if (m_mc_eff) delete m_mc_eff;
        if (m_mc_eff_sys) delete m_mc_eff_sys;
        if (m_sf_sys) delete m_sf_sys;
        if (m_eff_sys) delete m_eff_sys;
        if (m_sf_KineDepsys) delete m_sf_KineDepsys;
        if (m_eff_KineDepsys) delete m_eff_KineDepsys;
    }
    void EfficiencyScaleFactor::GenerateReplicas(int nrep, int seed) {
        GenerateReplicasFromHist(m_eff, nrep, seed, m_eff_replicas);
        GenerateReplicasFromHist(m_sf, nrep, seed, m_sf_replicas);
        GenerateReplicasFromHist(m_mc_eff, nrep, seed, m_mc_eff_replicas);
    }
    void EfficiencyScaleFactor::GenerateReplicasFromHist(HistHandler* h, int nrep, int seed, EfficiencyScaleFactor::SFvec &replicas) {
        if (!h) return;
        DeleteOldReplicas(replicas);
        TRandom3 Rndm(seed);
        replicas.resize(nrep);
        int nbins = h->NBins();
        for (int t = 0; t < nrep; t++) {
            HistHandler* replica = package_histo((TH1*) h->GetHist()->Clone(Form("rep%d_%s", t, h->GetHist()->GetName())));
            for (int bin = 0; bin < nbins; bin++) {
                replica->SetBinContent(bin, Rndm.Gaus(h->GetBinContent(bin), h->GetBinError(bin)));
            }
            replicas.at(t) = replica;
        }
    }
    void EfficiencyScaleFactor::ApplySysVariation() {
        if (m_sysType == MuonEfficiencySystType::Nominal) return;
        else if (m_sysType == MuonEfficiencySystType::Sys1Down) AddSysErrors(-1.);
        else if (m_sysType == MuonEfficiencySystType::Sys1Up) AddSysErrors(1.);
        else if (m_sysType == MuonEfficiencySystType::Stat1Down) AddStatErrors(-1.);
        else if (m_sysType == MuonEfficiencySystType::Stat1Up) AddStatErrors(1.);
        else if (m_sysType == MuonEfficiencySystType::LowPtSys1Down) AddSysErrors(-1.);
        else if (m_sysType == MuonEfficiencySystType::LowPtSys1Up) AddSysErrors(1.);
        else if (m_sysType == MuonEfficiencySystType::LowPtStat1Down) AddStatErrors(-1.);
        else if (m_sysType == MuonEfficiencySystType::LowPtStat1Up) AddStatErrors(1.);
    }
    void EfficiencyScaleFactor::AddStatErrors(float weight) {
        AddStatErrors_histo(m_sf, weight);
        AddStatErrors_histo(m_eff, weight);
        AddStatErrors_histo(m_mc_eff, weight);
    }

    void EfficiencyScaleFactor::AddSysErrors(float weight) {

        // turn on the pt dependent contribution, if there is any

        if (m_respond_to_kineDepSyst) {
            if (m_sf_KineDepsys) m_sf_KineDepsys->SetSystematicWeight(weight);
            if (m_eff_KineDepsys) m_eff_KineDepsys->SetSystematicWeight(weight);
        }
        AddSysErrors_histo(m_sf, m_sf_sys, weight);
        AddSysErrors_histo(m_eff, m_eff_sys, weight);
        AddSysErrors_histo(m_mc_eff, m_mc_eff_sys, weight);

        AddSysErrors_vector(m_sf_replicas, m_sf_sys, weight);
        AddSysErrors_vector(m_eff_replicas, m_sf_sys, weight);
        AddSysErrors_vector(m_mc_eff_replicas, m_sf_sys, weight);

    }
    void EfficiencyScaleFactor::AddSysErrors_vector(EfficiencyScaleFactor::SFvec &Vec, HistHandler* hsys, float weight) {
        for (auto Histo : Vec) {
            AddSysErrors_histo(Histo, hsys, weight);
        }
    }

    void EfficiencyScaleFactor::AddSysErrors_histo(HistHandler* h, HistHandler *hsys, float weight) {
        if (!h || !hsys) return;
        for (int t = 0; h && t < h->NBins(); ++t) {
            double binc = h->GetBinContent(t);
            binc += weight * hsys->GetBinContent(t);
            h->SetBinContent(t, binc);
        }
    }
    void EfficiencyScaleFactor::AddStatErrors_histo(HistHandler* h, float weight) {
        if (!h) return;
        for (int t = 0; t < h->NBins(); t++) {
            double binc = h->GetBinContent(t);
            binc += weight * h->GetBinError(t);
            h->SetBinContent(t, binc);
        }
    }

    void EfficiencyScaleFactor::DebugPrint(void) {
        m_sf->GetHist()->Print();
        m_sf_sys->GetHist()->Print();

        //Print only histograms if they are actually available
        if (m_eff) m_eff->GetHist()->Print();
        if (m_eff_sys) m_eff_sys->GetHist()->Print();

        if (m_mc_eff) m_mc_eff->GetHist()->Print();
        if (m_mc_eff_sys) m_mc_eff_sys->GetHist()->Print();
    }
    bool EfficiencyScaleFactor::SetSystematicBin(int bin) {
        if (!m_NominalFallBack) {
            Error("EfficiencyScaleFactor::SetSystematicBin()", "No fallback has been given");
            return false;
        }
        if (bin < 1 || bin > nBinsSF()) {
            Error("EfficiencyScaleFactor::SetSystematicBin()", "The current bin %i is out of the maximum range %u ", bin, nBinsSF());
            return false;
        }
        m_SystematicBin = bin;
        return true;
    }
>>>>>>> release/21.0.127

} /* namespace CP */
