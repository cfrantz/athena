/*
<<<<<<< HEAD
 Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
 */

#include "MuonEfficiencyCorrections/MuonSFTestHelper.h"
#include "MuonEfficiencyCorrections/UtilFunctions.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"


=======
 Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
 */

#include "MuonEfficiencyCorrections/MuonSFTestHelper.h"
>>>>>>> release/21.0.127
#include "PATInterfaces/SystematicsUtil.h"
#include <TH1D.h>
#define CHECK_CPCorr(Arg) \
    if (Arg.code() == CP::CorrectionCode::Error){    \
        Error(#Arg,"Correction Code 'Error' (returned in line %i) ",__LINE__); \
        return CP::CorrectionCode::Error;   \
    } 

namespace TestMuonSF {
<<<<<<< HEAD
        template<typename T> T getProperty(const asg::IAsgTool* interface_tool, const std::string& prop_name) {
        const asg::AsgTool* asg_tool = dynamic_cast<const asg::AsgTool*>(interface_tool);
        T prop;
        const T* HandlePtr = asg_tool->getProperty < T > (prop_name);
        if (!HandlePtr) Error("getProperty()", "Failed to retrieve property %s ", prop_name.c_str());
        else prop = (*HandlePtr);
        return prop;
    }

    //###########################################################
    //                       SFBranches
    //###########################################################
    SFBranches::SFBranches(TTree* tree) :
                m_tree(tree) {
    }
    //############################################################
    //                   TriggerSFBranches
    //############################################################
    TriggerSFBranches::TriggerSFBranches(TTree* tree, const ToolHandle<CP::IMuonTriggerScaleFactors>& Handle, const std::string& Trigger) :
                SFBranches(tree),
                m_handle(Handle),
                m_trigger(Trigger),
                m_nominal_SF(1.),
                m_stat_up_SF(1.),
                m_stat_down_SF(1.),
                m_sys_up_SF(1.),
                m_sys_down_SF(1.) {
    }
    CP::CorrectionCode TriggerSFBranches::fill(const xAOD::MuonContainer* muons) {
        if (getSF(muons, m_nominal_SF, CP::SystematicVariation("", 0)) == CP::CorrectionCode::Error) return CP::CorrectionCode::Error;
        if (getSF(muons, m_stat_up_SF, CP::SystematicVariation("MUON_EFF_TrigStatUncertainty", +1)) == CP::CorrectionCode::Error) return CP::CorrectionCode::Error;
        if (getSF(muons, m_stat_down_SF, CP::SystematicVariation("MUON_EFF_TrigStatUncertainty", -1)) == CP::CorrectionCode::Error) return CP::CorrectionCode::Error;
        if (getSF(muons, m_sys_up_SF, CP::SystematicVariation("MUON_EFF_TrigSystUncertainty", +1)) == CP::CorrectionCode::Error) return CP::CorrectionCode::Error;
        if (getSF(muons, m_sys_down_SF, CP::SystematicVariation("MUON_EFF_TrigSystUncertainty", -1)) == CP::CorrectionCode::Error) return CP::CorrectionCode::Error;
        return CP::CorrectionCode::Ok;
    }
    bool TriggerSFBranches::init() {
        if (!initBranch(m_nominal_SF, "")) return false;
        if (!initBranch(m_stat_up_SF, "STAT_UP")) return false;
        if (!initBranch(m_stat_down_SF, "STAT_DOWN")) return false;
        if (!initBranch(m_sys_up_SF, "SYS_UP")) return false;
        if (!initBranch(m_sys_down_SF, "SYS_DOWN")) return false;
        return true;
    }
    std::string TriggerSFBranches::name() const {
        return m_trigger;
    }
    CP::CorrectionCode TriggerSFBranches::getSF(const xAOD::MuonContainer* muons, double &Var, const CP::SystematicVariation &syst) {
        if (muons->empty()) {
            return CP::CorrectionCode::Ok;
        }
        CP::SystematicSet syst_set;
        syst_set.insert(syst);
        if (m_handle->applySystematicVariation(syst_set) != StatusCode::SUCCESS) {
            return CP::CorrectionCode::Error;
        }
        return m_handle->getTriggerScaleFactor(*muons, Var, name());
    }
    //############################################################
    //                   MuonEffiBranches
    //############################################################
    MuonEffiBranches::MuonEffiBranches(TTree* tree) :
                SFBranches(tree) {
    }
    //############################################################
    //                   MuonSFBranches
    //############################################################
    MuonSFBranches::MuonSFBranches(TTree* tree, const ToolHandle<CP::IMuonEfficiencyScaleFactors> &handle, const std::string& rel_name) :
                MuonEffiBranches(tree),
                m_handle(handle),
                m_release(rel_name),
                m_SFs()
    {
      auto mesf = dynamic_cast<const CP::MuonEfficiencyScaleFactors*>(handle.operator->());
      if (!mesf) std::abort();
      m_uncorrelate_sys = mesf->uncorrelate_sys();
    }
    CP::CorrectionCode MuonSFBranches::fill(const xAOD::Muon& muon) {
        /// Only the raw systematic sets have been activated
        /// We can loop over each set and fill it properly        
        if (!m_uncorrelate_sys){
            for (auto& Syst_SF : m_SFs) {            
                CP::CorrectionCode cc =  fill_systematic(muon,Syst_SF);
                if (cc == CP::CorrectionCode::Error) return cc;
            }
        }  else {            
            int bin =  m_handle->getUnCorrelatedSystBin(muon);
            if (bin < 0){
                Warning("MuonSFBranches()", "Did not find a valid bin for muon  with pT: %.2f GeV, eta: %.2f, phi: %2.f",
                        muon.pt(), muon.eta(),muon.phi());
                return CP::CorrectionCode::OutOfValidityRange;
            }        
            for (auto& Syst_SF: m_SFs){
                /// Only process the nominal set or the set matching the syst bin
                bool process = false;
                if (Syst_SF.first.name().empty()) process =true;
                else {                    
                     for (std::set<CP::SystematicVariation>::const_iterator t = Syst_SF.first.begin(); t != Syst_SF.first.end(); ++t) {
                        if ((*t).isToyVariation()) {
                            std::pair<unsigned, float> pair = (*t).getToyVariation();
                            if (pair.first == (unsigned) bin){
                                process = true;
                                break;
                            }
                        }
                    }
                }
                if (process) {
                    CP::CorrectionCode cc = fill_systematic(muon, Syst_SF);
                    if (cc == CP::CorrectionCode::Error) return cc;
                } else {
                    /// Assign a dummy value
                    Syst_SF.second.scale_factor = Syst_SF.second.mc_eff = Syst_SF.second.data_eff = -1.; 
                }                
            }           
        } 
        return CP::CorrectionCode::Ok;
    }
    CP::CorrectionCode MuonSFBranches::fill_systematic(const xAOD::Muon muon, std::pair<const CP::SystematicSet, MuonSFBranches::SFSet>&  Syst_SF){
        if (m_handle->applySystematicVariation(Syst_SF.first) != StatusCode::SUCCESS) {
            Error("MuonSFBranches()", "Failed to apply variation %s for %s", Syst_SF.first.name().c_str(), name().c_str());
            return CP::CorrectionCode::Error;
        }            
        CP::CorrectionCode cc = m_handle->getEfficiencyScaleFactor(muon, Syst_SF.second.scale_factor);
        if (cc == CP::CorrectionCode::Error) {
            Error("MuonSFBranches()", "Failed to retrieve %s scale-factor for variation %s", name().c_str(), Syst_SF.first.name().c_str());
            return CP::CorrectionCode::Error;
        } 
       
        /// No data-mc efficiencies provided for eta beyond 2.5
        if (std::fabs(muon.eta()) > 2.5) {
            Syst_SF.second.data_eff = -1;
            Syst_SF.second.mc_eff = -1;
            return CP::CorrectionCode::Ok;
        }            
        cc = m_handle->getDataEfficiency(muon, Syst_SF.second.data_eff);
        if (cc == CP::CorrectionCode::Error) {
             Error("MuonSFBranches()", "Failed to retrieve %s data efficiency for variation %s", name().c_str(), Syst_SF.first.name().c_str());
            return CP::CorrectionCode::Error;
        }            
        cc = m_handle->getMCEfficiency(muon, Syst_SF.second.mc_eff);
        if (cc == CP::CorrectionCode::Error) {
            Error("MuonSFBranches()", "Failed to retrieve %s mc efficiency for variation %s", name().c_str(), Syst_SF.first.name().c_str());
            return CP::CorrectionCode::Error;
        }
        return CP::CorrectionCode::Ok;        
    }
    
    std::string MuonSFBranches::name() const {
        return m_release + (m_release.empty() ? "" : "_") + getProperty<std::string>(m_handle.operator->(), "WorkingPoint");
    }
    bool MuonSFBranches::init() {
        for (auto set : CP::make_systematics_vector(m_handle->recommendedSystematics())) {
            std::map<CP::SystematicSet, SFSet>::iterator Itr = m_SFs.find(set);
            if (Itr == m_SFs.end()) {
                m_SFs.insert(std::pair<CP::SystematicSet, SFSet>(set, SFSet()));
                Itr = m_SFs.find(set);
                if (!AddToTree(set, Itr->second)) return false;
            }
        }
        return true;
    }
    bool MuonSFBranches::AddToTree(const CP::SystematicSet& syst, MuonSFBranches::SFSet& ScaleFactor) {
        std::string SystName = (syst.name().empty() ? std::string("") : std::string("__")) + syst.name();
        if (!initBranch(ScaleFactor.scale_factor, std::string("SF") + SystName)) return false;
        if (!initBranch(ScaleFactor.data_eff, std::string("DataEff") + SystName)) return false;
        if (!initBranch(ScaleFactor.mc_eff, std::string("MCEff") + SystName)) return false;
        return true;
    }
    //############################################################
    //                   MuonReplicaBranches
    //############################################################
    MuonReplicaBranches::MuonReplicaBranches(TTree* tree, const ToolHandle<CP::IMuonEfficiencyScaleFactors> &handle, const std::string& rel_name) :
                MuonEffiBranches(tree),
                m_handle(handle),
                m_release(rel_name),
                m_SFs() {
    }
    CP::CorrectionCode MuonReplicaBranches::fill(const xAOD::Muon& muon) {
        for (auto& Syst_SF : m_SFs) {
            if (m_handle->applySystematicVariation(Syst_SF.first) != StatusCode::SUCCESS) {
                return CP::CorrectionCode::Error;
            }
            CP::CorrectionCode cc = m_handle->getEfficiencyScaleFactorReplicas(muon, Syst_SF.second);
            if (cc == CP::CorrectionCode::Error) return CP::CorrectionCode::Error;
        }
        return CP::CorrectionCode::Ok;
    }
    std::string MuonReplicaBranches::name() const {
        return m_release + (m_release.empty() ? "" : "_") + getProperty<std::string>(m_handle.operator->(), "WorkingPoint");
    }
    bool MuonReplicaBranches::init() {
        for (auto set : CP::make_systematics_vector(m_handle->recommendedSystematics())) {
            std::map<CP::SystematicSet, std::vector<float>>::iterator Itr = m_SFs.find(set);
            if (Itr == m_SFs.end()) {
                m_SFs.insert(std::pair<CP::SystematicSet, std::vector<float> >(set, std::vector<float>()));
                Itr = m_SFs.find(set);
                std::string SystName = (set.name().empty() ? std::string("") : std::string("__")) + set.name();
                if (!initBranch(Itr->second, std::string("SFReplica") + SystName)) return false;
            }
        }
        return true;
    }
    //###########################################################
    //                  MuonInfoBranches
    //###########################################################
    MuonInfoBranches::MuonInfoBranches(TTree* tree, const ToolHandle<CP::IMuonSelectionTool>& sel_tool) :
                MuonEffiBranches(tree),
                m_selection_tool(sel_tool),
                m_pt(FLT_MAX),
                m_eta(FLT_MAX),
                m_phi(FLT_MAX),
                m_quality(-1),
                m_author(-1),
                m_type(-1),
                m_passLowPt(false),
                m_passHighPt(false),
                m_precLayers(0){
    }
    bool MuonInfoBranches::init() {
        if (!initBranch(m_pt, "pt")) return false;
        if (!initBranch(m_eta, "eta")) return false;
        if (!initBranch(m_phi, "phi")) return false;
        if (!initBranch(m_quality, "quality")) return false;
        if (!initBranch(m_author, "author")) return false;
        if (!m_selection_tool.isSet()) return true;
        if (!initBranch(m_passLowPt, "isLowPt")) return false;
        if (!initBranch(m_passHighPt, "isHighPt")) return false;
        if (!initBranch(m_precLayers, "PrecisionLayers")) return false;
        
       
        return true;
    }
    std::string MuonInfoBranches::name() const {
        return "Muon";
    }
    CP::CorrectionCode MuonInfoBranches::fill(const xAOD::Muon& muon) {
        m_pt = muon.pt();
        m_eta = muon.eta();
        m_phi = muon.phi();
        m_author = muon.author();
        m_type = muon.type();
        if (!muon.summaryValue(m_precLayers, xAOD::SummaryType::numberOfPrecisionLayers)){
            Error("MuonInfoBranches()", "Failed to retrieve the precision layers");
            return CP::CorrectionCode::Error;
        }
        if (m_selection_tool.isSet()){
            m_quality = m_selection_tool->getQuality(muon);
            m_passLowPt = m_selection_tool->passedLowPtEfficiencyCuts(muon);
            m_passHighPt = m_selection_tool->passedHighPtCuts(muon);
        } else m_quality = muon.quality();
        return CP::CorrectionCode::Ok;
    }

    //###########################################################
    //                  MuonSFTestHelper
    //###########################################################
    MuonSFTestHelper::MuonSFTestHelper(const std::string& release_name, bool HasOwnerShip) :
                m_name(release_name),
                m_tree(),
                m_tree_raw_ptr(new TTree("MuonEfficiencyTest", "MuonEfficiencyTest")),
                m_Branches(),
                m_sel_tool(){
        if (release_name.find("/") != std::string::npos) m_name = "c" + release_name.substr(release_name.rfind("/") + 1, m_name.size()); // branches cannot start with number
        m_Branches.push_back(std::make_unique<TestMuonSF::MuonInfoBranches>(tree(),m_sel_tool));
        if (HasOwnerShip) m_tree = std::shared_ptr < TTree > (m_tree_raw_ptr);
    }
    MuonSFTestHelper::MuonSFTestHelper(std::shared_ptr<TTree> tree, const std::string& release_name) :
                m_name(release_name),
                m_tree(tree),
                m_tree_raw_ptr(tree.get()),
                m_Branches(),
                m_sel_tool(){
        if (release_name.find("/") != std::string::npos) m_name = "c" + release_name.substr(release_name.rfind("/") + 1, m_name.size()); // branches cannot start with number
    }
    MuonSFTestHelper::MuonSFTestHelper(TTree* tree, const std::string& release_name) :
                m_name(release_name),
                m_tree(),
                m_tree_raw_ptr(tree),
                m_Branches(),
                m_sel_tool(){
        if (release_name.find("/") != std::string::npos) m_name = "c" + release_name.substr(release_name.rfind("/") + 1, m_name.size()); // branches cannot start with number
    }
    void MuonSFTestHelper::addTool(const asg::AnaToolHandle<CP::IMuonEfficiencyScaleFactors> &handle) {
        addTool(handle.getHandle());
    }
    void MuonSFTestHelper::addTool(const ToolHandle<CP::IMuonEfficiencyScaleFactors>& handle) {
        m_Branches.push_back(std::make_unique<TestMuonSF::MuonSFBranches>(tree(), handle, m_name));
    }
    void MuonSFTestHelper::addReplicaTool(const asg::AnaToolHandle<CP::IMuonEfficiencyScaleFactors> &handle) {
        addReplicaTool(handle.getHandle());
    }
    void MuonSFTestHelper::addReplicaTool(const ToolHandle<CP::IMuonEfficiencyScaleFactors>& handle) {
        m_Branches.push_back(std::make_unique<TestMuonSF::MuonReplicaBranches>(tree(), handle, m_name));
    }
    void MuonSFTestHelper::setSelectionTool(const asg::AnaToolHandle<CP::IMuonSelectionTool> & sel_tool){
        setSelectionTool(sel_tool.getHandle());
    }
    void MuonSFTestHelper::setSelectionTool(const ToolHandle<CP::IMuonSelectionTool> & sel_tool){
       m_sel_tool = sel_tool;    
    }
          
    bool MuonSFTestHelper::init() {
        if (m_Branches.empty()) {
            Error("init()", "No scale-factors have been defined thus far");
            return false;
        }
        for (auto& BR : m_Branches) {
            if (!BR->init()) return false;
        }
        return true;
    }
    CP::CorrectionCode MuonSFTestHelper::fill(const xAOD::Muon* mu) {
        return fill(*mu);
    }
    CP::CorrectionCode MuonSFTestHelper::fill(const xAOD::Muon& mu) {
        for (auto& br : m_Branches) {
            if (br->fill(mu) == CP::CorrectionCode::Error) {return CP::CorrectionCode::Error;}
        }
        return CP::CorrectionCode::Ok;
    }
    CP::CorrectionCode MuonSFTestHelper::fill(const xAOD::MuonContainer* muons) {
        for (const auto mu : *muons) {
            if (fill(mu) == CP::CorrectionCode::Error) return CP::CorrectionCode::Error;
            fillTree();
        }
        return CP::CorrectionCode::Ok;
    }
    TTree* MuonSFTestHelper::tree() const {
        return m_tree_raw_ptr;
    }
    std::shared_ptr<TTree> MuonSFTestHelper::tree_shared() const {
        return m_tree;
    }
    void MuonSFTestHelper::fillTree() {
        tree()->Fill();
=======

    void WriteHistogram(TFile* File, TH1* &Histo) {
        if (!Histo) {
            return;
        }
        File->cd();
        Histo->Write();
        delete Histo;
        Histo = NULL;
    }

    MuonSFTestHelper::MuonSFTestHelper(const std::string& Name) :
                    m_Tool(Name),
                    m_Nominal(),
                    m_Syst(),
                    m_Histos(),
                    m_init(false),
                    m_Print(false),
                    m_FillHistos(true) {

    }
    void MuonSFTestHelper::PrintSFs(bool B) {
        m_Print = B;
    }
    void MuonSFTestHelper::FillHistos(bool B) {
        m_FillHistos = B;
    }

    StatusCode MuonSFTestHelper::initialize() {
        if (m_init) {
            return StatusCode::SUCCESS;
        }
        if (!m_Tool.setProperty("ScaleFactorDecorationName", m_Tool.name()).isSuccess()) {
            Error("MuonSFTestHelper", "Failed to set ScaleFactorDecorationName property of the tool %s", m_Tool.name().c_str());
            return StatusCode::FAILURE;
        }
        if (!m_Tool.initialize().isSuccess()) {
            Error("MuonSFTestHelper", "Failed to initialize the tool %s", m_Tool.name().c_str());
            return StatusCode::FAILURE;
        }
        //Now fill the systematic vectors
        for (const auto& set : CP::make_systematics_vector(m_Tool.recommendedSystematics())) {
            if (set.name().empty()) continue;
            m_Syst.push_back(new CP::SystematicSet(set));
        }
        m_Syst.push_back(new CP::SystematicSet(m_Nominal));
        for (auto Sys : m_Syst) {
            CreateHistogram(*Sys);
        }
        m_init = true;
        return StatusCode::SUCCESS;
    }
    void MuonSFTestHelper::CreateHistogram(CP::SystematicSet& Set) {
        TestScaleFactors Histo;
        Histo.SF = NULL;
        Histo.eff = NULL;
        Histo.mceff = NULL;
        Histo.relSystSF = NULL;
        Histo.relSystEff = NULL;
        Histo.relSystmcEff = NULL;
        if (m_FillHistos) {
            unsigned int nBins = 100000;
            Histo.SF = new TH1D(("SF_" + m_Tool.name() + Set.name()).c_str(), "", nBins, 0., 2.1);
            Histo.SF->GetXaxis()->SetTitle("SF value");
            Histo.eff = new TH1D(("DataEff_" + m_Tool.name() + Set.name()).c_str(), "", nBins, 0., 1.1);
            Histo.eff->GetXaxis()->SetTitle("Data efficiency value");
            Histo.mceff = new TH1D(("MCEff_" + m_Tool.name() + Set.name()).c_str(), "", nBins, 0., 1.1);
            Histo.mceff->GetXaxis()->SetTitle("MC efficiency value");

            if (!Set.name().empty()) {
                Histo.relSystSF = new TH1D(("relSF_" + m_Tool.name() + Set.name()).c_str(), "", nBins, 0., 1.1);
                Histo.relSystEff = new TH1D(("relDataEff_" + m_Tool.name() + Set.name()).c_str(), "", nBins, 0., 1.1);
                Histo.relSystmcEff = new TH1D(("relMCEff_" + m_Tool.name() + Set.name()).c_str(), "", nBins, 0., 1.1);
                std::string xTitle = Set.name().find("STAT") != std::string::npos ? "Statistical" : "Systematic";
                Histo.relSystSF->GetXaxis()->SetTitle(Form("%s error of the SF", xTitle.c_str()));
                Histo.relSystEff->GetXaxis()->SetTitle(Form("%s error of the data efficiency", xTitle.c_str()));
                Histo.relSystmcEff->GetXaxis()->SetTitle(Form("%s error of the MC efficiency", xTitle.c_str()));
            }
        }
        m_Histos.insert(std::pair<CP::SystematicSet*, TestScaleFactors>(&Set, Histo));

    }
    CP::SystematicCode MuonSFTestHelper::GetMCEfficiency(float &eff, const CP::SystematicSet &Set) const {
        for (const auto& Tester : m_Histos) {
            if (Tester.first->name() == Set.name()) {
                eff = Tester.second.mcefficiency_Value;
                return CP::SystematicCode::Ok;
            }
        }
        return CP::SystematicCode::Unsupported;
    }
    CP::SystematicCode MuonSFTestHelper::GetEfficiency(float &eff, const CP::SystematicSet &Set) const {
        for (const auto& Tester : m_Histos) {
            if (Tester.first->name() == Set.name()) {
                eff = Tester.second.efficiency_Value;
                return CP::SystematicCode::Ok;
            }
        }
        return CP::SystematicCode::Unsupported;

    }
    CP::SystematicCode MuonSFTestHelper::GetScaleFactor(float &eff, const CP::SystematicSet &Set) const {
        for (const auto& Tester : m_Histos) {
            if (Tester.first->name() == Set.name()) {
                eff = Tester.second.sf_Value;
                return CP::SystematicCode::Ok;
            }
        }
        return CP::SystematicCode::Unsupported;

    }

    CP::CorrectionCode MuonSFTestHelper::TestSF(const xAOD::Muon& mu) {
        for (auto& Sys : m_Syst) {
            if (!m_Tool.applySystematicVariation(*Sys)) {
                return CP::CorrectionCode::Error;
            }
            float sf(0), eff(0.), mcEff(0.);
            CHECK_CPCorr(m_Tool.getEfficiencyScaleFactor(mu, sf));
            // Do not try to retrieve reconstruction efficiencies for high-eta muons
            if ((m_Tool.name().find("Reco") == std::string::npos) || fabs(mu.eta()) <= 2.5) {
                CHECK_CPCorr(m_Tool.getDataEfficiency(mu, eff));
                CHECK_CPCorr(m_Tool.getMCEfficiency(mu, mcEff));
            }
            std::string SystName = Sys->name().empty() ? "Nominal" : Sys->name();
            // Info(m_Tool.name().c_str(), " Calculated SF: %f, mcEff: %f and eff: %f for systematic %s using muon with pt: %.3f, eta: %.2f, phi : %.3f", sf, mcEff, eff, SystName.c_str(), mu.pt() / 1.e3, mu.eta(), mu.phi());
            TestScaleFactors& Histos = m_Histos.at(Sys);
            Histos.sf_Value = sf;
            Histos.efficiency_Value = eff;
            Histos.mcefficiency_Value = mcEff;

            if (m_FillHistos) {
                Histos.SF->Fill(sf);
                Histos.eff->Fill(eff);
                Histos.mceff->Fill(mcEff);
                if (!Sys->name().empty()) {
                    float CentralSF(0.), CentralEff(0.), CentralmcEff(0.);
                    if (!m_Tool.applySystematicVariation(m_Nominal)) {
                        return CP::CorrectionCode::Error;
                    }
                    CHECK_CPCorr(m_Tool.getEfficiencyScaleFactor(mu, CentralSF));
                    // Do not try to retrieve reconstruction efficiencies for high-eta muons
                    if ((m_Tool.name().find("Reco") == std::string::npos) || fabs(mu.eta()) <= 2.5) {
                        CHECK_CPCorr(m_Tool.getDataEfficiency(mu, CentralEff));
                        CHECK_CPCorr(m_Tool.getMCEfficiency(mu, CentralmcEff));
                    }
                    float relSystSF = fabs(CentralSF - sf);
                    float relSystEff = fabs(CentralEff - eff);
                    float relSystmcEff = fabs(CentralmcEff - eff);
                    if (relSystSF > 1.) Warning(m_Tool.name().c_str(), "SF %f has for systematic %s a relative uncertainty of %.2f", CentralSF, Sys->name().c_str(), relSystSF / CentralSF);
                    if (relSystEff > 1.) Warning(m_Tool.name().c_str(), "Data Efficiency %f has for systematic %s a relative uncertainty of %.2f", CentralEff, Sys->name().c_str(), relSystEff / CentralEff);
                    if (relSystmcEff > 1.) Warning(m_Tool.name().c_str(), "MC Efficiency %f has for systematic %s a relative uncertainty of %.2f", CentralmcEff, Sys->name().c_str(), relSystmcEff / CentralmcEff);
                    Histos.relSystSF->Fill(relSystSF);
                    Histos.relSystEff->Fill(relSystEff);
                    Histos.relSystmcEff->Fill(relSystmcEff);
                }
            }
        }
        return CP::CorrectionCode::Ok;
    }
    MuonSFTestHelper::~MuonSFTestHelper() {
        for (auto& Histo : m_Histos) {
            if (Histo.second.SF) delete Histo.second.SF;
            if (Histo.second.eff) delete Histo.second.eff;
            if (Histo.second.mceff) delete Histo.second.mceff;
            if (Histo.second.relSystSF) delete Histo.second.relSystSF;
            if (Histo.second.relSystEff) delete Histo.second.relSystEff;
            if (Histo.second.relSystmcEff) delete Histo.second.relSystmcEff;
        }
        for (auto& Sys : m_Syst) {
            delete Sys;
        }
    }
    bool MuonSFTestHelper::WriteHistosToFile(TFile* file) {
        if (!file) {
            return false;
        }
        for (auto& Histo : m_Histos) {
            WriteHistogram(file, Histo.second.SF);
            WriteHistogram(file, Histo.second.eff);
            WriteHistogram(file, Histo.second.mceff);
            WriteHistogram(file, Histo.second.relSystSF);
            WriteHistogram(file, Histo.second.relSystEff);
            WriteHistogram(file, Histo.second.relSystmcEff);
        }
        return true;
    }

    const CP::MuonEfficiencyScaleFactors& MuonSFTestHelper::GetTool() const {
        return m_Tool;
    }

    MuonSFReleaseComparer::MuonSFReleaseComparer(MuonSFTestHelper& Rel1, MuonSFTestHelper& Rel2) :
                    m_Rel1(Rel1),
                    m_Rel2(Rel2),
                    m_Histos() {

    }
    void MuonSFReleaseComparer::CreateHistogram(const CP::SystematicSet &Set) {
        unsigned int nBins = 100000;
        ComparingHistos H;
        H.SF = new TH1D(Form("CompSF_%s%s", m_Rel1.GetTool().name().c_str(), Set.name().c_str()), "", nBins, 0, 2.1);
        H.eff = new TH1D(Form("CompEff_%s%s", m_Rel1.GetTool().name().c_str(), Set.name().c_str()), "", nBins, 0, 2.1);
        m_Histos.insert(std::pair<CP::SystematicSet, ComparingHistos>(Set, H));
    }

    StatusCode MuonSFReleaseComparer::initialize() {
        if (m_Rel1.initialize().isSuccess() || m_Rel2.initialize().isSuccess()) {
            return StatusCode::FAILURE;
        }
        for (const auto& set : CP::make_systematics_vector(m_Rel1.GetTool().recommendedSystematics())) {
            if (set.name().empty()) continue;
            for (const auto& sys : set) {
                if (!m_Rel2.GetTool().isAffectedBySystematic(sys)) {
                    Error("MuonSFReleaseComprarer::initialize()", "Tool %s does not share the systematic %s provided by %s", m_Rel2.GetTool().name().c_str(), set.name().c_str(), m_Rel1.GetTool().name().c_str());
                    return StatusCode::FAILURE;
                }
            }
            CreateHistogram(set);
        }
        CreateHistogram(CP::SystematicSet());
        return StatusCode::SUCCESS;
    }
    CP::CorrectionCode MuonSFReleaseComparer::TestSF(const xAOD::Muon& mu) {
        CHECK_CPCorr(m_Rel1.TestSF(mu));
        CHECK_CPCorr(m_Rel2.TestSF(mu));
        for (auto& SystHistos : m_Histos) {
            float sfRel1(0.), sfRel2(0.), effRel1(0.), effRel2(0.);
            m_Rel1.GetScaleFactor(sfRel1, SystHistos.first);
            m_Rel2.GetScaleFactor(sfRel2, SystHistos.first);
            m_Rel1.GetEfficiency(effRel1, SystHistos.first);
            m_Rel2.GetEfficiency(effRel2, SystHistos.first);
            SystHistos.second.SF->Fill(fabs(sfRel1 - sfRel2));
            SystHistos.second.eff->Fill(fabs(effRel1 - effRel2));
        }
        return CP::CorrectionCode::Ok;
    }
    bool MuonSFReleaseComparer::WriteHistosToFile(TFile* file) {
        if (!m_Rel1.WriteHistosToFile(file) || !m_Rel2.WriteHistosToFile(file)) {
            return false;
        }
        for (auto& Histo : m_Histos) {
            WriteHistogram(file, Histo.second.SF);
            WriteHistogram(file, Histo.second.eff);
        }
        return true;
    }
    MuonSFReleaseComparer::~MuonSFReleaseComparer() {
        for (auto& Histo : m_Histos) {
            if (Histo.second.SF) delete Histo.second.SF;
            if (Histo.second.eff) delete Histo.second.eff;
        }
>>>>>>> release/21.0.127
    }

}

