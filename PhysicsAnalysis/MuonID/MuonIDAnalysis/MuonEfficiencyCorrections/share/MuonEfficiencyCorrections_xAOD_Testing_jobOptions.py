<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

#!/usr/bin/env python
import sys
from MuonEfficiencyCorrections.CommonToolSetup import *

=======
# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#!/usr/bin/env python
import sys
# a simple testing macro for the MuonEfficiencyCorrections_xAOD package in athena
#
# Usage: athena -c "FNAME='<input file>'" MuonEfficiencyCorrections_xAOD_Testing_jobOptions.py

if not "FNAME" in vars() and not "FNAME" in globals():
    print 'Usage: athena -c "FNAME=\'<input file>\'" MuonEfficiencyCorrections/MuonEfficiencyCorrections_xAOD_Testing_jobOptions.py'
    sys.exit(1)

import AthenaPoolCnvSvc.ReadAthenaPool
ServiceMgr.EventSelector.InputCollections = [ FNAME ]
>>>>>>> release/21.0.127

# a simple testing macro for the MuonEfficiencyCorrections_xAOD package in athena
#
# Usage: athena --filesInput <InputFile> MuonEfficiencyCorrections/MuonEfficiencyCorrections_xAOD_Testing_jobOptions.py
#  E.g.:  athena  --filesInput '/ptmp/mpp/junggjo9/Datasets/mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.deriv.DAOD_MUON1.*/*root.1' MuonEfficiencyCorrections/MuonEfficiencyCorrections_xAOD_Testing_jobOptions.py
# Access the algorithm sequence:
AssembleIO("MUONEFFTESTER")
from AthenaCommon.AlgSequence import AlgSequence
theJob = AlgSequence()

<<<<<<< HEAD
from MuonMomentumCorrections.MuonMomentumCorrectionsConf import CP__CalibratedMuonsProvider, CP__MuonCalibrationPeriodTool
toolName = "MuonCalibrationTool"
calibTool = CP__MuonCalibrationPeriodTool(toolName)
ToolSvc += calibTool

theJob += CP__CalibratedMuonsProvider("CalibratedMuonsProvider",Tool=calibTool, prwTool = GetPRWTool())

# Add the test algorithm:
from MuonEfficiencyCorrections.MuonEfficiencyCorrectionsConf import CP__MuonEfficiencyCorrections_TestAlg, CP__MuonCloseJetDecorationAlg
theJob += CP__MuonCloseJetDecorationAlg("JetDecorationAlg",
                                        MuonContainer = "CalibratedMuons")

alg = CP__MuonEfficiencyCorrections_TestAlg("EffiTestAlg")
alg.PileupReweightingTool = GetPRWTool()
alg.MuonSelectionTool = GetSelectionTool()
alg.DefaultRelease="cJan_2020"
alg.ValidationRelease="cApr_2020"
alg.SGKey = "CalibratedMuons"
## Select 30 GeV muons for the high-pt WP only
alg.MinPt = 3.e3
alg.MaxEta = 2.5
WPs = [
        # reconstruction WPs
        "LowPt",
        "Loose", 
        "Medium", 
        "Tight", 
        "HighPt", 
        "CaloTag",  
        "HighPt3Layers", 
        "LowPtMVA",    
        # track-to-vertex-association WPs
        "TTVA",
        # BadMuon veto SFs
        "BadMuonVeto_HighPt",            
        # isolation WPs
        "FCLooseIso",
        "FCTightIso",              
        "FCTightTrackOnlyIso",              
        "FCLoose_FixedRadIso",        
        "FCTight_FixedRadIso",
        "FCTightTrackOnly_FixedRadIso",
        "FCTight_FixedRadIso",           
        "FixedCutHighPtTrackOnlyIso",
        "FixedCutPflowLooseIso",            
        "FixedCutPflowTightIso",
       ]
for WP in WPs: 
    alg.EfficiencyTools += [GetMuonEfficiencyTool(WP,
                                                 Release="200202_Precision_r21",
                                                 BreakDownSystematics=False, 
                                                 UncorrelateSystematics=False)]
    alg.EfficiencyToolsForComparison += [GetMuonEfficiencyTool(WP, 
                                                CustomInput = "/ptmp/mpp/junggjo9/Cluster/SFFiles/Feb_2020_iso/",
                                                BreakDownSystematics=False, 
                                                UncorrelateSystematics=False)]

try: ToolSvc.MuonEfficiencyTool_CaloTag.ApplyKinematicSystematic = False
except: pass
try: ToolSvc.MuonEfficiencyTool_CaloTag_200202_Precision_r21.ApplyKinematicSystematic = False
except: pass

=======
#the ToolHandle constructor should be given "CP::PileupReweightingTool/myTool" as its string argument
PRWTool = CfgMgr.CP__PileupReweightingTool("MyPRWTool",
                DataScaleFactor=1.0/1.09,
                DataScaleFactorUP=1., 
                DataScaleFactorDOWN=1.0/1.18,
                ConfigFiles=["/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/mc15c_v2_defaults.NotRecommended.prw.root"],
                LumiCalcFiles=["/afs/cern.ch/atlas/project/muon/mcp/PRWFiles/ilumicalc_histograms_data15_13TeV.periodAllYear_DetStatus-v79-repro20-02_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.root", "/afs/cern.ch/atlas/project/muon/mcp/PRWFiles/ilumicalc_histograms_data16_13TeV.periodAllYear_DetStatus-v83-pro20-15_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.root"])
ToolSvc += PRWTool

CustomInputDir = ""

# Add the MCP tool
from MuonEfficiencyCorrections.MuonEfficiencyCorrectionsConf import CP__MuonEfficiencyScaleFactors
tool = CP__MuonEfficiencyScaleFactors("MyMCPTool")
tool.WorkingPoint = "Loose"
if CustomInputDir != "":
    tool.CustomInputFolder=CustomInputDir
ToolSvc += tool

# Add an MCP tool for TTVA SFs
from MuonEfficiencyCorrections.MuonEfficiencyCorrectionsConf import CP__MuonEfficiencyScaleFactors
TTVAtool = CP__MuonEfficiencyScaleFactors("MyMCPTTVATool")
TTVAtool.WorkingPoint = "TTVA"
if CustomInputDir != "":
    TTVAtool.CustomInputFolder=CustomInputDir
ToolSvc += TTVAtool

# Add Isolaiton tool
isotool = CP__MuonEfficiencyScaleFactors("TestIsolationSF")
isotool.WorkingPoint = "GradientLooseIso"
isotool.OutputLevel = VERBOSE
if CustomInputDir != "":
    isotool.CustomInputFolder=CustomInputDir
ToolSvc += isotool

from MuonEfficiencyCorrections.MuonEfficiencyCorrectionsConf import CP__MuonTriggerScaleFactors
trigsftool = CP__MuonTriggerScaleFactors("TriggerSFTool");
trigsftool.MuonQuality = "Medium"
ToolSvc += trigsftool

# Add the test algorithm:
from MuonEfficiencyCorrections.MuonEfficiencyCorrectionsConf import CP__MuonEfficiencyCorrections_TestAlg
alg = CP__MuonEfficiencyCorrections_TestAlg()
alg.ScaleFactorTool = tool
alg.IsolationScaleFactorTool = isotool
alg.TrigScaleFactorTool = trigsftool
alg.TTVAScaleFactorTool = TTVAtool
alg.PileupReweightingTool = PRWTool
alg.OutputLevel = DEBUG
>>>>>>> release/21.0.127
theJob += alg

# Do some additional tweaking:
from AthenaCommon.AppMgr import theApp
#theApp.EvtMax = 200

ServiceMgr.MessageSvc.OutputLevel = INFO
#ServiceMgr.MessageSvc.defaultLimit = 100
