<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
################################################################################
# Package: POOLRootAccess
################################################################################
>>>>>>> release/21.0.127

# Declare the package name:
atlas_subdir( POOLRootAccess )

<<<<<<< HEAD
=======
# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          Control/AthenaKernel
                          Control/StoreGate
                          GaudiKernel
			  Control/StoreGateBindings
                          PRIVATE
                          Control/xAODRootAccess
			  Control/AthAnalysisBaseComps
                          Event/xAOD/xAODBase
                          Event/xAOD/xAODEventInfo )

>>>>>>> release/21.0.127
# External dependencies:
find_package( ROOT COMPONENTS Core Tree MathCore Hist RIO pthread )

# Component(s) in the package:
<<<<<<< HEAD
atlas_add_library( POOLRootAccessLib
                   src/*.cxx
                   PUBLIC_HEADERS POOLRootAccess
                   PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                   LINK_LIBRARIES AthenaKernel GaudiKernel SGTools StoreGateLib
                   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} AthAnalysisBaseCompsLib StoreGateBindings )
=======
atlas_add_library( POOLRootAccess
                   src/*.cxx
                   PUBLIC_HEADERS POOLRootAccess
                   PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} 
		   LINK_LIBRARIES ${ROOT_LIBRARIES} xAODRootAccess AthAnalysisBaseCompsLib StoreGateLib 
                   PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} GaudiKernel SGtests StoreGateBindings  )
>>>>>>> release/21.0.127

atlas_add_dictionary( POOLRootAccessDict
                      POOLRootAccess/POOLRootAccessDict.h
                      POOLRootAccess/selection.xml
<<<<<<< HEAD
                      LINK_LIBRARIES POOLRootAccessLib )
=======
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                      LINK_LIBRARIES ${ROOT_LIBRARIES} POOLRootAccess )
>>>>>>> release/21.0.127

atlas_add_executable( ut_basicRead_test
                      test/ut_basicRead_test.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
<<<<<<< HEAD
                      LINK_LIBRARIES ${ROOT_LIBRARIES} AsgMessagingLib AthAnalysisBaseCompsLib POOLRootAccessLib xAODEventInfo xAODRootAccess )
=======
                      LINK_LIBRARIES ${ROOT_LIBRARIES} POOLRootAccess xAODEventInfo )
>>>>>>> release/21.0.127

atlas_add_executable( ut_basicxAODRead_test
                      test/ut_basicxAODRead_test.cxx
                      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
<<<<<<< HEAD
                      LINK_LIBRARIES ${ROOT_LIBRARIES} POOLRootAccessLib xAODBase xAODEventInfo xAODRootAccess )
=======
                      LINK_LIBRARIES ${ROOT_LIBRARIES} POOLRootAccess xAODEventInfo )
>>>>>>> release/21.0.127

# Install files from the package:
atlas_install_joboptions( share/*.opts )

