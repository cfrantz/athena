/*
<<<<<<< HEAD
   Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
 */

/* 
   To run with something like
   valgrind --tool=memcheck --leak-check=full --suppressions=$ROOTSYS/etc/valgrind-root.supp  --error-limit=no \
   --track-origins=yes --smc-check=all --trace-children=yes  --track-fds=yes --num-callers=30  \
   $ROOTCOREBIN/bin/x86_64-slc6-gcc49-opt/EgEfficiencyCorr_mem_check>valgrind.log 2>&1 & 

   In order to identify memory leaks in out methods 
   Look here:
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/* To run with something like
valgrind --tool=memcheck --leak-check=full --suppressions=$ROOTSYS/etc/valgrind-root.supp  --error-limit=no --track-origins=yes --smc-check=all --trace-children=yes  --track-fds=yes --num-callers=30  $ROOTCOREBIN/bin/x86_64-slc6-gcc49-opt/EgEfficiencyCorr_mem_check>valgrind.log 2>&1 & 

In order to identify memory leaks in out methods 
Look here:
>>>>>>> release/21.0.127
http://valgrind.org/docs/manual/faq.html#faq.deflost
*/

#include <string>
<<<<<<< HEAD
#include <vector>
#include "EgammaAnalysisInterfaces/IAsgElectronEfficiencyCorrectionTool.h"
#include "AsgMessaging/AsgMessaging.h"
#include "AsgTools/AnaToolHandle.h"
#ifdef XAOD_STANDALONE
// xAOD include(s):
#   include "xAODRootAccess/TEvent.h"
#endif // XAOD_STANDALONE
#include "CxxUtils/ubsan_suppress.h"
#include "TInterpreter.h"


int main( ) {

    // Suppress known ubsan warning we get from cling.
    CxxUtils::ubsan_suppress ([]() { TInterpreter::Instance(); });
    
#ifdef XAOD_STANDALONE
    xAOD::TEvent event;
#endif

    using namespace asg::msgUserCode;
    ANA_CHECK_SET_TYPE (int);

    asg::AnaToolHandle<IAsgElectronEfficiencyCorrectionTool>  tool ("AsgElectronEfficiencyCorrectionTool/ElectronEffCorrection");
    ANA_CHECK(tool.setProperty("CorrelationModel", "FULL" ) &&
            tool.setProperty("ForceDataType",1) &&
            tool.setProperty("IdKey", "Medium") &&
            tool.retrieve());

    asg::AnaToolHandle<IAsgElectronEfficiencyCorrectionTool> eccTool;
    eccTool.setTypeAndName("CP::ElectronChargeEfficiencyCorrectionTool/ElectronChargeCorrection");
    ANA_CHECK(eccTool.setProperty( "CorrectionFileName", 
                "ElectronEfficiencyCorrection/2015_2016/rel20.7/Moriond_February2017_v1/charge_misID/ChargeCorrectionSF.Medium_FixedCutTight.root" )&&
            eccTool.retrieve());

    return 0;
=======
#include <iostream>
#include <vector>
#include "ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h"
#include "AsgTools/AsgMessaging.h"

int main( ) {

  std::vector<std::string> inputFiles ={
    "ElectronEfficiencyCorrection/2015_2016/rel20.7/ICHEP_June2016_v1/offline/efficiencySF.offline.LooseAndBLayerLLH_d0z0_v11.2015_2016.13TeV.rel20.7.25ns.v01.root"} ;

  AsgElectronEfficiencyCorrectionTool myEgCorrections ("myEgCorrections");
  myEgCorrections.msg().setLevel(MSG::INFO);
  if (myEgCorrections.setProperty("CorrelationModel", "FULL" ) &&
      myEgCorrections.setProperty("ForceDataType",1) &&
      myEgCorrections.setProperty("CorrectionFileNameList",inputFiles) &&
      myEgCorrections.initialize())
    {
      std::cout<<"ALL FINE" <<std::endl;
      
    }

  
  return 0;
>>>>>>> release/21.0.127
}
