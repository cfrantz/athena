/*
   Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
 */

/**
  @class TElectronEfficiencyCorrectionTool
  @brief Calculate the egamma scale factors in pure ROOT
  @author Karsten Koeneke, Felix Buehrer, Kristin Lohwasser
  @date   July 2012
  */

// This class header
#include "ElectronEfficiencyCorrection/TElectronEfficiencyCorrectionTool.h"
// STL includes
#include <iostream>
<<<<<<< HEAD
#include <cmath>
#include <memory>
=======
#include <cfloat>
#include <math.h>
#include <limits.h>
#include "CxxUtils/make_unique.h"
#include <memory>

>>>>>>> release/21.0.127
// ROOT includes
#include "TSystem.h"
#include "TROOT.h"
#include "TFile.h"
#include "TClass.h"
#include "TKey.h"
#include "TH1.h"
#include "TH2.h"
#include "TString.h"
#include "TObjString.h"
#include "TMD5.h"

namespace mapkey{
enum key{ sf =1,
  stat=2,
  eig=3,
  uncorr=4,	      
  sys=5,
  end=6
};
const char* keytostring (int input){
  switch(input){
  case(sf) : 
    return "sf";
  case(stat) : 
    return "stat";
  case(eig) : 
    return "eig";
  case(uncorr) : 
    return "uncorr";
  case(sys) : 
    return "sys";
  }
  return "";
}
}


<<<<<<< HEAD
namespace{
const std::string LowPt_string("LowPt");
const std::vector<int> s_keys={mapkey::sf,mapkey::stat,mapkey::eig,mapkey::uncorr};
}


Root::TElectronEfficiencyCorrectionTool::TElectronEfficiencyCorrectionTool(const char *name) :
  asg::AsgMessaging(std::string(name)),
=======
// =============================================================================
// Constructor
// =============================================================================
Root::TElectronEfficiencyCorrectionTool::TElectronEfficiencyCorrectionTool(const char *name) :
  Root::TCalculatorToolBase(name),
  asg::AsgMessaging(std::string(name)),
  m_Rndm(0),
  m_randomCounter(0),
  m_isInitialized(false),
  m_detailLevel(2),
  m_toyMCSF(-1),
  m_seed(0),
>>>>>>> release/21.0.127
  m_doToyMC(false),
  m_doCombToyMC(false),
  m_nToyMC(0),
  m_seed(0),
  m_nSysMax(0),
<<<<<<< HEAD
  m_Rndm()
{
}

Root::TElectronEfficiencyCorrectionTool::~TElectronEfficiencyCorrectionTool() {

  /*
   * Make sure that all TObjArray 
   * elements are owned and deleted 
   */
  for (auto  &tempit : m_histList) {
    for (unsigned int i = 0; i < tempit.second.size(); ++i) {
      tempit.second.at(i).SetOwner(kTRUE);
    }
  }
  for (auto  &tempit : m_fastHistList) {
    for (unsigned int i = 0; i < tempit.second.size(); ++i) {
      tempit.second.at(i).SetOwner(kTRUE);
    }
  }
  for (auto  &tempit : m_sysList) {
    for (auto  &i : tempit) {
      i.SetOwner(kTRUE);
    }
  }
  for (auto  &tempit :  m_fastSysList) {
    for (auto  &i : tempit) {
      i.SetOwner(kTRUE);
    }
  }
  for (auto  &tempit : m_uncorrToyMCSystFull) {
    for (auto & i : tempit) {
      i.SetOwner(kTRUE);
    }
  }  
  for (auto  &tempit : m_uncorrToyMCSystFast) {
    for (auto & i : tempit) {
      i.SetOwner(kTRUE);
    }
  }
}

int Root::TElectronEfficiencyCorrectionTool::initialize() {
=======
  m_runNumBegin(0),
  m_runNumEnd(0),
  m_resultPrefix("efficiency_SF"),
  m_resultName(""),
  m_position_eff(0),
  m_position_err(0),
  m_position_statErr(0),
  m_position_nSys(0),
  m_position_uncorrSys(0),
  m_nbins(0),
  m_nSimpleUncorrSyst(0),
  m_position_globalBinNumber(0),
  m_runnumberIndex(-1),
  m_last_runnumber(0),
  m_last_dataType(PATCore::ParticleDataType::Full),
  m_last_hist(0),
  m_last_hists(0) {
}

// =============================================================================
// Destructor
// =============================================================================

Root::TElectronEfficiencyCorrectionTool::~TElectronEfficiencyCorrectionTool() {
  if (m_last_hist) {
    delete m_last_hist;
  }
  if (m_last_hists) {
    delete m_last_hists;
  }
  if (m_Rndm) {
    delete m_Rndm;
  }
  for (auto const &tempit : m_histList) {
    for (unsigned int i = 0; i < tempit.second.size(); ++i) {
      if (tempit.second.at(i)) {
        tempit.second.at(i)->SetOwner(kTRUE);
        delete tempit.second.at(i);
      }
    }
  }

  for (auto const &tempit : m_fastHistList) {
    for (unsigned int i = 0; i < tempit.second.size(); ++i) {
      if (tempit.second.at(i)) {
        tempit.second.at(i)->SetOwner(kTRUE);
        delete tempit.second.at(i);
      }
    }
  }
}

// =============================================================================
// Calculate the detail levels for a given eigenvector histogram
// =============================================================================
void
Root::TElectronEfficiencyCorrectionTool::calcDetailLevels(TH1D *eig) {
  m_sLevel[Root::TElectronEfficiencyCorrectionTool::simple] = 0;
  m_sLevel[Root::TElectronEfficiencyCorrectionTool::medium] = 0;
  m_sLevel[Root::TElectronEfficiencyCorrectionTool::detailed] = 0;
  int nSys = eig->GetNbinsX() - 1;
  double sign = 0;
  // Calculate detail level
  for (int i = nSys + 1; i >= 2; i--) {
    sign += eig->GetBinContent(i);
    if (sign > 0.8 && m_sLevel[Root::TElectronEfficiencyCorrectionTool::simple] == 0) {
      m_sLevel[Root::TElectronEfficiencyCorrectionTool::simple] = i - 2;
    }
    if (sign > 0.95 && m_sLevel[Root::TElectronEfficiencyCorrectionTool::medium] == 0) {
      m_sLevel[Root::TElectronEfficiencyCorrectionTool::medium] = i - 2;
    }
  }
 
    m_nSys = nSys;
}

// =============================================================================
// Build the toyMC tables from inputs
// =============================================================================
std::vector<TH2D *> *
Root::TElectronEfficiencyCorrectionTool::buildSingleToyMC(TH2D *sf, TH2D *stat, TH2D *uncorr, TObjArray *corr) {
  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ")! " << "entering function buildSingleToyMC");

  std::vector<TH2D *> *tmpHists = new std::vector<TH2D *>;
  int nBins = (stat->GetNbinsX() + 2) * (stat->GetNbinsY() + 2);
  for (int toy = 0; toy < m_nToyMC; toy++) {
    tmpHists->push_back((TH2D *) corr->At(0)->Clone());
  }
  // Loop over all bins
  for (int bin = 0; bin < nBins; bin++) {
    double val = stat->GetBinContent(bin);

    // Add uncorrelated systematics
    if (uncorr != NULL) {
      double valAdd = uncorr->GetBinContent(bin);
      val = sqrt(val * val + valAdd * valAdd);
    }

    // Add smaller correlated systematics as uncorrelated
    for (int i = 0; i < m_sLevel[m_detailLevel]; ++i) {
      if (corr->At(i) != NULL) {
        double valAdd = ((TH2D *) corr->At(i))->GetBinContent(bin);
        val = sqrt(val * val + valAdd * valAdd);
      }
    }
    for (int toy = 0; toy < m_nToyMC; toy++) {
      tmpHists->at(toy)->SetBinContent(bin, (val * m_Rndm->Gaus(0, 1)) + sf->GetBinContent(bin));
      m_randomCounter++;
      tmpHists->at(toy)->SetDirectory(0);
    }
  }

  return tmpHists;
}

// =============================================================================
// Build the combined toyMC tables from inputs
// =============================================================================
TH2D *
Root::TElectronEfficiencyCorrectionTool::buildSingleCombToyMC(TH2D *sf, TH2D *stat, TH2D *uncorr, TObjArray *corr) {
  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "entering function buildSingleCombToyMC");

  TH2D *tmpHist;
  int nBins = (stat->GetNbinsX() + 2) * (stat->GetNbinsY() + 2);
  tmpHist = (TH2D *) corr->At(0)->Clone();


  // Create random numbers for the corr. uncertainties
  double *rnd = new double[m_nSys];
  for (int s = 0; s < m_nSys; s++) {
    rnd[s] = m_Rndm->Gaus(0, 1);
    m_randomCounter++;
  }

  // Loop over all bins
  for (int bin = 0; bin < nBins; bin++) {
    double val = stat->GetBinContent(bin);


    // Add uncorrelated systematics
    if (uncorr != NULL) {
      double valAdd = uncorr->GetBinContent(bin);
      val = sqrt(val * val + valAdd * valAdd);
    }
    // Add smaller correlated systematics as uncorrelated

    for (int s = 0; s < m_sLevel[m_detailLevel]; s++) {
      if (corr->At(s) != NULL) {
        double valAdd = ((TH2D *) corr->At(s))->GetBinContent(bin);
        val = sqrt(val * val + valAdd * valAdd);
      }
    }

    val = val * m_Rndm->Gaus(0, 1);
    m_randomCounter++;

    // Add larger correlated systematics
    for (int s = m_sLevel[m_detailLevel]; s < m_nSys; s++) {
       if (corr->At(s) != NULL) {
        val += ((TH2D *) corr->At(s))->GetBinContent(bin) * rnd[s];
      }
    }
     tmpHist->SetBinContent(bin, val + sf->GetBinContent(bin));
  }

  tmpHist->SetDirectory(0);

  delete[]rnd;
  return tmpHist;
}

// =============================================================================
// Build the toyMC tables from inputs
// =============================================================================
std::vector<TObjArray *> *
Root::TElectronEfficiencyCorrectionTool::buildToyMCTable(TObjArray *sf, TObjArray *eig, TObjArray *stat,
                                                         TObjArray *uncorr, std::vector<TObjArray *> *corr) {
  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "entering function buildToyMCTable");

  std::vector<TObjArray *> *tmpVec = new std::vector<TObjArray *>;
  TObjArray *tmpArray;
  if (m_doCombToyMC) {
    for (int toyMC = 0; toyMC < m_nToyMC; toyMC++) {
      tmpArray = new TObjArray();
      for (int i = 0; i < stat->GetEntries(); ++i) {
        if (eig != NULL && uncorr != NULL) {
          calcDetailLevels((TH1D *) eig->At(i));
          tmpArray->Add(buildSingleCombToyMC((TH2D *) sf->At(i), (TH2D *) stat->At(i), (TH2D *) uncorr->At(i),
                                             corr->at(i)));
        }else {
          tmpArray->Add(buildSingleCombToyMC((TH2D *) sf->At(i), (TH2D *) stat->At(i), NULL, corr->at(i)));
        }
      }
      tmpVec->push_back(tmpArray);
    }
  }else {
    std::vector< std::vector<TH2D *> *> *tmpVec2 = new  std::vector< std::vector<TH2D *> *>;
    for (int i = 0; i < stat->GetEntries(); ++i) {
      calcDetailLevels((TH1D *) eig->At(i));
      tmpVec2->push_back(buildSingleToyMC((TH2D *) sf->At(i), (TH2D *) stat->At(i), (TH2D *) uncorr->At(i),
                                          corr->at(i)));
    }
    for (int toy = 0; toy < m_nToyMC; toy++) {
      tmpArray = new TObjArray();
      for (unsigned int i = 0; i < tmpVec2->size(); ++i) {
        tmpArray->Add(tmpVec2->at(i)->at(toy));
      }
      tmpVec->push_back(tmpArray);
    }
    delete tmpVec2;
  }

  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "m_Rndm->Uniform(0, 1) after throwing " << m_randomCounter
		<< " random numbers: " << m_Rndm->Uniform(0,
                                                          1));
  return tmpVec;
}

// =============================================================================
// Initialize this correction tool
// =============================================================================
int
Root::TElectronEfficiencyCorrectionTool::initialize() {
>>>>>>> release/21.0.127
  // use an int as a StatusCode
  int sc(1);

  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: "
<<<<<<< HEAD
                << __LINE__ << ")\n" << "Debug flag set. Printing verbose output!");

  //Check if files are present
  if (m_corrFileNameList.empty()) {
    ATH_MSG_ERROR(" No file added!");
=======
		<< __LINE__ << ") " << "Debug flag set. Printing verbose output!");

  if (m_isInitialized) {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "tool initialized twice!");
    return 0;
  }

  if (m_corrFileNameList.size() == 0) {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << " No file added!");
>>>>>>> release/21.0.127
    return 0;
  }
  ATH_MSG_DEBUG("Initializing tool with " << m_corrFileNameList.size() 
                << " configuration file(s)");

<<<<<<< HEAD
  
  if (m_doToyMC && m_doCombToyMC) {
    ATH_MSG_ERROR(" Both regular and combined toy MCs booked!" << " Only use one!");
    return 0;
  }
  /*
   * initialize the random number generator if toyMC propagation booked
   * Use the 1st 4 bytes of the CheckSum of the reccomendation file as seed
   */
  if (m_doToyMC || m_doCombToyMC) {
    if (m_seed == 0) {
    // Use the name of the correction  for auto-setting of the seed based on the md5-sum of the file
      const std::unique_ptr<char[]> fname(gSystem->ExpandPathName(m_corrFileNameList[0].c_str()));
      std::unique_ptr<TMD5> tmd=std::make_unique<TMD5>();
      const char* tmd_as_string=tmd->FileChecksum(fname.get())->AsString();
      m_seed = *(reinterpret_cast<const unsigned long int*>(tmd_as_string));
      ATH_MSG_DEBUG("Seed (automatically) set to " << m_seed);
    }else {
      ATH_MSG_DEBUG("Seed set to " << m_seed);
    }
    m_Rndm= TRandom3(m_seed);
  }
  /*
   * Load the needed histograms
   */
  if (0 == getHistograms()) {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ")\n" << "! Problem when calling getHistograms()");
    return 0;
  }
  const unsigned int nRunNumbersFull = m_begRunNumberList.size();
  const unsigned int nRunNumbersFast = m_begRunNumberListFastSim.size();

  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ")\n"
                << "Found " << nRunNumbersFast << " run number ranges for fast sim with a total of " <<
                m_fastHistList[mapkey::sf].size() << " scale factor histograms.");

  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ")\n"
                << "Found " << nRunNumbersFull << " run number ranges for full sim with a total of " <<
                m_histList[mapkey::sf].size() << " scale factor histograms.");

   ATH_MSG_DEBUG("Tool succesfully initialized!");

  return sc;
}

int 
Root::TElectronEfficiencyCorrectionTool::calculate(const PATCore::ParticleDataType::DataType dataType,
                                                   const unsigned int runnumber,
                                                   const double cluster_eta,
                                                   const double et, /* in MeV */
                                                   std::vector<double>& result,
                                                   size_t& index_of_corr,
                                                   size_t& index_of_toys) const {

  /* 
   * At this point, we know the size of the vector.
   * Position::End + m_nSysMax + m_nToyMC
   * The starting index of the sys is Position::End 
   * The starting point of the toys is Position::End+m_nSysMax
   */
  ATH_MSG_DEBUG("Max number of systematics seen : " << m_nSysMax << " number of toys " <<m_nToyMC);
  result.resize(static_cast<size_t> (Position::End)+m_nSysMax+m_nToyMC);
  const size_t position_corrSys= static_cast<size_t> (Position::End);
  const size_t position_uncorrToyMCSF=position_corrSys+m_nSysMax;
  //Set up the non-0 defaults
  result[static_cast<size_t> (Position::SF)]=-999;
  result[static_cast<size_t> (Position::Total)]=1; 
  if (m_nSysMax) {
    index_of_corr=position_corrSys;
  }
  if (m_nToyMC) {
    index_of_toys=position_uncorrToyMCSF;
  } 
  /* 
   * Determine Simulation flavour and find the run period
   */
  const bool isFastSim=dataType == PATCore::ParticleDataType::Fast;
  int runnumberIndex = -1;
  if (isFastSim) {
    for (unsigned int i = 0; i < m_begRunNumberListFastSim.size(); ++i) {
      if (m_begRunNumberListFastSim[i] <= runnumber && runnumber <= m_endRunNumberListFastSim[i]) {
        runnumberIndex=i;
        break;
      }
    }  
  }else {
    for (unsigned int i = 0; i < m_begRunNumberList.size(); ++i) {
      if (m_begRunNumberList[i] <= runnumber && runnumber <= m_endRunNumberList[i]) {
        runnumberIndex=i;
        break;
      }
    }
  }
  if(runnumberIndex <0 ){  
    ATH_MSG_DEBUG("(file: " << __FILE__ << ", line: " << __LINE__ << ")\n"<<
                  "No valid run number period  found for the current run number: " 
                  << runnumber <<" for simulation type: " << dataType); 
    return 0;
  }
  /* What we have is a map :
   * Key can be SF,Stat,Eigen,UnCorr
   * Entry is a vector<TObArray> 
   * Each vector<TObjArray>  has as many entries as supported Run periods.
   * Each TObjjArray has 2D histos (could be standard, low-et, or forward electrons)
   * The 2D Histo then has the number we want.
   * What follows is the logic to get to this number.
   */
  const std::unordered_map<int, std::vector< TObjArray > >& currentmap = (isFastSim)? m_fastHistList : m_histList;
  std::unordered_map<int, std::vector< TObjArray > >::const_iterator currentVector_itr = currentmap.find(mapkey::sf); 
  /*
   * See if we can find a  vector for key SF in the map 
   * and then if we can get the  corresponding TObjArray 
   * for the run period.
   */
  if (currentVector_itr == currentmap.end()) {
    ATH_MSG_DEBUG("(file: " << __FILE__ << ", line: " << __LINE__ << ")\n"<<
                  "No valid vector of sf ObjArray found for the current run number " 
                  << runnumber<<" for simulation type: " << dataType);  
    return 0;
  } 
  const std::vector<TObjArray>& currentVector=currentVector_itr->second;
  if (currentVector.empty() || runnumberIndex>= static_cast <int> (currentVector.size())) {
    ATH_MSG_DEBUG("(file: " << __FILE__ << ", line: " << __LINE__ << ")\n"<<
                  "No valid  sf ObjArray found for the current run number " 
                  << runnumber<<" for simulation type: " << dataType);  
    return 0;
  }
  /* 
   * At this stage we have found the relevant TObjArray
   * So we need to locate the right histogram.
   */
  const TObjArray& currentObjectArray = currentVector.at(runnumberIndex);  
  const int entries = currentObjectArray.GetEntries();
  /* 
   * Now the logic of finding the histogram
   * Some parts of the code can be perhaps improved ...
   */
  double xValue(et);
  double yValue(cluster_eta);
  int smallEt(0);
  int etaCov(0);
  int nSF(0);
  bool invalid = false;
  bool changedEt = false;
  int index = -1;
  TH2 *tmpHist(nullptr);

  for (int i = 0; i < entries ; ++i) {
    invalid = false;

    tmpHist = (TH2 *) (currentObjectArray.At(i));
    //invalid if we are below minimum et 
=======
  ATH_MSG_INFO("Initializing tool with " << m_corrFileNameList.size() << " configuration file(s)");

  ///////////////////////////////////////////////////
  // Check if the first file can be opened (needed for auto-setting of the seed based on the md5-sum of the file)
  const char *fname;
  fname = gSystem->ExpandPathName(m_corrFileNameList[0].c_str());
  std::unique_ptr<TFile> rootFile_tmp = CxxUtils::make_unique<TFile> (fname, "READ");
  if (!rootFile_tmp) {
    ATH_MSG_ERROR(
		  " (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "No ROOT file found here: " << m_corrFileNameList[0]);
    return 0;
  }
  rootFile_tmp->Close();
  // close it back again
  ///////////////////////////////////////////////////

  if (m_doToyMC && m_doCombToyMC) {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << " Both regular and combined toy MCs booked!"
		  << " Only use one!");
    return 0;
  }

  m_keys.push_back("sf");
  m_keys.push_back("stat");
  m_keys.push_back("eig");
  m_keys.push_back("uncorr");
  //
  // Cache for the last hist
  m_last_runnumber = 0;
  m_last_dataType = PATCore::ParticleDataType::Full;
  m_last_hist = new  std::map<std::string, const TH1 *>;
  m_last_hists = new std::map<std::string, const TObjArray *>;
  //
  //
  // initialize the random number generated if toyMC propagation booked
  if (m_doToyMC || m_doCombToyMC) {
    if (m_seed == 0) {
      TMD5 *tmd = new TMD5();
      m_seed = *reinterpret_cast<const int *>(tmd->FileChecksum(fname)->AsString());
      ATH_MSG_INFO("Seed (automatically) set to " << m_seed);
    }else {
      ATH_MSG_INFO("Seed set to " << m_seed);
    }
    m_Rndm = new TRandom3(m_seed);
  }
  //
  // Load the needed histograms
  if (0 == this->getHistograms()) {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "! Problem when calling getHistograms()");
    return 0;
  }
  unsigned int nRunNumbersFull = m_begRunNumberList.size();
  unsigned int nRunNumbersFast = m_begRunNumberListFastSim.size();

  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ") "
		<< "Found " << nRunNumbersFast << " run number ranges for fast sim with a total of " <<
                m_fastHistList["sf"].size() << " scale factor histograms.");

  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ") "
		<< "Found " << nRunNumbersFull << " run number ranges for full sim with a total of " <<
                m_histList["sf"].size() << " scale factor histograms.");

  // --------------------------------------------------------------------------
  // Register the cuts and check that the registration worked:
  // NOTE: THE ORDER IS IMPORTANT!!! Cut0 corresponds to bit 0, Cut1 to bit 1,...
  m_position_eff = m_result.addResult((m_resultPrefix + m_resultName).c_str(), "efficiency scale factor");
  if (m_position_eff < 0) {
    sc = 0; // Exceeded the number of allowed results
  }
  m_position_err = m_result.addResult(
				      (m_resultPrefix + m_resultName + "_err").c_str(), "efficiency scale factor uncertainty");
  if (m_position_err < 0) {
    sc = 0; // Exceeded the number of allowed results
  }
  m_position_statErr = m_result.addResult(
					  (m_resultPrefix + m_resultName + "_statErr").c_str(), "efficiency scale factor statistical uncertainty");
  if (m_position_statErr < 0) {
    sc = 0; // Exceeded the number of allowed results
  }
  m_position_nSys = m_result.addResult(
				       (m_resultPrefix + m_resultName + "_nSys").c_str(),
				       "number of corrrelated systematic uncertainties (dependent of detailLevel)");
  if (m_position_nSys < 0) {
    sc = 0; // Exceeded the number of allowed results
  }
  m_position_uncorrSys = m_result.addResult(
					    (m_resultPrefix + m_resultName + "_uncorrSys").c_str(), "total uncorrelated uncertainty");
  if (m_position_uncorrSys < 0) {
    sc = 0; // Exceeded the number of allowed results
  }
  for (int sys = 0; sys < m_nSysMax; sys++) {
    m_position_corrSys.push_back(m_result.addResult((m_resultPrefix + m_resultName + "_corrSys_" +
                                                     toString(sys)).c_str(),
                                                    "correlated uncertainty number " + toString(sys + 1)));
    if (m_position_corrSys.at(sys) < 0) {
      sc = 0; // Exceeded the number of allowed results
    }
    m_result.setResult(m_position_corrSys.at(sys), 0);
  }

  for (int toy = 0; toy < m_nToyMC; toy++) {
    m_position_uncorrToyMCSF.push_back(m_result.addResult((m_resultPrefix + m_resultName + "_uncorrToyMCSF_" +
                                                           toString(toy)).c_str(),
                                                          "Toy MC scale factor number " + toString(toy)));
    if (m_position_uncorrToyMCSF.at(toy) < 0) {
      sc = 0; // Exceeded the number of allowed results
    }
    m_result.setResult(m_position_uncorrToyMCSF.at(toy), 0);
  }

  m_position_globalBinNumber = m_result.addResult(
						  (m_resultPrefix + m_resultName + "_globalBinNumber").c_str(), "bin position in histograms");
  if (m_position_globalBinNumber < 0) {
    sc = 0; // Exceeded the number of allowed results
  }
  // Set the results to default values
  m_result.setResult(m_position_eff, -999.0);
  m_result.setResult(m_position_err, 1.0);
  m_result.setResult(m_position_statErr, 0.0);
  m_result.setResult(m_position_nSys, 0);
  m_result.setResult(m_position_uncorrSys, 0);
  m_isInitialized = kTRUE;

  ATH_MSG_INFO("Tool succesfully initialized!"<< m_nSys << "  " << m_nSysMax);
  return sc;
}

// =============================================================================
// Calculate the actual accept of each cut individually.
// =============================================================================
const Root::TResult &
Root::TElectronEfficiencyCorrectionTool::calculate(const PATCore::ParticleDataType::DataType dataType,
                                                   const unsigned int runnumber,
                                                   const double cluster_eta,
                                                   const double et /* in MeV */
                                                   ) {
  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << this->getName()
		<< "  entering function calculate");

  // check if a file is given and tool correctly initialized
  if (!m_isInitialized) {
    ATH_MSG_ERROR(
		  " (file: " << __FILE__ << ", line: " << __LINE__ << ") " <<
		  " Tool not correctly initialized or no scale factor file given. ");
    return m_result;
  }

  ATH_MSG_DEBUG(
		" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << this->getName() << " n Systematics: " << m_nSysMax);
  // Reset the results to default values
  m_result.setResult(m_position_eff, -999.0);
  m_result.setResult(m_position_err, 1.0);
  m_result.setResult(m_position_statErr, 0.0);
  m_result.setResult(m_position_uncorrSys, 0);
  for (int sys = 0; sys < m_nSysMax; sys++) {
    m_result.setResult(m_position_corrSys.at(sys), 0);
  }
  for (int toy = 0; toy < m_nToyMC; toy++) {
    m_result.setResult(m_position_uncorrToyMCSF.at(toy), 0);
  }
  m_result.setResult(m_position_globalBinNumber, 0);

  bool isFastSim(false);
  if (dataType == PATCore::ParticleDataType::Fast) {
    isFastSim = true;
  }

  bool sameHistos = kFALSE;
  if (m_last_runnumber == runnumber && m_last_dataType == dataType) {
    sameHistos = kTRUE;
  }
  m_last_runnumber = runnumber;
  m_last_dataType = dataType;

  // Only find histogram if not the same as last call
  if (sameHistos) {
    // Do not touch m_last histos cache
  }else {
    // else clear the last hist
    m_last_hist->clear();
    m_last_hists->clear();
    std::vector<TObjArray *> tmpVec;
    if (isFastSim) {
      for (unsigned int i = 0; i < m_begRunNumberListFastSim.size(); ++i) {
        if (m_begRunNumberListFastSim[i] <= runnumber && runnumber <= m_endRunNumberListFastSim[i]) {
          for (unsigned int ikey = 0; ikey < m_keys.size(); ++ikey) {
            tmpVec = m_fastHistList[m_keys.at(ikey)];
            if (tmpVec.size() > 0) {
              m_last_hists->insert(std::make_pair(m_keys.at(ikey), tmpVec.at(i)));
            }
          }
          m_runnumberIndex = i;
          break;
        }
      }
    }else {
      for (unsigned int i = 0; i < m_begRunNumberList.size(); ++i) {
        if (m_begRunNumberList[i] <= runnumber && runnumber <= m_endRunNumberList[i]) {
          for (unsigned int ikey = 0; ikey < m_keys.size(); ++ikey) {
            tmpVec = m_histList[m_keys.at(ikey)];
            if (tmpVec.size() > 0 && i < tmpVec.size()) {
              m_last_hists->insert(std::make_pair(m_keys.at(ikey), tmpVec.at(i)));
            }
          }
          m_runnumberIndex = i;
          break;
        }
      }
    }
    // If no valid hist, then, run number was out of range
    if (m_last_hists->find("sf") == m_last_hists->end()) {
      printDefaultReturnMessage(TString::Format(
						"No valid histogram found for the current run number: %i for simulation type: %i",
						runnumber, dataType),
                                __LINE__);
      return m_result;
    }
  }

  // Get the actual scale factor and its uncertainty from the current histogram
  double xValue, yValue;
  xValue = et;
  yValue = cluster_eta;
  int smallEt(0), etaCov(0), nSF(0);

  bool block = kFALSE;
  bool changed = kFALSE;
  bool isLowPt;
  int index = -1;
  TH2 *tmpHist(NULL);
  if (m_last_hists->find("sf") == m_last_hists->end()) {
    printDefaultReturnMessage("Could not find a histogram that ends with _sf, please check your input file!", __LINE__);
    return m_result;
  }
  for (int i = 0; i < (m_last_hists->find("sf"))->second->GetEntries(); ++i) {
    isLowPt = kFALSE;
    block = kFALSE;
    tmpHist = (TH2 *) (m_last_hists->find("sf")->second->At(i));
    if (TString(tmpHist->GetName()).Contains("LowPt")) {
      isLowPt = kTRUE;
    }
    block = kFALSE;
>>>>>>> release/21.0.127
    if (et < tmpHist->GetXaxis()->GetXmin()) {
      smallEt++;
      invalid = true;
    }
<<<<<<< HEAD
    //invalid if we are above max eta 
    if (std::abs(yValue) >= tmpHist->GetYaxis()->GetXmax()) {
=======

    if (TMath::Abs(yValue) >= tmpHist->GetYaxis()->GetXmax()) {
>>>>>>> release/21.0.127
      etaCov++;
      invalid=true;
    }
<<<<<<< HEAD
    // invalid if we are less than minimum eta (forward electrons)
    if (std::abs(yValue) < tmpHist->GetYaxis()->GetXmin()) {
      etaCov++;
      invalid = true;
    }
    /* 
     * Invalid if above max et and is a low Et histogram.
     * If not low Et histogram then change the xValue to the maximum
     * availabe Et of ths histogram. As we assume that the  SF stays the same 
     * for very high Et
     */
    if (et > tmpHist->GetXaxis()->GetXmax()) {
      if (TString(tmpHist->GetName()).Contains(LowPt_string)) {
        invalid = true;
      } else {
        xValue = tmpHist->GetXaxis()->GetBinCenter(tmpHist->GetNbinsX());
        changedEt = true;
      }
    }
    /*
     * Get the histogram index in the TObjArray
     * Also mark how many times we found something
     * as SF should be unique
     */
    if (!invalid) {
      index = i;
      if (!changedEt) {
=======

    // this is for forward electrons (eta-histos start at eta=2.47
    if (TMath::Abs(yValue) < tmpHist->GetYaxis()->GetXmin()) {
      etaCov++;
      block = kTRUE;
    }

    if (et > tmpHist->GetXaxis()->GetXmax()) {
      if (isLowPt) {
        block = kTRUE;
      } else {
        xValue = tmpHist->GetXaxis()->GetBinCenter(tmpHist->GetNbinsX());
        changed = kTRUE;
      }
    }
    if (!block) {
      index = i;
      if (!changed) {
>>>>>>> release/21.0.127
        nSF++;
      }
    }
  }
<<<<<<< HEAD
  if (smallEt == entries) {
    ATH_MSG_DEBUG("(file: " << __FILE__ << ", line: " << __LINE__ << ")\n"<<
                  "No correction factor provided for et " << xValue);  
    return 0;
  }
  if (etaCov == entries) {
    ATH_MSG_DEBUG("(file: " << __FILE__ << ", line: " << __LINE__ << ")\n"<<
                  "No correction factor provided for eta " << yValue);  
    return 0;
  }
  if (nSF>1) {
    ATH_MSG_WARNING("More than 1 SF found for eta=" << yValue << " , et = " 
                    << et << " , run number = " << runnumber << ". Please check your input files!");
  }
  /*
   * Now we have the index of the histogram 
   * for this region in the TObjectArray
   */
  TH2* currentHist(nullptr);
  if (index >= 0) {
    currentHist = static_cast<TH2*> (currentObjectArray.At(index));
  }
  else {
    ATH_MSG_DEBUG("(file: " << __FILE__ << ", line: " << __LINE__ << ")\n"<<
                  "No correction factor provided because of an invalid index" << yValue);
    return 0;
  }
  /*
   * If SF is only given in Abs(eta) convert eta input to std::abs()
   */
  constexpr double epsilon = 1e-6;
  if (currentHist->GetYaxis()->GetBinLowEdge(1) >= 0 - epsilon) {
    if (yValue < -1) {
      ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ")\n"
                    << "Scale factor only measured in Abs(eta) changing eta from " 
                    << yValue << " to " << std::abs(yValue));
    }
    yValue = std::abs(yValue);
  }
  const int globalBinNumber = currentHist->FindFixBin(xValue, yValue);
  const double scaleFactor = currentHist->GetBinContent(globalBinNumber);
  const double scaleFactorErr = currentHist->GetBinError(globalBinNumber);
  /* 
   * Write the retrieved values to the output
   * */
  result[static_cast<size_t> (Position::SF)]=scaleFactor;
  result[static_cast<size_t> (Position::Total)]=scaleFactorErr;
  /*
   * Do the stat error using the available info from the above (SF)
   */
  double statErr = -999;
  currentVector_itr = currentmap.find(mapkey::stat); 
  if (currentVector_itr != currentmap.end()) {
    const TH1 *stat = static_cast<TH1*>(currentVector_itr->second.at(runnumberIndex).At(index));
    statErr = stat->GetBinContent(globalBinNumber);
    result[static_cast<size_t> (Position::Stat)]=statErr;
  }
  /*
   * Do the Uncorr  uncertainty
   */
  double val = statErr;
  currentVector_itr = currentmap.find(mapkey::uncorr); 
  if (currentVector_itr != currentmap.end()) {
    if (currentVector_itr->second.at(runnumberIndex).GetEntriesFast()>0) {
     TH1 *uncorr = static_cast<TH1*>(currentVector_itr->second.at(runnumberIndex).At(index));
      const double valAdd = uncorr->GetBinContent(globalBinNumber);
      val = sqrt(val * val + valAdd * valAdd);
    }
  }
  result[static_cast<size_t> (Position::UnCorr)]=val; 
  /* 
   * The previous setup is becoming cumbersome 
   * for the N~16 systematic variations.
   * So we keep them in a vector of vector of TObjectArray
   * The first vector index being the runnumber
   * The second the systematic
   * And them the TObjArray for high low etc.
   * We invert the order in the output
   */
  const std::vector< std::vector< TObjArray > > &sysList = (isFastSim) ? m_fastSysList : m_sysList;
  std::vector<double> corrSys; 
  corrSys.reserve(16); 
  corrSys.clear();
  if (sysList.size() > static_cast<unsigned int> (index)) {
    if (sysList.at(index).size() > static_cast<unsigned int> (runnumberIndex)) {
      const int sys_entries = sysList.at(index).at( runnumberIndex).GetEntries();
      for (int sys = 0; sys < sys_entries; ++sys) {
        tmpHist = (TH2 *) sysList.at(index).at(runnumberIndex).At(sys_entries - 1 - sys);
        corrSys.push_back(tmpHist->GetBinContent(globalBinNumber));
        result[position_corrSys + sys_entries - 1 - sys] =corrSys[sys];
        }
      if (m_nSysMax > 0 && sys_entries<=1) {
        if (result[position_corrSys] == 0) {
            result[position_corrSys]=scaleFactorErr;
=======

  if (index >= 0) {
    if (m_last_hist->find("sf") == m_last_hist->end()) {
      m_last_hist->insert(std::make_pair("sf", (TH2 *) m_last_hists->find("sf")->second->At(index)));
    } else {
      m_last_hist->find("sf")->second = (TH2 *) m_last_hists->find("sf")->second->At(index);
    }
  }else {
    printDefaultReturnMessage(TString::Format(
					      "No correction factor provided because there was an index problem"), __LINE__);
    return m_result;
  }

  if (smallEt == m_last_hists->find("sf")->second->GetEntries()) {
    printDefaultReturnMessage(TString::Format("No correction factor provided for et=%f", xValue), __LINE__);
    return m_result;
  }

  if (etaCov == m_last_hists->find("sf")->second->GetEntries()) {
    printDefaultReturnMessage(TString::Format("No correction factor provided for eta=%f", yValue), __LINE__);
    return m_result;
  }

  if (nSF > 1) {
    ATH_MSG_WARNING(
		    "More than 1 SF found for eta=" << yValue << " , et = " << et << " , run number = " << runnumber <<
		    " . Please check your input files!");
  }

  // If SF is only given in Abs(eta) convert eta input to TMath::Abs()
  float epsilon = 1e-6;
  if (m_last_hist->find("sf")->second->GetYaxis()->GetBinLowEdge(1) >= 0 - epsilon) {
    if (yValue < 0) {
      ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ") "
		    << "Scale factor only measured in Abs(eta) changing eta from " << yValue << " to " << TMath::Abs(
														     yValue));
    }
    yValue = TMath::Abs(yValue);
  }

  int globalBinNumber = m_last_hist->find("sf")->second->FindFixBin(xValue, yValue);
  double scaleFactor = m_last_hist->find("sf")->second->GetBinContent(globalBinNumber);
  double scaleFactorErr = m_last_hist->find("sf")->second->GetBinError(globalBinNumber);

  // Write the retrieved values into the return object
  m_result.setResult(m_position_eff, scaleFactor);
  m_result.setResult(m_position_err, scaleFactorErr);

  std::map<std::string, const TObjArray *>::iterator tempit;
  double statErr = -999;

  //////////////////////////////

  tempit = m_last_hists->find("stat");
  if (tempit != m_last_hists->end()) {
    if (tempit->second->Last() != NULL) {
      TH1 *stat = (TH1 *) tempit->second->At(index);
      statErr = stat->GetBinContent(globalBinNumber);
      m_result.setResult(m_position_statErr, statErr);
    }
  }

  tempit = m_last_hists->find("eig");
  if (tempit != m_last_hists->end()) {
    if (tempit->second->Last() != NULL) {
      TH1 *eig = (TH1 *) tempit->second->At(index);
      m_sLevel[Root::TElectronEfficiencyCorrectionTool::simple] = 0;
      m_sLevel[Root::TElectronEfficiencyCorrectionTool::medium] = 0;
      m_sLevel[Root::TElectronEfficiencyCorrectionTool::detailed] = 0;
      int nSys = eig->GetNbinsX() - 1;
      double sign = 0;
      // Calculate detail level
      for (int i = nSys + 1; i >= 2; i--) {
        sign += eig->GetBinContent(i);
        if (sign > 0.8 && m_sLevel[Root::TElectronEfficiencyCorrectionTool::simple] == 0) {
          m_sLevel[Root::TElectronEfficiencyCorrectionTool::simple] = i - 2;
        }
        if (sign > 0.95 && m_sLevel[Root::TElectronEfficiencyCorrectionTool::medium] == 0) {
          m_sLevel[Root::TElectronEfficiencyCorrectionTool::medium] = i - 2;
        }
      }
      nSys -= m_sLevel[m_detailLevel];
      m_result.setResult(m_position_nSys, nSys);
    }
  }

  std::vector< std::vector< TObjArray * > > *sysList;
  if (isFastSim) {
    sysList = &m_fastSysList;
  } else {
    sysList = &m_sysList;
  }
  std::vector<double> corrSys;
  if (sysList != 0) {
    if (sysList->size() > (unsigned int) index) {
      if (sysList->at(index).size() > (unsigned int) m_runnumberIndex) {
        for (int sys = 0; sys < sysList->at(index).at(m_runnumberIndex)->GetEntries(); sys++) {
          tmpHist = (TH2 *) sysList->at(index).at(m_runnumberIndex)->At(sysList->at(index).at(
											      m_runnumberIndex)->GetEntries() - 1 - sys);

          corrSys.push_back(tmpHist->GetBinContent(globalBinNumber));
          m_result.setResult(m_position_corrSys[(sysList->at(index).at(
								       m_runnumberIndex)->GetEntries() - 1 - sys)], corrSys[sys]);
>>>>>>> release/21.0.127
        }
      }
    }
  }
<<<<<<< HEAD
  /* 
   * Do the toys
   */
  if (m_doToyMC || m_doCombToyMC) {
    const std::vector<std::vector<TObjArray > >& toyMCList = ((isFastSim) ? m_uncorrToyMCSystFast : m_uncorrToyMCSystFull);
    if (toyMCList.size() > (unsigned int) runnumberIndex) {
      for (int toy = 0; toy < m_nToyMC; toy++) {
        if (toyMCList.at(runnumberIndex).at(toy).GetLast() >= index) {
          result[position_uncorrToyMCSF+toy]=
            ((TH2 *) toyMCList.at(runnumberIndex).at(toy).At(index))->GetBinContent(globalBinNumber);
=======
  if (m_position_corrSys.size() > 0 && sysList->at(index).at(m_runnumberIndex)->GetEntries()<=1) {
    if (m_result.getResult(m_position_corrSys[0]) == 0) {
      m_result.setResult(m_position_corrSys[0], scaleFactorErr);
    }
  }
  statErr = -999;
  tempit = m_last_hists->find("stat");
  if (tempit != m_last_hists->end()) {
    if (tempit->second->Last() != NULL) {
      TH1 *stat = (TH1 *) tempit->second->At(index);
      statErr = stat->GetBinContent(globalBinNumber);
    }
  }
  double val = statErr;
  tempit = m_last_hists->find("uncorr");
  if (tempit != m_last_hists->end()) {
    if (tempit->second->Last() != 0) {
      TH1 *uncorr = (TH1 *) tempit->second->At(index);
      double valAdd = uncorr->GetBinContent(globalBinNumber);
      val = sqrt(val * val + valAdd * valAdd);
      for (int i = 0; i < m_sLevel[m_detailLevel]; ++i) {
        double valAdd = corrSys.at(corrSys.size() - 1 - i);
        val = sqrt(val * val + valAdd * valAdd);
      }
    }
  }
  if (val == -999) {
    val = 0;
  }
  m_result.setResult(m_position_uncorrSys, val);

  if (m_doToyMC || m_doCombToyMC) {
    std::vector< std::vector<TObjArray *> *> *toyMCList;
    if (isFastSim) {
      toyMCList = m_uncorrToyMCSystFast;
    } else {
      toyMCList = m_uncorrToyMCSystFull;
    }
    if (toyMCList != 0) {
      if (toyMCList->size() > (unsigned int) m_runnumberIndex) {
        for (int toy = 0; toy < m_nToyMC; toy++) {
          if (toyMCList->at(m_runnumberIndex)->at(toy)->GetLast() >= index) {
            m_result.setResult(m_position_uncorrToyMCSF.at(toy),
                               ((TH2 *) toyMCList->at(m_runnumberIndex)->at(toy)->At(index))->GetBinContent(
													    globalBinNumber));
          }
>>>>>>> release/21.0.127
        }
      }
    }
  }
<<<<<<< HEAD
  result[static_cast<size_t> (Position::GlobalBinNumber)]=globalBinNumber;
  return 1;
}
/*
 * Build the toyMC tables from inputs
 * Ownership should be tranfered to the map of the tables
 * and the proper delete happens in the dtor
 */
std::vector<TH2 *>
Root::TElectronEfficiencyCorrectionTool::buildSingleToyMC(const TH2 *sf, 
                                                          const TH2 *stat, 
                                                          const TH2 *uncorr, 
                                                          const TObjArray& corr,
                                                          int& randomCounter) {

  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ")! " 
                << "Entering function buildSingleToyMC");
  std::vector<TH2*> tmpHists;
  int nBins = (stat->GetNbinsX() + 2) * (stat->GetNbinsY() + 2);
  tmpHists.reserve(m_nToyMC);
for (int toy = 0; toy < m_nToyMC; toy++) {
    tmpHists.push_back((TH2 *) corr.At(0)->Clone());
  }
  // Loop over all bins
  for (int bin = 0; bin < nBins; bin++) {
    double val = stat->GetBinContent(bin);

    // Add uncorrelated systematics
    if (uncorr != nullptr) {
      double valAdd = uncorr->GetBinContent(bin);
      val = sqrt(val * val + valAdd * valAdd);
    }
    for (int toy = 0; toy < m_nToyMC; toy++) {
      tmpHists.at(toy)->SetBinContent(bin, (val * m_Rndm.Gaus(0, 1)) + sf->GetBinContent(bin));
      randomCounter++;
      tmpHists.at(toy)->SetDirectory(nullptr);
    }
=======
  m_result.setResult(m_position_globalBinNumber, globalBinNumber);

  return m_result;
}

// =============================================================================
// Helper function to retrieve number of uncorrelated bins
// =============================================================================
int
Root::TElectronEfficiencyCorrectionTool::getNbins(std::map<float, std::vector<float> > &pt_eta1) {
  std::vector<TObjArray *> tmpVec;

  tmpVec = m_histList["sf"];
  int nbinsTotal = 0;
  pt_eta1.clear();
  std::vector<float>eta1;
  eta1.clear();

  for (unsigned int ikey = 0; ikey < tmpVec.size(); ++ikey) {
    for (int entries = 0; entries < (tmpVec.at(ikey))->GetEntries(); entries++) {
      eta1.clear();

      TH2D *h_tmp = ((TH2D * ) (tmpVec.at(ikey))->At(entries));
      int nbinsX = h_tmp->GetNbinsX();
      int nbinsY = h_tmp->GetNbinsY();

      for (int biny = 1; biny <= nbinsY + 1; biny++) {
        eta1.push_back(h_tmp->GetYaxis()->GetBinLowEdge(biny));
        if (entries == (tmpVec.at(ikey))->GetEntries() - 1) {
          eta1.push_back(h_tmp->GetYaxis()->GetBinLowEdge(biny + 1));
        }
      }
      for (int binx = 1; binx <= nbinsX; binx++) {
        pt_eta1[h_tmp->GetXaxis()->GetBinLowEdge(binx)] = eta1;
        if (entries == (tmpVec.at(ikey))->GetEntries() - 1) {
          pt_eta1[h_tmp->GetXaxis()->GetBinLowEdge(binx + 1)] = eta1;
        }
      }
    }
  }

  for (auto &i : pt_eta1) {
    nbinsTotal += i.second.size();
  }

  return nbinsTotal;
}

// =============================================================================
// Helper function to retrieve the position of the first toy MC scale factor
// =============================================================================
int
Root::TElectronEfficiencyCorrectionTool::getFirstToyMCPosition() {
  if (!m_isInitialized) {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "Tool not initialized.");
    return -1;
  }

  if (m_nToyMC > 0) {
    return m_position_uncorrToyMCSF.at(0);
  }else {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") "
		  << " No toy scale factors booked, returning the position of the scale factor! This is most likely not what you want!");
    return 0;
>>>>>>> release/21.0.127
  }
  return tmpHists;
}
<<<<<<< HEAD
/*
 * Build the combined toyMC tables from inputs
 * Ownership should be tranfered to the map of the tables
 * and the proper delete happens in the dtor
 */
TH2*
Root::TElectronEfficiencyCorrectionTool::buildSingleCombToyMC(const TH2 *sf, 
                                                              const TH2 *stat, 
                                                              const TH2 *uncorr, 
                                                              const TObjArray& corr,
                                                              const int nSys,
                                                              int& randomCounter){

  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ")\n" 
                << "Entering function buildSingleCombToyMC");

  TH2 *tmpHist;
  const int nBins = (stat->GetNbinsX() + 2) * (stat->GetNbinsY() + 2);
  tmpHist = (TH2 *) corr.At(0)->Clone();
  // Create random numbers for the corr. uncertainties
  std::vector<double> rnd (nSys,0);
  for (int s = 0; s < nSys; ++s) {
    rnd[s] = m_Rndm.Gaus(0, 1);
    randomCounter++;
=======

int
Root::TElectronEfficiencyCorrectionTool::getLastToyMCPosition() {
  if (!m_isInitialized) {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "Tool not initialized.");
    return -1;
  }

  if (m_nToyMC > 0) {
    return m_position_uncorrToyMCSF.at(m_nToyMC - 1);
  } else {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") "
		  << "No toy scale factors booked, returning the position of the scale factor! This is most likely not what you want!");
    return 0;
>>>>>>> release/21.0.127
  }
  // Loop over all bins
  for (int bin = 0; bin < nBins; ++bin) {
    double val = stat->GetBinContent(bin);

<<<<<<< HEAD
    // Add uncorrelated systematics
    if (uncorr != nullptr) {
      double valAdd = uncorr->GetBinContent(bin);
      val = sqrt(val * val + valAdd * valAdd);
    }
    val = val * m_Rndm.Gaus(0,1);
    randomCounter++;
    // Add larger correlated systematics
    for (int s = 0; s < nSys; ++s) {
      if (corr.At(s) != nullptr) {
        val += ((TH2 *) corr.At(s))->GetBinContent(bin) * rnd[s];
      }
    }
    tmpHist->SetBinContent(bin, val + sf->GetBinContent(bin));
=======
int
Root::TElectronEfficiencyCorrectionTool::getFirstCorrSysPosition() {
  if (!m_isInitialized) {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "Tool not initialized.");
    return -1;
  }

  if (m_position_corrSys.size() > 0) {
    return m_position_corrSys.at(0);
  }else {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") "
		  << "No correlated uncertainties found, returning the position of the scale factor! This is most likely not what you want!");
    return 0;
>>>>>>> release/21.0.127
  }
  tmpHist->SetDirectory(nullptr);
  return tmpHist;
}
<<<<<<< HEAD
/*
 * Build the toyMC tables from inputs
 */
std::vector<TObjArray>
Root::TElectronEfficiencyCorrectionTool::buildToyMCTable(const TObjArray& sf, 
                                                         const TObjArray& eig, 
                                                         const TObjArray& stat,
                                                         const TObjArray& uncorr, 
                                                         const std::vector<TObjArray>& corr) {

  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ")\n" 
                << "Entering function buildToyMCTable");

  int nSys{};
  int randomCounter(0);
  std::vector<TObjArray> tmpVec;
  const int stat_entries = stat.GetEntries();
  if (m_doCombToyMC) {
    for (int toyMC = 0; toyMC < m_nToyMC; toyMC++) {
      TObjArray tmpArray;
      for (int i = 0; i < stat_entries; ++i) {
        if (eig.GetEntriesFast()>0 && uncorr.GetEntriesFast()>0) { 
          nSys = ((TH1*)eig.At(i))->GetNbinsX()-1;  
          tmpArray.Add(buildSingleCombToyMC((TH2 *) sf.At(i), 
                                            (TH2 *) stat.At(i), 
                                            (TH2 *) uncorr.At(i), 
                                            corr.at(i),
                                            nSys,
                                            randomCounter));
        }else {
          tmpArray.Add(buildSingleCombToyMC((TH2*) sf.At(i), 
                                            (TH2*) stat.At(i), 
                                            nullptr, 
                                            corr.at(i) ,
                                            nSys,
                                            randomCounter));
        }
      }
      tmpVec.push_back(tmpArray);
    }
  }else {
    std::vector< std::vector<TH2*> > tmpVec2 ;
    for (int i = 0; i < stat_entries; ++i) {
      nSys = ((TH1*)eig.At(i))->GetNbinsX()-1;
      tmpVec2.push_back(buildSingleToyMC((TH2*) sf.At(i), 
                                         (TH2*) stat.At(i), 
                                         (TH2*) uncorr.At(i),
                                         corr.at(i),
                                         randomCounter));
    }
    for (int toy = 0; toy < m_nToyMC; toy++) {
      TObjArray tmpArray;
      for (unsigned int i = 0; i < tmpVec2.size(); ++i) {
        tmpArray.Add(tmpVec2.at(i).at(toy));
      }
      tmpVec.push_back(tmpArray);
    }
  }
  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ")\n" 
                << "m_Rndm->Uniform(0, 1) after throwing " << randomCounter
                << " random numbers: " << m_Rndm.Uniform(0,1));

  return tmpVec;
=======

int
Root::TElectronEfficiencyCorrectionTool::getLastCorrSysPosition() {
  if (!m_isInitialized) {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "Tool not initialized.");
    return -1;
  }

  if (m_position_corrSys.size() > 0) {
    if (m_result.getResult(3) > 0) {
      return m_position_corrSys.at(m_result.getResult(3) - 1);
    }else {
      return m_position_corrSys.at(0);
    }
  }else {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") "
		  << "No correlated uncertainties found, returning the position of the scale factor! This is most likely not what you want!");
    return 0;
  }
}

int
Root::TElectronEfficiencyCorrectionTool::getGlobalBinNumberPosition() {
  if (!m_isInitialized) {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "Tool not initialized.");
    return -1;
  }
  return m_position_globalBinNumber;
>>>>>>> release/21.0.127
}
/*
 * Helper function to retrieve number of uncorrelated bins
 */
int
Root::TElectronEfficiencyCorrectionTool::getNbins(std::map<float, std::vector<float> > &pt_eta1) const {
  //Get sf histograms 
  const std::vector<TObjArray >& tmpVec = m_histList.at(mapkey::sf);
  int nbinsTotal = 0;
  pt_eta1.clear();
  std::vector<float>eta1;
  eta1.clear();

<<<<<<< HEAD
  //Loop over the different Run range (one TObjeArray for each)
  for (unsigned int ikey = 0; ikey < tmpVec.size(); ++ikey) {
    //Loop over the histograms for a given run numbers
    for (int entries = 0; entries < (tmpVec.at(ikey)).GetEntries(); ++entries) {
      eta1.clear();
      //Get number of bins
      TH2 *h_tmp = ((TH2 * ) (tmpVec.at(ikey)).At(entries));
      int nbinsX = h_tmp->GetNbinsX();
      int nbinsY = h_tmp->GetNbinsY();
      //fill in the eta pushing back
      for (int biny = 1; biny <= nbinsY; ++biny) {
        eta1.push_back(h_tmp->GetYaxis()->GetBinLowEdge(biny));
      }
      //associate each pt (bin) with the corresponding/available eta ones
      for (int binx = 1; binx <=nbinsX; ++binx) {
        pt_eta1[h_tmp->GetXaxis()->GetBinLowEdge(binx)] = eta1;
      }
    }
  }
  for (auto &i : pt_eta1) {
    nbinsTotal += i.second.size();
=======
void
Root::TElectronEfficiencyCorrectionTool::printResultMap() {
  if (!m_isInitialized) {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "Tool not initialized.");
    return;
  }
  ATH_MSG_INFO("!" << std::endl << "############" << std::endl << "Position  \t -\t Description ");
  for (unsigned int pos = 0; pos < m_result.getNResults(); pos++) {
    ATH_MSG_INFO(pos << " \t \t - \t " << m_result.getResultDescription(pos));
>>>>>>> release/21.0.127
  }
  return nbinsTotal;
}
/*
 * Get the  histograms from the input files
 */
int Root::TElectronEfficiencyCorrectionTool::getHistograms() {

<<<<<<< HEAD
  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ")\n" 
                << "Entering function getHistograms");
  // Cache the current directory in the root file
  TDirectory *origDir = gDirectory;
  /*
   * Get the name of the first input ROOT file and
   * interpret from that what we have:
   * efficiency vs. efficiencySF; offline vs. trigger; medium, loose,...
   */
  if (!m_corrFileNameList.empty()) {
    TString firstFileNameAndPath = m_corrFileNameList[0].c_str();
    std::unique_ptr<TObjArray> myStringList(firstFileNameAndPath.Tokenize("/"));
    int lastIdx = myStringList->GetLast();
    TString fileName = ((TObjString *) myStringList->At(lastIdx))->GetString();
    std::unique_ptr<TObjArray> myFileNameTokensList(fileName.Tokenize("."));

    if (myFileNameTokensList->GetLast() < 3) {
      ATH_MSG_ERROR("input file name has wrong format!");
      return 0;
    }
  }
  /*
   * Get all ROOT files and histograms
   */
  for (unsigned int i = 0; i < m_corrFileNameList.size(); ++i) {
    // Load the ROOT file
    const std::unique_ptr<char[]> fname (gSystem->ExpandPathName(m_corrFileNameList[i].c_str()));
    std::unique_ptr<TFile> rootFile( TFile::Open(fname.get(), "READ") );
    if (!rootFile) {
      ATH_MSG_ERROR( "No ROOT file found here: " <<m_corrFileNameList[i]);
      return 0;
    }
    // Loop over all directories inside the root file (correspond to the run number ranges
    TIter nextdir(rootFile->GetListOfKeys());
    TKey *dir=nullptr;
    TObject *obj=nullptr;
=======
// =============================================================================
// Get the input histograms from the input files
// =============================================================================
int
Root::TElectronEfficiencyCorrectionTool::getHistograms() {
  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "entering function getHistograms");

  // Cache the current directory in the root file
  TDirectory *origDir = gDirectory;

  // -------------------------------------------------------
  // Get the name of the first input ROOT file and
  // interpret from that what we have:
  // efficiency vs. efficiencySF; offline vs. trigger; medium, loose,...
  // -------------------------------------------------------
  if (!m_corrFileNameList.empty()) {
    TString firstFileNameAndPath = m_corrFileNameList[0].c_str();
    TObjArray *myStringList = firstFileNameAndPath.Tokenize("/");
    int lastIdx = myStringList->GetLast();
    TString fileName = ((TObjString *) myStringList->At(lastIdx))->GetString();
    TObjArray *myFileNameTokensList = fileName.Tokenize(".");
    if (myFileNameTokensList->GetLast() < 3) {
      ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "input file name has wrong format!");
      return 0;
    }

    if (m_resultPrefix.empty()) {// Only overwrite it if the user didn't explicitly set it
      m_resultPrefix = ((TObjString *) myFileNameTokensList->At(0))->GetString();
      m_resultPrefix += "_";
    }
    if (m_resultName.empty()) {// Only overwrite it if the user didn't explicitly set it
      m_resultName = ((TObjString *) myFileNameTokensList->At(1))->GetString()
	+ "_" + ((TObjString *) myFileNameTokensList->At(2))->GetString();
    }
    ATH_MSG_INFO("Using resultPrefix: " << m_resultPrefix
		 << " and resultName: " << m_resultName);

    delete myStringList;
    delete myFileNameTokensList;
  }

  // -------------------------------------------------------
  // Get all ROOT files and histograms
  // -------------------------------------------------------

  for (unsigned int i = 0; i < m_corrFileNameList.size(); ++i) {
    // Load the ROOT file
    const char *fname;
    fname = gSystem->ExpandPathName(m_corrFileNameList[i].c_str());
    std::unique_ptr<TFile> rootFile = CxxUtils::make_unique<TFile> (fname, "READ");
    if (!rootFile) {
      ATH_MSG_ERROR(
		    " (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "No ROOT file found here: " <<
		    m_corrFileNameList[i]);
      ;
      return 0;
    }

    if (m_doToyMC || m_doCombToyMC) {
      m_uncorrToyMCSystFull = new std::vector< std::vector<TObjArray *> * >;
      m_uncorrToyMCSystFast = new std::vector< std::vector<TObjArray *> *>;
    }
    // Loop over all directories inside the root file (correspond to the run number ranges
    TIter nextdir(rootFile->GetListOfKeys());
    TKey *dir;
    TObject *obj;
>>>>>>> release/21.0.127
    while ((dir = (TKey *) nextdir())) {
      obj = dir->ReadObj();
      if (obj->IsA()->InheritsFrom("TDirectory")) {
        // splits string by delimiter --> e.g RunNumber1_RunNumber2
        std::unique_ptr<TObjArray> dirNameArray(TString(obj->GetName()).Tokenize("_"));
<<<<<<< HEAD
        // returns index of last string --> if one, the directory name does not contain any run numbers
        int lastIdx = dirNameArray->GetLast();
        if (lastIdx != 1) {
          ATH_MSG_ERROR("The folder name seems to have the wrong format! Directory name:"<< obj->GetName());
          return 0;
        }
        rootFile->cd(obj->GetName());
        if (0 == this->setupHistogramsInFolder(*dirNameArray, lastIdx)) {
          ATH_MSG_ERROR("Unable to setup the histograms in directory " << dir->GetName()
                        << "in file " << m_corrFileNameList[i]);
          return 0;
        }
      }else {
        ATH_MSG_ERROR( "Wrong file content! Expected only Directories " <<
                      gDirectory->cd());
=======
        //// returns index of last string --> if one, the directory name does not contain any run numbers
        int lastIdx = dirNameArray->GetLast();
        if (lastIdx != 1) {
          ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "The folder name seems to have the wrong format! Directory name:"
			<< obj->GetName());
          return 0;
        }
        rootFile->cd(obj->GetName());
        if (0 == this->setupHistogramsInFolder(dirNameArray.get(), lastIdx)) {
          ATH_MSG_ERROR(
			" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "unable to setup the histograms in directory " << dir->GetName()
			<< "in file " << m_corrFileNameList[i]);
          return 0;
        }
      }else {
        ATH_MSG_ERROR(
		      " (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "Wrong file content! Expected only Directories " <<
		      gDirectory->cd());
>>>>>>> release/21.0.127
        return 0;
      }
      // Return to the original ROOT directory
      gDirectory = origDir;
    } // End: directory loop
  } // End: file loop
<<<<<<< HEAD
  return 1;
}
/*
 * Get the input histograms from a given folder/run number range
 */
int Root::TElectronEfficiencyCorrectionTool::setupHistogramsInFolder(const TObjArray& dirNameArray, int lastIdx) {

  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ")\n" 
                << "Entering funtion setupHistogramsInFolder");

  int runNumBegin(-1);
  TString myBegRunNumString = ( (TObjString*) dirNameArray.At(lastIdx - 1) )->GetString();
  if (myBegRunNumString.IsDigit()) {
    runNumBegin = myBegRunNumString.Atoi();
  }
  int runNumEnd(-1);
  TString myEndRunNumString = ( (TObjString *) dirNameArray.At(lastIdx) )->GetString();
  if (myEndRunNumString.IsDigit()) {
    runNumEnd = myEndRunNumString.Atoi();
  }
  if (runNumBegin < 0 || runNumEnd < 0 || runNumEnd < runNumBegin) {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ")\n" 
                  << "Could NOT interpret the run number range: " << runNumBegin
                  << " - " << runNumEnd);
    return 0;
  }
  /// setup pairs of obj arrays and keys --> e.g. "sf", new Array to take all SF Histos
  std::unordered_map<int, TObjArray> objsFull;
  std::unordered_map<int, TObjArray > objsFast;
  for (unsigned int ikey = 0; ikey < s_keys.size(); ++ikey) {
    TObjArray dummyFull;
    objsFull.insert(std::make_pair(s_keys.at(ikey),dummyFull));
    TObjArray dummyFast;
    objsFast.insert(std::make_pair(s_keys.at(ikey),dummyFast));
  }  
  TObjArray dummyFull;    
  objsFull.insert(std::make_pair(mapkey::sys, dummyFull));
  TObjArray dummyFast;
  objsFast.insert(std::make_pair(mapkey::sys, dummyFast));

  std::vector<TObjArray > sysObjsFull;
  std::vector<TObjArray > sysObjsFast;

  TIter nextkey(gDirectory->GetListOfKeys());
  TKey *key=nullptr;
  TObject *obj=nullptr;
  int seenSystematics= 0;

  //Loop of the keys 
=======

  return 1;
}

// =============================================================================
// Get the input histograms from a given folder/run number range
// =============================================================================
int
Root::TElectronEfficiencyCorrectionTool::setupHistogramsInFolder(TObjArray *dirNameArray, int lastIdx) {
  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "entering funtion setupHistogramsInFolder");

  std::unique_ptr<std::map<std::string, TObjArray *> > objsFull(new std::map<std::string, TObjArray *>);
  std::unique_ptr<std::map<std::string, TObjArray *> > objsFast(new std::map<std::string, TObjArray *>);

  std::unique_ptr<std::vector<TObjArray *> > sysObjsFull(new std::vector<TObjArray *>);
  std::unique_ptr<std::vector<TObjArray *> > sysObjsFast(new std::vector<TObjArray *>);

  /// setup pairs of obj arrays and keys --> e.g. "sf", new Array to take all SF Histos
  for (unsigned int ikey = 0; ikey < m_keys.size(); ++ikey) {
    objsFull->insert(std::make_pair(m_keys.at(ikey), new TObjArray()));
    objsFast->insert(std::make_pair(m_keys.at(ikey), new TObjArray()));
  }
  objsFull->insert(std::make_pair("sys", new TObjArray()));
  objsFast->insert(std::make_pair("sys", new TObjArray()));

  m_runNumBegin = -1;
  TString myBegRunNumString = ((TObjString *) dirNameArray->At(lastIdx - 1))->GetString();
  if (myBegRunNumString.IsDigit()) {
    m_runNumBegin = myBegRunNumString.Atoi();
  }
  m_runNumEnd = -1;
  TString myEndRunNumString = ((TObjString *) dirNameArray->At(lastIdx))->GetString();
  if (myEndRunNumString.IsDigit()) {
    m_runNumEnd = myEndRunNumString.Atoi();
  }
  if (m_runNumBegin < 0 || m_runNumEnd < 0 || m_runNumEnd < m_runNumBegin) {
    ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "Could NOT interpret the run number range: " << m_runNumBegin
		  << " - " << m_runNumEnd);
    return 0;
  }
  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << m_runNumBegin << "  " << m_runNumEnd);

  TIter nextkey(gDirectory->GetListOfKeys());
  TKey *key;
  TObject *obj(0);
  TString tmpName = "";
  TObjArray *tmpArrayFull(NULL);
  TObjArray *tmpArrayFast(NULL);
  int tmpCounter = 0;
  int loop = 0;
>>>>>>> release/21.0.127
  while ((key = (TKey *) nextkey())) {
    obj = key->ReadObj();
    if (obj->IsA()->InheritsFrom("TH1")) {
      // The histogram containing the scale factors need to end with _sf and need to contain either the string "FullSim"
      // or "AtlFast2"!
      if (TString(obj->GetName()).Contains("FullSim")) {
<<<<<<< HEAD
        setupTempMapsHelper( obj,objsFull,sysObjsFull, seenSystematics); 
      }
      else if (TString(obj->GetName()).Contains("AtlFast2")) {
        setupTempMapsHelper( obj,objsFast,sysObjsFast, seenSystematics); 
      } else {
        ATH_MSG_ERROR( "Could NOT interpret if the histogram: " << obj->GetName()
                      << " is full or fast simulation!");
        return 0;
      }
    }
  }
  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ")\n" 
                << "Setting up histograms for Run range  " <<
                runNumEnd);
  /*
   * Copy from the temporaries to the actual member variables
   * via the setup function
   */
  for (unsigned int ikey = 0; ikey < s_keys.size(); ++ikey) {
    if (objsFull.find(s_keys.at(ikey))->second.GetEntries() != 0) {
      if (0 == setup(objsFull.find(s_keys.at(ikey))->second, m_histList[s_keys.at(ikey)], 
                     m_begRunNumberList,m_endRunNumberList,runNumBegin,runNumEnd)) {
        ATH_MSG_ERROR("! Could NOT setup histogram " 
                      << s_keys.at(ikey)<< " for full sim!");
        return 0;
      }
    }
    if (objsFast.find(s_keys.at(ikey))->second.GetEntries() != 0) {
      if (0 == setup(objsFast.find(s_keys.at(ikey))->second, m_fastHistList[s_keys.at(ikey)],
                     m_begRunNumberListFastSim, m_endRunNumberListFastSim,runNumBegin,runNumEnd)) {
        ATH_MSG_ERROR("! Could NOT setup histogram " << s_keys.at(ikey)
                      << " for fast sim");
=======
        for (unsigned int ikey = 0; ikey < m_keys.size(); ++ikey) {
          if (TString(obj->GetName()).EndsWith("_" + (TString) m_keys.at(ikey))) {
            objsFull->find(m_keys.at(ikey))->second->Add(obj);
          }
          if (!loop && TString(obj->GetName()).EndsWith("_sys")) {
            objsFull->find("sys")->second->Add(obj);
            tmpCounter++;
            loop++;
            if (tmpArrayFull == 0 && (objsFull->find("sys")->second)) {
              tmpArrayFull = new TObjArray();
              tmpArrayFull->Add(obj);
            }else if (tmpArrayFull != 0) {
              tmpArrayFull->Add(obj);
            }
          }
        }

        if (!loop && TString(obj->GetName()).EndsWith("_sys")) {
          objsFull->find("sys")->second->Add(obj);
          tmpCounter++;
          loop++;
          if (tmpArrayFull == 0 && (objsFull->find("sys")->second)) {
            tmpArrayFull = new TObjArray();
            tmpArrayFull->Add(obj);
          }else if (tmpArrayFull != 0) {
            tmpArrayFull->Add(obj);
          }
        }

        tmpName = TString(obj->GetName());
        if (tmpName.Contains("_corr")) {
          if (tmpName.EndsWith("corr0")) {
            if (tmpCounter > m_nSysMax) {
              m_nSysMax = tmpCounter;
            }
            tmpCounter = 0;
            if (tmpArrayFull != 0) {
              sysObjsFull->push_back(tmpArrayFull);
            }
            tmpArrayFull = new TObjArray();
          }
          if (tmpArrayFull != 0) {
            tmpArrayFull->Add(obj);
          }else {
            ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "Tried to read systematic uncertainties for full sim scale factor,"
			  << " but could NOT find a histogram with name ending in \"corr0\". Please check input file! ");
            return 0;
          }
          tmpCounter++;
        }
        if (tmpCounter > m_nSysMax) {
          m_nSysMax = tmpCounter;
        }
      }else if (TString(obj->GetName()).Contains("AtlFast2")) {
        for (unsigned int ikey = 0; ikey < m_keys.size(); ++ikey) {
          if (TString(obj->GetName()).EndsWith("_" + (TString) m_keys.at(ikey))) {
            objsFast->find(m_keys.at(ikey))->second->Add(obj);
          }
        }
        tmpName = TString(obj->GetName());
        if (tmpName.Contains("_corr")) {
          if (tmpName.EndsWith("corr0")) {
            if (tmpCounter > m_nSysMax) {
              m_nSysMax = tmpCounter;
            }
            tmpCounter = 0;
            if (tmpArrayFast != 0) {
              sysObjsFast->push_back(tmpArrayFast);
            }
            tmpArrayFast = new TObjArray();
          }
          if (tmpArrayFast != 0) {
            tmpArrayFast->Add(obj);
          }else {
            ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "! Tried to read systematic uncertainties for fast sim scale factor, "
			  << "but could NOT find a histogram with name ending in \"corr0\". Please check input file! ");
            return 0;
          }
          tmpCounter++;
        }
        if (tmpCounter > m_nSysMax) {
          m_nSysMax = tmpCounter;
        }
      } else {
        ATH_MSG_ERROR(
		      " (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "Could NOT interpret if the histogram: " << obj->GetName()
		      << " is full or fast simulation!");
        return 0;
      }
    }
  }

  if (tmpArrayFull != 0) {
    sysObjsFull->push_back(tmpArrayFull);
  }
  if (tmpArrayFast != 0) {
    sysObjsFast->push_back(tmpArrayFast);
  }

  ATH_MSG_DEBUG(
		" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "setting up histograms for run ranges  " <<
		m_runNumEnd);

  for (unsigned int ikey = 0; ikey < m_keys.size(); ++ikey) {
    if (objsFull->find(m_keys.at(ikey))->second->GetEntries() != 0) {
      if (0 ==
          this->setup(objsFull->find(m_keys.at(ikey))->second, m_histList[m_keys.at(ikey)], m_begRunNumberList,
                      m_endRunNumberList)) {
        ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "! Could NOT setup histogram " << m_keys.at(
															      ikey)
		      << " for full sim!");
        return 0;
      }
    }
    if (objsFast->find(m_keys.at(ikey))->second->GetEntries() != 0) {
      if (0 ==
          this->setup(objsFast->find(std::string(m_keys.at(ikey)))->second, m_fastHistList[m_keys.at(ikey)],
                      m_begRunNumberListFastSim, m_endRunNumberListFastSim)) {
        ATH_MSG_ERROR(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << "! Could NOT setup histogram " << m_keys.at(
															      ikey)
		      << " for fast sim");
>>>>>>> release/21.0.127
        return 0;
      }
    }
  }
<<<<<<< HEAD
  for (unsigned int sys = 0; sys < sysObjsFast.size(); sys++) {
    m_fastSysList.resize(sysObjsFast.size());
    if (0 == setup(sysObjsFast.at(sys), m_fastSysList[sys], m_begRunNumberListFastSim, 
                   m_endRunNumberListFastSim,runNumBegin,runNumEnd)) {
      ATH_MSG_ERROR("! Could NOT setup systematic histograms for fast sim");
      return 0;
    }
  }
  for (unsigned int sys = 0; sys < sysObjsFull.size(); sys++) {
    m_sysList.resize(sysObjsFull.size());
    if (0 == setup(sysObjsFull.at(sys), m_sysList[sys], m_begRunNumberList, 
                   m_endRunNumberList,runNumBegin,runNumEnd)) {
      ATH_MSG_ERROR("! Could NOT setup systematic histograms for fast sim");
      return 0;
    }
  }
  //Toys
  if (m_doToyMC || m_doCombToyMC) {
    bool fullToysBooked = setupUncorrToySyst(objsFull,sysObjsFull,m_uncorrToyMCSystFull);
    bool fastToysBooked = setupUncorrToySyst(objsFast,sysObjsFast,m_uncorrToyMCSystFast); 
    if (fullToysBooked || fastToysBooked) {
      if (m_doToyMC) {
        ATH_MSG_DEBUG("Created tables for " << m_nToyMC << " ToyMC systematics ");
      }
      if (m_doCombToyMC) {
        ATH_MSG_DEBUG("Created tables for " << m_nToyMC << " combined ToyMC systematics ");
=======

  for (unsigned int sys = 0; sys < sysObjsFast->size(); sys++) {
    m_fastSysList.resize(sysObjsFast->size());
    if (0 ==
        this->setup(sysObjsFast->at(sys), m_fastSysList[sys], m_begRunNumberListFastSim, m_endRunNumberListFastSim)) {
      ATH_MSG_ERROR(
		    " (file: " << __FILE__ << ", line: " << __LINE__ << ") " <<
		    "! Could NOT setup systematic histograms for fast sim");
      return 0;
    }
  }

  for (unsigned int sys = 0; sys < sysObjsFull->size(); sys++) {
    m_sysList.resize(sysObjsFull->size());
    if (0 == this->setup(sysObjsFull->at(sys), m_sysList[sys], m_begRunNumberList, m_endRunNumberList)) {
      ATH_MSG_ERROR(
		    " (file: " << __FILE__ << ", line: " << __LINE__ << ") " <<
		    "! Could NOT setup systematic histograms for fast sim");
      return 0;
    }
  }

  ATH_MSG_DEBUG(" (file: " << __FILE__ << ", line: " << __LINE__ << ") " << this->getName()
		<< "Checking for (m_doToyMC || m_doCombToyMC)");

  if (m_doToyMC || m_doCombToyMC) {
    bool fullToysBooked = kFALSE;
    bool fastToysBooked = kFALSE;

    if (m_histList["sf"].size() > 0) {
      if (objsFull->find("eig")->second->GetEntries() < 1 || objsFull->find("stat")->second->GetEntries() < 1 ||
          objsFull->find("uncorr")->second->GetEntries() < 1) {
        if (objsFull->find("stat")->second->GetEntries() > 1 || objsFull->find("sys")->second->GetEntries() > 1) {
          m_uncorrToyMCSystFull->push_back(buildToyMCTable(objsFull->find("sf")->second, NULL,
                                                           objsFull->find("stat")->second,
                                                           NULL, sysObjsFull.get()));
          fullToysBooked = kTRUE;
        }else {
          ATH_MSG_INFO(
		       "! Toy MC error propagation booked, but not all needed histograms found in the output (For full sim). Skipping toy creation!");
        }
      }else {
        m_uncorrToyMCSystFull->push_back(buildToyMCTable(objsFull->find("sf")->second, objsFull->find("eig")->second,
                                                         objsFull->find("stat")->second,
                                                         objsFull->find("uncorr")->second, sysObjsFull.get()));
        fullToysBooked = kTRUE;
      }
    }

    ///// AF2
    if (m_fastHistList["sf"].size() > 0) {
      if (objsFast->find("eig")->second->GetEntries() < 1 || objsFast->find("stat")->second->GetEntries() < 1 ||
          objsFast->find("uncorr")->second->GetEntries() < 1) {
        if (objsFast->find("stat")->second->GetEntries() > 1 || objsFast->find("sys")->second->GetEntries() > 1) {
          m_uncorrToyMCSystFast->push_back(buildToyMCTable(objsFast->find("sf")->second, NULL,
                                                           objsFast->find("stat")->second,
                                                           NULL, sysObjsFast.get()));
          fastToysBooked = kTRUE;
        }else {
          ATH_MSG_INFO(
		       "! Toy MC error propagation booked, but not all needed histograms found in the output (For fast sim). Skipping toy creation!");
        }
      }else {
        m_uncorrToyMCSystFast->push_back(buildToyMCTable(objsFast->find("sf")->second, objsFast->find("eig")->second,
                                                         objsFast->find("stat")->second,
                                                         objsFast->find("uncorr")->second, sysObjsFast.get()));
        fastToysBooked = kTRUE;
      }
    }

    if (fullToysBooked || fastToysBooked) {
      if (m_doToyMC) {
        ATH_MSG_INFO("Created tables for " << m_nToyMC << " ToyMC systematics ");
      }
      if (m_doCombToyMC) {
        ATH_MSG_INFO("Created tables for " << m_nToyMC << " combined ToyMC systematics ");
>>>>>>> release/21.0.127
      }
    }
  }
  return 1;
}
/*
 * Helper for Setting up the temporary/intermediate maps to Key -> TObjecArray from the histos
 */
void Root::TElectronEfficiencyCorrectionTool::setupTempMapsHelper(TObject* obj, 
                                                                  std::unordered_map<int, TObjArray>& objs,
                                                                  std::vector<TObjArray >& sysObjs, 
                                                                  int& seenSystematics)  {
  //Add all except the correlated 
  for (unsigned int ikey = 0; ikey < s_keys.size(); ++ikey) {
    if (TString(obj->GetName()).EndsWith("_" +  TString(mapkey::keytostring(s_keys.at(ikey))))) {
      objs.find(s_keys.at(ikey))->second.Add(obj);
    }
  }

  const TString tmpName(obj->GetName());
  //Special treatment , this is only for photons 
  if (tmpName.EndsWith("_sys")) {
    objs.find(mapkey::sys)->second.Add(obj);
    TObjArray tmpArray;
    tmpArray.Add(obj);
    sysObjs.push_back(tmpArray);
    seenSystematics++;
  }

  //See if we are dealing with correlated
  if (tmpName.Contains("_corr")) {
    /*
     * This is the worse part ...
     * corr0 triggers a few things
     * We assume that 0 is the 1st
     * histogram in a series of corr(i) that 
     * we see for each of the vector entries that 
     * can be one for LowPt,Standard,Forward etc
     */
    if (tmpName.EndsWith("corr0")) {
      /*
       * 1st create a TObjectArray
       */
      TObjArray tmpArray;
      /* 
       * Register it to the vector
       */
      sysObjs.push_back(tmpArray);
      /*
       * Reset the counter here
       */
      seenSystematics=0;
    }
    /*
     * Now we can add to the TObjeArray
     * This can be Low Pt or high Pt
     */
    sysObjs.back().Add(obj);
    /*Increase the counter*/
    seenSystematics++;
  }

  if (seenSystematics > m_nSysMax) {
    m_nSysMax = seenSystematics;
  }
}
/*
 * Helper for Setting up the uncorrelated syst for the toys
 */
bool Root::TElectronEfficiencyCorrectionTool::setupUncorrToySyst(std::unordered_map<int, TObjArray>& objs,
                                                                 std::vector<TObjArray>& sysObjs,
                                                                 std::vector< std::vector<TObjArray>>& uncorrToyMCSyst){
  bool toysBooked = false;
  if (!m_histList[mapkey::sf].empty()) {
    if (objs.find(mapkey::eig)->second.GetEntries() < 1 || objs.find(mapkey::stat)->second.GetEntries() < 1 ||
        objs.find(mapkey::uncorr)->second.GetEntries() < 1) {

      if (objs.find(mapkey::stat)->second.GetEntries() > 1 || objs.find(mapkey::sys)->second.GetEntries() > 1) {

        TObjArray dummy;
        uncorrToyMCSyst.push_back(buildToyMCTable(objs.find(mapkey::sf)->second, 
                                                  dummy,
                                                  objs.find(mapkey::stat)->second,
                                                  dummy, 
                                                  sysObjs));
        toysBooked = true;
      }else {
        ATH_MSG_DEBUG("! Toy MC error propagation booked, but not all needed" 
                      <<"Histograms found in the output (For full sim). Skipping toy creation!");
      }
    }else {
      uncorrToyMCSyst.push_back(buildToyMCTable(objs.find(mapkey::sf)->second, 
                                                objs.find(mapkey::eig)->second,
                                                objs.find(mapkey::stat)->second,
                                                objs.find(mapkey::uncorr)->second, 
                                                sysObjs));
      toysBooked = true;
    }
  }

<<<<<<< HEAD
  return toysBooked;
}

/*
 * Fill and interpret the setup, depending 
 * on which histograms are found in the input file(s)
 */
int
Root::TElectronEfficiencyCorrectionTool::setup(const TObjArray& hists,
                                               std::vector< TObjArray> &histList,
                                               std::vector< unsigned int > &beginRunNumberList,
                                               std::vector< unsigned int > &endRunNumberList,
                                               const int runNumBegin,
                                               const int runNumEnd) const {
  if (hists.GetEntriesFast()==0) {
    ATH_MSG_ERROR( "! Could NOT find histogram with name *_sf in folder");
    return 0;
  }
  TH1 *tmpHist(nullptr);
  for (int i = 0; i < hists.GetEntries(); ++i) {
    tmpHist = (TH1 *) hists.At(i);
    tmpHist->SetDirectory(nullptr);
  }
  // Now, we have all the needed info. Fill the vectors accordingly
  histList.push_back(hists);
  if (!beginRunNumberList.empty()) {
    if (runNumBegin != (int) beginRunNumberList.back()) {
      beginRunNumberList.push_back(runNumBegin);
    }
  }else {
    beginRunNumberList.push_back(runNumBegin);
  }
  if (!endRunNumberList.empty()) {
    if (runNumEnd != (int) endRunNumberList.back()) {
      endRunNumberList.push_back(runNumEnd);
    }
  }else {
    endRunNumberList.push_back(runNumEnd);
  }
  return 1;
}
=======
// =============================================================================
// Fill and interpret the setup, depending on which histograms are found in the input file(s)
// =============================================================================
int
Root::TElectronEfficiencyCorrectionTool::setup(TObjArray *hists,
                                               std::vector< TObjArray * > &histList,
                                               std::vector< unsigned int > &beginRunNumberList,
                                               std::vector< unsigned int > &endRunNumberList) {
  if (!hists) {
    ATH_MSG_ERROR(
		  "(file: " << __FILE__ << ", line: " << __LINE__ << ") " << "! Could NOT find histogram with name *_sf in folder");
    return 0;
  }
  TH2 *tmpHist(0);
  for (int i = 0; i < hists->GetEntries(); ++i) {
    tmpHist = (TH2 *) hists->At(i);
    tmpHist->SetDirectory(0);
  }
  // Now, we have all the needed info. Fill the vectors accordingly
  histList.push_back(hists);
  if (beginRunNumberList.size() > 0) {
    if (m_runNumBegin != (int) beginRunNumberList.back()) {
      beginRunNumberList.push_back(m_runNumBegin);
    }
  }else {
    beginRunNumberList.push_back(m_runNumBegin);
  }
  if (endRunNumberList.size() > 0) {
    if (m_runNumEnd != (int) endRunNumberList.back()) {
      endRunNumberList.push_back(m_runNumEnd);
    }
  }else {
    endRunNumberList.push_back(m_runNumEnd);
  }

  return 1;
}

// =============================================================================
// print a message that the default scale factor is returned
// =============================================================================
void
Root::TElectronEfficiencyCorrectionTool::printDefaultReturnMessage(TString reason, int line) {
  ATH_MSG_DEBUG(
		this->getName() << " (file: " << __FILE__ << ", line: " << line << ")  " << reason << "\n" <<
		"Returning scale factor -999 ");
}
>>>>>>> release/21.0.127
