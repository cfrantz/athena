<<<<<<< HEAD
# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
# $Id: CMakeLists.txt 788319 2016-12-07 01:04:30Z christos $
################################################################################
# Package: IsolationCorrections
################################################################################
>>>>>>> release/21.0.127

# Declare the package name:
atlas_subdir( IsolationCorrections )

# Extra dependencies, based on the environment:
<<<<<<< HEAD
set( extra_libs )
if( NOT XAOD_STANDALONE )
   set( extra_libs AthAnalysisBaseCompsLib )
endif()

# External dependencies:
find_package( ROOT COMPONENTS Core Hist RIO Matrix)
find_package( Boost )
=======
set( extra_deps )
if( NOT XAOD_STANDALONE )
   set( extra_deps GaudiKernel 
   Control/AthenaBaseComps	       
   Control/AthAnalysisBaseComps)
endif()

# Declare the package's dependencies:
atlas_depends_on_subdirs(
   PUBLIC
   Control/AthToolSupport/AsgTools
   Event/xAOD/xAODEgamma
   Event/xAOD/xAODEventInfo
   Event/xAOD/xAODPrimitives
   Event/xAOD/xAODTracking
   Event/xAOD/xAODMetaData
   PhysicsAnalysis/AnalysisCommon/PATInterfaces
   PhysicsAnalysis/AnalysisCommon/PATCore	
   PRIVATE
   Tools/PathResolver
   ${extra_deps} )

# External dependencies:
find_package( ROOT COMPONENTS Core Hist RIO )
>>>>>>> release/21.0.127

atlas_add_library( IsolationCorrectionsLib
   IsolationCorrections/*.h Root/*.cxx
   PUBLIC_HEADERS IsolationCorrections
   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
<<<<<<< HEAD
   PRIVATE_INCLUDE_DIRS ${Boost_INCLUDE_DIRS}
   LINK_LIBRARIES ${ROOT_LIBRARIES} AsgMessagingLib AsgTools xAODEgamma xAODEventInfo xAODEventShape
   xAODPrimitives xAODTracking PATInterfaces PATCoreAcceptLib
   PRIVATE_LINK_LIBRARIES ${Boost_LIBRARIES} PathResolver ${extra_libs} )
=======
   LINK_LIBRARIES ${ROOT_LIBRARIES} AsgTools xAODEgamma xAODEventInfo
   xAODPrimitives xAODTracking PATInterfaces PATCoreLib
   PRIVATE_LINK_LIBRARIES PathResolver xAODMetaData)
>>>>>>> release/21.0.127

if( NOT XAOD_STANDALONE )
   atlas_add_component( IsolationCorrections
      src/components/*.cxx
<<<<<<< HEAD
      LINK_LIBRARIES IsolationCorrectionsLib )
=======
      LINK_LIBRARIES GaudiKernel AthenaBaseComps AthAnalysisBaseCompsLib IsolationCorrectionsLib )
>>>>>>> release/21.0.127
endif()

atlas_add_dictionary( IsolationCorrectionsDict
   IsolationCorrections/IsolationCorrectionsDict.h
   IsolationCorrections/selection.xml
   LINK_LIBRARIES IsolationCorrectionsLib )
<<<<<<< HEAD
=======

# Install files from the package:
atlas_install_data( data/*.root )

>>>>>>> release/21.0.127
