/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "MuPatTrackBuilder.h"
#include "MuonRecHelperTools/IMuonEDMHelperSvc.h"

#include "StoreGate/DataHandle.h"
#include "TrkSegment/SegmentCollection.h"
#include "TrkTrack/Track.h"
#include "TrkTrack/TrackCollection.h"
#include "MuonSegment/MuonSegment.h"

#include "TrkTrack/TrackStateOnSurface.h"
#include "Particle/TrackParticleContainer.h"
#include <vector>

using namespace Muon;

StatusCode MuPatTrackBuilder::initialize()
{
  if (m_trackMaker.retrieve().isFailure()){
    msg(MSG::FATAL) <<"Could not get " << m_trackMaker <<endmsg; 
    return StatusCode::FAILURE;
  }
<<<<<<< HEAD
  if (m_edmHelperSvc.retrieve().isFailure()){
    msg(MSG::FATAL) <<"Could not get " << m_edmHelperSvc <<endmsg; 
    return StatusCode::FAILURE;
  }
  if( msgLvl(MSG::DEBUG) ) msg(MSG::DEBUG) << "Retrieved " << m_trackMaker << endmsg;
  
  ATH_CHECK( m_segmentKey.initialize() );
  ATH_CHECK( m_spectroTrackKey.initialize() );
=======
  if (m_helper.retrieve().isFailure()){
    msg(MSG::FATAL) <<"Could not get " << m_helper <<endmsg; 
    return StatusCode::FAILURE;
  }
  if( msgLvl(MSG::DEBUG) ) msg(MSG::DEBUG) << "Retrieved " << m_trackMaker << endmsg;

/*  if (!m_convTool.empty()) {
    if (m_convTool.retrieve().isFailure()){
      ATH_MSG_FATAL ("Could not get track converter " << m_convTool << ". STACO will have problems.");
      return StatusCode::FAILURE;
    } else {
      ATH_MSG_DEBUG ("Retrieved " << m_convTool);
    }
  }
>>>>>>> release/21.0.127

  if ( not m_monTool.name().empty() ) {
    ATH_CHECK( m_monTool.retrieve() );
  }

  return StatusCode::SUCCESS; 
}

StatusCode MuPatTrackBuilder::execute()
{
  typedef std::vector<const Muon::MuonSegment*> MuonSegmentCollection;

<<<<<<< HEAD
  SG::ReadHandle<Trk::SegmentCollection> segmentColl (m_segmentKey);
  if (!segmentColl.isValid() ) {
    msg(MSG::WARNING) << "Could not find MuonSegmentCollection at " << segmentColl.name() <<endmsg;
    return StatusCode::RECOVERABLE;
  }
    
  if( !segmentColl.cptr() ) {
    msg(MSG::WARNING) << "Obtained zero pointer for MuonSegmentCollection at " << segmentColl.name() <<endmsg;
    return StatusCode::RECOVERABLE;
  }
      
  if( msgLvl(MSG::DEBUG) ) msg(MSG::DEBUG) << "Retrieved MuonSegmentCollection "  << segmentColl->size() << endmsg;
=======
  const DataHandle<Trk::SegmentCollection> segCol;
  if (evtStore()->retrieve(segCol,m_segmentLocation).isFailure() ) {
    msg(MSG::WARNING) << "Could not find MuonSegmentCollection at " << m_segmentLocation <<endmsg;
    return StatusCode::RECOVERABLE;
  }
    
  if( !segCol ) {
    msg(MSG::WARNING) << "Obtained zero pointer for MuonSegmentCollection at " << m_segmentLocation <<endmsg;
    return StatusCode::RECOVERABLE;
  }
      
  if( msgLvl(MSG::DEBUG) ) msg(MSG::DEBUG) << "Retrieved MuonSegmentCollection "  << segCol->size() << endmsg;
>>>>>>> release/21.0.127

  MuonSegmentCollection msc;
  msc.reserve(segmentColl->size());
  for (unsigned int i=0;i<segmentColl->size();++i){
    if (!segmentColl->at(i)) continue;
    const Muon::MuonSegment * ms = dynamic_cast<const Muon::MuonSegment*>(segmentColl->at(i));
    if (ms) msc.push_back( ms );
  }

<<<<<<< HEAD
  if (msc.size() != segmentColl->size()){
    msg(MSG::WARNING) << "Input segment collection (size " << segmentColl->size() << ") and translated MuonSegment collection (size "
=======
  if (msc.size() != segCol->size()){
    msg(MSG::WARNING) << "Input segment collection (size " << segCol->size() << ") and translated MuonSegment collection (size "
>>>>>>> release/21.0.127
                      << msc.size() << ") are not the same size." << endmsg;
  }

  TrackCollection * newtracks = m_trackMaker->find(msc);
  if (!newtracks) newtracks = new TrackCollection();

<<<<<<< HEAD
  SG::WriteHandle<TrackCollection> spectroTracks(m_spectroTrackKey); 	  
  if (spectroTracks.record(std::unique_ptr<TrackCollection>(newtracks)).isFailure()){    
      ATH_MSG_WARNING( "New Track Container " << spectroTracks.name() << " could not be recorded in StoreGate !");
      return StatusCode::RECOVERABLE;
  }
  ATH_MSG_DEBUG ("TrackCollection '" << m_spectroTrackKey.key() << "' recorded in storegate, ntracks: " << newtracks->size());

  //---------------------------------------------------------------------------------------------------------------------//
  //------------                Monitoring of muon segments and tracks inside the trigger algs               ------------//
  //------------ Author:  Laurynas Mince                                                                     ------------//
  //------------ Created: 03.10.2019                                                                         ------------//
  //---------------------------------------------------------------------------------------------------------------------//

  // Only run monitoring for online algorithms
  if ( not m_monTool.name().empty() ) {
    auto mstrks_n   = Monitored::Scalar<int>("mstrks_n", newtracks->size());
    auto mstrks_pt  = Monitored::Collection("mstrks_pt", *newtracks,
                      [](auto const& mstrk) {return mstrk->perigeeParameters()->momentum().perp()/1000.0;}); // pT converted to GeV
    auto mstrks_eta = Monitored::Collection("mstrks_eta", *newtracks,
                      [](auto const& mstrk) {return -log(tan(mstrk->perigeeParameters()->parameters()[Trk::theta] *0.5));});
    auto mstrks_phi = Monitored::Collection("mstrks_phi", *newtracks,
                      [](auto const& mstrk) {return mstrk->perigeeParameters()->parameters()[Trk::phi0];});
    auto mssegs_n   = Monitored::Scalar<int>("mssegs_n", msc.size());
    auto mssegs_eta = Monitored::Collection("mssegs_eta", msc, [](auto const& seg) {return seg->globalDirection().eta();});
    auto mssegs_phi = Monitored::Collection("mssegs_phi", msc, [](auto const& seg) {return seg->globalDirection().phi();});

    auto monitorIt = Monitored::Group(m_monTool, mstrks_n, mstrks_pt, mstrks_eta, mstrks_phi, mssegs_n, mssegs_eta, mssegs_phi);
=======
  if (m_extrapTrackLocation == ""){

    // Record the track collection for a track builder reporting params only in MS
    //
    if (evtStore()->record(newtracks,m_spectroTrackLocation,false).isFailure()){
      msg(MSG::WARNING) << "New Track Container " << m_spectroTrackLocation << " could not be recorded in StoreGate !" << endmsg;
      delete newtracks;
      return StatusCode::RECOVERABLE;
    }
    if( msgLvl(MSG::DEBUG) ) msg(MSG::DEBUG) << "TrackCollection '" << m_spectroTrackLocation << "' recorded in storegate, ntracks: " << newtracks->size() << endmsg;

    // how about particle making here???

  } else {

    // Record two track collections, with and without extrapolated parameters
    if (evtStore()->record(newtracks,m_spectroTrackLocation,false).isFailure()){
      msg(MSG::WARNING) << "New Track Container " << m_spectroTrackLocation << " could not be recorded in StoreGate !" << endmsg;
      delete newtracks;
      return StatusCode::RECOVERABLE;
    }
    ATH_MSG_DEBUG ("TrackCollection '" << m_spectroTrackLocation << "' recorded in storegate, ntracks: " << newtracks->size());

    // Make track particles out of it and re-record
/*    if (!m_convTool.empty()) {
      Rec::TrackParticleContainer * recpart = new Rec::TrackParticleContainer();
      if (newtracks->empty() || m_convTool->convertCollection( newtracks , recpart ).isSuccess() ){
        ATH_MSG_DEBUG ("Sucessfully converted first track collection to track particles, "<<
                       "now have container of size "<<recpart->size());
        evtStore()->record(recpart,m_spectroPartiLocation,false).ignore();
      } else {
        ATH_MSG_DEBUG ("Could not convert first track collection to track particles!");
        delete recpart;
      }
    }
 
    // BackTrack
    TrackCollection * BkTk_TrackCollection = new TrackCollection();
    for (unsigned int tk=0;tk<newtracks->size();++tk){
      if ( !newtracks->at(tk) ) continue;
      Trk::Track* pTrack      = (*newtracks)[tk] ;
      Trk::Track* BkTk_pTrack = p_IMuonBackTracker->MuonBackTrack(pTrack) ;
      if (BkTk_pTrack){
        BkTk_TrackCollection->push_back( BkTk_pTrack );
      }else{
	ATH_MSG_WARNING("Failed to create MS only track, copying track at IP ");
        BkTk_TrackCollection->push_back(  new Trk::Track(*(*newtracks)[tk]) );
      }
    }
    if (evtStore()->record(BkTk_TrackCollection,m_extrapTrackLocation,false).isFailure()){
      msg(MSG::WARNING) << "New Track Container " << m_extrapTrackLocation << " could not be recorded in StoreGate !" << endmsg;
      delete BkTk_TrackCollection;
      return StatusCode::RECOVERABLE;
    }
    if( msgLvl(MSG::DEBUG) ) msg(MSG::DEBUG) << "TrackCollection '" << m_extrapTrackLocation << "' recorded in storegate, ntracks: " << BkTk_TrackCollection->size() << endmsg;

    // Make track particles out of that and re-record
    if (!m_convTool.empty()) {
      Rec::TrackParticleContainer * BkTk_TrackParticleContainer = new Rec::TrackParticleContainer();
      if (BkTk_TrackCollection->empty() || m_convTool->convertCollection( BkTk_TrackCollection , BkTk_TrackParticleContainer ).isSuccess() ){
        ATH_MSG_DEBUG ("Sucessfully converted modified track collection to track particles, "<<
                       "now have container of size "<<BkTk_TrackParticleContainer->size());
        evtStore()->record(BkTk_TrackParticleContainer,m_extrapPartiLocation,false).ignore();
      } else {
        ATH_MSG_DEBUG ("Could not convert first track collection to track particles!");
        delete BkTk_TrackParticleContainer;
      }
    }*/
>>>>>>> release/21.0.127
  }

  //Memory cleanup ... 
  m_trackMaker->cleanUp();
  return StatusCode::SUCCESS;
} // execute

