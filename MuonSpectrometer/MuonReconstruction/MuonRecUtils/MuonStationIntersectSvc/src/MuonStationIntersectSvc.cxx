/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "MuonStationIntersectSvc/MuonStationIntersectSvc.h"

#include "MuonReadoutGeometry/MuonDetectorManager.h"
#include "MuonStationIntersectSvc/MuonIntersectGeometry.h"
#include "MuonStationIntersectSvc/MdtIntersectGeometry.h"
#include "MuonCondData/MdtCondDbData.h"

MuonStationIntersectSvc::MuonStationIntersectSvc(const std::string& name,ISvcLocator* sl) :
  AthService(name,sl) {
}

// queryInterface
StatusCode MuonStationIntersectSvc::queryInterface(const InterfaceID& riid, void** ppvIF)
{
  if(IID_IMuonStationIntersectSvc.versionMatch(riid) )
    {
      *ppvIF = (MuonStationIntersectSvc*)this;
    } else {
    return Service::queryInterface(riid, ppvIF);
  }
  return StatusCode::SUCCESS;
}


StatusCode MuonStationIntersectSvc::initialize()
{
<<<<<<< HEAD
  ATH_CHECK(m_idHelperSvc.retrieve());
  return StatusCode::SUCCESS;
}

const std::vector<std::unique_ptr<Muon::MdtIntersectGeometry> > MuonStationIntersectSvc::getStationGeometry( const Identifier& id, const MdtCondDbData* dbData, 
													      const MuonGM::MuonDetectorManager* detMgr ) const {
  // get ids of geometry associated with identifier
  const std::vector<Identifier> chambers = binPlusneighbours( id );
=======
  m_imp->m_log = new MsgStream(msgSvc(), name());
  m_imp->m_debug = m_imp->m_log->level() <= MSG::DEBUG;
  m_imp->m_verbose = m_imp->m_log->level() <= MSG::VERBOSE;

  if ( AthService::initialize().isFailure() ) {
    *m_imp->m_log << MSG::ERROR << "Service::initialise() failed" << endmsg;
    return StatusCode::FAILURE;
  }

  StoreGateSvc* detStore=0;
  if ( serviceLocator()->service("DetectorStore", detStore).isSuccess() ) {
    if ( detStore->retrieve( m_imp->m_detMgr ).isFailure() ) {
      *m_imp->m_log << MSG::ERROR << " Cannot retrieve MuonDetectorManager " << endmsg;
      return StatusCode::FAILURE;
    }
  }else{
    return StatusCode::FAILURE;
  }

//   // Set to be listener for end of event
  IIncidentSvc* incSvc;
  if (service("IncidentSvc",incSvc).isFailure()) {
    *m_imp->m_log << MSG::ERROR << "Unable to get the IncidentSvc" << endmsg;
    return StatusCode::FAILURE;
  }
  long int pri=100;
  incSvc->addListener( this, "EndEvent", pri);

  if( m_imp->m_idHelper.retrieve().isFailure() ){
    *m_imp->m_log << MSG::ERROR << "Failed to retrieve " << m_imp->m_idHelper << endmsg;
    return StatusCode::FAILURE;
  }

  if( m_imp->m_mdtSummarySvc.retrieve().isFailure() ){
    *m_imp->m_log << MSG::ERROR << "Failed to retrieve " << m_imp->m_mdtSummarySvc << endmsg;
    return StatusCode::FAILURE;
  }


  delete m_imp->m_log;
  m_imp->m_log = new MsgStream(msgSvc(), name());
  m_imp->m_debug = m_imp->m_log->level() <= MSG::DEBUG;
  m_imp->m_verbose = m_imp->m_log->level() <= MSG::VERBOSE;

  // create geometry
  m_imp->initGeometry();

  return StatusCode::SUCCESS;
}

StatusCode MuonStationIntersectSvc::finalize() {
  if( m_imp->m_debug ) *m_imp->m_log << MSG::DEBUG << "Finalizing " << endmsg;

  // free memory of geometry
  m_imp->clearGeometry();
  delete m_imp->m_log;

  return StatusCode::SUCCESS;;
}

const std::vector<const Muon::MuonIntersectGeometry*>& MuonStationIntersectSvc::getStationGeometry( const Identifier& id ) const {

  // get bins of geometry associated with identifier
  const std::vector<int>& chambers = m_imp->binPlusneighbours( id );
>>>>>>> release/21.0.127

  // vector to store result
  std::vector<std::unique_ptr<Muon::MdtIntersectGeometry> > stations;

  // loop over bins, retrieve geometry
  for( const auto& chId : chambers){
    if(dbData){
      if(!dbData->isGoodStation(chId)){
	ATH_MSG_VERBOSE ("chamber "<<m_idHelperSvc->toString(chId)<<" is dead");
	continue;
      }
    }
    stations.push_back(std::unique_ptr<Muon::MdtIntersectGeometry>(new Muon::MdtIntersectGeometry( chId, detMgr,dbData,&this->msgStream(),m_idHelperSvc.get())));
  }
  return stations;
}

const Muon::MuonStationIntersect
MuonStationIntersectSvc::tubesCrossedByTrack( const Identifier& id,
					      const Amg::Vector3D& pos,
					      const Amg::Vector3D& dir,
					      const MdtCondDbData* dbData,
					      const MuonGM::MuonDetectorManager* detMgr) const
{
  const std::vector<std::unique_ptr<Muon::MdtIntersectGeometry> >& stations = getStationGeometry( id, dbData, detMgr );
  std::vector<std::unique_ptr<Muon::MdtIntersectGeometry> >::const_iterator it = stations.begin();
  std::vector<std::unique_ptr<Muon::MdtIntersectGeometry> >::const_iterator it_end = stations.end();

<<<<<<< HEAD
  ATH_MSG_DEBUG(" Calculating intersections for chamber " << m_idHelperSvc->toString(id)
		<< " accounting for " << stations.size()<<" stations");
=======
  if( m_imp->m_debug ){
    *m_imp->m_log << MSG::DEBUG << " Calculating holes for chamber " << m_imp->m_idHelper->toString(id)
		  << " accounted stations " << stations.size() << endmsg;
  }
>>>>>>> release/21.0.127

  Muon::MuonStationIntersect::TubeIntersects tubeIntersects;
  for( ; it!=it_end; ++it ){

<<<<<<< HEAD
    const Muon::MuonStationIntersect intersect = (*it)->intersection(pos,dir);
=======
    if( m_imp->m_verbose ){
      const Muon::MdtIntersectGeometry* geo = dynamic_cast<const Muon::MdtIntersectGeometry*>(*it);
      if( geo ){
	const TrkDriftCircleMath::MdtChamberGeometry* mdtChamberGeometry = geo->mdtChamberGeometry();
	*m_imp->m_log << MSG::VERBOSE << " In chamber " << m_imp->m_idHelper->toString(geo->chamberId()) << " address " << geo << endmsg;
	if( mdtChamberGeometry ) mdtChamberGeometry->print();
      }
    }
    const Muon::MuonStationIntersect& intersect = (*it)->intersection(pos,dir);
>>>>>>> release/21.0.127
    tubeIntersects.insert( tubeIntersects.end(), intersect.tubeIntersects().begin(),
			   intersect.tubeIntersects().end() );
  }

  Muon::MuonStationIntersect intersection;
  intersection.setTubeIntersects( tubeIntersects );
  return intersection;
}

const std::vector<Identifier> MuonStationIntersectSvc::binPlusneighbours( const Identifier& id ) const
{

  std::vector<Identifier> chIds;
  int stName = m_idHelperSvc->mdtIdHelper().stationName(id);
  int stPhi = m_idHelperSvc->mdtIdHelper().stationPhi(id);
  int stEta = m_idHelperSvc->mdtIdHelper().stationEta(id);
  bool isBarrel = m_idHelperSvc->mdtIdHelper().isBarrel( id );
  int stEtaMin=m_idHelperSvc->mdtIdHelper().stationEtaMin(id);
  int stEtaMax=m_idHelperSvc->mdtIdHelper().stationEtaMax(id);

  int chEtaLeft = stEta-1;
  int chEtaRight = stEta+1;

  // chamber with smallest eta
  if( chEtaLeft < stEtaMin ) chEtaLeft = -999;

  // chamber with largest eta
  if( chEtaRight > stEtaMax ) chEtaRight = -999;

  Muon::MuonStationIndex::ChIndex chIndex = m_idHelperSvc->chamberIndex(id);

  // special treatment of EOS chambers
  if( chIndex == Muon::MuonStationIndex::EOS ) {
    chEtaRight = -999;
    chEtaLeft  = -999;
  }

  if( isBarrel ){
    // eta = 0 doesn't exist take next
    if( chEtaLeft == 0 ) chEtaLeft -= 1;
    if( chEtaRight == 0 ) chEtaRight += 1;
  }else{
    // do not combined positive and negative endcaps
    if( chEtaLeft == 0 ) chEtaLeft  = -999;
    if( chEtaRight == 0 ) chEtaRight = -999;
  }

  // no neighbours for BIS8
  if( chIndex == Muon::MuonStationIndex::BIS && std::abs(stEta) == 8 ){
    chEtaLeft  = -999;
    chEtaRight = -999;
  }

  // BIS 8 never neighbour of a chamber
  if( chIndex == Muon::MuonStationIndex::BIS ){
<<<<<<< HEAD
    if(std::abs(chEtaLeft) == 8 )  chEtaLeft  = -999;
    if(std::abs(chEtaRight) == 8 )  chEtaRight  = -999;
=======
    if( abs(chEtaLeft) == 8 )  chEtaLeft  = -999;
    if(abs(chEtaRight) == 8 )  chEtaRight  = -999;
  }

  if( chEtaLeft != -999 ) m_bins.push_back( toBin( stName, stPhi, chEtaLeft ) );
  m_bins.push_back( toBin( stName, stPhi, stEta ) );
  if( chEtaRight != -999 ) m_bins.push_back( toBin( stName, stPhi, chEtaRight ) );

  if( m_verbose ) {
    *m_log << MSG::VERBOSE << " returning hashes for id " << m_idHelper->toString(id) << " nhashes " << m_bins.size()
	   << "   eta " << chEtaLeft  << "     " << stEta << "     " << chEtaRight << " bins ";
    for( unsigned int i=0;i<m_bins.size();++i ) *m_log << " " << m_bins[i];
    *m_log << endmsg;
  }

  return m_bins;
}

void MuonStationIntersectSvc::Imp::initGeometry() const
{

  // only do this once
  if( m_geometry.empty() ) {
    m_bins.reserve(3);
    m_stations.reserve(3);
    m_stNameMax = m_idHelper->mdtIdHelper().stationNameIndexMax();
    m_stPhiMax  = m_idHelper->mdtIdHelper().stationPhiMax();
    int stPhiMin = m_idHelper->mdtIdHelper().stationPhiMin();

    m_stEtaMinEB.push_back(m_idHelper->mdtIdHelper().stationEtaMin(false));
    m_stEtaMinEB.push_back(m_idHelper->mdtIdHelper().stationEtaMin(true));
    m_stEtaMaxEB.push_back(m_idHelper->mdtIdHelper().stationEtaMax(false));
    m_stEtaMaxEB.push_back(m_idHelper->mdtIdHelper().stationEtaMax(true));

    m_stEtaMin  = m_stEtaMinEB[0] < m_stEtaMinEB[1] ? m_stEtaMinEB[0] : m_stEtaMinEB[1];
    m_stEtaMax  = m_stEtaMaxEB[0] > m_stEtaMaxEB[1] ? m_stEtaMaxEB[0] : m_stEtaMaxEB[1];

    m_etaRange = m_stEtaMax-m_stEtaMin+1;

    int hashmax = (1+m_stNameMax)*m_stPhiMax*(m_etaRange);
    m_geometry.resize( hashmax, StationData(0,0) );
    if( m_debug ) {
      *m_log << MSG::DEBUG << " initGeometry " << std::endl
	     << " max station name index " << m_stNameMax << std::endl
	     << " station phi " << stPhiMin << "  " << m_stPhiMax << endmsg
	     << " station eta: barrel " << m_stEtaMinEB[1] << "  " << m_stEtaMaxEB[1] << std::endl
	     << " station eta: endcap " << m_stEtaMinEB[0] << "  " << m_stEtaMaxEB[0] << std::endl
	     << " station eta: range  " << m_stEtaMin << "  " << m_stEtaMax
	     << " hash max " << hashmax << endmsg;
    }

  }
  MdtIdHelper::const_id_iterator it     = m_idHelper->mdtIdHelper().module_begin();
  MdtIdHelper::const_id_iterator it_end = m_idHelper->mdtIdHelper().module_end();
  for( ; it!=it_end;++it ){


    // get pointers to geometry

    IdentifierHash chHash;
    m_idHelper->mdtIdHelper().get_module_hash( *it, chHash );
    const MuonGM::MdtReadoutElement*  mdtROEl  =  m_detMgr->getMdtReadoutElement( *it );
    const MuonGM::MdtDetectorElement* mdtDetEl =  m_detMgr->getMdtDetectorElement( chHash );

    // consistency checks
    if( (!mdtROEl && mdtDetEl) || (!mdtDetEl && mdtROEl ) ){
      *m_log << MSG::WARNING << "Incompletely geometry found for " << m_idHelper->toStringChamber(*it)
	     << " DetEl " << mdtDetEl << " ReadoutEl " << mdtROEl << endmsg;
    }

    int bin = toBin(*it);
    if( bin < 0 || bin >= (int)m_geometry.size() ){
      *m_log << MSG::WARNING << " index out of range " << bin
	     << " for " << m_idHelper->toString(*it) << endmsg;
      continue;
    }

    if( mdtDetEl ){

      // sanity check
      if( mdtDetEl->identify() != *it ){
	*m_log << MSG::ERROR << " MdtDetectorElement return by geomodel has different Id than input Id: " << endmsg
	       << " In     " << m_idHelper->toString(*it) << endmsg
	       << " DetEl  " << m_idHelper->toString(mdtDetEl->identify()) << endmsg;
      }

      // get reference to bin
      StationData& data = m_geometry[bin];
      if( data.first ) {
	*m_log << MSG::WARNING << " problem initialising MdtDetectorElement vector, bin occupied " << bin << endmsg;
      }else{

	// set pointer to MdtDetectorElement
	data.first = mdtDetEl;

	if( m_verbose ) *m_log << MSG::VERBOSE << " Adding chamber " << m_idHelper->toString(mdtDetEl->identify()) << " bin " << bin << endmsg;

	// init geometry if requested
	if( m_initGeometry ) data.second = createMdtChamberGeometry( *it );

	if( m_verbose && data.second ){
	  const Muon::MdtIntersectGeometry* geo = dynamic_cast<const Muon::MdtIntersectGeometry*>(data.second);
	  if( geo ){
	    const TrkDriftCircleMath::MdtChamberGeometry* mdtChamberGeometry = geo->mdtChamberGeometry();
	    if( mdtChamberGeometry ) mdtChamberGeometry->print();
	  }else{
	    *m_log << MSG::WARNING << " dynamic_cast to MdtIntersectGeometry failed " << endmsg;
	  }
	}
      }

      // final sanity check, the first MdtReadoutElement for the MdtDetectorElement should be the same as the one from the Identifier
      if( mdtROEl != mdtDetEl->getMdtReadoutElement(1) && mdtROEl ){
	*m_log << MSG::ERROR << " Detected inconsistency in MuonGeoModel. MdtReadoutElement pointer returned from MdtDetectorElement wrong " << endmsg
	       << " ReadoutEl     " << m_idHelper->toString(mdtROEl->identify()) << endmsg
	       << " DetEl         " << m_idHelper->toString(mdtDetEl->identify()) << endmsg;
      }
    }
  }
}

void MuonStationIntersectSvc::Imp::clearGeometry() {
  // loop over geometry and delete entries
  std::vector<StationData>::iterator it = m_geometry.begin();
  std::vector<StationData>::iterator it_end = m_geometry.end();
  for( ;it!=it_end;++it ){
    if( it->second ){ // delete if not zero
      delete it->second;
      it->second = 0;
    }
>>>>>>> release/21.0.127
  }

<<<<<<< HEAD
  if( chEtaLeft != -999 && m_idHelperSvc->mdtIdHelper().validElement(m_idHelperSvc->mdtIdHelper().elementID(stName,chEtaLeft,stPhi)) ) 
    chIds.push_back( m_idHelperSvc->mdtIdHelper().elementID(stName,chEtaLeft,stPhi));
  chIds.push_back(m_idHelperSvc->mdtIdHelper().elementID(id));
  if( chEtaRight != -999 && m_idHelperSvc->mdtIdHelper().validElement(m_idHelperSvc->mdtIdHelper().elementID(stName,chEtaRight,stPhi)) ) 
    chIds.push_back( m_idHelperSvc->mdtIdHelper().elementID( stName,chEtaRight,stPhi ) );

  ATH_MSG_VERBOSE(" returning chambers for id " << m_idHelperSvc->toString(id) << " ids " << chIds.size()
		  << "   eta " << chEtaLeft  << "     " << stEta << "     " << chEtaRight << " chambers: ");
  for( unsigned int i=0;i<chIds.size();++i ) ATH_MSG_VERBOSE(m_idHelperSvc->toString(chIds[i]));
=======
const Muon::MuonIntersectGeometry* MuonStationIntersectSvc::Imp::createMdtChamberGeometry( const Identifier& chid ) const{
  if( m_verbose ) *m_log << MSG::VERBOSE << " Creating geometry for chamber " << m_idHelper->toStringChamber(chid) << endmsg;

  // check if chamber is active using IMDTConditionsSvc
  if( !m_mdtSummarySvc->isGoodChamber(chid) ) {
    if( m_debug ) *m_log << MSG::DEBUG << " Chamber flagged as dead by IMDTConditionsSvc " << m_idHelper->toStringChamber(chid) << endmsg;
    return 0;
  }
>>>>>>> release/21.0.127

  return chIds;
}

<<<<<<< HEAD
=======
void MuonStationIntersectSvc::handle(const Incident& inc) {
  if( m_imp->m_debug ) *m_imp->m_log << MSG::DEBUG << "entering handle(), incidence type " << inc.type()
				     << " from " << inc.source() << endmsg;

  // Only clear cache for EndEvent incident
  if (inc.type() != "EndEvent") return;

  m_imp->clearGeometry();

}
>>>>>>> release/21.0.127
