/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#include "MM_Digitization/MM_IonizationCluster.h"

<<<<<<< HEAD

MM_IonizationCluster::MM_IonizationCluster () {}

MM_IonizationCluster::MM_IonizationCluster (float HitX, float IonizationX, float IonizationY) : m_HitX(HitX), m_IonizationStart(IonizationX, IonizationY) {}


void MM_IonizationCluster::createElectrons(int nElectrons) {
  m_Electrons.reserve(nElectrons);
  for (int iElectron = 0; iElectron<nElectrons; iElectron++)
    m_Electrons.push_back(std::make_unique<MM_Electron>(m_IonizationStart.X()+m_HitX, m_IonizationStart.Y()));
}

void MM_IonizationCluster::propagateElectrons(float lorentzAngle, float driftVel) {

  for (auto& Electron : m_Electrons)
    Electron->propagateElectron(lorentzAngle, driftVel);

}

// accessors
std::vector<std::unique_ptr<MM_Electron>>& MM_IonizationCluster::getElectrons() { return m_Electrons; }
=======
using namespace std;

MM_NelectronProb MM_IonizationCluster::NelectronProb;

MM_IonizationCluster::MM_IonizationCluster () {} 

MM_IonizationCluster::MM_IonizationCluster (float _HitX, float _IonizationX, float _IonizationY) : HitX(_HitX), IonizationStart(_IonizationX, _IonizationY) {}

MM_IonizationCluster::MM_IonizationCluster (const MM_IonizationCluster& _MM_IonizationCluster) {
  
  HitX = _MM_IonizationCluster.getHitX();
  IonizationStart = _MM_IonizationCluster.getIonizationStart();
  Electrons = _MM_IonizationCluster.getElectrons();
  
}

void MM_IonizationCluster::createElectrons(TRandom3* rndm) {

  gRandom = rndm;
  int Nelectron = MM_IonizationCluster::NelectronProb.FindBin(MM_IonizationCluster::NelectronProb.GetRandom());
  Electrons.reserve(Nelectron);
  for (int iElectron = 0; iElectron<Nelectron; iElectron++) 
    Electrons.push_back(new MM_Electron(HitX+IonizationStart.X(), IonizationStart.Y()));
  
}

void MM_IonizationCluster::diffuseElectrons(float LongitudinalSigma, float TransverseSigma, TRandom3* rndm) {

  for (auto& Electron : Electrons)
    Electron->diffuseElectron(LongitudinalSigma, TransverseSigma, rndm);
  
}

void MM_IonizationCluster::propagateElectrons(float driftVelx, float driftVely, float driftVel) {
  
  for (auto& Electron : Electrons)
    Electron->propagateElectron(driftVelx, driftVely, driftVel);
  
}

void MM_IonizationCluster::avalancheElectrons(float gain, TRandom3* rndm) {

  for (auto& Electron : Electrons)
    Electron->avalancheElectron(gain, rndm);
  
}
  
// accessors
std::vector<MM_Electron*> MM_IonizationCluster::getElectrons() const { return Electrons; }
>>>>>>> release/21.0.127
