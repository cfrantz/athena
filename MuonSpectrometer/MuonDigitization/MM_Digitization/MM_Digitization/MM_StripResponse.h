/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#ifndef MM_DIGITIZATION_MM_STRIPRESPONSE_H
#define MM_DIGITIZATION_MM_STRIPRESPONSE_H
//
<<<<<<< HEAD
// MM_MM_StripResponse.cxx
=======
// MM_StripResponse.cxx
>>>>>>> release/21.0.127
//     Simulate strip response
//

#include "MM_Digitization/MM_IonizationCluster.h"
#include "MM_Digitization/MM_Electron.h"

#include <map>
#include <vector>
#include <algorithm>
#include <memory>

class MM_StripResponse {

<<<<<<< HEAD
 public:

  MM_StripResponse();
  MM_StripResponse(std::vector<std::unique_ptr<MM_IonizationCluster>>& IonizationClusters, float timeResolution, float stripPitch, int stripID, int minstripID, int maxstripID);
  void timeOrderElectrons();
  void calculateTimeSeries(float thetaD, int gasgap);
  //  void calculateTimeSeries();
  void simulateCrossTalk(float crossTalk1, float crossTalk2);
=======
  float timeResolution;
  float stripPitch;
  int stripID;
  int maxstripID;

  std::vector<MM_Electron*> Electrons;

  // First argument is time bin, second argument is strip ID
  std::map< int, std::map<int,float> > stripCharges;

  // Useful info for clustering later 
  std::map<int, int> stripTimeThreshold;
  std::map<int, float> stripTotalCharge;
  std::map<int, float> stripMaxCharge;
  std::map<int, int> stripTimeMaxCharge;

  //using vector for the moment -- hopefully this has better access and is not so expensive on the memory
  std::vector<int> v_strip;
  std::vector < std::vector <float> > v_stripTimeThreshold;
  std::vector < std::vector <float> > v_stripTotalCharge;
  std::vector<float> v_stripMaxCharge;
  std::vector<float> v_stripTimeMaxCharge;

 public:

  MM_StripResponse();
  MM_StripResponse(std::vector<MM_IonizationCluster> IonizationClusters, float _timeResolution, float _stripPitch, int _stripID, int _maxstripID);
  void timeOrderElectrons();
  void calculateTimeSeries();
  void simulateCrossTalk(float crossTalk1, float crossTalk2);      
>>>>>>> release/21.0.127
  void calculateSummaries(float chargeThreshold);
  std::map<int, int> getTimeThreshold() const;
  std::map<int, float> getTotalCharge() const;
  std::map<int, float> getMaxCharge() const;
  std::map<int, int> getTimeMaxCharge() const;
<<<<<<< HEAD

=======
  
>>>>>>> release/21.0.127
  std::vector<int> getStripVec() const;
  std::vector < std::vector < float > > getTimeThresholdVec() const;
  std::vector < std::vector < float > >getTotalChargeVec() const;
  std::vector<float> getMaxChargeVec() const;
  std::vector<float> getTimeMaxChargeVec() const;

<<<<<<< HEAD
  int getNElectrons();
  float getTotalCharge();
  std::vector<std::unique_ptr<MM_Electron>>& getElectrons();

 private:

  float m_timeResolution;
  float m_stripPitch;
  int m_stripID;
  int m_minstripID;
  int m_maxstripID;

  std::vector<std::unique_ptr<MM_Electron>> m_Electrons;

  // First argument is time bin, second argument is strip ID
  std::map< int, std::map<int,float> > m_stripCharges;

  // Useful info for clustering later
  std::map<int, int> m_stripTimeThreshold;
  std::map<int, float> m_stripTotalCharge;
  std::map<int, float> m_stripMaxCharge;
  std::map<int, int> m_stripTimeMaxCharge;

  //using vector for the moment -- hopefully this has better access and is not so expensive on the memory
  std::vector<int> m_v_strip;
  std::vector < std::vector <float> > m_v_stripTimeThreshold;
  std::vector < std::vector <float> > m_v_stripTotalCharge;
  std::vector<float> m_v_stripMaxCharge;
  std::vector<float> m_v_stripTimeMaxCharge;
=======
  
>>>>>>> release/21.0.127
};

#endif
