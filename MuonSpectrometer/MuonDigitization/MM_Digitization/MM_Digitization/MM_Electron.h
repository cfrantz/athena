/*
<<<<<<< HEAD
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
=======
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
>>>>>>> release/21.0.127
*/

#ifndef MM_DIGITIZATION_MM_ELECTRON_H
#define MM_DIGITIZATION_MM_ELECTRON_H
//
// MM_Electron.cxx
//     Primary electron objects that are diffused, propagated and avalanched
//

#include <memory>

<<<<<<< HEAD
=======
#include "TF1.h"
>>>>>>> release/21.0.127
#include "TRandom3.h"
#include "TVector2.h"

class MM_Electron {

<<<<<<< HEAD
 public:

  MM_Electron();
  MM_Electron(float x, float y);
  MM_Electron(const MM_Electron& MM_Electron);

  void diffuseElectron(float LongitudinalSigma, float TransverseSigma, TRandom3* rndm);
  void setOffsetPosition(float x, float y);
  void propagateElectron(float lorentzAngle, float driftVel);
  void setTime(float Time);
  void setCharge(float Charge);

  TVector2 getOffsetPosition() const;
  float getCharge() const;
  float getTime() const;
  float getX() const;
  float getY() const;
  float getInitialX() const;
  float getInitialY() const;

 private:

  TVector2 m_initialPosition;
  TVector2 m_offsetPosition;

  float m_time;
  float m_charge;


=======
  TVector2 initialPosition;  
  TVector2 offsetPosition;
  
  float time;
  float charge;      
  

 public:

  std::unique_ptr<TF1> PolyaFunction;
  std::unique_ptr<TF1> LongitudinalDiffusionFunction;
  std::unique_ptr<TF1> TransverseDiffusionFunction;

  MM_Electron();  
  MM_Electron(float _x, float _y);  
  MM_Electron(const MM_Electron& _MM_Electron);
  
  void diffuseElectron(float LongitudinalSigma, float TransverseSigma, TRandom3* rndm);    
  void setOffsetPosition(float x, float y);
  void propagateElectron(float driftVelx, float driftVely, float driftVel);
  void avalancheElectron(float gain, TRandom3* rndm);
  void setCharge(float Charge);

  float getCharge() const;
  float getTime() const;
  float getX() const;
  float getInitialX() const;
  float getInitialY() const;
  
   
>>>>>>> release/21.0.127
};

#endif
