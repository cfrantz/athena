/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#ifndef MuonCalib_NtupleSafeCast_h
#define MuonCalib_NtupleSafeCast_h

#include "limits"
#include "cmath"

namespace MuonCalib {

  inline float NtupleSafeCast(const double &dval) {
<<<<<<< HEAD
    if(std::abs(dval) < std::numeric_limits<float>::max())
=======
    if(std::fabs(dval) < std::numeric_limits<float>::max())
>>>>>>> release/21.0.127
      return static_cast<float>(dval);
    float ret=std::numeric_limits<float>::max();
    if(dval<0)
      ret*=-1;
    return ret;
  }
	
} //namespace MuonCalib

#endif
