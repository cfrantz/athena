/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "MuonCalibNtuple/TgcCalibHitNtupleBranch.h"
#include "MuonCalibNtuple/NtupleBranchCreator.h"

#include "MuonCalibEventBase/TgcCalibHitBase.h"

#include "TTree.h"

#include <iostream>

namespace MuonCalib {

<<<<<<< HEAD
  TgcCalibHitNtupleBranch::TgcCalibHitNtupleBranch(std::string branchName) : m_branchName(branchName), m_branchesInit(false), m_index(0)
  {}

  bool TgcCalibHitNtupleBranch::fillBranch(const TgcCalibHitBase &hit, const int segmentIndex ) {
    // check if branches were initialized
    if( !m_branchesInit ){
=======
  TgcCalibHitNtupleBranch::TgcCalibHitNtupleBranch(std::string branchName) : m_branchName(branchName), branchesInit(false), index(0)
  {}

  bool TgcCalibHitNtupleBranch::fillBranch(const TgcCalibHitBase &hit, const int segmentIndex ) {
    // check if branches where initialized
    if( !branchesInit ){
      //std::cout << "TgcCalibHitNtupleBranch::fillBranch  ERROR <branches where not initialized>"
      //	<<  std::endl;
>>>>>>> release/21.0.127
      return false;    
    }

    // check if index not out of range 
    if( m_index >= m_blockSize || m_index < 0 ){
      return false;
    }

    // copy values 
<<<<<<< HEAD
    m_segIndex[m_index]    = segmentIndex;
    m_id[m_index]          = (hit.identify()).getIdInt();
    m_stripWidth[m_index]  = hit.stripWidth();
    m_stripLength[m_index] = hit.stripLength();
    m_nStrips[m_index]     = hit.nStrips();
    m_error[m_index]       = hit.error();
    m_posX[m_index]  = hit.localPosition().x();
    m_posY[m_index]  = hit.localPosition().y();
    m_posZ[m_index]  = hit.localPosition().z();
    m_gPosX[m_index] = hit.globalPosition().x();
    m_gPosY[m_index] = hit.globalPosition().y();
    m_gPosZ[m_index] = hit.globalPosition().z();
=======
    segIndex[index]    = segmentIndex;
    id[index]          = (hit.identify()).getIdInt();
    stripWidth[index]  = hit.stripWidth();
    stripLength[index] = hit.stripLength();
    nStrips[index]     = hit.nStrips();
    error[index]       = hit.error();
    posX[index]  = hit.localPosition().x();
    posY[index]  = hit.localPosition().y();
    posZ[index]  = hit.localPosition().z();
    gPosX[index] = hit.globalPosition().x();
    gPosY[index] = hit.globalPosition().y();
    gPosZ[index] = hit.globalPosition().z();
>>>>>>> release/21.0.127

    // increment hit index
    ++m_index;
  
    return true;
  }  //end TgcCalibHitNtupleBranch::fillBranch

  bool TgcCalibHitNtupleBranch::createBranch(TTree *tree) {
    // check if pointer is valid
    if( !tree ){
      return false;
    }

    // helper class to create branches in trees
    NtupleBranchCreator branchCreator(m_branchName);

    std::string index_name ="nTgc";

    // create a branch for every data member
    branchCreator.createBranch( tree, index_name, &m_index, "/I");

    // all entries of same size, the number of hits in the event
    std::string array_size( std::string("[") + m_branchName + index_name + std::string("]") );

    // create the branches
<<<<<<< HEAD
    branchCreator.createBranch( tree, "segIndex",   &m_segIndex,   array_size + "/I" );
    branchCreator.createBranch( tree, "id",         &m_id,         array_size + "/I" );
    branchCreator.createBranch( tree, "nStrips",    &m_nStrips,    array_size + "/I" );
    branchCreator.createBranch( tree, "stripWidth", &m_stripWidth, array_size + "/F" );
    branchCreator.createBranch( tree, "stripLength",&m_stripLength,array_size + "/F" );
    branchCreator.createBranch( tree, "error",      &m_error,      array_size + "/F" );
    branchCreator.createBranch( tree, "posX",       &m_posX,       array_size + "/F" );
    branchCreator.createBranch( tree, "posY",       &m_posY,       array_size + "/F" );
    branchCreator.createBranch( tree, "posZ",       &m_posZ,       array_size + "/F" );
    branchCreator.createBranch( tree, "gPosX",      &m_gPosX,      array_size + "/F" );
    branchCreator.createBranch( tree, "gPosY",      &m_gPosY,      array_size + "/F" );
    branchCreator.createBranch( tree, "gPosZ",      &m_gPosZ,      array_size + "/F" );

    m_branchesInit = true;
=======
    branchCreator.createBranch( tree, "segIndex",   &segIndex,   array_size + "/I" );
    branchCreator.createBranch( tree, "id",         &id,         array_size + "/I" );
    branchCreator.createBranch( tree, "nStrips",    &nStrips,    array_size + "/I" );
    branchCreator.createBranch( tree, "stripWidth", &stripWidth, array_size + "/F" );
    branchCreator.createBranch( tree, "stripLength",&stripLength,array_size + "/F" );
    branchCreator.createBranch( tree, "error",      &error,      array_size + "/F" );
    branchCreator.createBranch( tree, "posX",       &posX,       array_size + "/F" );
    branchCreator.createBranch( tree, "posY",       &posY,       array_size + "/F" );
    branchCreator.createBranch( tree, "posZ",       &posZ,       array_size + "/F" );
    branchCreator.createBranch( tree, "gPosX",      &gPosX,      array_size + "/F" );
    branchCreator.createBranch( tree, "gPosY",      &gPosY,      array_size + "/F" );
    branchCreator.createBranch( tree, "gPosZ",      &gPosZ,      array_size + "/F" );

    branchesInit = true;
>>>>>>> release/21.0.127
  
    // reset branch
    reset();

    return true;
  }  //end TgcCalibHitNtupleBranch::createBranch

}  //end namespace MuonCalib
