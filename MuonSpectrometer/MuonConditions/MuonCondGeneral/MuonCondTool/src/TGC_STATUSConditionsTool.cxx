/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "MuonCondTool/TGC_STATUSConditionsTool.h"

#include "SGTools/TransientAddress.h"
#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeListSpecification.h"
#include "AthenaPoolUtilities/AthenaAttributeList.h"
#include "AthenaPoolUtilities/CondAttrListCollection.h"
#include "PathResolver/PathResolver.h"
#include "GeoModelInterfaces/IGeoModelSvc.h"

#include <fstream>
#include <string>
#include <stdio.h>
#include <map>

<<<<<<< HEAD
TGC_STATUSConditionsTool::TGC_STATUSConditionsTool (const std::string& type, const std::string& name, const IInterface* parent) :
    AthAlgTool(type, name, parent),
    m_IOVSvc(nullptr),
    m_chronoSvc(nullptr) {
=======
#include "MuonCondTool/TGC_STATUSConditionsTool.h"
#include "MuonCondInterface/ITGC_STATUSConditionsTool.h"

#include "GeoModelInterfaces/IGeoModelSvc.h"



TGC_STATUSConditionsTool::TGC_STATUSConditionsTool (const std::string& type,
				      const std::string& name,
				      const IInterface* parent)
  : AthAlgTool(type, name, parent), 
    m_detStore(0),
    m_IOVSvc(0),
    m_chronoSvc(0),
    log( msgSvc(), name ),
    m_debug(false),
    m_verbose(false)  
{
>>>>>>> release/21.0.127
  declareInterface< ITGC_STATUSConditionsTool >(this);
  m_tgcDqStatusDataLocation="TgcDqStatusKey";
  declareProperty("TgcDqFolder",m_FolderName="TGC/1/DetectorStatus");
}

StatusCode TGC_STATUSConditionsTool::updateAddress(StoreID::type /*storeID*/,
                                                   SG::TransientAddress* tad,
                                                   const EventContext& /*ctx*/) {
  std::string key  = tad->name();
  return StatusCode::FAILURE;
}

<<<<<<< HEAD
StatusCode TGC_STATUSConditionsTool::initialize() {
  ATH_MSG_INFO("Initializing - folders names are: Chamber Status "<<m_FolderName);
=======

StatusCode TGC_STATUSConditionsTool::initialize()
{
  log.setLevel(msgLevel());
  m_debug = log.level() <= MSG::DEBUG;
  m_verbose = log.level() <= MSG::VERBOSE;

  log << MSG::INFO << "Initializing - folders names are: Chamber Status "<<m_FolderName << endmsg;
   
  StatusCode sc = serviceLocator()->service("DetectorStore", m_detStore);
  if ( sc.isSuccess() ) {
     if( m_debug ) log << MSG::DEBUG << "Retrieved DetectorStore" << endmsg;
  }else{
    log << MSG::ERROR << "Failed to retrieve DetectorStore" << endmsg;
    return sc;
  }
  
>>>>>>> release/21.0.127
  // Get interface to IOVSvc
  bool CREATEIF(true);
<<<<<<< HEAD
  ATH_CHECK(service( "IOVSvc", m_IOVSvc, CREATEIF));
  // initialize the chrono service
  ATH_CHECK(service("ChronoStatSvc",m_chronoSvc));
  ATH_CHECK(m_idHelperSvc.retrieve());
=======
  sc = service( "IOVSvc", m_IOVSvc, CREATEIF );
  if ( sc.isFailure() )
    {
      log << MSG::ERROR << "Unable to get the IOVSvc" << endmsg;
      return StatusCode::FAILURE;
    }
  
  if(sc.isFailure()) return StatusCode::FAILURE;
  
  
  
  // initialize the chrono service
  sc = service("ChronoStatSvc",m_chronoSvc);
  if (sc != StatusCode::SUCCESS) {
    log << MSG::ERROR << "Could not find the ChronoSvc" << endmsg;
    return sc;
  }
  if(sc.isFailure()) return StatusCode::FAILURE;

  sc = m_detStore->retrieve(m_tgcIdHelper, "TGCIDHELPER" );
  if (sc.isFailure())
    {
      log<< MSG::FATAL << " Cannot retrieve TgcIdHelper " << endmsg;
      return sc;
    }
  
    
>>>>>>> release/21.0.127
  return StatusCode::SUCCESS;
}

StatusCode TGC_STATUSConditionsTool::loadParameterStatus(IOVSVC_CALLBACK_ARGS_P(I,keys)) {
  std::list<std::string>::const_iterator itr;
  for (itr=keys.begin(); itr!=keys.end(); ++itr) {
<<<<<<< HEAD
    ATH_MSG_INFO("LoadParameters "<< *itr << " I="<<I<<" ");
=======
    log << MSG::INFO <<"LoadParameters "<< *itr << " I="<<I<<" "<<endmsg;
>>>>>>> release/21.0.127
    if(*itr==m_FolderName) {
        ATH_CHECK(loadTgcDqStatus(I,keys));
    }
  }
  return StatusCode::SUCCESS;
}

<<<<<<< HEAD
StatusCode TGC_STATUSConditionsTool::loadTgcDqStatus(IOVSVC_CALLBACK_ARGS_P(I,keys)) {
  ATH_MSG_INFO("Load Tgc Status flags  from DB");
=======



      

StatusCode TGC_STATUSConditionsTool::loadTgcDqStatus(IOVSVC_CALLBACK_ARGS_P(I,keys)) 
{
  log.setLevel(msgLevel());
  m_debug = log.level() <= MSG::DEBUG;
  m_verbose = log.level() <= MSG::VERBOSE;
 
  StatusCode sc=StatusCode::SUCCESS;
  log << MSG::INFO << "Load Tgc Status flags  from DB" << endmsg;

>>>>>>> release/21.0.127
  // Print out callback information
  if (msgLvl(MSG::DEBUG)) ATH_MSG_DEBUG("Level " << I << " Keys: ");
  std::list<std::string>::const_iterator keyIt = keys.begin();
<<<<<<< HEAD
  for (; keyIt != keys.end(); ++ keyIt) if (msgLvl(MSG::DEBUG)) ATH_MSG_DEBUG(*keyIt << " ");
  
  const CondAttrListCollection* atrc = nullptr;
  ATH_MSG_INFO("Try to read from folder <"<< m_FolderName <<">");

  ATH_CHECK(detStore()->retrieve(atrc,m_FolderName));
  ATH_MSG_INFO("CondAttrListCollection from DB folder have been obtained with size "<< atrc->size());

=======
  for (; keyIt != keys.end(); ++ keyIt)  if( m_debug ) log << MSG::DEBUG << *keyIt << " ";
   if( m_debug )  log << MSG::DEBUG << endmsg;
  
  const CondAttrListCollection * atrc;
  log << MSG::INFO << "Try to read from folder <"<< m_FolderName <<">"<<endmsg;

  sc=m_detStore->retrieve(atrc,m_FolderName);
  if(sc.isFailure())  {
    log << MSG::ERROR
	<< "could not retreive the CondAttrListCollection from DB folder " 
	<< m_FolderName << endmsg;
    return sc;
	  }
  
  else
    log<<MSG::INFO<<" CondAttrListCollection from DB folder have been obtained with size "<< atrc->size() <<endmsg;
  
 
>>>>>>> release/21.0.127
  CondAttrListCollection::const_iterator itr;
  for (itr = atrc->begin(); itr != atrc->end(); ++itr) {
     const coral::AttributeList& atr=itr->second;
     int detector_status;
 
     detector_status=*(static_cast<const int*>((atr["detector_status"]).addressOfData()));
<<<<<<< HEAD
     if (msgLvl(MSG::DEBUG)) ATH_MSG_DEBUG("TGC detector status is " << detector_status);
=======
      if( m_debug ) log << MSG::DEBUG << "TGC detector status is " << detector_status << endmsg;
>>>>>>> release/21.0.127

     if (detector_status!=0){
       int channum=itr->first;
       Identifier chamberId = m_idHelperSvc->tgcIdHelper().elementID(Identifier(channum));
       m_cachedDeadStationsId.push_back(chamberId);
     }
<<<<<<< HEAD
  }
  if (msgLvl(MSG::VERBOSE)) ATH_MSG_VERBOSE("Collection CondAttrListCollection CLID " << atrc->clID());
  return StatusCode::SUCCESS; 
=======
  } 
  
    
   if( m_debug ) log << MSG::VERBOSE << "Collection CondAttrListCollection CLID "
       << atrc->clID() << endmsg;


 
   return  sc; 
    
>>>>>>> release/21.0.127
}
