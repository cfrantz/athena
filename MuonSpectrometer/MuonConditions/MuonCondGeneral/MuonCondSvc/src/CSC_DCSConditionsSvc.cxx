/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "MuonCondSvc/CSC_DCSConditionsSvc.h"

#include "MuonCondInterface/ICSC_DCSConditionsTool.h"
#include "AthenaPoolUtilities/CondAttrListCollection.h"
//#include "PathResolver/PathResolver.h"
#include "StoreGate/StoreGateSvc.h"

#include <set>
#include <string>
#include <algorithm>

#include "Identifier/Identifier.h"
#include "Gaudi/Property.h"

#include "Identifier/IdentifierHash.h"
#include <iostream>




CSC_DCSConditionsSvc::CSC_DCSConditionsSvc(const std::string& name, ISvcLocator* pSvcLocator) :
  AthService(name, pSvcLocator),
  m_condDataTool("CSC_DCSConditionsTool")
{
 
  declareProperty( "DCSInfofromCool",m_dcsInfofromCool=true);
  declareProperty( "CSC_DCSConditionsTool",  m_condDataTool, "CSC DCS Info from COOL");
}

CSC_DCSConditionsSvc::~CSC_DCSConditionsSvc()
{
}

StatusCode CSC_DCSConditionsSvc::initialize()
{
  
<<<<<<< HEAD
  ATH_MSG_INFO( "Initializing " << name() << " - package version " 
                << PACKAGE_VERSION  );
=======
  msg(MSG::INFO) << "Initializing " << name() << " - package version " 
		 << PACKAGE_VERSION << endmsg;
>>>>>>> release/21.0.127
  
  StoreGateSvc * detStore;
  StatusCode status = service("DetectorStore",detStore);
  if (status.isFailure()) {
<<<<<<< HEAD
    ATH_MSG_FATAL( "DetectorStore service not found !"  );
  } else {
    ATH_MSG_VERBOSE( "DetectorStore service found !"  );
=======
    msg(MSG::FATAL) << "DetectorStore service not found !" << endmsg; 
  } else {
    msg(MSG::INFO) << "DetectorStore service found !" << endmsg; 
>>>>>>> release/21.0.127
    
  }
  if(m_dcsInfofromCool)
    {
      StatusCode sc = m_condDataTool.retrieve();
      if ( sc.isFailure() )
	{
	  
<<<<<<< HEAD
	  ATH_MSG_ERROR( "Could not retrieve CSC_DCSConditionsTool"  );
=======
	  msg(MSG::ERROR) << "Could not retrieve CSC_DCSConditionsTool" << endmsg;
>>>>>>> release/21.0.127
	}
      else
	{
	  
<<<<<<< HEAD
	  ATH_MSG_VERBOSE("CSC_DCSConditionsTool retrieved with statusCode = "<<sc<<" pointer = "<<m_condDataTool );
=======
	  msg(MSG::INFO)<<"CSC_DCSConditionsTool retrieved with statusCode = "<<sc<<" pointer = "<<m_condDataTool<<endmsg;
>>>>>>> release/21.0.127
	}
      std::vector<std::string> folderNames;
      folderNames.push_back((m_condDataTool)->ChamberFolder());
      folderNames.push_back((m_condDataTool)->HVFolderName());
      
      
<<<<<<< HEAD
      ATH_MSG_VERBOSE("Register call-back  against "<<folderNames.size()<<" folders listed below " );
=======
      msg(MSG::INFO)<<"Register call-back  against "<<folderNames.size()<<" folders listed below "<<endmsg;
>>>>>>> release/21.0.127
      //bool aFolderFound = false;
      short ic=0;
      for (std::vector<std::string>::const_iterator ifld =folderNames.begin(); ifld!=folderNames.end(); ++ifld )
	{
	  ++ic;
	  ATH_MSG_VERBOSE(" Folder n. "<<ic<<" <"<<(*ifld)<<">"  );
	  if (detStore->contains<CondAttrListCollection>(*ifld)) {
	    //    aFolderFound=true;
<<<<<<< HEAD
	    ATH_MSG_VERBOSE("     found in the DetStore" );
=======
	    msg(MSG::INFO)<<"     found in the DetStore"<<endmsg;
>>>>>>> release/21.0.127
	    const DataHandle<CondAttrListCollection> CSCDCSData;
	    if (detStore->regFcn(&ICSC_DCSConditionsSvc::initInfo,
				 dynamic_cast<ICSC_DCSConditionsSvc *>(this),
				 CSCDCSData,
				 *ifld)!=StatusCode::SUCCESS)
	      {
		ATH_MSG_WARNING("Unable to register call back for initDCSInfo against folder <"<<(*ifld)<<">"  );
	      }
<<<<<<< HEAD
	    else ATH_MSG_VERBOSE("initDCSInfo registered for call-back against folder <"<<(*ifld)<<">" );
	  }
	  else
	    {   
	      ATH_MSG_WARNING("Folder "<<(*ifld)
                              <<" NOT found in the DetStore --- failing to init ???" );
=======
	    else msg(MSG::INFO)<<"initDCSInfo registered for call-back against folder <"<<(*ifld)<<">"<<endmsg;
	  }
	  else
	    {   
	      msg(MSG::WARNING)<<"Folder "<<(*ifld)
			       <<" NOT found in the DetStore --- failing to init ???"<<endmsg;
>>>>>>> release/21.0.127
	    }
	}
    }
  
  return StatusCode::SUCCESS;
}

StatusCode CSC_DCSConditionsSvc::finalize()
{
  
<<<<<<< HEAD
  ATH_MSG_VERBOSE( "Finalize"  );
=======
  msg(MSG::INFO) << "Finalize" << endmsg;
>>>>>>> release/21.0.127
  return StatusCode::SUCCESS;
}


StatusCode CSC_DCSConditionsSvc::queryInterface(const InterfaceID& riid, void** ppvInterface)
{
<<<<<<< HEAD
  if(ICSC_DCSConditionsSvc::interfaceID().versionMatch(riid) )
    {
      *ppvInterface = this;      
    } else if ( ICSCConditionsSvc::interfaceID().versionMatch(riid) ) {
      *ppvInterface = dynamic_cast<ICSCConditionsSvc*>(this);
    } else {
      
      return AthService::queryInterface(riid, ppvInterface);
    }
=======
  msg(MSG::INFO) << "queryInterface Start" << endmsg;
  if(ICSC_DCSConditionsSvc::interfaceID().versionMatch(riid) )
    {
      msg(MSG::INFO) << "versionMatch=true" << endmsg;
      msg(MSG::INFO) << "OK***************************" << endmsg;
      *ppvInterface = this;      
    } else if ( ICSCConditionsSvc::interfaceID().versionMatch(riid) ) {
      *ppvInterface = dynamic_cast<ICSCConditionsSvc*>(this);
      msg(MSG::INFO) << "service cast***************************" << endmsg;
    } else {
      msg(MSG::INFO) << "cannot find the interface!***************************" << endmsg;
      
      return AthService::queryInterface(riid, ppvInterface);
    }
  msg(MSG::INFO) << "queryInterface succesfull" << endmsg;
>>>>>>> release/21.0.127
  addRef(); 
  return StatusCode::SUCCESS;
}



StatusCode CSC_DCSConditionsSvc::initInfo(IOVSVC_CALLBACK_ARGS_P(I,keys))
{
<<<<<<< HEAD
  ATH_MSG_VERBOSE("initDCSInfo has been called" );
  ATH_MSG_VERBOSE("ToolHandle in initMappingModel - <"<<m_condDataTool<<">" );
=======
  msg(MSG::INFO)<<"initDCSInfo has been called"<<endmsg;
  msg(MSG::INFO)<<"ToolHandle in initMappingModel - <"<<m_condDataTool<<">"<<endmsg;
>>>>>>> release/21.0.127
  
  if(m_dcsInfofromCool)
    {
      
      StatusCode sc = m_condDataTool->loadParameters(I, keys);
      if (sc.isFailure())
	{
<<<<<<< HEAD
	  ATH_MSG_WARNING("Reading DCS from COOL failed; NO CSC DCS INFO AVAILABLE" );
=======
	  msg(MSG::WARNING)<<"Reading DCS from COOL failed; NO CSC DCS INFO AVAILABLE"<<endmsg;
>>>>>>> release/21.0.127
	}
      
    }
  
  return StatusCode::SUCCESS;
}


bool CSC_DCSConditionsSvc::canReportAbout(MuonConditions::Hierarchy h) {
  return (h == MuonConditions::CSC_TECHNOLOGY);
} 


bool CSC_DCSConditionsSvc::isGoodChamber(const Identifier & /*Id*/) const{
  return true;
}


bool CSC_DCSConditionsSvc::isGoodWireLayer(const Identifier & /*Id*/) const{
  return true;
}

bool CSC_DCSConditionsSvc::isGood(const Identifier & /*Id*/) const{
  return true;
}


const std::vector<Identifier>& CSC_DCSConditionsSvc::deadStationsId() const{
  
  unsigned int size_new =m_condDataTool->deadStationsId().size();
 
<<<<<<< HEAD
  ATH_MSG_VERBOSE("DCS SERVICE: Number of DEAD CHAMBERS: "<<size_new  );
=======
  msg(MSG::VERBOSE)<<"DCS SERVICE: Number of DEAD CHAMBERS: "<<size_new <<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->deadStationsId();
}




const std::vector<Identifier>& CSC_DCSConditionsSvc::deadWireLayersId() const{
   
  
  unsigned int size_new =m_condDataTool->deadWireLayersId().size();
  
<<<<<<< HEAD
  ATH_MSG_VERBOSE("DCS SERVICE: Number of DEAD Wire Layer: "<<size_new  );
=======
  msg(MSG::VERBOSE)<<"DCS SERVICE: Number of DEAD Wire Layer: "<<size_new <<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->deadWireLayersId();
  
}


const std::vector<std::string>& CSC_DCSConditionsSvc::deadStations() const{
  
  unsigned int size_new =m_condDataTool->deadStations().size();
 
<<<<<<< HEAD
  ATH_MSG_VERBOSE("DCS SERVICE: Number of DEAD CHAMBERS: "<<size_new  );
=======
  msg(MSG::VERBOSE)<<"DCS SERVICE: Number of DEAD CHAMBERS: "<<size_new <<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->deadStations();
}




const std::vector<std::string>& CSC_DCSConditionsSvc::deadWireLayers() const{
   
  
  unsigned int size_new =m_condDataTool->deadWireLayers().size();
  
<<<<<<< HEAD
  ATH_MSG_VERBOSE("DCS SERVICE: Number of DEAD Wire Layer: "<<size_new  );
=======
  msg(MSG::VERBOSE)<<"DCS SERVICE: Number of DEAD Wire Layer: "<<size_new <<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->deadWireLayers();
  
}

