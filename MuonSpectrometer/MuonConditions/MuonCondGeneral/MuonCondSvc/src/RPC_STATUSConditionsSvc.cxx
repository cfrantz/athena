/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "MuonCondSvc/RPC_STATUSConditionsSvc.h"

#include "MuonCondInterface/IRpcDetectorStatusDbTool.h"
#include "AthenaPoolUtilities/CondAttrListCollection.h"
//#include "PathResolver/PathResolver.h"
#include "StoreGate/StoreGateSvc.h"

#include <set>
#include <string>
#include <algorithm>

#include "Identifier/Identifier.h"
#include "Gaudi/Property.h"

#include "Identifier/IdentifierHash.h"
#include <iostream>




RPC_STATUSConditionsSvc::RPC_STATUSConditionsSvc(const std::string& name, ISvcLocator* pSvcLocator) :
  AthService(name, pSvcLocator),
  m_condDataTool("RpcDetectorStatusDbTool"),
  m_RPC_PanelEfficiency(0)
{
 
  //  declareProperty( "DCSInfofromCool",m_dcsInfofromCool=true);
  declareProperty( "RpcDetectorStatusDbTool",  m_condDataTool, "RPC Info from COOL");
}

RPC_STATUSConditionsSvc::~RPC_STATUSConditionsSvc()
{
}

StatusCode RPC_STATUSConditionsSvc::initialize()
{
  
<<<<<<< HEAD
  ATH_MSG_INFO( "Initializing " << name() << " - package version " 
                << PACKAGE_VERSION  );
=======
  msg(MSG::INFO) << "Initializing " << name() << " - package version " 
		 << PACKAGE_VERSION << endmsg;
>>>>>>> release/21.0.127
  
  StoreGateSvc * detStore;
  StatusCode status = service("DetectorStore",detStore);
  if (status.isFailure()) {
<<<<<<< HEAD
    ATH_MSG_FATAL( "DetectorStore service not found !"  );
  } else {
    ATH_MSG_VERBOSE( "DetectorStore service found !"  );
=======
    msg(MSG::FATAL) << "DetectorStore service not found !" << endmsg; 
  } else {
    msg(MSG::INFO) << "DetectorStore service found !" << endmsg; 
>>>>>>> release/21.0.127
    
  }
  //if(m_dcsInfofromCool)
  //{
  StatusCode sc = m_condDataTool.retrieve();
  if ( sc.isFailure() )
    {
      
<<<<<<< HEAD
      ATH_MSG_ERROR( "Could not retrieve RPC_STATUSConditionsTool"  );
=======
      msg(MSG::ERROR) << "Could not retrieve RPC_STATUSConditionsTool" << endmsg;
>>>>>>> release/21.0.127
    }
  else
    {
      
<<<<<<< HEAD
      ATH_MSG_VERBOSE("RPC_STATUSConditionsTool retrieved with statusCode = "<<sc<<" pointer = "<<m_condDataTool );
=======
      msg(MSG::INFO)<<"RPC_STATUSConditionsTool retrieved with statusCode = "<<sc<<" pointer = "<<m_condDataTool<<endmsg;
>>>>>>> release/21.0.127
    }
  
  
  
  std::vector<std::string> folderNames;
  folderNames.push_back((m_condDataTool)->FolderName());
   
<<<<<<< HEAD
  ATH_MSG_VERBOSE("Register call-back  against "<<folderNames.size()<<" folders listed below " );
=======
  msg(MSG::INFO)<<"Register call-back  against "<<folderNames.size()<<" folders listed below "<<endmsg;
>>>>>>> release/21.0.127
  //bool aFolderFound = false;
  short ic=0;
  for (std::vector<std::string>::const_iterator ifld =folderNames.begin(); ifld!=folderNames.end(); ++ifld )
    {
      ++ic;
      ATH_MSG_VERBOSE(" Folder n. "<<ic<<" <"<<(*ifld)<<">" );
      if (detStore->contains<CondAttrListCollection>(*ifld)) {
	//aFolderFound=true;
<<<<<<< HEAD
	ATH_MSG_VERBOSE("     found in the DetStore" );
=======
	msg(MSG::INFO)<<"     found in the DetStore"<<endmsg;
>>>>>>> release/21.0.127
	const DataHandle<CondAttrListCollection> RPCData;
	if (detStore->regFcn(&IRPC_STATUSConditionsSvc::initInfo,
			     dynamic_cast<IRPC_STATUSConditionsSvc *>(this),
			     RPCData,
			     *ifld)!=StatusCode::SUCCESS)
	  {
	    ATH_MSG_WARNING("Unable to register call back for initDCSInfo against folder <"<<(*ifld)<<">" );
	    //return StatusCode::FAILURE;
	  }
<<<<<<< HEAD
        else ATH_MSG_VERBOSE("initDCSInfo registered for call-back against folder <"<<(*ifld)<<">" );
      }
      else
	{   
	  ATH_MSG_WARNING("Folder "<<(*ifld)
                          <<" NOT found in the DetStore --- failing to init ???" );
=======
	    else msg(MSG::INFO)<<"initDCSInfo registered for call-back against folder <"<<(*ifld)<<">"<<endmsg;
      }
      else
	{   
	  msg(MSG::WARNING)<<"Folder "<<(*ifld)
			   <<" NOT found in the DetStore --- failing to init ???"<<endmsg;
>>>>>>> release/21.0.127
	  //	      break;
	}
    }
  
  
  return StatusCode::SUCCESS;
}

StatusCode RPC_STATUSConditionsSvc::finalize()
{

<<<<<<< HEAD
  ATH_MSG_VERBOSE( "Finalize"  );
=======
     msg(MSG::INFO) << "Finalize" << endmsg;
>>>>>>> release/21.0.127
  return StatusCode::SUCCESS;
}


StatusCode RPC_STATUSConditionsSvc::queryInterface(const InterfaceID& riid, void** ppvInterface)
{
<<<<<<< HEAD
  if(IRPC_STATUSConditionsSvc::interfaceID().versionMatch(riid) )
    {
      *ppvInterface = this;      
    } else if ( IRPCConditionsSvc::interfaceID().versionMatch(riid) ) {
      *ppvInterface = dynamic_cast<IRPCConditionsSvc*>(this);
    } else {
     
      return AthService::queryInterface(riid, ppvInterface);
    }
=======
  msg(MSG::INFO) << "queryInterface Start" << endmsg;
  if(IRPC_STATUSConditionsSvc::interfaceID().versionMatch(riid) )
    {
      msg(MSG::INFO) << "versionMatch=true" << endmsg;
      msg(MSG::INFO) << "OK***************************" << endmsg;
      *ppvInterface = this;      
    } else if ( IRPCConditionsSvc::interfaceID().versionMatch(riid) ) {
      *ppvInterface = dynamic_cast<IRPCConditionsSvc*>(this);
      msg(MSG::INFO) << "service cast***************************" << endmsg;
    } else {
      msg(MSG::INFO) << "cannot find the interface!***************************" << endmsg;
     
      return AthService::queryInterface(riid, ppvInterface);
    }
  msg(MSG::INFO) << "queryInterface succesfull" << endmsg;
>>>>>>> release/21.0.127
  addRef(); 
  return StatusCode::SUCCESS;
}


	
StatusCode RPC_STATUSConditionsSvc::initInfo(IOVSVC_CALLBACK_ARGS_P(I,keys))
{
<<<<<<< HEAD
  ATH_MSG_VERBOSE("initDCSInfo has been called" );
  ATH_MSG_VERBOSE("ToolHandle in initMappingModel - <"<<m_condDataTool<<">" );
=======
  msg(MSG::INFO)<<"initDCSInfo has been called"<<endmsg;
  msg(MSG::INFO)<<"ToolHandle in initMappingModel - <"<<m_condDataTool<<">"<<endmsg;
>>>>>>> release/21.0.127
   
  //  if(m_dcsInfofromCool)
  // {
      
      StatusCode sc = m_condDataTool->loadParameterStatus(I, keys);
      if (sc.isFailure())
	{
<<<<<<< HEAD
	  ATH_MSG_WARNING("Reading DCS from COOL failed; NO INFO AVAILABLE" );
=======
	  msg(MSG::WARNING)<<"Reading DCS from COOL failed; NO INFO AVAILABLE"<<endmsg;
>>>>>>> release/21.0.127
	}
      
      // }
    
  return StatusCode::SUCCESS;
}


bool RPC_STATUSConditionsSvc::isGoodPanel(const Identifier & /*Id*/) const{

  return true;
}



bool RPC_STATUSConditionsSvc::isGoodStrip(const Identifier & /*Id*/) const{
  
  return true;
}


const std::vector<Identifier>& RPC_STATUSConditionsSvc::EffPanelId() const{
  
   
<<<<<<< HEAD
  ATH_MSG_VERBOSE("SERVICE: Number of Panel: " );
=======
  msg(MSG::VERBOSE)<<"SERVICE: Number of Panel: "<<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->EffPanelId();
}


const std::vector<Identifier>& RPC_STATUSConditionsSvc::EffStripId() const{
  
<<<<<<< HEAD
  ATH_MSG_VERBOSE("Eff Strip RPC " );
=======
  msg(MSG::VERBOSE)<<"Eff Strip RPC "<<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->EffStripId();
}

const std::vector<Identifier>& RPC_STATUSConditionsSvc::offPanelId() const{
   
<<<<<<< HEAD
  ATH_MSG_VERBOSE("DCS SERVICE: RPC  off PANEL NOT AVAILABLE: " );
=======
  msg(MSG::VERBOSE)<<"DCS SERVICE: RPC  off PANEL NOT AVAILABLE: "<<endmsg;
>>>>>>> release/21.0.127
  
  return m_cachedoffPanelId;
 
  
}

const std::vector<Identifier>& RPC_STATUSConditionsSvc::deadPanelId() const{
   
<<<<<<< HEAD
  ATH_MSG_VERBOSE("DCS SERVICE: RPC  dead PANEL NOT AVAILABLE: " );
=======
  msg(MSG::VERBOSE)<<"DCS SERVICE: RPC  dead PANEL NOT AVAILABLE: "<<endmsg;
>>>>>>> release/21.0.127
  
  return m_cacheddeadPanelId;
 
  
}
const std::map<Identifier,double>& RPC_STATUSConditionsSvc::RPC_EfficiencyMap(){
  
<<<<<<< HEAD
  ATH_MSG_VERBOSE("Efficiency Map per RPC panel" );
=======
  msg(MSG::VERBOSE)<<"Efficiency Map per RPC panel"<<endmsg;
>>>>>>> release/21.0.127
  //std::cout<<"Efficiency Map per RPC panel: "<<m_condDataTool->RPC_EfficiencyMap().size() <<std::endl;
  return m_condDataTool->RPC_EfficiencyMap();
}
const std::map<Identifier,double>& RPC_STATUSConditionsSvc::RPC_EfficiencyGapMap(){
  
<<<<<<< HEAD
  ATH_MSG_VERBOSE("EfficiencyGap Map per RPC panel" );
=======
  msg(MSG::VERBOSE)<<"EfficiencyGap Map per RPC panel"<<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->RPC_EfficiencyGapMap();
}


const std::map<Identifier,double>& RPC_STATUSConditionsSvc::RPC_MeanClusterSizeMap(){
  
<<<<<<< HEAD
  ATH_MSG_VERBOSE("MeanClusterSize Map per RPC panel" );
=======
  msg(MSG::VERBOSE)<<"MeanClusterSize Map per RPC panel"<<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->RPC_MeanClusterSizeMap();
}


const std::map<Identifier,double>& RPC_STATUSConditionsSvc::RPC_FracClusterSize1Map(){
  
<<<<<<< HEAD
  ATH_MSG_VERBOSE("FracClusterSize1 Map per RPC panel" );
=======
  msg(MSG::VERBOSE)<<"FracClusterSize1 Map per RPC panel"<<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->RPC_FracClusterSize1Map();
}


const std::map<Identifier,double>& RPC_STATUSConditionsSvc::RPC_FracClusterSize2Map(){
  
<<<<<<< HEAD
  ATH_MSG_VERBOSE("FracClusterSize2 Map per RPC panel" );
=======
  msg(MSG::VERBOSE)<<"FracClusterSize2 Map per RPC panel"<<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->RPC_FracClusterSize2Map();
}

const std::map<Identifier,double>& RPC_STATUSConditionsSvc::RPC_FracClusterSize3Map(){
  
<<<<<<< HEAD
  ATH_MSG_VERBOSE("FracClusterSize3 Map per RPC panel" );
=======
  msg(MSG::VERBOSE)<<"FracClusterSize3 Map per RPC panel"<<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->RPC_FracClusterSize3Map();
}

const std::map<Identifier,std::string>& RPC_STATUSConditionsSvc::RPC_DeadStripListMap(){
  
<<<<<<< HEAD
  ATH_MSG_VERBOSE("DeadStripList Map per RPC panel" );
=======
  msg(MSG::VERBOSE)<<"DeadStripList Map per RPC panel"<<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->RPC_DeadStripListMap();
}


const std::map<Identifier,float>& RPC_STATUSConditionsSvc::RPC_FracDeadStripMap(){
  
<<<<<<< HEAD
  ATH_MSG_VERBOSE("FracDeadStrip Map per RPC panel" );
=======
  msg(MSG::VERBOSE)<<"FracDeadStrip Map per RPC panel"<<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->RPC_FracDeadStripMap();
}

const std::map<Identifier,int>& RPC_STATUSConditionsSvc::RPC_ProjectedTracksMap(){
  
<<<<<<< HEAD
  ATH_MSG_VERBOSE("ProjectedTracks Map per RPC panel" );
=======
  msg(MSG::VERBOSE)<<"ProjectedTracks Map per RPC panel"<<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->RPC_ProjectedTracksMap();
}


const std::map<Identifier,int>& RPC_STATUSConditionsSvc::RPC_DeadStripList(){
  
<<<<<<< HEAD
  ATH_MSG_VERBOSE("DeadStripList per RPC panel" );
=======
  msg(MSG::VERBOSE)<<"DeadStripList per RPC panel"<<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->RPC_DeadStripList();
}


const std::map<Identifier,std::vector<double> >& RPC_STATUSConditionsSvc::RPC_TimeMapforStrip(){
  
<<<<<<< HEAD
  ATH_MSG_VERBOSE("DeadStripList per RPC panel" );
=======
  msg(MSG::VERBOSE)<<"DeadStripList per RPC panel"<<endmsg;
>>>>>>> release/21.0.127
  
  return m_condDataTool->RPC_TimeMapforStrip();
}
