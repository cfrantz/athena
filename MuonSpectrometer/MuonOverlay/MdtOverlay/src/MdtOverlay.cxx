/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

// Andrei Gaponenko <agaponenko@lbl.gov>, 2006, 2007
// Ketevi A. Assamagan <ketevi@bnl.gov>, March 2008
// Piyali Banerjee <Piyali.Banerjee@cern.ch>, March 2011

<<<<<<< HEAD
#include <MdtOverlay/MdtOverlay.h>
=======
#include "MdtOverlay/MdtOverlay.h"

#include "StoreGate/StoreGateSvc.h"
#include "StoreGate/DataHandle.h"
#include "StoreGate/ReadHandle.h"
#include "StoreGate/WriteHandle.h"
#include "CxxUtils/make_unique.h"
>>>>>>> release/21.0.127

#include <StoreGate/ReadHandle.h>
#include <StoreGate/WriteHandle.h>

#include <IDC_OverlayBase/IDC_OverlayHelpers.h>


//================================================================
namespace Overlay
{
  /** template specialization function to add 2 MDT Digits - basically the operator+=
   * A declaration of the specialization must happen before the template is used from
   * the overlayContainer() method.  So we just put this implementation at the beginning
   * of this file.
   */
  template<>
  void mergeChannelData(MdtDigit& signalDigit,
                        const MdtDigit& bkgDigit,
                        const IDC_MuonOverlayBase *algorithm)
  {
    // We want to use the integration window provided by MdtOverlay, thus the constraint
    const MdtOverlay *parent = dynamic_cast<const MdtOverlay *>(algorithm);
    if (!parent) {
      throw std::runtime_error("mergeChannelData<MdtDigit>() called by a wrong parent algorithm? Must be MdtOverlay.");
    }
<<<<<<< HEAD

    int sig_tdc = signalDigit.tdc();
    int bkg_tdc = bkgDigit.tdc();

    /** signal masks the background - no correction to the ADC
        FIXME: Probably should return the masked hit as well */
    if ( abs(sig_tdc - bkg_tdc) > parent->adcIntegrationWindow() && sig_tdc < bkg_tdc ) {
      // do nothing - keep baseDigit.
=======
    
    // ----------------------------------------------------------------
    // confirm that the specialization is being used by printing out...
    static bool first_time = true;
    if(first_time) {
      first_time = false;
      parent->msg(MSG::INFO)<<"Overlay::mergeChannelData<MdtDigit>(): "
			    <<"MDT specific code is called for "
			    <<typeid(MdtDigit).name()
			    <<endmsg;
>>>>>>> release/21.0.127
    }
    /** Physics hit masks the background hit - no correct to the AOD
        FIXME: Probably should return the masked hit as well */
    else if ( abs(sig_tdc - bkg_tdc) > parent->adcIntegrationWindow() && sig_tdc > bkg_tdc ) {
      // Use the background digit as the final answer
      signalDigit = bkgDigit;
    }
    /** the 2 hits overlap withing the ADC integration window
        the ADC will add partially
        the TDC is from the first hit that crosses the threshold
        FIXME: how to add partially for correct - for now just add the ADD total */
    else if ( abs(sig_tdc - bkg_tdc) < parent->adcIntegrationWindow() )  {
      int tdc = std::min( signalDigit.tdc(), bkgDigit.tdc() );
      int adc = signalDigit.adc() + bkgDigit.adc();
      signalDigit = MdtDigit(signalDigit.identify(), tdc, adc);
    }
  }
<<<<<<< HEAD
} // namespace Overlay

=======

}
>>>>>>> release/21.0.127

//================================================================
MdtOverlay::MdtOverlay(const std::string &name, ISvcLocator *pSvcLocator)
  : IDC_MuonOverlayBase(name, pSvcLocator)
{
<<<<<<< HEAD
=======
  
  /** modifiable properties in job options */
  declareProperty("TempStore", m_storeGateTemp, "help");
  declareProperty("TempBkgStore", m_storeGateTempBkg, "help");
  declareProperty("mainInputMDT_Name", m_mainInputMDT_Name="MDT_DIGITS");
  declareProperty("overlayInputMDT_Name", m_overlayInputMDT_Name="MDT_DIGITS");
  declareProperty("IntegrationWindow", m_adcIntegrationWindow = 20.0 ); // in ns 
  declareProperty("CopySDO", m_copySDO=true);
  declareProperty("DigitizationTool", m_digTool);
  declareProperty("ConvertRDOToDigitTool", m_rdoTool);
  declareProperty("MDTSDO", m_sdo="MDT_SDO");
  declareProperty("CopyObject", m_copyObjects=false);
  declareProperty("CleanOverlayData", m_clean_overlay_data=false);//clean out the overlay data before doing overlay, so you only get MC hits in the output overlay
  declareProperty("CleanOverlaySignal", m_clean_overlay_signal=false);//clean out the signal MC before doing overlay
>>>>>>> release/21.0.127
}

//================================================================
StatusCode MdtOverlay::initialize()
{
<<<<<<< HEAD
  ATH_MSG_DEBUG("Initializing...");
=======
  msg(MSG::INFO) << "MdtOverlay initialized" << endmsg;

 if (m_storeGateTemp.retrieve().isFailure()) {
     ATH_MSG_FATAL("MdtOverlay::initialize(): TempStore for signal not found !");
     return StatusCode::FAILURE;
  }

  if (m_storeGateTempBkg.retrieve().isFailure()) {
     ATH_MSG_FATAL("MdtOverlay::initialize(): TempStoreBkg not found !");
     return StatusCode::FAILURE;
     }

  /** initialize the detectore store service */
  StoreGateSvc* detStore=0;
  StatusCode sc = serviceLocator()->service("DetectorStore", detStore);
  if (sc.isFailure()) {
    msg( MSG::FATAL ) << "DetectorStore service not found !" << endmsg;
    return StatusCode::FAILURE;
  }

  /** access to the CSC Identifier helper */
  sc = detStore->retrieve(m_mdtHelper, "MDTIDHELPER");
  if (sc.isFailure()) {
    msg( MSG::FATAL ) << "Could not get MdtIdHelper !" << endmsg;
    return StatusCode::FAILURE;
  } 
  else {
    msg( MSG::DEBUG ) << " Found the MdtIdHelper. " << endmsg;
  }

  if (m_digTool.retrieve().isFailure()) {
    msg( MSG::FATAL ) << "Could not retrieve MDT Digitization Tool!"
                      << endmsg;
    return StatusCode::FAILURE;
  }
  msg( MSG::DEBUG ) << "Retrieved MDT Digitization Tool." << endmsg;
  
  if (m_rdoTool.retrieve().isFailure()) {
    msg( MSG::FATAL ) << "Could not retrieve MDT RDO -> Digit Tool!"
                      << endmsg;
    return StatusCode::FAILURE;
  }
  msg( MSG::DEBUG ) << "Retrieved MDT RDO -> Digit Tool." << endmsg;
>>>>>>> release/21.0.127

  // Check and initialize keys
  ATH_CHECK( m_bkgInputKey.initialize(!m_bkgInputKey.key().empty()) );
  ATH_MSG_VERBOSE("Initialized ReadHandleKey: " << m_bkgInputKey );
  ATH_CHECK(m_signalInputKey.initialize());
  ATH_MSG_VERBOSE("Initialized ReadHandleKey: " << m_signalInputKey );
  ATH_CHECK(m_outputKey.initialize());
  ATH_MSG_VERBOSE("Initialized WriteHandleKey: " << m_outputKey );

<<<<<<< HEAD
=======
//================================================================
StatusCode MdtOverlay::overlayFinalize() 
{
  msg( MSG::INFO ) << "MdtOverlay finalized" << endmsg;
>>>>>>> release/21.0.127
  return StatusCode::SUCCESS;
}

//================================================================
<<<<<<< HEAD
StatusCode MdtOverlay::execute(const EventContext& ctx) const {
  ATH_MSG_DEBUG("execute() begin");


  const MdtDigitContainer *bkgContainerPtr = nullptr;
  if (!m_bkgInputKey.empty()) {
    SG::ReadHandle<MdtDigitContainer> bkgContainer (m_bkgInputKey, ctx);
    if (!bkgContainer.isValid()) {
      ATH_MSG_ERROR("Could not get background MDT container " << bkgContainer.name() << " from store " << bkgContainer.store());
      return StatusCode::FAILURE;
    }
    bkgContainerPtr = bkgContainer.cptr();

    ATH_MSG_DEBUG("Found background MdtDigitContainer called " << bkgContainer.name() << " in store " << bkgContainer.store());
    ATH_MSG_DEBUG("MDT Background = " << Overlay::debugPrint(bkgContainer.cptr()));
    ATH_MSG_VERBOSE("MDT background has digit_size " << bkgContainer->digit_size());
  }

  SG::ReadHandle<MdtDigitContainer> signalContainer(m_signalInputKey, ctx);
  if(!signalContainer.isValid() ) {
    ATH_MSG_ERROR("Could not get signal MDT container " << signalContainer.name() << " from store " << signalContainer.store());
    return StatusCode::FAILURE;
  }
  ATH_MSG_DEBUG("Found signal MdtDigitContainer called " << signalContainer.name() << " in store " << signalContainer.store());
  ATH_MSG_DEBUG("MDT Signal     = " << Overlay::debugPrint(signalContainer.cptr()));
  ATH_MSG_VERBOSE("MDT signal has digit_size " << signalContainer->digit_size());

  SG::WriteHandle<MdtDigitContainer> outputContainer(m_outputKey, ctx);
  ATH_CHECK(outputContainer.record(std::make_unique<MdtDigitContainer>(signalContainer->size())));
  if (!outputContainer.isValid()) {
    ATH_MSG_ERROR("Could not record output MdtDigitContainer called " << outputContainer.name() << " to store " << outputContainer.store());
    return StatusCode::FAILURE;
  }
  ATH_MSG_DEBUG("Recorded output MdtDigitContainer called " << outputContainer.name() << " in store " << outputContainer.store());

  // Do the actual overlay
  ATH_CHECK(overlayContainer(bkgContainerPtr, signalContainer.cptr(), outputContainer.ptr()));
  ATH_MSG_DEBUG("MDT Result     = " << Overlay::debugPrint(outputContainer.cptr()));


  ATH_MSG_DEBUG("execute() end");
=======
StatusCode MdtOverlay::overlayExecute() {
  msg( MSG::DEBUG ) << "MdtOverlay::execute() begin"<< endmsg;

  //----------------------------------------------------------------
  msg( MSG::VERBOSE ) << "Retrieving data input MDT container" << endmsg;

  /** In the real data stream, run RDO -> Digit converter to make Digit
      this will be used in the overlay job */
  if ( m_rdoTool->digitize().isFailure() ) {
     msg( MSG::ERROR ) << "On the fly MDT RDO -> Digit failed " << endmsg;
     return StatusCode::FAILURE;
  }

  /** in the simulation stream, run digitization of the fly
      and make Digit - this will be used as input to the overlay job */
  if ( m_digTool->digitize().isFailure() ) {
     msg( MSG::ERROR ) << "On the fly MDT digitization failed " << endmsg;
     return StatusCode::FAILURE;
  }

  /** save a copy of the MDT Digit Container in a temp store */
  if ( m_copyObjects ) this->copyMuonIDCobject<MdtDigitContainer,MdtDigit>(&*m_storeGateMC,&*m_storeGateTemp);

    SG::ReadHandle<MdtDigitContainer> dataContainer(m_mainInputMDT_Name, m_storeGateData->name());
   if (!dataContainer.isValid()) {
     msg( MSG::ERROR ) << "Could not get data MDT container " << m_mainInputMDT_Name << endmsg;
     return StatusCode::FAILURE;
     }
   ATH_MSG_INFO("MDT Data   = "<<shortPrint(dataContainer.cptr()));

   msg( MSG::VERBOSE ) << "Retrieving MC  input MDT container" << endmsg;
   SG::ReadHandle<MdtDigitContainer> mcContainer(m_overlayInputMDT_Name,  m_storeGateMC->name());
   if(!mcContainer.isValid() ) {
     msg( MSG::ERROR ) << "Could not get overlay MDT container " << m_overlayInputMDT_Name << endmsg;
     return StatusCode::FAILURE;
   }
   ATH_MSG_INFO("MDT MC   = "<<shortPrint(mcContainer.cptr()));
   /*
   MdtDigitContainer *mdt_temp_bkg = copyMuonDigitContainer<MdtDigitContainer,MdtDigit>(dataContainer.cptr());

  if ( m_storeGateTempBkg->record(mdt_temp_bkg, m_mainInputMDT_Name).isFailure() ) {
     msg( MSG::WARNING ) << "Failed to record background MdtDigitContainer to temporary background store " << endmsg;
     }
   */
   msg( MSG::VERBOSE ) << "MDT data has digit_size "<<dataContainer->digit_size()<<endmsg;

  msg( MSG::VERBOSE ) << "MDT signal data has digit_size "<<mcContainer->digit_size()<<endmsg;
 
  SG::WriteHandle<MdtDigitContainer> outputContainer(m_mainInputMDT_Name, m_storeGateOutput->name());
  outputContainer = CxxUtils::make_unique<MdtDigitContainer>(dataContainer->size());

  if(dataContainer.isValid() && mcContainer.isValid() && outputContainer.isValid()) { 
    if(!m_clean_overlay_data && !m_clean_overlay_signal){
      //Do the actual overlay
      this->overlayContainer(dataContainer.cptr(), mcContainer.cptr(), outputContainer.ptr());
    }
    else if (m_clean_overlay_data) {
      MdtDigitContainer nobkg(0);
      this->overlayContainer(&nobkg , mcContainer.cptr() , outputContainer.ptr());
    }
     else if (m_clean_overlay_signal) {
      MdtDigitContainer nomc(0);
      this->overlayContainer(dataContainer.cptr(), &nomc , outputContainer.ptr());
      }
  }
  ATH_MSG_INFO("MDT Result   = "<<shortPrint(outputContainer.cptr()));

  //----------------------------------------------------------------
  msg( MSG::DEBUG ) <<"Processing MC truth data"<<endmsg;

  // Main stream is normally real data without any MC info.
  // In tests we may use a MC generated file instead of real data.
  // Remove truth info from the main input stream, if any.
  //
  // Here we handle just MDT-specific truth classes.
  // (McEventCollection is done by the base.)

  // Now copy MDT-specific MC truth objects to the output.
  // Copy only if it is not already copied by one of other muon algorithms
  if ( m_copySDO ) 
     //this->copyAllObjectsOfType<MuonSimDataCollection>(&*m_storeGateOutput, &*m_storeGateMC);
     this->copyMuonObjects<MuonSimDataCollection>(&*m_storeGateOutput, &*m_storeGateMC, m_sdo);

  //----------------------------------------------------------------
  msg( MSG::DEBUG ) << "MdtOverlay::execute() end"<< endmsg;
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}
