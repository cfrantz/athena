/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

// Andrei Gaponenko <agaponenko@lbl.gov>, 2006, 2007
// Ketevi A. Assamagan <ketevi@bnl.gov>, March 2008
// Piyali Banerjee <Piyali.Banerjee@cern.ch>, March 2011

<<<<<<< HEAD
#include <TgcOverlay/TgcOverlay.h>
=======
#include "TgcOverlay/TgcOverlay.h"

#include "StoreGate/StoreGateSvc.h"
#include "StoreGate/DataHandle.h"
#include "StoreGate/ReadHandle.h"
#include "StoreGate/WriteHandle.h"
#include "CxxUtils/make_unique.h"
>>>>>>> release/21.0.127

#include <StoreGate/ReadHandle.h>
#include <StoreGate/WriteHandle.h>

#include <IDC_OverlayBase/IDC_OverlayHelpers.h>


//================================================================
TgcOverlay::TgcOverlay(const std::string &name, ISvcLocator *pSvcLocator) :
  IDC_MuonOverlayBase(name, pSvcLocator)
{
}

//================================================================
StatusCode TgcOverlay::initialize()
{
<<<<<<< HEAD
  ATH_MSG_DEBUG("Initializing...");

  ATH_CHECK(m_bkgInputKey.initialize());
  ATH_MSG_VERBOSE("Initialized ReadHandleKey: " << m_bkgInputKey );
  ATH_CHECK(m_signalInputKey.initialize());
  ATH_MSG_VERBOSE("Initialized ReadHandleKey: " << m_signalInputKey );
  ATH_CHECK(m_outputKey.initialize());
  ATH_MSG_VERBOSE("Initialized WriteHandleKey: " << m_outputKey );
=======
  msg( MSG::INFO ) << "TgcOverlay initialized" << endmsg;

  if (m_storeGateTemp.retrieve().isFailure()) {
     ATH_MSG_FATAL("TgcOverlay::initialize(): TempStore for signal not found !");
     return StatusCode::FAILURE;
  }

  if (m_storeGateTempBkg.retrieve().isFailure()) {
     ATH_MSG_FATAL("TgcOverlay::initialize(): TempStoreBkg not found !");
     return StatusCode::FAILURE;
  }

  /** initialize the detectore store service */
  StoreGateSvc* detStore=0;
  StatusCode sc = serviceLocator()->service("DetectorStore", detStore);
  if (sc.isFailure()) {
    msg( MSG::FATAL ) << "DetectorStore service not found !" << endmsg;
    return StatusCode::FAILURE;
  }

  /** access to the TGC Identifier helper */
  sc = detStore->retrieve(m_tgcHelper, "TGCIDHELPER");
  if (sc.isFailure()) {
    msg( MSG::FATAL ) << "Could not get TgcIdHelper !" << endmsg;
    return StatusCode::FAILURE;
  } 
  else {
    msg( MSG::DEBUG ) << " Found the TgcIdHelper. " << endmsg;
  }

  if (m_digTool.retrieve().isFailure()) {
    msg( MSG::FATAL ) << "Could not retrieve TGC Digitization Tool!"
                      << endmsg;
    return StatusCode::FAILURE;
  }
  msg( MSG::DEBUG ) << "Retrieved TGC Digitization Tool." << endmsg;
  
  if (m_rdoTool.retrieve().isFailure()) {
    msg( MSG::FATAL ) << "Could not retrieve TGC RDO -> Digit Tool!"
                      << endmsg;
    return StatusCode::FAILURE;
  }
  msg( MSG::DEBUG ) << "Retrieved TGC RDO -> Digit Tool." << endmsg;
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}

//================================================================
<<<<<<< HEAD
StatusCode TgcOverlay::execute(const EventContext& ctx) const {
  ATH_MSG_DEBUG("TgcOverlay::execute() begin");


  SG::ReadHandle<TgcDigitContainer> bkgContainer (m_bkgInputKey, ctx);
  if (!bkgContainer.isValid()) {
    ATH_MSG_ERROR("Could not get background TgcDigitContainer called " << bkgContainer.name() << " from store " << bkgContainer.store());
=======
StatusCode TgcOverlay::overlayFinalize() 
{
  msg( MSG::INFO ) << "TgcOverlay finalized" << endmsg;
  return StatusCode::SUCCESS;
}

//================================================================
StatusCode TgcOverlay::overlayExecute() {
  msg( MSG::DEBUG ) << "TgcOverlay::execute() begin"<< endmsg;

  //----------------------------------------------------------------
  /** In the real data stream, run RDO -> Digit converter to make Digit
      this will be used in the overlay job */
  if ( m_rdoTool->digitize().isFailure() ) {
    msg( MSG::ERROR ) << "On the fly TGC RDO -> Digit failed " << endmsg;
    return StatusCode::FAILURE;
  }

  /** in the simulation stream, run digitization of the fly
      and make Digit - this will be used as input to the overlay job */
  if ( m_digTool->digitize().isFailure() ) {
    msg( MSG::ERROR ) << "On the fly TGC digitization failed " << endmsg;
>>>>>>> release/21.0.127
    return StatusCode::FAILURE;
  }
  ATH_MSG_DEBUG("Found background TgcDigitContainer called " << bkgContainer.name() << " in store " << bkgContainer.store());
  ATH_MSG_DEBUG("TGC Background = " << Overlay::debugPrint(bkgContainer.cptr()));
  ATH_MSG_VERBOSE("TGC background has digit_size " << bkgContainer->digit_size());

<<<<<<< HEAD
  SG::ReadHandle<TgcDigitContainer> signalContainer(m_signalInputKey, ctx);
  if (!signalContainer.isValid() ) {
    ATH_MSG_ERROR("Could not get signal TgcDigitContainer called " << signalContainer.name() << " from store " << signalContainer.store());
    return StatusCode::FAILURE;
  }
  ATH_MSG_DEBUG("Found overlay TgcDigitContainer called " << signalContainer.name() << " in store " << signalContainer.store());
  ATH_MSG_DEBUG("TGC Signal       = " << Overlay::debugPrint(signalContainer.cptr()));
  ATH_MSG_VERBOSE("TGC signal has digit_size " << signalContainer->digit_size());

  SG::WriteHandle<TgcDigitContainer> outputContainer(m_outputKey, ctx);
  ATH_CHECK(outputContainer.record(std::make_unique<TgcDigitContainer>(bkgContainer->size())));
  if (!outputContainer.isValid()) {
    ATH_MSG_ERROR("Could not record output TgcDigitContainer called " << outputContainer.name() << " to store " << outputContainer.store());
    return StatusCode::FAILURE;
  }
  ATH_MSG_DEBUG("Recorded output TgcDigitContainer called " << outputContainer.name() << " in store " << outputContainer.store());

  // Do the actual overlay
  ATH_CHECK(overlayMultiHitContainer(bkgContainer.cptr(), signalContainer.cptr(), outputContainer.ptr()));
  ATH_MSG_DEBUG("TGC Result     = " << Overlay::debugPrint(outputContainer.cptr()));

=======
  /** save a copy of the TGC Digit Container in a temp store */
  if ( m_copyObjects )
     this->copyMuonIDCobject<TgcDigitContainer,TgcDigit>(&*m_storeGateMC,&*m_storeGateTemp);

  SG::ReadHandle<TgcDigitContainer> dataContainer(m_mainInputTGC_Name, m_storeGateData->name());
  if ( !dataContainer.isValid() ) {
    msg( MSG::ERROR ) << "Could not get data TGC container " << m_mainInputTGC_Name << endmsg;
    return StatusCode::FAILURE;
  }
  ATH_MSG_INFO("TGC Data   = "<<shortPrint(dataContainer.cptr()));

  msg( MSG::VERBOSE ) << "Retrieving MC input TGC container" << endmsg;
  SG::ReadHandle<TgcDigitContainer> mcContainer(m_overlayInputTGC_Name, m_storeGateMC->name()); 
  if(!mcContainer.isValid() ) {
    msg( MSG::ERROR ) << "Could not get overlay TGC container " << m_overlayInputTGC_Name << endmsg;
    return StatusCode::FAILURE;
  }
  ATH_MSG_INFO("TGC MC   = "<<shortPrint(mcContainer.cptr()));

  /*  TgcDigitContainer *tgc_temp_bkg = copyMuonDigitContainer<TgcDigitContainer,TgcDigit>(dataContainer.cptr());
  if ( m_storeGateTempBkg->record(tgc_temp_bkg, m_mainInputTGC_Name).isFailure() ) {
     msg( MSG::WARNING ) << "Failed to record background TgcDigitContainer to temporary background store " << endmsg;
     }*/

  SG::WriteHandle<TgcDigitContainer> outputContainer(m_mainInputTGC_Name, m_storeGateOutput->name());
  outputContainer = CxxUtils::make_unique<TgcDigitContainer>(dataContainer->size());
  //Do the actual overlay
  if(dataContainer.isValid() && mcContainer.isValid() && outputContainer.isValid()) { 
    this->overlayContainer(dataContainer.cptr(), mcContainer.cptr(), outputContainer.ptr());
  }
  ATH_MSG_INFO("TGC Result   = "<<shortPrint(outputContainer.cptr()));

  //----------------------------------------------------------------
  msg(MSG::DEBUG ) <<"Processing MC truth data"<<endmsg;
>>>>>>> release/21.0.127

  ATH_MSG_DEBUG("TgcOverlay::execute() end");

<<<<<<< HEAD
  return StatusCode::SUCCESS;
=======
  // Now copy TGC-specific MC truth objects to the output.
  if ( m_copySDO )
    this->copyObjects<MuonSimDataCollection>(&*m_storeGateOutput, &*m_storeGateMC, m_sdo);

  //----------------------------------------------------------------
  msg( MSG::DEBUG ) << "TgcOverlay::execute() end"<< endmsg;

  return StatusCode::SUCCESS; 
>>>>>>> release/21.0.127
}
