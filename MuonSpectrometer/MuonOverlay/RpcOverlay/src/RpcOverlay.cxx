/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

// Andrei Gaponenko <agaponenko@lbl.gov>, 2006, 2007
// Ketevi A. Assamagan <ketevi@bnl.gov>, March 2008
// Piyali Banerjee <Piyali.Banerjee@cern.ch>, March 2011

<<<<<<< HEAD
#include <RpcOverlay/RpcOverlay.h>
=======
#include "RpcOverlay/RpcOverlay.h"

#include "StoreGate/StoreGateSvc.h"
#include "StoreGate/DataHandle.h"
#include "StoreGate/ReadHandle.h"
#include "StoreGate/WriteHandle.h"
#include "CxxUtils/make_unique.h"

#include "GeneratorObjects/McEventCollection.h"
#include "MuonSimData/MuonSimDataCollection.h"

#include "MuonIdHelpers/RpcIdHelper.h"
#include "MuonDigitContainer/RpcDigitContainer.h"
>>>>>>> release/21.0.127

#include <StoreGate/ReadHandle.h>
#include <StoreGate/WriteHandle.h>

#include <IDC_OverlayBase/IDC_OverlayHelpers.h>


//================================================================
RpcOverlay::RpcOverlay(const std::string &name, ISvcLocator *pSvcLocator) :
  IDC_MuonOverlayBase(name, pSvcLocator)
{
}

//================================================================
StatusCode RpcOverlay::initialize()
{
<<<<<<< HEAD
  ATH_MSG_DEBUG("Initializing...");

  ATH_CHECK(m_bkgInputKey.initialize());
  ATH_MSG_VERBOSE("Initialized ReadHandleKey: " << m_bkgInputKey );
  ATH_CHECK(m_signalInputKey.initialize());
  ATH_MSG_VERBOSE("Initialized ReadHandleKey: " << m_signalInputKey );
  ATH_CHECK(m_outputKey.initialize());
  ATH_MSG_VERBOSE("Initialized WriteHandleKey: " << m_outputKey );
=======
  msg( MSG::INFO ) << "RpcOverlay initialized" << endmsg;

  if (m_storeGateTemp.retrieve().isFailure()) {
     ATH_MSG_FATAL("RpcOverlay::initialize(): TempStore for signal not found !");
     return StatusCode::FAILURE;
  }
  
  if (m_storeGateTempBkg.retrieve().isFailure()) {
     ATH_MSG_FATAL("RpcOverlay::initialize(): TempStoreBkg not found !");
     return StatusCode::FAILURE;
  }

  /** initialize the detectore store service */
  StoreGateSvc* detStore=0;
  StatusCode sc = serviceLocator()->service("DetectorStore", detStore);
  if (sc.isFailure()) {
    msg( MSG::FATAL ) << "DetectorStore service not found !" << endmsg;
    return StatusCode::FAILURE;
  }

  /** access to the CSC Identifier helper */
  sc = detStore->retrieve(m_rpcHelper, "RPCIDHELPER");
  if (sc.isFailure()) {
    msg( MSG::FATAL ) << "Could not get RpcIdHelper !" << endmsg;
    return StatusCode::FAILURE;
  } 
  else {
    msg( MSG::DEBUG ) << " Found the RpcIdHelper. " << endmsg;
  }

  if (m_digTool.retrieve().isFailure()) {
    msg( MSG::FATAL ) << "Could not retrieve RPC Digitization Tool!"
        << endmsg;
    return StatusCode::FAILURE;
  }
  msg( MSG::DEBUG ) << "Retrieved RPC Digitization Tool." << endmsg;
  
  if (m_rdoTool.retrieve().isFailure()) {
    msg( MSG::FATAL ) << "Could not retrieve RPC RDO -> Digit Tool!"
                      << endmsg;
    return StatusCode::FAILURE;
  }
  msg( MSG::DEBUG ) << "Retrieved RPC RDO -> Digit Tool." << endmsg;
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}

//================================================================
StatusCode RpcOverlay::execute(const EventContext& ctx) const
{
<<<<<<< HEAD
  ATH_MSG_DEBUG("RpcOverlay::execute() begin");


  SG::ReadHandle<RpcDigitContainer> bkgContainer (m_bkgInputKey, ctx);
  if (!bkgContainer.isValid()) {
    ATH_MSG_ERROR("Could not get background RPC container " << bkgContainer.name() << " from store " << bkgContainer.store());
    return StatusCode::FAILURE;
  }
  ATH_MSG_DEBUG("Found background RpcDigitContainer called " << bkgContainer.name() << " in store " << bkgContainer.store());
  ATH_MSG_DEBUG("RPC Background = " << Overlay::debugPrint(bkgContainer.cptr()));
  ATH_MSG_VERBOSE("RPC background has digit_size " << bkgContainer->digit_size());

  SG::ReadHandle<RpcDigitContainer> signalContainer(m_signalInputKey, ctx);
  if (!signalContainer.isValid() ) {
    ATH_MSG_ERROR("Could not get signal RPC container " << signalContainer.name() << " from store " << signalContainer.store());
    return StatusCode::FAILURE;
  }
  ATH_MSG_DEBUG("Found signal RpcDigitContainer called " << signalContainer.name() << " in store " << signalContainer.store());
  ATH_MSG_DEBUG("RPC Signal     = " << Overlay::debugPrint(signalContainer.cptr()));
  ATH_MSG_VERBOSE("RPC signal has digit_size " << signalContainer->digit_size());

  SG::WriteHandle<RpcDigitContainer> outputContainer(m_outputKey, ctx);
  ATH_CHECK(outputContainer.record(std::make_unique<RpcDigitContainer>(bkgContainer->size())));
  if (!outputContainer.isValid()) {
    ATH_MSG_ERROR("Could not record output RpcDigitContainer called " << outputContainer.name() << " to store " << outputContainer.store());
    return StatusCode::FAILURE;
  }
  ATH_MSG_DEBUG("Recorded output RpcDigitContainer called " << outputContainer.name() << " in store " << outputContainer.store());

  // Do the actual overlay
  ATH_CHECK(overlayMultiHitContainer(bkgContainer.cptr(), signalContainer.cptr(), outputContainer.ptr()));
  ATH_MSG_DEBUG("RPC Result     = " << Overlay::debugPrint(outputContainer.cptr()));


  ATH_MSG_DEBUG("RpcOverlay::execute() end");
=======
  msg( MSG::INFO ) << "RpcOverlay finalized" << endmsg;
  return StatusCode::SUCCESS;
}

//================================================================
StatusCode RpcOverlay::overlayExecute() {
  msg( MSG::DEBUG ) << "RpcOverlay::execute() begin"<< endmsg;

  //----------------------------------------------------------------

  /** In the real data stream, run RDO -> Digit converter to make Digit
      this will be used in the overlay job */
  if ( m_rdoTool->digitize().isFailure() ) {
     msg( MSG::ERROR ) << "On the fly RPC RDO -> Digit failed " << endmsg;
     return StatusCode::FAILURE;
  }

  /** in the simulation stream, run digitization of the fly
      and make Digit - this will be used as input to the overlay job */
  if ( m_digTool->digitize().isFailure() ) {
     msg( MSG::ERROR ) << "On the fly RPC digitization failed " << endmsg;
     return StatusCode::FAILURE;
  }

  /** save a copy of the RPC Digit Container in a temp store */
  if ( m_copyObjects ) 
     this->copyMuonIDCobject<RpcDigitContainer,RpcDigit>(&*m_storeGateMC,&*m_storeGateTemp);

  SG::ReadHandle<RpcDigitContainer> dataContainer(m_mainInputRPC_Name, m_storeGateData->name());
   if ( !dataContainer.isValid() ) {
     msg( MSG::ERROR ) << "Could not get data RPC container " << m_mainInputRPC_Name << endmsg;
     return StatusCode::FAILURE;
  }
   ATH_MSG_INFO("RPC Data   = "<<shortPrint(dataContainer.cptr()));

  msg( MSG::VERBOSE ) << "Retrieving MC input RPC container" << endmsg;
  SG::ReadHandle<RpcDigitContainer> mcContainer(m_overlayInputRPC_Name, m_storeGateMC->name());
  if(!mcContainer.isValid()) {
    msg( MSG::ERROR ) << "Could not get overlay RPC container " << m_overlayInputRPC_Name << endmsg;
    return StatusCode::FAILURE;
  }
  ATH_MSG_INFO("RPC MC   = "<<shortPrint(mcContainer.cptr()));

  /* RpcDigitContainer *rpc_temp_bkg = copyMuonDigitContainer<RpcDigitContainer,RpcDigit>(dataContainer.cptr());

  if ( m_storeGateTempBkg->record(rpc_temp_bkg, m_mainInputRPC_Name).isFailure() ) {
     msg( MSG::WARNING ) << "Failed to record background RpcDigitContainer to temporary background store " << endmsg;
     }*/

  SG::WriteHandle<RpcDigitContainer> outputContainer(m_mainInputRPC_Name, m_storeGateOutput->name());
  outputContainer = CxxUtils::make_unique<RpcDigitContainer>(dataContainer->size());
  //Do the actual overlay
  if(dataContainer.isValid() && mcContainer.isValid() && outputContainer.isValid()) { 
    this->overlayContainer(dataContainer.cptr(), mcContainer.cptr(), outputContainer.ptr());
  }
  ATH_MSG_INFO("RPC Result   = "<<shortPrint(outputContainer.cptr()));

  //----------------------------------------------------------------
  msg( MSG::DEBUG ) <<"Processing MC truth data"<<endmsg;

  // Main stream is normally real data without any MC info.
  // In tests we may use a MC generated file instead of real data.
  // Remove truth info from the main input stream, if any.
  //
  // Here we handle just RPC-specific truth classes.
  // (McEventCollection is done by the base.)

  // Now copy RPC-specific MC truth objects to the output.
  if ( m_copySDO )
      this->copyObjects<MuonSimDataCollection>(&*m_storeGateOutput, &*m_storeGateMC, m_sdo);

  //----------------------------------------------------------------
  msg( MSG::DEBUG ) << "RpcOverlay::execute() end"<< endmsg;
>>>>>>> release/21.0.127

  return StatusCode::SUCCESS;
}
