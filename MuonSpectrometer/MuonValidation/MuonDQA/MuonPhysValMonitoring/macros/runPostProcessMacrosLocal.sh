#!/bin/bash

if [ $# -lt 1 ]; then
    echo "Please give input file!"
    exit
fi

inputfilename=`basename $1 .root`
tmpfile=$(dirname $1)/$(basename $1 .root).PROC.root
cp $1 $tmpfile

## Settings doAverage option will add a TF1 function of a horizontal line
#  showing overall efficiency, resolution, scale, etc.

## Creates efficiency and other ratio plots
<<<<<<< HEAD
=======
<<<<<<< HEAD:MuonSpectrometer/MuonValidation/MuonDQA/MuonPhysValMonitoring/macros/MuonValidation_runPostProcessMacrosRtt.sh
#MuonValidation_CreateEffAndRecoFracPlots.py $tmpfile #doAverage

## Creates pt resolution and pt scale plots. doBinned option makes res plots for each bin in 2D hists
MuonValidation_CreateResolutionProfiles.py $tmpfile #doAverage doBinned

## Creates profiles of pull plots over pt
#MuonValidation_CreatePullProfiles.py $tmpfile #doAverage
=======
>>>>>>> release/21.0.127
python CreateEffAndRecoFracPlots.py $tmpfile #doAverage

## Creates pt resolution and pt scale plots. doBinned option makes res plots for each bin in 2D hists
python CreateResolutionProfiles.py $tmpfile #doAverage doBinned

## Creates profiles of pull plots over pt
python CreatePullProfiles.py $tmpfile #doAverage
<<<<<<< HEAD
=======
>>>>>>> release/21.0.127:MuonSpectrometer/MuonValidation/MuonDQA/MuonPhysValMonitoring/macros/runPostProcessMacrosLocal.sh
>>>>>>> release/21.0.127

## Normalizes all plots to unity (for comparisons). MUST RUN AFTER EFFICIENCY MACROS!!! OTHERWISE EFFICIENCY WILL BE WRONG!!!!
python normalizator.py $tmpfile
 
echo "Created $tmpfile."
