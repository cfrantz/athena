/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#ifndef IMUONRPCRODDECODER_H
#define IMUONRPCRODDECODER_H

#include "GaudiKernel/IAlgTool.h"
#include "ByteStreamData/RawEvent.h"
#include "eformat/SourceIdentifier.h"

<<<<<<< HEAD
class RpcPadContainer;
class RpcSectorLogicContainer;
class IdentifierHash;
=======

//using eformat::helper::SourceIdentifier;


class RpcPadContainer;

//using OFFLINE_FRAGMENTS_NAMESPACE::ROBFragment;
>>>>>>> release/21.0.127

namespace Muon
{

static const InterfaceID IID_IRpcROD_Decoder("Muon::IRpcROD_Decoder", 1, 0);

class IRpcROD_Decoder : virtual public IAlgTool
{
public:
    static const InterfaceID& interfaceID()
    {
        return IID_IRpcROD_Decoder;
    }

    // enter declaration of your interface-defining member functions here
<<<<<<< HEAD
    // bool decodeSL - Used to determine whether sector logic is being decoded too
    virtual StatusCode fillCollections(const OFFLINE_FRAGMENTS_NAMESPACE::ROBFragment& robFrag, 
                         			   RpcPadContainer& rdoIdc, const std::vector<IdentifierHash> &collections, RpcSectorLogicContainer*, const bool& decodeSL) const = 0;
=======
    virtual StatusCode fillCollections(const OFFLINE_FRAGMENTS_NAMESPACE::ROBFragment& robFrag, 
                         RpcPadContainer& rdoIdc,
			 std::vector<IdentifierHash>collections) const = 0;
>>>>>>> release/21.0.127

};

} // end of namespace

#endif
