/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "RpcROD_Decoder.h"

#include <deque>
#include <vector>
#include <utility>
#include <algorithm>

#include "eformat/Version.h"
#include "ByteStreamData/RawEvent.h"
#include "AthenaBaseComps/AthAlgTool.h"

Muon::RpcROD_Decoder::RpcROD_Decoder (const std::string& type, const std::string& name, const IInterface* parent) :
  AthAlgTool(type,name,parent) {
    declareInterface< IRpcROD_Decoder  >( this );
    declareProperty("SpecialROBNumber",m_specialROBNumber=-1);
    declareProperty("Sector13Data",m_sector13Data=false);
    declareProperty("DataErrorPrintLimit",m_maxprinterror=1000);
}

StatusCode Muon::RpcROD_Decoder::initialize() {
<<<<<<< HEAD

  ATH_CHECK(m_idHelperSvc.retrieve());

  ATH_CHECK(m_rpcReadKey.initialize());

  if (m_specialROBNumber>0) {
    ATH_MSG_DEBUG("Setting the special ROB Number to: 0x" << MSG::hex << m_specialROBNumber << MSG::dec );
  }
=======
 
    //   if (m_storeGate.retrieve().isFailure())
    //   {
    //       msg(MSG::ERROR) << "StoreGate not found " << endmsg;
    //       return StatusCode::FAILURE;
    //   }  
    //   if (m_storeGate.retrieve().isFailure())
    //   {
    //       msg(MSG::ERROR) << "StoreGate not found " << endmsg;
    //       return StatusCode::FAILURE;
    //   }


  ServiceHandle<IRPCcablingServerSvc> rpc_server("RPCcablingServerSvc", name());
  if (rpc_server.retrieve().isFailure()) {
	msg(MSG::FATAL) << " Can't get RPCcablingServerSvc " << endmsg;
	return StatusCode::FAILURE;
    }
  
    if (StatusCode::SUCCESS != rpc_server->giveCabling(m_cabling)) {
	msg(MSG::FATAL) << " Can't get RPCcablingSvc from Server" << endmsg;
	return StatusCode::FAILURE; 
    }

    StatusCode status=StatusCode::SUCCESS;
    /*
    try 
      {
	// needs to build differently with the id-hash 
	m_bsErrCont = new RpcByteStreamErrorContainer();
      } 
    catch(std::bad_alloc) {
      msg (MSG::FATAL)
        << "Could not create a new RPC ByteStreamErrorContainer!"<< endmsg;
      return StatusCode::FAILURE;
    }
    ATH_MSG_VERBOSE("RPC ByteStreamErrorContainer created");
    status = evtStore()->record(m_bsErrCont,m_bsErrContainerName);
    if (status!=StatusCode::SUCCESS)
      {
    	ATH_MSG_ERROR("Problems while recording RPC ByteStreamErrorContainer in SG");
      }
    else 
      ATH_MSG_INFO("RPC ByteStreamErrorContainer recorded in SG");
    */	

    //   ServiceHandle<StoreGateSvc> detStore("DetectorStore", name());
    //   if (detStore.retrieve().isFailure())
    //   {
    //       msg(MSG::ERROR) << "DetectorStore not found" << endmsg;
    //       return StatusCode::FAILURE;
    //   }

    /*  Do I have to use MuonMgr to retrieve the IdHelpers???
	if (detStore->retrieve(m_muonMgr).isFailure())
	{
	msg(MSG::ERROR) << "Cannot retrieve MuonDetectorManager" << endmsg;
	return sc;
	}
    */  

    // Get the RPC id helper from the detector store
    status = detStore()->retrieve(m_pRpcIdHelper, "RPCIDHELPER");
    if (status.isFailure()) {
	msg(MSG::FATAL) << "Could not get RpcIdHelper !" << endmsg;
	return StatusCode::FAILURE;
    } 
    else {
	msg(MSG::VERBOSE) << " Found the RpcIdHelper. " << endmsg;
    }
  
    //m_hashfunc = new RpcPadIdHash();
  
    if (m_specialROBNumber>0) {
	msg(MSG::DEBUG) << "Setting the special ROB Number to: 0x" << MSG::hex << m_specialROBNumber
			<< MSG::dec <<endmsg;
    }

    //==LBTAG initialize vector and variables for format failure check
    for(int i=0; i<13; i++) m_RPCcheckfail[i]=0;
    m_previous=0;
    m_printerror=0;

    /*
    IIncidentSvc* incsvc;
    status = service("IncidentSvc", incsvc);
    int priority = 100;
    if( status.isSuccess() ) {
        incsvc->addListener( this, "BeginEvent", priority);
	incsvc->addListener( this, "EndEvent", priority);
    }	
    */

    // //niko
    // if (m_byteStreamErrSvc.retrieve().isFailure()) 
    // msg(MSG::FATAL) <<"Failed to get RPC_ByteStreamErrorSvc"<<endmsg;

    return StatusCode::SUCCESS;
>>>>>>> release/21.0.127

  //==LBTAG initialize vector and variables for format failure check
  for(int i=0; i<13; i++) m_RPCcheckfail[i]=0;
  m_printerror=0;

<<<<<<< HEAD
  return StatusCode::SUCCESS;
=======
//At the start of every event, create the bytestream error contatiner and store it to 
//Store-Gate but not fill here  
void Muon::RpcROD_Decoder::handle(const Incident& inc) {
  if ((inc.type() == "BeginEvent") ){
    msg(MSG::DEBUG) << "In handle::BeginEvent"<<endmsg;   
    /*
    // cleanup and record the error container 
    if (!evtStore()->contains<Muon::RpcByteStreamErrorContainer>(m_bsErrContainerName)){
      m_bsErrCont->clear();
      msg(MSG::DEBUG) << "In handle::BeginEvent after clear"<<endmsg;   
      //ServiceHandle<StoreGateSvc> sg = evtStore();
      //msg(MSG::DEBUG) << "In handle::BeginEvent after eventStore"<<endmsg;   
      // StatusCode status = evtStore()->record(m_bsErrCont,m_bsErrContainerName);
      // msg(MSG::DEBUG) << "In handle::BeginEvent after record"<<endmsg;   
      // if (status.isFailure() )
      // 	msg(MSG::ERROR) << "Failed to record BSErrors to SG"<<endmsg;   
    }
    */
  }
  
  return;
>>>>>>> release/21.0.127
}



StatusCode Muon::RpcROD_Decoder::finalize() {
  //==LBTAG print format failure final statistics
  printcheckformat();
  return StatusCode::SUCCESS;
} 


#include "RpcROD_Decoder_v302.C"
