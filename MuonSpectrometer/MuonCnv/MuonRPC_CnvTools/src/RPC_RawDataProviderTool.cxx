/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "RPC_RawDataProviderTool.h"

#include "ByteStreamCnvSvcBase/IROBDataProviderSvc.h"

#include "Gaudi/Interfaces/IOptionsSvc.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ThreadLocalContext.h"

// #include "valgrind/callgrind.h"

#define padMaxIndex 600
#define slMaxIndex  200


<<<<<<< HEAD
=======
using eformat::helper::SourceIdentifier;
>>>>>>> release/21.0.127
using OFFLINE_FRAGMENTS_NAMESPACE::ROBFragment;

Muon::RPC_RawDataProviderTool::RPC_RawDataProviderTool(
    const std::string& t,
    const std::string& n,
    const IInterface*  p) :
    RPC_RawDataProviderToolCore(t, n, p),
    m_AllowCreation(false)
{
  declareInterface<IMuonRawDataProviderTool>(this);
}


Muon::RPC_RawDataProviderTool::~RPC_RawDataProviderTool()
{}


StatusCode Muon::RPC_RawDataProviderTool::initialize()
{
<<<<<<< HEAD
    ATH_CHECK(RPC_RawDataProviderToolCore::initialize());
=======

    StatusCode sc = AlgTool::initialize();
    if (sc.isFailure()) return sc;

   sc = service("ActiveStoreSvc", m_activeStore);
   if ( !sc.isSuccess() ) {
      ATH_MSG_FATAL(  "Could not get active store service" );
      return sc;
   }

    if (m_decoder.retrieve().isFailure())
    {
        ATH_MSG_FATAL( "Failed to retrieve tool " << m_decoder );
        return StatusCode::FAILURE;
    }
    else
        ATH_MSG_INFO( "Retrieved tool " << m_decoder );


    //m_hashfunc = new RpcPadIdHash();

    // get cabling svc
    const IRPCcablingServerSvc* RpcCabGet = 0;
    sc = service("RPCcablingServerSvc", RpcCabGet);
    if (sc.isFailure()) {
      ATH_MSG_FATAL( "Could not get RPCcablingServerSvc !" );
      return StatusCode::FAILURE;
    }
    else ATH_MSG_VERBOSE( " RPCcablingServerSvc retrieved" );
  
    sc = RpcCabGet->giveCabling(m_rpcCabling);
    if (sc.isFailure()) {
      ATH_MSG_FATAL( "Could not get RPCcablingSvc from the Server !" );
      m_rpcCabling = 0;
      return StatusCode::FAILURE;
    } 
    else {
      ATH_MSG_VERBOSE( " RPCcablingSvc obtained " );
    }    

    // Get ROBDataProviderSvc
    if (m_robDataProvider.retrieve().isFailure()) {
      ATH_MSG_FATAL("Failed to retrieve serive " << m_robDataProvider);
      return StatusCode::FAILURE;
    } else
      ATH_MSG_INFO( "Retrieved service " << m_robDataProvider);
    
    // Check if EventSelector has the ByteStreamCnvSvc
    bool has_bytestream = false;
    IJobOptionsSvc* jobOptionsSvc;
    sc = service("JobOptionsSvc", jobOptionsSvc, false);
    if (sc.isFailure()) {
      ATH_MSG_DEBUG( "Could not find JobOptionsSvc");
      jobOptionsSvc = 0;
    } else {
      IService* svc = dynamic_cast<IService*>(jobOptionsSvc);
      if(svc != 0 ) {
        ATH_MSG_INFO( " Tool = " << name() 
           << " is connected to JobOptionsSvc Service = "
           << svc->name() );
      } else return StatusCode::FAILURE;
    }
    
    IJobOptionsSvc* TrigConfSvc;
    sc = service("TrigConf::HLTJobOptionsSvc", TrigConfSvc, false);
    if (sc.isFailure()) {
      msg(MSG::DEBUG) << "Could not find TrigConf::HLTJobOptionsSvc" << endmsg;
      TrigConfSvc = 0;
    } else {
      IService* svc = dynamic_cast<IService*>(TrigConfSvc);
      if(svc != 0 ) {
        ATH_MSG_INFO( " Tool = " << name() 
           << " is connected to JobOptionsSvc Service = "
           << svc->name() );
      } else return StatusCode::FAILURE;
    }
    
    if(jobOptionsSvc==0 && TrigConfSvc==0)
    {
      ATH_MSG_FATAL( "Bad job configuration" );
      return StatusCode::FAILURE;  
    }
>>>>>>> release/21.0.127
    
    ServiceHandle<Gaudi::Interfaces::IOptionsSvc> joSvc("JobOptionsSvc", name());
    ATH_CHECK(joSvc.retrieve());

    // register the container only when the input from ByteStream is set up
    if (joSvc->has("EventSelector.ByteStreamInputSvc") || m_containerKey.key() != "RPCPAD")
    {
<<<<<<< HEAD
      m_AllowCreation = true;
=======
        for (std::vector<const Property*>::const_iterator 
            cur  = eventSelProps->begin();
      cur != eventSelProps->end(); cur++) {
      
          if( (*cur)->name() == "ByteStreamInputSvc" ) has_bytestream = true;
        }
    } 
    else has_bytestream = true;
    //{
    //    msg(MSG::FATAL) << "Cannot get the job configuration" << endmsg;
  //return StatusCode::FAILURE;
    //}
    
    
    // register the container only when the imput from ByteStream is set up     
    m_activeStore->setStore( &*evtStore() ); 
    if( has_bytestream || m_rdoContainerKey != "RPCPAD" )
    {
        RpcPadContainer* container = 
      Muon::MuonRdoContainerAccess::retrieveRpcPad(m_rdoContainerKey);
      
  // create and register the container only once
        if(container==0)
        {
            try 
            {
                container = new RpcPadContainer(padMaxIndex);
            } 
            catch(std::bad_alloc) 
            {
              ATH_MSG_FATAL( "Could not create a new RPC PAD container!");
                return StatusCode::FAILURE;
            }

            // record the container for being used by the convert method

            if( Muon::MuonRdoContainerAccess::record(container,
                                                     m_rdoContainerKey,
                                                     serviceLocator(), msg(), 
                                                     (&*evtStore())).isFailure() )
            {
              ATH_MSG_FATAL("Recording of container " 
                  << m_rdoContainerKey
                  << " into MuonRdoContainerManager has failed");
              return StatusCode::FAILURE;
            }
  }
  
  RpcSectorLogicContainer* sec = 
      Muon::MuonRdoContainerAccess::retrieveRpcSec("RPC_SECTORLOGIC");
  
  if(sec==0)
        {
            try 
            {
                sec = new RpcSectorLogicContainer();
            } 
            catch(std::bad_alloc) 
            {
              ATH_MSG_WARNING( "Could not create a new RpcSectorLogic container!");
                //return StatusCode::FAILURE;
            }

            // record the container for being used by the convert method

            if( Muon::MuonRdoContainerAccess::record(sec,
                                                     "RPC_SECTORLOGIC",
                                                     serviceLocator(),
                                                     msg()).isFailure() )
            {
              ATH_MSG_WARNING("Recording of container " 
              << "RPC_SECTORLOGIC"
              << " into MuonRdoContainerManager has failed");
              //return StatusCode::FAILURE;
            }
  }
>>>>>>> release/21.0.127
    }
    else
    {
      ATH_MSG_DEBUG( "ByteStream conversion service not found.\n"<< "RPC PAD container not registered.");
    }
    
    ATH_MSG_INFO( "initialize() successful in " << name());
    
    return StatusCode::SUCCESS;
}


// the new one 
StatusCode Muon::RPC_RawDataProviderTool::convert()
{
  return this->convert( Gaudi::Hive::currentContext() );
}

StatusCode Muon::RPC_RawDataProviderTool::convert(const EventContext& ctx) const
{
  SG::ReadCondHandle<RpcCablingCondData> readHandle{m_readKey, ctx};
  const RpcCablingCondData* readCdo{*readHandle};
//CALLGRIND_START_INSTRUMENTATION
  /// 
  //m_decoder->setSLdecodingRequest();
  // We used to set a variable in decoder to turn on SL decoding, but not allowed with const functions
  // So we will pass this through with EventContext and it was only active when running
  // full decoding it seems
  std::vector<const OFFLINE_FRAGMENTS_NAMESPACE::ROBFragment*> vecOfRobf;
  std::vector<uint32_t> robIds = readCdo->giveFullListOfRobIds();
  m_robDataProvider->getROBData( robIds, vecOfRobf);
//CALLGRIND_STOP_INSTRUMENTATION
  return convert(vecOfRobf, ctx); // using the old one
}

// the old one 
StatusCode Muon::RPC_RawDataProviderTool::convert(const ROBFragmentList& vecRobs)
{
  return this->convert( vecRobs, Gaudi::Hive::currentContext() );
}

StatusCode Muon::RPC_RawDataProviderTool::convert(const ROBFragmentList& vecRobs, const EventContext& ctx) const
{
 //CALLGRIND_START_INSTRUMENTATION
    std::vector<IdentifierHash> collections;
 //CALLGRIND_STOP_INSTRUMENTATION
    return convert(vecRobs, collections, ctx); 
}

// the new one 
StatusCode Muon::RPC_RawDataProviderTool::convert(const std::vector<uint32_t>& robIds)
{
  return this->convert( robIds, Gaudi::Hive::currentContext() );
}

StatusCode Muon::RPC_RawDataProviderTool::convert(const std::vector<uint32_t>& robIds, const EventContext& ctx) const
{
 //CALLGRIND_START_INSTRUMENTATION
    std::vector<IdentifierHash> collections;
    std::vector<const OFFLINE_FRAGMENTS_NAMESPACE::ROBFragment*> vecOfRobf;
    m_robDataProvider->getROBData(robIds, vecOfRobf);
 //CALLGRIND_STOP_INSTRUMENTATION
    return convert(vecOfRobf, collections, ctx); 
}

// the new one
StatusCode Muon::RPC_RawDataProviderTool::convert(const std::vector<IdentifierHash>& rdoIdhVect)
{
  return this->convert( rdoIdhVect, Gaudi::Hive::currentContext() );
}

StatusCode Muon::RPC_RawDataProviderTool::convert(const std::vector<IdentifierHash>& rdoIdhVect, const EventContext& ctx) const
{
  SG::ReadCondHandle<RpcCablingCondData> readHandle{m_readKey, ctx};
  const RpcCablingCondData* readCdo{*readHandle};
 //CALLGRIND_START_INSTRUMENTATION
    std::vector<const OFFLINE_FRAGMENTS_NAMESPACE::ROBFragment*> vecOfRobf;
    std::vector<uint32_t> robIds;
    CHECK( readCdo->giveROB_fromRDO(rdoIdhVect, robIds) );
    m_robDataProvider->getROBData(robIds, vecOfRobf);
//CALLGRIND_STOP_INSTRUMENTATION
    return convert(vecOfRobf, rdoIdhVect, ctx); // using the old one 
}

// the old one 
StatusCode Muon::RPC_RawDataProviderTool::convert(const ROBFragmentList& vecRobs, const std::vector<IdentifierHash>& collections)
{
  return this->convert( vecRobs, collections, Gaudi::Hive::currentContext() );
}

StatusCode Muon::RPC_RawDataProviderTool::convert(const ROBFragmentList& vecRobs, const std::vector<IdentifierHash>& collections, const EventContext& ctx) const
{
 //CALLGRIND_START_INSTRUMENTATION

  if(m_AllowCreation == false)
  {
    ATH_MSG_WARNING( "Container create disabled due to byte stream");

        return StatusCode::SUCCESS; // Maybe it should be false to stop the job
                                    // because the convert method should not
                                    // have been called .... but this depends
                                    // on the user experience
  }

  SG::WriteHandle<RpcPadContainer> rdoContainerHandle(m_containerKey, ctx);
  SG::WriteHandle<RpcSectorLogicContainer> logicHandle(m_sec, ctx);

  RpcPadContainer* pad = 0;
  RpcSectorLogicContainer* logic = 0;

  // here we have to check if the container is already present and if it is we retrieve from SG
  if (rdoContainerHandle.isPresent()) {
    const RpcPadContainer* pad_c;
    ATH_CHECK( evtStore()->retrieve (pad_c, m_containerKey.key()) );
    pad = const_cast<RpcPadContainer*>(pad_c);
  } else {
    ATH_CHECK( rdoContainerHandle.record(std::make_unique<RpcPadContainer> (padMaxIndex) ) );
    pad = rdoContainerHandle.ptr();
  }

  if (logicHandle.isPresent()) {
    const RpcSectorLogicContainer* logic_c;
    ATH_CHECK( evtStore()->retrieve (logic_c, m_sec.key()) );
    logic = const_cast<RpcSectorLogicContainer*>(logic_c);                  
  } else {
    ATH_MSG_DEBUG("Writing out RpcSectorLogicContainer");
    ATH_CHECK( logicHandle.record (std::make_unique<RpcSectorLogicContainer>()) );
    logic = logicHandle.ptr();
  }
    
<<<<<<< HEAD
  // pass the containers to the convertIntoContainers function in the RPC_RawDataProviderToolCore base class
  ATH_CHECK( convertIntoContainers(vecRobs, collections, pad, logic, true) );

  return StatusCode::SUCCESS;
=======
    for (ROBFragmentList::const_iterator itFrag = vecRobs.begin(); itFrag != vecRobs.end(); itFrag++)
    {
        //convert only if data payload is delivered
        if ( (**itFrag).rod_ndata()!=0 )
        {
            std::vector<IdentifierHash> coll =
                                      to_be_converted(**itFrag,collections);
      
            if (m_decoder->fillCollections(**itFrag, *pad, coll).isFailure())
                {
                    // store the error conditions into the StatusCode and continue
                }
        }
        else
        {
	  if(msgLvl(MSG::DEBUG))
            {
                uint32_t sourceId= (**itFrag).source_id();
                msg(MSG::DEBUG) << " ROB " << MSG::hex << sourceId
        << " is delivered with an empty payload" << MSG::dec 
        << endmsg;
            }
            // store the error condition into the StatusCode and continue
        }
    }
    //in presence of errors return FAILURE
//CALLGRIND_STOP_INSTRUMENTATION
    return StatusCode::SUCCESS;
>>>>>>> release/21.0.127
}







