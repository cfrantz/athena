/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#ifndef MUONMdtRdoToPrepDataTool_H
#define MUONMdtRdoToPrepDataTool_H
     
#include "MdtRdoToPrepDataToolCore.h"
#include "CxxUtils/checker_macros.h"

namespace Muon 
{

  /** @class MdtRdoToPrepDataTool 

      This is for the Doxygen-Documentation.  
      Please delete these lines and fill in information about
      the Algorithm!
      Please precede every member function declaration with a
      short Doxygen comment stating the purpose of this function.
      
      @author  Edward Moyse <Edward.Moyse@cern.ch>
  */  

  class ATLAS_NOT_THREAD_SAFE MdtRdoToPrepDataTool : virtual public MdtRdoToPrepDataToolCore
  {
  public:
    MdtRdoToPrepDataTool(const std::string&,const std::string&,const IInterface*);

    /** default destructor */
    virtual ~MdtRdoToPrepDataTool()=default;

    /** standard Athena-Algorithm method */
    virtual StatusCode initialize() override;
      
<<<<<<< HEAD
  protected:
    virtual SetupMdtPrepDataContainerStatus setupMdtPrepDataContainer() override;
  }; 
=======
    private:
            
  
        /// store gate transactions
        StoreGateSvc * m_sgSvc;
        
        /// Muon Detector Descriptor
        const MuonGM::MuonDetectorManager * m_muonMgr;
        
        /// MDT identifier helper
        const MdtIdHelper * m_mdtHelper;
        
        /// MDT calibration service
        MdtCalibrationSvc* m_calibrationSvc;
        MdtCalibrationSvcSettings* m_mdtCalibSvcSettings; 
        MdtCalibHit* m_calibHit;
        double m_invSpeed;

        /// MdtPrepRawData containers
        Muon::MdtPrepDataContainer * m_mdtPrepDataContainer;
        std::string m_outputCollectionLocation;        
        
        /** member variables for algorithm properties: */
        /**If true, then 'fix' Identifiers from RDOs*/
        bool m_doIdentifierCorrection;
        bool m_calibratePrepData; //!< toggle on/off calibration of MdtPrepData
        bool m_decodeData; //!< toggle on/off the decoding of MDT RDO into MdtPrepData

        bool m_useBStoRdoTool;

        /** class member version of retrieving MsgStream */
    //        MsgStream* m_log;
        //bool m_debug;
        //bool m_verbose;

//         // Rob Data Provider handle
//         ServiceHandle<IROBDataProviderSvc>          m_robDataProvider;
        // handle tp the MDT_RawDataProviderTool
        ToolHandle<Muon::IMuonRawDataProviderTool> m_rawDataProviderTool;
        ToolHandle<Muon::IMDT_RDO_Decoder> m_mdtDecoder;
	ToolHandle<Muon::MuonIdHelperTool> m_idHelper;

	//retreive the cabling svc
      	MuonMDT_CablingSvc* m_mdtCabling;

        //keepTrackOfFullEventDecoding
        bool m_fullEventDone;

        bool m_isTestBeam; //!< Support for test beam data (twin tubes)

        bool m_BMEpresent;
        bool m_BMGpresent;
        int m_BMEid;
        int m_BMGid;

        // + TWIN TUBE
        bool   m_useTwin;
        bool   m_useAllBOLTwin;
        bool   m_use1DPrepDataTwin;
        bool   m_twinCorrectSlewing;
        bool   m_discardSecondaryHitTwin;
        //  const MdtCalibrationDbSvc* m_dbSvc;
        int twin_chamber[2][3][36];
        int secondaryHit_twin_chamber[2][3][36];
        // - TWIN TUBE

        std::map<Identifier, std::vector<Identifier> > m_DeadChannels;
        void initDeadChannels(const MuonGM::MdtReadoutElement* mydetEl);

    }; 
>>>>>>> release/21.0.127
} // end of namespace

#endif 
